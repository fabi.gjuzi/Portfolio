<?php
class Spot extends core\Model
{
    function getSpotWithAllBlocks($spotId)
	{
		$query_builder = $this->prepareQuery();
		//$query_builder->setVerbal(true);
        
		$query_builder->addJoinTable("block", "id_spot", "spot")
		  			  ->setJoinTableColumn("spot_id");
		$query_builder->addWhereField("spot_id", $spotId);
		return $query_builder->executeQuery();
	}

	public function getAllSpotsByUser($object)
    {
        $query_builder = $this->prepareQuery();
        $query = 'call  getAllSpotsByUser("'.$object['username'].'");';
        $query_builder->setCustomQuery($query);
        return $query_builder->executeCustomQuery();
    }
}