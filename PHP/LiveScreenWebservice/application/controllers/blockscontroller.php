<?php
use core\Controller;

class BlocksController extends Controller
{
    protected $_with_model = true;

    /**
     *
     * @method GET
     */

    public function getAll()
    {
        $request_body = $this->Block->selectAll();
        if(empty($request_body))
        {
            $this->set("message", "The request returned 0 values");
            $this->set("response_body", []);
            $this->set("status", 500);
            $this->set("result", "error");
            $this->set("code", 1);
        }
        else
        {
            $this->set("message", "The request completed succesfully");
            $this->set("response_body", $request_body);
            $this->set("result", "success");
            $this->set("status", 200);
            $this->set("code", 0);
        }
    }
    
    /**
     *
     * @method GET
     */

    public function get($id)
    {
        $request_body = $this->Block->selectById($id);
        if(empty($request_body))
        {
            if(empty($request_body))
            {
                $this->set("message", "The request returned 0 values.");
            }
            else if(is_string($request_body))
            {
                $this->set("message", $request_body);
            }

            $this->set("response_body", []);
            $this->set("status", 500);
            $this->set("result", "error");
            $this->set("code", 1);
        }
        else
        {
            $this->set("message", "The request completed succesfully");
            $this->set("response_body", $request_body);
            $this->set("result", "success");
            $this->set("status", 200);
            $this->set("code", 0);
        }
    }
    
    /**
     *
     * @method DELETE
     */
    
    public function delete($id)
    {
        $request = $this->Block->deleteById($id);
        $this->set("message", $request);
        $this->set("response_body", []);
        $this->set("status", 200);
        $this->set("result", "success");
        $this->set("code", 0);
        
    }

    /**
     *
     * @method POST
     */

    public function add($object)
    {
        $request = $this->Block->add($object);
        if($request === true)
        {
            $request_body = $this->Block->select($object);
            if($request_body == false)
            {
                $this->set("message", "There was an error in the database, could not retrieve data!");
                $this->set("response_body", []);
                $this->set("status", 500);
                $this->set("result", "error");
                $this->set("code", 2);
            }
            else
            {
                $this->set("message", "The request completed succesfully");
                $this->set("response_body", $request_body);
                $this->set("result", "success");
                $this->set("status", 200);
                $this->set("code", 0);
            }
        }
        else
        {
            $this->set("message", $request);
            $this->set("response_body", []);
            $this->set("status", 500);
            $this->set("result", "error");
            $this->set("code", 1);
        }
    }

    /**
     *
     * @method PUT
     */

    public function update($object)
    {
        $response = $this->Block->update($object);
        if($response === true)
        {
            $this->set("message", "The request completed succesfully");
            $this->set("response_body", $object);
            $this->set("result", "success");
            $this->set("status", 200);
            $this->set("code", 0);
        }
        else if($response === false)
        {
            $this->set("message", $response);
            $this->set("response_body", []);
            $this->set("status", 500);
            $this->set("result", "error");
            $this->set("code", 2);
        }
        else if(is_string($response))
        {
            $this->set("message", $response);
            $this->set("response_body", []);
            $this->set("status", 500);
            $this->set("result", "error");
            $this->set("code", 1);
        }
    }
}
