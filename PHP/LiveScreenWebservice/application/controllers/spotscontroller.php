<?php
use core\Controller;

class SpotsController extends Controller
{
    protected $_with_model = true;

    /**
     *
     * @method POST
     */
    public function generateVideo($json)
    {
    	$block = new Block();
    	$query_buidler = $block->prepareQuery();
    	$query_buidler->setVerbal(false)
    				  ->addWhereField("id_spot", 22);
    	$query_buidler->addJoinTable("content_type", "content_type_id", "block")
    				  ->setJoinTableColumn("id_content_type");
    	$request_body = $query_buidler->executeQuery();
    	
    	//print_r($request_body);
    	
    	//find max z index
    	$max_z = 0; 
    	for($i = 0; $i < count($request_body); $i++)
    	{
    		if($request_body[$i]->z_index > $max_z)
    		{
    			$max_z = $request_body[$i]->z_index;
    		}
    	}
    	
    	//each row of the 2D array represents a z_index which gets filled with the elements
		$array = array();
    	for($j = 0; $j <= $max_z; $j++){
    		$array[$j] = array();
    		$count = 0;
    		for($i = 0; $i < count($request_body); $i++)
    		{
    			if($request_body[$i]->z_index == $j)
    			{
    				$array[$j][$count] = $request_body[$i];
    				$count++;
    			}
    		}
    	}
    	
    	//now sort each row of the 2D array by the elements start time
    	for($k = 0; $k <= $max_z; $k++)
    	{
    		//perform sorting on row
    		$temp;
    		for($j = 0; $j < count($array[$k]) - 1; $j++)
    		{
    			for($i = $j + 1; $i < count($array[$k]); $i++)
    			{
    				if($array[$k][$i]->start_time < $array[$k][$j]->start_time)
    				{
    					$temp = $array[$k][$i];
    					$array[$k][$i] = $array[$k][$j];
    					$array[$k][$j] = $temp;
    				}
    			}
    		}
    	}
    	
    	$video_duration = 0;
    	for($j = 0; $j <= $max_z; $j++)
    	{
    		for($i = 0; $i < count($array[$j]); $i++)
    		{
    			if($array[$j][$i]->start_time + $array[$j][$i]->duration > $video_duration)
    			{
    				$video_duration = $array[$j][$i]->start_time + $array[$j][$i]->duration;
    			}
    		}
    	}
    	
    	print_r($array);
    	
    	chdir("video_resources");
    	
    	$unix_time = time();
    	
    	$command_string = "rm -f generated.mp4 && ffmpeg -f lavfi -i color=c=black:s=1280x720:d=" . $video_duration . " -vf "
    			. '"drawtext=fontfile=/path/to/font.ttf:fontsize=30:fontcolor=white:x=(w-text_w)/2:y=(h-text_h)/2:text='
    			. "''" . '"' . " generated" . $unix_time . ".mp4 2>&1";
    	
    	$command_string .= " && ffmpeg -f lavfi -y -i anullsrc=channel_layout=stereo:sample_rate=44100 -y -i generated" . $unix_time 
    					. ".mp4 -shortest -c:v copy -c:a aac temp" . $unix_time . ".mp4";
    	$command_string .= " && rm -f generated" . $unix_time . ".mp4 && mv temp" . $unix_time . ".mp4 generated" . $unix_time . ".mp4";
    			
    	//start generating the shell commands to execute
    	//add audio after the video has been generated
    	for($j = 0; $j <= $max_z; $j++)
    	{
    		for($i = 0; $i < count($array[$j]); $i++)
    		{	
    			$layout_string;
    			if(strcmp($array[$j][$i]->name, "image") == 0 )
    			{	
    				if($array[$j][$i]->id_layout == 1)
    				{
    					$layout_string = "0:0";
    				}
    				else if($array[$j][$i]->id_layout == 2)
    				{
    					$layout_string = "(main_w-overlay_w):0";
    				}
    				else if($array[$j][$i]->id_layout == 3)
    				{
    					$layout_string = "(main_w-overlay_w)/2:(main_h-overlay_h)/2";
    				}
    				else if($array[$j][$i]->id_layout == 4)
    				{
    					$layout_string = "0:0";
    					$command_string .= " && ffmpeg -y -i droplet_copy.jpg -vf scale=-1280:720 droplet.png 2>&1";
    				}
    				else if($array[$j][$i]->id_layout == 5)
    				{
    					$layout_string = "0:(main_h-overlay_h)";
    				}
    				else if($array[$j][$i]->id_layout == 6)
    				{
    					$layout_string = "(main_w-overlay_w):(main_h-overlay_h)";
    				}
    				
    				if($array[$j][$i]->id_animation_entry == 1 && $array[$j][$i]->id_animation_exit == 2)
    				{
    					$command_string .= " && ffmpeg -y -i generated" . $unix_time . ".mp4 -loop 1 -i droplet.jpg -filter_complex " . '"'
										   . "[1] format=rgba,fade=in:st=" . $array[$j][$i]->start_time . ":d=1:alpha=1,"
										   . "fade=out:st=" . ($array[$j][$i]->start_time + $array[$j][$i]->duration - 1) 
										   . ":d=1:alpha=1 [ovr]; [0][ovr] overlay=" 
										   . $layout_string
										   . '"' . " -t " . $video_duration . " -y temp" . $unix_time . ".mp4 2>&1";
						$command_string .= " && rm -f generated" . $unix_time . ".mp4 && mv temp" . $unix_time 
										   . ".mp4 generated" . $unix_time . ".mp4";
    				}
    				else if($array[$j][$i]->id_animation_entry == 1 && $array[$j][$i]->id_animation_exit == 4)
    				{
    					$command_string .= " && ffmpeg -y -i generated" . $unix_time . ".mp4 -loop 1 -i droplet.jpg -filter_complex "
    									   . '"[1]format=yuva420p, fade=in:st=' . $array[$j][$i]->start_time .':d=1:alpha=1[i]; '
    									   . '[0][i]overlay=' . $layout_string 
    									   . ':' 
    									   . "enable='between(t," . $array[$j][$i]->start_time . "," 
    									   . ($array[$j][$i]->start_time + $array[$j][$i]->duration) .")'" 
    									   . '" ' . ' -t ' . $video_duration . ' -c:v libx264 temp' . $unix_time . '.mp4 2>&1';
    					$command_string .= " && rm -f generated" . $unix_time . ".mp4 && mv temp" . $unix_time 
    										. ".mp4 generated" . $unix_time . ".mp4";
    				}
    				else if($array[$j][$i]->id_animation_exit == 2 && $array[$j][$i]->id_animation_entry == 3)
    				{
    					$command_string .= " && ffmpeg -y -i generated" . $unix_time . ".mp4 -loop 1 -i sunset.jpg -filter_complex "
    									   . '"[1]format=yuva420p, fade=out:st=' 
    									   . ($array[$j][$i]->start_time + $array[$j][$i]->duration - 1) .':d=1:alpha=1[i]; [0][i]overlay=W-w-100:H-h-100:'
    									   . "enable='between(t," . $array[$j][$i]->start_time . ","
    									   . ($array[$j][$i]->start_time + $array[$j][$i]->duration) .")':"
    									   .'shortest=1" ' . '-c:v libx264 temp' . $unix_time . '.mp4 2>&1';
    					$command_string .= " && rm -f generated" . $unix_time . ".mp4 && mv temp" . $unix_time 
    										. ".mp4 generated" . $unix_time . ".mp4";
    				}
    				else 
    				{
    					$command_string .=  " && ffmpeg -y -i generated" . $unix_time . ".mp4 -i sunset.jpg -filter_complex " . '"[0:v][1:v] overlay=25:25:enable='
    										. "'between(t," . $array[$j][$i]->start_time . "," . ($array[$j][$i]->start_time + $array[$j][$i]->duration) . ")'"
    										. '"' . " -pix_fmt yuv420p -c:a copy temp" . $unix_time . ".mp4 2>&1";
    					$command_string .= " && rm -f generated" . $unix_time . ".mp4 && mv temp" . $unix_time 
    										. ".mp4 generated" . $unix_time . ".mp4";
    				}
    				
    			}
    			else if(strcmp($array[$j][$i]->name, "text") == 0 )
    			{	
    				if($array[$j][$i]->id_layout == 1)
    				{
    					$layout_string = "x=0: y=0";
    				}
    				else if($array[$j][$i]->id_layout == 2)
    				{
    					$layout_string = "x=(w-text_w): y=0";
    				}
    				else if($array[$j][$i]->id_layout == 3)
    				{
    					$layout_string = "x=(w-text_w)/2: y=(h-text_h)/2";
    				}
    				else if($array[$j][$i]->id_layout == 5)
    				{
    					$layout_string = "x=0: y=(h-text_h)";
    				}
    				else if($array[$j][$i]->id_layout == 6)
    				{
    					$layout_string = "x=(w-text_w): y=(h-text_h)";
    				}
    				
    				$FID = 1; // fade in duration
    				$FOD = 1; // fade out duration
    				$DS = 1.0; // display start
    				$DE = 3.0; // display end
    				
    				if($array[$j][$i]->id_animation_entry == 1 && $array[$j][$i]->id_animation_exit == 2)
    				{
    					$DS = $array[$j][$i]->start_time;	
    					$DE = $array[$j][$i]->start_time + $array[$j][$i]->duration - 1;
    				}
    				else if($array[$j][$i]->id_animation_entry == 1 && $array[$j][$i]->id_animation_exit == 4)
    				{
    					$DS = $array[$j][$i]->start_time;
    					$FOD = 0.001;
    				}
    				else if($array[$j][$i]->id_animation_entry == 3 && $array[$j][$i]->id_animation_exit == 2)
    				{
    					$DE = $array[$j][$i]->start_time + $array[$j][$i]->duration - 1;
    					$FID = 0.001;
    				}
    				else
    				{
    					$FID = 0.001;
    					$FOD = 0.001;
    				}
    				
    				$command_string .= ' && ffmpeg -y -i generated' . $unix_time . '.mp4 -vf drawtext="fontfile=/usr/share/TTF/DejaVuSerif.ttf:'
    								   	. "text='Stack Overflow': fontcolor=white: fontsize=24:" . $layout_string .":"
    								   	. "fontcolor_expr=fff000%{eif\\\\\\\\: clip(255*(1*between(t\\\\, $DS + $FID\\\\, $DE - $FOD) + "
    									. "((t - $DS)/$FID)*between(t\\\\, $DS\\\\, $DS + $FID) + (-(t - $DE)/$FOD)"
    									. "*between(t\\\\, $DE - $FOD\\\\, $DE) )\\\\, 0\\\\, 255) \\\\\\\\: x\\\\\\\\: 2 }:"
    									. "enable='between(t," . $array[$j][$i]->start_time . "," 
    									. ($array[$j][$i]->start_time + $array[$j][$i]->duration) . ")'"
    									. '"' . " -codec:a copy temp" . $unix_time . ".mp4 2>&1";
    				$command_string .= " && rm -f generated" . $unix_time . ".mp4 && mv temp" . $unix_time 
    									. ".mp4 generated" . $unix_time . ".mp4";
    				
    			}
    			else if(strcmp($array[$j][$i]->name, "video") == 0 )
    			{
    				if($array[$j][$i]->id_layout == 1)
    				{
    					$layout_string = "0:0";
    				}
    				else if($array[$j][$i]->id_layout == 2)
    				{
    					$layout_string = "(main_w-overlay_w):0";
    				}
    				else if($array[$j][$i]->id_layout == 3)
    				{
    					$layout_string = "(main_w-overlay_w)/2:(main_h-overlay_h)/2";
    				}
    				else if($array[$j][$i]->id_layout == 4)
    				{
    					$layout_string = "0:0";
    					$command_string .= " && ffmpeg -y -i butterfly_copy.mp4 -vf scale=1280:720 butterfly.mp4 2>&1";
    				}
    				else if($array[$j][$i]->id_layout == 5)
    				{
    					$layout_string = "0:(main_h-overlay_h)";
    				}
    				else if($array[$j][$i]->id_layout == 6)
    				{
    					$layout_string = "(main_w-overlay_w):(main_h-overlay_h)";
    				}
    				
    				if($array[$j][$i]->id_animation_entry == 1 && $array[$j][$i]->id_animation_exit == 2)
    				{
    					
    					//$command_string .= " && ffmpeg -y -itsoffset 2 -i flower.mp4 test.mp4";
    					
    					$command_string .= " && ffmpeg -y -itsoffset 2 -i butterfly.mp4 -i generated" . $unix_time . ".mp4 -filter_complex "
    										. '"[1:v]setpts=PTS-0/TB[a] ;[0:v]format=pix_fmts=yuva420p'
    										. ',fade=t=in:st=' . $array[$j][$i]->start_time . ':d=1:alpha=1'
    										. ',fade=t=out:st=' . ($array[$j][$i]->start_time +$array[$j][$i]->duration - 1) .':d=1:alpha=1'
    										. ',[a]overlay=' . $layout_string 
    										. ':enable=gte(t\,'. ($array[$j][$i]->start_time - 2) .'):[out]"'
    										. " -map [out] -map 0:1 -map 1:1 -c:v libx264 -crf 18 -pix_fmt yuv420p -c:a copy temp2" . $unix_time . ".mp4 2>&1";
    					
    					$command_string .= " && ffmpeg -y -i temp2" . $unix_time . ".mp4 -filter_complex "
    										. '"[0:1][0:2] amix=inputs=2:duration=longest"' . ' -c:a pcm_s16le temp2' . $unix_time . '.mkv 2>&1';
    					
    					/*$command_string .= " && ffmpeg -y -i generated.mp4 -i butterfly.mp4 -filter_complex "
    										. '"' . '[0:v]setpts=PTS-STARTPTS[v0];[1:v]setpts=PTS-STARTPTS+7/TB[v1];[v0][v1]overlay=eof_action=pass[out1]' .'"' 
    										. " -map [out1] temp2.mp4 2>&1";*/
    					
    					
    					/*$command_string .= " && ffmpeg -y -i butterfly.mp4 -i generated.mp4 -filter_complex "
    										. '"' . "[0:v]fps=fps=30, setpts=PTS-STARTPTS, scale=1280:720, pad=iw*2:ih:iw:0[in0]; [in0][1:v] overlay=0:0 [outv];"
    										. "[0:1][1:1] amix [outa]" . '"' . ' -map [outv] -map [outa] final.mp4 2>&1';*/
    					
    					$command_string .= " && rm -f temp2" . $unix_time . ".mp4 && mv temp2" . $unix_time 
    									. ".mkv generated" . $unix_time . ".mp4";
    				}
    				else if($array[$j][$i]->id_animation_entry == 1 && $array[$j][$i]->id_animation_exit == 4)
    				{
    					$command_string .= " && ffmpeg -y -i butterfly.mp4 -i generated" . $unix_time . ".mp4 -filter_complex "
    										. '"[1:v]setpts=PTS-0/TB[a] ;[0:v]format=pix_fmts=yuva420p'
    										. ',fade=t=in:st=' . $array[$j][$i]->start_time . ':d=1:alpha=1'
    										.':d=1:alpha=1'
    										. ',[a]overlay=' . $layout_string
    										. ':enable=gte(t\,'. ($array[$j][$i]->start_time) .'):shortest=1[out]"'
    										. " -map [out] -map 0:a? -c:v libx264 -crf 18 -pix_fmt yuv420p -c:a copy temp" . $unix_time . ".mp4 2>&1";
    										
    					$command_string .= " && rm -f generated" . $unix_time . ".mp4 && mv temp" . $unix_time 
    									. ".mp4 generated" . $unix_time . ".mp4";
    				}
    				else if($array[$j][$i]->id_animation_entry == 3 && $array[$j][$i]->id_animation_exit == 2)
    				{
    					
    				}
    				else 
    				{
	    				$command_string .= " && ffmpeg -y -i butterfly.mp4 -i generated" . $unix_time . ".mp4 -filter_complex" . '"[1:v]setpts=PTS-10/TB[a]; \
	                 					 	[0:v][a]overlay=enable=gte(t\,5):shortest=1[out]"'
	    									. "-map [out] -map 0:a? -c:v libx264 -crf 18 -pix_fmt yuv420p -c:a copy ov" . $unix_time . ".mp4";
    				}
    			}
    		}
    	}
    	
    	
    	/*
    	//fill audio array
    	$audio_array = array();
    	$count = 0;
    	for($j = 0; $j <= $max_z; $j++)
    	{
    		for($i = 0; $i < count($array[$j]); $i++)
    		{
    			if(strcmp($array[$j][$i]->name, "audio") == 0 )
    			{
    				$audio_array[$count] = $array[$j][$i];
    				$count++;
    			}
    		}
    	}
    	
    	//now sort each row of the 2D array by the elements start time
    	for($k = 0; $k <= $max_z; $k++)
    	{
    		//perform sorting on row
    		$temp;
    		for($j = 0; $j < count($audio_array[$k]) - 1; $j++)
    		{
    			for($i = $j + 1; $i < count($audio_array[$k]); $i++)
    			{
    				if($audio_array[$k][$i]->start_time < $audio_array[$k][$j]->start_time)
    				{
    					$temp = $audio_array[$k][$i];
    					$audio_array[$k][$i] = $audio_array[$k][$j];
    					$audio_array[$k][$j] = $temp;
    				}
    			}
    		}
    	}
    	
    	//create the unified audio file
    	for($i = 0; $i < count($audio_array) - 1; $i++)
    	{
    		if($i = 0)
    		{
    			$command_string .= "&& mv " . $audio_array[0]->name . "*" . " generated_audio.mp3";
    		}
    		else 
    		{ 
	    		if($audio_array[$i]->start_time + $audio_array[$i]->duration > $audio_array[$i + 1]->start_time)
	    		{
	    			//trim audio
	    			$command_string .= " && ffmpeg -i input.mp3 -ss " . $audio_array[$i]->start_time . " -t " 
	    								. ($audio_array[$i + 1]->start_time - $audio_array[$i]->start_time)  
	    								. "-acodec copy output.mp3";
	    		} else {
	    			
	    		}
    		}
    	}
    	*/

    	//add audio after video has been created
    	for($j = 0; $j <= $max_z; $j++)
    	{
    		for($i = 0; $i < count($array[$j]); $i++)
    		{
    			if(strcmp($array[$j][$i]->name, "audio") == 0 )
    			{	
    				//find audio offset
    				$offset = "";
    				if($array[$j][$i]->start_time < 60)
    				{
    					if($array[$j][$i]->start_time < 10)
    					{
    						$offset = "00:00:0" . (int) $array[$j][$i]->start_time . ".0";
    					}
    					else
    					{
    						$offset = "00:00:" . (int) $array[$j][$i]->start_time . ".0";
    					}
    				}
    				else if($array[$j][$i]->start_time >= 60 && $array[$j][$i]->start_time < 120)
    				{
    					if($array[$j][$i]->start_time - 60 < 10)
    					{
    						$offset = "00:01:0" . $array[$j][$i]->start_time . ".0";
    					}
    					else
    					{
    						$offset = "00:01:" . $array[$j][$i]->start_time . ".0";
    					}
    				}
    				else
    				{
    					$offset = "00:02:00.0";
    				}
    					
    				//append audio
    				$command_string .= " && ffmpeg -y -i generated" . $unix_time . ".mp4 -itsoffset " . $offset . " -i vivaldi.mp3 -vcodec copy -acodec copy -map 0:v -map 0:1 -map 1:a "
    									."temp" . $unix_time . ".mp4 2>&1"
    								   	. " && rm -f generated" . $unix_time . ".mp4 && mv temp" . $unix_time 
    									. ".mp4 generated" . $unix_time . ".mp4 2>&1";
    		
    				$command_string .= " && ffmpeg -y -i generated" . $unix_time . ".mp4 -filter_complex " 
    									. '"[0:1][0:2] amerge=inputs=2"' . ' -c:a pcm_s16le temp' . $unix_time . '.mkv 2>&1';
    									//. " && rm -f generated.mp4 && mv temp.mp4 generated.mp4";
    			
    			}
    		}
    	}
    	
    	echo "Shell** ";
    	echo $command_string;
    	echo shell_exec($command_string);
    	echo " **Shell";
    	
    	if(empty($request_body))
    	{
    		$this->set("message", "The request returned 0 values");
    		$this->set("response_body", []);
    		$this->set("status", 500);
    		$this->set("result", "error");
    		$this->set("code", 1);
    	}
    	else
    	{
    		$this->set("message", "The request completed succesfully");
    		$this->set("response_body", $request_body);
    		$this->set("result", "success");
    		$this->set("status", 200);
    		$this->set("code", 0);
    	}
    }
    
    /**
     *
     * @method GET
     */

    public function getAll()
    {
        $request_body = $this->Spot->selectAll();
        if(empty($request_body))
        {
            $this->set("message", "The request returned 0 values");
            $this->set("response_body", []);
            $this->set("status", 500);
            $this->set("result", "error");
            $this->set("code", 1);
        }
        else
        {
            $this->set("message", "The request completed succesfully");
            $this->set("response_body", $request_body);
            $this->set("result", "success");
            $this->set("status", 200);
            $this->set("code", 0);
        }
    }
    
    /**
     *
     * @method GET
     */

    public function get($id)
    {
        $request_body = $this->Spot->selectById($id);
        if(empty($request_body))
        {
            if(empty($request_body))
            {
                $this->set("message", "The request returned 0 values.");
            }
            else if(is_string($request_body))
            {
                $this->set("message", $request_body);
            }

            $this->set("response_body", []);
            $this->set("status", 500);
            $this->set("result", "error");
            $this->set("code", 1);
        }
        else
        {
            $this->set("message", "The request completed succesfully");
            $this->set("response_body", $request_body);
            $this->set("result", "success");
            $this->set("status", 200);
            $this->set("code", 0);
        }
    }

    /**
     *
     * @method GET
     */

    public function getSpotWithAllBlocks($spotId)
    {
        $request_body = $this->Spot->getSpotWithAllBlocks($spotId);
        if(empty($request_body))
        {
            if(empty($request_body))
            {
                $this->set("message", "The request returned 0 values.");
            }
            else if(is_string($request_body))
            {
                $this->set("message", $request_body);
            }

            $this->set("response_body", []);
            $this->set("status", 500);
            $this->set("result", "error");
            $this->set("code", 1);
        }
        else
        {
            $this->set("message", "The request completed succesfully");
            $this->set("response_body", $request_body);
            $this->set("result", "success");
            $this->set("status", 200);
            $this->set("code", 0);
        }
    }

    /**
     *
     * @method POST
     */
    
    public function getAllSpotsByUser($object)
    {
        $request_body = $this->Spot->getAllSpotsByUser($object);
        if(empty($request_body))
        {
            $this->set("message", "The request returned 0 values");
            $this->set("response_body", []);
            $this->set("status", 500);
            $this->set("result", "error");
            $this->set("code", 1);
        }
        else
        {
            $this->set("message", "");
            $this->set("response_body", $request_body);
            $this->set("result", "success");
            $this->set("status", 200);
            $this->set("code", 1);
        }
    }
    
    /**
     *
     * @method DELETE
     */
    
    public function delete($id)
    {
        $request = $this->Spot->deleteById($id);
        $this->set("message", $request);
        $this->set("response_body", []);
        $this->set("status", 200);
        $this->set("result", "success");
        $this->set("code", 0);
        
    }

    /**
     *
     * @method POST
     */

    public function add($object)
    {
        $request = $this->Spot->add($object);
        if($request === true)
        {
            $request_body = $this->Spot->select($object);
            if($request_body == false)
            {
                $this->set("message", "There was an error in the database, could not retrieve data!");
                $this->set("response_body", []);
                $this->set("status", 500);
                $this->set("result", "error");
                $this->set("code", 2);
            }
            else
            {
                $this->set("message", "The request completed succesfully");
                $this->set("response_body", $request_body);
                $this->set("result", "success");
                $this->set("status", 200);
                $this->set("code", 0);
            }
        }
        else
        {
            $this->set("message", $request);
            $this->set("response_body", []);
            $this->set("status", 500);
            $this->set("result", "error");
            $this->set("code", 1);
        }
    }

    /**
     *
     * @method PUT
     */

    public function update($object)
    {
        $response = $this->Spot->update($object);
        if($response === true)
        {
            $this->set("message", "The request completed succesfully");
            $this->set("response_body", $object);
            $this->set("result", "success");
            $this->set("status", 200);
            $this->set("code", 0);
        }
        else if($response === false)
        {
            $this->set("message", $response);
            $this->set("response_body", []);
            $this->set("status", 500);
            $this->set("result", "error");
            $this->set("code", 2);
        }
        else if(is_string($response))
        {
            $this->set("message", $response);
            $this->set("response_body", []);
            $this->set("status", 500);
            $this->set("result", "error");
            $this->set("code", 1);
        }
    }
}
