<?php
use core\javascript\json\uJSONObject;

$json_root= new uJSONObject();
$json_response= new uJSONObject();

if(isset($response_body))
{
    $json_response->add("body", $response_body);
    $json_response->add("message", $message);
    $json_response->add("code", $code);
    $json_root->add("result", $result);
    $json_root->add("status", $status);
}

$json_root->add("response", $json_response);
echo $json_root->parseJSON();