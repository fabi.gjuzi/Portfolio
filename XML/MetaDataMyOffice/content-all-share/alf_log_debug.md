Apache Maven 3.0.5
Maven home: /usr/share/maven
Java version: 1.7.0_101, vendor: Oracle Corporation
Java home: /usr/lib/jvm/java-7-openjdk-amd64/jre
Default locale: en_US, platform encoding: UTF-8
OS name: "linux", version: "3.19.0-64-generic", arch: "amd64", family: "unix"
[INFO] Error stacktraces are turned on.
[DEBUG] Reading global settings from /usr/share/maven/conf/settings.xml
[DEBUG] Reading user settings from /root/.m2/settings.xml
[DEBUG] Using local repository at /root/.m2/repository
[DEBUG] Using manager EnhancedLocalRepositoryManager with priority 10 for /root/.m2/repository
[INFO] Scanning for projects...
[DEBUG] org.apache.maven.archetype:archetype-packaging:jar:2.2:
[DEBUG]    org.codehaus.plexus:plexus-utils:jar:1.1:runtime
[DEBUG] Created new class realm maven.api
[DEBUG] Importing foreign packages into class realm maven.api
[DEBUG]   Imported: org.apache.maven.wagon.events < plexus.core
[DEBUG]   Imported: org.sonatype.aether.transfer < plexus.core
[DEBUG]   Imported: org.apache.maven.exception < plexus.core
[DEBUG]   Imported: org.sonatype.aether.metadata < plexus.core
[DEBUG]   Imported: org.codehaus.plexus.util.xml.Xpp3Dom < plexus.core
[DEBUG]   Imported: org.sonatype.aether.collection < plexus.core
[DEBUG]   Imported: org.sonatype.aether.version < plexus.core
[DEBUG]   Imported: org.apache.maven.monitor < plexus.core
[DEBUG]   Imported: org.apache.maven.wagon.repository < plexus.core
[DEBUG]   Imported: org.apache.maven.repository < plexus.core
[DEBUG]   Imported: org.apache.maven.wagon.resource < plexus.core
[DEBUG]   Imported: org.codehaus.plexus.logging < plexus.core
[DEBUG]   Imported: org.apache.maven.profiles < plexus.core
[DEBUG]   Imported: org.sonatype.aether.repository < plexus.core
[DEBUG]   Imported: org.apache.maven.classrealm < plexus.core
[DEBUG]   Imported: org.apache.maven.execution < plexus.core
[DEBUG]   Imported: org.sonatype.aether.artifact < plexus.core
[DEBUG]   Imported: org.sonatype.aether.spi < plexus.core
[DEBUG]   Imported: org.apache.maven.reporting < plexus.core
[DEBUG]   Imported: org.apache.maven.usability < plexus.core
[DEBUG]   Imported: org.codehaus.plexus.container < plexus.core
[DEBUG]   Imported: org.codehaus.plexus.component < plexus.core
[DEBUG]   Imported: org.codehaus.plexus.util.xml.pull.XmlSerializer < plexus.core
[DEBUG]   Imported: org.apache.maven.wagon.authentication < plexus.core
[DEBUG]   Imported: org.apache.maven.lifecycle < plexus.core
[DEBUG]   Imported: org.codehaus.plexus.classworlds < plexus.core
[DEBUG]   Imported: org.sonatype.aether.graph < plexus.core
[DEBUG]   Imported: org.sonatype.aether.* < plexus.core
[DEBUG]   Imported: org.apache.maven.settings < plexus.core
[DEBUG]   Imported: org.codehaus.classworlds < plexus.core
[DEBUG]   Imported: org.sonatype.aether.impl < plexus.core
[DEBUG]   Imported: org.apache.maven.wagon.* < plexus.core
[DEBUG]   Imported: org.apache.maven.toolchain < plexus.core
[DEBUG]   Imported: org.sonatype.aether.deployment < plexus.core
[DEBUG]   Imported: org.apache.maven.wagon.observers < plexus.core
[DEBUG]   Imported: org.codehaus.plexus.util.xml.pull.XmlPullParserException < plexus.core
[DEBUG]   Imported: org.codehaus.plexus.util.xml.pull.XmlPullParser < plexus.core
[DEBUG]   Imported: org.apache.maven.configuration < plexus.core
[DEBUG]   Imported: org.apache.maven.cli < plexus.core
[DEBUG]   Imported: org.sonatype.aether.installation < plexus.core
[DEBUG]   Imported: org.codehaus.plexus.context < plexus.core
[DEBUG]   Imported: org.apache.maven.wagon.authorization < plexus.core
[DEBUG]   Imported: org.apache.maven.project < plexus.core
[DEBUG]   Imported: org.apache.maven.rtinfo < plexus.core
[DEBUG]   Imported: org.codehaus.plexus.lifecycle < plexus.core
[DEBUG]   Imported: org.codehaus.plexus.configuration < plexus.core
[DEBUG]   Imported: org.apache.maven.artifact < plexus.core
[DEBUG]   Imported: org.apache.maven.model < plexus.core
[DEBUG]   Imported: org.apache.maven.* < plexus.core
[DEBUG]   Imported: org.apache.maven.wagon.proxy < plexus.core
[DEBUG]   Imported: org.sonatype.aether.resolution < plexus.core
[DEBUG]   Imported: org.apache.maven.plugin < plexus.core
[DEBUG]   Imported: org.codehaus.plexus.* < plexus.core
[DEBUG]   Imported: org.codehaus.plexus.personality < plexus.core
[DEBUG] Populating class realm maven.api
[DEBUG] Created new class realm extension>org.apache.maven.archetype:archetype-packaging:2.2
[DEBUG] Importing foreign packages into class realm extension>org.apache.maven.archetype:archetype-packaging:2.2
[DEBUG]   Imported:  < maven.api
[DEBUG] Populating class realm extension>org.apache.maven.archetype:archetype-packaging:2.2
[DEBUG]   Included: org.apache.maven.archetype:archetype-packaging:jar:2.2
[DEBUG]   Included: org.codehaus.plexus:plexus-utils:jar:1.1
[DEBUG] org.apache.maven.wagon:wagon-webdav-jackrabbit:jar:2.2:
[DEBUG]    org.apache.maven.wagon:wagon-http-shared:jar:2.2:compile
[DEBUG]       nekohtml:xercesMinimal:jar:1.9.6.2:compile
[DEBUG]       nekohtml:nekohtml:jar:1.9.6.2:compile
[DEBUG]       commons-logging:commons-logging:jar:1.1.1:compile
[DEBUG]    org.apache.jackrabbit:jackrabbit-webdav:jar:2.2.5:compile
[DEBUG]       org.apache.jackrabbit:jackrabbit-jcr-commons:jar:2.2.5:compile
[DEBUG]       org.slf4j:slf4j-api:jar:1.6.1:compile (version managed from 1.5.11)
[DEBUG]       org.slf4j:jcl-over-slf4j:jar:1.6.1:test (scope managed from compile) (version managed from 1.5.11)
[DEBUG]    commons-httpclient:commons-httpclient:jar:3.1:compile
[DEBUG]       commons-codec:commons-codec:jar:1.2:compile
[DEBUG]    org.apache.maven.wagon:wagon-provider-api:jar:2.2:compile
[DEBUG]       org.codehaus.plexus:plexus-utils:jar:3.0:compile
[DEBUG] Created new class realm extension>org.apache.maven.wagon:wagon-webdav-jackrabbit:2.2
[DEBUG] Importing foreign packages into class realm extension>org.apache.maven.wagon:wagon-webdav-jackrabbit:2.2
[DEBUG]   Imported:  < maven.api
[DEBUG] Populating class realm extension>org.apache.maven.wagon:wagon-webdav-jackrabbit:2.2
[DEBUG]   Included: org.apache.maven.wagon:wagon-webdav-jackrabbit:jar:2.2
[DEBUG]   Included: org.apache.maven.wagon:wagon-http-shared:jar:2.2
[DEBUG]   Included: nekohtml:xercesMinimal:jar:1.9.6.2
[DEBUG]   Included: nekohtml:nekohtml:jar:1.9.6.2
[DEBUG]   Included: commons-logging:commons-logging:jar:1.1.1
[DEBUG]   Included: org.apache.jackrabbit:jackrabbit-webdav:jar:2.2.5
[DEBUG]   Included: org.apache.jackrabbit:jackrabbit-jcr-commons:jar:2.2.5
[DEBUG]   Included: org.slf4j:slf4j-api:jar:1.6.1
[DEBUG]   Included: commons-httpclient:commons-httpclient:jar:3.1
[DEBUG]   Included: commons-codec:commons-codec:jar:1.2
[DEBUG]   Included: org.codehaus.plexus:plexus-utils:jar:3.0
[DEBUG] org.alfresco.maven.plugin:alfresco-maven-plugin:jar:2.0.0:
[DEBUG]    org.apache.maven:maven-plugin-api:jar:3.0.4:compile
[DEBUG]       org.apache.maven:maven-model:jar:3.0.4:compile
[DEBUG]       org.sonatype.sisu:sisu-inject-plexus:jar:2.3.0:compile
[DEBUG]          org.sonatype.sisu:sisu-inject-bean:jar:2.3.0:compile
[DEBUG]             org.sonatype.sisu:sisu-guice:jar:no_aop:3.1.0:compile
[DEBUG]                org.sonatype.sisu:sisu-guava:jar:0.9.9:compile
[DEBUG]    org.apache.maven:maven-archiver:jar:2.5:compile
[DEBUG]       org.codehaus.plexus:plexus-utils:jar:3.0:compile
[DEBUG]       org.codehaus.plexus:plexus-interpolation:jar:1.15:compile
[DEBUG]    org.codehaus.plexus:plexus-archiver:jar:2.3:compile
[DEBUG]       org.codehaus.plexus:plexus-container-default:jar:1.0-alpha-9-stable-1:compile
[DEBUG]          junit:junit:jar:3.8.1:compile
[DEBUG]          classworlds:classworlds:jar:1.1-alpha-2:compile
[DEBUG]       org.codehaus.plexus:plexus-io:jar:2.0.6:compile
[DEBUG]    org.apache.maven:maven-artifact:jar:3.0.4:compile
[DEBUG]    org.apache.maven:maven-core:jar:3.0.4:compile
[DEBUG]       org.apache.maven:maven-settings:jar:3.0.4:compile
[DEBUG]       org.apache.maven:maven-settings-builder:jar:3.0.4:compile
[DEBUG]       org.apache.maven:maven-repository-metadata:jar:3.0.4:compile
[DEBUG]       org.apache.maven:maven-model-builder:jar:3.0.4:compile
[DEBUG]       org.apache.maven:maven-aether-provider:jar:3.0.4:compile
[DEBUG]          org.sonatype.aether:aether-spi:jar:1.13.1:compile
[DEBUG]       org.sonatype.aether:aether-impl:jar:1.13.1:compile
[DEBUG]       org.sonatype.aether:aether-api:jar:1.13.1:compile
[DEBUG]       org.sonatype.aether:aether-util:jar:1.13.1:compile
[DEBUG]       org.codehaus.plexus:plexus-classworlds:jar:2.4:compile
[DEBUG]       org.codehaus.plexus:plexus-component-annotations:jar:1.5.5:compile
[DEBUG]       org.sonatype.plexus:plexus-sec-dispatcher:jar:1.3:compile
[DEBUG]          org.sonatype.plexus:plexus-cipher:jar:1.4:compile
[DEBUG]    commons-io:commons-io:jar:2.1:compile
[DEBUG]    org.alfresco:alfresco-mmt:jar:5.0.c:compile
[DEBUG]    org.alfrescolabs.alfresco-technical-validation:org.alfrescolabs.alfresco-technical-validation:jar:0.4.0:compile
[DEBUG]       org.clojure:clojure:jar:1.6.0:compile
[DEBUG]       org.clojure:tools.cli:jar:0.3.1:compile
[DEBUG]       org.clojure:tools.logging:jar:0.3.0:compile
[DEBUG]       clojurewerkz:neocons:jar:3.0.0:compile
[DEBUG]          cheshire:cheshire:jar:5.3.1:compile
[DEBUG]             com.fasterxml.jackson.core:jackson-core:jar:2.3.1:compile
[DEBUG]             com.fasterxml.jackson.dataformat:jackson-dataformat-smile:jar:2.3.1:compile
[DEBUG]             tigris:tigris:jar:0.1.1:compile
[DEBUG]          clj-http:clj-http:jar:0.9.1:compile
[DEBUG]             org.apache.httpcomponents:httpcore:jar:4.3.2:compile
[DEBUG]             org.apache.httpcomponents:httpclient:jar:4.3.2:compile
[DEBUG]             org.apache.httpcomponents:httpmime:jar:4.3.2:compile
[DEBUG]             commons-codec:commons-codec:jar:1.9:compile
[DEBUG]             slingshot:slingshot:jar:0.10.3:compile
[DEBUG]             crouton:crouton:jar:0.1.1:compile
[DEBUG]                org.jsoup:jsoup:jar:1.7.1:compile
[DEBUG]             org.clojure:tools.reader:jar:0.8.3:compile
[DEBUG]             potemkin:potemkin:jar:0.3.4:compile
[DEBUG]                clj-tuple:clj-tuple:jar:0.1.2:compile
[DEBUG]                riddley:riddley:jar:0.1.6:compile
[DEBUG]          clojurewerkz:support:jar:0.20.0:compile
[DEBUG]             com.google.guava:guava:jar:14.0.1:compile
[DEBUG]          clojurewerkz:urly:jar:2.0.0-alpha5:compile
[DEBUG]       ch.qos.logback:logback-classic:jar:1.1.2:compile
[DEBUG]          ch.qos.logback:logback-core:jar:1.1.2:compile
[DEBUG]          org.slf4j:slf4j-api:jar:1.7.6:compile
[DEBUG]       me.raynes:conch:jar:0.7.0:compile
[DEBUG]          org.flatland:useful:jar:0.10.6:compile
[DEBUG]             org.clojure:tools.macro:jar:0.1.1:compile
[DEBUG]       jansi-clj:jansi-clj:jar:0.1.0:compile
[DEBUG]          org.fusesource.jansi:jansi:jar:1.11:compile
[DEBUG]       io.aviso:pretty:jar:0.1.12:compile
[DEBUG]       org.clojars.pmonks:depends:jar:0.3.0:compile
[DEBUG]          org.clojure:data.json:jar:0.2.4:compile
[DEBUG]          org.ow2.asm:asm:jar:5.0.3:compile
[DEBUG]          net.java.truevfs:truevfs-kernel-impl:jar:0.10.6:compile
[DEBUG]             net.java.truevfs:truevfs-kernel-spec:jar:0.10.6:compile
[DEBUG]                net.java.truecommons:truecommons-cio:jar:2.3.4:compile
[DEBUG]                   net.java.truecommons:truecommons-io:jar:2.3.4:compile
[DEBUG]                net.java.truecommons:truecommons-services:jar:2.3.4:compile
[DEBUG]                   net.java.truecommons:truecommons-logging:jar:2.3.4:compile
[DEBUG]                   javax.inject:javax.inject:jar:1:compile
[DEBUG]             org.scala-lang:scala-library:jar:2.10.3:compile
[DEBUG]          net.java.truevfs:truevfs-access:jar:0.10.6:compile
[DEBUG]          net.java.truevfs:truevfs-driver-file:jar:0.10.6:compile
[DEBUG]          net.java.truevfs:truevfs-driver-zip:jar:0.10.6:compile
[DEBUG]             net.java.truevfs:truevfs-comp-ibm437:jar:0.10.6:runtime
[DEBUG]             net.java.truevfs:truevfs-comp-zipdriver:jar:0.10.6:compile
[DEBUG]                net.java.truevfs:truevfs-comp-zip:jar:0.10.6:compile
[DEBUG]                org.apache.commons:commons-compress:jar:1.7:compile
[DEBUG]                org.bouncycastle:bcprov-jdk16:jar:1.46:compile
[DEBUG]          net.java.truevfs:truevfs-driver-jar:jar:0.10.6:compile
[DEBUG]          net.java.truecommons:truecommons-key-disable:jar:2.3.4:compile
[DEBUG]             net.java.truecommons:truecommons-key-spec:jar:2.3.4:compile
[DEBUG]                net.java.truecommons:truecommons-shed:jar:2.3.4:compile
[DEBUG]       org.clojars.pmonks:multigrep:jar:0.2.0:compile
[DEBUG]       org.clojars.pmonks:bookmark-writer:jar:0.1.0:compile
[DEBUG]          org.docx4j:docx4j:jar:3.0.1:compile
[DEBUG]             org.plutext:jaxb-svg11:jar:1.0.2:compile
[DEBUG]             org.plutext:jaxb-xslfo:jar:1.0.1:compile
[DEBUG]             org.plutext:jaxb-xmldsig-core:jar:1.0.0:compile
[DEBUG]             commons-lang:commons-lang:jar:2.4:compile
[DEBUG]             org.apache.xmlgraphics:xmlgraphics-commons:jar:1.5:compile
[DEBUG]             commons-logging:commons-logging:jar:1.1.1:compile
[DEBUG]             org.apache.xmlgraphics:fop:jar:1.1:compile
[DEBUG]                org.apache.xmlgraphics:batik-svg-dom:jar:1.7:compile
[DEBUG]                   org.apache.xmlgraphics:batik-anim:jar:1.7:compile
[DEBUG]                   org.apache.xmlgraphics:batik-css:jar:1.7:compile
[DEBUG]                   org.apache.xmlgraphics:batik-dom:jar:1.7:compile
[DEBUG]                   org.apache.xmlgraphics:batik-parser:jar:1.7:compile
[DEBUG]                   org.apache.xmlgraphics:batik-util:jar:1.7:compile
[DEBUG]                org.apache.xmlgraphics:batik-bridge:jar:1.7:compile
[DEBUG]                   org.apache.xmlgraphics:batik-script:jar:1.7:compile
[DEBUG]                      org.apache.xmlgraphics:batik-js:jar:1.7:compile
[DEBUG]                   org.apache.xmlgraphics:batik-xml:jar:1.7:compile
[DEBUG]                org.apache.xmlgraphics:batik-awt-util:jar:1.7:compile
[DEBUG]                org.apache.xmlgraphics:batik-gvt:jar:1.7:compile
[DEBUG]                org.apache.xmlgraphics:batik-transcoder:jar:1.7:compile
[DEBUG]                   org.apache.xmlgraphics:batik-svggen:jar:1.7:compile
[DEBUG]                org.apache.xmlgraphics:batik-extension:jar:1.7:compile
[DEBUG]                org.apache.xmlgraphics:batik-ext:jar:1.7:compile
[DEBUG]             org.apache.avalon.framework:avalon-framework-api:jar:4.3.1:compile
[DEBUG]             org.apache.avalon.framework:avalon-framework-impl:jar:4.3.1:compile
[DEBUG]             xalan:xalan:jar:2.7.1:compile
[DEBUG]                xalan:serializer:jar:2.7.1:compile
[DEBUG]             net.arnx:wmf2svg:jar:0.9.0:compile
[DEBUG]             org.apache.poi:poi-scratchpad:jar:3.8:compile
[DEBUG]                org.apache.poi:poi:jar:3.8:compile
[DEBUG]             org.antlr:antlr-runtime:jar:3.3:compile
[DEBUG]             org.antlr:stringtemplate:jar:3.2.1:compile
[DEBUG]                antlr:antlr:jar:2.7.7:compile
[DEBUG]       org.clojars.pmonks:spinner:jar:0.2.0:compile
[DEBUG] Created new class realm extension>org.alfresco.maven.plugin:alfresco-maven-plugin:2.0.0
[DEBUG] Importing foreign packages into class realm extension>org.alfresco.maven.plugin:alfresco-maven-plugin:2.0.0
[DEBUG]   Imported:  < maven.api
[DEBUG] Populating class realm extension>org.alfresco.maven.plugin:alfresco-maven-plugin:2.0.0
[DEBUG]   Included: org.alfresco.maven.plugin:alfresco-maven-plugin:jar:2.0.0
[DEBUG]   Included: org.sonatype.sisu:sisu-inject-bean:jar:2.3.0
[DEBUG]   Included: org.sonatype.sisu:sisu-guice:jar:no_aop:3.1.0
[DEBUG]   Included: org.sonatype.sisu:sisu-guava:jar:0.9.9
[DEBUG]   Included: org.apache.maven:maven-archiver:jar:2.5
[DEBUG]   Included: org.codehaus.plexus:plexus-utils:jar:3.0
[DEBUG]   Included: org.codehaus.plexus:plexus-interpolation:jar:1.15
[DEBUG]   Included: org.codehaus.plexus:plexus-archiver:jar:2.3
[DEBUG]   Included: junit:junit:jar:3.8.1
[DEBUG]   Included: org.codehaus.plexus:plexus-io:jar:2.0.6
[DEBUG]   Included: org.sonatype.aether:aether-util:jar:1.13.1
[DEBUG]   Included: org.codehaus.plexus:plexus-component-annotations:jar:1.5.5
[DEBUG]   Included: org.sonatype.plexus:plexus-sec-dispatcher:jar:1.3
[DEBUG]   Included: org.sonatype.plexus:plexus-cipher:jar:1.4
[DEBUG]   Included: commons-io:commons-io:jar:2.1
[DEBUG]   Included: org.alfresco:alfresco-mmt:jar:5.0.c
[DEBUG]   Included: org.alfrescolabs.alfresco-technical-validation:org.alfrescolabs.alfresco-technical-validation:jar:0.4.0
[DEBUG]   Included: org.clojure:clojure:jar:1.6.0
[DEBUG]   Included: org.clojure:tools.cli:jar:0.3.1
[DEBUG]   Included: org.clojure:tools.logging:jar:0.3.0
[DEBUG]   Included: clojurewerkz:neocons:jar:3.0.0
[DEBUG]   Included: cheshire:cheshire:jar:5.3.1
[DEBUG]   Included: com.fasterxml.jackson.core:jackson-core:jar:2.3.1
[DEBUG]   Included: com.fasterxml.jackson.dataformat:jackson-dataformat-smile:jar:2.3.1
[DEBUG]   Included: tigris:tigris:jar:0.1.1
[DEBUG]   Included: clj-http:clj-http:jar:0.9.1
[DEBUG]   Included: org.apache.httpcomponents:httpcore:jar:4.3.2
[DEBUG]   Included: org.apache.httpcomponents:httpclient:jar:4.3.2
[DEBUG]   Included: org.apache.httpcomponents:httpmime:jar:4.3.2
[DEBUG]   Included: commons-codec:commons-codec:jar:1.9
[DEBUG]   Included: slingshot:slingshot:jar:0.10.3
[DEBUG]   Included: crouton:crouton:jar:0.1.1
[DEBUG]   Included: org.jsoup:jsoup:jar:1.7.1
[DEBUG]   Included: org.clojure:tools.reader:jar:0.8.3
[DEBUG]   Included: potemkin:potemkin:jar:0.3.4
[DEBUG]   Included: clj-tuple:clj-tuple:jar:0.1.2
[DEBUG]   Included: riddley:riddley:jar:0.1.6
[DEBUG]   Included: clojurewerkz:support:jar:0.20.0
[DEBUG]   Included: com.google.guava:guava:jar:14.0.1
[DEBUG]   Included: clojurewerkz:urly:jar:2.0.0-alpha5
[DEBUG]   Included: ch.qos.logback:logback-classic:jar:1.1.2
[DEBUG]   Included: ch.qos.logback:logback-core:jar:1.1.2
[DEBUG]   Included: org.slf4j:slf4j-api:jar:1.7.6
[DEBUG]   Included: me.raynes:conch:jar:0.7.0
[DEBUG]   Included: org.flatland:useful:jar:0.10.6
[DEBUG]   Included: org.clojure:tools.macro:jar:0.1.1
[DEBUG]   Included: jansi-clj:jansi-clj:jar:0.1.0
[DEBUG]   Included: org.fusesource.jansi:jansi:jar:1.11
[DEBUG]   Included: io.aviso:pretty:jar:0.1.12
[DEBUG]   Included: org.clojars.pmonks:depends:jar:0.3.0
[DEBUG]   Included: org.clojure:data.json:jar:0.2.4
[DEBUG]   Included: org.ow2.asm:asm:jar:5.0.3
[DEBUG]   Included: net.java.truevfs:truevfs-kernel-impl:jar:0.10.6
[DEBUG]   Included: net.java.truevfs:truevfs-kernel-spec:jar:0.10.6
[DEBUG]   Included: net.java.truecommons:truecommons-cio:jar:2.3.4
[DEBUG]   Included: net.java.truecommons:truecommons-io:jar:2.3.4
[DEBUG]   Included: net.java.truecommons:truecommons-services:jar:2.3.4
[DEBUG]   Included: net.java.truecommons:truecommons-logging:jar:2.3.4
[DEBUG]   Included: javax.inject:javax.inject:jar:1
[DEBUG]   Included: org.scala-lang:scala-library:jar:2.10.3
[DEBUG]   Included: net.java.truevfs:truevfs-access:jar:0.10.6
[DEBUG]   Included: net.java.truevfs:truevfs-driver-file:jar:0.10.6
[DEBUG]   Included: net.java.truevfs:truevfs-driver-zip:jar:0.10.6
[DEBUG]   Included: net.java.truevfs:truevfs-comp-ibm437:jar:0.10.6
[DEBUG]   Included: net.java.truevfs:truevfs-comp-zipdriver:jar:0.10.6
[DEBUG]   Included: net.java.truevfs:truevfs-comp-zip:jar:0.10.6
[DEBUG]   Included: org.apache.commons:commons-compress:jar:1.7
[DEBUG]   Included: org.bouncycastle:bcprov-jdk16:jar:1.46
[DEBUG]   Included: net.java.truevfs:truevfs-driver-jar:jar:0.10.6
[DEBUG]   Included: net.java.truecommons:truecommons-key-disable:jar:2.3.4
[DEBUG]   Included: net.java.truecommons:truecommons-key-spec:jar:2.3.4
[DEBUG]   Included: net.java.truecommons:truecommons-shed:jar:2.3.4
[DEBUG]   Included: org.clojars.pmonks:multigrep:jar:0.2.0
[DEBUG]   Included: org.clojars.pmonks:bookmark-writer:jar:0.1.0
[DEBUG]   Included: org.docx4j:docx4j:jar:3.0.1
[DEBUG]   Included: org.plutext:jaxb-svg11:jar:1.0.2
[DEBUG]   Included: org.plutext:jaxb-xslfo:jar:1.0.1
[DEBUG]   Included: org.plutext:jaxb-xmldsig-core:jar:1.0.0
[DEBUG]   Included: commons-lang:commons-lang:jar:2.4
[DEBUG]   Included: org.apache.xmlgraphics:xmlgraphics-commons:jar:1.5
[DEBUG]   Included: commons-logging:commons-logging:jar:1.1.1
[DEBUG]   Included: org.apache.xmlgraphics:fop:jar:1.1
[DEBUG]   Included: org.apache.xmlgraphics:batik-svg-dom:jar:1.7
[DEBUG]   Included: org.apache.xmlgraphics:batik-anim:jar:1.7
[DEBUG]   Included: org.apache.xmlgraphics:batik-css:jar:1.7
[DEBUG]   Included: org.apache.xmlgraphics:batik-dom:jar:1.7
[DEBUG]   Included: org.apache.xmlgraphics:batik-parser:jar:1.7
[DEBUG]   Included: org.apache.xmlgraphics:batik-util:jar:1.7
[DEBUG]   Included: org.apache.xmlgraphics:batik-bridge:jar:1.7
[DEBUG]   Included: org.apache.xmlgraphics:batik-script:jar:1.7
[DEBUG]   Included: org.apache.xmlgraphics:batik-js:jar:1.7
[DEBUG]   Included: org.apache.xmlgraphics:batik-xml:jar:1.7
[DEBUG]   Included: org.apache.xmlgraphics:batik-awt-util:jar:1.7
[DEBUG]   Included: org.apache.xmlgraphics:batik-gvt:jar:1.7
[DEBUG]   Included: org.apache.xmlgraphics:batik-transcoder:jar:1.7
[DEBUG]   Included: org.apache.xmlgraphics:batik-svggen:jar:1.7
[DEBUG]   Included: org.apache.xmlgraphics:batik-extension:jar:1.7
[DEBUG]   Included: org.apache.xmlgraphics:batik-ext:jar:1.7
[DEBUG]   Included: org.apache.avalon.framework:avalon-framework-api:jar:4.3.1
[DEBUG]   Included: org.apache.avalon.framework:avalon-framework-impl:jar:4.3.1
[DEBUG]   Included: xalan:xalan:jar:2.7.1
[DEBUG]   Included: xalan:serializer:jar:2.7.1
[DEBUG]   Included: net.arnx:wmf2svg:jar:0.9.0
[DEBUG]   Included: org.apache.poi:poi-scratchpad:jar:3.8
[DEBUG]   Included: org.apache.poi:poi:jar:3.8
[DEBUG]   Included: org.antlr:antlr-runtime:jar:3.3
[DEBUG]   Included: org.antlr:stringtemplate:jar:3.2.1
[DEBUG]   Included: antlr:antlr:jar:2.7.7
[DEBUG]   Included: org.clojars.pmonks:spinner:jar:0.2.0
[DEBUG] Extension realms for project com.netprojex:content-all-share:amp:1.0-SNAPSHOT: [ClassRealm[extension>org.apache.maven.archetype:archetype-packaging:2.2, parent: sun.misc.Launcher$AppClassLoader@71b456f], ClassRealm[extension>org.apache.maven.wagon:wagon-webdav-jackrabbit:2.2, parent: sun.misc.Launcher$AppClassLoader@71b456f], ClassRealm[extension>org.alfresco.maven.plugin:alfresco-maven-plugin:2.0.0, parent: sun.misc.Launcher$AppClassLoader@71b456f]]
[DEBUG] Created new class realm project>com.netprojex:content-all-share:1.0-SNAPSHOT
[DEBUG] Populating class realm project>com.netprojex:content-all-share:1.0-SNAPSHOT
[DEBUG]   Included: org.apache.maven.archetype:archetype-packaging:jar:2.2
[DEBUG] Looking up lifecyle mappings for packaging amp from ClassRealm[project>com.netprojex:content-all-share:1.0-SNAPSHOT, parent: ClassRealm[maven.api, parent: null]]
[DEBUG] === REACTOR BUILD PLAN ================================================
[DEBUG] Project: com.netprojex:content-all-share:amp:1.0-SNAPSHOT
[DEBUG] Tasks:   [integration-test]
[DEBUG] Style:   Regular
[DEBUG] =======================================================================
[INFO]                                                                         
[INFO] ------------------------------------------------------------------------
[INFO] Building content-all-share AMP project 1.0-SNAPSHOT
[INFO] ------------------------------------------------------------------------
[DEBUG] Resolving plugin version for org.apache.maven.plugins:maven-deploy-plugin
[DEBUG] Could not find metadata org.apache.maven.plugins:maven-deploy-plugin/maven-metadata.xml in local (/root/.m2/repository)
[DEBUG] Failure to find org.apache.maven.plugins:maven-deploy-plugin/maven-metadata.xml in https://oss.sonatype.org/content/repositories/snapshots was cached in the local repository, resolution will not be reattempted until the update interval of sonatype-snapshots has elapsed or updates are forced
[DEBUG] Failure to find org.apache.maven.plugins:maven-deploy-plugin/maven-metadata.xml in https://artifacts.alfresco.com/nexus/content/groups/public was cached in the local repository, resolution will not be reattempted until the update interval of alfresco-plugin-public has elapsed or updates are forced
[DEBUG] Failure to find org.apache.maven.plugins:maven-deploy-plugin/maven-metadata.xml in https://artifacts.alfresco.com/nexus/content/groups/public-snapshots was cached in the local repository, resolution will not be reattempted until the update interval of alfresco-plugin-public-snapshots has elapsed or updates are forced
[DEBUG] Skipped remote update check for org.apache.maven.plugins:maven-deploy-plugin/maven-metadata.xml, locally cached metadata up-to-date.
[DEBUG] Resolved plugin version for org.apache.maven.plugins:maven-deploy-plugin to 2.8.2 from repository central (http://repo.maven.apache.org/maven2, releases)
[DEBUG] Lifecycle default -> [validate, initialize, generate-sources, process-sources, generate-resources, process-resources, compile, process-classes, generate-test-sources, process-test-sources, generate-test-resources, process-test-resources, test-compile, process-test-classes, test, prepare-package, package, pre-integration-test, integration-test, post-integration-test, verify, install, deploy]
[DEBUG] Lifecycle clean -> [pre-clean, clean, post-clean]
[DEBUG] Lifecycle site -> [pre-site, site, post-site, site-deploy]
[DEBUG] Lifecycle default -> [validate, initialize, generate-sources, process-sources, generate-resources, process-resources, compile, process-classes, generate-test-sources, process-test-sources, generate-test-resources, process-test-resources, test-compile, process-test-classes, test, prepare-package, package, pre-integration-test, integration-test, post-integration-test, verify, install, deploy]
[DEBUG] Lifecycle clean -> [pre-clean, clean, post-clean]
[DEBUG] Lifecycle site -> [pre-site, site, post-site, site-deploy]
[DEBUG] Lifecycle default -> [validate, initialize, generate-sources, process-sources, generate-resources, process-resources, compile, process-classes, generate-test-sources, process-test-sources, generate-test-resources, process-test-resources, test-compile, process-test-classes, test, prepare-package, package, pre-integration-test, integration-test, post-integration-test, verify, install, deploy]
[DEBUG] Lifecycle clean -> [pre-clean, clean, post-clean]
[DEBUG] Lifecycle site -> [pre-site, site, post-site, site-deploy]
[DEBUG] Lifecycle default -> [validate, initialize, generate-sources, process-sources, generate-resources, process-resources, compile, process-classes, generate-test-sources, process-test-sources, generate-test-resources, process-test-resources, test-compile, process-test-classes, test, prepare-package, package, pre-integration-test, integration-test, post-integration-test, verify, install, deploy]
[DEBUG] Lifecycle clean -> [pre-clean, clean, post-clean]
[DEBUG] Lifecycle site -> [pre-site, site, post-site, site-deploy]
[DEBUG] Lifecycle default -> [validate, initialize, generate-sources, process-sources, generate-resources, process-resources, compile, process-classes, generate-test-sources, process-test-sources, generate-test-resources, process-test-resources, test-compile, process-test-classes, test, prepare-package, package, pre-integration-test, integration-test, post-integration-test, verify, install, deploy]
[DEBUG] Lifecycle clean -> [pre-clean, clean, post-clean]
[DEBUG] Lifecycle site -> [pre-site, site, post-site, site-deploy]
[DEBUG] Lifecycle default -> [validate, initialize, generate-sources, process-sources, generate-resources, process-resources, compile, process-classes, generate-test-sources, process-test-sources, generate-test-resources, process-test-resources, test-compile, process-test-classes, test, prepare-package, package, pre-integration-test, integration-test, post-integration-test, verify, install, deploy]
[DEBUG] Lifecycle clean -> [pre-clean, clean, post-clean]
[DEBUG] Lifecycle site -> [pre-site, site, post-site, site-deploy]
[DEBUG] Lifecycle default -> [validate, initialize, generate-sources, process-sources, generate-resources, process-resources, compile, process-classes, generate-test-sources, process-test-sources, generate-test-resources, process-test-resources, test-compile, process-test-classes, test, prepare-package, package, pre-integration-test, integration-test, post-integration-test, verify, install, deploy]
[DEBUG] Lifecycle clean -> [pre-clean, clean, post-clean]
[DEBUG] Lifecycle site -> [pre-site, site, post-site, site-deploy]
[DEBUG] Lifecycle default -> [validate, initialize, generate-sources, process-sources, generate-resources, process-resources, compile, process-classes, generate-test-sources, process-test-sources, generate-test-resources, process-test-resources, test-compile, process-test-classes, test, prepare-package, package, pre-integration-test, integration-test, post-integration-test, verify, install, deploy]
[DEBUG] Lifecycle clean -> [pre-clean, clean, post-clean]
[DEBUG] Lifecycle site -> [pre-site, site, post-site, site-deploy]
[DEBUG] Lifecycle default -> [validate, initialize, generate-sources, process-sources, generate-resources, process-resources, compile, process-classes, generate-test-sources, process-test-sources, generate-test-resources, process-test-resources, test-compile, process-test-classes, test, prepare-package, package, pre-integration-test, integration-test, post-integration-test, verify, install, deploy]
[DEBUG] Lifecycle clean -> [pre-clean, clean, post-clean]
[DEBUG] Lifecycle site -> [pre-site, site, post-site, site-deploy]
[DEBUG] Lifecycle default -> [validate, initialize, generate-sources, process-sources, generate-resources, process-resources, compile, process-classes, generate-test-sources, process-test-sources, generate-test-resources, process-test-resources, test-compile, process-test-classes, test, prepare-package, package, pre-integration-test, integration-test, post-integration-test, verify, install, deploy]
[DEBUG] Lifecycle clean -> [pre-clean, clean, post-clean]
[DEBUG] Lifecycle site -> [pre-site, site, post-site, site-deploy]
[DEBUG] Lifecycle default -> [validate, initialize, generate-sources, process-sources, generate-resources, process-resources, compile, process-classes, generate-test-sources, process-test-sources, generate-test-resources, process-test-resources, test-compile, process-test-classes, test, prepare-package, package, pre-integration-test, integration-test, post-integration-test, verify, install, deploy]
[DEBUG] Lifecycle clean -> [pre-clean, clean, post-clean]
[DEBUG] Lifecycle site -> [pre-site, site, post-site, site-deploy]
[DEBUG] === PROJECT BUILD PLAN ================================================
[DEBUG] Project:       com.netprojex:content-all-share:1.0-SNAPSHOT
[DEBUG] Dependencies (collect): []
[DEBUG] Dependencies (resolve): [compile, runtime, test]
[DEBUG] Repositories (dependencies): [sonatype-snapshots (https://oss.sonatype.org/content/repositories/snapshots/, snapshots), alfresco-public (https://artifacts.alfresco.com/nexus/content/groups/public, releases+snapshots), alfresco-public-snapshots (https://artifacts.alfresco.com/nexus/content/groups/public-snapshots, releases+snapshots), central (http://repo.maven.apache.org/maven2, releases)]
[DEBUG] Repositories (plugins)     : [sonatype-snapshots (https://oss.sonatype.org/content/repositories/snapshots, releases+snapshots), alfresco-plugin-public (https://artifacts.alfresco.com/nexus/content/groups/public, releases+snapshots), alfresco-plugin-public-snapshots (https://artifacts.alfresco.com/nexus/content/groups/public-snapshots, releases+snapshots), central (http://repo.maven.apache.org/maven2, releases)]
[DEBUG] -----------------------------------------------------------------------
[DEBUG] Goal:          org.alfresco.maven.plugin:alfresco-maven-plugin:2.0.0:set-version (default-set-version)
[DEBUG] Style:         Regular
[DEBUG] Configuration: <?xml version="1.0" encoding="UTF-8"?>
<configuration>
  <customVersionSuffix>${maven.alfresco.customVersionSuffix}</customVersionSuffix>
  <project default-value="${project}"/>
  <propertyName default-value="noSnapshotVersion">${maven.alfresco.propertyName}</propertyName>
  <snapshotSuffix default-value="-SNAPSHOT">${maven.alfresco.snapshotSuffix}</snapshotSuffix>
  <snapshotToTimestamp default-value="false">true</snapshotToTimestamp>
  <version default-value="${project.version}"/>
</configuration>
[DEBUG] -----------------------------------------------------------------------
[DEBUG] Goal:          org.apache.maven.plugins:maven-resources-plugin:2.7:resources (default-resources)
[DEBUG] Style:         Regular
[DEBUG] Configuration: <?xml version="1.0" encoding="UTF-8"?>
<configuration>
  <buildFilters default-value="${project.build.filters}"/>
  <encoding default-value="${project.build.sourceEncoding}">UTF-8</encoding>
  <escapeString>${maven.resources.escapeString}</escapeString>
  <escapeWindowsPaths default-value="true">${maven.resources.escapeWindowsPaths}</escapeWindowsPaths>
  <includeEmptyDirs default-value="false">${maven.resources.includeEmptyDirs}</includeEmptyDirs>
  <nonFilteredFileExtensions>
    <nonFilteredFileExtension>ftl</nonFilteredFileExtension>
    <nonFilteredFileExtension>acp</nonFilteredFileExtension>
    <nonFilteredFileExtension>jpg</nonFilteredFileExtension>
    <nonFilteredFileExtension>png</nonFilteredFileExtension>
    <nonFilteredFileExtension>gif</nonFilteredFileExtension>
    <nonFilteredFileExtension>svg</nonFilteredFileExtension>
    <nonFilteredFileExtension>pdf</nonFilteredFileExtension>
    <nonFilteredFileExtension>doc</nonFilteredFileExtension>
    <nonFilteredFileExtension>docx</nonFilteredFileExtension>
    <nonFilteredFileExtension>xls</nonFilteredFileExtension>
    <nonFilteredFileExtension>xlsx</nonFilteredFileExtension>
    <nonFilteredFileExtension>ppt</nonFilteredFileExtension>
    <nonFilteredFileExtension>pptx</nonFilteredFileExtension>
    <nonFilteredFileExtension>bin</nonFilteredFileExtension>
    <nonFilteredFileExtension>lic</nonFilteredFileExtension>
    <nonFilteredFileExtension>swf</nonFilteredFileExtension>
    <nonFilteredFileExtension>zip</nonFilteredFileExtension>
    <nonFilteredFileExtension>msg</nonFilteredFileExtension>
    <nonFilteredFileExtension>jar</nonFilteredFileExtension>
    <nonFilteredFileExtension>ttf</nonFilteredFileExtension>
    <nonFilteredFileExtension>eot</nonFilteredFileExtension>
    <nonFilteredFileExtension>woff</nonFilteredFileExtension>
    <nonFilteredFileExtension>woff2</nonFilteredFileExtension>
  </nonFilteredFileExtensions>
  <outputDirectory default-value="${project.build.outputDirectory}"/>
  <overwrite default-value="false">${maven.resources.overwrite}</overwrite>
  <project default-value="${project}"/>
  <resources default-value="${project.resources}"/>
  <session default-value="${session}"/>
  <supportMultiLineFiltering default-value="false">${maven.resources.supportMultiLineFiltering}</supportMultiLineFiltering>
  <useBuildFilters default-value="true"/>
  <useDefaultDelimiters default-value="true"/>
</configuration>
[DEBUG] -----------------------------------------------------------------------
[DEBUG] Goal:          org.apache.maven.plugins:maven-compiler-plugin:3.2:compile (default-compile)
[DEBUG] Style:         Regular
[DEBUG] Configuration: <?xml version="1.0" encoding="UTF-8"?>
<configuration>
  <basedir default-value="${basedir}"/>
  <buildDirectory default-value="${project.build.directory}"/>
  <classpathElements default-value="${project.compileClasspathElements}"/>
  <compileSourceRoots default-value="${project.compileSourceRoots}"/>
  <compilerId default-value="javac">${maven.compiler.compilerId}</compilerId>
  <compilerReuseStrategy default-value="${reuseCreated}">${maven.compiler.compilerReuseStrategy}</compilerReuseStrategy>
  <compilerVersion>${maven.compiler.compilerVersion}</compilerVersion>
  <debug default-value="true">${maven.compiler.debug}</debug>
  <debuglevel>${maven.compiler.debuglevel}</debuglevel>
  <encoding default-value="${project.build.sourceEncoding}">${encoding}</encoding>
  <executable>${maven.compiler.executable}</executable>
  <failOnError default-value="true">${maven.compiler.failOnError}</failOnError>
  <forceJavacCompilerUse default-value="false">${maven.compiler.forceJavacCompilerUse}</forceJavacCompilerUse>
  <fork default-value="false">${maven.compiler.fork}</fork>
  <generatedSourcesDirectory default-value="${project.build.directory}/generated-sources/annotations"/>
  <maxmem>${maven.compiler.maxmem}</maxmem>
  <meminitial>${maven.compiler.meminitial}</meminitial>
  <mojoExecution>${mojoExecution}</mojoExecution>
  <optimize default-value="false">${maven.compiler.optimize}</optimize>
  <outputDirectory default-value="${project.build.outputDirectory}"/>
  <project default-value="${project}"/>
  <projectArtifact default-value="${project.artifact}"/>
  <session default-value="${session}"/>
  <showDeprecation default-value="false">${maven.compiler.showDeprecation}</showDeprecation>
  <showWarnings default-value="false">${maven.compiler.showWarnings}</showWarnings>
  <skipMain>${maven.main.skip}</skipMain>
  <skipMultiThreadWarning default-value="false">${maven.compiler.skipMultiThreadWarning}</skipMultiThreadWarning>
  <source default-value="1.5">1.7</source>
  <staleMillis default-value="0">${lastModGranularityMs}</staleMillis>
  <target default-value="1.5">1.7</target>
  <useIncrementalCompilation default-value="true">${maven.compiler.useIncrementalCompilation}</useIncrementalCompilation>
  <verbose default-value="false">${maven.compiler.verbose}</verbose>
</configuration>
[DEBUG] -----------------------------------------------------------------------
[DEBUG] Goal:          org.apache.maven.plugins:maven-resources-plugin:2.7:testResources (default-testResources)
[DEBUG] Style:         Regular
[DEBUG] Configuration: <?xml version="1.0" encoding="UTF-8"?>
<configuration>
  <buildFilters default-value="${project.build.filters}"/>
  <encoding default-value="${project.build.sourceEncoding}">UTF-8</encoding>
  <escapeString>${maven.resources.escapeString}</escapeString>
  <escapeWindowsPaths default-value="true">${maven.resources.escapeWindowsPaths}</escapeWindowsPaths>
  <includeEmptyDirs default-value="false">${maven.resources.includeEmptyDirs}</includeEmptyDirs>
  <nonFilteredFileExtensions>
    <nonFilteredFileExtension>ftl</nonFilteredFileExtension>
    <nonFilteredFileExtension>acp</nonFilteredFileExtension>
    <nonFilteredFileExtension>jpg</nonFilteredFileExtension>
    <nonFilteredFileExtension>png</nonFilteredFileExtension>
    <nonFilteredFileExtension>gif</nonFilteredFileExtension>
    <nonFilteredFileExtension>svg</nonFilteredFileExtension>
    <nonFilteredFileExtension>pdf</nonFilteredFileExtension>
    <nonFilteredFileExtension>doc</nonFilteredFileExtension>
    <nonFilteredFileExtension>docx</nonFilteredFileExtension>
    <nonFilteredFileExtension>xls</nonFilteredFileExtension>
    <nonFilteredFileExtension>xlsx</nonFilteredFileExtension>
    <nonFilteredFileExtension>ppt</nonFilteredFileExtension>
    <nonFilteredFileExtension>pptx</nonFilteredFileExtension>
    <nonFilteredFileExtension>bin</nonFilteredFileExtension>
    <nonFilteredFileExtension>lic</nonFilteredFileExtension>
    <nonFilteredFileExtension>swf</nonFilteredFileExtension>
    <nonFilteredFileExtension>zip</nonFilteredFileExtension>
    <nonFilteredFileExtension>msg</nonFilteredFileExtension>
    <nonFilteredFileExtension>jar</nonFilteredFileExtension>
    <nonFilteredFileExtension>ttf</nonFilteredFileExtension>
    <nonFilteredFileExtension>eot</nonFilteredFileExtension>
    <nonFilteredFileExtension>woff</nonFilteredFileExtension>
    <nonFilteredFileExtension>woff2</nonFilteredFileExtension>
  </nonFilteredFileExtensions>
  <outputDirectory default-value="${project.build.testOutputDirectory}"/>
  <overwrite default-value="false">${maven.resources.overwrite}</overwrite>
  <project default-value="${project}"/>
  <resources default-value="${project.testResources}"/>
  <session default-value="${session}"/>
  <skip>${maven.test.skip}</skip>
  <supportMultiLineFiltering default-value="false">${maven.resources.supportMultiLineFiltering}</supportMultiLineFiltering>
  <useBuildFilters default-value="true"/>
  <useDefaultDelimiters default-value="true"/>
</configuration>
[DEBUG] -----------------------------------------------------------------------
[DEBUG] Goal:          org.apache.maven.plugins:maven-resources-plugin:2.7:copy-resources (add-module-properties-to-test-classpath)
[DEBUG] Style:         Regular
[DEBUG] Configuration: <?xml version="1.0" encoding="UTF-8"?>
<configuration>
  <buildFilters default-value="${project.build.filters}"/>
  <encoding default-value="${project.build.sourceEncoding}">UTF-8</encoding>
  <escapeString>${maven.resources.escapeString}</escapeString>
  <escapeWindowsPaths default-value="true">${maven.resources.escapeWindowsPaths}</escapeWindowsPaths>
  <includeEmptyDirs default-value="false">${maven.resources.includeEmptyDirs}</includeEmptyDirs>
  <nonFilteredFileExtensions>
    <nonFilteredFileExtension>ftl</nonFilteredFileExtension>
    <nonFilteredFileExtension>acp</nonFilteredFileExtension>
    <nonFilteredFileExtension>jpg</nonFilteredFileExtension>
    <nonFilteredFileExtension>png</nonFilteredFileExtension>
    <nonFilteredFileExtension>gif</nonFilteredFileExtension>
    <nonFilteredFileExtension>svg</nonFilteredFileExtension>
    <nonFilteredFileExtension>pdf</nonFilteredFileExtension>
    <nonFilteredFileExtension>doc</nonFilteredFileExtension>
    <nonFilteredFileExtension>docx</nonFilteredFileExtension>
    <nonFilteredFileExtension>xls</nonFilteredFileExtension>
    <nonFilteredFileExtension>xlsx</nonFilteredFileExtension>
    <nonFilteredFileExtension>ppt</nonFilteredFileExtension>
    <nonFilteredFileExtension>pptx</nonFilteredFileExtension>
    <nonFilteredFileExtension>bin</nonFilteredFileExtension>
    <nonFilteredFileExtension>lic</nonFilteredFileExtension>
    <nonFilteredFileExtension>swf</nonFilteredFileExtension>
    <nonFilteredFileExtension>zip</nonFilteredFileExtension>
    <nonFilteredFileExtension>msg</nonFilteredFileExtension>
    <nonFilteredFileExtension>jar</nonFilteredFileExtension>
    <nonFilteredFileExtension>ttf</nonFilteredFileExtension>
    <nonFilteredFileExtension>eot</nonFilteredFileExtension>
    <nonFilteredFileExtension>woff</nonFilteredFileExtension>
    <nonFilteredFileExtension>woff2</nonFilteredFileExtension>
  </nonFilteredFileExtensions>
  <outputDirectory>/home/programues/Documents/metadata_myoffice/content-all-share/target/test-classes</outputDirectory>
  <overwrite default-value="false">${maven.resources.overwrite}</overwrite>
  <project default-value="${project}"/>
  <resources>
    <resource>
      <directory>src/main/amp</directory>
      <includes>
        <include>module.properties</include>
      </includes>
      <filtering>true</filtering>
      <targetPath>alfresco/module/content-all-share</targetPath>
    </resource>
  </resources>
  <session default-value="${session}"/>
  <supportMultiLineFiltering default-value="false">${maven.resources.supportMultiLineFiltering}</supportMultiLineFiltering>
  <useBuildFilters default-value="true"/>
  <useDefaultDelimiters default-value="true"/>
</configuration>
[DEBUG] -----------------------------------------------------------------------
[DEBUG] Goal:          org.apache.maven.plugins:maven-resources-plugin:2.7:copy-resources (add-module-config-to-test-classpath)
[DEBUG] Style:         Regular
[DEBUG] Configuration: <?xml version="1.0" encoding="UTF-8"?>
<configuration>
  <buildFilters default-value="${project.build.filters}"/>
  <encoding default-value="${project.build.sourceEncoding}">UTF-8</encoding>
  <escapeString>${maven.resources.escapeString}</escapeString>
  <escapeWindowsPaths default-value="true">${maven.resources.escapeWindowsPaths}</escapeWindowsPaths>
  <includeEmptyDirs default-value="false">${maven.resources.includeEmptyDirs}</includeEmptyDirs>
  <nonFilteredFileExtensions>
    <nonFilteredFileExtension>ftl</nonFilteredFileExtension>
    <nonFilteredFileExtension>acp</nonFilteredFileExtension>
    <nonFilteredFileExtension>jpg</nonFilteredFileExtension>
    <nonFilteredFileExtension>png</nonFilteredFileExtension>
    <nonFilteredFileExtension>gif</nonFilteredFileExtension>
    <nonFilteredFileExtension>svg</nonFilteredFileExtension>
    <nonFilteredFileExtension>pdf</nonFilteredFileExtension>
    <nonFilteredFileExtension>doc</nonFilteredFileExtension>
    <nonFilteredFileExtension>docx</nonFilteredFileExtension>
    <nonFilteredFileExtension>xls</nonFilteredFileExtension>
    <nonFilteredFileExtension>xlsx</nonFilteredFileExtension>
    <nonFilteredFileExtension>ppt</nonFilteredFileExtension>
    <nonFilteredFileExtension>pptx</nonFilteredFileExtension>
    <nonFilteredFileExtension>bin</nonFilteredFileExtension>
    <nonFilteredFileExtension>lic</nonFilteredFileExtension>
    <nonFilteredFileExtension>swf</nonFilteredFileExtension>
    <nonFilteredFileExtension>zip</nonFilteredFileExtension>
    <nonFilteredFileExtension>msg</nonFilteredFileExtension>
    <nonFilteredFileExtension>jar</nonFilteredFileExtension>
    <nonFilteredFileExtension>ttf</nonFilteredFileExtension>
    <nonFilteredFileExtension>eot</nonFilteredFileExtension>
    <nonFilteredFileExtension>woff</nonFilteredFileExtension>
    <nonFilteredFileExtension>woff2</nonFilteredFileExtension>
  </nonFilteredFileExtensions>
  <outputDirectory>/home/programues/Documents/metadata_myoffice/content-all-share/target/test-classes</outputDirectory>
  <overwrite default-value="false">${maven.resources.overwrite}</overwrite>
  <project default-value="${project}"/>
  <resources>
    <resource>
      <directory>src/main/amp/config</directory>
      <includes>
        <include>**/*</include>
      </includes>
      <filtering>true</filtering>
    </resource>
  </resources>
  <session default-value="${session}"/>
  <supportMultiLineFiltering default-value="false">${maven.resources.supportMultiLineFiltering}</supportMultiLineFiltering>
  <useBuildFilters default-value="true"/>
  <useDefaultDelimiters default-value="true"/>
</configuration>
[DEBUG] -----------------------------------------------------------------------
[DEBUG] Goal:          org.apache.maven.plugins:maven-compiler-plugin:3.2:testCompile (default-testCompile)
[DEBUG] Style:         Regular
[DEBUG] Configuration: <?xml version="1.0" encoding="UTF-8"?>
<configuration>
  <basedir default-value="${basedir}"/>
  <buildDirectory default-value="${project.build.directory}"/>
  <classpathElements default-value="${project.testClasspathElements}"/>
  <compileSourceRoots default-value="${project.testCompileSourceRoots}"/>
  <compilerId default-value="javac">${maven.compiler.compilerId}</compilerId>
  <compilerReuseStrategy default-value="${reuseCreated}">${maven.compiler.compilerReuseStrategy}</compilerReuseStrategy>
  <compilerVersion>${maven.compiler.compilerVersion}</compilerVersion>
  <debug default-value="true">${maven.compiler.debug}</debug>
  <debuglevel>${maven.compiler.debuglevel}</debuglevel>
  <encoding default-value="${project.build.sourceEncoding}">${encoding}</encoding>
  <executable>${maven.compiler.executable}</executable>
  <failOnError default-value="true">${maven.compiler.failOnError}</failOnError>
  <forceJavacCompilerUse default-value="false">${maven.compiler.forceJavacCompilerUse}</forceJavacCompilerUse>
  <fork default-value="false">${maven.compiler.fork}</fork>
  <generatedTestSourcesDirectory default-value="${project.build.directory}/generated-test-sources/test-annotations"/>
  <maxmem>${maven.compiler.maxmem}</maxmem>
  <meminitial>${maven.compiler.meminitial}</meminitial>
  <mojoExecution>${mojoExecution}</mojoExecution>
  <optimize default-value="false">${maven.compiler.optimize}</optimize>
  <outputDirectory default-value="${project.build.testOutputDirectory}"/>
  <project default-value="${project}"/>
  <session default-value="${session}"/>
  <showDeprecation default-value="false">${maven.compiler.showDeprecation}</showDeprecation>
  <showWarnings default-value="false">${maven.compiler.showWarnings}</showWarnings>
  <skip>${maven.test.skip}</skip>
  <skipMultiThreadWarning default-value="false">${maven.compiler.skipMultiThreadWarning}</skipMultiThreadWarning>
  <source default-value="1.5">1.7</source>
  <staleMillis default-value="0">${lastModGranularityMs}</staleMillis>
  <target default-value="1.5">1.7</target>
  <testSource>${maven.compiler.testSource}</testSource>
  <testTarget>${maven.compiler.testTarget}</testTarget>
  <useIncrementalCompilation default-value="true">${maven.compiler.useIncrementalCompilation}</useIncrementalCompilation>
  <verbose default-value="false">${maven.compiler.verbose}</verbose>
</configuration>
[DEBUG] -----------------------------------------------------------------------
[DEBUG] Goal:          org.apache.maven.plugins:maven-surefire-plugin:2.18:test (default-test)
[DEBUG] Style:         Regular
[DEBUG] Configuration: <?xml version="1.0" encoding="UTF-8"?>
<configuration>
  <additionalClasspathElements>${maven.test.additionalClasspath}</additionalClasspathElements>
  <argLine>${argLine}</argLine>
  <basedir default-value="${basedir}"/>
  <childDelegation default-value="false">${childDelegation}</childDelegation>
  <classesDirectory default-value="${project.build.outputDirectory}"/>
  <classpathDependencyExcludes>${maven.test.dependency.excludes}</classpathDependencyExcludes>
  <debugForkedProcess>${maven.surefire.debug}</debugForkedProcess>
  <dependenciesToScan>${dependenciesToScan}</dependenciesToScan>
  <disableXmlReport default-value="false">${disableXmlReport}</disableXmlReport>
  <enableAssertions default-value="true">${enableAssertions}</enableAssertions>
  <excludedGroups>${excludedGroups}</excludedGroups>
  <failIfNoSpecifiedTests>${surefire.failIfNoSpecifiedTests}</failIfNoSpecifiedTests>
  <failIfNoTests>${failIfNoTests}</failIfNoTests>
  <forkCount default-value="1">${forkCount}</forkCount>
  <forkMode default-value="once">${forkMode}</forkMode>
  <forkedProcessTimeoutInSeconds>${surefire.timeout}</forkedProcessTimeoutInSeconds>
  <groups>${groups}</groups>
  <junitArtifactName default-value="junit:junit">${junitArtifactName}</junitArtifactName>
  <jvm>${jvm}</jvm>
  <localRepository default-value="${localRepository}"/>
  <objectFactory>${objectFactory}</objectFactory>
  <parallel>${parallel}</parallel>
  <parallelMavenExecution default-value="${session.parallel}"/>
  <parallelOptimized default-value="true">${parallelOptimized}</parallelOptimized>
  <parallelTestsTimeoutForcedInSeconds>${surefire.parallel.forcedTimeout}</parallelTestsTimeoutForcedInSeconds>
  <parallelTestsTimeoutInSeconds>${surefire.parallel.timeout}</parallelTestsTimeoutInSeconds>
  <perCoreThreadCount default-value="true">${perCoreThreadCount}</perCoreThreadCount>
  <pluginArtifactMap>${plugin.artifactMap}</pluginArtifactMap>
  <pluginDescriptor default-value="${plugin}"/>
  <printSummary default-value="true">${surefire.printSummary}</printSummary>
  <projectArtifactMap>${project.artifactMap}</projectArtifactMap>
  <redirectTestOutputToFile default-value="false">${maven.test.redirectTestOutputToFile}</redirectTestOutputToFile>
  <remoteRepositories default-value="${project.pluginArtifactRepositories}"/>
  <reportFormat default-value="brief">${surefire.reportFormat}</reportFormat>
  <reportNameSuffix default-value="">${surefire.reportNameSuffix}</reportNameSuffix>
  <reportsDirectory default-value="${project.build.directory}/surefire-reports"/>
  <rerunFailingTestsCount default-value="0">${surefire.rerunFailingTestsCount}</rerunFailingTestsCount>
  <reuseForks default-value="true">${reuseForks}</reuseForks>
  <runOrder default-value="filesystem"/>
  <skip default-value="false">${maven.test.skip}</skip>
  <skipExec>${maven.test.skip.exec}</skipExec>
  <skipTests default-value="false">${skipTests}</skipTests>
  <test>${test}</test>
  <testClassesDirectory default-value="${project.build.testOutputDirectory}"/>
  <testFailureIgnore default-value="false">${maven.test.failure.ignore}</testFailureIgnore>
  <testNGArtifactName default-value="org.testng:testng">${testNGArtifactName}</testNGArtifactName>
  <testSourceDirectory default-value="${project.build.testSourceDirectory}"/>
  <threadCount>${threadCount}</threadCount>
  <threadCountClasses default-value="0">${threadCountClasses}</threadCountClasses>
  <threadCountMethods default-value="0">${threadCountMethods}</threadCountMethods>
  <threadCountSuites default-value="0">${threadCountSuites}</threadCountSuites>
  <trimStackTrace default-value="true">${trimStackTrace}</trimStackTrace>
  <useFile default-value="true">${surefire.useFile}</useFile>
  <useManifestOnlyJar default-value="true">${surefire.useManifestOnlyJar}</useManifestOnlyJar>
  <useSystemClassLoader default-value="true">${surefire.useSystemClassLoader}</useSystemClassLoader>
  <useUnlimitedThreads default-value="false">${useUnlimitedThreads}</useUnlimitedThreads>
  <workingDirectory>${basedir}</workingDirectory>
  <project default-value="${project}"/>
  <session default-value="${session}"/>
</configuration>
[DEBUG] -----------------------------------------------------------------------
[DEBUG] Goal:          org.apache.maven.plugins:maven-dependency-plugin:2.9:unpack (unpack-alfresco)
[DEBUG] Style:         Regular
[DEBUG] Configuration: <?xml version="1.0" encoding="UTF-8"?>
<configuration>
  <artifact>${artifact}</artifact>
  <artifactItems>
    <artifactItem>
      <groupId>org.alfresco</groupId>
      <artifactId>share</artifactId>
      <type>war</type>
      <version>5.0.c</version>
    </artifactItem>
  </artifactItems>
  <excludes>${mdep.unpack.excludes}</excludes>
  <ignorePermissions default-value="false">${dependency.ignorePermissions}</ignorePermissions>
  <includes>${mdep.unpack.includes}</includes>
  <local default-value="${localRepository}"/>
  <markersDirectory default-value="${project.build.directory}/dependency-maven-plugin-markers"/>
  <outputAbsoluteArtifactFilename default-value="false">${outputAbsoluteArtifactFilename}</outputAbsoluteArtifactFilename>
  <outputDirectory default-value="${project.build.directory}/dependency">/home/programues/Documents/metadata_myoffice/content-all-share/target/content-all-share-war</outputDirectory>
  <overWriteIfNewer default-value="true">${mdep.overIfNewer}</overWriteIfNewer>
  <overWriteReleases default-value="false">${mdep.overWriteReleases}</overWriteReleases>
  <overWriteSnapshots default-value="false">${mdep.overWriteSnapshots}</overWriteSnapshots>
  <project default-value="${project}"/>
  <reactorProjects default-value="${reactorProjects}"/>
  <remoteRepos default-value="${project.remoteArtifactRepositories}"/>
  <silent default-value="false">${silent}</silent>
  <skip default-value="false">${mdep.skip}</skip>
  <useJvmChmod default-value="true">${dependency.useJvmChmod}</useJvmChmod>
</configuration>
[DEBUG] -----------------------------------------------------------------------
[DEBUG] Goal:          org.alfresco.maven.plugin:alfresco-maven-plugin:2.0.0:amp (default-amp)
[DEBUG] Style:         Regular
[DEBUG] Configuration: <?xml version="1.0" encoding="UTF-8"?>
<configuration>
  <ampBuildDirectory default-value="${project.build.directory}/${project.build.finalName}">${maven.alfresco.ampBuildDirectory}</ampBuildDirectory>
  <ampFinalDir default-value="${project.build.directory}">${maven.alfresco.ampFinalDir}</ampFinalDir>
  <ampFinalName default-value="${project.build.finalName}">${maven.alfresco.ampFinalName}</ampFinalName>
  <attachClasses default-value="false">${maven.alfresco.attachClasses}</attachClasses>
  <attachConfig default-value="false">${maven.alfresco.attachConfig}</attachConfig>
  <classesDirectory default-value="${project.build.outputDirectory}"/>
  <classifier>${maven.alfresco.classifier}</classifier>
  <includeDependencies default-value="true">${maven.alfresco.includeDependencies}</includeDependencies>
  <includeWebResources default-value="true">false</includeWebResources>
  <project default-value="${project}"/>
  <session default-value="${session}"/>
</configuration>
[DEBUG] -----------------------------------------------------------------------
[DEBUG] Goal:          org.alfresco.maven.plugin:alfresco-maven-plugin:2.0.0:install (amps-to-war-overlay)
[DEBUG] Style:         Regular
[DEBUG] Configuration: <?xml version="1.0" encoding="UTF-8"?>
<configuration>
  <ampLocation default-value="${project.build.directory}/${project.build.finalName}.amp">${maven.alfresco.ampLocation}</ampLocation>
  <backup default-value="false">${maven.alfresco.backup}</backup>
  <force default-value="true">${maven.alfresco.force}</force>
  <skipAmpInstallation default-value="false">${maven.alfresco.skipAmpInstallation}</skipAmpInstallation>
  <skipWarManifestCheck default-value="false">${maven.alfresco.skipWarManifestCheck}</skipWarManifestCheck>
  <verbose default-value="false">${maven.alfresco.verbose}</verbose>
  <warLocation default-value="${project.build.directory}/${project.build.finalName}-war">${maven.alfresco.warLocation}</warLocation>
</configuration>
[DEBUG] --- init fork of com.netprojex:content-all-share:1.0-SNAPSHOT for org.apache.tomcat.maven:tomcat7-maven-plugin:2.2:run (run-embedded) ---
[DEBUG] Dependencies (collect): []
[DEBUG] Dependencies (resolve): [compile]
[DEBUG] -----------------------------------------------------------------------
[DEBUG] Goal:          org.alfresco.maven.plugin:alfresco-maven-plugin:2.0.0:set-version (default-set-version)
[DEBUG] Style:         Regular
[DEBUG] Configuration: <?xml version="1.0" encoding="UTF-8"?>
<configuration>
  <customVersionSuffix>${maven.alfresco.customVersionSuffix}</customVersionSuffix>
  <project default-value="${project}"/>
  <propertyName default-value="noSnapshotVersion">${maven.alfresco.propertyName}</propertyName>
  <snapshotSuffix default-value="-SNAPSHOT">${maven.alfresco.snapshotSuffix}</snapshotSuffix>
  <snapshotToTimestamp default-value="false">true</snapshotToTimestamp>
  <version default-value="${project.version}"/>
</configuration>
[DEBUG] -----------------------------------------------------------------------
[DEBUG] Goal:          org.apache.maven.plugins:maven-resources-plugin:2.7:resources (default-resources)
[DEBUG] Style:         Regular
[DEBUG] Configuration: <?xml version="1.0" encoding="UTF-8"?>
<configuration>
  <buildFilters default-value="${project.build.filters}"/>
  <encoding default-value="${project.build.sourceEncoding}">UTF-8</encoding>
  <escapeString>${maven.resources.escapeString}</escapeString>
  <escapeWindowsPaths default-value="true">${maven.resources.escapeWindowsPaths}</escapeWindowsPaths>
  <includeEmptyDirs default-value="false">${maven.resources.includeEmptyDirs}</includeEmptyDirs>
  <nonFilteredFileExtensions>
    <nonFilteredFileExtension>ftl</nonFilteredFileExtension>
    <nonFilteredFileExtension>acp</nonFilteredFileExtension>
    <nonFilteredFileExtension>jpg</nonFilteredFileExtension>
    <nonFilteredFileExtension>png</nonFilteredFileExtension>
    <nonFilteredFileExtension>gif</nonFilteredFileExtension>
    <nonFilteredFileExtension>svg</nonFilteredFileExtension>
    <nonFilteredFileExtension>pdf</nonFilteredFileExtension>
    <nonFilteredFileExtension>doc</nonFilteredFileExtension>
    <nonFilteredFileExtension>docx</nonFilteredFileExtension>
    <nonFilteredFileExtension>xls</nonFilteredFileExtension>
    <nonFilteredFileExtension>xlsx</nonFilteredFileExtension>
    <nonFilteredFileExtension>ppt</nonFilteredFileExtension>
    <nonFilteredFileExtension>pptx</nonFilteredFileExtension>
    <nonFilteredFileExtension>bin</nonFilteredFileExtension>
    <nonFilteredFileExtension>lic</nonFilteredFileExtension>
    <nonFilteredFileExtension>swf</nonFilteredFileExtension>
    <nonFilteredFileExtension>zip</nonFilteredFileExtension>
    <nonFilteredFileExtension>msg</nonFilteredFileExtension>
    <nonFilteredFileExtension>jar</nonFilteredFileExtension>
    <nonFilteredFileExtension>ttf</nonFilteredFileExtension>
    <nonFilteredFileExtension>eot</nonFilteredFileExtension>
    <nonFilteredFileExtension>woff</nonFilteredFileExtension>
    <nonFilteredFileExtension>woff2</nonFilteredFileExtension>
  </nonFilteredFileExtensions>
  <outputDirectory default-value="${project.build.outputDirectory}"/>
  <overwrite default-value="false">${maven.resources.overwrite}</overwrite>
  <project default-value="${project}"/>
  <resources default-value="${project.resources}"/>
  <session default-value="${session}"/>
  <supportMultiLineFiltering default-value="false">${maven.resources.supportMultiLineFiltering}</supportMultiLineFiltering>
  <useBuildFilters default-value="true"/>
  <useDefaultDelimiters default-value="true"/>
</configuration>
[DEBUG] -----------------------------------------------------------------------
[DEBUG] Goal:          org.apache.maven.plugins:maven-compiler-plugin:3.2:compile (default-compile)
[DEBUG] Style:         Regular
[DEBUG] Configuration: <?xml version="1.0" encoding="UTF-8"?>
<configuration>
  <basedir default-value="${basedir}"/>
  <buildDirectory default-value="${project.build.directory}"/>
  <classpathElements default-value="${project.compileClasspathElements}"/>
  <compileSourceRoots default-value="${project.compileSourceRoots}"/>
  <compilerId default-value="javac">${maven.compiler.compilerId}</compilerId>
  <compilerReuseStrategy default-value="${reuseCreated}">${maven.compiler.compilerReuseStrategy}</compilerReuseStrategy>
  <compilerVersion>${maven.compiler.compilerVersion}</compilerVersion>
  <debug default-value="true">${maven.compiler.debug}</debug>
  <debuglevel>${maven.compiler.debuglevel}</debuglevel>
  <encoding default-value="${project.build.sourceEncoding}">${encoding}</encoding>
  <executable>${maven.compiler.executable}</executable>
  <failOnError default-value="true">${maven.compiler.failOnError}</failOnError>
  <forceJavacCompilerUse default-value="false">${maven.compiler.forceJavacCompilerUse}</forceJavacCompilerUse>
  <fork default-value="false">${maven.compiler.fork}</fork>
  <generatedSourcesDirectory default-value="${project.build.directory}/generated-sources/annotations"/>
  <maxmem>${maven.compiler.maxmem}</maxmem>
  <meminitial>${maven.compiler.meminitial}</meminitial>
  <mojoExecution>${mojoExecution}</mojoExecution>
  <optimize default-value="false">${maven.compiler.optimize}</optimize>
  <outputDirectory default-value="${project.build.outputDirectory}"/>
  <project default-value="${project}"/>
  <projectArtifact default-value="${project.artifact}"/>
  <session default-value="${session}"/>
  <showDeprecation default-value="false">${maven.compiler.showDeprecation}</showDeprecation>
  <showWarnings default-value="false">${maven.compiler.showWarnings}</showWarnings>
  <skipMain>${maven.main.skip}</skipMain>
  <skipMultiThreadWarning default-value="false">${maven.compiler.skipMultiThreadWarning}</skipMultiThreadWarning>
  <source default-value="1.5">1.7</source>
  <staleMillis default-value="0">${lastModGranularityMs}</staleMillis>
  <target default-value="1.5">1.7</target>
  <useIncrementalCompilation default-value="true">${maven.compiler.useIncrementalCompilation}</useIncrementalCompilation>
  <verbose default-value="false">${maven.compiler.verbose}</verbose>
</configuration>
[DEBUG] --- exit fork of com.netprojex:content-all-share:1.0-SNAPSHOT for org.apache.tomcat.maven:tomcat7-maven-plugin:2.2:run (run-embedded) ---
[DEBUG] -----------------------------------------------------------------------
[DEBUG] Goal:          org.apache.tomcat.maven:tomcat7-maven-plugin:2.2:run (run-embedded)
[DEBUG] Style:         Regular
[DEBUG] Configuration: <?xml version="1.0" encoding="UTF-8"?>
<configuration>
  <addContextWarDependencies default-value="false">${maven.tomcat.addContextWarDependencies}</addContextWarDependencies>
  <addWarDependenciesInClassloader default-value="true">${maven.tomcat.addWarDependenciesInClassloader}</addWarDependenciesInClassloader>
  <additionalConfigFilesDir default-value="${basedir}/src/main/tomcatconf">${maven.tomcat.additionalConfigFilesDir}</additionalConfigFilesDir>
  <address>${maven.tomcat.address}</address>
  <ajpPort default-value="0">${maven.tomcat.ajp.port}</ajpPort>
  <ajpProtocol default-value="org.apache.coyote.ajp.AjpProtocol">${maven.tomcat.ajp.protocol}</ajpProtocol>
  <backgroundProcessorDelay default-value="-1">${maven.tomcat.backgroundProcessorDelay}</backgroundProcessorDelay>
  <clientAuth default-value="false">${maven.tomcat.https.clientAuth}</clientAuth>
  <configurationDir default-value="${project.build.directory}/tomcat"/>
  <contextFile>/home/programues/Documents/metadata_myoffice/content-all-share/tomcat/context.xml</contextFile>
  <contextReloadable default-value="false">${maven.tomcat.contextReloadable}</contextReloadable>
  <defaultContextFile default-value="${project.build.directory}/${project.build.finalName}/META-INF/context.xml"/>
  <delegate default-value="true">true</delegate>
  <dependencies default-value="${project.artifacts}"/>
  <fork default-value="false">${maven.tomcat.fork}</fork>
  <hostName default-value="localhost">${maven.tomcat.hostName}</hostName>
  <httpsPort default-value="0">${maven.tomcat.httpsPort}</httpsPort>
  <ignorePackaging default-value="false">true</ignorePackaging>
  <jarScanAllDirectories default-value="true">${maven.tomcat.jarScan.allDirectories}</jarScanAllDirectories>
  <keystoreType default-value="JKS"/>
  <local default-value="${localRepository}"/>
  <packaging default-value="${project.packaging}"/>
  <path default-value="/${project.artifactId}">${maven.tomcat.path}</path>
  <pluginArtifacts default-value="${plugin.artifacts}"/>
  <port default-value="8080">${maven.tomcat.port}</port>
  <propertiesPortFilePath>${maven.tomcat.propertiesPortFilePath}</propertiesPortFilePath>
  <protocol default-value="HTTP/1.1">${maven.tomcat.protocol}</protocol>
  <serverXml>${maven.tomcat.serverXml}</serverXml>
  <session default-value="${session}"/>
  <skip default-value="false">${maven.tomcat.skip}</skip>
  <staticContextDocbase>${maven.tomcat.staticContextDocbase}</staticContextDocbase>
  <staticContextPath default-value="/">${maven.tomcat.staticContextPath}</staticContextPath>
  <systemProperties>
    <java.io.tmpdir>/home/programues/Documents/metadata_myoffice/content-all-share/target</java.io.tmpdir>
  </systemProperties>
  <tomcatLoggingFile>${maven.tomcat.tomcatLogging.file}</tomcatLoggingFile>
  <tomcatUsers>${maven.tomcat.tomcatUsers.file}</tomcatUsers>
  <tomcatWebXml>${maven.tomcat.webXml}</tomcatWebXml>
  <uriEncoding default-value="ISO-8859-1">${maven.tomcat.uriEncoding}</uriEncoding>
  <useBodyEncodingForURI default-value="false">${maven.tomcat.useBodyEncodingForURI}</useBodyEncodingForURI>
  <useNaming default-value="true">${maven.tomcat.useNaming}</useNaming>
  <useSeparateTomcatClassLoader default-value="false">true</useSeparateTomcatClassLoader>
  <useTestClasspath default-value="false">false</useTestClasspath>
  <warSourceDirectory default-value="${basedir}/src/main/webapp">${tomcat.warSourceDirectory}</warSourceDirectory>
  <project default-value="${project}"/>
  <settings default-value="${settings}"/>
</configuration>
[DEBUG] =======================================================================
[DEBUG] Could not find metadata org.jvnet.staxex:stax-ex/maven-metadata.xml in local (/root/.m2/repository)
[DEBUG] Failure to find org.jvnet.staxex:stax-ex/maven-metadata.xml in https://artifacts.alfresco.com/nexus/content/groups/public was cached in the local repository, resolution will not be reattempted until the update interval of alfresco-public has elapsed or updates are forced
[DEBUG] Failure to find org.jvnet.staxex:stax-ex/maven-metadata.xml in https://artifacts.alfresco.com/nexus/content/groups/public-snapshots was cached in the local repository, resolution will not be reattempted until the update interval of alfresco-public-snapshots has elapsed or updates are forced
[DEBUG] Skipped remote update check for org.jvnet.staxex:stax-ex/maven-metadata.xml, locally cached metadata up-to-date.
[DEBUG] Failure to find org.jvnet.staxex:stax-ex/maven-metadata.xml in http://repository.jboss.org/nexus/content/groups/public/ was cached in the local repository, resolution will not be reattempted until the update interval of jboss.public has elapsed or updates are forced
[DEBUG] Failure to find org.jvnet.staxex:stax-ex/maven-metadata.xml in http://download.java.net/maven/2/ was cached in the local repository, resolution will not be reattempted until the update interval of maven2-repository.dev.java.net has elapsed or updates are forced
[DEBUG] com.netprojex:content-all-share:amp:1.0-SNAPSHOT
[DEBUG]    org.alfresco:share:jar:classes:5.0.c:provided
[DEBUG]       org.alfresco:alfresco-web-framework-commons:jar:classes:5.0.c:provided (scope managed from compile by org.alfresco:alfresco-platform-distribution:5.0.c)
[DEBUG]       org.alfresco:alfresco-jlan-embed:jar:5.0.c:provided (scope managed from compile by org.alfresco:alfresco-platform-distribution:5.0.c)
[DEBUG]          org.alfresco:alfresco-core:jar:5.0.c:provided (scope managed from compile by org.alfresco:alfresco-platform-distribution:5.0.c)
[DEBUG]             commons-codec:commons-codec:jar:1.9:provided
[DEBUG]             commons-httpclient:commons-httpclient:jar:3.1:provided
[DEBUG]             org.safehaus.jug:jug:jar:asl:2.0.0:provided
[DEBUG]             log4j:log4j:jar:1.2.17:provided
[DEBUG]             org.json:json:jar:20090211:provided
[DEBUG]             org.springframework:spring-orm:jar:3.2.10.RELEASE:provided
[DEBUG]                org.springframework:spring-beans:jar:3.2.10.RELEASE:provided
[DEBUG]                org.springframework:spring-core:jar:3.2.10.RELEASE:provided
[DEBUG]                org.springframework:spring-jdbc:jar:3.2.10.RELEASE:provided
[DEBUG]                org.springframework:spring-tx:jar:3.2.10.RELEASE:provided
[DEBUG]             org.springframework.extensions.surf:spring-surf-core-configservice:jar:5.0.c:provided
[DEBUG]                org.springframework.extensions.surf:spring-surf-core:jar:5.0.c:provided
[DEBUG]                org.springframework:spring-context:jar:3.2.10.RELEASE:provided
[DEBUG]                   org.springframework:spring-aop:jar:3.2.10.RELEASE:provided
[DEBUG]                org.springframework:spring-context-support:jar:3.2.10.RELEASE:provided
[DEBUG]                org.springframework:spring-web:jar:3.2.10.RELEASE:provided
[DEBUG]             com.sun.xml.bind:jaxb-impl:jar:2.1.11:provided
[DEBUG]             dom4j:dom4j:jar:1.6.1:provided
[DEBUG]                xml-apis:xml-apis:jar:1.4.01:provided (version managed from 1.0.b2 by org.alfresco:alfresco-parent:5.0.c)
[DEBUG]             org.codehaus.guessencoding:guessencoding:jar:1.0:provided
[DEBUG]             javax.transaction:jta:jar:1.0.1b:provided
[DEBUG]             joda-time:joda-time:jar:2.3:provided
[DEBUG]             org.apache.httpcomponents:httpclient:jar:4.3.3:provided
[DEBUG]                org.apache.httpcomponents:httpcore:jar:4.3.2:provided
[DEBUG]          com.hazelcast:hazelcast:jar:2.4:provided
[DEBUG]       org.apache.chemistry.opencmis:chemistry-opencmis-client-impl:jar:0.11.0:provided
[DEBUG]          org.apache.chemistry.opencmis:chemistry-opencmis-client-api:jar:0.11.0:provided
[DEBUG]          org.apache.chemistry.opencmis:chemistry-opencmis-commons-api:jar:0.11.0:provided
[DEBUG]          org.apache.chemistry.opencmis:chemistry-opencmis-commons-impl:jar:0.11.0:provided
[DEBUG]             org.codehaus.woodstox:woodstox-core-asl:jar:4.2.0:provided
[DEBUG]                javax.xml.stream:stax-api:jar:1.0-2:provided
[DEBUG]                org.codehaus.woodstox:stax2-api:jar:3.1.1:provided
[DEBUG]             org.jvnet.mimepull:mimepull:jar:1.9.4:provided
[DEBUG]             com.sun.xml.ws:jaxws-rt:jar:2.1.7:provided
[DEBUG]                javax.xml.ws:jaxws-api:jar:2.1:provided
[DEBUG]                   javax.xml.bind:jaxb-api:jar:2.1:provided
[DEBUG]                com.sun.xml.messaging.saaj:saaj-impl:jar:1.3.3:provided
[DEBUG]                   javax.xml.soap:saaj-api:jar:1.3:provided
[DEBUG]                com.sun.xml.stream.buffer:streambuffer:jar:0.9:provided
[DEBUG]                   javax.activation:activation:jar:1.1:provided
[DEBUG]                org.jvnet.staxex:stax-ex:jar:1.2:provided
[DEBUG]                com.sun.org.apache.xml.internal:resolver:jar:20050927:provided
[DEBUG]          org.apache.chemistry.opencmis:chemistry-opencmis-client-bindings:jar:0.11.0:provided
[DEBUG]          org.slf4j:slf4j-api:jar:1.7.5:compile
[DEBUG]       org.alfresco.cmis.client:alfresco-opencmis-extension:jar:1.0:provided
[DEBUG]       org.springframework.extensions.surf:spring-cmis-framework:jar:5.0.c:provided
[DEBUG]          org.springframework.extensions.surf:spring-webscripts:jar:5.0.c:provided
[DEBUG]             commons-beanutils:commons-beanutils:jar:1.9.1:provided
[DEBUG]             org.htmlparser:htmlparser:jar:2.1:provided
[DEBUG]                org.htmlparser:htmllexer:jar:2.1:provided
[DEBUG]             org.springframework:spring-webmvc:jar:3.2.10.RELEASE:provided
[DEBUG]                org.springframework:spring-expression:jar:3.2.10.RELEASE:provided
[DEBUG]       org.mozilla:rhino:jar:1.7R4-alfresco-patched:provided
[DEBUG]       com.googlecode.json-simple:json-simple:jar:1.1.1:provided
[DEBUG]       org.tuckey:urlrewritefilter:jar:4.0.4:provided
[DEBUG]       commons-fileupload:commons-fileupload:jar:1.3.1:provided
[DEBUG]          commons-io:commons-io:jar:2.4:provided (version managed from 2.2 by org.alfresco:alfresco-parent:5.0.c)
[DEBUG]       jaxen:jaxen:jar:1.1.6:provided
[DEBUG]       org.freemarker:freemarker:jar:2.3.20-alfresco-patched:provided
[DEBUG]       stax:stax-api:jar:1.0.1:provided
[DEBUG]       com.hazelcast:hazelcast-spring:jar:2.4:provided
[DEBUG]       javax.servlet:jstl:jar:1.2:provided
[DEBUG]       org.apache.myfaces.core:myfaces-api:jar:1.1.8:provided
[DEBUG]          commons-logging:commons-logging:jar:1.1.3:provided (version managed from 1.1.1 by org.alfresco:alfresco-parent:5.0.c)
[DEBUG]       org.apache.myfaces.core:myfaces-impl:jar:1.1.8:provided
[DEBUG]          commons-el:commons-el:jar:1.0:provided
[DEBUG]          commons-lang:commons-lang:jar:2.6:provided (version managed from 2.1 by org.alfresco:alfresco-parent:5.0.c)
[DEBUG]          commons-collections:commons-collections:jar:3.2.1:provided (version managed from 3.1 by org.alfresco:alfresco-parent:5.0.c)
[DEBUG]          commons-digester:commons-digester:jar:2.1:provided (version managed from 1.8 by org.alfresco:alfresco-parent:5.0.c)
[DEBUG]       org.dojotoolkit:dojo:zip:1.9.0:provided
[DEBUG]       com.yahoo.platform.yui:yui:zip:2.9.0-alfresco-20140211:provided
[DEBUG]       com.asual.lesscss:lesscss-engine:jar:1.5.0:provided
[DEBUG]    org.springframework.extensions.surf:spring-surf-api:jar:5.0.c:provided
[DEBUG]       org.springframework.extensions.surf:spring-surf:jar:5.0.c:provided
[DEBUG]          com.yahoo.platform.yui:yuicompressor:jar:2.4.8-alfresco-patched:provided
[DEBUG]          com.googlecode.concurrentlinkedhashmap:concurrentlinkedhashmap-lru:jar:1.2:provided
[DEBUG]       org.springframework.extensions.surf:spring-webscripts-api:jar:5.0.c:provided
[DEBUG]       javax.servlet:servlet-api:jar:2.5:provided (scope managed from compile by org.alfresco:alfresco-parent:5.0.c)
[DEBUG]    org.alfresco.maven:alfresco-rad:jar:2.0.0:compile
[DEBUG]       com.tradeshift:junit-remote:jar:3:compile
[DEBUG]          junit:junit:jar:4.11:compile (version managed from 4.8 by org.alfresco:alfresco-parent:5.0.c)
[DEBUG]             org.hamcrest:hamcrest-core:jar:1.3:compile
[DEBUG]          org.eclipse.jetty:jetty-servlet:jar:7.4.0.v20110414:compile
[DEBUG]             org.eclipse.jetty:jetty-security:jar:7.4.0.v20110414:compile
[DEBUG]          args4j:args4j:jar:2.0.16:compile
[DEBUG]       org.springframework:spring-test:jar:3.0.6.RELEASE:compile
[DEBUG]    org.eclipse.jetty:jetty-server:jar:7.4.0.v20110414:compile
[DEBUG]       org.eclipse.jetty:jetty-continuation:jar:7.4.0.v20110414:compile
[DEBUG]       org.eclipse.jetty:jetty-http:jar:7.4.0.v20110414:compile
[DEBUG]          org.eclipse.jetty:jetty-io:jar:7.4.0.v20110414:compile
[DEBUG]             org.eclipse.jetty:jetty-util:jar:7.4.0.v20110414:compile
[INFO] 
[INFO] --- alfresco-maven-plugin:2.0.0:set-version (default-set-version) @ content-all-share ---
[DEBUG] org.alfresco.maven.plugin:alfresco-maven-plugin:jar:2.0.0:
[DEBUG]    org.apache.maven:maven-plugin-api:jar:3.0.4:compile
[DEBUG]       org.apache.maven:maven-model:jar:3.0.4:compile
[DEBUG]       org.sonatype.sisu:sisu-inject-plexus:jar:2.3.0:compile
[DEBUG]          org.sonatype.sisu:sisu-inject-bean:jar:2.3.0:compile
[DEBUG]             org.sonatype.sisu:sisu-guice:jar:no_aop:3.1.0:compile
[DEBUG]                org.sonatype.sisu:sisu-guava:jar:0.9.9:compile
[DEBUG]    org.apache.maven:maven-archiver:jar:2.5:compile
[DEBUG]       org.codehaus.plexus:plexus-utils:jar:3.0:compile
[DEBUG]       org.codehaus.plexus:plexus-interpolation:jar:1.15:compile
[DEBUG]    org.codehaus.plexus:plexus-archiver:jar:2.3:compile
[DEBUG]       org.codehaus.plexus:plexus-container-default:jar:1.0-alpha-9-stable-1:compile
[DEBUG]          junit:junit:jar:3.8.1:compile
[DEBUG]          classworlds:classworlds:jar:1.1-alpha-2:compile
[DEBUG]       org.codehaus.plexus:plexus-io:jar:2.0.6:compile
[DEBUG]    org.apache.maven:maven-artifact:jar:3.0.4:compile
[DEBUG]    org.apache.maven:maven-core:jar:3.0.4:compile
[DEBUG]       org.apache.maven:maven-settings:jar:3.0.4:compile
[DEBUG]       org.apache.maven:maven-settings-builder:jar:3.0.4:compile
[DEBUG]       org.apache.maven:maven-repository-metadata:jar:3.0.4:compile
[DEBUG]       org.apache.maven:maven-model-builder:jar:3.0.4:compile
[DEBUG]       org.apache.maven:maven-aether-provider:jar:3.0.4:compile
[DEBUG]          org.sonatype.aether:aether-spi:jar:1.13.1:compile
[DEBUG]       org.sonatype.aether:aether-impl:jar:1.13.1:compile
[DEBUG]       org.sonatype.aether:aether-api:jar:1.13.1:compile
[DEBUG]       org.sonatype.aether:aether-util:jar:1.13.1:compile
[DEBUG]       org.codehaus.plexus:plexus-classworlds:jar:2.4:compile
[DEBUG]       org.codehaus.plexus:plexus-component-annotations:jar:1.5.5:compile
[DEBUG]       org.sonatype.plexus:plexus-sec-dispatcher:jar:1.3:compile
[DEBUG]          org.sonatype.plexus:plexus-cipher:jar:1.4:compile
[DEBUG]    commons-io:commons-io:jar:2.1:compile
[DEBUG]    org.alfresco:alfresco-mmt:jar:5.0.c:compile
[DEBUG]    org.alfrescolabs.alfresco-technical-validation:org.alfrescolabs.alfresco-technical-validation:jar:0.4.0:compile
[DEBUG]       org.clojure:clojure:jar:1.6.0:compile
[DEBUG]       org.clojure:tools.cli:jar:0.3.1:compile
[DEBUG]       org.clojure:tools.logging:jar:0.3.0:compile
[DEBUG]       clojurewerkz:neocons:jar:3.0.0:compile
[DEBUG]          cheshire:cheshire:jar:5.3.1:compile
[DEBUG]             com.fasterxml.jackson.core:jackson-core:jar:2.3.1:compile
[DEBUG]             com.fasterxml.jackson.dataformat:jackson-dataformat-smile:jar:2.3.1:compile
[DEBUG]             tigris:tigris:jar:0.1.1:compile
[DEBUG]          clj-http:clj-http:jar:0.9.1:compile
[DEBUG]             org.apache.httpcomponents:httpcore:jar:4.3.2:compile
[DEBUG]             org.apache.httpcomponents:httpclient:jar:4.3.2:compile
[DEBUG]             org.apache.httpcomponents:httpmime:jar:4.3.2:compile
[DEBUG]             commons-codec:commons-codec:jar:1.9:compile
[DEBUG]             slingshot:slingshot:jar:0.10.3:compile
[DEBUG]             crouton:crouton:jar:0.1.1:compile
[DEBUG]                org.jsoup:jsoup:jar:1.7.1:compile
[DEBUG]             org.clojure:tools.reader:jar:0.8.3:compile
[DEBUG]             potemkin:potemkin:jar:0.3.4:compile
[DEBUG]                clj-tuple:clj-tuple:jar:0.1.2:compile
[DEBUG]                riddley:riddley:jar:0.1.6:compile
[DEBUG]          clojurewerkz:support:jar:0.20.0:compile
[DEBUG]             com.google.guava:guava:jar:14.0.1:compile
[DEBUG]          clojurewerkz:urly:jar:2.0.0-alpha5:compile
[DEBUG]       ch.qos.logback:logback-classic:jar:1.1.2:compile
[DEBUG]          ch.qos.logback:logback-core:jar:1.1.2:compile
[DEBUG]          org.slf4j:slf4j-api:jar:1.7.6:compile
[DEBUG]       me.raynes:conch:jar:0.7.0:compile
[DEBUG]          org.flatland:useful:jar:0.10.6:compile
[DEBUG]             org.clojure:tools.macro:jar:0.1.1:compile
[DEBUG]       jansi-clj:jansi-clj:jar:0.1.0:compile
[DEBUG]          org.fusesource.jansi:jansi:jar:1.11:compile
[DEBUG]       io.aviso:pretty:jar:0.1.12:compile
[DEBUG]       org.clojars.pmonks:depends:jar:0.3.0:compile
[DEBUG]          org.clojure:data.json:jar:0.2.4:compile
[DEBUG]          org.ow2.asm:asm:jar:5.0.3:compile
[DEBUG]          net.java.truevfs:truevfs-kernel-impl:jar:0.10.6:compile
[DEBUG]             net.java.truevfs:truevfs-kernel-spec:jar:0.10.6:compile
[DEBUG]                net.java.truecommons:truecommons-cio:jar:2.3.4:compile
[DEBUG]                   net.java.truecommons:truecommons-io:jar:2.3.4:compile
[DEBUG]                net.java.truecommons:truecommons-services:jar:2.3.4:compile
[DEBUG]                   net.java.truecommons:truecommons-logging:jar:2.3.4:compile
[DEBUG]                   javax.inject:javax.inject:jar:1:compile
[DEBUG]             org.scala-lang:scala-library:jar:2.10.3:compile
[DEBUG]          net.java.truevfs:truevfs-access:jar:0.10.6:compile
[DEBUG]          net.java.truevfs:truevfs-driver-file:jar:0.10.6:compile
[DEBUG]          net.java.truevfs:truevfs-driver-zip:jar:0.10.6:compile
[DEBUG]             net.java.truevfs:truevfs-comp-ibm437:jar:0.10.6:runtime
[DEBUG]             net.java.truevfs:truevfs-comp-zipdriver:jar:0.10.6:compile
[DEBUG]                net.java.truevfs:truevfs-comp-zip:jar:0.10.6:compile
[DEBUG]                org.apache.commons:commons-compress:jar:1.7:compile
[DEBUG]                org.bouncycastle:bcprov-jdk16:jar:1.46:compile
[DEBUG]          net.java.truevfs:truevfs-driver-jar:jar:0.10.6:compile
[DEBUG]          net.java.truecommons:truecommons-key-disable:jar:2.3.4:compile
[DEBUG]             net.java.truecommons:truecommons-key-spec:jar:2.3.4:compile
[DEBUG]                net.java.truecommons:truecommons-shed:jar:2.3.4:compile
[DEBUG]       org.clojars.pmonks:multigrep:jar:0.2.0:compile
[DEBUG]       org.clojars.pmonks:bookmark-writer:jar:0.1.0:compile
[DEBUG]          org.docx4j:docx4j:jar:3.0.1:compile
[DEBUG]             org.plutext:jaxb-svg11:jar:1.0.2:compile
[DEBUG]             org.plutext:jaxb-xslfo:jar:1.0.1:compile
[DEBUG]             org.plutext:jaxb-xmldsig-core:jar:1.0.0:compile
[DEBUG]             commons-lang:commons-lang:jar:2.4:compile
[DEBUG]             org.apache.xmlgraphics:xmlgraphics-commons:jar:1.5:compile
[DEBUG]             commons-logging:commons-logging:jar:1.1.1:compile
[DEBUG]             org.apache.xmlgraphics:fop:jar:1.1:compile
[DEBUG]                org.apache.xmlgraphics:batik-svg-dom:jar:1.7:compile
[DEBUG]                   org.apache.xmlgraphics:batik-anim:jar:1.7:compile
[DEBUG]                   org.apache.xmlgraphics:batik-css:jar:1.7:compile
[DEBUG]                   org.apache.xmlgraphics:batik-dom:jar:1.7:compile
[DEBUG]                   org.apache.xmlgraphics:batik-parser:jar:1.7:compile
[DEBUG]                   org.apache.xmlgraphics:batik-util:jar:1.7:compile
[DEBUG]                org.apache.xmlgraphics:batik-bridge:jar:1.7:compile
[DEBUG]                   org.apache.xmlgraphics:batik-script:jar:1.7:compile
[DEBUG]                      org.apache.xmlgraphics:batik-js:jar:1.7:compile
[DEBUG]                   org.apache.xmlgraphics:batik-xml:jar:1.7:compile
[DEBUG]                org.apache.xmlgraphics:batik-awt-util:jar:1.7:compile
[DEBUG]                org.apache.xmlgraphics:batik-gvt:jar:1.7:compile
[DEBUG]                org.apache.xmlgraphics:batik-transcoder:jar:1.7:compile
[DEBUG]                   org.apache.xmlgraphics:batik-svggen:jar:1.7:compile
[DEBUG]                org.apache.xmlgraphics:batik-extension:jar:1.7:compile
[DEBUG]                org.apache.xmlgraphics:batik-ext:jar:1.7:compile
[DEBUG]             org.apache.avalon.framework:avalon-framework-api:jar:4.3.1:compile
[DEBUG]             org.apache.avalon.framework:avalon-framework-impl:jar:4.3.1:compile
[DEBUG]             xalan:xalan:jar:2.7.1:compile
[DEBUG]                xalan:serializer:jar:2.7.1:compile
[DEBUG]             net.arnx:wmf2svg:jar:0.9.0:compile
[DEBUG]             org.apache.poi:poi-scratchpad:jar:3.8:compile
[DEBUG]                org.apache.poi:poi:jar:3.8:compile
[DEBUG]             org.antlr:antlr-runtime:jar:3.3:compile
[DEBUG]             org.antlr:stringtemplate:jar:3.2.1:compile
[DEBUG]                antlr:antlr:jar:2.7.7:compile
[DEBUG]       org.clojars.pmonks:spinner:jar:0.2.0:compile
[DEBUG] Created new class realm plugin>org.alfresco.maven.plugin:alfresco-maven-plugin:2.0.0
[DEBUG] Importing foreign packages into class realm plugin>org.alfresco.maven.plugin:alfresco-maven-plugin:2.0.0
[DEBUG]   Imported:  < project>com.netprojex:content-all-share:1.0-SNAPSHOT
[DEBUG] Populating class realm plugin>org.alfresco.maven.plugin:alfresco-maven-plugin:2.0.0
[DEBUG]   Included: org.alfresco.maven.plugin:alfresco-maven-plugin:jar:2.0.0
[DEBUG]   Included: org.sonatype.sisu:sisu-inject-bean:jar:2.3.0
[DEBUG]   Included: org.sonatype.sisu:sisu-guice:jar:no_aop:3.1.0
[DEBUG]   Included: org.sonatype.sisu:sisu-guava:jar:0.9.9
[DEBUG]   Included: org.apache.maven:maven-archiver:jar:2.5
[DEBUG]   Included: org.codehaus.plexus:plexus-utils:jar:3.0
[DEBUG]   Included: org.codehaus.plexus:plexus-interpolation:jar:1.15
[DEBUG]   Included: org.codehaus.plexus:plexus-archiver:jar:2.3
[DEBUG]   Included: junit:junit:jar:3.8.1
[DEBUG]   Included: org.codehaus.plexus:plexus-io:jar:2.0.6
[DEBUG]   Included: org.sonatype.aether:aether-util:jar:1.13.1
[DEBUG]   Included: org.codehaus.plexus:plexus-component-annotations:jar:1.5.5
[DEBUG]   Included: org.sonatype.plexus:plexus-sec-dispatcher:jar:1.3
[DEBUG]   Included: org.sonatype.plexus:plexus-cipher:jar:1.4
[DEBUG]   Included: commons-io:commons-io:jar:2.1
[DEBUG]   Included: org.alfresco:alfresco-mmt:jar:5.0.c
[DEBUG]   Included: org.alfrescolabs.alfresco-technical-validation:org.alfrescolabs.alfresco-technical-validation:jar:0.4.0
[DEBUG]   Included: org.clojure:clojure:jar:1.6.0
[DEBUG]   Included: org.clojure:tools.cli:jar:0.3.1
[DEBUG]   Included: org.clojure:tools.logging:jar:0.3.0
[DEBUG]   Included: clojurewerkz:neocons:jar:3.0.0
[DEBUG]   Included: cheshire:cheshire:jar:5.3.1
[DEBUG]   Included: com.fasterxml.jackson.core:jackson-core:jar:2.3.1
[DEBUG]   Included: com.fasterxml.jackson.dataformat:jackson-dataformat-smile:jar:2.3.1
[DEBUG]   Included: tigris:tigris:jar:0.1.1
[DEBUG]   Included: clj-http:clj-http:jar:0.9.1
[DEBUG]   Included: org.apache.httpcomponents:httpcore:jar:4.3.2
[DEBUG]   Included: org.apache.httpcomponents:httpclient:jar:4.3.2
[DEBUG]   Included: org.apache.httpcomponents:httpmime:jar:4.3.2
[DEBUG]   Included: commons-codec:commons-codec:jar:1.9
[DEBUG]   Included: slingshot:slingshot:jar:0.10.3
[DEBUG]   Included: crouton:crouton:jar:0.1.1
[DEBUG]   Included: org.jsoup:jsoup:jar:1.7.1
[DEBUG]   Included: org.clojure:tools.reader:jar:0.8.3
[DEBUG]   Included: potemkin:potemkin:jar:0.3.4
[DEBUG]   Included: clj-tuple:clj-tuple:jar:0.1.2
[DEBUG]   Included: riddley:riddley:jar:0.1.6
[DEBUG]   Included: clojurewerkz:support:jar:0.20.0
[DEBUG]   Included: com.google.guava:guava:jar:14.0.1
[DEBUG]   Included: clojurewerkz:urly:jar:2.0.0-alpha5
[DEBUG]   Included: ch.qos.logback:logback-classic:jar:1.1.2
[DEBUG]   Included: ch.qos.logback:logback-core:jar:1.1.2
[DEBUG]   Included: org.slf4j:slf4j-api:jar:1.7.6
[DEBUG]   Included: me.raynes:conch:jar:0.7.0
[DEBUG]   Included: org.flatland:useful:jar:0.10.6
[DEBUG]   Included: org.clojure:tools.macro:jar:0.1.1
[DEBUG]   Included: jansi-clj:jansi-clj:jar:0.1.0
[DEBUG]   Included: org.fusesource.jansi:jansi:jar:1.11
[DEBUG]   Included: io.aviso:pretty:jar:0.1.12
[DEBUG]   Included: org.clojars.pmonks:depends:jar:0.3.0
[DEBUG]   Included: org.clojure:data.json:jar:0.2.4
[DEBUG]   Included: org.ow2.asm:asm:jar:5.0.3
[DEBUG]   Included: net.java.truevfs:truevfs-kernel-impl:jar:0.10.6
[DEBUG]   Included: net.java.truevfs:truevfs-kernel-spec:jar:0.10.6
[DEBUG]   Included: net.java.truecommons:truecommons-cio:jar:2.3.4
[DEBUG]   Included: net.java.truecommons:truecommons-io:jar:2.3.4
[DEBUG]   Included: net.java.truecommons:truecommons-services:jar:2.3.4
[DEBUG]   Included: net.java.truecommons:truecommons-logging:jar:2.3.4
[DEBUG]   Included: javax.inject:javax.inject:jar:1
[DEBUG]   Included: org.scala-lang:scala-library:jar:2.10.3
[DEBUG]   Included: net.java.truevfs:truevfs-access:jar:0.10.6
[DEBUG]   Included: net.java.truevfs:truevfs-driver-file:jar:0.10.6
[DEBUG]   Included: net.java.truevfs:truevfs-driver-zip:jar:0.10.6
[DEBUG]   Included: net.java.truevfs:truevfs-comp-ibm437:jar:0.10.6
[DEBUG]   Included: net.java.truevfs:truevfs-comp-zipdriver:jar:0.10.6
[DEBUG]   Included: net.java.truevfs:truevfs-comp-zip:jar:0.10.6
[DEBUG]   Included: org.apache.commons:commons-compress:jar:1.7
[DEBUG]   Included: org.bouncycastle:bcprov-jdk16:jar:1.46
[DEBUG]   Included: net.java.truevfs:truevfs-driver-jar:jar:0.10.6
[DEBUG]   Included: net.java.truecommons:truecommons-key-disable:jar:2.3.4
[DEBUG]   Included: net.java.truecommons:truecommons-key-spec:jar:2.3.4
[DEBUG]   Included: net.java.truecommons:truecommons-shed:jar:2.3.4
[DEBUG]   Included: org.clojars.pmonks:multigrep:jar:0.2.0
[DEBUG]   Included: org.clojars.pmonks:bookmark-writer:jar:0.1.0
[DEBUG]   Included: org.docx4j:docx4j:jar:3.0.1
[DEBUG]   Included: org.plutext:jaxb-svg11:jar:1.0.2
[DEBUG]   Included: org.plutext:jaxb-xslfo:jar:1.0.1
[DEBUG]   Included: org.plutext:jaxb-xmldsig-core:jar:1.0.0
[DEBUG]   Included: commons-lang:commons-lang:jar:2.4
[DEBUG]   Included: org.apache.xmlgraphics:xmlgraphics-commons:jar:1.5
[DEBUG]   Included: commons-logging:commons-logging:jar:1.1.1
[DEBUG]   Included: org.apache.xmlgraphics:fop:jar:1.1
[DEBUG]   Included: org.apache.xmlgraphics:batik-svg-dom:jar:1.7
[DEBUG]   Included: org.apache.xmlgraphics:batik-anim:jar:1.7
[DEBUG]   Included: org.apache.xmlgraphics:batik-css:jar:1.7
[DEBUG]   Included: org.apache.xmlgraphics:batik-dom:jar:1.7
[DEBUG]   Included: org.apache.xmlgraphics:batik-parser:jar:1.7
[DEBUG]   Included: org.apache.xmlgraphics:batik-util:jar:1.7
[DEBUG]   Included: org.apache.xmlgraphics:batik-bridge:jar:1.7
[DEBUG]   Included: org.apache.xmlgraphics:batik-script:jar:1.7
[DEBUG]   Included: org.apache.xmlgraphics:batik-js:jar:1.7
[DEBUG]   Included: org.apache.xmlgraphics:batik-xml:jar:1.7
[DEBUG]   Included: org.apache.xmlgraphics:batik-awt-util:jar:1.7
[DEBUG]   Included: org.apache.xmlgraphics:batik-gvt:jar:1.7
[DEBUG]   Included: org.apache.xmlgraphics:batik-transcoder:jar:1.7
[DEBUG]   Included: org.apache.xmlgraphics:batik-svggen:jar:1.7
[DEBUG]   Included: org.apache.xmlgraphics:batik-extension:jar:1.7
[DEBUG]   Included: org.apache.xmlgraphics:batik-ext:jar:1.7
[DEBUG]   Included: org.apache.avalon.framework:avalon-framework-api:jar:4.3.1
[DEBUG]   Included: org.apache.avalon.framework:avalon-framework-impl:jar:4.3.1
[DEBUG]   Included: xalan:xalan:jar:2.7.1
[DEBUG]   Included: xalan:serializer:jar:2.7.1
[DEBUG]   Included: net.arnx:wmf2svg:jar:0.9.0
[DEBUG]   Included: org.apache.poi:poi-scratchpad:jar:3.8
[DEBUG]   Included: org.apache.poi:poi:jar:3.8
[DEBUG]   Included: org.antlr:antlr-runtime:jar:3.3
[DEBUG]   Included: org.antlr:stringtemplate:jar:3.2.1
[DEBUG]   Included: antlr:antlr:jar:2.7.7
[DEBUG]   Included: org.clojars.pmonks:spinner:jar:0.2.0
[DEBUG]   Excluded: org.apache.maven:maven-plugin-api:jar:3.0.4
[DEBUG]   Excluded: org.apache.maven:maven-model:jar:3.0.4
[DEBUG]   Excluded: org.sonatype.sisu:sisu-inject-plexus:jar:2.3.0
[DEBUG]   Excluded: org.codehaus.plexus:plexus-container-default:jar:1.0-alpha-9-stable-1
[DEBUG]   Excluded: classworlds:classworlds:jar:1.1-alpha-2
[DEBUG]   Excluded: org.apache.maven:maven-artifact:jar:3.0.4
[DEBUG]   Excluded: org.apache.maven:maven-core:jar:3.0.4
[DEBUG]   Excluded: org.apache.maven:maven-settings:jar:3.0.4
[DEBUG]   Excluded: org.apache.maven:maven-settings-builder:jar:3.0.4
[DEBUG]   Excluded: org.apache.maven:maven-repository-metadata:jar:3.0.4
[DEBUG]   Excluded: org.apache.maven:maven-model-builder:jar:3.0.4
[DEBUG]   Excluded: org.apache.maven:maven-aether-provider:jar:3.0.4
[DEBUG]   Excluded: org.sonatype.aether:aether-spi:jar:1.13.1
[DEBUG]   Excluded: org.sonatype.aether:aether-impl:jar:1.13.1
[DEBUG]   Excluded: org.sonatype.aether:aether-api:jar:1.13.1
[DEBUG]   Excluded: org.codehaus.plexus:plexus-classworlds:jar:2.4
[DEBUG] Configuring mojo org.alfresco.maven.plugin:alfresco-maven-plugin:2.0.0:set-version from plugin realm ClassRealm[plugin>org.alfresco.maven.plugin:alfresco-maven-plugin:2.0.0, parent: sun.misc.Launcher$AppClassLoader@71b456f]
[DEBUG] Configuring mojo 'org.alfresco.maven.plugin:alfresco-maven-plugin:2.0.0:set-version' with basic configurator -->
[DEBUG]   (f) project = MavenProject: com.netprojex:content-all-share:1.0-SNAPSHOT @ /home/programues/Documents/metadata_myoffice/content-all-share/pom.xml
[DEBUG]   (f) propertyName = noSnapshotVersion
[DEBUG]   (f) snapshotSuffix = -SNAPSHOT
[DEBUG]   (f) snapshotToTimestamp = true
[DEBUG]   (f) version = 1.0-SNAPSHOT
[DEBUG] -- end configuration --
[INFO] Removed -SNAPSHOT suffix from version - 1.0
[INFO] Added timestamp to version - 1.0.1702271512
[INFO] 
[INFO] --- maven-resources-plugin:2.7:resources (default-resources) @ content-all-share ---
[DEBUG] org.apache.maven.plugins:maven-resources-plugin:jar:2.7:
[DEBUG]    org.apache.maven:maven-plugin-api:jar:2.2.1:compile
[DEBUG]    org.apache.maven:maven-project:jar:2.2.1:compile
[DEBUG]       org.apache.maven:maven-profile:jar:2.2.1:compile
[DEBUG]       org.apache.maven:maven-artifact-manager:jar:2.2.1:compile
[DEBUG]          backport-util-concurrent:backport-util-concurrent:jar:3.1:compile
[DEBUG]       org.apache.maven:maven-plugin-registry:jar:2.2.1:compile
[DEBUG]       org.codehaus.plexus:plexus-container-default:jar:1.0-alpha-9-stable-1:compile
[DEBUG]          junit:junit:jar:3.8.1:compile
[DEBUG]    org.apache.maven:maven-core:jar:2.2.1:compile
[DEBUG]       org.apache.maven:maven-plugin-parameter-documenter:jar:2.2.1:compile
[DEBUG]       org.slf4j:slf4j-jdk14:jar:1.5.6:runtime
[DEBUG]          org.slf4j:slf4j-api:jar:1.5.6:runtime
[DEBUG]       org.slf4j:jcl-over-slf4j:jar:1.5.6:runtime
[DEBUG]       org.apache.maven.reporting:maven-reporting-api:jar:2.2.1:compile
[DEBUG]          org.apache.maven.doxia:doxia-sink-api:jar:1.1:compile
[DEBUG]          org.apache.maven.doxia:doxia-logging-api:jar:1.1:compile
[DEBUG]       org.apache.maven:maven-repository-metadata:jar:2.2.1:compile
[DEBUG]       org.apache.maven:maven-error-diagnostics:jar:2.2.1:compile
[DEBUG]       commons-cli:commons-cli:jar:1.2:compile
[DEBUG]       org.apache.maven:maven-plugin-descriptor:jar:2.2.1:compile
[DEBUG]       org.codehaus.plexus:plexus-interactivity-api:jar:1.0-alpha-4:compile
[DEBUG]       classworlds:classworlds:jar:1.1:compile
[DEBUG]       org.sonatype.plexus:plexus-sec-dispatcher:jar:1.3:compile
[DEBUG]          org.sonatype.plexus:plexus-cipher:jar:1.4:compile
[DEBUG]    org.apache.maven:maven-artifact:jar:2.2.1:compile
[DEBUG]    org.apache.maven:maven-settings:jar:2.2.1:compile
[DEBUG]    org.apache.maven:maven-model:jar:2.2.1:compile
[DEBUG]    org.apache.maven:maven-monitor:jar:2.2.1:compile
[DEBUG]    org.codehaus.plexus:plexus-utils:jar:3.0.15:compile
[DEBUG]    org.apache.maven.shared:maven-filtering:jar:1.2:compile
[DEBUG]       org.apache.maven.shared:maven-shared-utils:jar:0.3:compile
[DEBUG]          com.google.code.findbugs:jsr305:jar:2.0.1:compile
[DEBUG]       org.sonatype.plexus:plexus-build-api:jar:0.0.4:compile
[DEBUG]    org.codehaus.plexus:plexus-interpolation:jar:1.19:compile
[DEBUG] Created new class realm plugin>org.apache.maven.plugins:maven-resources-plugin:2.7
[DEBUG] Importing foreign packages into class realm plugin>org.apache.maven.plugins:maven-resources-plugin:2.7
[DEBUG]   Imported:  < project>com.netprojex:content-all-share:1.0-SNAPSHOT
[DEBUG] Populating class realm plugin>org.apache.maven.plugins:maven-resources-plugin:2.7
[DEBUG]   Included: org.apache.maven.plugins:maven-resources-plugin:jar:2.7
[DEBUG]   Included: backport-util-concurrent:backport-util-concurrent:jar:3.1
[DEBUG]   Included: junit:junit:jar:3.8.1
[DEBUG]   Included: org.slf4j:slf4j-jdk14:jar:1.5.6
[DEBUG]   Included: org.slf4j:slf4j-api:jar:1.5.6
[DEBUG]   Included: org.slf4j:jcl-over-slf4j:jar:1.5.6
[DEBUG]   Included: org.apache.maven.reporting:maven-reporting-api:jar:2.2.1
[DEBUG]   Included: org.apache.maven.doxia:doxia-sink-api:jar:1.1
[DEBUG]   Included: org.apache.maven.doxia:doxia-logging-api:jar:1.1
[DEBUG]   Included: commons-cli:commons-cli:jar:1.2
[DEBUG]   Included: org.codehaus.plexus:plexus-interactivity-api:jar:1.0-alpha-4
[DEBUG]   Included: org.sonatype.plexus:plexus-sec-dispatcher:jar:1.3
[DEBUG]   Included: org.sonatype.plexus:plexus-cipher:jar:1.4
[DEBUG]   Included: org.codehaus.plexus:plexus-utils:jar:3.0.15
[DEBUG]   Included: org.apache.maven.shared:maven-filtering:jar:1.2
[DEBUG]   Included: org.apache.maven.shared:maven-shared-utils:jar:0.3
[DEBUG]   Included: com.google.code.findbugs:jsr305:jar:2.0.1
[DEBUG]   Included: org.sonatype.plexus:plexus-build-api:jar:0.0.4
[DEBUG]   Included: org.codehaus.plexus:plexus-interpolation:jar:1.19
[DEBUG]   Excluded: org.apache.maven:maven-plugin-api:jar:2.2.1
[DEBUG]   Excluded: org.apache.maven:maven-project:jar:2.2.1
[DEBUG]   Excluded: org.apache.maven:maven-profile:jar:2.2.1
[DEBUG]   Excluded: org.apache.maven:maven-artifact-manager:jar:2.2.1
[DEBUG]   Excluded: org.apache.maven:maven-plugin-registry:jar:2.2.1
[DEBUG]   Excluded: org.codehaus.plexus:plexus-container-default:jar:1.0-alpha-9-stable-1
[DEBUG]   Excluded: org.apache.maven:maven-core:jar:2.2.1
[DEBUG]   Excluded: org.apache.maven:maven-plugin-parameter-documenter:jar:2.2.1
[DEBUG]   Excluded: org.apache.maven:maven-repository-metadata:jar:2.2.1
[DEBUG]   Excluded: org.apache.maven:maven-error-diagnostics:jar:2.2.1
[DEBUG]   Excluded: org.apache.maven:maven-plugin-descriptor:jar:2.2.1
[DEBUG]   Excluded: classworlds:classworlds:jar:1.1
[DEBUG]   Excluded: org.apache.maven:maven-artifact:jar:2.2.1
[DEBUG]   Excluded: org.apache.maven:maven-settings:jar:2.2.1
[DEBUG]   Excluded: org.apache.maven:maven-model:jar:2.2.1
[DEBUG]   Excluded: org.apache.maven:maven-monitor:jar:2.2.1
[DEBUG] Configuring mojo org.apache.maven.plugins:maven-resources-plugin:2.7:resources from plugin realm ClassRealm[plugin>org.apache.maven.plugins:maven-resources-plugin:2.7, parent: sun.misc.Launcher$AppClassLoader@71b456f]
[DEBUG] Configuring mojo 'org.apache.maven.plugins:maven-resources-plugin:2.7:resources' with basic configurator -->
[DEBUG]   (f) buildFilters = []
[DEBUG]   (f) encoding = UTF-8
[DEBUG]   (f) escapeWindowsPaths = true
[DEBUG]   (s) includeEmptyDirs = false
[DEBUG]   (f) nonFilteredFileExtensions = [ftl, acp, jpg, png, gif, svg, pdf, doc, docx, xls, xlsx, ppt, pptx, bin, lic, swf, zip, msg, jar, ttf, eot, woff, woff2]
[DEBUG]   (s) outputDirectory = /home/programues/Documents/metadata_myoffice/content-all-share/target/classes
[DEBUG]   (s) overwrite = false
[DEBUG]   (f) project = MavenProject: com.netprojex:content-all-share:1.0-SNAPSHOT @ /home/programues/Documents/metadata_myoffice/content-all-share/pom.xml
[DEBUG]   (s) resources = [Resource {targetPath: null, filtering: true, FileSet {directory: /home/programues/Documents/metadata_myoffice/content-all-share/src/main/resources, PatternSet [includes: {}, excludes: {}]}}, Resource {targetPath: ../content-all-share, filtering: true, FileSet {directory: /home/programues/Documents/metadata_myoffice/content-all-share/src/main/amp, PatternSet [includes: {}, excludes: {}]}}]
[DEBUG]   (f) session = org.apache.maven.execution.MavenSession@21b4a7
[DEBUG]   (f) supportMultiLineFiltering = false
[DEBUG]   (f) useBuildFilters = true
[DEBUG]   (s) useDefaultDelimiters = true
[DEBUG] -- end configuration --
[DEBUG] properties used {env.OLDPWD=/home/programues/Documents/metadata_myoffice/content-all-share, file.encoding.pkg=sun.io, java.home=/usr/lib/jvm/java-7-openjdk-amd64/jre, app.testing.jvm.args=-Xms256m -Xmx1524m -XX:MaxPermSize=256m -Duser.language=en, stagingDirectory=../../target/staging/poms/alfresco-sdk-parent, env.DISPLAY=:0.0, env.LS_COLORS=rs=0:di=01;34:ln=01;36:mh=00:pi=40;33:so=01;35:do=01;35:bd=40;33;01:cd=40;33;01:or=40;31;01:su=37;41:sg=30;43:ca=30;41:tw=30;42:ow=34;42:st=37;44:ex=01;32:*.tar=01;31:*.tgz=01;31:*.arj=01;31:*.taz=01;31:*.lzh=01;31:*.lzma=01;31:*.tlz=01;31:*.txz=01;31:*.zip=01;31:*.z=01;31:*.Z=01;31:*.dz=01;31:*.gz=01;31:*.lz=01;31:*.xz=01;31:*.bz2=01;31:*.bz=01;31:*.tbz=01;31:*.tbz2=01;31:*.tz=01;31:*.deb=01;31:*.rpm=01;31:*.jar=01;31:*.war=01;31:*.ear=01;31:*.sar=01;31:*.rar=01;31:*.ace=01;31:*.zoo=01;31:*.cpio=01;31:*.7z=01;31:*.rz=01;31:*.jpg=01;35:*.jpeg=01;35:*.gif=01;35:*.bmp=01;35:*.pbm=01;35:*.pgm=01;35:*.ppm=01;35:*.tga=01;35:*.xbm=01;35:*.xpm=01;35:*.tif=01;35:*.tiff=01;35:*.png=01;35:*.svg=01;35:*.svgz=01;35:*.mng=01;35:*.pcx=01;35:*.mov=01;35:*.mpg=01;35:*.mpeg=01;35:*.m2v=01;35:*.mkv=01;35:*.webm=01;35:*.ogm=01;35:*.mp4=01;35:*.m4v=01;35:*.mp4v=01;35:*.vob=01;35:*.qt=01;35:*.nuv=01;35:*.wmv=01;35:*.asf=01;35:*.rm=01;35:*.rmvb=01;35:*.flc=01;35:*.avi=01;35:*.fli=01;35:*.flv=01;35:*.gl=01;35:*.dl=01;35:*.xcf=01;35:*.xwd=01;35:*.yuv=01;35:*.cgm=01;35:*.emf=01;35:*.axv=01;35:*.anx=01;35:*.ogv=01;35:*.ogx=01;35:*.aac=00;36:*.au=00;36:*.flac=00;36:*.mid=00;36:*.midi=00;36:*.mka=00;36:*.mp3=00;36:*.mpc=00;36:*.ogg=00;36:*.ra=00;36:*.wav=00;36:*.axa=00;36:*.oga=00;36:*.spx=00;36:*.xspf=00;36:, env.XDG_VTNR=7, alfresco.client.war=share, alfresco.client.war.version=5.0.c, classworlds.conf=/usr/share/maven/bin/m2.conf, env.SUDO_USER=programues, app.log.root.level=WARN, java.endorsed.dirs=/usr/lib/jvm/java-7-openjdk-amd64/jre/lib/endorsed, env.LOGNAME=root, alfresco.db.password=alfresco, env.USERNAME=root, maven.jar.version=2.5, app.amp.excludes=, env.XDG_SEAT=seat0, maven.war.version=2.5, env.LC_PAPER=cs_CZ.UTF-8, sun.os.patch.level=unknown, java.vendor.url=http://java.oracle.com/, maven.tomcat.port=8081, maven.dependency.version=2.9, maven.resources.version=2.7, java.version=1.7.0_101, env.ANDROID_HOME=/op/adt/sdk/, env.MAVEN_OPTS=-javaagent:/root/.m2/repository/org/springframework/springloaded/1.2.0.RELEASE/springloaded-1.2.0.RELEASE.jar -noverify, java.vendor.url.bug=http://bugreport.sun.com/bugreport/, env.LANGUAGE=en_US, skipTests=true, alfresco.groupId=org.alfresco, maven.alfresco.version=2.0.0, user.name=root, env.LANG=en_US.UTF-8, env.LC_MONETARY=cs_CZ.UTF-8, sun.io.unicode.encoding=UnicodeLittle, sun.jnu.encoding=UTF-8, java.runtime.name=OpenJDK Runtime Environment, env.LC_NAME=cs_CZ.UTF-8, java.specification.name=Java Platform API Specification, user.timezone=, env.LESSOPEN=| /usr/bin/lesspipe %s, path.separator=:, alfresco.db.url=jdbc:h2:./alf_data_dev/h2_data/alf_dev;MODE=PostgreSQL;AUTO_SERVER=TRUE;DB_CLOSE_ON_EXIT=FALSE;LOCK_TIMEOUT=10000;MVCC=FALSE;LOCK_MODE=0, file.encoding=UTF-8, env.HOME=/root, app.amp.output.folder=../content-all-share, sun.java.command=org.codehaus.plexus.classworlds.launcher.Launcher integration-test -Pamp-to-war -X, maven.install.version=2.5.2, env.LC_TELEPHONE=cs_CZ.UTF-8, env._=/usr/bin/mvn, app.filtering.enabled=true, java.io.tmpdir=/tmp, env.LC_IDENTIFICATION=cs_CZ.UTF-8, env.ANRDOID_NDK_ROOT=/home/programues/Android/android-ndk-r12b/, user.language=en, app.slf4j.scope=runtime, env.NDK=/home/programues/Android/android-ndk-r12b/, line.separator=
, app.amp.overlay.excludes=, app.log.dir=/home/programues/Documents/metadata_myoffice/content-all-share/target/, env=local, alfresco.community.default.version=5.0.c, alfresco.data.location=alf_data_dev, maven.tomcat.version=2.2, java.vm.info=mixed mode, maven.buildhelper.version=1.9.1, alfresco.db.username=alfresco, java.vm.specification.name=Java Virtual Machine Specification, alfresco.client.war.folder=/home/programues/Documents/metadata_myoffice/content-all-share/target/content-all-share-war, env.COLORTERM=xfce4-terminal, app.slf4j.version=1.5.11, env.LC_MEASUREMENT=cs_CZ.UTF-8, alfresco.repo.url=http://localhost:8080/alfresco, env.LC_TIME=cs_CZ.UTF-8, java.awt.printerjob=sun.print.PSPrinterJob, maven.jetty.version=7.4.0.v20110414, alfresco.client.contextPath=/share, maven.clean.version=2.6.1, os.name=Linux, java.specification.vendor=Oracle Corporation, alfresco.db.params=MODE=PostgreSQL;AUTO_SERVER=TRUE;DB_CLOSE_ON_EXIT=FALSE;LOCK_TIMEOUT=10000;MVCC=FALSE;LOCK_MODE=0, alfresco.repo.artifactId=alfresco, env.TERM=xterm, java.vm.name=OpenJDK 64-Bit Server VM, java.library.path=/usr/java/packages/lib/amd64:/usr/lib/x86_64-linux-gnu/jni:/lib/x86_64-linux-gnu:/usr/lib/x86_64-linux-gnu:/usr/lib/jni:/lib:/usr/lib, env.PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/games:/usr/local/games:/opt/android-sdk/sdk/tools:/opt/android-sdk/sdk/platform-tools, env.LESSCLOSE=/usr/bin/lesspipe %s %s, alfresco.client.war.groupId=org.alfresco, alfresco.version=5.0.c, java.class.version=51.0, env.SHLVL=2, app.properties.test.include=**, alfresco.db.name=alf_dev, maven.compiler.version=3.2, sun.boot.library.path=/usr/lib/jvm/java-7-openjdk-amd64/jre/lib/amd64, project.build.sourceEncoding=UTF-8, app.amp.folder=src/main/amp, sun.management.compiler=HotSpot 64-Bit Tiered Compilers, java.awt.graphicsenv=sun.awt.X11GraphicsEnvironment, maven.enforcer.plugin=1.3.1, env.USER=root, junit.version=4.11, alfresco.sdk.parent.version=1.0-SNAPSHOT, java.vm.specification.version=1.7, springloaded.version=1.2.0.RELEASE, awt.toolkit=sun.awt.X11.XToolkit, sun.cpu.isalist=, alfresco.db.datasource.class=org.h2.jdbcx.JdbcDataSource, java.ext.dirs=/usr/lib/jvm/java-7-openjdk-amd64/jre/lib/ext:/usr/java/packages/lib/ext, os.version=3.19.0-64-generic, user.home=/root, maven.release.version=2.5.1, java.vm.vendor=Oracle Corporation, env.JAVA_HOME=/usr/lib/jvm/java-7-openjdk-amd64, env.SUDO_COMMAND=/bin/su, env.XDG_SESSION_ID=c1, env.XAUTHORITY=/home/programues/.Xauthority, app.properties.test.folder=src/test/properties/local, user.dir=/home/programues/Documents/metadata_myoffice/content-all-share, app.properties.include=**, env.MAIL=/var/mail/root, env.NDK_TARGET=arm-linux-androideabi-4.9, env.PWD=/home/programues/Documents/metadata_myoffice/content-all-share, sun.cpu.endian=little, maven.antrun.version=1.7, java.vm.version=24.95-b01, java.class.path=/usr/share/maven/boot/plexus-classworlds-2.x.jar:/root/.m2/repository/org/springframework/springloaded/1.2.0.RELEASE/springloaded-1.2.0.RELEASE.jar, env.LC_ADDRESS=cs_CZ.UTF-8, os.arch=amd64, maven.build.version=Apache Maven 3.0.5, env.LC_NUMERIC=cs_CZ.UTF-8, env.SUDO_UID=1000, sun.java.launcher=SUN_STANDARD, alfresco.enterprise.default.version=5.0, java.vm.specification.vendor=Oracle Corporation, noSnapshotVersion=1.0.1702271512, file.separator=/, h2.version=1.4.182, java.runtime.version=1.7.0_101-b00, sun.boot.class.path=/usr/lib/jvm/java-7-openjdk-amd64/jre/lib/resources.jar:/usr/lib/jvm/java-7-openjdk-amd64/jre/lib/rt.jar:/usr/lib/jvm/java-7-openjdk-amd64/jre/lib/sunrsasign.jar:/usr/lib/jvm/java-7-openjdk-amd64/jre/lib/jsse.jar:/usr/lib/jvm/java-7-openjdk-amd64/jre/lib/jce.jar:/usr/lib/jvm/java-7-openjdk-amd64/jre/lib/charsets.jar:/usr/lib/jvm/java-7-openjdk-amd64/jre/lib/rhino.jar:/usr/lib/jvm/java-7-openjdk-amd64/jre/lib/jfr.jar:/usr/lib/jvm/java-7-openjdk-amd64/jre/classes, maven.replacer.version=1.5.3, scm.url.base=https://github.com/Alfresco/alfresco-sdk, maven.surefire.version=2.18, maven.version=3.0.5, maven.archetype.version=2.2, user.country=US, env.SHELL=/bin/bash, maven.home=/usr/share/maven, alfresco.share.artifactId=share, java.vendor=Oracle Corporation, app.properties.folder=src/main/properties/local, java.specification.version=1.7, env.SUDO_GID=1000, sun.arch.data.model=64}
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[DEBUG] resource with targetPath null
directory /home/programues/Documents/metadata_myoffice/content-all-share/src/main/resources
excludes []
includes []
[DEBUG] ignoreDelta true
[INFO] Copying 3 resources
[DEBUG] file share-config-custom.xml.sample has a filtered file extension
[DEBUG] filtering /home/programues/Documents/metadata_myoffice/content-all-share/src/main/resources/META-INF/share-config-custom.xml.sample to /home/programues/Documents/metadata_myoffice/content-all-share/target/classes/META-INF/share-config-custom.xml.sample
[DEBUG] file test.html has a filtered file extension
[DEBUG] filtering /home/programues/Documents/metadata_myoffice/content-all-share/src/main/resources/META-INF/resources/test.html to /home/programues/Documents/metadata_myoffice/content-all-share/target/classes/META-INF/resources/test.html
[DEBUG] file share-config-custom.xml has a filtered file extension
[DEBUG] filtering /home/programues/Documents/metadata_myoffice/content-all-share/src/main/resources/META-INF/share-config-custom.xml to /home/programues/Documents/metadata_myoffice/content-all-share/target/classes/META-INF/share-config-custom.xml
[DEBUG] resource with targetPath ../content-all-share
directory /home/programues/Documents/metadata_myoffice/content-all-share/src/main/amp
excludes []
includes []
[DEBUG] ignoreDelta true
[INFO] Copying 65 resources to ../content-all-share
[DEBUG] file alfresco-imagelib.png has a non filtered file extension
[DEBUG] copy /home/programues/Documents/metadata_myoffice/content-all-share/src/main/amp/web/themes/uTheme/images/alfresco-imagelib.png to /home/programues/Documents/metadata_myoffice/content-all-share/target/classes/../content-all-share/web/themes/uTheme/images/alfresco-imagelib.png
[DEBUG] file wait.gif has a non filtered file extension
[DEBUG] copy /home/programues/Documents/metadata_myoffice/content-all-share/src/main/amp/web/themes/uTheme/images/wait.gif to /home/programues/Documents/metadata_myoffice/content-all-share/target/classes/../content-all-share/web/themes/uTheme/images/wait.gif
[DEBUG] file upload-button-sprite.png has a non filtered file extension
[DEBUG] copy /home/programues/Documents/metadata_myoffice/content-all-share/src/main/amp/web/themes/uTheme/images/upload-button-sprite.png to /home/programues/Documents/metadata_myoffice/content-all-share/target/classes/../content-all-share/web/themes/uTheme/images/upload-button-sprite.png
[DEBUG] file alfresco-logo-bg.jpg has a non filtered file extension
[DEBUG] copy /home/programues/Documents/metadata_myoffice/content-all-share/src/main/amp/web/themes/uTheme/images/alfresco-logo-bg.jpg to /home/programues/Documents/metadata_myoffice/content-all-share/target/classes/../content-all-share/web/themes/uTheme/images/alfresco-logo-bg.jpg
[DEBUG] file collapsed.png has a non filtered file extension
[DEBUG] copy /home/programues/Documents/metadata_myoffice/content-all-share/src/main/amp/web/themes/uTheme/images/collapsed.png to /home/programues/Documents/metadata_myoffice/content-all-share/target/classes/../content-all-share/web/themes/uTheme/images/collapsed.png
[DEBUG] file categoryview-sprite.gif has a non filtered file extension
[DEBUG] copy /home/programues/Documents/metadata_myoffice/content-all-share/src/main/amp/web/themes/uTheme/images/categoryview-sprite.gif to /home/programues/Documents/metadata_myoffice/content-all-share/target/classes/../content-all-share/web/themes/uTheme/images/categoryview-sprite.gif
[DEBUG] file app-logo-48.png has a non filtered file extension
[DEBUG] copy /home/programues/Documents/metadata_myoffice/content-all-share/src/main/amp/web/themes/uTheme/images/app-logo-48.png to /home/programues/Documents/metadata_myoffice/content-all-share/target/classes/../content-all-share/web/themes/uTheme/images/app-logo-48.png
[DEBUG] file app-logo.png has a non filtered file extension
[DEBUG] copy /home/programues/Documents/metadata_myoffice/content-all-share/src/main/amp/web/themes/uTheme/images/app-logo.png to /home/programues/Documents/metadata_myoffice/content-all-share/target/classes/../content-all-share/web/themes/uTheme/images/app-logo.png
[DEBUG] file treeview-sprite.gif has a non filtered file extension
[DEBUG] copy /home/programues/Documents/metadata_myoffice/content-all-share/src/main/amp/web/themes/uTheme/images/treeview-sprite.gif to /home/programues/Documents/metadata_myoffice/content-all-share/target/classes/../content-all-share/web/themes/uTheme/images/treeview-sprite.gif
[DEBUG] file logo.png has a non filtered file extension
[DEBUG] copy /home/programues/Documents/metadata_myoffice/content-all-share/src/main/amp/web/themes/uTheme/images/logo.png to /home/programues/Documents/metadata_myoffice/content-all-share/target/classes/../content-all-share/web/themes/uTheme/images/logo.png
[DEBUG] file app-logo-share.png has a non filtered file extension
[DEBUG] copy /home/programues/Documents/metadata_myoffice/content-all-share/src/main/amp/web/themes/uTheme/images/app-logo-share.png to /home/programues/Documents/metadata_myoffice/content-all-share/target/classes/../content-all-share/web/themes/uTheme/images/app-logo-share.png
[DEBUG] file link-menu-button-arrow.png has a non filtered file extension
[DEBUG] copy /home/programues/Documents/metadata_myoffice/content-all-share/src/main/amp/web/themes/uTheme/images/link-menu-button-arrow.png to /home/programues/Documents/metadata_myoffice/content-all-share/target/classes/../content-all-share/web/themes/uTheme/images/link-menu-button-arrow.png
[DEBUG] file expanded.png has a non filtered file extension
[DEBUG] copy /home/programues/Documents/metadata_myoffice/content-all-share/src/main/amp/web/themes/uTheme/images/expanded.png to /home/programues/Documents/metadata_myoffice/content-all-share/target/classes/../content-all-share/web/themes/uTheme/images/expanded.png
[DEBUG] file alfresco-linklib.png has a non filtered file extension
[DEBUG] copy /home/programues/Documents/metadata_myoffice/content-all-share/src/main/amp/web/themes/uTheme/images/alfresco-linklib.png to /home/programues/Documents/metadata_myoffice/content-all-share/target/classes/../content-all-share/web/themes/uTheme/images/alfresco-linklib.png
[DEBUG] file feed-icon-16.png has a non filtered file extension
[DEBUG] copy /home/programues/Documents/metadata_myoffice/content-all-share/src/main/amp/web/themes/uTheme/images/feed-icon-16.png to /home/programues/Documents/metadata_myoffice/content-all-share/target/classes/../content-all-share/web/themes/uTheme/images/feed-icon-16.png
[DEBUG] file sprite.png has a non filtered file extension
[DEBUG] copy /home/programues/Documents/metadata_myoffice/content-all-share/src/main/amp/web/themes/uTheme/images/sprite.png to /home/programues/Documents/metadata_myoffice/content-all-share/target/classes/../content-all-share/web/themes/uTheme/images/sprite.png
[DEBUG] file presentation.css has a filtered file extension
[DEBUG] filtering /home/programues/Documents/metadata_myoffice/content-all-share/src/main/amp/web/themes/uTheme/presentation.css to /home/programues/Documents/metadata_myoffice/content-all-share/target/classes/../content-all-share/web/themes/uTheme/presentation.css
[DEBUG] file wait.gif has a non filtered file extension
[DEBUG] copy /home/programues/Documents/metadata_myoffice/content-all-share/src/main/amp/web/themes/uTheme/yui/assets/wait.gif to /home/programues/Documents/metadata_myoffice/content-all-share/target/classes/../content-all-share/web/themes/uTheme/yui/assets/wait.gif
[DEBUG] file split-button-arrow.png has a non filtered file extension
[DEBUG] copy /home/programues/Documents/metadata_myoffice/content-all-share/src/main/amp/web/themes/uTheme/yui/assets/split-button-arrow.png to /home/programues/Documents/metadata_myoffice/content-all-share/target/classes/../content-all-share/web/themes/uTheme/yui/assets/split-button-arrow.png
[DEBUG] file split-button-arrow-focus.png has a non filtered file extension
[DEBUG] copy /home/programues/Documents/metadata_myoffice/content-all-share/src/main/amp/web/themes/uTheme/yui/assets/split-button-arrow-focus.png to /home/programues/Documents/metadata_myoffice/content-all-share/target/classes/../content-all-share/web/themes/uTheme/yui/assets/split-button-arrow-focus.png
[DEBUG] file editor-sprite-active.gif has a non filtered file extension
[DEBUG] copy /home/programues/Documents/metadata_myoffice/content-all-share/src/main/amp/web/themes/uTheme/yui/assets/editor-sprite-active.gif to /home/programues/Documents/metadata_myoffice/content-all-share/target/classes/../content-all-share/web/themes/uTheme/yui/assets/editor-sprite-active.gif
[DEBUG] file editor-knob.gif has a non filtered file extension
[DEBUG] copy /home/programues/Documents/metadata_myoffice/content-all-share/src/main/amp/web/themes/uTheme/yui/assets/editor-knob.gif to /home/programues/Documents/metadata_myoffice/content-all-share/target/classes/../content-all-share/web/themes/uTheme/yui/assets/editor-knob.gif
[DEBUG] file menubaritem_submenuindicator_disabled.png has a non filtered file extension
[DEBUG] copy /home/programues/Documents/metadata_myoffice/content-all-share/src/main/amp/web/themes/uTheme/yui/assets/menubaritem_submenuindicator_disabled.png to /home/programues/Documents/metadata_myoffice/content-all-share/target/classes/../content-all-share/web/themes/uTheme/yui/assets/menubaritem_submenuindicator_disabled.png
[DEBUG] file treeview-loading.gif has a non filtered file extension
[DEBUG] copy /home/programues/Documents/metadata_myoffice/content-all-share/src/main/amp/web/themes/uTheme/yui/assets/treeview-loading.gif to /home/programues/Documents/metadata_myoffice/content-all-share/target/classes/../content-all-share/web/themes/uTheme/yui/assets/treeview-loading.gif
[DEBUG] file split-button-arrow-active.png has a non filtered file extension
[DEBUG] copy /home/programues/Documents/metadata_myoffice/content-all-share/src/main/amp/web/themes/uTheme/yui/assets/split-button-arrow-active.png to /home/programues/Documents/metadata_myoffice/content-all-share/target/classes/../content-all-share/web/themes/uTheme/yui/assets/split-button-arrow-active.png
[DEBUG] file skin.css has a filtered file extension
[DEBUG] filtering /home/programues/Documents/metadata_myoffice/content-all-share/src/main/amp/web/themes/uTheme/yui/assets/skin.css to /home/programues/Documents/metadata_myoffice/content-all-share/target/classes/../content-all-share/web/themes/uTheme/yui/assets/skin.css
[DEBUG] file transparent.gif has a non filtered file extension
[DEBUG] copy /home/programues/Documents/metadata_myoffice/content-all-share/src/main/amp/web/themes/uTheme/yui/assets/transparent.gif to /home/programues/Documents/metadata_myoffice/content-all-share/target/classes/../content-all-share/web/themes/uTheme/yui/assets/transparent.gif
[DEBUG] file editor-sprite.gif has a non filtered file extension
[DEBUG] copy /home/programues/Documents/metadata_myoffice/content-all-share/src/main/amp/web/themes/uTheme/yui/assets/editor-sprite.gif to /home/programues/Documents/metadata_myoffice/content-all-share/target/classes/../content-all-share/web/themes/uTheme/yui/assets/editor-sprite.gif
[DEBUG] file menuitem_checkbox.png has a non filtered file extension
[DEBUG] copy /home/programues/Documents/metadata_myoffice/content-all-share/src/main/amp/web/themes/uTheme/yui/assets/menuitem_checkbox.png to /home/programues/Documents/metadata_myoffice/content-all-share/target/classes/../content-all-share/web/themes/uTheme/yui/assets/menuitem_checkbox.png
[DEBUG] file toolbar-checked-gradient.gif has a non filtered file extension
[DEBUG] copy /home/programues/Documents/metadata_myoffice/content-all-share/src/main/amp/web/themes/uTheme/yui/assets/toolbar-checked-gradient.gif to /home/programues/Documents/metadata_myoffice/content-all-share/target/classes/../content-all-share/web/themes/uTheme/yui/assets/toolbar-checked-gradient.gif
[DEBUG] file asc.gif has a non filtered file extension
[DEBUG] copy /home/programues/Documents/metadata_myoffice/content-all-share/src/main/amp/web/themes/uTheme/yui/assets/asc.gif to /home/programues/Documents/metadata_myoffice/content-all-share/target/classes/../content-all-share/web/themes/uTheme/yui/assets/asc.gif
[DEBUG] file layout_sprite.png has a non filtered file extension
[DEBUG] copy /home/programues/Documents/metadata_myoffice/content-all-share/src/main/amp/web/themes/uTheme/yui/assets/layout_sprite.png to /home/programues/Documents/metadata_myoffice/content-all-share/target/classes/../content-all-share/web/themes/uTheme/yui/assets/layout_sprite.png
[DEBUG] file menuitem_checkbox_disabled.png has a non filtered file extension
[DEBUG] copy /home/programues/Documents/metadata_myoffice/content-all-share/src/main/amp/web/themes/uTheme/yui/assets/menuitem_checkbox_disabled.png to /home/programues/Documents/metadata_myoffice/content-all-share/target/classes/../content-all-share/web/themes/uTheme/yui/assets/menuitem_checkbox_disabled.png
[DEBUG] file loading.gif has a non filtered file extension
[DEBUG] copy /home/programues/Documents/metadata_myoffice/content-all-share/src/main/amp/web/themes/uTheme/yui/assets/loading.gif to /home/programues/Documents/metadata_myoffice/content-all-share/target/classes/../content-all-share/web/themes/uTheme/yui/assets/loading.gif
[DEBUG] file bg-v.gif has a non filtered file extension
[DEBUG] copy /home/programues/Documents/metadata_myoffice/content-all-share/src/main/amp/web/themes/uTheme/yui/assets/bg-v.gif to /home/programues/Documents/metadata_myoffice/content-all-share/target/classes/../content-all-share/web/themes/uTheme/yui/assets/bg-v.gif
[DEBUG] file picker_mask.png has a non filtered file extension
[DEBUG] copy /home/programues/Documents/metadata_myoffice/content-all-share/src/main/amp/web/themes/uTheme/yui/assets/picker_mask.png to /home/programues/Documents/metadata_myoffice/content-all-share/target/classes/../content-all-share/web/themes/uTheme/yui/assets/picker_mask.png
[DEBUG] file dt-arrow-dn.png has a non filtered file extension
[DEBUG] copy /home/programues/Documents/metadata_myoffice/content-all-share/src/main/amp/web/themes/uTheme/yui/assets/dt-arrow-dn.png to /home/programues/Documents/metadata_myoffice/content-all-share/target/classes/../content-all-share/web/themes/uTheme/yui/assets/dt-arrow-dn.png
[DEBUG] file menubaritem_submenuindicator.png has a non filtered file extension
[DEBUG] copy /home/programues/Documents/metadata_myoffice/content-all-share/src/main/amp/web/themes/uTheme/yui/assets/menubaritem_submenuindicator.png to /home/programues/Documents/metadata_myoffice/content-all-share/target/classes/../content-all-share/web/themes/uTheme/yui/assets/menubaritem_submenuindicator.png
[DEBUG] file bg-h.gif has a non filtered file extension
[DEBUG] copy /home/programues/Documents/metadata_myoffice/content-all-share/src/main/amp/web/themes/uTheme/yui/assets/bg-h.gif to /home/programues/Documents/metadata_myoffice/content-all-share/target/classes/../content-all-share/web/themes/uTheme/yui/assets/bg-h.gif
[DEBUG] file menuitem_submenuindicator.png has a non filtered file extension
[DEBUG] copy /home/programues/Documents/metadata_myoffice/content-all-share/src/main/amp/web/themes/uTheme/yui/assets/menuitem_submenuindicator.png to /home/programues/Documents/metadata_myoffice/content-all-share/target/classes/../content-all-share/web/themes/uTheme/yui/assets/menuitem_submenuindicator.png
[DEBUG] file desc.gif has a non filtered file extension
[DEBUG] copy /home/programues/Documents/metadata_myoffice/content-all-share/src/main/amp/web/themes/uTheme/yui/assets/desc.gif to /home/programues/Documents/metadata_myoffice/content-all-share/target/classes/../content-all-share/web/themes/uTheme/yui/assets/desc.gif
[DEBUG] file treeview-sprite.gif has a non filtered file extension
[DEBUG] copy /home/programues/Documents/metadata_myoffice/content-all-share/src/main/amp/web/themes/uTheme/yui/assets/treeview-sprite.gif to /home/programues/Documents/metadata_myoffice/content-all-share/target/classes/../content-all-share/web/themes/uTheme/yui/assets/treeview-sprite.gif
[DEBUG] file menuitem_submenuindicator_disabled.png has a non filtered file extension
[DEBUG] copy /home/programues/Documents/metadata_myoffice/content-all-share/src/main/amp/web/themes/uTheme/yui/assets/menuitem_submenuindicator_disabled.png to /home/programues/Documents/metadata_myoffice/content-all-share/target/classes/../content-all-share/web/themes/uTheme/yui/assets/menuitem_submenuindicator_disabled.png
[DEBUG] file split-button-arrow-disabled.png has a non filtered file extension
[DEBUG] copy /home/programues/Documents/metadata_myoffice/content-all-share/src/main/amp/web/themes/uTheme/yui/assets/split-button-arrow-disabled.png to /home/programues/Documents/metadata_myoffice/content-all-share/target/classes/../content-all-share/web/themes/uTheme/yui/assets/split-button-arrow-disabled.png
[DEBUG] file header_background.png has a non filtered file extension
[DEBUG] copy /home/programues/Documents/metadata_myoffice/content-all-share/src/main/amp/web/themes/uTheme/yui/assets/header_background.png to /home/programues/Documents/metadata_myoffice/content-all-share/target/classes/../content-all-share/web/themes/uTheme/yui/assets/header_background.png
[DEBUG] file menu-button-arrow-disabled.png has a non filtered file extension
[DEBUG] copy /home/programues/Documents/metadata_myoffice/content-all-share/src/main/amp/web/themes/uTheme/yui/assets/menu-button-arrow-disabled.png to /home/programues/Documents/metadata_myoffice/content-all-share/target/classes/../content-all-share/web/themes/uTheme/yui/assets/menu-button-arrow-disabled.png
[DEBUG] file blankimage.png has a non filtered file extension
[DEBUG] copy /home/programues/Documents/metadata_myoffice/content-all-share/src/main/amp/web/themes/uTheme/yui/assets/blankimage.png to /home/programues/Documents/metadata_myoffice/content-all-share/target/classes/../content-all-share/web/themes/uTheme/yui/assets/blankimage.png
[DEBUG] file dt-arrow-up.png has a non filtered file extension
[DEBUG] copy /home/programues/Documents/metadata_myoffice/content-all-share/src/main/amp/web/themes/uTheme/yui/assets/dt-arrow-up.png to /home/programues/Documents/metadata_myoffice/content-all-share/target/classes/../content-all-share/web/themes/uTheme/yui/assets/dt-arrow-up.png
[DEBUG] file hue_bg.png has a non filtered file extension
[DEBUG] copy /home/programues/Documents/metadata_myoffice/content-all-share/src/main/amp/web/themes/uTheme/yui/assets/hue_bg.png to /home/programues/Documents/metadata_myoffice/content-all-share/target/classes/../content-all-share/web/themes/uTheme/yui/assets/hue_bg.png
[DEBUG] file sprite.png has a non filtered file extension
[DEBUG] copy /home/programues/Documents/metadata_myoffice/content-all-share/src/main/amp/web/themes/uTheme/yui/assets/sprite.png to /home/programues/Documents/metadata_myoffice/content-all-share/target/classes/../content-all-share/web/themes/uTheme/yui/assets/sprite.png
[DEBUG] file menu-button-arrow.png has a non filtered file extension
[DEBUG] copy /home/programues/Documents/metadata_myoffice/content-all-share/src/main/amp/web/themes/uTheme/yui/assets/menu-button-arrow.png to /home/programues/Documents/metadata_myoffice/content-all-share/target/classes/../content-all-share/web/themes/uTheme/yui/assets/menu-button-arrow.png
[DEBUG] file split-button-arrow-hover.png has a non filtered file extension
[DEBUG] copy /home/programues/Documents/metadata_myoffice/content-all-share/src/main/amp/web/themes/uTheme/yui/assets/split-button-arrow-hover.png to /home/programues/Documents/metadata_myoffice/content-all-share/target/classes/../content-all-share/web/themes/uTheme/yui/assets/split-button-arrow-hover.png
[DEBUG] file TemplateWidget.js has a filtered file extension
[DEBUG] filtering /home/programues/Documents/metadata_myoffice/content-all-share/src/main/amp/web/js/example/widgets/TemplateWidget.js to /home/programues/Documents/metadata_myoffice/content-all-share/target/classes/../content-all-share/web/js/example/widgets/TemplateWidget.js
[DEBUG] file TemplateWidget.html has a filtered file extension
[DEBUG] filtering /home/programues/Documents/metadata_myoffice/content-all-share/src/main/amp/web/js/example/widgets/templates/TemplateWidget.html to /home/programues/Documents/metadata_myoffice/content-all-share/target/classes/../content-all-share/web/js/example/widgets/templates/TemplateWidget.html
[DEBUG] file TemplateWidget.css has a filtered file extension
[DEBUG] filtering /home/programues/Documents/metadata_myoffice/content-all-share/src/main/amp/web/js/example/widgets/css/TemplateWidget.css to /home/programues/Documents/metadata_myoffice/content-all-share/target/classes/../content-all-share/web/js/example/widgets/css/TemplateWidget.css
[DEBUG] file TemplateWidget.properties has a filtered file extension
[DEBUG] filtering /home/programues/Documents/metadata_myoffice/content-all-share/src/main/amp/web/js/example/widgets/i18n/TemplateWidget.properties to /home/programues/Documents/metadata_myoffice/content-all-share/target/classes/../content-all-share/web/js/example/widgets/i18n/TemplateWidget.properties
[DEBUG] file uTheme.xml has a filtered file extension
[DEBUG] filtering /home/programues/Documents/metadata_myoffice/content-all-share/src/main/amp/config/alfresco/site-data/themes/uTheme.xml to /home/programues/Documents/metadata_myoffice/content-all-share/target/classes/../content-all-share/config/alfresco/site-data/themes/uTheme.xml
[DEBUG] file baBanca.properties has a filtered file extension
[DEBUG] filtering /home/programues/Documents/metadata_myoffice/content-all-share/src/main/amp/config/alfresco/module/content-all-share/messages/baBanca.properties to /home/programues/Documents/metadata_myoffice/content-all-share/target/classes/../content-all-share/config/alfresco/module/content-all-share/messages/baBanca.properties
[DEBUG] file example-widgets.xml has a filtered file extension
[DEBUG] filtering /home/programues/Documents/metadata_myoffice/content-all-share/src/main/amp/config/alfresco/web-extension/site-data/extensions/example-widgets.xml to /home/programues/Documents/metadata_myoffice/content-all-share/target/classes/../content-all-share/config/alfresco/web-extension/site-data/extensions/example-widgets.xml
[DEBUG] file simple-page.get.desc.xml has a filtered file extension
[DEBUG] filtering /home/programues/Documents/metadata_myoffice/content-all-share/src/main/amp/config/alfresco/web-extension/site-webscripts/com/example/pages/simple-page.get.desc.xml to /home/programues/Documents/metadata_myoffice/content-all-share/target/classes/../content-all-share/config/alfresco/web-extension/site-webscripts/com/example/pages/simple-page.get.desc.xml
[DEBUG] file simple-page.get.js has a filtered file extension
[DEBUG] filtering /home/programues/Documents/metadata_myoffice/content-all-share/src/main/amp/config/alfresco/web-extension/site-webscripts/com/example/pages/simple-page.get.js to /home/programues/Documents/metadata_myoffice/content-all-share/target/classes/../content-all-share/config/alfresco/web-extension/site-webscripts/com/example/pages/simple-page.get.js
[DEBUG] file simple-page.get.html.ftl has a non filtered file extension
[DEBUG] copy /home/programues/Documents/metadata_myoffice/content-all-share/src/main/amp/config/alfresco/web-extension/site-webscripts/com/example/pages/simple-page.get.html.ftl to /home/programues/Documents/metadata_myoffice/content-all-share/target/classes/../content-all-share/config/alfresco/web-extension/site-webscripts/com/example/pages/simple-page.get.html.ftl
[DEBUG] file content-tutorial-share-context.xml has a filtered file extension
[DEBUG] filtering /home/programues/Documents/metadata_myoffice/content-all-share/src/main/amp/config/alfresco/web-extension/content-tutorial-share-context.xml to /home/programues/Documents/metadata_myoffice/content-all-share/target/classes/../content-all-share/config/alfresco/web-extension/content-tutorial-share-context.xml
[DEBUG] file module.properties has a filtered file extension
[DEBUG] filtering /home/programues/Documents/metadata_myoffice/content-all-share/src/main/amp/module.properties to /home/programues/Documents/metadata_myoffice/content-all-share/target/classes/../content-all-share/module.properties
[DEBUG] file file-mapping.properties has a filtered file extension
[DEBUG] filtering /home/programues/Documents/metadata_myoffice/content-all-share/src/main/amp/file-mapping.properties to /home/programues/Documents/metadata_myoffice/content-all-share/target/classes/../content-all-share/file-mapping.properties
[DEBUG] no use filter components
[INFO] 
[INFO] --- maven-compiler-plugin:3.2:compile (default-compile) @ content-all-share ---
[DEBUG] org.apache.maven.plugins:maven-compiler-plugin:jar:3.2:
[DEBUG]    org.apache.maven:maven-plugin-api:jar:2.0.9:compile
[DEBUG]    org.apache.maven:maven-artifact:jar:2.0.9:compile
[DEBUG]       org.codehaus.plexus:plexus-utils:jar:1.5.1:compile
[DEBUG]    org.apache.maven:maven-core:jar:2.0.9:compile
[DEBUG]       org.apache.maven:maven-settings:jar:2.0.9:compile
[DEBUG]       org.apache.maven:maven-plugin-parameter-documenter:jar:2.0.9:compile
[DEBUG]       org.apache.maven:maven-profile:jar:2.0.9:compile
[DEBUG]       org.apache.maven:maven-model:jar:2.0.9:compile
[DEBUG]       org.apache.maven:maven-repository-metadata:jar:2.0.9:compile
[DEBUG]       org.apache.maven:maven-error-diagnostics:jar:2.0.9:compile
[DEBUG]       org.apache.maven:maven-project:jar:2.0.9:compile
[DEBUG]          org.apache.maven:maven-plugin-registry:jar:2.0.9:compile
[DEBUG]       org.apache.maven:maven-plugin-descriptor:jar:2.0.9:compile
[DEBUG]       org.apache.maven:maven-artifact-manager:jar:2.0.9:compile
[DEBUG]       org.apache.maven:maven-monitor:jar:2.0.9:compile
[DEBUG]    org.apache.maven:maven-toolchain:jar:1.0:compile
[DEBUG]    org.apache.maven.shared:maven-shared-utils:jar:0.1:compile
[DEBUG]       com.google.code.findbugs:jsr305:jar:2.0.1:compile
[DEBUG]    org.apache.maven.shared:maven-shared-incremental:jar:1.1:compile
[DEBUG]       org.codehaus.plexus:plexus-component-annotations:jar:1.5.5:compile
[DEBUG]    org.codehaus.plexus:plexus-compiler-api:jar:2.4:compile
[DEBUG]    org.codehaus.plexus:plexus-compiler-manager:jar:2.4:compile
[DEBUG]    org.codehaus.plexus:plexus-compiler-javac:jar:2.4:runtime
[DEBUG]    org.codehaus.plexus:plexus-container-default:jar:1.5.5:compile
[DEBUG]       org.codehaus.plexus:plexus-classworlds:jar:2.2.2:compile
[DEBUG]       org.apache.xbean:xbean-reflect:jar:3.4:compile
[DEBUG]          log4j:log4j:jar:1.2.12:compile
[DEBUG]          commons-logging:commons-logging-api:jar:1.1:compile
[DEBUG]       com.google.collections:google-collections:jar:1.0:compile
[DEBUG]       junit:junit:jar:3.8.2:compile
[DEBUG] Created new class realm plugin>org.apache.maven.plugins:maven-compiler-plugin:3.2
[DEBUG] Importing foreign packages into class realm plugin>org.apache.maven.plugins:maven-compiler-plugin:3.2
[DEBUG]   Imported:  < project>com.netprojex:content-all-share:1.0-SNAPSHOT
[DEBUG] Populating class realm plugin>org.apache.maven.plugins:maven-compiler-plugin:3.2
[DEBUG]   Included: org.apache.maven.plugins:maven-compiler-plugin:jar:3.2
[DEBUG]   Included: org.codehaus.plexus:plexus-utils:jar:1.5.1
[DEBUG]   Included: org.apache.maven.shared:maven-shared-utils:jar:0.1
[DEBUG]   Included: com.google.code.findbugs:jsr305:jar:2.0.1
[DEBUG]   Included: org.apache.maven.shared:maven-shared-incremental:jar:1.1
[DEBUG]   Included: org.codehaus.plexus:plexus-component-annotations:jar:1.5.5
[DEBUG]   Included: org.codehaus.plexus:plexus-compiler-api:jar:2.4
[DEBUG]   Included: org.codehaus.plexus:plexus-compiler-manager:jar:2.4
[DEBUG]   Included: org.codehaus.plexus:plexus-compiler-javac:jar:2.4
[DEBUG]   Included: org.apache.xbean:xbean-reflect:jar:3.4
[DEBUG]   Included: log4j:log4j:jar:1.2.12
[DEBUG]   Included: commons-logging:commons-logging-api:jar:1.1
[DEBUG]   Included: com.google.collections:google-collections:jar:1.0
[DEBUG]   Included: junit:junit:jar:3.8.2
[DEBUG]   Excluded: org.apache.maven:maven-plugin-api:jar:2.0.9
[DEBUG]   Excluded: org.apache.maven:maven-artifact:jar:2.0.9
[DEBUG]   Excluded: org.apache.maven:maven-core:jar:2.0.9
[DEBUG]   Excluded: org.apache.maven:maven-settings:jar:2.0.9
[DEBUG]   Excluded: org.apache.maven:maven-plugin-parameter-documenter:jar:2.0.9
[DEBUG]   Excluded: org.apache.maven:maven-profile:jar:2.0.9
[DEBUG]   Excluded: org.apache.maven:maven-model:jar:2.0.9
[DEBUG]   Excluded: org.apache.maven:maven-repository-metadata:jar:2.0.9
[DEBUG]   Excluded: org.apache.maven:maven-error-diagnostics:jar:2.0.9
[DEBUG]   Excluded: org.apache.maven:maven-project:jar:2.0.9
[DEBUG]   Excluded: org.apache.maven:maven-plugin-registry:jar:2.0.9
[DEBUG]   Excluded: org.apache.maven:maven-plugin-descriptor:jar:2.0.9
[DEBUG]   Excluded: org.apache.maven:maven-artifact-manager:jar:2.0.9
[DEBUG]   Excluded: org.apache.maven:maven-monitor:jar:2.0.9
[DEBUG]   Excluded: org.apache.maven:maven-toolchain:jar:1.0
[DEBUG]   Excluded: org.codehaus.plexus:plexus-container-default:jar:1.5.5
[DEBUG]   Excluded: org.codehaus.plexus:plexus-classworlds:jar:2.2.2
[DEBUG] Configuring mojo org.apache.maven.plugins:maven-compiler-plugin:3.2:compile from plugin realm ClassRealm[plugin>org.apache.maven.plugins:maven-compiler-plugin:3.2, parent: sun.misc.Launcher$AppClassLoader@71b456f]
[DEBUG] Configuring mojo 'org.apache.maven.plugins:maven-compiler-plugin:3.2:compile' with basic configurator -->
[DEBUG]   (f) basedir = /home/programues/Documents/metadata_myoffice/content-all-share
[DEBUG]   (f) buildDirectory = /home/programues/Documents/metadata_myoffice/content-all-share/target
[DEBUG]   (f) classpathElements = [/home/programues/Documents/metadata_myoffice/content-all-share/target/classes, /root/.m2/repository/org/alfresco/share/5.0.c/share-5.0.c-classes.jar, /root/.m2/repository/org/alfresco/alfresco-web-framework-commons/5.0.c/alfresco-web-framework-commons-5.0.c-classes.jar, /root/.m2/repository/org/alfresco/alfresco-jlan-embed/5.0.c/alfresco-jlan-embed-5.0.c.jar, /root/.m2/repository/org/alfresco/alfresco-core/5.0.c/alfresco-core-5.0.c.jar, /root/.m2/repository/commons-codec/commons-codec/1.9/commons-codec-1.9.jar, /root/.m2/repository/commons-httpclient/commons-httpclient/3.1/commons-httpclient-3.1.jar, /root/.m2/repository/org/safehaus/jug/jug/2.0.0/jug-2.0.0-asl.jar, /root/.m2/repository/log4j/log4j/1.2.17/log4j-1.2.17.jar, /root/.m2/repository/org/json/json/20090211/json-20090211.jar, /root/.m2/repository/org/springframework/spring-orm/3.2.10.RELEASE/spring-orm-3.2.10.RELEASE.jar, /root/.m2/repository/org/springframework/spring-beans/3.2.10.RELEASE/spring-beans-3.2.10.RELEASE.jar, /root/.m2/repository/org/springframework/spring-core/3.2.10.RELEASE/spring-core-3.2.10.RELEASE.jar, /root/.m2/repository/org/springframework/spring-jdbc/3.2.10.RELEASE/spring-jdbc-3.2.10.RELEASE.jar, /root/.m2/repository/org/springframework/spring-tx/3.2.10.RELEASE/spring-tx-3.2.10.RELEASE.jar, /root/.m2/repository/org/springframework/extensions/surf/spring-surf-core-configservice/5.0.c/spring-surf-core-configservice-5.0.c.jar, /root/.m2/repository/org/springframework/extensions/surf/spring-surf-core/5.0.c/spring-surf-core-5.0.c.jar, /root/.m2/repository/org/springframework/spring-context/3.2.10.RELEASE/spring-context-3.2.10.RELEASE.jar, /root/.m2/repository/org/springframework/spring-aop/3.2.10.RELEASE/spring-aop-3.2.10.RELEASE.jar, /root/.m2/repository/org/springframework/spring-context-support/3.2.10.RELEASE/spring-context-support-3.2.10.RELEASE.jar, /root/.m2/repository/org/springframework/spring-web/3.2.10.RELEASE/spring-web-3.2.10.RELEASE.jar, /root/.m2/repository/com/sun/xml/bind/jaxb-impl/2.1.11/jaxb-impl-2.1.11.jar, /root/.m2/repository/dom4j/dom4j/1.6.1/dom4j-1.6.1.jar, /root/.m2/repository/xml-apis/xml-apis/1.4.01/xml-apis-1.4.01.jar, /root/.m2/repository/org/codehaus/guessencoding/guessencoding/1.0/guessencoding-1.0.jar, /root/.m2/repository/javax/transaction/jta/1.0.1b/jta-1.0.1b.jar, /root/.m2/repository/joda-time/joda-time/2.3/joda-time-2.3.jar, /root/.m2/repository/org/apache/httpcomponents/httpclient/4.3.3/httpclient-4.3.3.jar, /root/.m2/repository/org/apache/httpcomponents/httpcore/4.3.2/httpcore-4.3.2.jar, /root/.m2/repository/com/hazelcast/hazelcast/2.4/hazelcast-2.4.jar, /root/.m2/repository/org/apache/chemistry/opencmis/chemistry-opencmis-client-impl/0.11.0/chemistry-opencmis-client-impl-0.11.0.jar, /root/.m2/repository/org/apache/chemistry/opencmis/chemistry-opencmis-client-api/0.11.0/chemistry-opencmis-client-api-0.11.0.jar, /root/.m2/repository/org/apache/chemistry/opencmis/chemistry-opencmis-commons-api/0.11.0/chemistry-opencmis-commons-api-0.11.0.jar, /root/.m2/repository/org/apache/chemistry/opencmis/chemistry-opencmis-commons-impl/0.11.0/chemistry-opencmis-commons-impl-0.11.0.jar, /root/.m2/repository/org/codehaus/woodstox/woodstox-core-asl/4.2.0/woodstox-core-asl-4.2.0.jar, /root/.m2/repository/javax/xml/stream/stax-api/1.0-2/stax-api-1.0-2.jar, /root/.m2/repository/org/codehaus/woodstox/stax2-api/3.1.1/stax2-api-3.1.1.jar, /root/.m2/repository/org/jvnet/mimepull/mimepull/1.9.4/mimepull-1.9.4.jar, /root/.m2/repository/com/sun/xml/ws/jaxws-rt/2.1.7/jaxws-rt-2.1.7.jar, /root/.m2/repository/javax/xml/ws/jaxws-api/2.1/jaxws-api-2.1.jar, /root/.m2/repository/javax/xml/bind/jaxb-api/2.1/jaxb-api-2.1.jar, /root/.m2/repository/com/sun/xml/messaging/saaj/saaj-impl/1.3.3/saaj-impl-1.3.3.jar, /root/.m2/repository/javax/xml/soap/saaj-api/1.3/saaj-api-1.3.jar, /root/.m2/repository/com/sun/xml/stream/buffer/streambuffer/0.9/streambuffer-0.9.jar, /root/.m2/repository/javax/activation/activation/1.1/activation-1.1.jar, /root/.m2/repository/org/jvnet/staxex/stax-ex/1.2/stax-ex-1.2.jar, /root/.m2/repository/com/sun/org/apache/xml/internal/resolver/20050927/resolver-20050927.jar, /root/.m2/repository/org/apache/chemistry/opencmis/chemistry-opencmis-client-bindings/0.11.0/chemistry-opencmis-client-bindings-0.11.0.jar, /root/.m2/repository/org/slf4j/slf4j-api/1.7.5/slf4j-api-1.7.5.jar, /root/.m2/repository/org/alfresco/cmis/client/alfresco-opencmis-extension/1.0/alfresco-opencmis-extension-1.0.jar, /root/.m2/repository/org/springframework/extensions/surf/spring-cmis-framework/5.0.c/spring-cmis-framework-5.0.c.jar, /root/.m2/repository/org/springframework/extensions/surf/spring-webscripts/5.0.c/spring-webscripts-5.0.c.jar, /root/.m2/repository/commons-beanutils/commons-beanutils/1.9.1/commons-beanutils-1.9.1.jar, /root/.m2/repository/org/htmlparser/htmlparser/2.1/htmlparser-2.1.jar, /root/.m2/repository/org/htmlparser/htmllexer/2.1/htmllexer-2.1.jar, /root/.m2/repository/org/springframework/spring-webmvc/3.2.10.RELEASE/spring-webmvc-3.2.10.RELEASE.jar, /root/.m2/repository/org/springframework/spring-expression/3.2.10.RELEASE/spring-expression-3.2.10.RELEASE.jar, /root/.m2/repository/org/mozilla/rhino/1.7R4-alfresco-patched/rhino-1.7R4-alfresco-patched.jar, /root/.m2/repository/com/googlecode/json-simple/json-simple/1.1.1/json-simple-1.1.1.jar, /root/.m2/repository/org/tuckey/urlrewritefilter/4.0.4/urlrewritefilter-4.0.4.jar, /root/.m2/repository/commons-fileupload/commons-fileupload/1.3.1/commons-fileupload-1.3.1.jar, /root/.m2/repository/commons-io/commons-io/2.4/commons-io-2.4.jar, /root/.m2/repository/jaxen/jaxen/1.1.6/jaxen-1.1.6.jar, /root/.m2/repository/org/freemarker/freemarker/2.3.20-alfresco-patched/freemarker-2.3.20-alfresco-patched.jar, /root/.m2/repository/stax/stax-api/1.0.1/stax-api-1.0.1.jar, /root/.m2/repository/com/hazelcast/hazelcast-spring/2.4/hazelcast-spring-2.4.jar, /root/.m2/repository/javax/servlet/jstl/1.2/jstl-1.2.jar, /root/.m2/repository/org/apache/myfaces/core/myfaces-api/1.1.8/myfaces-api-1.1.8.jar, /root/.m2/repository/commons-logging/commons-logging/1.1.3/commons-logging-1.1.3.jar, /root/.m2/repository/org/apache/myfaces/core/myfaces-impl/1.1.8/myfaces-impl-1.1.8.jar, /root/.m2/repository/commons-el/commons-el/1.0/commons-el-1.0.jar, /root/.m2/repository/commons-lang/commons-lang/2.6/commons-lang-2.6.jar, /root/.m2/repository/commons-collections/commons-collections/3.2.1/commons-collections-3.2.1.jar, /root/.m2/repository/commons-digester/commons-digester/2.1/commons-digester-2.1.jar, /root/.m2/repository/com/asual/lesscss/lesscss-engine/1.5.0/lesscss-engine-1.5.0.jar, /root/.m2/repository/org/springframework/extensions/surf/spring-surf-api/5.0.c/spring-surf-api-5.0.c.jar, /root/.m2/repository/org/springframework/extensions/surf/spring-surf/5.0.c/spring-surf-5.0.c.jar, /root/.m2/repository/com/yahoo/platform/yui/yuicompressor/2.4.8-alfresco-patched/yuicompressor-2.4.8-alfresco-patched.jar, /root/.m2/repository/com/googlecode/concurrentlinkedhashmap/concurrentlinkedhashmap-lru/1.2/concurrentlinkedhashmap-lru-1.2.jar, /root/.m2/repository/org/springframework/extensions/surf/spring-webscripts-api/5.0.c/spring-webscripts-api-5.0.c.jar, /root/.m2/repository/javax/servlet/servlet-api/2.5/servlet-api-2.5.jar, /root/.m2/repository/org/alfresco/maven/alfresco-rad/2.0.0/alfresco-rad-2.0.0.jar, /root/.m2/repository/com/tradeshift/junit-remote/3/junit-remote-3.jar, /root/.m2/repository/junit/junit/4.11/junit-4.11.jar, /root/.m2/repository/org/hamcrest/hamcrest-core/1.3/hamcrest-core-1.3.jar, /root/.m2/repository/org/eclipse/jetty/jetty-servlet/7.4.0.v20110414/jetty-servlet-7.4.0.v20110414.jar, /root/.m2/repository/org/eclipse/jetty/jetty-security/7.4.0.v20110414/jetty-security-7.4.0.v20110414.jar, /root/.m2/repository/args4j/args4j/2.0.16/args4j-2.0.16.jar, /root/.m2/repository/org/springframework/spring-test/3.0.6.RELEASE/spring-test-3.0.6.RELEASE.jar, /root/.m2/repository/org/eclipse/jetty/jetty-server/7.4.0.v20110414/jetty-server-7.4.0.v20110414.jar, /root/.m2/repository/org/eclipse/jetty/jetty-continuation/7.4.0.v20110414/jetty-continuation-7.4.0.v20110414.jar, /root/.m2/repository/org/eclipse/jetty/jetty-http/7.4.0.v20110414/jetty-http-7.4.0.v20110414.jar, /root/.m2/repository/org/eclipse/jetty/jetty-io/7.4.0.v20110414/jetty-io-7.4.0.v20110414.jar, /root/.m2/repository/org/eclipse/jetty/jetty-util/7.4.0.v20110414/jetty-util-7.4.0.v20110414.jar]
[DEBUG]   (f) compileSourceRoots = [/home/programues/Documents/metadata_myoffice/content-all-share/src/main/java]
[DEBUG]   (f) compilerId = javac
[DEBUG]   (f) debug = true
[DEBUG]   (f) encoding = UTF-8
[DEBUG]   (f) failOnError = true
[DEBUG]   (f) forceJavacCompilerUse = false
[DEBUG]   (f) fork = false
[DEBUG]   (f) generatedSourcesDirectory = /home/programues/Documents/metadata_myoffice/content-all-share/target/generated-sources/annotations
[DEBUG]   (f) mojoExecution = org.apache.maven.plugins:maven-compiler-plugin:3.2:compile {execution: default-compile}
[DEBUG]   (f) optimize = false
[DEBUG]   (f) outputDirectory = /home/programues/Documents/metadata_myoffice/content-all-share/target/classes
[DEBUG]   (f) project = MavenProject: com.netprojex:content-all-share:1.0-SNAPSHOT @ /home/programues/Documents/metadata_myoffice/content-all-share/pom.xml
[DEBUG]   (f) projectArtifact = com.netprojex:content-all-share:amp:1.0-SNAPSHOT
[DEBUG]   (f) session = org.apache.maven.execution.MavenSession@21b4a7
[DEBUG]   (f) showDeprecation = false
[DEBUG]   (f) showWarnings = false
[DEBUG]   (f) skipMultiThreadWarning = false
[DEBUG]   (f) source = 1.7
[DEBUG]   (f) staleMillis = 0
[DEBUG]   (f) target = 1.7
[DEBUG]   (f) useIncrementalCompilation = true
[DEBUG]   (f) verbose = false
[DEBUG] -- end configuration --
[DEBUG] Using compiler 'javac'.
[INFO] No sources to compile
[INFO] 
[INFO] --- maven-resources-plugin:2.7:testResources (default-testResources) @ content-all-share ---
[DEBUG] Configuring mojo org.apache.maven.plugins:maven-resources-plugin:2.7:testResources from plugin realm ClassRealm[plugin>org.apache.maven.plugins:maven-resources-plugin:2.7, parent: sun.misc.Launcher$AppClassLoader@71b456f]
[DEBUG] Configuring mojo 'org.apache.maven.plugins:maven-resources-plugin:2.7:testResources' with basic configurator -->
[DEBUG]   (f) buildFilters = []
[DEBUG]   (f) encoding = UTF-8
[DEBUG]   (f) escapeWindowsPaths = true
[DEBUG]   (s) includeEmptyDirs = false
[DEBUG]   (f) nonFilteredFileExtensions = [ftl, acp, jpg, png, gif, svg, pdf, doc, docx, xls, xlsx, ppt, pptx, bin, lic, swf, zip, msg, jar, ttf, eot, woff, woff2]
[DEBUG]   (s) outputDirectory = /home/programues/Documents/metadata_myoffice/content-all-share/target/test-classes
[DEBUG]   (s) overwrite = false
[DEBUG]   (f) project = MavenProject: com.netprojex:content-all-share:1.0-SNAPSHOT @ /home/programues/Documents/metadata_myoffice/content-all-share/pom.xml
[DEBUG]   (s) resources = [Resource {targetPath: null, filtering: true, FileSet {directory: /home/programues/Documents/metadata_myoffice/content-all-share/src/test/resources, PatternSet [includes: {}, excludes: {}]}}]
[DEBUG]   (f) session = org.apache.maven.execution.MavenSession@21b4a7
[DEBUG]   (f) supportMultiLineFiltering = false
[DEBUG]   (f) useBuildFilters = true
[DEBUG]   (s) useDefaultDelimiters = true
[DEBUG] -- end configuration --
[DEBUG] properties used {env.OLDPWD=/home/programues/Documents/metadata_myoffice/content-all-share, file.encoding.pkg=sun.io, java.home=/usr/lib/jvm/java-7-openjdk-amd64/jre, app.testing.jvm.args=-Xms256m -Xmx1524m -XX:MaxPermSize=256m -Duser.language=en, stagingDirectory=../../target/staging/poms/alfresco-sdk-parent, env.DISPLAY=:0.0, env.LS_COLORS=rs=0:di=01;34:ln=01;36:mh=00:pi=40;33:so=01;35:do=01;35:bd=40;33;01:cd=40;33;01:or=40;31;01:su=37;41:sg=30;43:ca=30;41:tw=30;42:ow=34;42:st=37;44:ex=01;32:*.tar=01;31:*.tgz=01;31:*.arj=01;31:*.taz=01;31:*.lzh=01;31:*.lzma=01;31:*.tlz=01;31:*.txz=01;31:*.zip=01;31:*.z=01;31:*.Z=01;31:*.dz=01;31:*.gz=01;31:*.lz=01;31:*.xz=01;31:*.bz2=01;31:*.bz=01;31:*.tbz=01;31:*.tbz2=01;31:*.tz=01;31:*.deb=01;31:*.rpm=01;31:*.jar=01;31:*.war=01;31:*.ear=01;31:*.sar=01;31:*.rar=01;31:*.ace=01;31:*.zoo=01;31:*.cpio=01;31:*.7z=01;31:*.rz=01;31:*.jpg=01;35:*.jpeg=01;35:*.gif=01;35:*.bmp=01;35:*.pbm=01;35:*.pgm=01;35:*.ppm=01;35:*.tga=01;35:*.xbm=01;35:*.xpm=01;35:*.tif=01;35:*.tiff=01;35:*.png=01;35:*.svg=01;35:*.svgz=01;35:*.mng=01;35:*.pcx=01;35:*.mov=01;35:*.mpg=01;35:*.mpeg=01;35:*.m2v=01;35:*.mkv=01;35:*.webm=01;35:*.ogm=01;35:*.mp4=01;35:*.m4v=01;35:*.mp4v=01;35:*.vob=01;35:*.qt=01;35:*.nuv=01;35:*.wmv=01;35:*.asf=01;35:*.rm=01;35:*.rmvb=01;35:*.flc=01;35:*.avi=01;35:*.fli=01;35:*.flv=01;35:*.gl=01;35:*.dl=01;35:*.xcf=01;35:*.xwd=01;35:*.yuv=01;35:*.cgm=01;35:*.emf=01;35:*.axv=01;35:*.anx=01;35:*.ogv=01;35:*.ogx=01;35:*.aac=00;36:*.au=00;36:*.flac=00;36:*.mid=00;36:*.midi=00;36:*.mka=00;36:*.mp3=00;36:*.mpc=00;36:*.ogg=00;36:*.ra=00;36:*.wav=00;36:*.axa=00;36:*.oga=00;36:*.spx=00;36:*.xspf=00;36:, env.XDG_VTNR=7, alfresco.client.war=share, alfresco.client.war.version=5.0.c, classworlds.conf=/usr/share/maven/bin/m2.conf, env.SUDO_USER=programues, app.log.root.level=WARN, java.endorsed.dirs=/usr/lib/jvm/java-7-openjdk-amd64/jre/lib/endorsed, env.LOGNAME=root, alfresco.db.password=alfresco, env.USERNAME=root, maven.jar.version=2.5, app.amp.excludes=, env.XDG_SEAT=seat0, maven.war.version=2.5, env.LC_PAPER=cs_CZ.UTF-8, sun.os.patch.level=unknown, java.vendor.url=http://java.oracle.com/, maven.tomcat.port=8081, maven.dependency.version=2.9, maven.resources.version=2.7, java.version=1.7.0_101, env.ANDROID_HOME=/op/adt/sdk/, env.MAVEN_OPTS=-javaagent:/root/.m2/repository/org/springframework/springloaded/1.2.0.RELEASE/springloaded-1.2.0.RELEASE.jar -noverify, java.vendor.url.bug=http://bugreport.sun.com/bugreport/, env.LANGUAGE=en_US, skipTests=true, alfresco.groupId=org.alfresco, maven.alfresco.version=2.0.0, user.name=root, env.LANG=en_US.UTF-8, env.LC_MONETARY=cs_CZ.UTF-8, sun.io.unicode.encoding=UnicodeLittle, sun.jnu.encoding=UTF-8, java.runtime.name=OpenJDK Runtime Environment, env.LC_NAME=cs_CZ.UTF-8, java.specification.name=Java Platform API Specification, user.timezone=, env.LESSOPEN=| /usr/bin/lesspipe %s, path.separator=:, alfresco.db.url=jdbc:h2:./alf_data_dev/h2_data/alf_dev;MODE=PostgreSQL;AUTO_SERVER=TRUE;DB_CLOSE_ON_EXIT=FALSE;LOCK_TIMEOUT=10000;MVCC=FALSE;LOCK_MODE=0, file.encoding=UTF-8, env.HOME=/root, app.amp.output.folder=../content-all-share, sun.java.command=org.codehaus.plexus.classworlds.launcher.Launcher integration-test -Pamp-to-war -X, maven.install.version=2.5.2, env.LC_TELEPHONE=cs_CZ.UTF-8, env._=/usr/bin/mvn, app.filtering.enabled=true, java.io.tmpdir=/tmp, env.LC_IDENTIFICATION=cs_CZ.UTF-8, env.ANRDOID_NDK_ROOT=/home/programues/Android/android-ndk-r12b/, user.language=en, app.slf4j.scope=runtime, env.NDK=/home/programues/Android/android-ndk-r12b/, line.separator=
, app.amp.overlay.excludes=, app.log.dir=/home/programues/Documents/metadata_myoffice/content-all-share/target/, env=local, alfresco.community.default.version=5.0.c, alfresco.data.location=alf_data_dev, maven.tomcat.version=2.2, java.vm.info=mixed mode, maven.buildhelper.version=1.9.1, alfresco.db.username=alfresco, java.vm.specification.name=Java Virtual Machine Specification, alfresco.client.war.folder=/home/programues/Documents/metadata_myoffice/content-all-share/target/content-all-share-war, env.COLORTERM=xfce4-terminal, app.slf4j.version=1.5.11, env.LC_MEASUREMENT=cs_CZ.UTF-8, alfresco.repo.url=http://localhost:8080/alfresco, env.LC_TIME=cs_CZ.UTF-8, java.awt.printerjob=sun.print.PSPrinterJob, maven.jetty.version=7.4.0.v20110414, alfresco.client.contextPath=/share, maven.clean.version=2.6.1, os.name=Linux, java.specification.vendor=Oracle Corporation, alfresco.db.params=MODE=PostgreSQL;AUTO_SERVER=TRUE;DB_CLOSE_ON_EXIT=FALSE;LOCK_TIMEOUT=10000;MVCC=FALSE;LOCK_MODE=0, alfresco.repo.artifactId=alfresco, env.TERM=xterm, java.vm.name=OpenJDK 64-Bit Server VM, java.library.path=/usr/java/packages/lib/amd64:/usr/lib/x86_64-linux-gnu/jni:/lib/x86_64-linux-gnu:/usr/lib/x86_64-linux-gnu:/usr/lib/jni:/lib:/usr/lib, env.PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/games:/usr/local/games:/opt/android-sdk/sdk/tools:/opt/android-sdk/sdk/platform-tools, env.LESSCLOSE=/usr/bin/lesspipe %s %s, alfresco.client.war.groupId=org.alfresco, alfresco.version=5.0.c, java.class.version=51.0, env.SHLVL=2, app.properties.test.include=**, alfresco.db.name=alf_dev, maven.compiler.version=3.2, sun.boot.library.path=/usr/lib/jvm/java-7-openjdk-amd64/jre/lib/amd64, project.build.sourceEncoding=UTF-8, app.amp.folder=src/main/amp, sun.management.compiler=HotSpot 64-Bit Tiered Compilers, java.awt.graphicsenv=sun.awt.X11GraphicsEnvironment, maven.enforcer.plugin=1.3.1, env.USER=root, junit.version=4.11, alfresco.sdk.parent.version=1.0-SNAPSHOT, java.vm.specification.version=1.7, springloaded.version=1.2.0.RELEASE, awt.toolkit=sun.awt.X11.XToolkit, sun.cpu.isalist=, alfresco.db.datasource.class=org.h2.jdbcx.JdbcDataSource, java.ext.dirs=/usr/lib/jvm/java-7-openjdk-amd64/jre/lib/ext:/usr/java/packages/lib/ext, os.version=3.19.0-64-generic, user.home=/root, maven.release.version=2.5.1, java.vm.vendor=Oracle Corporation, env.JAVA_HOME=/usr/lib/jvm/java-7-openjdk-amd64, env.SUDO_COMMAND=/bin/su, env.XDG_SESSION_ID=c1, env.XAUTHORITY=/home/programues/.Xauthority, app.properties.test.folder=src/test/properties/local, user.dir=/home/programues/Documents/metadata_myoffice/content-all-share, app.properties.include=**, env.MAIL=/var/mail/root, env.NDK_TARGET=arm-linux-androideabi-4.9, env.PWD=/home/programues/Documents/metadata_myoffice/content-all-share, sun.cpu.endian=little, maven.antrun.version=1.7, java.vm.version=24.95-b01, java.class.path=/usr/share/maven/boot/plexus-classworlds-2.x.jar:/root/.m2/repository/org/springframework/springloaded/1.2.0.RELEASE/springloaded-1.2.0.RELEASE.jar, env.LC_ADDRESS=cs_CZ.UTF-8, os.arch=amd64, maven.build.version=Apache Maven 3.0.5, env.LC_NUMERIC=cs_CZ.UTF-8, env.SUDO_UID=1000, sun.java.launcher=SUN_STANDARD, alfresco.enterprise.default.version=5.0, java.vm.specification.vendor=Oracle Corporation, noSnapshotVersion=1.0.1702271512, file.separator=/, h2.version=1.4.182, java.runtime.version=1.7.0_101-b00, sun.boot.class.path=/usr/lib/jvm/java-7-openjdk-amd64/jre/lib/resources.jar:/usr/lib/jvm/java-7-openjdk-amd64/jre/lib/rt.jar:/usr/lib/jvm/java-7-openjdk-amd64/jre/lib/sunrsasign.jar:/usr/lib/jvm/java-7-openjdk-amd64/jre/lib/jsse.jar:/usr/lib/jvm/java-7-openjdk-amd64/jre/lib/jce.jar:/usr/lib/jvm/java-7-openjdk-amd64/jre/lib/charsets.jar:/usr/lib/jvm/java-7-openjdk-amd64/jre/lib/rhino.jar:/usr/lib/jvm/java-7-openjdk-amd64/jre/lib/jfr.jar:/usr/lib/jvm/java-7-openjdk-amd64/jre/classes, maven.replacer.version=1.5.3, scm.url.base=https://github.com/Alfresco/alfresco-sdk, maven.surefire.version=2.18, maven.version=3.0.5, maven.archetype.version=2.2, user.country=US, env.SHELL=/bin/bash, maven.home=/usr/share/maven, alfresco.share.artifactId=share, java.vendor=Oracle Corporation, app.properties.folder=src/main/properties/local, java.specification.version=1.7, env.SUDO_GID=1000, sun.arch.data.model=64}
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[DEBUG] resource with targetPath null
directory /home/programues/Documents/metadata_myoffice/content-all-share/src/test/resources
excludes []
includes []
[DEBUG] ignoreDelta true
[INFO] Copying 2 resources
[DEBUG] file share-config-custom.xml has a filtered file extension
[DEBUG] filtering /home/programues/Documents/metadata_myoffice/content-all-share/src/test/resources/alfresco/web-extension/share-config-custom.xml to /home/programues/Documents/metadata_myoffice/content-all-share/target/test-classes/alfresco/web-extension/share-config-custom.xml
[DEBUG] file log4j.properties has a filtered file extension
[DEBUG] filtering /home/programues/Documents/metadata_myoffice/content-all-share/src/test/resources/log4j.properties to /home/programues/Documents/metadata_myoffice/content-all-share/target/test-classes/log4j.properties
[DEBUG] no use filter components
[INFO] 
[INFO] --- maven-resources-plugin:2.7:copy-resources (add-module-properties-to-test-classpath) @ content-all-share ---
[DEBUG] Configuring mojo org.apache.maven.plugins:maven-resources-plugin:2.7:copy-resources from plugin realm ClassRealm[plugin>org.apache.maven.plugins:maven-resources-plugin:2.7, parent: sun.misc.Launcher$AppClassLoader@71b456f]
[DEBUG] Configuring mojo 'org.apache.maven.plugins:maven-resources-plugin:2.7:copy-resources' with basic configurator -->
[DEBUG]   (f) buildFilters = []
[DEBUG]   (f) encoding = UTF-8
[DEBUG]   (f) escapeWindowsPaths = true
[DEBUG]   (s) includeEmptyDirs = false
[DEBUG]   (f) nonFilteredFileExtensions = [ftl, acp, jpg, png, gif, svg, pdf, doc, docx, xls, xlsx, ppt, pptx, bin, lic, swf, zip, msg, jar, ttf, eot, woff, woff2]
[DEBUG]   (s) outputDirectory = /home/programues/Documents/metadata_myoffice/content-all-share/target/test-classes
[DEBUG]   (s) overwrite = false
[DEBUG]   (f) project = MavenProject: com.netprojex:content-all-share:1.0-SNAPSHOT @ /home/programues/Documents/metadata_myoffice/content-all-share/pom.xml
[DEBUG]   (s) directory = src/main/amp
[DEBUG]   (s) includes = [module.properties]
[DEBUG]   (s) filtering = true
[DEBUG]   (s) targetPath = alfresco/module/content-all-share
[DEBUG]   (s) resources = [Resource {targetPath: alfresco/module/content-all-share, filtering: true, FileSet {directory: src/main/amp, PatternSet [includes: {module.properties}, excludes: {}]}}]
[DEBUG]   (f) session = org.apache.maven.execution.MavenSession@21b4a7
[DEBUG]   (f) supportMultiLineFiltering = false
[DEBUG]   (f) useBuildFilters = true
[DEBUG]   (s) useDefaultDelimiters = true
[DEBUG] -- end configuration --
[DEBUG] properties used {env.OLDPWD=/home/programues/Documents/metadata_myoffice/content-all-share, file.encoding.pkg=sun.io, java.home=/usr/lib/jvm/java-7-openjdk-amd64/jre, app.testing.jvm.args=-Xms256m -Xmx1524m -XX:MaxPermSize=256m -Duser.language=en, stagingDirectory=../../target/staging/poms/alfresco-sdk-parent, env.DISPLAY=:0.0, env.LS_COLORS=rs=0:di=01;34:ln=01;36:mh=00:pi=40;33:so=01;35:do=01;35:bd=40;33;01:cd=40;33;01:or=40;31;01:su=37;41:sg=30;43:ca=30;41:tw=30;42:ow=34;42:st=37;44:ex=01;32:*.tar=01;31:*.tgz=01;31:*.arj=01;31:*.taz=01;31:*.lzh=01;31:*.lzma=01;31:*.tlz=01;31:*.txz=01;31:*.zip=01;31:*.z=01;31:*.Z=01;31:*.dz=01;31:*.gz=01;31:*.lz=01;31:*.xz=01;31:*.bz2=01;31:*.bz=01;31:*.tbz=01;31:*.tbz2=01;31:*.tz=01;31:*.deb=01;31:*.rpm=01;31:*.jar=01;31:*.war=01;31:*.ear=01;31:*.sar=01;31:*.rar=01;31:*.ace=01;31:*.zoo=01;31:*.cpio=01;31:*.7z=01;31:*.rz=01;31:*.jpg=01;35:*.jpeg=01;35:*.gif=01;35:*.bmp=01;35:*.pbm=01;35:*.pgm=01;35:*.ppm=01;35:*.tga=01;35:*.xbm=01;35:*.xpm=01;35:*.tif=01;35:*.tiff=01;35:*.png=01;35:*.svg=01;35:*.svgz=01;35:*.mng=01;35:*.pcx=01;35:*.mov=01;35:*.mpg=01;35:*.mpeg=01;35:*.m2v=01;35:*.mkv=01;35:*.webm=01;35:*.ogm=01;35:*.mp4=01;35:*.m4v=01;35:*.mp4v=01;35:*.vob=01;35:*.qt=01;35:*.nuv=01;35:*.wmv=01;35:*.asf=01;35:*.rm=01;35:*.rmvb=01;35:*.flc=01;35:*.avi=01;35:*.fli=01;35:*.flv=01;35:*.gl=01;35:*.dl=01;35:*.xcf=01;35:*.xwd=01;35:*.yuv=01;35:*.cgm=01;35:*.emf=01;35:*.axv=01;35:*.anx=01;35:*.ogv=01;35:*.ogx=01;35:*.aac=00;36:*.au=00;36:*.flac=00;36:*.mid=00;36:*.midi=00;36:*.mka=00;36:*.mp3=00;36:*.mpc=00;36:*.ogg=00;36:*.ra=00;36:*.wav=00;36:*.axa=00;36:*.oga=00;36:*.spx=00;36:*.xspf=00;36:, env.XDG_VTNR=7, alfresco.client.war=share, alfresco.client.war.version=5.0.c, classworlds.conf=/usr/share/maven/bin/m2.conf, env.SUDO_USER=programues, app.log.root.level=WARN, java.endorsed.dirs=/usr/lib/jvm/java-7-openjdk-amd64/jre/lib/endorsed, env.LOGNAME=root, alfresco.db.password=alfresco, env.USERNAME=root, maven.jar.version=2.5, app.amp.excludes=, env.XDG_SEAT=seat0, maven.war.version=2.5, env.LC_PAPER=cs_CZ.UTF-8, sun.os.patch.level=unknown, java.vendor.url=http://java.oracle.com/, maven.tomcat.port=8081, maven.dependency.version=2.9, maven.resources.version=2.7, java.version=1.7.0_101, env.ANDROID_HOME=/op/adt/sdk/, env.MAVEN_OPTS=-javaagent:/root/.m2/repository/org/springframework/springloaded/1.2.0.RELEASE/springloaded-1.2.0.RELEASE.jar -noverify, java.vendor.url.bug=http://bugreport.sun.com/bugreport/, env.LANGUAGE=en_US, skipTests=true, alfresco.groupId=org.alfresco, maven.alfresco.version=2.0.0, user.name=root, env.LANG=en_US.UTF-8, env.LC_MONETARY=cs_CZ.UTF-8, sun.io.unicode.encoding=UnicodeLittle, sun.jnu.encoding=UTF-8, java.runtime.name=OpenJDK Runtime Environment, env.LC_NAME=cs_CZ.UTF-8, java.specification.name=Java Platform API Specification, user.timezone=, env.LESSOPEN=| /usr/bin/lesspipe %s, path.separator=:, alfresco.db.url=jdbc:h2:./alf_data_dev/h2_data/alf_dev;MODE=PostgreSQL;AUTO_SERVER=TRUE;DB_CLOSE_ON_EXIT=FALSE;LOCK_TIMEOUT=10000;MVCC=FALSE;LOCK_MODE=0, file.encoding=UTF-8, env.HOME=/root, app.amp.output.folder=../content-all-share, sun.java.command=org.codehaus.plexus.classworlds.launcher.Launcher integration-test -Pamp-to-war -X, maven.install.version=2.5.2, env.LC_TELEPHONE=cs_CZ.UTF-8, env._=/usr/bin/mvn, app.filtering.enabled=true, java.io.tmpdir=/tmp, env.LC_IDENTIFICATION=cs_CZ.UTF-8, env.ANRDOID_NDK_ROOT=/home/programues/Android/android-ndk-r12b/, user.language=en, app.slf4j.scope=runtime, env.NDK=/home/programues/Android/android-ndk-r12b/, line.separator=
, app.amp.overlay.excludes=, app.log.dir=/home/programues/Documents/metadata_myoffice/content-all-share/target/, env=local, alfresco.community.default.version=5.0.c, alfresco.data.location=alf_data_dev, maven.tomcat.version=2.2, java.vm.info=mixed mode, maven.buildhelper.version=1.9.1, alfresco.db.username=alfresco, java.vm.specification.name=Java Virtual Machine Specification, alfresco.client.war.folder=/home/programues/Documents/metadata_myoffice/content-all-share/target/content-all-share-war, env.COLORTERM=xfce4-terminal, app.slf4j.version=1.5.11, env.LC_MEASUREMENT=cs_CZ.UTF-8, alfresco.repo.url=http://localhost:8080/alfresco, env.LC_TIME=cs_CZ.UTF-8, java.awt.printerjob=sun.print.PSPrinterJob, maven.jetty.version=7.4.0.v20110414, alfresco.client.contextPath=/share, maven.clean.version=2.6.1, os.name=Linux, java.specification.vendor=Oracle Corporation, alfresco.db.params=MODE=PostgreSQL;AUTO_SERVER=TRUE;DB_CLOSE_ON_EXIT=FALSE;LOCK_TIMEOUT=10000;MVCC=FALSE;LOCK_MODE=0, alfresco.repo.artifactId=alfresco, env.TERM=xterm, java.vm.name=OpenJDK 64-Bit Server VM, java.library.path=/usr/java/packages/lib/amd64:/usr/lib/x86_64-linux-gnu/jni:/lib/x86_64-linux-gnu:/usr/lib/x86_64-linux-gnu:/usr/lib/jni:/lib:/usr/lib, env.PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/games:/usr/local/games:/opt/android-sdk/sdk/tools:/opt/android-sdk/sdk/platform-tools, env.LESSCLOSE=/usr/bin/lesspipe %s %s, alfresco.client.war.groupId=org.alfresco, alfresco.version=5.0.c, java.class.version=51.0, env.SHLVL=2, app.properties.test.include=**, alfresco.db.name=alf_dev, maven.compiler.version=3.2, sun.boot.library.path=/usr/lib/jvm/java-7-openjdk-amd64/jre/lib/amd64, project.build.sourceEncoding=UTF-8, app.amp.folder=src/main/amp, sun.management.compiler=HotSpot 64-Bit Tiered Compilers, java.awt.graphicsenv=sun.awt.X11GraphicsEnvironment, maven.enforcer.plugin=1.3.1, env.USER=root, junit.version=4.11, alfresco.sdk.parent.version=1.0-SNAPSHOT, java.vm.specification.version=1.7, springloaded.version=1.2.0.RELEASE, awt.toolkit=sun.awt.X11.XToolkit, sun.cpu.isalist=, alfresco.db.datasource.class=org.h2.jdbcx.JdbcDataSource, java.ext.dirs=/usr/lib/jvm/java-7-openjdk-amd64/jre/lib/ext:/usr/java/packages/lib/ext, os.version=3.19.0-64-generic, user.home=/root, maven.release.version=2.5.1, java.vm.vendor=Oracle Corporation, env.JAVA_HOME=/usr/lib/jvm/java-7-openjdk-amd64, env.SUDO_COMMAND=/bin/su, env.XDG_SESSION_ID=c1, env.XAUTHORITY=/home/programues/.Xauthority, app.properties.test.folder=src/test/properties/local, user.dir=/home/programues/Documents/metadata_myoffice/content-all-share, app.properties.include=**, env.MAIL=/var/mail/root, env.NDK_TARGET=arm-linux-androideabi-4.9, env.PWD=/home/programues/Documents/metadata_myoffice/content-all-share, sun.cpu.endian=little, maven.antrun.version=1.7, java.vm.version=24.95-b01, java.class.path=/usr/share/maven/boot/plexus-classworlds-2.x.jar:/root/.m2/repository/org/springframework/springloaded/1.2.0.RELEASE/springloaded-1.2.0.RELEASE.jar, env.LC_ADDRESS=cs_CZ.UTF-8, os.arch=amd64, maven.build.version=Apache Maven 3.0.5, env.LC_NUMERIC=cs_CZ.UTF-8, env.SUDO_UID=1000, sun.java.launcher=SUN_STANDARD, alfresco.enterprise.default.version=5.0, java.vm.specification.vendor=Oracle Corporation, noSnapshotVersion=1.0.1702271512, file.separator=/, h2.version=1.4.182, java.runtime.version=1.7.0_101-b00, sun.boot.class.path=/usr/lib/jvm/java-7-openjdk-amd64/jre/lib/resources.jar:/usr/lib/jvm/java-7-openjdk-amd64/jre/lib/rt.jar:/usr/lib/jvm/java-7-openjdk-amd64/jre/lib/sunrsasign.jar:/usr/lib/jvm/java-7-openjdk-amd64/jre/lib/jsse.jar:/usr/lib/jvm/java-7-openjdk-amd64/jre/lib/jce.jar:/usr/lib/jvm/java-7-openjdk-amd64/jre/lib/charsets.jar:/usr/lib/jvm/java-7-openjdk-amd64/jre/lib/rhino.jar:/usr/lib/jvm/java-7-openjdk-amd64/jre/lib/jfr.jar:/usr/lib/jvm/java-7-openjdk-amd64/jre/classes, maven.replacer.version=1.5.3, scm.url.base=https://github.com/Alfresco/alfresco-sdk, maven.surefire.version=2.18, maven.version=3.0.5, maven.archetype.version=2.2, user.country=US, env.SHELL=/bin/bash, maven.home=/usr/share/maven, alfresco.share.artifactId=share, java.vendor=Oracle Corporation, app.properties.folder=src/main/properties/local, java.specification.version=1.7, env.SUDO_GID=1000, sun.arch.data.model=64}
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[DEBUG] resource with targetPath alfresco/module/content-all-share
directory src/main/amp
excludes []
includes [module.properties]
[DEBUG] ignoreDelta true
[INFO] Copying 1 resource to alfresco/module/content-all-share
[DEBUG] file module.properties has a filtered file extension
[DEBUG] filtering /home/programues/Documents/metadata_myoffice/content-all-share/src/main/amp/module.properties to /home/programues/Documents/metadata_myoffice/content-all-share/target/test-classes/alfresco/module/content-all-share/module.properties
[DEBUG] no use filter components
[INFO] 
[INFO] --- maven-resources-plugin:2.7:copy-resources (add-module-config-to-test-classpath) @ content-all-share ---
[DEBUG] Configuring mojo org.apache.maven.plugins:maven-resources-plugin:2.7:copy-resources from plugin realm ClassRealm[plugin>org.apache.maven.plugins:maven-resources-plugin:2.7, parent: sun.misc.Launcher$AppClassLoader@71b456f]
[DEBUG] Configuring mojo 'org.apache.maven.plugins:maven-resources-plugin:2.7:copy-resources' with basic configurator -->
[DEBUG]   (f) buildFilters = []
[DEBUG]   (f) encoding = UTF-8
[DEBUG]   (f) escapeWindowsPaths = true
[DEBUG]   (s) includeEmptyDirs = false
[DEBUG]   (f) nonFilteredFileExtensions = [ftl, acp, jpg, png, gif, svg, pdf, doc, docx, xls, xlsx, ppt, pptx, bin, lic, swf, zip, msg, jar, ttf, eot, woff, woff2]
[DEBUG]   (s) outputDirectory = /home/programues/Documents/metadata_myoffice/content-all-share/target/test-classes
[DEBUG]   (s) overwrite = false
[DEBUG]   (f) project = MavenProject: com.netprojex:content-all-share:1.0-SNAPSHOT @ /home/programues/Documents/metadata_myoffice/content-all-share/pom.xml
[DEBUG]   (s) directory = src/main/amp/config
[DEBUG]   (s) includes = [**/*]
[DEBUG]   (s) filtering = true
[DEBUG]   (s) resources = [Resource {targetPath: null, filtering: true, FileSet {directory: src/main/amp/config, PatternSet [includes: {**/*}, excludes: {}]}}]
[DEBUG]   (f) session = org.apache.maven.execution.MavenSession@21b4a7
[DEBUG]   (f) supportMultiLineFiltering = false
[DEBUG]   (f) useBuildFilters = true
[DEBUG]   (s) useDefaultDelimiters = true
[DEBUG] -- end configuration --
[DEBUG] properties used {env.OLDPWD=/home/programues/Documents/metadata_myoffice/content-all-share, file.encoding.pkg=sun.io, java.home=/usr/lib/jvm/java-7-openjdk-amd64/jre, app.testing.jvm.args=-Xms256m -Xmx1524m -XX:MaxPermSize=256m -Duser.language=en, stagingDirectory=../../target/staging/poms/alfresco-sdk-parent, env.DISPLAY=:0.0, env.LS_COLORS=rs=0:di=01;34:ln=01;36:mh=00:pi=40;33:so=01;35:do=01;35:bd=40;33;01:cd=40;33;01:or=40;31;01:su=37;41:sg=30;43:ca=30;41:tw=30;42:ow=34;42:st=37;44:ex=01;32:*.tar=01;31:*.tgz=01;31:*.arj=01;31:*.taz=01;31:*.lzh=01;31:*.lzma=01;31:*.tlz=01;31:*.txz=01;31:*.zip=01;31:*.z=01;31:*.Z=01;31:*.dz=01;31:*.gz=01;31:*.lz=01;31:*.xz=01;31:*.bz2=01;31:*.bz=01;31:*.tbz=01;31:*.tbz2=01;31:*.tz=01;31:*.deb=01;31:*.rpm=01;31:*.jar=01;31:*.war=01;31:*.ear=01;31:*.sar=01;31:*.rar=01;31:*.ace=01;31:*.zoo=01;31:*.cpio=01;31:*.7z=01;31:*.rz=01;31:*.jpg=01;35:*.jpeg=01;35:*.gif=01;35:*.bmp=01;35:*.pbm=01;35:*.pgm=01;35:*.ppm=01;35:*.tga=01;35:*.xbm=01;35:*.xpm=01;35:*.tif=01;35:*.tiff=01;35:*.png=01;35:*.svg=01;35:*.svgz=01;35:*.mng=01;35:*.pcx=01;35:*.mov=01;35:*.mpg=01;35:*.mpeg=01;35:*.m2v=01;35:*.mkv=01;35:*.webm=01;35:*.ogm=01;35:*.mp4=01;35:*.m4v=01;35:*.mp4v=01;35:*.vob=01;35:*.qt=01;35:*.nuv=01;35:*.wmv=01;35:*.asf=01;35:*.rm=01;35:*.rmvb=01;35:*.flc=01;35:*.avi=01;35:*.fli=01;35:*.flv=01;35:*.gl=01;35:*.dl=01;35:*.xcf=01;35:*.xwd=01;35:*.yuv=01;35:*.cgm=01;35:*.emf=01;35:*.axv=01;35:*.anx=01;35:*.ogv=01;35:*.ogx=01;35:*.aac=00;36:*.au=00;36:*.flac=00;36:*.mid=00;36:*.midi=00;36:*.mka=00;36:*.mp3=00;36:*.mpc=00;36:*.ogg=00;36:*.ra=00;36:*.wav=00;36:*.axa=00;36:*.oga=00;36:*.spx=00;36:*.xspf=00;36:, env.XDG_VTNR=7, alfresco.client.war=share, alfresco.client.war.version=5.0.c, classworlds.conf=/usr/share/maven/bin/m2.conf, env.SUDO_USER=programues, app.log.root.level=WARN, java.endorsed.dirs=/usr/lib/jvm/java-7-openjdk-amd64/jre/lib/endorsed, env.LOGNAME=root, alfresco.db.password=alfresco, env.USERNAME=root, maven.jar.version=2.5, app.amp.excludes=, env.XDG_SEAT=seat0, maven.war.version=2.5, env.LC_PAPER=cs_CZ.UTF-8, sun.os.patch.level=unknown, java.vendor.url=http://java.oracle.com/, maven.tomcat.port=8081, maven.dependency.version=2.9, maven.resources.version=2.7, java.version=1.7.0_101, env.ANDROID_HOME=/op/adt/sdk/, env.MAVEN_OPTS=-javaagent:/root/.m2/repository/org/springframework/springloaded/1.2.0.RELEASE/springloaded-1.2.0.RELEASE.jar -noverify, java.vendor.url.bug=http://bugreport.sun.com/bugreport/, env.LANGUAGE=en_US, skipTests=true, alfresco.groupId=org.alfresco, maven.alfresco.version=2.0.0, user.name=root, env.LANG=en_US.UTF-8, env.LC_MONETARY=cs_CZ.UTF-8, sun.io.unicode.encoding=UnicodeLittle, sun.jnu.encoding=UTF-8, java.runtime.name=OpenJDK Runtime Environment, env.LC_NAME=cs_CZ.UTF-8, java.specification.name=Java Platform API Specification, user.timezone=, env.LESSOPEN=| /usr/bin/lesspipe %s, path.separator=:, alfresco.db.url=jdbc:h2:./alf_data_dev/h2_data/alf_dev;MODE=PostgreSQL;AUTO_SERVER=TRUE;DB_CLOSE_ON_EXIT=FALSE;LOCK_TIMEOUT=10000;MVCC=FALSE;LOCK_MODE=0, file.encoding=UTF-8, env.HOME=/root, app.amp.output.folder=../content-all-share, sun.java.command=org.codehaus.plexus.classworlds.launcher.Launcher integration-test -Pamp-to-war -X, maven.install.version=2.5.2, env.LC_TELEPHONE=cs_CZ.UTF-8, env._=/usr/bin/mvn, app.filtering.enabled=true, java.io.tmpdir=/tmp, env.LC_IDENTIFICATION=cs_CZ.UTF-8, env.ANRDOID_NDK_ROOT=/home/programues/Android/android-ndk-r12b/, user.language=en, app.slf4j.scope=runtime, env.NDK=/home/programues/Android/android-ndk-r12b/, line.separator=
, app.amp.overlay.excludes=, app.log.dir=/home/programues/Documents/metadata_myoffice/content-all-share/target/, env=local, alfresco.community.default.version=5.0.c, alfresco.data.location=alf_data_dev, maven.tomcat.version=2.2, java.vm.info=mixed mode, maven.buildhelper.version=1.9.1, alfresco.db.username=alfresco, java.vm.specification.name=Java Virtual Machine Specification, alfresco.client.war.folder=/home/programues/Documents/metadata_myoffice/content-all-share/target/content-all-share-war, env.COLORTERM=xfce4-terminal, app.slf4j.version=1.5.11, env.LC_MEASUREMENT=cs_CZ.UTF-8, alfresco.repo.url=http://localhost:8080/alfresco, env.LC_TIME=cs_CZ.UTF-8, java.awt.printerjob=sun.print.PSPrinterJob, maven.jetty.version=7.4.0.v20110414, alfresco.client.contextPath=/share, maven.clean.version=2.6.1, os.name=Linux, java.specification.vendor=Oracle Corporation, alfresco.db.params=MODE=PostgreSQL;AUTO_SERVER=TRUE;DB_CLOSE_ON_EXIT=FALSE;LOCK_TIMEOUT=10000;MVCC=FALSE;LOCK_MODE=0, alfresco.repo.artifactId=alfresco, env.TERM=xterm, java.vm.name=OpenJDK 64-Bit Server VM, java.library.path=/usr/java/packages/lib/amd64:/usr/lib/x86_64-linux-gnu/jni:/lib/x86_64-linux-gnu:/usr/lib/x86_64-linux-gnu:/usr/lib/jni:/lib:/usr/lib, env.PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/games:/usr/local/games:/opt/android-sdk/sdk/tools:/opt/android-sdk/sdk/platform-tools, env.LESSCLOSE=/usr/bin/lesspipe %s %s, alfresco.client.war.groupId=org.alfresco, alfresco.version=5.0.c, java.class.version=51.0, env.SHLVL=2, app.properties.test.include=**, alfresco.db.name=alf_dev, maven.compiler.version=3.2, sun.boot.library.path=/usr/lib/jvm/java-7-openjdk-amd64/jre/lib/amd64, project.build.sourceEncoding=UTF-8, app.amp.folder=src/main/amp, sun.management.compiler=HotSpot 64-Bit Tiered Compilers, java.awt.graphicsenv=sun.awt.X11GraphicsEnvironment, maven.enforcer.plugin=1.3.1, env.USER=root, junit.version=4.11, alfresco.sdk.parent.version=1.0-SNAPSHOT, java.vm.specification.version=1.7, springloaded.version=1.2.0.RELEASE, awt.toolkit=sun.awt.X11.XToolkit, sun.cpu.isalist=, alfresco.db.datasource.class=org.h2.jdbcx.JdbcDataSource, java.ext.dirs=/usr/lib/jvm/java-7-openjdk-amd64/jre/lib/ext:/usr/java/packages/lib/ext, os.version=3.19.0-64-generic, user.home=/root, maven.release.version=2.5.1, java.vm.vendor=Oracle Corporation, env.JAVA_HOME=/usr/lib/jvm/java-7-openjdk-amd64, env.SUDO_COMMAND=/bin/su, env.XDG_SESSION_ID=c1, env.XAUTHORITY=/home/programues/.Xauthority, app.properties.test.folder=src/test/properties/local, user.dir=/home/programues/Documents/metadata_myoffice/content-all-share, app.properties.include=**, env.MAIL=/var/mail/root, env.NDK_TARGET=arm-linux-androideabi-4.9, env.PWD=/home/programues/Documents/metadata_myoffice/content-all-share, sun.cpu.endian=little, maven.antrun.version=1.7, java.vm.version=24.95-b01, java.class.path=/usr/share/maven/boot/plexus-classworlds-2.x.jar:/root/.m2/repository/org/springframework/springloaded/1.2.0.RELEASE/springloaded-1.2.0.RELEASE.jar, env.LC_ADDRESS=cs_CZ.UTF-8, os.arch=amd64, maven.build.version=Apache Maven 3.0.5, env.LC_NUMERIC=cs_CZ.UTF-8, env.SUDO_UID=1000, sun.java.launcher=SUN_STANDARD, alfresco.enterprise.default.version=5.0, java.vm.specification.vendor=Oracle Corporation, noSnapshotVersion=1.0.1702271512, file.separator=/, h2.version=1.4.182, java.runtime.version=1.7.0_101-b00, sun.boot.class.path=/usr/lib/jvm/java-7-openjdk-amd64/jre/lib/resources.jar:/usr/lib/jvm/java-7-openjdk-amd64/jre/lib/rt.jar:/usr/lib/jvm/java-7-openjdk-amd64/jre/lib/sunrsasign.jar:/usr/lib/jvm/java-7-openjdk-amd64/jre/lib/jsse.jar:/usr/lib/jvm/java-7-openjdk-amd64/jre/lib/jce.jar:/usr/lib/jvm/java-7-openjdk-amd64/jre/lib/charsets.jar:/usr/lib/jvm/java-7-openjdk-amd64/jre/lib/rhino.jar:/usr/lib/jvm/java-7-openjdk-amd64/jre/lib/jfr.jar:/usr/lib/jvm/java-7-openjdk-amd64/jre/classes, maven.replacer.version=1.5.3, scm.url.base=https://github.com/Alfresco/alfresco-sdk, maven.surefire.version=2.18, maven.version=3.0.5, maven.archetype.version=2.2, user.country=US, env.SHELL=/bin/bash, maven.home=/usr/share/maven, alfresco.share.artifactId=share, java.vendor=Oracle Corporation, app.properties.folder=src/main/properties/local, java.specification.version=1.7, env.SUDO_GID=1000, sun.arch.data.model=64}
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[DEBUG] resource with targetPath null
directory src/main/amp/config
excludes []
includes [**/*]
[DEBUG] ignoreDelta true
[INFO] Copying 7 resources
[DEBUG] file uTheme.xml has a filtered file extension
[DEBUG] filtering /home/programues/Documents/metadata_myoffice/content-all-share/src/main/amp/config/alfresco/site-data/themes/uTheme.xml to /home/programues/Documents/metadata_myoffice/content-all-share/target/test-classes/alfresco/site-data/themes/uTheme.xml
[DEBUG] file baBanca.properties has a filtered file extension
[DEBUG] filtering /home/programues/Documents/metadata_myoffice/content-all-share/src/main/amp/config/alfresco/module/content-all-share/messages/baBanca.properties to /home/programues/Documents/metadata_myoffice/content-all-share/target/test-classes/alfresco/module/content-all-share/messages/baBanca.properties
[DEBUG] file example-widgets.xml has a filtered file extension
[DEBUG] filtering /home/programues/Documents/metadata_myoffice/content-all-share/src/main/amp/config/alfresco/web-extension/site-data/extensions/example-widgets.xml to /home/programues/Documents/metadata_myoffice/content-all-share/target/test-classes/alfresco/web-extension/site-data/extensions/example-widgets.xml
[DEBUG] file simple-page.get.desc.xml has a filtered file extension
[DEBUG] filtering /home/programues/Documents/metadata_myoffice/content-all-share/src/main/amp/config/alfresco/web-extension/site-webscripts/com/example/pages/simple-page.get.desc.xml to /home/programues/Documents/metadata_myoffice/content-all-share/target/test-classes/alfresco/web-extension/site-webscripts/com/example/pages/simple-page.get.desc.xml
[DEBUG] file simple-page.get.js has a filtered file extension
[DEBUG] filtering /home/programues/Documents/metadata_myoffice/content-all-share/src/main/amp/config/alfresco/web-extension/site-webscripts/com/example/pages/simple-page.get.js to /home/programues/Documents/metadata_myoffice/content-all-share/target/test-classes/alfresco/web-extension/site-webscripts/com/example/pages/simple-page.get.js
[DEBUG] file simple-page.get.html.ftl has a non filtered file extension
[DEBUG] copy /home/programues/Documents/metadata_myoffice/content-all-share/src/main/amp/config/alfresco/web-extension/site-webscripts/com/example/pages/simple-page.get.html.ftl to /home/programues/Documents/metadata_myoffice/content-all-share/target/test-classes/alfresco/web-extension/site-webscripts/com/example/pages/simple-page.get.html.ftl
[DEBUG] file content-tutorial-share-context.xml has a filtered file extension
[DEBUG] filtering /home/programues/Documents/metadata_myoffice/content-all-share/src/main/amp/config/alfresco/web-extension/content-tutorial-share-context.xml to /home/programues/Documents/metadata_myoffice/content-all-share/target/test-classes/alfresco/web-extension/content-tutorial-share-context.xml
[DEBUG] no use filter components
[INFO] 
[INFO] --- maven-compiler-plugin:3.2:testCompile (default-testCompile) @ content-all-share ---
[DEBUG] Configuring mojo org.apache.maven.plugins:maven-compiler-plugin:3.2:testCompile from plugin realm ClassRealm[plugin>org.apache.maven.plugins:maven-compiler-plugin:3.2, parent: sun.misc.Launcher$AppClassLoader@71b456f]
[DEBUG] Configuring mojo 'org.apache.maven.plugins:maven-compiler-plugin:3.2:testCompile' with basic configurator -->
[DEBUG]   (f) basedir = /home/programues/Documents/metadata_myoffice/content-all-share
[DEBUG]   (f) buildDirectory = /home/programues/Documents/metadata_myoffice/content-all-share/target
[DEBUG]   (f) classpathElements = [/home/programues/Documents/metadata_myoffice/content-all-share/target/test-classes, /home/programues/Documents/metadata_myoffice/content-all-share/target/classes, /root/.m2/repository/org/alfresco/share/5.0.c/share-5.0.c-classes.jar, /root/.m2/repository/org/alfresco/alfresco-web-framework-commons/5.0.c/alfresco-web-framework-commons-5.0.c-classes.jar, /root/.m2/repository/org/alfresco/alfresco-jlan-embed/5.0.c/alfresco-jlan-embed-5.0.c.jar, /root/.m2/repository/org/alfresco/alfresco-core/5.0.c/alfresco-core-5.0.c.jar, /root/.m2/repository/commons-codec/commons-codec/1.9/commons-codec-1.9.jar, /root/.m2/repository/commons-httpclient/commons-httpclient/3.1/commons-httpclient-3.1.jar, /root/.m2/repository/org/safehaus/jug/jug/2.0.0/jug-2.0.0-asl.jar, /root/.m2/repository/log4j/log4j/1.2.17/log4j-1.2.17.jar, /root/.m2/repository/org/json/json/20090211/json-20090211.jar, /root/.m2/repository/org/springframework/spring-orm/3.2.10.RELEASE/spring-orm-3.2.10.RELEASE.jar, /root/.m2/repository/org/springframework/spring-beans/3.2.10.RELEASE/spring-beans-3.2.10.RELEASE.jar, /root/.m2/repository/org/springframework/spring-core/3.2.10.RELEASE/spring-core-3.2.10.RELEASE.jar, /root/.m2/repository/org/springframework/spring-jdbc/3.2.10.RELEASE/spring-jdbc-3.2.10.RELEASE.jar, /root/.m2/repository/org/springframework/spring-tx/3.2.10.RELEASE/spring-tx-3.2.10.RELEASE.jar, /root/.m2/repository/org/springframework/extensions/surf/spring-surf-core-configservice/5.0.c/spring-surf-core-configservice-5.0.c.jar, /root/.m2/repository/org/springframework/extensions/surf/spring-surf-core/5.0.c/spring-surf-core-5.0.c.jar, /root/.m2/repository/org/springframework/spring-context/3.2.10.RELEASE/spring-context-3.2.10.RELEASE.jar, /root/.m2/repository/org/springframework/spring-aop/3.2.10.RELEASE/spring-aop-3.2.10.RELEASE.jar, /root/.m2/repository/org/springframework/spring-context-support/3.2.10.RELEASE/spring-context-support-3.2.10.RELEASE.jar, /root/.m2/repository/org/springframework/spring-web/3.2.10.RELEASE/spring-web-3.2.10.RELEASE.jar, /root/.m2/repository/com/sun/xml/bind/jaxb-impl/2.1.11/jaxb-impl-2.1.11.jar, /root/.m2/repository/dom4j/dom4j/1.6.1/dom4j-1.6.1.jar, /root/.m2/repository/xml-apis/xml-apis/1.4.01/xml-apis-1.4.01.jar, /root/.m2/repository/org/codehaus/guessencoding/guessencoding/1.0/guessencoding-1.0.jar, /root/.m2/repository/javax/transaction/jta/1.0.1b/jta-1.0.1b.jar, /root/.m2/repository/joda-time/joda-time/2.3/joda-time-2.3.jar, /root/.m2/repository/org/apache/httpcomponents/httpclient/4.3.3/httpclient-4.3.3.jar, /root/.m2/repository/org/apache/httpcomponents/httpcore/4.3.2/httpcore-4.3.2.jar, /root/.m2/repository/com/hazelcast/hazelcast/2.4/hazelcast-2.4.jar, /root/.m2/repository/org/apache/chemistry/opencmis/chemistry-opencmis-client-impl/0.11.0/chemistry-opencmis-client-impl-0.11.0.jar, /root/.m2/repository/org/apache/chemistry/opencmis/chemistry-opencmis-client-api/0.11.0/chemistry-opencmis-client-api-0.11.0.jar, /root/.m2/repository/org/apache/chemistry/opencmis/chemistry-opencmis-commons-api/0.11.0/chemistry-opencmis-commons-api-0.11.0.jar, /root/.m2/repository/org/apache/chemistry/opencmis/chemistry-opencmis-commons-impl/0.11.0/chemistry-opencmis-commons-impl-0.11.0.jar, /root/.m2/repository/org/codehaus/woodstox/woodstox-core-asl/4.2.0/woodstox-core-asl-4.2.0.jar, /root/.m2/repository/javax/xml/stream/stax-api/1.0-2/stax-api-1.0-2.jar, /root/.m2/repository/org/codehaus/woodstox/stax2-api/3.1.1/stax2-api-3.1.1.jar, /root/.m2/repository/org/jvnet/mimepull/mimepull/1.9.4/mimepull-1.9.4.jar, /root/.m2/repository/com/sun/xml/ws/jaxws-rt/2.1.7/jaxws-rt-2.1.7.jar, /root/.m2/repository/javax/xml/ws/jaxws-api/2.1/jaxws-api-2.1.jar, /root/.m2/repository/javax/xml/bind/jaxb-api/2.1/jaxb-api-2.1.jar, /root/.m2/repository/com/sun/xml/messaging/saaj/saaj-impl/1.3.3/saaj-impl-1.3.3.jar, /root/.m2/repository/javax/xml/soap/saaj-api/1.3/saaj-api-1.3.jar, /root/.m2/repository/com/sun/xml/stream/buffer/streambuffer/0.9/streambuffer-0.9.jar, /root/.m2/repository/javax/activation/activation/1.1/activation-1.1.jar, /root/.m2/repository/org/jvnet/staxex/stax-ex/1.2/stax-ex-1.2.jar, /root/.m2/repository/com/sun/org/apache/xml/internal/resolver/20050927/resolver-20050927.jar, /root/.m2/repository/org/apache/chemistry/opencmis/chemistry-opencmis-client-bindings/0.11.0/chemistry-opencmis-client-bindings-0.11.0.jar, /root/.m2/repository/org/slf4j/slf4j-api/1.7.5/slf4j-api-1.7.5.jar, /root/.m2/repository/org/alfresco/cmis/client/alfresco-opencmis-extension/1.0/alfresco-opencmis-extension-1.0.jar, /root/.m2/repository/org/springframework/extensions/surf/spring-cmis-framework/5.0.c/spring-cmis-framework-5.0.c.jar, /root/.m2/repository/org/springframework/extensions/surf/spring-webscripts/5.0.c/spring-webscripts-5.0.c.jar, /root/.m2/repository/commons-beanutils/commons-beanutils/1.9.1/commons-beanutils-1.9.1.jar, /root/.m2/repository/org/htmlparser/htmlparser/2.1/htmlparser-2.1.jar, /root/.m2/repository/org/htmlparser/htmllexer/2.1/htmllexer-2.1.jar, /root/.m2/repository/org/springframework/spring-webmvc/3.2.10.RELEASE/spring-webmvc-3.2.10.RELEASE.jar, /root/.m2/repository/org/springframework/spring-expression/3.2.10.RELEASE/spring-expression-3.2.10.RELEASE.jar, /root/.m2/repository/org/mozilla/rhino/1.7R4-alfresco-patched/rhino-1.7R4-alfresco-patched.jar, /root/.m2/repository/com/googlecode/json-simple/json-simple/1.1.1/json-simple-1.1.1.jar, /root/.m2/repository/org/tuckey/urlrewritefilter/4.0.4/urlrewritefilter-4.0.4.jar, /root/.m2/repository/commons-fileupload/commons-fileupload/1.3.1/commons-fileupload-1.3.1.jar, /root/.m2/repository/commons-io/commons-io/2.4/commons-io-2.4.jar, /root/.m2/repository/jaxen/jaxen/1.1.6/jaxen-1.1.6.jar, /root/.m2/repository/org/freemarker/freemarker/2.3.20-alfresco-patched/freemarker-2.3.20-alfresco-patched.jar, /root/.m2/repository/stax/stax-api/1.0.1/stax-api-1.0.1.jar, /root/.m2/repository/com/hazelcast/hazelcast-spring/2.4/hazelcast-spring-2.4.jar, /root/.m2/repository/javax/servlet/jstl/1.2/jstl-1.2.jar, /root/.m2/repository/org/apache/myfaces/core/myfaces-api/1.1.8/myfaces-api-1.1.8.jar, /root/.m2/repository/commons-logging/commons-logging/1.1.3/commons-logging-1.1.3.jar, /root/.m2/repository/org/apache/myfaces/core/myfaces-impl/1.1.8/myfaces-impl-1.1.8.jar, /root/.m2/repository/commons-el/commons-el/1.0/commons-el-1.0.jar, /root/.m2/repository/commons-lang/commons-lang/2.6/commons-lang-2.6.jar, /root/.m2/repository/commons-collections/commons-collections/3.2.1/commons-collections-3.2.1.jar, /root/.m2/repository/commons-digester/commons-digester/2.1/commons-digester-2.1.jar, /root/.m2/repository/com/asual/lesscss/lesscss-engine/1.5.0/lesscss-engine-1.5.0.jar, /root/.m2/repository/org/springframework/extensions/surf/spring-surf-api/5.0.c/spring-surf-api-5.0.c.jar, /root/.m2/repository/org/springframework/extensions/surf/spring-surf/5.0.c/spring-surf-5.0.c.jar, /root/.m2/repository/com/yahoo/platform/yui/yuicompressor/2.4.8-alfresco-patched/yuicompressor-2.4.8-alfresco-patched.jar, /root/.m2/repository/com/googlecode/concurrentlinkedhashmap/concurrentlinkedhashmap-lru/1.2/concurrentlinkedhashmap-lru-1.2.jar, /root/.m2/repository/org/springframework/extensions/surf/spring-webscripts-api/5.0.c/spring-webscripts-api-5.0.c.jar, /root/.m2/repository/javax/servlet/servlet-api/2.5/servlet-api-2.5.jar, /root/.m2/repository/org/alfresco/maven/alfresco-rad/2.0.0/alfresco-rad-2.0.0.jar, /root/.m2/repository/com/tradeshift/junit-remote/3/junit-remote-3.jar, /root/.m2/repository/junit/junit/4.11/junit-4.11.jar, /root/.m2/repository/org/hamcrest/hamcrest-core/1.3/hamcrest-core-1.3.jar, /root/.m2/repository/org/eclipse/jetty/jetty-servlet/7.4.0.v20110414/jetty-servlet-7.4.0.v20110414.jar, /root/.m2/repository/org/eclipse/jetty/jetty-security/7.4.0.v20110414/jetty-security-7.4.0.v20110414.jar, /root/.m2/repository/args4j/args4j/2.0.16/args4j-2.0.16.jar, /root/.m2/repository/org/springframework/spring-test/3.0.6.RELEASE/spring-test-3.0.6.RELEASE.jar, /root/.m2/repository/org/eclipse/jetty/jetty-server/7.4.0.v20110414/jetty-server-7.4.0.v20110414.jar, /root/.m2/repository/org/eclipse/jetty/jetty-continuation/7.4.0.v20110414/jetty-continuation-7.4.0.v20110414.jar, /root/.m2/repository/org/eclipse/jetty/jetty-http/7.4.0.v20110414/jetty-http-7.4.0.v20110414.jar, /root/.m2/repository/org/eclipse/jetty/jetty-io/7.4.0.v20110414/jetty-io-7.4.0.v20110414.jar, /root/.m2/repository/org/eclipse/jetty/jetty-util/7.4.0.v20110414/jetty-util-7.4.0.v20110414.jar]
[DEBUG]   (f) compileSourceRoots = [/home/programues/Documents/metadata_myoffice/content-all-share/src/test/java]
[DEBUG]   (f) compilerId = javac
[DEBUG]   (f) debug = true
[DEBUG]   (f) encoding = UTF-8
[DEBUG]   (f) failOnError = true
[DEBUG]   (f) forceJavacCompilerUse = false
[DEBUG]   (f) fork = false
[DEBUG]   (f) generatedTestSourcesDirectory = /home/programues/Documents/metadata_myoffice/content-all-share/target/generated-test-sources/test-annotations
[DEBUG]   (f) mojoExecution = org.apache.maven.plugins:maven-compiler-plugin:3.2:testCompile {execution: default-testCompile}
[DEBUG]   (f) optimize = false
[DEBUG]   (f) outputDirectory = /home/programues/Documents/metadata_myoffice/content-all-share/target/test-classes
[DEBUG]   (f) project = MavenProject: com.netprojex:content-all-share:1.0-SNAPSHOT @ /home/programues/Documents/metadata_myoffice/content-all-share/pom.xml
[DEBUG]   (f) session = org.apache.maven.execution.MavenSession@21b4a7
[DEBUG]   (f) showDeprecation = false
[DEBUG]   (f) showWarnings = false
[DEBUG]   (f) skipMultiThreadWarning = false
[DEBUG]   (f) source = 1.7
[DEBUG]   (f) staleMillis = 0
[DEBUG]   (f) target = 1.7
[DEBUG]   (f) useIncrementalCompilation = true
[DEBUG]   (f) verbose = false
[DEBUG] -- end configuration --
[DEBUG] Using compiler 'javac'.
[INFO] No sources to compile
[INFO] 
[INFO] --- maven-surefire-plugin:2.18:test (default-test) @ content-all-share ---
[DEBUG] org.apache.maven.plugins:maven-surefire-plugin:jar:2.18:
[DEBUG]    org.apache.maven:maven-plugin-api:jar:2.2.1:compile
[DEBUG]    org.apache.maven.surefire:maven-surefire-common:jar:2.18:compile
[DEBUG]       org.apache.maven.surefire:surefire-booter:jar:2.18:compile
[DEBUG]       org.apache.maven:maven-artifact:jar:2.2.1:compile
[DEBUG]          org.codehaus.plexus:plexus-utils:jar:1.5.15:compile
[DEBUG]       org.apache.maven:maven-plugin-descriptor:jar:2.2.1:compile
[DEBUG]          org.codehaus.plexus:plexus-container-default:jar:1.0-alpha-9-stable-1:compile
[DEBUG]             junit:junit:jar:3.8.1:test (scope managed from compile)
[DEBUG]       org.apache.maven:maven-project:jar:2.2.1:compile
[DEBUG]          org.apache.maven:maven-settings:jar:2.2.1:compile
[DEBUG]          org.apache.maven:maven-profile:jar:2.2.1:compile
[DEBUG]          org.apache.maven:maven-artifact-manager:jar:2.2.1:compile
[DEBUG]             backport-util-concurrent:backport-util-concurrent:jar:3.1:compile
[DEBUG]          org.apache.maven:maven-plugin-registry:jar:2.2.1:compile
[DEBUG]          org.codehaus.plexus:plexus-interpolation:jar:1.11:compile
[DEBUG]       org.apache.maven:maven-model:jar:2.2.1:compile
[DEBUG]       org.apache.maven:maven-core:jar:2.2.1:compile
[DEBUG]          org.apache.maven:maven-plugin-parameter-documenter:jar:2.2.1:compile
[DEBUG]          org.slf4j:slf4j-jdk14:jar:1.5.6:runtime
[DEBUG]             org.slf4j:slf4j-api:jar:1.5.6:runtime
[DEBUG]          org.slf4j:jcl-over-slf4j:jar:1.5.6:runtime
[DEBUG]          org.apache.maven.reporting:maven-reporting-api:jar:3.0:compile (version managed from 2.2.1)
[DEBUG]          org.apache.maven:maven-repository-metadata:jar:2.2.1:compile
[DEBUG]          org.apache.maven:maven-error-diagnostics:jar:2.2.1:compile
[DEBUG]          org.apache.maven:maven-monitor:jar:2.2.1:compile
[DEBUG]          classworlds:classworlds:jar:1.1:compile
[DEBUG]          org.sonatype.plexus:plexus-sec-dispatcher:jar:1.3:compile
[DEBUG]             org.sonatype.plexus:plexus-cipher:jar:1.4:compile
[DEBUG]       org.apache.commons:commons-lang3:jar:3.1:compile
[DEBUG]    org.apache.maven.surefire:surefire-api:jar:2.18:compile
[DEBUG]    org.apache.maven:maven-toolchain:jar:2.2.1:compile
[DEBUG]    org.apache.maven.plugin-tools:maven-plugin-annotations:jar:3.3:compile
[DEBUG] Created new class realm plugin>org.apache.maven.plugins:maven-surefire-plugin:2.18
[DEBUG] Importing foreign packages into class realm plugin>org.apache.maven.plugins:maven-surefire-plugin:2.18
[DEBUG]   Imported:  < project>com.netprojex:content-all-share:1.0-SNAPSHOT
[DEBUG] Populating class realm plugin>org.apache.maven.plugins:maven-surefire-plugin:2.18
[DEBUG]   Included: org.apache.maven.plugins:maven-surefire-plugin:jar:2.18
[DEBUG]   Included: org.apache.maven.surefire:maven-surefire-common:jar:2.18
[DEBUG]   Included: org.apache.maven.surefire:surefire-booter:jar:2.18
[DEBUG]   Included: org.codehaus.plexus:plexus-utils:jar:1.5.15
[DEBUG]   Included: backport-util-concurrent:backport-util-concurrent:jar:3.1
[DEBUG]   Included: org.codehaus.plexus:plexus-interpolation:jar:1.11
[DEBUG]   Included: org.slf4j:slf4j-jdk14:jar:1.5.6
[DEBUG]   Included: org.slf4j:slf4j-api:jar:1.5.6
[DEBUG]   Included: org.slf4j:jcl-over-slf4j:jar:1.5.6
[DEBUG]   Included: org.apache.maven.reporting:maven-reporting-api:jar:3.0
[DEBUG]   Included: org.sonatype.plexus:plexus-sec-dispatcher:jar:1.3
[DEBUG]   Included: org.sonatype.plexus:plexus-cipher:jar:1.4
[DEBUG]   Included: org.apache.commons:commons-lang3:jar:3.1
[DEBUG]   Included: org.apache.maven.surefire:surefire-api:jar:2.18
[DEBUG]   Included: org.apache.maven.plugin-tools:maven-plugin-annotations:jar:3.3
[DEBUG]   Excluded: org.apache.maven:maven-plugin-api:jar:2.2.1
[DEBUG]   Excluded: org.apache.maven:maven-artifact:jar:2.2.1
[DEBUG]   Excluded: org.apache.maven:maven-plugin-descriptor:jar:2.2.1
[DEBUG]   Excluded: org.codehaus.plexus:plexus-container-default:jar:1.0-alpha-9-stable-1
[DEBUG]   Excluded: junit:junit:jar:3.8.1
[DEBUG]   Excluded: org.apache.maven:maven-project:jar:2.2.1
[DEBUG]   Excluded: org.apache.maven:maven-settings:jar:2.2.1
[DEBUG]   Excluded: org.apache.maven:maven-profile:jar:2.2.1
[DEBUG]   Excluded: org.apache.maven:maven-artifact-manager:jar:2.2.1
[DEBUG]   Excluded: org.apache.maven:maven-plugin-registry:jar:2.2.1
[DEBUG]   Excluded: org.apache.maven:maven-model:jar:2.2.1
[DEBUG]   Excluded: org.apache.maven:maven-core:jar:2.2.1
[DEBUG]   Excluded: org.apache.maven:maven-plugin-parameter-documenter:jar:2.2.1
[DEBUG]   Excluded: org.apache.maven:maven-repository-metadata:jar:2.2.1
[DEBUG]   Excluded: org.apache.maven:maven-error-diagnostics:jar:2.2.1
[DEBUG]   Excluded: org.apache.maven:maven-monitor:jar:2.2.1
[DEBUG]   Excluded: classworlds:classworlds:jar:1.1
[DEBUG]   Excluded: org.apache.maven:maven-toolchain:jar:2.2.1
[DEBUG] Configuring mojo org.apache.maven.plugins:maven-surefire-plugin:2.18:test from plugin realm ClassRealm[plugin>org.apache.maven.plugins:maven-surefire-plugin:2.18, parent: sun.misc.Launcher$AppClassLoader@71b456f]
[DEBUG] Configuring mojo 'org.apache.maven.plugins:maven-surefire-plugin:2.18:test' with basic configurator -->
[DEBUG]   (s) additionalClasspathElements = []
[DEBUG]   (s) basedir = /home/programues/Documents/metadata_myoffice/content-all-share
[DEBUG]   (s) childDelegation = false
[DEBUG]   (s) classesDirectory = /home/programues/Documents/metadata_myoffice/content-all-share/target/classes
[DEBUG]   (s) classpathDependencyExcludes = []
[DEBUG]   (s) dependenciesToScan = []
[DEBUG]   (s) disableXmlReport = false
[DEBUG]   (s) enableAssertions = true
[DEBUG]   (f) forkCount = 1
[DEBUG]   (s) forkMode = once
[DEBUG]   (s) junitArtifactName = junit:junit
[DEBUG]   (s) localRepository =        id: local
      url: file:///root/.m2/repository/
   layout: none

[DEBUG]   (f) parallelMavenExecution = false
[DEBUG]   (s) parallelOptimized = true
[DEBUG]   (s) perCoreThreadCount = true
[DEBUG]   (s) pluginArtifactMap = {org.apache.maven.plugins:maven-surefire-plugin=org.apache.maven.plugins:maven-surefire-plugin:maven-plugin:2.18:, org.apache.maven.surefire:maven-surefire-common=org.apache.maven.surefire:maven-surefire-common:jar:2.18:compile, org.apache.maven.surefire:surefire-booter=org.apache.maven.surefire:surefire-booter:jar:2.18:compile, org.codehaus.plexus:plexus-utils=org.codehaus.plexus:plexus-utils:jar:1.5.15:compile, backport-util-concurrent:backport-util-concurrent=backport-util-concurrent:backport-util-concurrent:jar:3.1:compile, org.codehaus.plexus:plexus-interpolation=org.codehaus.plexus:plexus-interpolation:jar:1.11:compile, org.slf4j:slf4j-jdk14=org.slf4j:slf4j-jdk14:jar:1.5.6:runtime, org.slf4j:slf4j-api=org.slf4j:slf4j-api:jar:1.5.6:runtime, org.slf4j:jcl-over-slf4j=org.slf4j:jcl-over-slf4j:jar:1.5.6:runtime, org.apache.maven.reporting:maven-reporting-api=org.apache.maven.reporting:maven-reporting-api:jar:3.0:compile, org.sonatype.plexus:plexus-sec-dispatcher=org.sonatype.plexus:plexus-sec-dispatcher:jar:1.3:compile, org.sonatype.plexus:plexus-cipher=org.sonatype.plexus:plexus-cipher:jar:1.4:compile, org.apache.commons:commons-lang3=org.apache.commons:commons-lang3:jar:3.1:compile, org.apache.maven.surefire:surefire-api=org.apache.maven.surefire:surefire-api:jar:2.18:compile, org.apache.maven.plugin-tools:maven-plugin-annotations=org.apache.maven.plugin-tools:maven-plugin-annotations:jar:3.3:compile}
[DEBUG]   (f) pluginDescriptor = Component Descriptor: role: 'org.apache.maven.plugin.Mojo', implementation: 'org.apache.maven.plugin.surefire.HelpMojo', role hint: 'org.apache.maven.plugins:maven-surefire-plugin:2.18:help'
role: 'org.apache.maven.plugin.Mojo', implementation: 'org.apache.maven.plugin.surefire.SurefirePlugin', role hint: 'org.apache.maven.plugins:maven-surefire-plugin:2.18:test'
---
[DEBUG]   (s) printSummary = true
[DEBUG]   (s) projectArtifactMap = {org.alfresco:share=org.alfresco:share:jar:classes:5.0.c:provided, org.alfresco:alfresco-web-framework-commons=org.alfresco:alfresco-web-framework-commons:jar:classes:5.0.c:provided, org.alfresco:alfresco-jlan-embed=org.alfresco:alfresco-jlan-embed:jar:5.0.c:provided, org.alfresco:alfresco-core=org.alfresco:alfresco-core:jar:5.0.c:provided, commons-codec:commons-codec=commons-codec:commons-codec:jar:1.9:provided, commons-httpclient:commons-httpclient=commons-httpclient:commons-httpclient:jar:3.1:provided, org.safehaus.jug:jug=org.safehaus.jug:jug:jar:asl:2.0.0:provided, log4j:log4j=log4j:log4j:jar:1.2.17:provided, org.json:json=org.json:json:jar:20090211:provided, org.springframework:spring-orm=org.springframework:spring-orm:jar:3.2.10.RELEASE:provided, org.springframework:spring-beans=org.springframework:spring-beans:jar:3.2.10.RELEASE:provided, org.springframework:spring-core=org.springframework:spring-core:jar:3.2.10.RELEASE:provided, org.springframework:spring-jdbc=org.springframework:spring-jdbc:jar:3.2.10.RELEASE:provided, org.springframework:spring-tx=org.springframework:spring-tx:jar:3.2.10.RELEASE:provided, org.springframework.extensions.surf:spring-surf-core-configservice=org.springframework.extensions.surf:spring-surf-core-configservice:jar:5.0.c:provided, org.springframework.extensions.surf:spring-surf-core=org.springframework.extensions.surf:spring-surf-core:jar:5.0.c:provided, org.springframework:spring-context=org.springframework:spring-context:jar:3.2.10.RELEASE:provided, org.springframework:spring-aop=org.springframework:spring-aop:jar:3.2.10.RELEASE:provided, org.springframework:spring-context-support=org.springframework:spring-context-support:jar:3.2.10.RELEASE:provided, org.springframework:spring-web=org.springframework:spring-web:jar:3.2.10.RELEASE:provided, com.sun.xml.bind:jaxb-impl=com.sun.xml.bind:jaxb-impl:jar:2.1.11:provided, dom4j:dom4j=dom4j:dom4j:jar:1.6.1:provided, xml-apis:xml-apis=xml-apis:xml-apis:jar:1.4.01:provided, org.codehaus.guessencoding:guessencoding=org.codehaus.guessencoding:guessencoding:jar:1.0:provided, javax.transaction:jta=javax.transaction:jta:jar:1.0.1b:provided, joda-time:joda-time=joda-time:joda-time:jar:2.3:provided, org.apache.httpcomponents:httpclient=org.apache.httpcomponents:httpclient:jar:4.3.3:provided, org.apache.httpcomponents:httpcore=org.apache.httpcomponents:httpcore:jar:4.3.2:provided, com.hazelcast:hazelcast=com.hazelcast:hazelcast:jar:2.4:provided, org.apache.chemistry.opencmis:chemistry-opencmis-client-impl=org.apache.chemistry.opencmis:chemistry-opencmis-client-impl:jar:0.11.0:provided, org.apache.chemistry.opencmis:chemistry-opencmis-client-api=org.apache.chemistry.opencmis:chemistry-opencmis-client-api:jar:0.11.0:provided, org.apache.chemistry.opencmis:chemistry-opencmis-commons-api=org.apache.chemistry.opencmis:chemistry-opencmis-commons-api:jar:0.11.0:provided, org.apache.chemistry.opencmis:chemistry-opencmis-commons-impl=org.apache.chemistry.opencmis:chemistry-opencmis-commons-impl:jar:0.11.0:provided, org.codehaus.woodstox:woodstox-core-asl=org.codehaus.woodstox:woodstox-core-asl:jar:4.2.0:provided, javax.xml.stream:stax-api=javax.xml.stream:stax-api:jar:1.0-2:provided, org.codehaus.woodstox:stax2-api=org.codehaus.woodstox:stax2-api:jar:3.1.1:provided, org.jvnet.mimepull:mimepull=org.jvnet.mimepull:mimepull:jar:1.9.4:provided, com.sun.xml.ws:jaxws-rt=com.sun.xml.ws:jaxws-rt:jar:2.1.7:provided, javax.xml.ws:jaxws-api=javax.xml.ws:jaxws-api:jar:2.1:provided, javax.xml.bind:jaxb-api=javax.xml.bind:jaxb-api:jar:2.1:provided, com.sun.xml.messaging.saaj:saaj-impl=com.sun.xml.messaging.saaj:saaj-impl:jar:1.3.3:provided, javax.xml.soap:saaj-api=javax.xml.soap:saaj-api:jar:1.3:provided, com.sun.xml.stream.buffer:streambuffer=com.sun.xml.stream.buffer:streambuffer:jar:0.9:provided, javax.activation:activation=javax.activation:activation:jar:1.1:provided, org.jvnet.staxex:stax-ex=org.jvnet.staxex:stax-ex:jar:1.2:provided, com.sun.org.apache.xml.internal:resolver=com.sun.org.apache.xml.internal:resolver:jar:20050927:provided, org.apache.chemistry.opencmis:chemistry-opencmis-client-bindings=org.apache.chemistry.opencmis:chemistry-opencmis-client-bindings:jar:0.11.0:provided, org.slf4j:slf4j-api=org.slf4j:slf4j-api:jar:1.7.5:compile, org.alfresco.cmis.client:alfresco-opencmis-extension=org.alfresco.cmis.client:alfresco-opencmis-extension:jar:1.0:provided, org.springframework.extensions.surf:spring-cmis-framework=org.springframework.extensions.surf:spring-cmis-framework:jar:5.0.c:provided, org.springframework.extensions.surf:spring-webscripts=org.springframework.extensions.surf:spring-webscripts:jar:5.0.c:provided, commons-beanutils:commons-beanutils=commons-beanutils:commons-beanutils:jar:1.9.1:provided, org.htmlparser:htmlparser=org.htmlparser:htmlparser:jar:2.1:provided, org.htmlparser:htmllexer=org.htmlparser:htmllexer:jar:2.1:provided, org.springframework:spring-webmvc=org.springframework:spring-webmvc:jar:3.2.10.RELEASE:provided, org.springframework:spring-expression=org.springframework:spring-expression:jar:3.2.10.RELEASE:provided, org.mozilla:rhino=org.mozilla:rhino:jar:1.7R4-alfresco-patched:provided, com.googlecode.json-simple:json-simple=com.googlecode.json-simple:json-simple:jar:1.1.1:provided, org.tuckey:urlrewritefilter=org.tuckey:urlrewritefilter:jar:4.0.4:provided, commons-fileupload:commons-fileupload=commons-fileupload:commons-fileupload:jar:1.3.1:provided, commons-io:commons-io=commons-io:commons-io:jar:2.4:provided, jaxen:jaxen=jaxen:jaxen:jar:1.1.6:provided, org.freemarker:freemarker=org.freemarker:freemarker:jar:2.3.20-alfresco-patched:provided, stax:stax-api=stax:stax-api:jar:1.0.1:provided, com.hazelcast:hazelcast-spring=com.hazelcast:hazelcast-spring:jar:2.4:provided, javax.servlet:jstl=javax.servlet:jstl:jar:1.2:provided, org.apache.myfaces.core:myfaces-api=org.apache.myfaces.core:myfaces-api:jar:1.1.8:provided, commons-logging:commons-logging=commons-logging:commons-logging:jar:1.1.3:provided, org.apache.myfaces.core:myfaces-impl=org.apache.myfaces.core:myfaces-impl:jar:1.1.8:provided, commons-el:commons-el=commons-el:commons-el:jar:1.0:provided, commons-lang:commons-lang=commons-lang:commons-lang:jar:2.6:provided, commons-collections:commons-collections=commons-collections:commons-collections:jar:3.2.1:provided, commons-digester:commons-digester=commons-digester:commons-digester:jar:2.1:provided, org.dojotoolkit:dojo=org.dojotoolkit:dojo:zip:1.9.0:provided, com.yahoo.platform.yui:yui=com.yahoo.platform.yui:yui:zip:2.9.0-alfresco-20140211:provided, com.asual.lesscss:lesscss-engine=com.asual.lesscss:lesscss-engine:jar:1.5.0:provided, org.springframework.extensions.surf:spring-surf-api=org.springframework.extensions.surf:spring-surf-api:jar:5.0.c:provided, org.springframework.extensions.surf:spring-surf=org.springframework.extensions.surf:spring-surf:jar:5.0.c:provided, com.yahoo.platform.yui:yuicompressor=com.yahoo.platform.yui:yuicompressor:jar:2.4.8-alfresco-patched:provided, com.googlecode.concurrentlinkedhashmap:concurrentlinkedhashmap-lru=com.googlecode.concurrentlinkedhashmap:concurrentlinkedhashmap-lru:jar:1.2:provided, org.springframework.extensions.surf:spring-webscripts-api=org.springframework.extensions.surf:spring-webscripts-api:jar:5.0.c:provided, javax.servlet:servlet-api=javax.servlet:servlet-api:jar:2.5:provided, org.alfresco.maven:alfresco-rad=org.alfresco.maven:alfresco-rad:jar:2.0.0:compile, com.tradeshift:junit-remote=com.tradeshift:junit-remote:jar:3:compile, junit:junit=junit:junit:jar:4.11:compile, org.hamcrest:hamcrest-core=org.hamcrest:hamcrest-core:jar:1.3:compile, org.eclipse.jetty:jetty-servlet=org.eclipse.jetty:jetty-servlet:jar:7.4.0.v20110414:compile, org.eclipse.jetty:jetty-security=org.eclipse.jetty:jetty-security:jar:7.4.0.v20110414:compile, args4j:args4j=args4j:args4j:jar:2.0.16:compile, org.springframework:spring-test=org.springframework:spring-test:jar:3.0.6.RELEASE:compile, org.eclipse.jetty:jetty-server=org.eclipse.jetty:jetty-server:jar:7.4.0.v20110414:compile, org.eclipse.jetty:jetty-continuation=org.eclipse.jetty:jetty-continuation:jar:7.4.0.v20110414:compile, org.eclipse.jetty:jetty-http=org.eclipse.jetty:jetty-http:jar:7.4.0.v20110414:compile, org.eclipse.jetty:jetty-io=org.eclipse.jetty:jetty-io:jar:7.4.0.v20110414:compile, org.eclipse.jetty:jetty-util=org.eclipse.jetty:jetty-util:jar:7.4.0.v20110414:compile}
[DEBUG]   (s) redirectTestOutputToFile = false
[DEBUG]   (s) remoteRepositories = [       id: sonatype-snapshots
      url: https://oss.sonatype.org/content/repositories/snapshots
   layout: default
snapshots: [enabled => true, update => daily]
 releases: [enabled => true, update => daily]
,        id: alfresco-plugin-public
      url: https://artifacts.alfresco.com/nexus/content/groups/public
   layout: default
snapshots: [enabled => true, update => daily]
 releases: [enabled => true, update => daily]
,        id: alfresco-plugin-public-snapshots
      url: https://artifacts.alfresco.com/nexus/content/groups/public-snapshots
   layout: default
snapshots: [enabled => true, update => daily]
 releases: [enabled => true, update => daily]
,        id: central
      url: http://repo.maven.apache.org/maven2
   layout: default
snapshots: [enabled => false, update => daily]
 releases: [enabled => true, update => never]
]
[DEBUG]   (s) reportFormat = brief
[DEBUG]   (s) reportsDirectory = /home/programues/Documents/metadata_myoffice/content-all-share/target/surefire-reports
[DEBUG]   (f) rerunFailingTestsCount = 0
[DEBUG]   (f) reuseForks = true
[DEBUG]   (s) runOrder = filesystem
[DEBUG]   (s) skip = false
[DEBUG]   (s) skipTests = true
[DEBUG]   (s) testClassesDirectory = /home/programues/Documents/metadata_myoffice/content-all-share/target/test-classes
[DEBUG]   (s) testFailureIgnore = false
[DEBUG]   (s) testNGArtifactName = org.testng:testng
[DEBUG]   (s) testSourceDirectory = /home/programues/Documents/metadata_myoffice/content-all-share/src/test/java
[DEBUG]   (s) threadCountClasses = 0
[DEBUG]   (s) threadCountMethods = 0
[DEBUG]   (s) threadCountSuites = 0
[DEBUG]   (s) trimStackTrace = true
[DEBUG]   (s) useFile = true
[DEBUG]   (s) useManifestOnlyJar = true
[DEBUG]   (s) useSystemClassLoader = true
[DEBUG]   (s) useUnlimitedThreads = false
[DEBUG]   (s) workingDirectory = /home/programues/Documents/metadata_myoffice/content-all-share
[DEBUG]   (s) project = MavenProject: com.netprojex:content-all-share:1.0-SNAPSHOT @ /home/programues/Documents/metadata_myoffice/content-all-share/pom.xml
[DEBUG]   (s) session = org.apache.maven.execution.MavenSession@21b4a7
[DEBUG] -- end configuration --
[INFO] Tests are skipped.
[INFO] 
[INFO] --- maven-dependency-plugin:2.9:unpack (unpack-alfresco) @ content-all-share ---
[DEBUG] org.apache.maven.plugins:maven-dependency-plugin:jar:2.9:
[DEBUG]    org.apache.maven:maven-artifact:jar:2.2.1:compile
[DEBUG]    org.apache.maven:maven-plugin-api:jar:2.2.1:compile
[DEBUG]    org.apache.maven:maven-project:jar:2.2.1:compile
[DEBUG]       org.apache.maven:maven-settings:jar:2.2.1:compile
[DEBUG]       org.apache.maven:maven-profile:jar:2.2.1:compile
[DEBUG]       org.apache.maven:maven-plugin-registry:jar:2.2.1:compile
[DEBUG]       org.codehaus.plexus:plexus-interpolation:jar:1.11:compile
[DEBUG]       org.codehaus.plexus:plexus-container-default:jar:1.0-alpha-9-stable-1:compile
[DEBUG]          junit:junit:jar:3.8.1:compile
[DEBUG]    org.apache.maven:maven-model:jar:2.2.1:compile
[DEBUG]    org.apache.maven:maven-core:jar:2.2.1:compile
[DEBUG]       org.apache.maven:maven-plugin-parameter-documenter:jar:2.2.1:compile
[DEBUG]       org.slf4j:slf4j-jdk14:jar:1.5.6:runtime
[DEBUG]          org.slf4j:slf4j-api:jar:1.5.6:runtime
[DEBUG]       org.slf4j:jcl-over-slf4j:jar:1.5.6:runtime
[DEBUG]       org.apache.maven:maven-error-diagnostics:jar:2.2.1:compile
[DEBUG]       commons-cli:commons-cli:jar:1.2:compile
[DEBUG]       org.apache.maven:maven-plugin-descriptor:jar:2.2.1:compile
[DEBUG]       org.codehaus.plexus:plexus-interactivity-api:jar:1.0-alpha-4:compile
[DEBUG]       org.apache.maven:maven-monitor:jar:2.2.1:compile
[DEBUG]       org.sonatype.plexus:plexus-sec-dispatcher:jar:1.3:compile
[DEBUG]          org.sonatype.plexus:plexus-cipher:jar:1.4:compile
[DEBUG]    org.apache.maven:maven-artifact-manager:jar:2.2.1:compile
[DEBUG]       backport-util-concurrent:backport-util-concurrent:jar:3.1:compile
[DEBUG]    org.apache.maven:maven-repository-metadata:jar:2.2.1:compile
[DEBUG]    org.apache.maven.reporting:maven-reporting-api:jar:3.0:compile
[DEBUG]    org.apache.maven.reporting:maven-reporting-impl:jar:2.2:compile
[DEBUG]       org.apache.maven.doxia:doxia-core:jar:1.2:compile
[DEBUG]          xerces:xercesImpl:jar:2.9.1:compile
[DEBUG]             xml-apis:xml-apis:jar:1.3.04:compile
[DEBUG]          org.apache.httpcomponents:httpclient:jar:4.0.2:compile
[DEBUG]             org.apache.httpcomponents:httpcore:jar:4.0.1:compile
[DEBUG]             commons-codec:commons-codec:jar:1.3:compile
[DEBUG]       commons-validator:commons-validator:jar:1.3.1:compile
[DEBUG]          commons-beanutils:commons-beanutils:jar:1.7.0:compile
[DEBUG]          commons-digester:commons-digester:jar:1.6:compile
[DEBUG]          commons-logging:commons-logging:jar:1.0.4:compile
[DEBUG]    commons-io:commons-io:jar:1.4:compile
[DEBUG]    org.apache.maven.doxia:doxia-sink-api:jar:1.4:compile
[DEBUG]       org.apache.maven.doxia:doxia-logging-api:jar:1.4:compile
[DEBUG]    org.apache.maven.doxia:doxia-site-renderer:jar:1.4:compile
[DEBUG]       org.apache.maven.doxia:doxia-decoration-model:jar:1.4:compile
[DEBUG]       org.apache.maven.doxia:doxia-module-xhtml:jar:1.4:compile
[DEBUG]       org.apache.maven.doxia:doxia-module-fml:jar:1.4:compile
[DEBUG]       org.codehaus.plexus:plexus-i18n:jar:1.0-beta-7:compile
[DEBUG]       org.codehaus.plexus:plexus-velocity:jar:1.1.7:compile
[DEBUG]       org.apache.velocity:velocity:jar:1.5:compile
[DEBUG]          oro:oro:jar:2.0.8:compile
[DEBUG]       org.apache.velocity:velocity-tools:jar:2.0:compile
[DEBUG]          commons-chain:commons-chain:jar:1.1:compile
[DEBUG]          dom4j:dom4j:jar:1.1:compile
[DEBUG]          sslext:sslext:jar:1.2-0:compile
[DEBUG]          org.apache.struts:struts-core:jar:1.3.8:compile
[DEBUG]             antlr:antlr:jar:2.7.2:compile
[DEBUG]          org.apache.struts:struts-taglib:jar:1.3.8:compile
[DEBUG]          org.apache.struts:struts-tiles:jar:1.3.8:compile
[DEBUG]       org.codehaus.plexus:plexus-component-annotations:jar:1.5.5:compile
[DEBUG]    org.codehaus.plexus:plexus-archiver:jar:2.4.4:compile
[DEBUG]       org.apache.commons:commons-compress:jar:1.5:compile
[DEBUG]          org.tukaani:xz:jar:1.2:compile
[DEBUG]    org.codehaus.plexus:plexus-utils:jar:3.0.15:compile
[DEBUG]    org.apache.maven.shared:file-management:jar:1.2.1:compile
[DEBUG]       org.apache.maven.shared:maven-shared-io:jar:1.1:compile
[DEBUG]          org.apache.maven.wagon:wagon-provider-api:jar:1.0-alpha-6:compile
[DEBUG]    org.codehaus.plexus:plexus-io:jar:2.0.9:compile
[DEBUG]    org.apache.maven.shared:maven-dependency-analyzer:jar:1.5:compile
[DEBUG]       org.ow2.asm:asm:jar:5.0.2:compile
[DEBUG]    org.apache.maven.shared:maven-dependency-tree:jar:2.2:compile
[DEBUG]       org.eclipse.aether:aether-util:jar:0.9.0.M2:compile
[DEBUG]    org.apache.maven.shared:maven-common-artifact-filters:jar:1.4:compile
[DEBUG]    org.apache.maven.shared:maven-invoker:jar:2.1.1:compile
[DEBUG]    commons-lang:commons-lang:jar:2.6:compile
[DEBUG]    commons-collections:commons-collections:jar:3.2.1:compile
[DEBUG]    classworlds:classworlds:jar:1.1:compile
[DEBUG] Created new class realm plugin>org.apache.maven.plugins:maven-dependency-plugin:2.9
[DEBUG] Importing foreign packages into class realm plugin>org.apache.maven.plugins:maven-dependency-plugin:2.9
[DEBUG]   Imported:  < project>com.netprojex:content-all-share:1.0-SNAPSHOT
[DEBUG] Populating class realm plugin>org.apache.maven.plugins:maven-dependency-plugin:2.9
[DEBUG]   Included: org.apache.maven.plugins:maven-dependency-plugin:jar:2.9
[DEBUG]   Included: org.codehaus.plexus:plexus-interpolation:jar:1.11
[DEBUG]   Included: junit:junit:jar:3.8.1
[DEBUG]   Included: org.slf4j:slf4j-jdk14:jar:1.5.6
[DEBUG]   Included: org.slf4j:slf4j-api:jar:1.5.6
[DEBUG]   Included: org.slf4j:jcl-over-slf4j:jar:1.5.6
[DEBUG]   Included: commons-cli:commons-cli:jar:1.2
[DEBUG]   Included: org.codehaus.plexus:plexus-interactivity-api:jar:1.0-alpha-4
[DEBUG]   Included: org.sonatype.plexus:plexus-sec-dispatcher:jar:1.3
[DEBUG]   Included: org.sonatype.plexus:plexus-cipher:jar:1.4
[DEBUG]   Included: backport-util-concurrent:backport-util-concurrent:jar:3.1
[DEBUG]   Included: org.apache.maven.reporting:maven-reporting-api:jar:3.0
[DEBUG]   Included: org.apache.maven.reporting:maven-reporting-impl:jar:2.2
[DEBUG]   Included: org.apache.maven.doxia:doxia-core:jar:1.2
[DEBUG]   Included: xerces:xercesImpl:jar:2.9.1
[DEBUG]   Included: xml-apis:xml-apis:jar:1.3.04
[DEBUG]   Included: org.apache.httpcomponents:httpclient:jar:4.0.2
[DEBUG]   Included: org.apache.httpcomponents:httpcore:jar:4.0.1
[DEBUG]   Included: commons-codec:commons-codec:jar:1.3
[DEBUG]   Included: commons-validator:commons-validator:jar:1.3.1
[DEBUG]   Included: commons-beanutils:commons-beanutils:jar:1.7.0
[DEBUG]   Included: commons-digester:commons-digester:jar:1.6
[DEBUG]   Included: commons-logging:commons-logging:jar:1.0.4
[DEBUG]   Included: commons-io:commons-io:jar:1.4
[DEBUG]   Included: org.apache.maven.doxia:doxia-sink-api:jar:1.4
[DEBUG]   Included: org.apache.maven.doxia:doxia-logging-api:jar:1.4
[DEBUG]   Included: org.apache.maven.doxia:doxia-site-renderer:jar:1.4
[DEBUG]   Included: org.apache.maven.doxia:doxia-decoration-model:jar:1.4
[DEBUG]   Included: org.apache.maven.doxia:doxia-module-xhtml:jar:1.4
[DEBUG]   Included: org.apache.maven.doxia:doxia-module-fml:jar:1.4
[DEBUG]   Included: org.codehaus.plexus:plexus-i18n:jar:1.0-beta-7
[DEBUG]   Included: org.codehaus.plexus:plexus-velocity:jar:1.1.7
[DEBUG]   Included: org.apache.velocity:velocity:jar:1.5
[DEBUG]   Included: oro:oro:jar:2.0.8
[DEBUG]   Included: org.apache.velocity:velocity-tools:jar:2.0
[DEBUG]   Included: commons-chain:commons-chain:jar:1.1
[DEBUG]   Included: dom4j:dom4j:jar:1.1
[DEBUG]   Included: sslext:sslext:jar:1.2-0
[DEBUG]   Included: org.apache.struts:struts-core:jar:1.3.8
[DEBUG]   Included: antlr:antlr:jar:2.7.2
[DEBUG]   Included: org.apache.struts:struts-taglib:jar:1.3.8
[DEBUG]   Included: org.apache.struts:struts-tiles:jar:1.3.8
[DEBUG]   Included: org.codehaus.plexus:plexus-component-annotations:jar:1.5.5
[DEBUG]   Included: org.codehaus.plexus:plexus-archiver:jar:2.4.4
[DEBUG]   Included: org.apache.commons:commons-compress:jar:1.5
[DEBUG]   Included: org.tukaani:xz:jar:1.2
[DEBUG]   Included: org.codehaus.plexus:plexus-utils:jar:3.0.15
[DEBUG]   Included: org.apache.maven.shared:file-management:jar:1.2.1
[DEBUG]   Included: org.apache.maven.shared:maven-shared-io:jar:1.1
[DEBUG]   Included: org.codehaus.plexus:plexus-io:jar:2.0.9
[DEBUG]   Included: org.apache.maven.shared:maven-dependency-analyzer:jar:1.5
[DEBUG]   Included: org.ow2.asm:asm:jar:5.0.2
[DEBUG]   Included: org.apache.maven.shared:maven-dependency-tree:jar:2.2
[DEBUG]   Included: org.eclipse.aether:aether-util:jar:0.9.0.M2
[DEBUG]   Included: org.apache.maven.shared:maven-common-artifact-filters:jar:1.4
[DEBUG]   Included: org.apache.maven.shared:maven-invoker:jar:2.1.1
[DEBUG]   Included: commons-lang:commons-lang:jar:2.6
[DEBUG]   Included: commons-collections:commons-collections:jar:3.2.1
[DEBUG]   Excluded: org.apache.maven:maven-artifact:jar:2.2.1
[DEBUG]   Excluded: org.apache.maven:maven-plugin-api:jar:2.2.1
[DEBUG]   Excluded: org.apache.maven:maven-project:jar:2.2.1
[DEBUG]   Excluded: org.apache.maven:maven-settings:jar:2.2.1
[DEBUG]   Excluded: org.apache.maven:maven-profile:jar:2.2.1
[DEBUG]   Excluded: org.apache.maven:maven-plugin-registry:jar:2.2.1
[DEBUG]   Excluded: org.codehaus.plexus:plexus-container-default:jar:1.0-alpha-9-stable-1
[DEBUG]   Excluded: org.apache.maven:maven-model:jar:2.2.1
[DEBUG]   Excluded: org.apache.maven:maven-core:jar:2.2.1
[DEBUG]   Excluded: org.apache.maven:maven-plugin-parameter-documenter:jar:2.2.1
[DEBUG]   Excluded: org.apache.maven:maven-error-diagnostics:jar:2.2.1
[DEBUG]   Excluded: org.apache.maven:maven-plugin-descriptor:jar:2.2.1
[DEBUG]   Excluded: org.apache.maven:maven-monitor:jar:2.2.1
[DEBUG]   Excluded: org.apache.maven:maven-artifact-manager:jar:2.2.1
[DEBUG]   Excluded: org.apache.maven:maven-repository-metadata:jar:2.2.1
[DEBUG]   Excluded: org.apache.maven.wagon:wagon-provider-api:jar:1.0-alpha-6
[DEBUG]   Excluded: classworlds:classworlds:jar:1.1
[DEBUG] Configuring mojo org.apache.maven.plugins:maven-dependency-plugin:2.9:unpack from plugin realm ClassRealm[plugin>org.apache.maven.plugins:maven-dependency-plugin:2.9, parent: sun.misc.Launcher$AppClassLoader@71b456f]
[DEBUG] Configuring mojo 'org.apache.maven.plugins:maven-dependency-plugin:2.9:unpack' with basic configurator -->
[DEBUG]   (s) groupId = org.alfresco
[DEBUG]   (s) artifactId = share
[DEBUG]   (s) type = war
[DEBUG]   (s) version = 5.0.c
[DEBUG]   (s) artifactItems = [org.alfresco:share:5.0.c:war]
[DEBUG]   (f) ignorePermissions = false
[DEBUG]   (s) local =        id: local
      url: file:///root/.m2/repository/
   layout: none

[DEBUG]   (s) markersDirectory = /home/programues/Documents/metadata_myoffice/content-all-share/target/dependency-maven-plugin-markers
[DEBUG]   (f) outputAbsoluteArtifactFilename = false
[DEBUG]   (s) outputDirectory = /home/programues/Documents/metadata_myoffice/content-all-share/target/content-all-share-war
[DEBUG]   (s) overWriteIfNewer = true
[DEBUG]   (s) overWriteReleases = false
[DEBUG]   (s) overWriteSnapshots = false
[DEBUG]   (f) project = MavenProject: com.netprojex:content-all-share:1.0-SNAPSHOT @ /home/programues/Documents/metadata_myoffice/content-all-share/pom.xml
[DEBUG]   (f) reactorProjects = [MavenProject: com.netprojex:content-all-share:1.0-SNAPSHOT @ /home/programues/Documents/metadata_myoffice/content-all-share/pom.xml]
[DEBUG]   (s) remoteRepos = [       id: sonatype-snapshots
      url: https://oss.sonatype.org/content/repositories/snapshots/
   layout: default
snapshots: [enabled => true, update => daily]
 releases: [enabled => false, update => daily]
,        id: alfresco-public
      url: https://artifacts.alfresco.com/nexus/content/groups/public
   layout: default
snapshots: [enabled => true, update => daily]
 releases: [enabled => true, update => daily]
,        id: alfresco-public-snapshots
      url: https://artifacts.alfresco.com/nexus/content/groups/public-snapshots
   layout: default
snapshots: [enabled => true, update => daily]
 releases: [enabled => true, update => daily]
,        id: central
      url: http://repo.maven.apache.org/maven2
   layout: default
snapshots: [enabled => false, update => daily]
 releases: [enabled => true, update => daily]
]
[DEBUG]   (f) silent = false
[DEBUG]   (s) skip = false
[DEBUG]   (s) useJvmChmod = true
[DEBUG] -- end configuration --
[INFO] Configured Artifact: org.alfresco:share:5.0.c:war
[INFO] share-5.0.c.war already unpacked.
[INFO] 
[INFO] --- alfresco-maven-plugin:2.0.0:amp (default-amp) @ content-all-share ---
[DEBUG] Configuring mojo org.alfresco.maven.plugin:alfresco-maven-plugin:2.0.0:amp from plugin realm ClassRealm[plugin>org.alfresco.maven.plugin:alfresco-maven-plugin:2.0.0, parent: sun.misc.Launcher$AppClassLoader@71b456f]
[DEBUG] Configuring mojo 'org.alfresco.maven.plugin:alfresco-maven-plugin:2.0.0:amp' with basic configurator -->
[DEBUG]   (f) ampBuildDirectory = /home/programues/Documents/metadata_myoffice/content-all-share/target/content-all-share
[DEBUG]   (f) ampFinalDir = /home/programues/Documents/metadata_myoffice/content-all-share/target
[DEBUG]   (f) ampFinalName = content-all-share
[DEBUG]   (f) attachClasses = false
[DEBUG]   (f) attachConfig = false
[DEBUG]   (f) classesDirectory = /home/programues/Documents/metadata_myoffice/content-all-share/target/classes
[DEBUG]   (f) includeDependencies = true
[DEBUG]   (f) includeWebResources = false
[DEBUG]   (f) project = MavenProject: com.netprojex:content-all-share:1.0-SNAPSHOT @ /home/programues/Documents/metadata_myoffice/content-all-share/pom.xml
[DEBUG]   (f) session = org.apache.maven.execution.MavenSession@21b4a7
[DEBUG] -- end configuration --
[INFO] Building jar: /home/programues/Documents/metadata_myoffice/content-all-share/target/content-all-share/lib/content-all-share.jar
[INFO] Adding directory to AMP package [ '/home/programues/Documents/metadata_myoffice/content-all-share/target/content-all-share' '']
[INFO] Building amp: /home/programues/Documents/metadata_myoffice/content-all-share/target/content-all-share.amp
[INFO] 
[INFO] --- alfresco-maven-plugin:2.0.0:install (amps-to-war-overlay) @ content-all-share ---
[DEBUG] Configuring mojo org.alfresco.maven.plugin:alfresco-maven-plugin:2.0.0:install from plugin realm ClassRealm[plugin>org.alfresco.maven.plugin:alfresco-maven-plugin:2.0.0, parent: sun.misc.Launcher$AppClassLoader@71b456f]
[DEBUG] Configuring mojo 'org.alfresco.maven.plugin:alfresco-maven-plugin:2.0.0:install' with basic configurator -->
[DEBUG]   (f) ampLocation = /home/programues/Documents/metadata_myoffice/content-all-share/target/content-all-share.amp
[DEBUG]   (f) backup = false
[DEBUG]   (f) force = true
[DEBUG]   (f) skipAmpInstallation = false
[DEBUG]   (f) skipWarManifestCheck = false
[DEBUG]   (f) verbose = false
[DEBUG]   (f) warLocation = /home/programues/Documents/metadata_myoffice/content-all-share/target/content-all-share-war
[DEBUG] -- end configuration --
   - WARNING: The file '/WEB-INF/lib/slf4j-api-1.7.5.jar' is being overwritten by this module. The original has been backed-up to '/WEB-INF/classes/alfresco/module/backup/cd62d7a5-fcf6-11e6-a054-2d46cc6d9e04.bin'
[INFO] 
[INFO] >>> tomcat7-maven-plugin:2.2:run (run-embedded) @ content-all-share >>>
[DEBUG] com.netprojex:content-all-share:amp:1.0-SNAPSHOT
[DEBUG]    org.alfresco:share:jar:classes:5.0.c:provided
[DEBUG]       org.alfresco:alfresco-web-framework-commons:jar:classes:5.0.c:provided (scope managed from compile by org.alfresco:alfresco-platform-distribution:5.0.c)
[DEBUG]       org.alfresco:alfresco-jlan-embed:jar:5.0.c:provided (scope managed from compile by org.alfresco:alfresco-platform-distribution:5.0.c)
[DEBUG]          org.alfresco:alfresco-core:jar:5.0.c:provided (scope managed from compile by org.alfresco:alfresco-platform-distribution:5.0.c)
[DEBUG]             commons-codec:commons-codec:jar:1.9:provided
[DEBUG]             commons-httpclient:commons-httpclient:jar:3.1:provided
[DEBUG]             org.safehaus.jug:jug:jar:asl:2.0.0:provided
[DEBUG]             log4j:log4j:jar:1.2.17:provided
[DEBUG]             org.json:json:jar:20090211:provided
[DEBUG]             org.springframework:spring-orm:jar:3.2.10.RELEASE:provided
[DEBUG]                org.springframework:spring-beans:jar:3.2.10.RELEASE:provided
[DEBUG]                org.springframework:spring-core:jar:3.2.10.RELEASE:provided
[DEBUG]                org.springframework:spring-jdbc:jar:3.2.10.RELEASE:provided
[DEBUG]                org.springframework:spring-tx:jar:3.2.10.RELEASE:provided
[DEBUG]             org.springframework.extensions.surf:spring-surf-core-configservice:jar:5.0.c:provided
[DEBUG]                org.springframework.extensions.surf:spring-surf-core:jar:5.0.c:provided
[DEBUG]                org.springframework:spring-context:jar:3.2.10.RELEASE:provided
[DEBUG]                   org.springframework:spring-aop:jar:3.2.10.RELEASE:provided
[DEBUG]                org.springframework:spring-context-support:jar:3.2.10.RELEASE:provided
[DEBUG]                org.springframework:spring-web:jar:3.2.10.RELEASE:provided
[DEBUG]             com.sun.xml.bind:jaxb-impl:jar:2.1.11:provided
[DEBUG]             dom4j:dom4j:jar:1.6.1:provided
[DEBUG]                xml-apis:xml-apis:jar:1.4.01:provided (version managed from 1.0.b2 by org.alfresco:alfresco-parent:5.0.c)
[DEBUG]             org.codehaus.guessencoding:guessencoding:jar:1.0:provided
[DEBUG]             javax.transaction:jta:jar:1.0.1b:provided
[DEBUG]             joda-time:joda-time:jar:2.3:provided
[DEBUG]             org.apache.httpcomponents:httpclient:jar:4.3.3:provided
[DEBUG]                org.apache.httpcomponents:httpcore:jar:4.3.2:provided
[DEBUG]          com.hazelcast:hazelcast:jar:2.4:provided
[DEBUG]       org.apache.chemistry.opencmis:chemistry-opencmis-client-impl:jar:0.11.0:provided
[DEBUG]          org.apache.chemistry.opencmis:chemistry-opencmis-client-api:jar:0.11.0:provided
[DEBUG]          org.apache.chemistry.opencmis:chemistry-opencmis-commons-api:jar:0.11.0:provided
[DEBUG]          org.apache.chemistry.opencmis:chemistry-opencmis-commons-impl:jar:0.11.0:provided
[DEBUG]             org.codehaus.woodstox:woodstox-core-asl:jar:4.2.0:provided
[DEBUG]                javax.xml.stream:stax-api:jar:1.0-2:provided
[DEBUG]                org.codehaus.woodstox:stax2-api:jar:3.1.1:provided
[DEBUG]             org.jvnet.mimepull:mimepull:jar:1.9.4:provided
[DEBUG]             com.sun.xml.ws:jaxws-rt:jar:2.1.7:provided
[DEBUG]                javax.xml.ws:jaxws-api:jar:2.1:provided
[DEBUG]                   javax.xml.bind:jaxb-api:jar:2.1:provided
[DEBUG]                com.sun.xml.messaging.saaj:saaj-impl:jar:1.3.3:provided
[DEBUG]                   javax.xml.soap:saaj-api:jar:1.3:provided
[DEBUG]                com.sun.xml.stream.buffer:streambuffer:jar:0.9:provided
[DEBUG]                   javax.activation:activation:jar:1.1:provided
[DEBUG]                org.jvnet.staxex:stax-ex:jar:1.2:provided
[DEBUG]                com.sun.org.apache.xml.internal:resolver:jar:20050927:provided
[DEBUG]          org.apache.chemistry.opencmis:chemistry-opencmis-client-bindings:jar:0.11.0:provided
[DEBUG]          org.slf4j:slf4j-api:jar:1.7.5:compile
[DEBUG]       org.alfresco.cmis.client:alfresco-opencmis-extension:jar:1.0:provided
[DEBUG]       org.springframework.extensions.surf:spring-cmis-framework:jar:5.0.c:provided
[DEBUG]          org.springframework.extensions.surf:spring-webscripts:jar:5.0.c:provided
[DEBUG]             commons-beanutils:commons-beanutils:jar:1.9.1:provided
[DEBUG]             org.htmlparser:htmlparser:jar:2.1:provided
[DEBUG]                org.htmlparser:htmllexer:jar:2.1:provided
[DEBUG]             org.springframework:spring-webmvc:jar:3.2.10.RELEASE:provided
[DEBUG]                org.springframework:spring-expression:jar:3.2.10.RELEASE:provided
[DEBUG]       org.mozilla:rhino:jar:1.7R4-alfresco-patched:provided
[DEBUG]       com.googlecode.json-simple:json-simple:jar:1.1.1:provided
[DEBUG]       org.tuckey:urlrewritefilter:jar:4.0.4:provided
[DEBUG]       commons-fileupload:commons-fileupload:jar:1.3.1:provided
[DEBUG]          commons-io:commons-io:jar:2.4:provided (version managed from 2.2 by org.alfresco:alfresco-parent:5.0.c)
[DEBUG]       jaxen:jaxen:jar:1.1.6:provided
[DEBUG]       org.freemarker:freemarker:jar:2.3.20-alfresco-patched:provided
[DEBUG]       stax:stax-api:jar:1.0.1:provided
[DEBUG]       com.hazelcast:hazelcast-spring:jar:2.4:provided
[DEBUG]       javax.servlet:jstl:jar:1.2:provided
[DEBUG]       org.apache.myfaces.core:myfaces-api:jar:1.1.8:provided
[DEBUG]          commons-logging:commons-logging:jar:1.1.3:provided (version managed from 1.1.1 by org.alfresco:alfresco-parent:5.0.c)
[DEBUG]       org.apache.myfaces.core:myfaces-impl:jar:1.1.8:provided
[DEBUG]          commons-el:commons-el:jar:1.0:provided
[DEBUG]          commons-lang:commons-lang:jar:2.6:provided (version managed from 2.1 by org.alfresco:alfresco-parent:5.0.c)
[DEBUG]          commons-collections:commons-collections:jar:3.2.1:provided (version managed from 3.1 by org.alfresco:alfresco-parent:5.0.c)
[DEBUG]          commons-digester:commons-digester:jar:2.1:provided (version managed from 1.8 by org.alfresco:alfresco-parent:5.0.c)
[DEBUG]       org.dojotoolkit:dojo:zip:1.9.0:provided
[DEBUG]       com.yahoo.platform.yui:yui:zip:2.9.0-alfresco-20140211:provided
[DEBUG]       com.asual.lesscss:lesscss-engine:jar:1.5.0:provided
[DEBUG]    org.springframework.extensions.surf:spring-surf-api:jar:5.0.c:provided
[DEBUG]       org.springframework.extensions.surf:spring-surf:jar:5.0.c:provided
[DEBUG]          com.yahoo.platform.yui:yuicompressor:jar:2.4.8-alfresco-patched:provided
[DEBUG]          com.googlecode.concurrentlinkedhashmap:concurrentlinkedhashmap-lru:jar:1.2:provided
[DEBUG]       org.springframework.extensions.surf:spring-webscripts-api:jar:5.0.c:provided
[DEBUG]       javax.servlet:servlet-api:jar:2.5:provided (scope managed from compile by org.alfresco:alfresco-parent:5.0.c)
[DEBUG]    org.alfresco.maven:alfresco-rad:jar:2.0.0:compile
[DEBUG]       com.tradeshift:junit-remote:jar:3:compile
[DEBUG]          junit:junit:jar:4.11:compile (version managed from 4.8 by org.alfresco:alfresco-parent:5.0.c)
[DEBUG]             org.hamcrest:hamcrest-core:jar:1.3:compile
[DEBUG]          org.eclipse.jetty:jetty-servlet:jar:7.4.0.v20110414:compile
[DEBUG]             org.eclipse.jetty:jetty-security:jar:7.4.0.v20110414:compile
[DEBUG]          args4j:args4j:jar:2.0.16:compile
[DEBUG]       org.springframework:spring-test:jar:3.0.6.RELEASE:compile
[DEBUG]    org.eclipse.jetty:jetty-server:jar:7.4.0.v20110414:compile
[DEBUG]       org.eclipse.jetty:jetty-continuation:jar:7.4.0.v20110414:compile
[DEBUG]       org.eclipse.jetty:jetty-http:jar:7.4.0.v20110414:compile
[DEBUG]          org.eclipse.jetty:jetty-io:jar:7.4.0.v20110414:compile
[DEBUG]             org.eclipse.jetty:jetty-util:jar:7.4.0.v20110414:compile
[INFO] 
[INFO] --- alfresco-maven-plugin:2.0.0:set-version (default-set-version) @ content-all-share ---
[DEBUG] Configuring mojo org.alfresco.maven.plugin:alfresco-maven-plugin:2.0.0:set-version from plugin realm ClassRealm[plugin>org.alfresco.maven.plugin:alfresco-maven-plugin:2.0.0, parent: sun.misc.Launcher$AppClassLoader@71b456f]
[DEBUG] Configuring mojo 'org.alfresco.maven.plugin:alfresco-maven-plugin:2.0.0:set-version' with basic configurator -->
[DEBUG]   (f) project = MavenProject: com.netprojex:content-all-share:1.0-SNAPSHOT @ /home/programues/Documents/metadata_myoffice/content-all-share/pom.xml
[DEBUG]   (f) propertyName = noSnapshotVersion
[DEBUG]   (f) snapshotSuffix = -SNAPSHOT
[DEBUG]   (f) snapshotToTimestamp = true
[DEBUG]   (f) version = 1.0-SNAPSHOT
[DEBUG] -- end configuration --
[INFO] Removed -SNAPSHOT suffix from version - 1.0
[INFO] Added timestamp to version - 1.0.1702271512
[INFO] 
[INFO] --- maven-resources-plugin:2.7:resources (default-resources) @ content-all-share ---
[DEBUG] Configuring mojo org.apache.maven.plugins:maven-resources-plugin:2.7:resources from plugin realm ClassRealm[plugin>org.apache.maven.plugins:maven-resources-plugin:2.7, parent: sun.misc.Launcher$AppClassLoader@71b456f]
[DEBUG] Configuring mojo 'org.apache.maven.plugins:maven-resources-plugin:2.7:resources' with basic configurator -->
[DEBUG]   (f) buildFilters = []
[DEBUG]   (f) encoding = UTF-8
[DEBUG]   (f) escapeWindowsPaths = true
[DEBUG]   (s) includeEmptyDirs = false
[DEBUG]   (f) nonFilteredFileExtensions = [ftl, acp, jpg, png, gif, svg, pdf, doc, docx, xls, xlsx, ppt, pptx, bin, lic, swf, zip, msg, jar, ttf, eot, woff, woff2]
[DEBUG]   (s) outputDirectory = /home/programues/Documents/metadata_myoffice/content-all-share/target/classes
[DEBUG]   (s) overwrite = false
[DEBUG]   (f) project = MavenProject: com.netprojex:content-all-share:1.0-SNAPSHOT @ /home/programues/Documents/metadata_myoffice/content-all-share/pom.xml
[DEBUG]   (s) resources = [Resource {targetPath: null, filtering: true, FileSet {directory: /home/programues/Documents/metadata_myoffice/content-all-share/src/main/resources, PatternSet [includes: {}, excludes: {}]}}, Resource {targetPath: ../content-all-share, filtering: true, FileSet {directory: /home/programues/Documents/metadata_myoffice/content-all-share/src/main/amp, PatternSet [includes: {}, excludes: {}]}}]
[DEBUG]   (f) session = org.apache.maven.execution.MavenSession@21b4a7
[DEBUG]   (f) supportMultiLineFiltering = false
[DEBUG]   (f) useBuildFilters = true
[DEBUG]   (s) useDefaultDelimiters = true
[DEBUG] -- end configuration --
[DEBUG] properties used {env.OLDPWD=/home/programues/Documents/metadata_myoffice/content-all-share, file.encoding.pkg=sun.io, java.home=/usr/lib/jvm/java-7-openjdk-amd64/jre, app.testing.jvm.args=-Xms256m -Xmx1524m -XX:MaxPermSize=256m -Duser.language=en, stagingDirectory=../../target/staging/poms/alfresco-sdk-parent, env.DISPLAY=:0.0, env.LS_COLORS=rs=0:di=01;34:ln=01;36:mh=00:pi=40;33:so=01;35:do=01;35:bd=40;33;01:cd=40;33;01:or=40;31;01:su=37;41:sg=30;43:ca=30;41:tw=30;42:ow=34;42:st=37;44:ex=01;32:*.tar=01;31:*.tgz=01;31:*.arj=01;31:*.taz=01;31:*.lzh=01;31:*.lzma=01;31:*.tlz=01;31:*.txz=01;31:*.zip=01;31:*.z=01;31:*.Z=01;31:*.dz=01;31:*.gz=01;31:*.lz=01;31:*.xz=01;31:*.bz2=01;31:*.bz=01;31:*.tbz=01;31:*.tbz2=01;31:*.tz=01;31:*.deb=01;31:*.rpm=01;31:*.jar=01;31:*.war=01;31:*.ear=01;31:*.sar=01;31:*.rar=01;31:*.ace=01;31:*.zoo=01;31:*.cpio=01;31:*.7z=01;31:*.rz=01;31:*.jpg=01;35:*.jpeg=01;35:*.gif=01;35:*.bmp=01;35:*.pbm=01;35:*.pgm=01;35:*.ppm=01;35:*.tga=01;35:*.xbm=01;35:*.xpm=01;35:*.tif=01;35:*.tiff=01;35:*.png=01;35:*.svg=01;35:*.svgz=01;35:*.mng=01;35:*.pcx=01;35:*.mov=01;35:*.mpg=01;35:*.mpeg=01;35:*.m2v=01;35:*.mkv=01;35:*.webm=01;35:*.ogm=01;35:*.mp4=01;35:*.m4v=01;35:*.mp4v=01;35:*.vob=01;35:*.qt=01;35:*.nuv=01;35:*.wmv=01;35:*.asf=01;35:*.rm=01;35:*.rmvb=01;35:*.flc=01;35:*.avi=01;35:*.fli=01;35:*.flv=01;35:*.gl=01;35:*.dl=01;35:*.xcf=01;35:*.xwd=01;35:*.yuv=01;35:*.cgm=01;35:*.emf=01;35:*.axv=01;35:*.anx=01;35:*.ogv=01;35:*.ogx=01;35:*.aac=00;36:*.au=00;36:*.flac=00;36:*.mid=00;36:*.midi=00;36:*.mka=00;36:*.mp3=00;36:*.mpc=00;36:*.ogg=00;36:*.ra=00;36:*.wav=00;36:*.axa=00;36:*.oga=00;36:*.spx=00;36:*.xspf=00;36:, env.XDG_VTNR=7, alfresco.client.war=share, alfresco.client.war.version=5.0.c, classworlds.conf=/usr/share/maven/bin/m2.conf, env.SUDO_USER=programues, app.log.root.level=WARN, java.endorsed.dirs=/usr/lib/jvm/java-7-openjdk-amd64/jre/lib/endorsed, env.LOGNAME=root, alfresco.db.password=alfresco, env.USERNAME=root, maven.jar.version=2.5, app.amp.excludes=, env.XDG_SEAT=seat0, maven.war.version=2.5, env.LC_PAPER=cs_CZ.UTF-8, sun.os.patch.level=unknown, java.vendor.url=http://java.oracle.com/, maven.tomcat.port=8081, maven.dependency.version=2.9, maven.resources.version=2.7, java.version=1.7.0_101, env.ANDROID_HOME=/op/adt/sdk/, env.MAVEN_OPTS=-javaagent:/root/.m2/repository/org/springframework/springloaded/1.2.0.RELEASE/springloaded-1.2.0.RELEASE.jar -noverify, java.vendor.url.bug=http://bugreport.sun.com/bugreport/, env.LANGUAGE=en_US, skipTests=true, alfresco.groupId=org.alfresco, maven.alfresco.version=2.0.0, user.name=root, env.LANG=en_US.UTF-8, env.LC_MONETARY=cs_CZ.UTF-8, sun.io.unicode.encoding=UnicodeLittle, sun.jnu.encoding=UTF-8, java.runtime.name=OpenJDK Runtime Environment, env.LC_NAME=cs_CZ.UTF-8, java.specification.name=Java Platform API Specification, user.timezone=, env.LESSOPEN=| /usr/bin/lesspipe %s, path.separator=:, alfresco.db.url=jdbc:h2:./alf_data_dev/h2_data/alf_dev;MODE=PostgreSQL;AUTO_SERVER=TRUE;DB_CLOSE_ON_EXIT=FALSE;LOCK_TIMEOUT=10000;MVCC=FALSE;LOCK_MODE=0, file.encoding=UTF-8, env.HOME=/root, app.amp.output.folder=../content-all-share, sun.java.command=org.codehaus.plexus.classworlds.launcher.Launcher integration-test -Pamp-to-war -X, maven.install.version=2.5.2, env.LC_TELEPHONE=cs_CZ.UTF-8, env._=/usr/bin/mvn, app.filtering.enabled=true, java.io.tmpdir=/tmp, env.LC_IDENTIFICATION=cs_CZ.UTF-8, env.ANRDOID_NDK_ROOT=/home/programues/Android/android-ndk-r12b/, user.language=en, app.slf4j.scope=runtime, env.NDK=/home/programues/Android/android-ndk-r12b/, line.separator=
, app.amp.overlay.excludes=, app.log.dir=/home/programues/Documents/metadata_myoffice/content-all-share/target/, env=local, alfresco.community.default.version=5.0.c, alfresco.data.location=alf_data_dev, maven.tomcat.version=2.2, java.vm.info=mixed mode, maven.buildhelper.version=1.9.1, alfresco.db.username=alfresco, java.vm.specification.name=Java Virtual Machine Specification, alfresco.client.war.folder=/home/programues/Documents/metadata_myoffice/content-all-share/target/content-all-share-war, env.COLORTERM=xfce4-terminal, app.slf4j.version=1.5.11, env.LC_MEASUREMENT=cs_CZ.UTF-8, alfresco.repo.url=http://localhost:8080/alfresco, env.LC_TIME=cs_CZ.UTF-8, java.awt.printerjob=sun.print.PSPrinterJob, maven.jetty.version=7.4.0.v20110414, alfresco.client.contextPath=/share, maven.clean.version=2.6.1, os.name=Linux, java.specification.vendor=Oracle Corporation, alfresco.db.params=MODE=PostgreSQL;AUTO_SERVER=TRUE;DB_CLOSE_ON_EXIT=FALSE;LOCK_TIMEOUT=10000;MVCC=FALSE;LOCK_MODE=0, alfresco.repo.artifactId=alfresco, env.TERM=xterm, java.vm.name=OpenJDK 64-Bit Server VM, java.library.path=/usr/java/packages/lib/amd64:/usr/lib/x86_64-linux-gnu/jni:/lib/x86_64-linux-gnu:/usr/lib/x86_64-linux-gnu:/usr/lib/jni:/lib:/usr/lib, env.PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/games:/usr/local/games:/opt/android-sdk/sdk/tools:/opt/android-sdk/sdk/platform-tools, env.LESSCLOSE=/usr/bin/lesspipe %s %s, alfresco.client.war.groupId=org.alfresco, alfresco.version=5.0.c, java.class.version=51.0, env.SHLVL=2, app.properties.test.include=**, alfresco.db.name=alf_dev, maven.compiler.version=3.2, sun.boot.library.path=/usr/lib/jvm/java-7-openjdk-amd64/jre/lib/amd64, project.build.sourceEncoding=UTF-8, app.amp.folder=src/main/amp, sun.management.compiler=HotSpot 64-Bit Tiered Compilers, java.awt.graphicsenv=sun.awt.X11GraphicsEnvironment, maven.enforcer.plugin=1.3.1, env.USER=root, junit.version=4.11, alfresco.sdk.parent.version=1.0-SNAPSHOT, java.vm.specification.version=1.7, springloaded.version=1.2.0.RELEASE, awt.toolkit=sun.awt.X11.XToolkit, sun.cpu.isalist=, alfresco.db.datasource.class=org.h2.jdbcx.JdbcDataSource, java.ext.dirs=/usr/lib/jvm/java-7-openjdk-amd64/jre/lib/ext:/usr/java/packages/lib/ext, os.version=3.19.0-64-generic, user.home=/root, maven.release.version=2.5.1, java.vm.vendor=Oracle Corporation, env.JAVA_HOME=/usr/lib/jvm/java-7-openjdk-amd64, env.SUDO_COMMAND=/bin/su, env.XDG_SESSION_ID=c1, env.XAUTHORITY=/home/programues/.Xauthority, app.properties.test.folder=src/test/properties/local, user.dir=/home/programues/Documents/metadata_myoffice/content-all-share, app.properties.include=**, env.MAIL=/var/mail/root, env.NDK_TARGET=arm-linux-androideabi-4.9, env.PWD=/home/programues/Documents/metadata_myoffice/content-all-share, sun.cpu.endian=little, maven.antrun.version=1.7, java.vm.version=24.95-b01, java.class.path=/usr/share/maven/boot/plexus-classworlds-2.x.jar:/root/.m2/repository/org/springframework/springloaded/1.2.0.RELEASE/springloaded-1.2.0.RELEASE.jar, env.LC_ADDRESS=cs_CZ.UTF-8, os.arch=amd64, maven.build.version=Apache Maven 3.0.5, env.LC_NUMERIC=cs_CZ.UTF-8, env.SUDO_UID=1000, sun.java.launcher=SUN_STANDARD, alfresco.enterprise.default.version=5.0, java.vm.specification.vendor=Oracle Corporation, noSnapshotVersion=1.0.1702271512, file.separator=/, h2.version=1.4.182, java.runtime.version=1.7.0_101-b00, sun.boot.class.path=/usr/lib/jvm/java-7-openjdk-amd64/jre/lib/resources.jar:/usr/lib/jvm/java-7-openjdk-amd64/jre/lib/rt.jar:/usr/lib/jvm/java-7-openjdk-amd64/jre/lib/sunrsasign.jar:/usr/lib/jvm/java-7-openjdk-amd64/jre/lib/jsse.jar:/usr/lib/jvm/java-7-openjdk-amd64/jre/lib/jce.jar:/usr/lib/jvm/java-7-openjdk-amd64/jre/lib/charsets.jar:/usr/lib/jvm/java-7-openjdk-amd64/jre/lib/rhino.jar:/usr/lib/jvm/java-7-openjdk-amd64/jre/lib/jfr.jar:/usr/lib/jvm/java-7-openjdk-amd64/jre/classes, maven.replacer.version=1.5.3, scm.url.base=https://github.com/Alfresco/alfresco-sdk, maven.surefire.version=2.18, maven.version=3.0.5, maven.archetype.version=2.2, user.country=US, env.SHELL=/bin/bash, maven.home=/usr/share/maven, alfresco.share.artifactId=share, java.vendor=Oracle Corporation, app.properties.folder=src/main/properties/local, java.specification.version=1.7, env.SUDO_GID=1000, sun.arch.data.model=64}
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[DEBUG] resource with targetPath null
directory /home/programues/Documents/metadata_myoffice/content-all-share/src/main/resources
excludes []
includes []
[DEBUG] ignoreDelta true
[INFO] Copying 3 resources
[DEBUG] file share-config-custom.xml.sample has a filtered file extension
[DEBUG] filtering /home/programues/Documents/metadata_myoffice/content-all-share/src/main/resources/META-INF/share-config-custom.xml.sample to /home/programues/Documents/metadata_myoffice/content-all-share/target/classes/META-INF/share-config-custom.xml.sample
[DEBUG] file test.html has a filtered file extension
[DEBUG] filtering /home/programues/Documents/metadata_myoffice/content-all-share/src/main/resources/META-INF/resources/test.html to /home/programues/Documents/metadata_myoffice/content-all-share/target/classes/META-INF/resources/test.html
[DEBUG] file share-config-custom.xml has a filtered file extension
[DEBUG] filtering /home/programues/Documents/metadata_myoffice/content-all-share/src/main/resources/META-INF/share-config-custom.xml to /home/programues/Documents/metadata_myoffice/content-all-share/target/classes/META-INF/share-config-custom.xml
[DEBUG] resource with targetPath ../content-all-share
directory /home/programues/Documents/metadata_myoffice/content-all-share/src/main/amp
excludes []
includes []
[DEBUG] ignoreDelta true
[INFO] Copying 65 resources to ../content-all-share
[DEBUG] file alfresco-imagelib.png has a non filtered file extension
[DEBUG] copy /home/programues/Documents/metadata_myoffice/content-all-share/src/main/amp/web/themes/uTheme/images/alfresco-imagelib.png to /home/programues/Documents/metadata_myoffice/content-all-share/target/classes/../content-all-share/web/themes/uTheme/images/alfresco-imagelib.png
[DEBUG] file wait.gif has a non filtered file extension
[DEBUG] copy /home/programues/Documents/metadata_myoffice/content-all-share/src/main/amp/web/themes/uTheme/images/wait.gif to /home/programues/Documents/metadata_myoffice/content-all-share/target/classes/../content-all-share/web/themes/uTheme/images/wait.gif
[DEBUG] file upload-button-sprite.png has a non filtered file extension
[DEBUG] copy /home/programues/Documents/metadata_myoffice/content-all-share/src/main/amp/web/themes/uTheme/images/upload-button-sprite.png to /home/programues/Documents/metadata_myoffice/content-all-share/target/classes/../content-all-share/web/themes/uTheme/images/upload-button-sprite.png
[DEBUG] file alfresco-logo-bg.jpg has a non filtered file extension
[DEBUG] copy /home/programues/Documents/metadata_myoffice/content-all-share/src/main/amp/web/themes/uTheme/images/alfresco-logo-bg.jpg to /home/programues/Documents/metadata_myoffice/content-all-share/target/classes/../content-all-share/web/themes/uTheme/images/alfresco-logo-bg.jpg
[DEBUG] file collapsed.png has a non filtered file extension
[DEBUG] copy /home/programues/Documents/metadata_myoffice/content-all-share/src/main/amp/web/themes/uTheme/images/collapsed.png to /home/programues/Documents/metadata_myoffice/content-all-share/target/classes/../content-all-share/web/themes/uTheme/images/collapsed.png
[DEBUG] file categoryview-sprite.gif has a non filtered file extension
[DEBUG] copy /home/programues/Documents/metadata_myoffice/content-all-share/src/main/amp/web/themes/uTheme/images/categoryview-sprite.gif to /home/programues/Documents/metadata_myoffice/content-all-share/target/classes/../content-all-share/web/themes/uTheme/images/categoryview-sprite.gif
[DEBUG] file app-logo-48.png has a non filtered file extension
[DEBUG] copy /home/programues/Documents/metadata_myoffice/content-all-share/src/main/amp/web/themes/uTheme/images/app-logo-48.png to /home/programues/Documents/metadata_myoffice/content-all-share/target/classes/../content-all-share/web/themes/uTheme/images/app-logo-48.png
[DEBUG] file app-logo.png has a non filtered file extension
[DEBUG] copy /home/programues/Documents/metadata_myoffice/content-all-share/src/main/amp/web/themes/uTheme/images/app-logo.png to /home/programues/Documents/metadata_myoffice/content-all-share/target/classes/../content-all-share/web/themes/uTheme/images/app-logo.png
[DEBUG] file treeview-sprite.gif has a non filtered file extension
[DEBUG] copy /home/programues/Documents/metadata_myoffice/content-all-share/src/main/amp/web/themes/uTheme/images/treeview-sprite.gif to /home/programues/Documents/metadata_myoffice/content-all-share/target/classes/../content-all-share/web/themes/uTheme/images/treeview-sprite.gif
[DEBUG] file logo.png has a non filtered file extension
[DEBUG] copy /home/programues/Documents/metadata_myoffice/content-all-share/src/main/amp/web/themes/uTheme/images/logo.png to /home/programues/Documents/metadata_myoffice/content-all-share/target/classes/../content-all-share/web/themes/uTheme/images/logo.png
[DEBUG] file app-logo-share.png has a non filtered file extension
[DEBUG] copy /home/programues/Documents/metadata_myoffice/content-all-share/src/main/amp/web/themes/uTheme/images/app-logo-share.png to /home/programues/Documents/metadata_myoffice/content-all-share/target/classes/../content-all-share/web/themes/uTheme/images/app-logo-share.png
[DEBUG] file link-menu-button-arrow.png has a non filtered file extension
[DEBUG] copy /home/programues/Documents/metadata_myoffice/content-all-share/src/main/amp/web/themes/uTheme/images/link-menu-button-arrow.png to /home/programues/Documents/metadata_myoffice/content-all-share/target/classes/../content-all-share/web/themes/uTheme/images/link-menu-button-arrow.png
[DEBUG] file expanded.png has a non filtered file extension
[DEBUG] copy /home/programues/Documents/metadata_myoffice/content-all-share/src/main/amp/web/themes/uTheme/images/expanded.png to /home/programues/Documents/metadata_myoffice/content-all-share/target/classes/../content-all-share/web/themes/uTheme/images/expanded.png
[DEBUG] file alfresco-linklib.png has a non filtered file extension
[DEBUG] copy /home/programues/Documents/metadata_myoffice/content-all-share/src/main/amp/web/themes/uTheme/images/alfresco-linklib.png to /home/programues/Documents/metadata_myoffice/content-all-share/target/classes/../content-all-share/web/themes/uTheme/images/alfresco-linklib.png
[DEBUG] file feed-icon-16.png has a non filtered file extension
[DEBUG] copy /home/programues/Documents/metadata_myoffice/content-all-share/src/main/amp/web/themes/uTheme/images/feed-icon-16.png to /home/programues/Documents/metadata_myoffice/content-all-share/target/classes/../content-all-share/web/themes/uTheme/images/feed-icon-16.png
[DEBUG] file sprite.png has a non filtered file extension
[DEBUG] copy /home/programues/Documents/metadata_myoffice/content-all-share/src/main/amp/web/themes/uTheme/images/sprite.png to /home/programues/Documents/metadata_myoffice/content-all-share/target/classes/../content-all-share/web/themes/uTheme/images/sprite.png
[DEBUG] file presentation.css has a filtered file extension
[DEBUG] filtering /home/programues/Documents/metadata_myoffice/content-all-share/src/main/amp/web/themes/uTheme/presentation.css to /home/programues/Documents/metadata_myoffice/content-all-share/target/classes/../content-all-share/web/themes/uTheme/presentation.css
[DEBUG] file wait.gif has a non filtered file extension
[DEBUG] copy /home/programues/Documents/metadata_myoffice/content-all-share/src/main/amp/web/themes/uTheme/yui/assets/wait.gif to /home/programues/Documents/metadata_myoffice/content-all-share/target/classes/../content-all-share/web/themes/uTheme/yui/assets/wait.gif
[DEBUG] file split-button-arrow.png has a non filtered file extension
[DEBUG] copy /home/programues/Documents/metadata_myoffice/content-all-share/src/main/amp/web/themes/uTheme/yui/assets/split-button-arrow.png to /home/programues/Documents/metadata_myoffice/content-all-share/target/classes/../content-all-share/web/themes/uTheme/yui/assets/split-button-arrow.png
[DEBUG] file split-button-arrow-focus.png has a non filtered file extension
[DEBUG] copy /home/programues/Documents/metadata_myoffice/content-all-share/src/main/amp/web/themes/uTheme/yui/assets/split-button-arrow-focus.png to /home/programues/Documents/metadata_myoffice/content-all-share/target/classes/../content-all-share/web/themes/uTheme/yui/assets/split-button-arrow-focus.png
[DEBUG] file editor-sprite-active.gif has a non filtered file extension
[DEBUG] copy /home/programues/Documents/metadata_myoffice/content-all-share/src/main/amp/web/themes/uTheme/yui/assets/editor-sprite-active.gif to /home/programues/Documents/metadata_myoffice/content-all-share/target/classes/../content-all-share/web/themes/uTheme/yui/assets/editor-sprite-active.gif
[DEBUG] file editor-knob.gif has a non filtered file extension
[DEBUG] copy /home/programues/Documents/metadata_myoffice/content-all-share/src/main/amp/web/themes/uTheme/yui/assets/editor-knob.gif to /home/programues/Documents/metadata_myoffice/content-all-share/target/classes/../content-all-share/web/themes/uTheme/yui/assets/editor-knob.gif
[DEBUG] file menubaritem_submenuindicator_disabled.png has a non filtered file extension
[DEBUG] copy /home/programues/Documents/metadata_myoffice/content-all-share/src/main/amp/web/themes/uTheme/yui/assets/menubaritem_submenuindicator_disabled.png to /home/programues/Documents/metadata_myoffice/content-all-share/target/classes/../content-all-share/web/themes/uTheme/yui/assets/menubaritem_submenuindicator_disabled.png
[DEBUG] file treeview-loading.gif has a non filtered file extension
[DEBUG] copy /home/programues/Documents/metadata_myoffice/content-all-share/src/main/amp/web/themes/uTheme/yui/assets/treeview-loading.gif to /home/programues/Documents/metadata_myoffice/content-all-share/target/classes/../content-all-share/web/themes/uTheme/yui/assets/treeview-loading.gif
[DEBUG] file split-button-arrow-active.png has a non filtered file extension
[DEBUG] copy /home/programues/Documents/metadata_myoffice/content-all-share/src/main/amp/web/themes/uTheme/yui/assets/split-button-arrow-active.png to /home/programues/Documents/metadata_myoffice/content-all-share/target/classes/../content-all-share/web/themes/uTheme/yui/assets/split-button-arrow-active.png
[DEBUG] file skin.css has a filtered file extension
[DEBUG] filtering /home/programues/Documents/metadata_myoffice/content-all-share/src/main/amp/web/themes/uTheme/yui/assets/skin.css to /home/programues/Documents/metadata_myoffice/content-all-share/target/classes/../content-all-share/web/themes/uTheme/yui/assets/skin.css
[DEBUG] file transparent.gif has a non filtered file extension
[DEBUG] copy /home/programues/Documents/metadata_myoffice/content-all-share/src/main/amp/web/themes/uTheme/yui/assets/transparent.gif to /home/programues/Documents/metadata_myoffice/content-all-share/target/classes/../content-all-share/web/themes/uTheme/yui/assets/transparent.gif
[DEBUG] file editor-sprite.gif has a non filtered file extension
[DEBUG] copy /home/programues/Documents/metadata_myoffice/content-all-share/src/main/amp/web/themes/uTheme/yui/assets/editor-sprite.gif to /home/programues/Documents/metadata_myoffice/content-all-share/target/classes/../content-all-share/web/themes/uTheme/yui/assets/editor-sprite.gif
[DEBUG] file menuitem_checkbox.png has a non filtered file extension
[DEBUG] copy /home/programues/Documents/metadata_myoffice/content-all-share/src/main/amp/web/themes/uTheme/yui/assets/menuitem_checkbox.png to /home/programues/Documents/metadata_myoffice/content-all-share/target/classes/../content-all-share/web/themes/uTheme/yui/assets/menuitem_checkbox.png
[DEBUG] file toolbar-checked-gradient.gif has a non filtered file extension
[DEBUG] copy /home/programues/Documents/metadata_myoffice/content-all-share/src/main/amp/web/themes/uTheme/yui/assets/toolbar-checked-gradient.gif to /home/programues/Documents/metadata_myoffice/content-all-share/target/classes/../content-all-share/web/themes/uTheme/yui/assets/toolbar-checked-gradient.gif
[DEBUG] file asc.gif has a non filtered file extension
[DEBUG] copy /home/programues/Documents/metadata_myoffice/content-all-share/src/main/amp/web/themes/uTheme/yui/assets/asc.gif to /home/programues/Documents/metadata_myoffice/content-all-share/target/classes/../content-all-share/web/themes/uTheme/yui/assets/asc.gif
[DEBUG] file layout_sprite.png has a non filtered file extension
[DEBUG] copy /home/programues/Documents/metadata_myoffice/content-all-share/src/main/amp/web/themes/uTheme/yui/assets/layout_sprite.png to /home/programues/Documents/metadata_myoffice/content-all-share/target/classes/../content-all-share/web/themes/uTheme/yui/assets/layout_sprite.png
[DEBUG] file menuitem_checkbox_disabled.png has a non filtered file extension
[DEBUG] copy /home/programues/Documents/metadata_myoffice/content-all-share/src/main/amp/web/themes/uTheme/yui/assets/menuitem_checkbox_disabled.png to /home/programues/Documents/metadata_myoffice/content-all-share/target/classes/../content-all-share/web/themes/uTheme/yui/assets/menuitem_checkbox_disabled.png
[DEBUG] file loading.gif has a non filtered file extension
[DEBUG] copy /home/programues/Documents/metadata_myoffice/content-all-share/src/main/amp/web/themes/uTheme/yui/assets/loading.gif to /home/programues/Documents/metadata_myoffice/content-all-share/target/classes/../content-all-share/web/themes/uTheme/yui/assets/loading.gif
[DEBUG] file bg-v.gif has a non filtered file extension
[DEBUG] copy /home/programues/Documents/metadata_myoffice/content-all-share/src/main/amp/web/themes/uTheme/yui/assets/bg-v.gif to /home/programues/Documents/metadata_myoffice/content-all-share/target/classes/../content-all-share/web/themes/uTheme/yui/assets/bg-v.gif
[DEBUG] file picker_mask.png has a non filtered file extension
[DEBUG] copy /home/programues/Documents/metadata_myoffice/content-all-share/src/main/amp/web/themes/uTheme/yui/assets/picker_mask.png to /home/programues/Documents/metadata_myoffice/content-all-share/target/classes/../content-all-share/web/themes/uTheme/yui/assets/picker_mask.png
[DEBUG] file dt-arrow-dn.png has a non filtered file extension
[DEBUG] copy /home/programues/Documents/metadata_myoffice/content-all-share/src/main/amp/web/themes/uTheme/yui/assets/dt-arrow-dn.png to /home/programues/Documents/metadata_myoffice/content-all-share/target/classes/../content-all-share/web/themes/uTheme/yui/assets/dt-arrow-dn.png
[DEBUG] file menubaritem_submenuindicator.png has a non filtered file extension
[DEBUG] copy /home/programues/Documents/metadata_myoffice/content-all-share/src/main/amp/web/themes/uTheme/yui/assets/menubaritem_submenuindicator.png to /home/programues/Documents/metadata_myoffice/content-all-share/target/classes/../content-all-share/web/themes/uTheme/yui/assets/menubaritem_submenuindicator.png
[DEBUG] file bg-h.gif has a non filtered file extension
[DEBUG] copy /home/programues/Documents/metadata_myoffice/content-all-share/src/main/amp/web/themes/uTheme/yui/assets/bg-h.gif to /home/programues/Documents/metadata_myoffice/content-all-share/target/classes/../content-all-share/web/themes/uTheme/yui/assets/bg-h.gif
[DEBUG] file menuitem_submenuindicator.png has a non filtered file extension
[DEBUG] copy /home/programues/Documents/metadata_myoffice/content-all-share/src/main/amp/web/themes/uTheme/yui/assets/menuitem_submenuindicator.png to /home/programues/Documents/metadata_myoffice/content-all-share/target/classes/../content-all-share/web/themes/uTheme/yui/assets/menuitem_submenuindicator.png
[DEBUG] file desc.gif has a non filtered file extension
[DEBUG] copy /home/programues/Documents/metadata_myoffice/content-all-share/src/main/amp/web/themes/uTheme/yui/assets/desc.gif to /home/programues/Documents/metadata_myoffice/content-all-share/target/classes/../content-all-share/web/themes/uTheme/yui/assets/desc.gif
[DEBUG] file treeview-sprite.gif has a non filtered file extension
[DEBUG] copy /home/programues/Documents/metadata_myoffice/content-all-share/src/main/amp/web/themes/uTheme/yui/assets/treeview-sprite.gif to /home/programues/Documents/metadata_myoffice/content-all-share/target/classes/../content-all-share/web/themes/uTheme/yui/assets/treeview-sprite.gif
[DEBUG] file menuitem_submenuindicator_disabled.png has a non filtered file extension
[DEBUG] copy /home/programues/Documents/metadata_myoffice/content-all-share/src/main/amp/web/themes/uTheme/yui/assets/menuitem_submenuindicator_disabled.png to /home/programues/Documents/metadata_myoffice/content-all-share/target/classes/../content-all-share/web/themes/uTheme/yui/assets/menuitem_submenuindicator_disabled.png
[DEBUG] file split-button-arrow-disabled.png has a non filtered file extension
[DEBUG] copy /home/programues/Documents/metadata_myoffice/content-all-share/src/main/amp/web/themes/uTheme/yui/assets/split-button-arrow-disabled.png to /home/programues/Documents/metadata_myoffice/content-all-share/target/classes/../content-all-share/web/themes/uTheme/yui/assets/split-button-arrow-disabled.png
[DEBUG] file header_background.png has a non filtered file extension
[DEBUG] copy /home/programues/Documents/metadata_myoffice/content-all-share/src/main/amp/web/themes/uTheme/yui/assets/header_background.png to /home/programues/Documents/metadata_myoffice/content-all-share/target/classes/../content-all-share/web/themes/uTheme/yui/assets/header_background.png
[DEBUG] file menu-button-arrow-disabled.png has a non filtered file extension
[DEBUG] copy /home/programues/Documents/metadata_myoffice/content-all-share/src/main/amp/web/themes/uTheme/yui/assets/menu-button-arrow-disabled.png to /home/programues/Documents/metadata_myoffice/content-all-share/target/classes/../content-all-share/web/themes/uTheme/yui/assets/menu-button-arrow-disabled.png
[DEBUG] file blankimage.png has a non filtered file extension
[DEBUG] copy /home/programues/Documents/metadata_myoffice/content-all-share/src/main/amp/web/themes/uTheme/yui/assets/blankimage.png to /home/programues/Documents/metadata_myoffice/content-all-share/target/classes/../content-all-share/web/themes/uTheme/yui/assets/blankimage.png
[DEBUG] file dt-arrow-up.png has a non filtered file extension
[DEBUG] copy /home/programues/Documents/metadata_myoffice/content-all-share/src/main/amp/web/themes/uTheme/yui/assets/dt-arrow-up.png to /home/programues/Documents/metadata_myoffice/content-all-share/target/classes/../content-all-share/web/themes/uTheme/yui/assets/dt-arrow-up.png
[DEBUG] file hue_bg.png has a non filtered file extension
[DEBUG] copy /home/programues/Documents/metadata_myoffice/content-all-share/src/main/amp/web/themes/uTheme/yui/assets/hue_bg.png to /home/programues/Documents/metadata_myoffice/content-all-share/target/classes/../content-all-share/web/themes/uTheme/yui/assets/hue_bg.png
[DEBUG] file sprite.png has a non filtered file extension
[DEBUG] copy /home/programues/Documents/metadata_myoffice/content-all-share/src/main/amp/web/themes/uTheme/yui/assets/sprite.png to /home/programues/Documents/metadata_myoffice/content-all-share/target/classes/../content-all-share/web/themes/uTheme/yui/assets/sprite.png
[DEBUG] file menu-button-arrow.png has a non filtered file extension
[DEBUG] copy /home/programues/Documents/metadata_myoffice/content-all-share/src/main/amp/web/themes/uTheme/yui/assets/menu-button-arrow.png to /home/programues/Documents/metadata_myoffice/content-all-share/target/classes/../content-all-share/web/themes/uTheme/yui/assets/menu-button-arrow.png
[DEBUG] file split-button-arrow-hover.png has a non filtered file extension
[DEBUG] copy /home/programues/Documents/metadata_myoffice/content-all-share/src/main/amp/web/themes/uTheme/yui/assets/split-button-arrow-hover.png to /home/programues/Documents/metadata_myoffice/content-all-share/target/classes/../content-all-share/web/themes/uTheme/yui/assets/split-button-arrow-hover.png
[DEBUG] file TemplateWidget.js has a filtered file extension
[DEBUG] filtering /home/programues/Documents/metadata_myoffice/content-all-share/src/main/amp/web/js/example/widgets/TemplateWidget.js to /home/programues/Documents/metadata_myoffice/content-all-share/target/classes/../content-all-share/web/js/example/widgets/TemplateWidget.js
[DEBUG] file TemplateWidget.html has a filtered file extension
[DEBUG] filtering /home/programues/Documents/metadata_myoffice/content-all-share/src/main/amp/web/js/example/widgets/templates/TemplateWidget.html to /home/programues/Documents/metadata_myoffice/content-all-share/target/classes/../content-all-share/web/js/example/widgets/templates/TemplateWidget.html
[DEBUG] file TemplateWidget.css has a filtered file extension
[DEBUG] filtering /home/programues/Documents/metadata_myoffice/content-all-share/src/main/amp/web/js/example/widgets/css/TemplateWidget.css to /home/programues/Documents/metadata_myoffice/content-all-share/target/classes/../content-all-share/web/js/example/widgets/css/TemplateWidget.css
[DEBUG] file TemplateWidget.properties has a filtered file extension
[DEBUG] filtering /home/programues/Documents/metadata_myoffice/content-all-share/src/main/amp/web/js/example/widgets/i18n/TemplateWidget.properties to /home/programues/Documents/metadata_myoffice/content-all-share/target/classes/../content-all-share/web/js/example/widgets/i18n/TemplateWidget.properties
[DEBUG] file uTheme.xml has a filtered file extension
[DEBUG] filtering /home/programues/Documents/metadata_myoffice/content-all-share/src/main/amp/config/alfresco/site-data/themes/uTheme.xml to /home/programues/Documents/metadata_myoffice/content-all-share/target/classes/../content-all-share/config/alfresco/site-data/themes/uTheme.xml
[DEBUG] file baBanca.properties has a filtered file extension
[DEBUG] filtering /home/programues/Documents/metadata_myoffice/content-all-share/src/main/amp/config/alfresco/module/content-all-share/messages/baBanca.properties to /home/programues/Documents/metadata_myoffice/content-all-share/target/classes/../content-all-share/config/alfresco/module/content-all-share/messages/baBanca.properties
[DEBUG] file example-widgets.xml has a filtered file extension
[DEBUG] filtering /home/programues/Documents/metadata_myoffice/content-all-share/src/main/amp/config/alfresco/web-extension/site-data/extensions/example-widgets.xml to /home/programues/Documents/metadata_myoffice/content-all-share/target/classes/../content-all-share/config/alfresco/web-extension/site-data/extensions/example-widgets.xml
[DEBUG] file simple-page.get.desc.xml has a filtered file extension
[DEBUG] filtering /home/programues/Documents/metadata_myoffice/content-all-share/src/main/amp/config/alfresco/web-extension/site-webscripts/com/example/pages/simple-page.get.desc.xml to /home/programues/Documents/metadata_myoffice/content-all-share/target/classes/../content-all-share/config/alfresco/web-extension/site-webscripts/com/example/pages/simple-page.get.desc.xml
[DEBUG] file simple-page.get.js has a filtered file extension
[DEBUG] filtering /home/programues/Documents/metadata_myoffice/content-all-share/src/main/amp/config/alfresco/web-extension/site-webscripts/com/example/pages/simple-page.get.js to /home/programues/Documents/metadata_myoffice/content-all-share/target/classes/../content-all-share/config/alfresco/web-extension/site-webscripts/com/example/pages/simple-page.get.js
[DEBUG] file simple-page.get.html.ftl has a non filtered file extension
[DEBUG] copy /home/programues/Documents/metadata_myoffice/content-all-share/src/main/amp/config/alfresco/web-extension/site-webscripts/com/example/pages/simple-page.get.html.ftl to /home/programues/Documents/metadata_myoffice/content-all-share/target/classes/../content-all-share/config/alfresco/web-extension/site-webscripts/com/example/pages/simple-page.get.html.ftl
[DEBUG] file content-tutorial-share-context.xml has a filtered file extension
[DEBUG] filtering /home/programues/Documents/metadata_myoffice/content-all-share/src/main/amp/config/alfresco/web-extension/content-tutorial-share-context.xml to /home/programues/Documents/metadata_myoffice/content-all-share/target/classes/../content-all-share/config/alfresco/web-extension/content-tutorial-share-context.xml
[DEBUG] file module.properties has a filtered file extension
[DEBUG] filtering /home/programues/Documents/metadata_myoffice/content-all-share/src/main/amp/module.properties to /home/programues/Documents/metadata_myoffice/content-all-share/target/classes/../content-all-share/module.properties
[DEBUG] file file-mapping.properties has a filtered file extension
[DEBUG] filtering /home/programues/Documents/metadata_myoffice/content-all-share/src/main/amp/file-mapping.properties to /home/programues/Documents/metadata_myoffice/content-all-share/target/classes/../content-all-share/file-mapping.properties
[DEBUG] no use filter components
[INFO] 
[INFO] --- maven-compiler-plugin:3.2:compile (default-compile) @ content-all-share ---
[DEBUG] Configuring mojo org.apache.maven.plugins:maven-compiler-plugin:3.2:compile from plugin realm ClassRealm[plugin>org.apache.maven.plugins:maven-compiler-plugin:3.2, parent: sun.misc.Launcher$AppClassLoader@71b456f]
[DEBUG] Configuring mojo 'org.apache.maven.plugins:maven-compiler-plugin:3.2:compile' with basic configurator -->
[DEBUG]   (f) basedir = /home/programues/Documents/metadata_myoffice/content-all-share
[DEBUG]   (f) buildDirectory = /home/programues/Documents/metadata_myoffice/content-all-share/target
[DEBUG]   (f) classpathElements = [/home/programues/Documents/metadata_myoffice/content-all-share/target/classes, /root/.m2/repository/org/alfresco/share/5.0.c/share-5.0.c-classes.jar, /root/.m2/repository/org/alfresco/alfresco-web-framework-commons/5.0.c/alfresco-web-framework-commons-5.0.c-classes.jar, /root/.m2/repository/org/alfresco/alfresco-jlan-embed/5.0.c/alfresco-jlan-embed-5.0.c.jar, /root/.m2/repository/org/alfresco/alfresco-core/5.0.c/alfresco-core-5.0.c.jar, /root/.m2/repository/commons-codec/commons-codec/1.9/commons-codec-1.9.jar, /root/.m2/repository/commons-httpclient/commons-httpclient/3.1/commons-httpclient-3.1.jar, /root/.m2/repository/org/safehaus/jug/jug/2.0.0/jug-2.0.0-asl.jar, /root/.m2/repository/log4j/log4j/1.2.17/log4j-1.2.17.jar, /root/.m2/repository/org/json/json/20090211/json-20090211.jar, /root/.m2/repository/org/springframework/spring-orm/3.2.10.RELEASE/spring-orm-3.2.10.RELEASE.jar, /root/.m2/repository/org/springframework/spring-beans/3.2.10.RELEASE/spring-beans-3.2.10.RELEASE.jar, /root/.m2/repository/org/springframework/spring-core/3.2.10.RELEASE/spring-core-3.2.10.RELEASE.jar, /root/.m2/repository/org/springframework/spring-jdbc/3.2.10.RELEASE/spring-jdbc-3.2.10.RELEASE.jar, /root/.m2/repository/org/springframework/spring-tx/3.2.10.RELEASE/spring-tx-3.2.10.RELEASE.jar, /root/.m2/repository/org/springframework/extensions/surf/spring-surf-core-configservice/5.0.c/spring-surf-core-configservice-5.0.c.jar, /root/.m2/repository/org/springframework/extensions/surf/spring-surf-core/5.0.c/spring-surf-core-5.0.c.jar, /root/.m2/repository/org/springframework/spring-context/3.2.10.RELEASE/spring-context-3.2.10.RELEASE.jar, /root/.m2/repository/org/springframework/spring-aop/3.2.10.RELEASE/spring-aop-3.2.10.RELEASE.jar, /root/.m2/repository/org/springframework/spring-context-support/3.2.10.RELEASE/spring-context-support-3.2.10.RELEASE.jar, /root/.m2/repository/org/springframework/spring-web/3.2.10.RELEASE/spring-web-3.2.10.RELEASE.jar, /root/.m2/repository/com/sun/xml/bind/jaxb-impl/2.1.11/jaxb-impl-2.1.11.jar, /root/.m2/repository/dom4j/dom4j/1.6.1/dom4j-1.6.1.jar, /root/.m2/repository/xml-apis/xml-apis/1.4.01/xml-apis-1.4.01.jar, /root/.m2/repository/org/codehaus/guessencoding/guessencoding/1.0/guessencoding-1.0.jar, /root/.m2/repository/javax/transaction/jta/1.0.1b/jta-1.0.1b.jar, /root/.m2/repository/joda-time/joda-time/2.3/joda-time-2.3.jar, /root/.m2/repository/org/apache/httpcomponents/httpclient/4.3.3/httpclient-4.3.3.jar, /root/.m2/repository/org/apache/httpcomponents/httpcore/4.3.2/httpcore-4.3.2.jar, /root/.m2/repository/com/hazelcast/hazelcast/2.4/hazelcast-2.4.jar, /root/.m2/repository/org/apache/chemistry/opencmis/chemistry-opencmis-client-impl/0.11.0/chemistry-opencmis-client-impl-0.11.0.jar, /root/.m2/repository/org/apache/chemistry/opencmis/chemistry-opencmis-client-api/0.11.0/chemistry-opencmis-client-api-0.11.0.jar, /root/.m2/repository/org/apache/chemistry/opencmis/chemistry-opencmis-commons-api/0.11.0/chemistry-opencmis-commons-api-0.11.0.jar, /root/.m2/repository/org/apache/chemistry/opencmis/chemistry-opencmis-commons-impl/0.11.0/chemistry-opencmis-commons-impl-0.11.0.jar, /root/.m2/repository/org/codehaus/woodstox/woodstox-core-asl/4.2.0/woodstox-core-asl-4.2.0.jar, /root/.m2/repository/javax/xml/stream/stax-api/1.0-2/stax-api-1.0-2.jar, /root/.m2/repository/org/codehaus/woodstox/stax2-api/3.1.1/stax2-api-3.1.1.jar, /root/.m2/repository/org/jvnet/mimepull/mimepull/1.9.4/mimepull-1.9.4.jar, /root/.m2/repository/com/sun/xml/ws/jaxws-rt/2.1.7/jaxws-rt-2.1.7.jar, /root/.m2/repository/javax/xml/ws/jaxws-api/2.1/jaxws-api-2.1.jar, /root/.m2/repository/javax/xml/bind/jaxb-api/2.1/jaxb-api-2.1.jar, /root/.m2/repository/com/sun/xml/messaging/saaj/saaj-impl/1.3.3/saaj-impl-1.3.3.jar, /root/.m2/repository/javax/xml/soap/saaj-api/1.3/saaj-api-1.3.jar, /root/.m2/repository/com/sun/xml/stream/buffer/streambuffer/0.9/streambuffer-0.9.jar, /root/.m2/repository/javax/activation/activation/1.1/activation-1.1.jar, /root/.m2/repository/org/jvnet/staxex/stax-ex/1.2/stax-ex-1.2.jar, /root/.m2/repository/com/sun/org/apache/xml/internal/resolver/20050927/resolver-20050927.jar, /root/.m2/repository/org/apache/chemistry/opencmis/chemistry-opencmis-client-bindings/0.11.0/chemistry-opencmis-client-bindings-0.11.0.jar, /root/.m2/repository/org/slf4j/slf4j-api/1.7.5/slf4j-api-1.7.5.jar, /root/.m2/repository/org/alfresco/cmis/client/alfresco-opencmis-extension/1.0/alfresco-opencmis-extension-1.0.jar, /root/.m2/repository/org/springframework/extensions/surf/spring-cmis-framework/5.0.c/spring-cmis-framework-5.0.c.jar, /root/.m2/repository/org/springframework/extensions/surf/spring-webscripts/5.0.c/spring-webscripts-5.0.c.jar, /root/.m2/repository/commons-beanutils/commons-beanutils/1.9.1/commons-beanutils-1.9.1.jar, /root/.m2/repository/org/htmlparser/htmlparser/2.1/htmlparser-2.1.jar, /root/.m2/repository/org/htmlparser/htmllexer/2.1/htmllexer-2.1.jar, /root/.m2/repository/org/springframework/spring-webmvc/3.2.10.RELEASE/spring-webmvc-3.2.10.RELEASE.jar, /root/.m2/repository/org/springframework/spring-expression/3.2.10.RELEASE/spring-expression-3.2.10.RELEASE.jar, /root/.m2/repository/org/mozilla/rhino/1.7R4-alfresco-patched/rhino-1.7R4-alfresco-patched.jar, /root/.m2/repository/com/googlecode/json-simple/json-simple/1.1.1/json-simple-1.1.1.jar, /root/.m2/repository/org/tuckey/urlrewritefilter/4.0.4/urlrewritefilter-4.0.4.jar, /root/.m2/repository/commons-fileupload/commons-fileupload/1.3.1/commons-fileupload-1.3.1.jar, /root/.m2/repository/commons-io/commons-io/2.4/commons-io-2.4.jar, /root/.m2/repository/jaxen/jaxen/1.1.6/jaxen-1.1.6.jar, /root/.m2/repository/org/freemarker/freemarker/2.3.20-alfresco-patched/freemarker-2.3.20-alfresco-patched.jar, /root/.m2/repository/stax/stax-api/1.0.1/stax-api-1.0.1.jar, /root/.m2/repository/com/hazelcast/hazelcast-spring/2.4/hazelcast-spring-2.4.jar, /root/.m2/repository/javax/servlet/jstl/1.2/jstl-1.2.jar, /root/.m2/repository/org/apache/myfaces/core/myfaces-api/1.1.8/myfaces-api-1.1.8.jar, /root/.m2/repository/commons-logging/commons-logging/1.1.3/commons-logging-1.1.3.jar, /root/.m2/repository/org/apache/myfaces/core/myfaces-impl/1.1.8/myfaces-impl-1.1.8.jar, /root/.m2/repository/commons-el/commons-el/1.0/commons-el-1.0.jar, /root/.m2/repository/commons-lang/commons-lang/2.6/commons-lang-2.6.jar, /root/.m2/repository/commons-collections/commons-collections/3.2.1/commons-collections-3.2.1.jar, /root/.m2/repository/commons-digester/commons-digester/2.1/commons-digester-2.1.jar, /root/.m2/repository/com/asual/lesscss/lesscss-engine/1.5.0/lesscss-engine-1.5.0.jar, /root/.m2/repository/org/springframework/extensions/surf/spring-surf-api/5.0.c/spring-surf-api-5.0.c.jar, /root/.m2/repository/org/springframework/extensions/surf/spring-surf/5.0.c/spring-surf-5.0.c.jar, /root/.m2/repository/com/yahoo/platform/yui/yuicompressor/2.4.8-alfresco-patched/yuicompressor-2.4.8-alfresco-patched.jar, /root/.m2/repository/com/googlecode/concurrentlinkedhashmap/concurrentlinkedhashmap-lru/1.2/concurrentlinkedhashmap-lru-1.2.jar, /root/.m2/repository/org/springframework/extensions/surf/spring-webscripts-api/5.0.c/spring-webscripts-api-5.0.c.jar, /root/.m2/repository/javax/servlet/servlet-api/2.5/servlet-api-2.5.jar, /root/.m2/repository/org/alfresco/maven/alfresco-rad/2.0.0/alfresco-rad-2.0.0.jar, /root/.m2/repository/com/tradeshift/junit-remote/3/junit-remote-3.jar, /root/.m2/repository/junit/junit/4.11/junit-4.11.jar, /root/.m2/repository/org/hamcrest/hamcrest-core/1.3/hamcrest-core-1.3.jar, /root/.m2/repository/org/eclipse/jetty/jetty-servlet/7.4.0.v20110414/jetty-servlet-7.4.0.v20110414.jar, /root/.m2/repository/org/eclipse/jetty/jetty-security/7.4.0.v20110414/jetty-security-7.4.0.v20110414.jar, /root/.m2/repository/args4j/args4j/2.0.16/args4j-2.0.16.jar, /root/.m2/repository/org/springframework/spring-test/3.0.6.RELEASE/spring-test-3.0.6.RELEASE.jar, /root/.m2/repository/org/eclipse/jetty/jetty-server/7.4.0.v20110414/jetty-server-7.4.0.v20110414.jar, /root/.m2/repository/org/eclipse/jetty/jetty-continuation/7.4.0.v20110414/jetty-continuation-7.4.0.v20110414.jar, /root/.m2/repository/org/eclipse/jetty/jetty-http/7.4.0.v20110414/jetty-http-7.4.0.v20110414.jar, /root/.m2/repository/org/eclipse/jetty/jetty-io/7.4.0.v20110414/jetty-io-7.4.0.v20110414.jar, /root/.m2/repository/org/eclipse/jetty/jetty-util/7.4.0.v20110414/jetty-util-7.4.0.v20110414.jar]
[DEBUG]   (f) compileSourceRoots = [/home/programues/Documents/metadata_myoffice/content-all-share/src/main/java]
[DEBUG]   (f) compilerId = javac
[DEBUG]   (f) debug = true
[DEBUG]   (f) encoding = UTF-8
[DEBUG]   (f) failOnError = true
[DEBUG]   (f) forceJavacCompilerUse = false
[DEBUG]   (f) fork = false
[DEBUG]   (f) generatedSourcesDirectory = /home/programues/Documents/metadata_myoffice/content-all-share/target/generated-sources/annotations
[DEBUG]   (f) mojoExecution = org.apache.maven.plugins:maven-compiler-plugin:3.2:compile {execution: default-compile}
[DEBUG]   (f) optimize = false
[DEBUG]   (f) outputDirectory = /home/programues/Documents/metadata_myoffice/content-all-share/target/classes
[DEBUG]   (f) project = MavenProject: com.netprojex:content-all-share:1.0-SNAPSHOT @ /home/programues/Documents/metadata_myoffice/content-all-share/pom.xml
[DEBUG]   (f) projectArtifact = com.netprojex:content-all-share:amp:1.0-SNAPSHOT
[DEBUG]   (f) session = org.apache.maven.execution.MavenSession@21b4a7
[DEBUG]   (f) showDeprecation = false
[DEBUG]   (f) showWarnings = false
[DEBUG]   (f) skipMultiThreadWarning = false
[DEBUG]   (f) source = 1.7
[DEBUG]   (f) staleMillis = 0
[DEBUG]   (f) target = 1.7
[DEBUG]   (f) useIncrementalCompilation = true
[DEBUG]   (f) verbose = false
[DEBUG] -- end configuration --
[DEBUG] Using compiler 'javac'.
[INFO] No sources to compile
[INFO] 
[INFO] <<< tomcat7-maven-plugin:2.2:run (run-embedded) @ content-all-share <<<
[INFO] 
[INFO] --- tomcat7-maven-plugin:2.2:run (run-embedded) @ content-all-share ---
[DEBUG] org.apache.tomcat.maven:tomcat7-maven-plugin:jar:2.2:
[DEBUG]    com.h2database:h2:jar:1.4.182:runtime
[DEBUG]    org.codehaus.plexus:plexus-archiver:jar:2.3:runtime
[DEBUG]       org.codehaus.plexus:plexus-container-default:jar:1.0-alpha-9-stable-1:compile
[DEBUG]          junit:junit:jar:4.10:compile (version managed from 3.8.1)
[DEBUG]             org.hamcrest:hamcrest-core:jar:1.1:compile
[DEBUG]          classworlds:classworlds:jar:1.1-alpha-2:compile
[DEBUG]       org.codehaus.plexus:plexus-io:jar:2.0.6:runtime
[DEBUG]    org.apache.tomcat.embed:tomcat-embed-core:jar:7.0.47:compile
[DEBUG]    org.apache.tomcat:tomcat-util:jar:7.0.47:compile
[DEBUG]    org.apache.tomcat:tomcat-coyote:jar:7.0.47:compile
[DEBUG]    org.apache.tomcat:tomcat-api:jar:7.0.47:compile
[DEBUG]    org.apache.tomcat:tomcat-jdbc:jar:7.0.47:compile
[DEBUG]    org.apache.tomcat:tomcat-dbcp:jar:7.0.47:compile
[DEBUG]    org.apache.tomcat:tomcat-servlet-api:jar:7.0.47:compile
[DEBUG]    org.apache.tomcat:tomcat-jsp-api:jar:7.0.47:compile
[DEBUG]    org.apache.tomcat:tomcat-jasper:jar:7.0.47:compile
[DEBUG]    org.apache.tomcat:tomcat-jasper-el:jar:7.0.47:compile
[DEBUG]    org.apache.tomcat:tomcat-el-api:jar:7.0.47:compile
[DEBUG]    org.apache.tomcat:tomcat-catalina:jar:7.0.47:compile
[DEBUG]    org.apache.tomcat:tomcat-tribes:jar:7.0.47:compile
[DEBUG]    org.apache.tomcat:tomcat-catalina-ha:jar:7.0.47:compile
[DEBUG]    org.apache.tomcat:tomcat-annotations-api:jar:7.0.47:compile
[DEBUG]    org.apache.tomcat:tomcat-juli:jar:7.0.47:compile
[DEBUG]    org.apache.tomcat.embed:tomcat-embed-logging-juli:jar:7.0.47:compile
[DEBUG]    org.apache.tomcat.embed:tomcat-embed-logging-log4j:jar:7.0.47:compile
[DEBUG]    org.eclipse.jdt.core.compiler:ecj:jar:4.2.2:compile
[DEBUG]    org.apache.tomcat.maven:common-tomcat-maven-plugin:jar:2.2:compile
[DEBUG]       org.apache.maven:maven-project:jar:2.2.1:compile
[DEBUG]          org.apache.maven:maven-profile:jar:2.2.1:compile
[DEBUG]          org.apache.maven:maven-artifact-manager:jar:2.2.1:compile
[DEBUG]             backport-util-concurrent:backport-util-concurrent:jar:3.1:compile
[DEBUG]          org.apache.maven:maven-plugin-registry:jar:2.2.1:compile
[DEBUG]       com.google.guava:guava:jar:10.0.1:compile
[DEBUG]          com.google.code.findbugs:jsr305:jar:1.3.9:compile
[DEBUG]       org.apache.httpcomponents:httpclient:jar:4.3.1:compile
[DEBUG]          org.apache.httpcomponents:httpcore:jar:4.3:compile
[DEBUG]          commons-logging:commons-logging:jar:1.1.3:compile
[DEBUG]       org.codehaus.plexus:plexus-component-annotations:jar:1.5.5:compile
[DEBUG]       commons-codec:commons-codec:jar:1.6:compile
[DEBUG]    org.apache.tomcat.maven:tomcat7-war-runner:jar:2.2:compile
[DEBUG]       commons-cli:commons-cli:jar:1.2:compile
[DEBUG]    org.apache.maven:maven-plugin-api:jar:2.2.1:compile
[DEBUG]    org.apache.maven:maven-archiver:jar:2.4.2:compile
[DEBUG]       org.apache.maven:maven-artifact:jar:2.2.1:compile (version managed from 2.0.6)
[DEBUG]       org.apache.maven:maven-model:jar:2.2.1:compile (version managed from 2.0.6)
[DEBUG]       org.codehaus.plexus:plexus-interpolation:jar:1.13:compile
[DEBUG]    org.codehaus.plexus:plexus-classworlds:jar:2.2.2:compile
[DEBUG]    commons-io:commons-io:jar:2.2:compile
[DEBUG]    commons-lang:commons-lang:jar:2.6:compile
[DEBUG]    org.apache.commons:commons-compress:jar:1.4.1:compile
[DEBUG]       org.tukaani:xz:jar:1.0:compile
[DEBUG]    org.codehaus.plexus:plexus-utils:jar:3.0.15:compile
[DEBUG]    org.apache.maven.shared:maven-filtering:jar:1.0:compile
[DEBUG]       org.apache.maven:maven-core:jar:2.2.1:compile (version managed from 2.0.6)
[DEBUG]          org.apache.maven:maven-plugin-parameter-documenter:jar:2.2.1:compile
[DEBUG]          org.apache.maven.reporting:maven-reporting-api:jar:2.2.1:compile
[DEBUG]             org.apache.maven.doxia:doxia-sink-api:jar:1.1:compile
[DEBUG]             org.apache.maven.doxia:doxia-logging-api:jar:1.1:compile
[DEBUG]          org.apache.maven:maven-repository-metadata:jar:2.2.1:compile
[DEBUG]          org.apache.maven:maven-error-diagnostics:jar:2.2.1:compile
[DEBUG]          org.apache.maven:maven-plugin-descriptor:jar:2.2.1:compile
[DEBUG]          org.codehaus.plexus:plexus-interactivity-api:jar:1.0-alpha-4:compile
[DEBUG]          org.sonatype.plexus:plexus-sec-dispatcher:jar:1.3:compile
[DEBUG]             org.sonatype.plexus:plexus-cipher:jar:1.4:compile
[DEBUG]       org.apache.maven:maven-monitor:jar:2.2.1:compile (version managed from 2.0.6)
[DEBUG]       org.apache.maven:maven-settings:jar:2.2.1:compile (version managed from 2.0.6)
[DEBUG]       org.sonatype.plexus:plexus-build-api:jar:0.0.4:compile
[DEBUG]    org.slf4j:jcl-over-slf4j:jar:1.7.5:compile
[DEBUG]       org.slf4j:slf4j-api:jar:1.7.5:compile
[DEBUG] Created new class realm plugin>org.apache.tomcat.maven:tomcat7-maven-plugin:2.2
[DEBUG] Importing foreign packages into class realm plugin>org.apache.tomcat.maven:tomcat7-maven-plugin:2.2
[DEBUG]   Imported:  < project>com.netprojex:content-all-share:1.0-SNAPSHOT
[DEBUG] Populating class realm plugin>org.apache.tomcat.maven:tomcat7-maven-plugin:2.2
[DEBUG]   Included: org.apache.tomcat.maven:tomcat7-maven-plugin:jar:2.2
[DEBUG]   Included: com.h2database:h2:jar:1.4.182
[DEBUG]   Included: org.codehaus.plexus:plexus-archiver:jar:2.3
[DEBUG]   Included: junit:junit:jar:4.10
[DEBUG]   Included: org.hamcrest:hamcrest-core:jar:1.1
[DEBUG]   Included: org.codehaus.plexus:plexus-io:jar:2.0.6
[DEBUG]   Included: org.apache.tomcat.embed:tomcat-embed-core:jar:7.0.47
[DEBUG]   Included: org.apache.tomcat:tomcat-util:jar:7.0.47
[DEBUG]   Included: org.apache.tomcat:tomcat-coyote:jar:7.0.47
[DEBUG]   Included: org.apache.tomcat:tomcat-api:jar:7.0.47
[DEBUG]   Included: org.apache.tomcat:tomcat-jdbc:jar:7.0.47
[DEBUG]   Included: org.apache.tomcat:tomcat-dbcp:jar:7.0.47
[DEBUG]   Included: org.apache.tomcat:tomcat-servlet-api:jar:7.0.47
[DEBUG]   Included: org.apache.tomcat:tomcat-jsp-api:jar:7.0.47
[DEBUG]   Included: org.apache.tomcat:tomcat-jasper:jar:7.0.47
[DEBUG]   Included: org.apache.tomcat:tomcat-jasper-el:jar:7.0.47
[DEBUG]   Included: org.apache.tomcat:tomcat-el-api:jar:7.0.47
[DEBUG]   Included: org.apache.tomcat:tomcat-catalina:jar:7.0.47
[DEBUG]   Included: org.apache.tomcat:tomcat-tribes:jar:7.0.47
[DEBUG]   Included: org.apache.tomcat:tomcat-catalina-ha:jar:7.0.47
[DEBUG]   Included: org.apache.tomcat:tomcat-annotations-api:jar:7.0.47
[DEBUG]   Included: org.apache.tomcat:tomcat-juli:jar:7.0.47
[DEBUG]   Included: org.apache.tomcat.embed:tomcat-embed-logging-juli:jar:7.0.47
[DEBUG]   Included: org.apache.tomcat.embed:tomcat-embed-logging-log4j:jar:7.0.47
[DEBUG]   Included: org.eclipse.jdt.core.compiler:ecj:jar:4.2.2
[DEBUG]   Included: org.apache.tomcat.maven:common-tomcat-maven-plugin:jar:2.2
[DEBUG]   Included: backport-util-concurrent:backport-util-concurrent:jar:3.1
[DEBUG]   Included: com.google.guava:guava:jar:10.0.1
[DEBUG]   Included: com.google.code.findbugs:jsr305:jar:1.3.9
[DEBUG]   Included: org.apache.httpcomponents:httpclient:jar:4.3.1
[DEBUG]   Included: org.apache.httpcomponents:httpcore:jar:4.3
[DEBUG]   Included: commons-logging:commons-logging:jar:1.1.3
[DEBUG]   Included: org.codehaus.plexus:plexus-component-annotations:jar:1.5.5
[DEBUG]   Included: commons-codec:commons-codec:jar:1.6
[DEBUG]   Included: org.apache.tomcat.maven:tomcat7-war-runner:jar:2.2
[DEBUG]   Included: commons-cli:commons-cli:jar:1.2
[DEBUG]   Included: org.apache.maven:maven-archiver:jar:2.4.2
[DEBUG]   Included: org.codehaus.plexus:plexus-interpolation:jar:1.13
[DEBUG]   Included: commons-io:commons-io:jar:2.2
[DEBUG]   Included: commons-lang:commons-lang:jar:2.6
[DEBUG]   Included: org.apache.commons:commons-compress:jar:1.4.1
[DEBUG]   Included: org.tukaani:xz:jar:1.0
[DEBUG]   Included: org.codehaus.plexus:plexus-utils:jar:3.0.15
[DEBUG]   Included: org.apache.maven.shared:maven-filtering:jar:1.0
[DEBUG]   Included: org.apache.maven.reporting:maven-reporting-api:jar:2.2.1
[DEBUG]   Included: org.apache.maven.doxia:doxia-sink-api:jar:1.1
[DEBUG]   Included: org.apache.maven.doxia:doxia-logging-api:jar:1.1
[DEBUG]   Included: org.codehaus.plexus:plexus-interactivity-api:jar:1.0-alpha-4
[DEBUG]   Included: org.sonatype.plexus:plexus-sec-dispatcher:jar:1.3
[DEBUG]   Included: org.sonatype.plexus:plexus-cipher:jar:1.4
[DEBUG]   Included: org.sonatype.plexus:plexus-build-api:jar:0.0.4
[DEBUG]   Included: org.slf4j:jcl-over-slf4j:jar:1.7.5
[DEBUG]   Included: org.slf4j:slf4j-api:jar:1.7.5
[DEBUG]   Excluded: org.codehaus.plexus:plexus-container-default:jar:1.0-alpha-9-stable-1
[DEBUG]   Excluded: classworlds:classworlds:jar:1.1-alpha-2
[DEBUG]   Excluded: org.apache.maven:maven-project:jar:2.2.1
[DEBUG]   Excluded: org.apache.maven:maven-profile:jar:2.2.1
[DEBUG]   Excluded: org.apache.maven:maven-artifact-manager:jar:2.2.1
[DEBUG]   Excluded: org.apache.maven:maven-plugin-registry:jar:2.2.1
[DEBUG]   Excluded: org.apache.maven:maven-plugin-api:jar:2.2.1
[DEBUG]   Excluded: org.apache.maven:maven-artifact:jar:2.2.1
[DEBUG]   Excluded: org.apache.maven:maven-model:jar:2.2.1
[DEBUG]   Excluded: org.codehaus.plexus:plexus-classworlds:jar:2.2.2
[DEBUG]   Excluded: org.apache.maven:maven-core:jar:2.2.1
[DEBUG]   Excluded: org.apache.maven:maven-plugin-parameter-documenter:jar:2.2.1
[DEBUG]   Excluded: org.apache.maven:maven-repository-metadata:jar:2.2.1
[DEBUG]   Excluded: org.apache.maven:maven-error-diagnostics:jar:2.2.1
[DEBUG]   Excluded: org.apache.maven:maven-plugin-descriptor:jar:2.2.1
[DEBUG]   Excluded: org.apache.maven:maven-monitor:jar:2.2.1
[DEBUG]   Excluded: org.apache.maven:maven-settings:jar:2.2.1
[DEBUG] Configuring mojo org.apache.tomcat.maven:tomcat7-maven-plugin:2.2:run from plugin realm ClassRealm[plugin>org.apache.tomcat.maven:tomcat7-maven-plugin:2.2, parent: sun.misc.Launcher$AppClassLoader@71b456f]
[DEBUG] Configuring mojo 'org.apache.tomcat.maven:tomcat7-maven-plugin:2.2:run' with basic configurator -->
[DEBUG]   (f) addContextWarDependencies = false
[DEBUG]   (f) addWarDependenciesInClassloader = true
[DEBUG]   (f) additionalConfigFilesDir = /home/programues/Documents/metadata_myoffice/content-all-share/src/main/tomcatconf
[DEBUG]   (f) ajpPort = 0
[DEBUG]   (f) ajpProtocol = org.apache.coyote.ajp.AjpProtocol
[DEBUG]   (f) backgroundProcessorDelay = -1
[DEBUG]   (f) clientAuth = false
[DEBUG]   (f) configurationDir = /home/programues/Documents/metadata_myoffice/content-all-share/target/tomcat
[DEBUG]   (f) contextFile = /home/programues/Documents/metadata_myoffice/content-all-share/tomcat/context.xml
[DEBUG]   (f) contextReloadable = false
[DEBUG]   (f) defaultContextFile = /home/programues/Documents/metadata_myoffice/content-all-share/target/content-all-share/META-INF/context.xml
[DEBUG]   (f) delegate = true
[DEBUG]   (f) dependencies = [org.alfresco:share:jar:classes:5.0.c:provided, org.alfresco:alfresco-web-framework-commons:jar:classes:5.0.c:provided, org.alfresco:alfresco-jlan-embed:jar:5.0.c:provided, org.alfresco:alfresco-core:jar:5.0.c:provided, commons-codec:commons-codec:jar:1.9:provided, commons-httpclient:commons-httpclient:jar:3.1:provided, org.safehaus.jug:jug:jar:asl:2.0.0:provided, log4j:log4j:jar:1.2.17:provided, org.json:json:jar:20090211:provided, org.springframework:spring-orm:jar:3.2.10.RELEASE:provided, org.springframework:spring-beans:jar:3.2.10.RELEASE:provided, org.springframework:spring-core:jar:3.2.10.RELEASE:provided, org.springframework:spring-jdbc:jar:3.2.10.RELEASE:provided, org.springframework:spring-tx:jar:3.2.10.RELEASE:provided, org.springframework.extensions.surf:spring-surf-core-configservice:jar:5.0.c:provided, org.springframework.extensions.surf:spring-surf-core:jar:5.0.c:provided, org.springframework:spring-context:jar:3.2.10.RELEASE:provided, org.springframework:spring-aop:jar:3.2.10.RELEASE:provided, org.springframework:spring-context-support:jar:3.2.10.RELEASE:provided, org.springframework:spring-web:jar:3.2.10.RELEASE:provided, com.sun.xml.bind:jaxb-impl:jar:2.1.11:provided, dom4j:dom4j:jar:1.6.1:provided, xml-apis:xml-apis:jar:1.4.01:provided, org.codehaus.guessencoding:guessencoding:jar:1.0:provided, javax.transaction:jta:jar:1.0.1b:provided, joda-time:joda-time:jar:2.3:provided, org.apache.httpcomponents:httpclient:jar:4.3.3:provided, org.apache.httpcomponents:httpcore:jar:4.3.2:provided, com.hazelcast:hazelcast:jar:2.4:provided, org.apache.chemistry.opencmis:chemistry-opencmis-client-impl:jar:0.11.0:provided, org.apache.chemistry.opencmis:chemistry-opencmis-client-api:jar:0.11.0:provided, org.apache.chemistry.opencmis:chemistry-opencmis-commons-api:jar:0.11.0:provided, org.apache.chemistry.opencmis:chemistry-opencmis-commons-impl:jar:0.11.0:provided, org.codehaus.woodstox:woodstox-core-asl:jar:4.2.0:provided, javax.xml.stream:stax-api:jar:1.0-2:provided, org.codehaus.woodstox:stax2-api:jar:3.1.1:provided, org.jvnet.mimepull:mimepull:jar:1.9.4:provided, com.sun.xml.ws:jaxws-rt:jar:2.1.7:provided, javax.xml.ws:jaxws-api:jar:2.1:provided, javax.xml.bind:jaxb-api:jar:2.1:provided, com.sun.xml.messaging.saaj:saaj-impl:jar:1.3.3:provided, javax.xml.soap:saaj-api:jar:1.3:provided, com.sun.xml.stream.buffer:streambuffer:jar:0.9:provided, javax.activation:activation:jar:1.1:provided, org.jvnet.staxex:stax-ex:jar:1.2:provided, com.sun.org.apache.xml.internal:resolver:jar:20050927:provided, org.apache.chemistry.opencmis:chemistry-opencmis-client-bindings:jar:0.11.0:provided, org.slf4j:slf4j-api:jar:1.7.5:compile, org.alfresco.cmis.client:alfresco-opencmis-extension:jar:1.0:provided, org.springframework.extensions.surf:spring-cmis-framework:jar:5.0.c:provided, org.springframework.extensions.surf:spring-webscripts:jar:5.0.c:provided, commons-beanutils:commons-beanutils:jar:1.9.1:provided, org.htmlparser:htmlparser:jar:2.1:provided, org.htmlparser:htmllexer:jar:2.1:provided, org.springframework:spring-webmvc:jar:3.2.10.RELEASE:provided, org.springframework:spring-expression:jar:3.2.10.RELEASE:provided, org.mozilla:rhino:jar:1.7R4-alfresco-patched:provided, com.googlecode.json-simple:json-simple:jar:1.1.1:provided, org.tuckey:urlrewritefilter:jar:4.0.4:provided, commons-fileupload:commons-fileupload:jar:1.3.1:provided, commons-io:commons-io:jar:2.4:provided, jaxen:jaxen:jar:1.1.6:provided, org.freemarker:freemarker:jar:2.3.20-alfresco-patched:provided, stax:stax-api:jar:1.0.1:provided, com.hazelcast:hazelcast-spring:jar:2.4:provided, javax.servlet:jstl:jar:1.2:provided, org.apache.myfaces.core:myfaces-api:jar:1.1.8:provided, commons-logging:commons-logging:jar:1.1.3:provided, org.apache.myfaces.core:myfaces-impl:jar:1.1.8:provided, commons-el:commons-el:jar:1.0:provided, commons-lang:commons-lang:jar:2.6:provided, commons-collections:commons-collections:jar:3.2.1:provided, commons-digester:commons-digester:jar:2.1:provided, org.dojotoolkit:dojo:zip:1.9.0:provided, com.yahoo.platform.yui:yui:zip:2.9.0-alfresco-20140211:provided, com.asual.lesscss:lesscss-engine:jar:1.5.0:provided, org.springframework.extensions.surf:spring-surf-api:jar:5.0.c:provided, org.springframework.extensions.surf:spring-surf:jar:5.0.c:provided, com.yahoo.platform.yui:yuicompressor:jar:2.4.8-alfresco-patched:provided, com.googlecode.concurrentlinkedhashmap:concurrentlinkedhashmap-lru:jar:1.2:provided, org.springframework.extensions.surf:spring-webscripts-api:jar:5.0.c:provided, javax.servlet:servlet-api:jar:2.5:provided, org.alfresco.maven:alfresco-rad:jar:2.0.0:compile, com.tradeshift:junit-remote:jar:3:compile, junit:junit:jar:4.11:compile, org.hamcrest:hamcrest-core:jar:1.3:compile, org.eclipse.jetty:jetty-servlet:jar:7.4.0.v20110414:compile, org.eclipse.jetty:jetty-security:jar:7.4.0.v20110414:compile, args4j:args4j:jar:2.0.16:compile, org.springframework:spring-test:jar:3.0.6.RELEASE:compile, org.eclipse.jetty:jetty-server:jar:7.4.0.v20110414:compile, org.eclipse.jetty:jetty-continuation:jar:7.4.0.v20110414:compile, org.eclipse.jetty:jetty-http:jar:7.4.0.v20110414:compile, org.eclipse.jetty:jetty-io:jar:7.4.0.v20110414:compile, org.eclipse.jetty:jetty-util:jar:7.4.0.v20110414:compile]
[DEBUG]   (f) fork = false
[DEBUG]   (f) hostName = localhost
[DEBUG]   (f) httpsPort = 0
[DEBUG]   (f) ignorePackaging = true
[DEBUG]   (f) jarScanAllDirectories = true
[DEBUG]   (f) keystoreType = JKS
[DEBUG]   (f) local =        id: local
      url: file:///root/.m2/repository/
   layout: none

[DEBUG]   (f) packaging = amp
[DEBUG]   (f) path = /content-all-share
[DEBUG]   (f) pluginArtifacts = [org.apache.tomcat.maven:tomcat7-maven-plugin:maven-plugin:2.2:, com.h2database:h2:jar:1.4.182:runtime, org.codehaus.plexus:plexus-archiver:jar:2.3:runtime, junit:junit:jar:4.10:compile, org.hamcrest:hamcrest-core:jar:1.1:compile, org.codehaus.plexus:plexus-io:jar:2.0.6:runtime, org.apache.tomcat.embed:tomcat-embed-core:jar:7.0.47:compile, org.apache.tomcat:tomcat-util:jar:7.0.47:compile, org.apache.tomcat:tomcat-coyote:jar:7.0.47:compile, org.apache.tomcat:tomcat-api:jar:7.0.47:compile, org.apache.tomcat:tomcat-jdbc:jar:7.0.47:compile, org.apache.tomcat:tomcat-dbcp:jar:7.0.47:compile, org.apache.tomcat:tomcat-servlet-api:jar:7.0.47:compile, org.apache.tomcat:tomcat-jsp-api:jar:7.0.47:compile, org.apache.tomcat:tomcat-jasper:jar:7.0.47:compile, org.apache.tomcat:tomcat-jasper-el:jar:7.0.47:compile, org.apache.tomcat:tomcat-el-api:jar:7.0.47:compile, org.apache.tomcat:tomcat-catalina:jar:7.0.47:compile, org.apache.tomcat:tomcat-tribes:jar:7.0.47:compile, org.apache.tomcat:tomcat-catalina-ha:jar:7.0.47:compile, org.apache.tomcat:tomcat-annotations-api:jar:7.0.47:compile, org.apache.tomcat:tomcat-juli:jar:7.0.47:compile, org.apache.tomcat.embed:tomcat-embed-logging-juli:jar:7.0.47:compile, org.apache.tomcat.embed:tomcat-embed-logging-log4j:jar:7.0.47:compile, org.eclipse.jdt.core.compiler:ecj:jar:4.2.2:compile, org.apache.tomcat.maven:common-tomcat-maven-plugin:jar:2.2:compile, backport-util-concurrent:backport-util-concurrent:jar:3.1:compile, com.google.guava:guava:jar:10.0.1:compile, com.google.code.findbugs:jsr305:jar:1.3.9:compile, org.apache.httpcomponents:httpclient:jar:4.3.1:compile, org.apache.httpcomponents:httpcore:jar:4.3:compile, commons-logging:commons-logging:jar:1.1.3:compile, org.codehaus.plexus:plexus-component-annotations:jar:1.5.5:compile, commons-codec:commons-codec:jar:1.6:compile, org.apache.tomcat.maven:tomcat7-war-runner:jar:2.2:compile, commons-cli:commons-cli:jar:1.2:compile, org.apache.maven:maven-archiver:jar:2.4.2:compile, org.codehaus.plexus:plexus-interpolation:jar:1.13:compile, commons-io:commons-io:jar:2.2:compile, commons-lang:commons-lang:jar:2.6:compile, org.apache.commons:commons-compress:jar:1.4.1:compile, org.tukaani:xz:jar:1.0:compile, org.codehaus.plexus:plexus-utils:jar:3.0.15:compile, org.apache.maven.shared:maven-filtering:jar:1.0:compile, org.apache.maven.reporting:maven-reporting-api:jar:2.2.1:compile, org.apache.maven.doxia:doxia-sink-api:jar:1.1:compile, org.apache.maven.doxia:doxia-logging-api:jar:1.1:compile, org.codehaus.plexus:plexus-interactivity-api:jar:1.0-alpha-4:compile, org.sonatype.plexus:plexus-sec-dispatcher:jar:1.3:compile, org.sonatype.plexus:plexus-cipher:jar:1.4:compile, org.sonatype.plexus:plexus-build-api:jar:0.0.4:compile, org.slf4j:jcl-over-slf4j:jar:1.7.5:compile, org.slf4j:slf4j-api:jar:1.7.5:compile]
[DEBUG]   (f) port = 8081
[DEBUG]   (f) protocol = HTTP/1.1
[DEBUG]   (f) session = org.apache.maven.execution.MavenSession@21b4a7
[DEBUG]   (f) skip = false
[DEBUG]   (f) staticContextPath = /
[DEBUG]   (f) systemProperties = {java.io.tmpdir=/home/programues/Documents/metadata_myoffice/content-all-share/target}
[DEBUG]   (f) uriEncoding = ISO-8859-1
[DEBUG]   (f) useBodyEncodingForURI = false
[DEBUG]   (f) useNaming = true
[DEBUG]   (f) useSeparateTomcatClassLoader = true
[DEBUG]   (f) useTestClasspath = false
[DEBUG]   (f) warSourceDirectory = /home/programues/Documents/metadata_myoffice/content-all-share/src/main/webapp
[DEBUG]   (f) project = MavenProject: com.netprojex:content-all-share:1.0-SNAPSHOT @ /home/programues/Documents/metadata_myoffice/content-all-share/pom.xml
[DEBUG]   (f) settings = org.apache.maven.execution.SettingsAdapter@3207905b
[DEBUG] -- end configuration --
[INFO] Running war on http://localhost:8081/content-all-share
[INFO] Using existing Tomcat server configuration at /home/programues/Documents/metadata_myoffice/content-all-share/target/tomcat
[INFO] setting SystemProperties:
[INFO]  java.io.tmpdir=/home/programues/Documents/metadata_myoffice/content-all-share/target
[DEBUG] properties used {env.OLDPWD=/home/programues/Documents/metadata_myoffice/content-all-share, file.encoding.pkg=sun.io, java.home=/usr/lib/jvm/java-7-openjdk-amd64/jre, app.testing.jvm.args=-Xms256m -Xmx1524m -XX:MaxPermSize=256m -Duser.language=en, stagingDirectory=../../target/staging/poms/alfresco-sdk-parent, env.DISPLAY=:0.0, env.LS_COLORS=rs=0:di=01;34:ln=01;36:mh=00:pi=40;33:so=01;35:do=01;35:bd=40;33;01:cd=40;33;01:or=40;31;01:su=37;41:sg=30;43:ca=30;41:tw=30;42:ow=34;42:st=37;44:ex=01;32:*.tar=01;31:*.tgz=01;31:*.arj=01;31:*.taz=01;31:*.lzh=01;31:*.lzma=01;31:*.tlz=01;31:*.txz=01;31:*.zip=01;31:*.z=01;31:*.Z=01;31:*.dz=01;31:*.gz=01;31:*.lz=01;31:*.xz=01;31:*.bz2=01;31:*.bz=01;31:*.tbz=01;31:*.tbz2=01;31:*.tz=01;31:*.deb=01;31:*.rpm=01;31:*.jar=01;31:*.war=01;31:*.ear=01;31:*.sar=01;31:*.rar=01;31:*.ace=01;31:*.zoo=01;31:*.cpio=01;31:*.7z=01;31:*.rz=01;31:*.jpg=01;35:*.jpeg=01;35:*.gif=01;35:*.bmp=01;35:*.pbm=01;35:*.pgm=01;35:*.ppm=01;35:*.tga=01;35:*.xbm=01;35:*.xpm=01;35:*.tif=01;35:*.tiff=01;35:*.png=01;35:*.svg=01;35:*.svgz=01;35:*.mng=01;35:*.pcx=01;35:*.mov=01;35:*.mpg=01;35:*.mpeg=01;35:*.m2v=01;35:*.mkv=01;35:*.webm=01;35:*.ogm=01;35:*.mp4=01;35:*.m4v=01;35:*.mp4v=01;35:*.vob=01;35:*.qt=01;35:*.nuv=01;35:*.wmv=01;35:*.asf=01;35:*.rm=01;35:*.rmvb=01;35:*.flc=01;35:*.avi=01;35:*.fli=01;35:*.flv=01;35:*.gl=01;35:*.dl=01;35:*.xcf=01;35:*.xwd=01;35:*.yuv=01;35:*.cgm=01;35:*.emf=01;35:*.axv=01;35:*.anx=01;35:*.ogv=01;35:*.ogx=01;35:*.aac=00;36:*.au=00;36:*.flac=00;36:*.mid=00;36:*.midi=00;36:*.mka=00;36:*.mp3=00;36:*.mpc=00;36:*.ogg=00;36:*.ra=00;36:*.wav=00;36:*.axa=00;36:*.oga=00;36:*.spx=00;36:*.xspf=00;36:, env.XDG_VTNR=7, alfresco.client.war=share, alfresco.client.war.version=5.0.c, classworlds.conf=/usr/share/maven/bin/m2.conf, env.SUDO_USER=programues, app.log.root.level=WARN, java.endorsed.dirs=/usr/lib/jvm/java-7-openjdk-amd64/jre/lib/endorsed, env.LOGNAME=root, alfresco.db.password=alfresco, env.USERNAME=root, maven.jar.version=2.5, app.amp.excludes=, env.XDG_SEAT=seat0, maven.war.version=2.5, env.LC_PAPER=cs_CZ.UTF-8, sun.os.patch.level=unknown, java.vendor.url=http://java.oracle.com/, maven.tomcat.port=8081, maven.dependency.version=2.9, maven.resources.version=2.7, java.version=1.7.0_101, env.ANDROID_HOME=/op/adt/sdk/, env.MAVEN_OPTS=-javaagent:/root/.m2/repository/org/springframework/springloaded/1.2.0.RELEASE/springloaded-1.2.0.RELEASE.jar -noverify, java.vendor.url.bug=http://bugreport.sun.com/bugreport/, env.LANGUAGE=en_US, skipTests=true, alfresco.groupId=org.alfresco, maven.alfresco.version=2.0.0, user.name=root, env.LANG=en_US.UTF-8, env.LC_MONETARY=cs_CZ.UTF-8, sun.io.unicode.encoding=UnicodeLittle, sun.jnu.encoding=UTF-8, java.runtime.name=OpenJDK Runtime Environment, env.LC_NAME=cs_CZ.UTF-8, java.specification.name=Java Platform API Specification, user.timezone=, env.LESSOPEN=| /usr/bin/lesspipe %s, path.separator=:, alfresco.db.url=jdbc:h2:./alf_data_dev/h2_data/alf_dev;MODE=PostgreSQL;AUTO_SERVER=TRUE;DB_CLOSE_ON_EXIT=FALSE;LOCK_TIMEOUT=10000;MVCC=FALSE;LOCK_MODE=0, file.encoding=UTF-8, env.HOME=/root, app.amp.output.folder=../content-all-share, sun.java.command=org.codehaus.plexus.classworlds.launcher.Launcher integration-test -Pamp-to-war -X, maven.install.version=2.5.2, env.LC_TELEPHONE=cs_CZ.UTF-8, env._=/usr/bin/mvn, app.filtering.enabled=true, java.io.tmpdir=/tmp, env.LC_IDENTIFICATION=cs_CZ.UTF-8, env.ANRDOID_NDK_ROOT=/home/programues/Android/android-ndk-r12b/, user.language=en, app.slf4j.scope=runtime, env.NDK=/home/programues/Android/android-ndk-r12b/, line.separator=
, app.amp.overlay.excludes=, app.log.dir=/home/programues/Documents/metadata_myoffice/content-all-share/target/, env=local, alfresco.community.default.version=5.0.c, alfresco.data.location=alf_data_dev, maven.tomcat.version=2.2, java.vm.info=mixed mode, maven.buildhelper.version=1.9.1, alfresco.db.username=alfresco, java.vm.specification.name=Java Virtual Machine Specification, alfresco.client.war.folder=/home/programues/Documents/metadata_myoffice/content-all-share/target/content-all-share-war, env.COLORTERM=xfce4-terminal, app.slf4j.version=1.5.11, env.LC_MEASUREMENT=cs_CZ.UTF-8, alfresco.repo.url=http://localhost:8080/alfresco, env.LC_TIME=cs_CZ.UTF-8, java.awt.printerjob=sun.print.PSPrinterJob, maven.jetty.version=7.4.0.v20110414, alfresco.client.contextPath=/share, maven.clean.version=2.6.1, os.name=Linux, java.specification.vendor=Oracle Corporation, alfresco.db.params=MODE=PostgreSQL;AUTO_SERVER=TRUE;DB_CLOSE_ON_EXIT=FALSE;LOCK_TIMEOUT=10000;MVCC=FALSE;LOCK_MODE=0, alfresco.repo.artifactId=alfresco, env.TERM=xterm, java.vm.name=OpenJDK 64-Bit Server VM, java.library.path=/usr/java/packages/lib/amd64:/usr/lib/x86_64-linux-gnu/jni:/lib/x86_64-linux-gnu:/usr/lib/x86_64-linux-gnu:/usr/lib/jni:/lib:/usr/lib, env.PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/games:/usr/local/games:/opt/android-sdk/sdk/tools:/opt/android-sdk/sdk/platform-tools, env.LESSCLOSE=/usr/bin/lesspipe %s %s, alfresco.client.war.groupId=org.alfresco, alfresco.version=5.0.c, java.class.version=51.0, env.SHLVL=2, app.properties.test.include=**, alfresco.db.name=alf_dev, maven.compiler.version=3.2, sun.boot.library.path=/usr/lib/jvm/java-7-openjdk-amd64/jre/lib/amd64, project.build.sourceEncoding=UTF-8, app.amp.folder=src/main/amp, sun.management.compiler=HotSpot 64-Bit Tiered Compilers, java.awt.graphicsenv=sun.awt.X11GraphicsEnvironment, maven.enforcer.plugin=1.3.1, env.USER=root, junit.version=4.11, alfresco.sdk.parent.version=1.0-SNAPSHOT, java.vm.specification.version=1.7, springloaded.version=1.2.0.RELEASE, awt.toolkit=sun.awt.X11.XToolkit, sun.cpu.isalist=, alfresco.db.datasource.class=org.h2.jdbcx.JdbcDataSource, java.ext.dirs=/usr/lib/jvm/java-7-openjdk-amd64/jre/lib/ext:/usr/java/packages/lib/ext, os.version=3.19.0-64-generic, user.home=/root, maven.release.version=2.5.1, java.vm.vendor=Oracle Corporation, env.JAVA_HOME=/usr/lib/jvm/java-7-openjdk-amd64, env.SUDO_COMMAND=/bin/su, env.XDG_SESSION_ID=c1, env.XAUTHORITY=/home/programues/.Xauthority, app.properties.test.folder=src/test/properties/local, user.dir=/home/programues/Documents/metadata_myoffice/content-all-share, app.properties.include=**, env.MAIL=/var/mail/root, env.NDK_TARGET=arm-linux-androideabi-4.9, env.PWD=/home/programues/Documents/metadata_myoffice/content-all-share, sun.cpu.endian=little, maven.antrun.version=1.7, java.vm.version=24.95-b01, java.class.path=/usr/share/maven/boot/plexus-classworlds-2.x.jar:/root/.m2/repository/org/springframework/springloaded/1.2.0.RELEASE/springloaded-1.2.0.RELEASE.jar, env.LC_ADDRESS=cs_CZ.UTF-8, os.arch=amd64, maven.build.version=Apache Maven 3.0.5, env.LC_NUMERIC=cs_CZ.UTF-8, env.SUDO_UID=1000, sun.java.launcher=SUN_STANDARD, alfresco.enterprise.default.version=5.0, java.vm.specification.vendor=Oracle Corporation, noSnapshotVersion=1.0.1702271512, file.separator=/, h2.version=1.4.182, java.runtime.version=1.7.0_101-b00, sun.boot.class.path=/usr/lib/jvm/java-7-openjdk-amd64/jre/lib/resources.jar:/usr/lib/jvm/java-7-openjdk-amd64/jre/lib/rt.jar:/usr/lib/jvm/java-7-openjdk-amd64/jre/lib/sunrsasign.jar:/usr/lib/jvm/java-7-openjdk-amd64/jre/lib/jsse.jar:/usr/lib/jvm/java-7-openjdk-amd64/jre/lib/jce.jar:/usr/lib/jvm/java-7-openjdk-amd64/jre/lib/charsets.jar:/usr/lib/jvm/java-7-openjdk-amd64/jre/lib/rhino.jar:/usr/lib/jvm/java-7-openjdk-amd64/jre/lib/jfr.jar:/usr/lib/jvm/java-7-openjdk-amd64/jre/classes, maven.replacer.version=1.5.3, scm.url.base=https://github.com/Alfresco/alfresco-sdk, maven.surefire.version=2.18, maven.version=3.0.5, maven.archetype.version=2.2, user.country=US, env.SHELL=/bin/bash, maven.home=/usr/share/maven, alfresco.share.artifactId=share, java.vendor=Oracle Corporation, app.properties.folder=src/main/properties/local, java.specification.version=1.7, env.SUDO_GID=1000, sun.arch.data.model=64}
[DEBUG] filtering /home/programues/Documents/metadata_myoffice/content-all-share/tomcat/context.xml to /home/programues/Documents/metadata_myoffice/content-all-share/target/tomcat-maven-plugin8606966824735008421temp-ctx-file
[DEBUG] context reloadable: false
[DEBUG]  generated context file <Context docBase="/home/programues/Documents/metadata_myoffice/content-all-share/target/content-all-share-war" path="/share" reloadable="false" backgroundProcessorDelay="-1">
  <Resources extraResourcePaths="/=/home/programues/Documents/metadata_myoffice/content-all-share/target/content-all-share/web" className="org.apache.naming.resources.VirtualDirContext"/>
  <Loader virtualClasspath="/home/programues/Documents/metadata_myoffice/content-all-share/target/classes;/home/programues/Documents/metadata_myoffice/content-all-share/target/test-classes;/home/programues/Documents/metadata_myoffice/content-all-share/target/content-all-share/config" className="org.apache.catalina.loader.VirtualWebappLoader" searchVirtualFirst="true"/>
  <JarScanner scanAllDirectories="true"/>
</Context>
[INFO] create webapp with contextPath: /share
[DEBUG] context reloadable: false
[DEBUG] adding classPathElementFile file:/home/programues/Documents/metadata_myoffice/content-all-share/target/classes/
[DEBUG] add dependency to webapploader org.slf4j:slf4j-api:1.7.5:compile
[DEBUG] add dependency to webapploader org.alfresco.maven:alfresco-rad:2.0.0:compile
[DEBUG] add dependency to webapploader com.tradeshift:junit-remote:3:compile
[DEBUG] add dependency to webapploader junit:junit:4.11:compile
[DEBUG] add dependency to webapploader org.hamcrest:hamcrest-core:1.3:compile
[DEBUG] add dependency to webapploader org.eclipse.jetty:jetty-servlet:7.4.0.v20110414:compile
[DEBUG] add dependency to webapploader org.eclipse.jetty:jetty-security:7.4.0.v20110414:compile
[DEBUG] add dependency to webapploader args4j:args4j:2.0.16:compile
[DEBUG] add dependency to webapploader org.springframework:spring-test:3.0.6.RELEASE:compile
[DEBUG] add dependency to webapploader org.eclipse.jetty:jetty-server:7.4.0.v20110414:compile
[DEBUG] add dependency to webapploader org.eclipse.jetty:jetty-continuation:7.4.0.v20110414:compile
[DEBUG] add dependency to webapploader org.eclipse.jetty:jetty-http:7.4.0.v20110414:compile
[DEBUG] add dependency to webapploader org.eclipse.jetty:jetty-io:7.4.0.v20110414:compile
[DEBUG] add dependency to webapploader org.eclipse.jetty:jetty-util:7.4.0.v20110414:compile
2017-02-27 15:13:18,247  INFO  [extensions.webscripts.TemplateProcessorRegistry] [localhost-startStop-1] Registered template processor freemarker for extension ftl
 2017-02-27 15:13:18,279  INFO  [extensions.webscripts.ScriptProcessorRegistry] [localhost-startStop-1] Registered script processor javascript for extension js
 2017-02-27 15:13:18,280  INFO  [extensions.webscripts.TemplateProcessorRegistry] [localhost-startStop-1] Registered template processor freemarker for extension ftl
 2017-02-27 15:13:18,282  INFO  [extensions.webscripts.ScriptProcessorRegistry] [localhost-startStop-1] Registered script processor javascript for extension js
 2017-02-27 15:13:24,847  INFO  [extensions.webscripts.DeclarativeRegistry] [localhost-startStop-1] Registered 369 Web Scripts (+0 failed), 383 URLs
 2017-02-27 15:13:24,873  INFO  [extensions.webscripts.DeclarativeRegistry] [localhost-startStop-1] Registered 8 Package Description Documents (+0 failed) 
 2017-02-27 15:13:24,873  INFO  [extensions.webscripts.DeclarativeRegistry] [localhost-startStop-1] Registered 0 Schema Description Documents (+0 failed) 
 2017-02-27 15:13:25,577  INFO  [extensions.webscripts.AbstractRuntimeContainer] [localhost-startStop-1] Initialised Spring Surf Container Web Script Container (in 7270.9146ms)
 2017-02-27 15:13:25,589  INFO  [extensions.webscripts.TemplateProcessorRegistry] [localhost-startStop-1] Registered template processor freemarker for extension ftl
 2017-02-27 15:13:25,591  INFO  [extensions.webscripts.ScriptProcessorRegistry] [localhost-startStop-1] Registered script processor javascript for extension js
 2017-02-27 15:13:25,952  WARN  [shared_impl.util.LocaleUtils] [localhost-startStop-1] Locale name in faces-config.xml null or empty, setting locale to default locale : en_US
 2017-02-27 15:14:30,464  INFO  [web.site.EditionInterceptor] [http-bio-8081-exec-3] Successfully retrieved license information from Alfresco.
 2017-02-27 15:14:42,057  INFO  [web.scripts.ImapServerStatus] [http-bio-8081-exec-5] Successfully retrieved IMAP server status from Alfresco: disabled
 2017-02-27 15:14:57,485  INFO  [web.scripts.MimetypesQuery] [http-bio-8081-exec-7] Successfully retrieved mimetypes information from Alfresco.
 