package primaNotaFacile;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

import net.unnitech.primanotafacile.*;


@Endpoint
public class ArchiveEndpoint {
	private static final String NAMESPACE_URI = "http://unnitech.net/primanotafacile";

	private ArchiveRepository archiveRepository;

	@Autowired
	public ArchiveEndpoint(ArchiveRepository archiveRepository) {
		this.archiveRepository = archiveRepository;
	}
	
	@PayloadRoot(namespace = NAMESPACE_URI, localPart = "addArchiveRequest")
	@ResponsePayload
	public AddArchiveResponse addArchive(@RequestPayload AddArchiveRequest request) {
		AddArchiveResponse response = new AddArchiveResponse();
		response.setResponse(archiveRepository.addArchive(request.getArchive()));

		return response;
	}
	
	@PayloadRoot(namespace = NAMESPACE_URI, localPart = "deleteArchiveRequest")
	@ResponsePayload
	public DeleteArchiveResponse deleteArchive(@RequestPayload DeleteArchiveRequest request) {
		DeleteArchiveResponse response = new DeleteArchiveResponse();
		response.setResponse(archiveRepository.deleteArchive(request.getArchiveName()));

		return response;
	}
	
	@PayloadRoot(namespace = NAMESPACE_URI, localPart = "updateArchiveRequest")
	@ResponsePayload
	public UpdateArchiveResponse updateArchive(@RequestPayload UpdateArchiveRequest request) {
		UpdateArchiveResponse response = new UpdateArchiveResponse();
		response.setResponse(archiveRepository.updateArchive(request.getOldArchive(), request.getNewArchive()));
		return response;
	}
	
	@PayloadRoot(namespace = NAMESPACE_URI, localPart = "getArchiveRequest")
	@ResponsePayload
	public GetArchiveResponse getArchive(@RequestPayload GetArchiveRequest request) {
		GetArchiveResponse response = new GetArchiveResponse();
		response.setArchive(archiveRepository.findArchive(request.getArchiveName()));

		return response;
	}
  
	@PayloadRoot(namespace = NAMESPACE_URI, localPart = "getAllArchiveRequest")
	@ResponsePayload
	public GetAllArchiveResponse getAllArchive(@RequestPayload GetAllArchiveRequest request) {
		GetAllArchiveResponse response = new GetAllArchiveResponse();
		response.setAllArchiveResponse(archiveRepository.findAllArchives());

		return response;
	}
	
	@PayloadRoot(namespace = NAMESPACE_URI, localPart = "copyArchiveRequest")
	@ResponsePayload
	public CopyArchiveResponse copyArchive(@RequestPayload CopyArchiveRequest request) {
		CopyArchiveResponse response = new CopyArchiveResponse();
		response.setResponse(archiveRepository.copyArchive(request.getArchiveName()));

		return response;
	}
  
	
	//methods for bank
	@PayloadRoot(namespace = NAMESPACE_URI, localPart = "getBankRequest")
	@ResponsePayload
	public GetBankResponse getBank(@RequestPayload GetBankRequest request) {
		GetBankResponse response = new GetBankResponse();
		response.setBank(archiveRepository.findBank(request.getBankId(),request.getArchiveName()));
		return response;
	}
	
	@PayloadRoot(namespace = NAMESPACE_URI, localPart = "getAllBankRequest")
	@ResponsePayload
	public GetAllBankResponse getAllBank(@RequestPayload GetAllBankRequest request) {
		GetAllBankResponse response = new GetAllBankResponse();
		response.setAllBankResponse(archiveRepository.findAllBanks(request.getArchiveName()));

		return response;
	}
	
	@PayloadRoot(namespace = NAMESPACE_URI, localPart = "getAllBankCustomRequest")
	@ResponsePayload
	public GetAllBankCustomResponse getAllBankCustom(@RequestPayload GetAllBankCustomRequest request) {
		GetAllBankCustomResponse response = new GetAllBankCustomResponse();
		response.setAllBankCustomResponse(archiveRepository.findAllBanksCustom(request.getArchiveName(),request.getQueryClient()));

		return response;
	}
	
	@PayloadRoot(namespace = NAMESPACE_URI, localPart = "getBanksLikeNameRequest")
	@ResponsePayload
	public GetBanksLikeNameResponse getBanksLikeName(@RequestPayload GetBanksLikeNameRequest request) {
		GetBanksLikeNameResponse response = new GetBanksLikeNameResponse();
		response.setAllBanksLikeNameResponse(archiveRepository.findBanksLikeName(request.getName(),request.getArchiveName()));

		return response;
	}
	
	@PayloadRoot(namespace = NAMESPACE_URI, localPart = "getBanksByArchiveRequest")
	@ResponsePayload
	public GetBanksByArchiveResponse getBanksByArchive(@RequestPayload GetBanksByArchiveRequest request) {
		GetBanksByArchiveResponse response = new GetBanksByArchiveResponse();
		response.setAllBanksByArchiveResponse(archiveRepository.findBanksByArchive(request.getArchiveName()));

		return response;
	}
	
	@PayloadRoot(namespace = NAMESPACE_URI, localPart = "addBankRequest")
	@ResponsePayload
	public AddBankResponse addBank(@RequestPayload AddBankRequest request) {
		AddBankResponse response = new AddBankResponse();
		response.setResponse(archiveRepository.addBank(request.getBank(),request.getArchiveName()));

		return response;
	}
	
	@PayloadRoot(namespace = NAMESPACE_URI, localPart = "deleteBankRequest")
	@ResponsePayload
	public DeleteBankResponse deleteBank(@RequestPayload DeleteBankRequest request) {
		DeleteBankResponse response = new DeleteBankResponse();
		response.setResponse(archiveRepository.deleteBank(request.getBankId(),request.getArchiveName()));

		return response;
	}
	
	@PayloadRoot(namespace = NAMESPACE_URI, localPart = "updateBankRequest")
	@ResponsePayload
	public UpdateBankResponse updateBank(@RequestPayload UpdateBankRequest request) {
		UpdateBankResponse response = new UpdateBankResponse();
		response.setResponse(archiveRepository.updateBank(request.getBank(),request.getArchiveName()));

		return response;
	}
	
	
	//methods for company
	@PayloadRoot(namespace = NAMESPACE_URI, localPart = "getCompanyRequest")
	@ResponsePayload
	public GetCompanyResponse getCompany(@RequestPayload GetCompanyRequest request) {
		GetCompanyResponse response = new GetCompanyResponse();
		response.setCompany(archiveRepository.findCompany(request.getCompanyId(),request.getArchiveName()));
		return response;
	}
	@PayloadRoot(namespace = NAMESPACE_URI, localPart = "getAllCompaniesRequest")
	@ResponsePayload
	public GetAllCompaniesResponse getAllCompanies(@RequestPayload GetAllCompaniesRequest request) {
		GetAllCompaniesResponse response = new GetAllCompaniesResponse();
		response.setAllCompaniesResponse(archiveRepository.findAllCompanies(request.getArchiveName()));

		return response;
	}
	@PayloadRoot(namespace = NAMESPACE_URI, localPart = "getAllCompaniesCustomRequest")
	@ResponsePayload
	public GetAllCompaniesCustomResponse getAllCompaniesCustom(@RequestPayload GetAllCompaniesCustomRequest request) {
		GetAllCompaniesCustomResponse response = new GetAllCompaniesCustomResponse();
		response.setAllCompaniesCustomResponse(archiveRepository.findAllCompaniesCustom(request.getArchiveName(),request.getQueryClient()));

		return response;
	}
	
	@PayloadRoot(namespace = NAMESPACE_URI, localPart = "getCompaniesLikeNameRequest")
	@ResponsePayload
	public GetCompaniesLikeNameResponse getCompaniesLikeName(@RequestPayload GetCompaniesLikeNameRequest request) {
		GetCompaniesLikeNameResponse response = new GetCompaniesLikeNameResponse();
		response.setAllCompaniesLikeNameResponse(archiveRepository.findCompaniesLikeName(request.getName(),request.getArchiveName()));

		return response;
	}
	
	@PayloadRoot(namespace = NAMESPACE_URI, localPart = "addCompanyRequest")
	@ResponsePayload
	public AddCompanyResponse addCompany(@RequestPayload AddCompanyRequest request) {
		AddCompanyResponse response = new AddCompanyResponse();
		response.setResponse(archiveRepository.addCompany(request.getCompany(),request.getArchiveName()));

		return response;
	}
	
	@PayloadRoot(namespace = NAMESPACE_URI, localPart = "deleteCompanyRequest")
	@ResponsePayload
	public DeleteCompanyResponse deleteCompany(@RequestPayload DeleteCompanyRequest request) {
		DeleteCompanyResponse response = new DeleteCompanyResponse();
		response.setResponse(archiveRepository.deleteCompany(request.getCompanyId(),request.getArchiveName()));

		return response;
	}
	
	@PayloadRoot(namespace = NAMESPACE_URI, localPart = "updateCompanyRequest")
	@ResponsePayload
	public UpdateCompanyResponse updateCompany(@RequestPayload UpdateCompanyRequest request) {
		UpdateCompanyResponse response = new UpdateCompanyResponse();
		response.setResponse(archiveRepository.updateCompany(request.getCompany(),request.getArchiveName()));

		return response;
	}
	
	//methods for contact
	@PayloadRoot(namespace = NAMESPACE_URI, localPart = "getContactRequest")
	@ResponsePayload
	public GetContactResponse getContact(@RequestPayload GetContactRequest request) {
		GetContactResponse response = new GetContactResponse();
		response.setContact(archiveRepository.findContact(request.getContactId(),request.getArchiveName()));
		return response;
	}
	@PayloadRoot(namespace = NAMESPACE_URI, localPart = "getAllContactsRequest")
	@ResponsePayload
	public GetAllContactsResponse getAllContacts(@RequestPayload GetAllContactsRequest request) {
		GetAllContactsResponse response = new GetAllContactsResponse();
		response.setAllContactsResponse(archiveRepository.findAllContacts(request.getArchiveName()));

		return response;
	}
	@PayloadRoot(namespace = NAMESPACE_URI, localPart = "getAllContactsCustomRequest")
	@ResponsePayload
	public GetAllContactsCustomResponse getAllContactsCustom(@RequestPayload GetAllContactsCustomRequest request) {
		GetAllContactsCustomResponse response = new GetAllContactsCustomResponse();
		response.setAllContactsCustomResponse(archiveRepository.findAllContactsCustom(request.getArchiveName(),request.getQueryClient()));

		return response;
	}
	
	@PayloadRoot(namespace = NAMESPACE_URI, localPart = "getContactsLikeNameRequest")
	@ResponsePayload
	public GetContactsLikeNameResponse getContactsLikeName(@RequestPayload GetContactsLikeNameRequest request) {
		GetContactsLikeNameResponse response = new GetContactsLikeNameResponse();
		response.setAllContactsLikeNameResponse(archiveRepository.findContactsLikeName(request.getFullName(),request.getArchiveName()));

		return response;
	}
	
	@PayloadRoot(namespace = NAMESPACE_URI, localPart = "addContactRequest")
	@ResponsePayload
	public AddContactResponse addContact(@RequestPayload AddContactRequest request) {
		AddContactResponse response = new AddContactResponse();
		response.setResponse(archiveRepository.addContact(request.getContact(),request.getArchiveName()));

		return response;
	}
	
	@PayloadRoot(namespace = NAMESPACE_URI, localPart = "deleteContactRequest")
	@ResponsePayload
	public DeleteContactResponse deleteContact(@RequestPayload DeleteContactRequest request) {
		DeleteContactResponse response = new DeleteContactResponse();
		response.setResponse(archiveRepository.deleteContact(request.getContactId(),request.getArchiveName()));

		return response;
	}
	
	@PayloadRoot(namespace = NAMESPACE_URI, localPart = "updateContactRequest")
	@ResponsePayload
	public UpdateContactResponse updateContact(@RequestPayload UpdateContactRequest request) {
		UpdateContactResponse response = new UpdateContactResponse();
		response.setResponse(archiveRepository.updateContact(request.getContact(),request.getArchiveName()));

		return response;
	}
	
	//methods for entries
	@PayloadRoot(namespace = NAMESPACE_URI, localPart = "getEntryRequest")
	@ResponsePayload
	public GetEntryResponse getEntry(@RequestPayload GetEntryRequest request) {
		GetEntryResponse response = new GetEntryResponse();
		response.setEntry(archiveRepository.findEntry(request.getEntryId(),request.getArchiveName()));
		return response;
	}
	@PayloadRoot(namespace = NAMESPACE_URI, localPart = "getAllEntriesRequest")
	@ResponsePayload
	public GetAllEntriesResponse getAllEntries(@RequestPayload GetAllEntriesRequest request) {
		GetAllEntriesResponse response = new GetAllEntriesResponse();
		response.setAllEntriesResponse(archiveRepository.findAllEntries(request.getArchiveName()));

		return response;
	}
	@PayloadRoot(namespace = NAMESPACE_URI, localPart = "getEntriesByOperationTypeRequest")
	@ResponsePayload
	public GetEntriesByOperationTypeResponse getEntriesByOperationType(@RequestPayload GetEntriesByOperationTypeRequest request) {
		GetEntriesByOperationTypeResponse response = new GetEntriesByOperationTypeResponse();
		response.setAllEntriesByOperationTypeResponse(archiveRepository.findEntriesByOperationType(request.getIdOperationType(),request.getArchiveName()));

		return response;
	}
	@PayloadRoot(namespace = NAMESPACE_URI, localPart = "getEntriesByBankRequest")
	@ResponsePayload
	public GetEntriesByBankResponse getEntriesByBank(@RequestPayload GetEntriesByBankRequest request) {
		GetEntriesByBankResponse response = new GetEntriesByBankResponse();
		
		response.setAllEntriesByBankResponse(archiveRepository.findEntriesByBank(request.getIdBank(),request.getArchiveName()));

		return response;
	}
	@PayloadRoot(namespace = NAMESPACE_URI, localPart = "getEntriesByDateRequest")
	@ResponsePayload
	public GetEntriesByDateResponse getEntriesByDate(@RequestPayload GetEntriesByDateRequest request) {
		GetEntriesByDateResponse response = new GetEntriesByDateResponse();
		response.setAllEntriesByDateResponse(archiveRepository.findEntriesByDate(request.getEntryDate(),request.getArchiveName()));

		return response;
	}
	@PayloadRoot(namespace = NAMESPACE_URI, localPart = "getEntriesBeforeDateRequest")
	@ResponsePayload
	public GetEntriesBeforeDateResponse getEntriesBeforeDate(@RequestPayload GetEntriesBeforeDateRequest request) {
		GetEntriesBeforeDateResponse response = new GetEntriesBeforeDateResponse();
		response.setAllEntriesBeforeDateResponse(archiveRepository.findEntriesBeforeDate(request.getEntryDate(),request.getArchiveName()));

		return response;
	}
	@PayloadRoot(namespace = NAMESPACE_URI, localPart = "getEntriesAfterDateRequest")
	@ResponsePayload
	public GetEntriesAfterDateResponse getEntriesAfterDate(@RequestPayload GetEntriesAfterDateRequest request) {
		GetEntriesAfterDateResponse response = new GetEntriesAfterDateResponse();
		response.setAllEntriesAfterDateResponse(archiveRepository.findEntriesAfterDate(request.getEntryDate(),request.getArchiveName()));

		return response;
	}
	
	@PayloadRoot(namespace = NAMESPACE_URI, localPart = "getEntriesBetweenDatesRequest")
	@ResponsePayload
	public GetEntriesBetweenDatesResponse getEntriesBetweenDates(@RequestPayload GetEntriesBetweenDatesRequest request) {
		GetEntriesBetweenDatesResponse response = new GetEntriesBetweenDatesResponse();
		response.setAllEntriesBetweenDatesResponse(archiveRepository.findEntriesBetweenDates(request.getEntryStartDate(), request.getEntryEndDate(),request.getArchiveName()));

		return response;
	}
	
	@PayloadRoot(namespace = NAMESPACE_URI, localPart = "getAllEntriesCustomRequest")
	@ResponsePayload
	public GetAllEntriesCustomResponse getAllEntriesCustom(@RequestPayload GetAllEntriesCustomRequest request) {
		GetAllEntriesCustomResponse response = new GetAllEntriesCustomResponse();
		response.setAllEntriesCustomResponse(archiveRepository.findAllEntriesCustom(request.getArchiveName(),request.getQueryClient()));

		return response;
	}
	@PayloadRoot(namespace = NAMESPACE_URI, localPart = "getAllEntriesPreviousPeriodRequest")
	@ResponsePayload
	public GetAllEntriesPreviousPeriodResponse getAllEntriesPreviousPeriod(@RequestPayload GetAllEntriesPreviousPeriodRequest request) {
		GetAllEntriesPreviousPeriodResponse response = new GetAllEntriesPreviousPeriodResponse();//get query Client will be only the end date in string, we're not changing it because we need to update all arraylist methods
		response.setEntryPreviousPeriod(archiveRepository.findTotalsForEntriesPreviousPeriod(request.getQueryClient(), request.getArchiveName()));
		return response;
	}
	
	
	@PayloadRoot(namespace = NAMESPACE_URI, localPart = "addEntryRequest")
	@ResponsePayload
	public AddEntryResponse addEntry(@RequestPayload AddEntryRequest request) {
		AddEntryResponse response = new AddEntryResponse();
		response.setResponse(archiveRepository.addEntry(request.getEntry(),request.getArchiveName()));

		return response;
	}
	
	@PayloadRoot(namespace = NAMESPACE_URI, localPart = "deleteEntryRequest")
	@ResponsePayload
	public DeleteEntryResponse deleteEntry(@RequestPayload DeleteEntryRequest request) {
		DeleteEntryResponse response = new DeleteEntryResponse();
		response.setResponse(archiveRepository.deleteEntry(request.getEntryId(),request.getArchiveName()));

		return response;
	}
	
	@PayloadRoot(namespace = NAMESPACE_URI, localPart = "updateEntryRequest")
	@ResponsePayload
	public UpdateEntryResponse updateEntry(@RequestPayload UpdateEntryRequest request) {
		UpdateEntryResponse response = new UpdateEntryResponse();
		response.setResponse(archiveRepository.updateEntry(request.getEntry(),request.getArchiveName()));

		return response;
	}
	
	//methods for operation
	@PayloadRoot(namespace = NAMESPACE_URI, localPart = "getOperationTypeRequest")
	@ResponsePayload
	public GetOperationTypeResponse getOperationType(@RequestPayload GetOperationTypeRequest request) {
		GetOperationTypeResponse response = new GetOperationTypeResponse();
		response.setOperationType(archiveRepository.findOperationType(request.getOperationTypeId(),request.getArchiveName()));
		return response;
	}
	@PayloadRoot(namespace = NAMESPACE_URI, localPart = "getAllOperationTypesRequest")
	@ResponsePayload
	public GetAllOperationTypesResponse getAllOperationTypes(@RequestPayload GetAllOperationTypesRequest request) {
		GetAllOperationTypesResponse response = new GetAllOperationTypesResponse();
		response.setAllOperationTypesResponse(archiveRepository.findAllOperationTypes(request.getArchiveName()));

		return response;
	}
	
	@PayloadRoot(namespace = NAMESPACE_URI, localPart = "getAllOperationTypesCustomRequest")
	@ResponsePayload
	public GetAllOperationTypesCustomResponse getAllOperationTypesCustom(@RequestPayload GetAllOperationTypesCustomRequest request) {
		GetAllOperationTypesCustomResponse response = new GetAllOperationTypesCustomResponse();
		response.setAllOperationTypesCustomResponse(archiveRepository.findAllOperationTypesCustom(request.getArchiveName(),request.getQueryClient()));

		return response;
	}
	
	@PayloadRoot(namespace = NAMESPACE_URI, localPart = "addOperationTypeRequest")
	@ResponsePayload
	public AddOperationTypeResponse addOperationType(@RequestPayload AddOperationTypeRequest request) {
		AddOperationTypeResponse response = new AddOperationTypeResponse();
		response.setResponse(archiveRepository.addOperationType(request.getOperationType(),request.getArchiveName()));

		return response;
	}
	
	@PayloadRoot(namespace = NAMESPACE_URI, localPart = "deleteOperationTypeRequest")
	@ResponsePayload
	public DeleteOperationTypeResponse deleteOperationType(@RequestPayload DeleteOperationTypeRequest request) {
		DeleteOperationTypeResponse response = new DeleteOperationTypeResponse();
		response.setResponse(archiveRepository.deleteOperationType(request.getOperationTypeId(),request.getArchiveName()));

		return response;
	}
	
	@PayloadRoot(namespace = NAMESPACE_URI, localPart = "updateOperationTypeRequest")
	@ResponsePayload
	public UpdateOperationTypeResponse updateOperationType(@RequestPayload UpdateOperationTypeRequest request) {
		UpdateOperationTypeResponse response = new UpdateOperationTypeResponse();
		response.setResponse(archiveRepository.updateOperationType(request.getOperationType(),request.getArchiveName()));

		return response;
	}
	
	//methods for reminder
	@PayloadRoot(namespace = NAMESPACE_URI, localPart = "getReminderRequest")
	@ResponsePayload
	public GetReminderResponse getReminder(@RequestPayload GetReminderRequest request) {
		GetReminderResponse response = new GetReminderResponse();
		response.setReminder(archiveRepository.findReminder(request.getReminderId(),request.getArchiveName()));
		return response;
	}
	@PayloadRoot(namespace = NAMESPACE_URI, localPart = "getAllRemindersRequest")
	@ResponsePayload
	public GetAllRemindersResponse getAllReminders(@RequestPayload GetAllRemindersRequest request) {
		GetAllRemindersResponse response = new GetAllRemindersResponse();
		response.setAllRemindersResponse(archiveRepository.findAllReminders(request.getArchiveName()));

		return response;
	}
	
	@PayloadRoot(namespace = NAMESPACE_URI, localPart = "getAllRemindersByContactRequest")
	@ResponsePayload
	public GetAllRemindersByContactResponse getAllRemindersByContact(@RequestPayload GetAllRemindersByContactRequest request) {
		GetAllRemindersByContactResponse response = new GetAllRemindersByContactResponse();
		response.setAllRemindersByContactResponse(archiveRepository.findAllRemindersByContact(request.getArchiveName(),request.getIdContact()));

		return response;
	}
	
	@PayloadRoot(namespace = NAMESPACE_URI, localPart = "getAllRemindersLikeDescriptionRequest")
	@ResponsePayload
	public GetAllRemindersLikeDescriptionResponse getAllRemindersLikeDescription(@RequestPayload GetAllRemindersLikeDescriptionRequest request) {
		GetAllRemindersLikeDescriptionResponse response = new GetAllRemindersLikeDescriptionResponse();
		response.setAllRemindersLikeDescriptionResponse(archiveRepository.findAllRemindersLikeDescription(request.getArchiveName(),request.getDescription()));

		return response;
	}
	
	@PayloadRoot(namespace = NAMESPACE_URI, localPart = "getAllRemindersBeforeDateRequest")
	@ResponsePayload
	public GetAllRemindersBeforeDateResponse getAllRemindersBeforeDate(@RequestPayload GetAllRemindersBeforeDateRequest request) {
		GetAllRemindersBeforeDateResponse response = new GetAllRemindersBeforeDateResponse();
		response.setAllRemindersBeforeDateResponse(archiveRepository.findAllRemindersBeforeDate(request.getArchiveName(),request.getDate()));

		return response;
	}
	
	@PayloadRoot(namespace = NAMESPACE_URI, localPart = "getAllRemindersAfterDateRequest")
	@ResponsePayload
	public GetAllRemindersAfterDateResponse getAllRemindersAfterDate(@RequestPayload GetAllRemindersAfterDateRequest request) {
		GetAllRemindersAfterDateResponse response = new GetAllRemindersAfterDateResponse();
		response.setAllRemindersAfterDateResponse(archiveRepository.findAllRemindersAfterDate(request.getArchiveName(),request.getDate()));

		return response;
	}
	
	@PayloadRoot(namespace = NAMESPACE_URI, localPart = "getAllRemindersBetweenDatesRequest")
	@ResponsePayload
	public GetAllRemindersBetweenDatesResponse getAllRemindersBetweenDates(@RequestPayload GetAllRemindersBetweenDatesRequest request) {
		GetAllRemindersBetweenDatesResponse response = new GetAllRemindersBetweenDatesResponse();
		response.setAllRemindersBetweenDatesResponse(archiveRepository.findAllRemindersBetweenDates(request.getArchiveName(), request.getStartDate(), request.getEndDate()));

		return response;
	}
	
	@PayloadRoot(namespace = NAMESPACE_URI, localPart = "getAllRemindersCustomRequest")
	@ResponsePayload
	public GetAllRemindersCustomResponse getAllRemindersCustom(@RequestPayload GetAllRemindersCustomRequest request) {
		GetAllRemindersCustomResponse response = new GetAllRemindersCustomResponse();
		response.setAllRemindersCustomResponse(archiveRepository.findAllRemindersCustom(request.getArchiveName(),request.getQuery()));

		return response;
	}
	
	
	@PayloadRoot(namespace = NAMESPACE_URI, localPart = "addReminderRequest")
	@ResponsePayload
	public AddReminderResponse addReminder(@RequestPayload AddReminderRequest request) {
		AddReminderResponse response = new AddReminderResponse();
		response.setResponse(archiveRepository.addReminder(request.getReminder(),request.getArchiveName()));

		return response;
	}
	
	@PayloadRoot(namespace = NAMESPACE_URI, localPart = "deleteReminderRequest")
	@ResponsePayload
	public DeleteReminderResponse deleteReminder(@RequestPayload DeleteReminderRequest request) {
		DeleteReminderResponse response = new DeleteReminderResponse();
		response.setResponse(archiveRepository.deleteReminder(request.getReminderId(),request.getArchiveName()));

		return response;
	}
	
	@PayloadRoot(namespace = NAMESPACE_URI, localPart = "updateReminderRequest")
	@ResponsePayload
	public UpdateReminderResponse updateReminder(@RequestPayload UpdateReminderRequest request) {
		UpdateReminderResponse response = new UpdateReminderResponse();
		response.setResponse(archiveRepository.updateReminder(request.getReminder(),request.getArchiveName()));

		return response;
	}
	
	//methods for sqlite_sequence
	//update: this methods are not necessary. SQLite sequence is an automatic created table from SQLite Engine to
	//keep track of autoincrement fields in different tables. Hence we won't use these methods. 
	/*
	@PayloadRoot(namespace = NAMESPACE_URI, localPart = "getSqliteSequenceRequest")
	@ResponsePayload
	public GetSqliteSequenceResponse getSqliteSequence(@RequestPayload GetSqliteSequenceRequest request) {
		GetSqliteSequenceResponse response = new GetSqliteSequenceResponse();
		response.setSqliteSequence(archiveRepository.findSqliteSequence(request.getName(),request.getArchiveName()));
		return response;
	}
	@PayloadRoot(namespace = NAMESPACE_URI, localPart = "getAllSqliteSequenciesRequest")
	@ResponsePayload
	public GetAllSqliteSequenciesResponse getAllSqliteSequencies(@RequestPayload GetAllSqliteSequenciesRequest request) {
		GetAllSqliteSequenciesResponse response = new GetAllSqliteSequenciesResponse();
		response.setAllSqliteSequenciesResponse(archiveRepository.findAllSqliteSequencies(request.getArchiveName()));

		return response;
	} 
	
	@PayloadRoot(namespace = NAMESPACE_URI, localPart = "addSqliteSequenceRequest")
	@ResponsePayload
	public AddSqliteSequenceResponse addSqliteSequence(@RequestPayload AddSqliteSequenceRequest request) {
		AddSqliteSequenceResponse response = new AddSqliteSequenceResponse();
		response.setResponse(archiveRepository.addSqliteSequence(request.getSqliteSequence(),request.getArchiveName()));

		return response;
	}
	
	@PayloadRoot(namespace = NAMESPACE_URI, localPart = "deleteSqliteSequenceRequest")
	@ResponsePayload
	public DeleteSqliteSequenceResponse deleteSqliteSequence(@RequestPayload DeleteSqliteSequenceRequest request) {
		DeleteSqliteSequenceResponse response = new DeleteSqliteSequenceResponse();
		response.setResponse(archiveRepository.deleteSqliteSequence(request.getSqliteSequenceId(),request.getArchiveName()));

		return response;
	}
	
	@PayloadRoot(namespace = NAMESPACE_URI, localPart = "updateSqliteSequenceRequest")
	@ResponsePayload
	public UpdateSqliteSequenceResponse updateSqliteSequence(@RequestPayload UpdateSqliteSequenceRequest request) {
		UpdateSqliteSequenceResponse response = new UpdateSqliteSequenceResponse();
		response.setResponse(archiveRepository.updateSqliteSequence(request.getSqliteSequence(),request.getArchiveName()));

		return response;
	}
	*/
}

