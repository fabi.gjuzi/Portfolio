package primaNotaFacile;

import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import net.unnitech.primanotafacile.*;

import org.springframework.stereotype.Component;
import org.springframework.util.Assert;
import net.unnitech.database.OjdbcConnection;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;

@Component
public class ArchiveRepository {
	
	public Archive findArchive(String archive_name) {
		Assert.notNull(archive_name);
		Archive result = null;
		//kerkimi i arkives do te beje search per file <archive_name> ne direktorine ku ekzekutohet aplikacioni.
		  File f = new File(System.getProperty("user.dir")+"\\"+archive_name);
		  if(f.exists() && !f.isDirectory()) { 
		      // do something
			  //provojme lidhjen me database-in
			  Connection database_connection = null;
				try
				{
					database_connection = new OjdbcConnection().connectToSqlite(archive_name);
				}
				catch(SQLException exception)
				{
					System.out.println("Failed to connect to the database");
					return result;
				}
			  Archive archive = new Archive();
			  archive.setArchiveName(archive_name);
			  archive.setPassword(null);
			  result = archive;
			  return result;
		  }
		  else {
			  System.out.println("I'm looking at : "+System.getProperty("user.dir"));
			  System.out.println("Can't find archive: "+archive_name);
			  return result;
		  }
		
		
	}
	
	public List<Archive> findAllArchives() {
		//kerkimi i te gjithe arkivave  do te beje search per all files *.sqlite3 ne direktorine ku ekzekutohet aplikacioni.
		
		 System.out.println("I'm looking at : "+System.getProperty("user.dir"));
				File dir = new File(System.getProperty("user.dir"));
				File [] files = dir.listFiles(new FilenameFilter() {
					@Override
					public boolean accept(File dir, String name) {
						return name.endsWith(".sqlite3");
					}
		});
				List<Archive> allArchives = new ArrayList<Archive>();
				
		for (File file : files) {
			 
			if(file.exists() && !file.isDirectory()) { 
			      // do something
				  //provojme lidhjen me database-in
				String emriFile = FilenameUtils.removeExtension(file.getName());
				System.out.println("Emri i file-it: "+emriFile);  
				Connection database_connection = null;
					try
					{
						database_connection = new OjdbcConnection().connectToSqlite(file.getName());
						
					}
					catch(SQLException exception)
					{
						System.out.println("Failed to connect to the database");
						//return result;
					}
				
				Archive archive = new Archive();
				  archive.setArchiveName(FilenameUtils.removeExtension(file.getName()));
				  archive.setPassword("");
				  allArchives.add(archive);
				 
			  }
			  else {
				  System.out.println("Can't find archive: "+file);
				  //return result;
			  }
		}
		
		return allArchives;
			
	}
	
	public String addArchive(Archive archive){
		String result = "ERROR";
		//Shtimi i arkives do te beje shtimin e nje skeme te re me emrin qe vjen si parameter
		//sipas http://www.sqlitetutorial.net/sqlite-java/create-database/
		//sqlite krijon automatikisht Db if nuk gjen db me emrin e dhene
		//do te kontrollojme qe emri i kerkuar per database-in te mos jete i perdorur
		//kerkimi i arkives do te beje search per file <archive_name> ne direktorine ku ekzekutohet aplikacioni.
		  File f = new File(System.getProperty("user.dir")+"/"+archive.getArchiveName());
		  if(f.exists() && !f.isDirectory()) { 
			  System.out.println("Archive :"+archive.getArchiveName()+" already exists");
			  return result;
		  }
		  else {
			  //nuk ekziston jemi ok
				Connection database_connection = null;
				try
				{
					database_connection = new OjdbcConnection().connectToSqlite(archive.getArchiveName());
					
					//me krijimin e db duhet te ekzekutojme skriptin per krijimin e tabelave
					
				    Statement stmt = null;
				    try {
				        	stmt = database_connection.createStatement();
				        	String sql =  "CREATE TABLE entry "
				        			+ "(entry_id INTEGER PRIMARY KEY AUTOINCREMENT, "
				        			+ "entry_date DATETIME, document_number DATETIME, "
				        			+ "document_date DATETIME, id_operation_type INTEGER REFERENCES operation_type (operation_type_id), "
				        			+ "description CHAR (100), incoming_value_bank REAL DEFAULT (0),"
				        			+ " outgoing_value_bank REAL DEFAULT (0) , id_bank INTEGER REFERENCES bank (bank_id),"
				        			+ " id_contact INTEGER,out_cash REAL DEFAULT (0),"
				        			+ "incoming_value_cash REAL  DEFAULT (0),"
				        			+ "outgoing_value_cash REAL  DEFAULT (0));  "
				        			+ " CREATE TABLE operation_type (name CHAR (50) NOT NULL, "
				        			+ "incoming BOOLEAN, outgoing BOOLEAN, operation_type_id INTEGER PRIMARY KEY AUTOINCREMENT);  "
				        			+ " CREATE TABLE contact (contact_id INTEGER PRIMARY KEY AUTOINCREMENT,"
				        			+ " full_name CHAR (100) NOT NULL, address CHAR (100), city CHAR (30), "
				        			+ " zip_code CHAR (10), state CHAR (30), phone_home CHAR (30), "
				        			+ " phone_work CHAR (30), mobile_number CHAR (30), fax CHAR (30), "
				        			+ " email1 CHAR (50), email2 CHAR (50), instant_message CHAR (50), "
				        			+ "web CHAR (100), note TEXT, contact_type CHAR (20) );    "
				        			+ "  CREATE TABLE company (company_id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,"
				        			+ " name CHAR (50) NOT NULL, tax_code CHAR (25) NOT NULL, vat_number CHAR (20) NOT NULL, "
				        			+ "adress CHAR (100), city CHAR (30), zip_code CHAR (10), country CHAR (30), "
				        			+ "phone_number CHAR (20), fax CHAR (30), e_mail CHAR (50), web CHAR (100), default_currency CHAR (10)); "
				        			+ "  CREATE TABLE bank (bank_id	INTEGER PRIMARY KEY AUTOINCREMENT,"
				        			+ " name	CHAR(30) NOT NULL,	description	CHAR(200), "
				        			+ "	disabled	BOOLEAN NOT NULL DEFAULT (0));  "
				        			+ "CREATE TABLE reminder (reminder_id INTEGER PRIMARY KEY  AUTOINCREMENT  NOT NULL  UNIQUE "
				        			+ ", date VARCHAR, description TEXT, id_contact INTEGER, value REAL NOT NULL  DEFAULT 0"
				        			+ ", completed BOOL NOT NULL  DEFAULT 0)";

				        			
				        	stmt.executeUpdate(sql);
				        	stmt.close();
				        	database_connection.close();
				        	System.out.println("Archive: "+archive.getArchiveName()+" and it's tables were created successfully!");	
				        	result = "OK";
							return result;
				    } catch ( Exception e ) {
				      System.out.println("Failed to execute query, creating tables "+ e.getMessage() );
				      return result;
				    }
				    					
				}
				catch(SQLException exception)
				{
					System.out.println("Failed to connect to the database");
					return result;
				}
		  }		
	}
	
	public String deleteArchive(String archive_name){
		//delete archive praktikisht do te beje fshirjen e file-it <archive_name> nese ekziston
		String result = "ERROR";
		Assert.notNull(archive_name);
		  System.out.println("We're searching in directory: "+System.getProperty("user.dir"));
		  
		  File f = new File(System.getProperty("user.dir")+"/"+archive_name);
		  System.out.println("The value of file f : "+f.getAbsolutePath());
		  if(f.exists() && !f.isDirectory()) { 
		      if (f.delete()==true){
		    	  System.out.println("Archive "+archive_name+" deleted successfully");
		    	  result = "OK";
		    	  
		      }else{
		    	  System.out.println("Can't delete archive: "+archive_name);
		    	  result = "ERROR";
		      }			
		      
		      return result;
		  }
		  else {
			  System.out.println("Can't find archive: "+archive_name);
			  return result;
		  }
		
	}
	
	public String updateArchive(String oldArchive, String newArchive){
		//kjo metode do te beje rename te file-it sqlite nga emri i vjeter ne emrin e ri 
		String result = "ERROR";
		// File (or directory) with old name
		File file = new File(System.getProperty("user.dir")+"/"+oldArchive+".sqlite3");

		// File (or directory) with new name
		File file2 = new File(System.getProperty("user.dir")+"/"+newArchive+".sqlite3");

		if (file2.exists()){
			System.out.println("Can't rename with an existing filename!");
			result = "ERROR";
			return result;
		}
		   
		// Rename file (or directory)
		boolean success = file.renameTo(file2);

		if (success) {
			System.out.println("Renaming was successful!");
			result = "OK";
			return result;
		}else{
			System.out.println("Error renaming database from: "+oldArchive + " to "+newArchive+" !");
			result = "ERROR";
			return result;
		}

		
	}
	
	public String copyArchive(String archiveName){
		//copy archive will search for the existing archive from the name parameter.
		//Then will copy this file by adding a _copy in the end of the file name
		String result = "ERROR";
		//check that file exists
		File f = new File(System.getProperty("user.dir")+"/"+archiveName+".sqlite3");
		  if(f.exists() && !f.isDirectory()) { 
			  //file exists.
			  //create the copy file
			  File copyFile = new File(System.getProperty("user.dir")+"/"+archiveName+"_copy.sqlite3");
			  //copy file with apache commons library
			  try {
				FileUtils.copyFile(f, copyFile);
				System.out.println(f.getName() +" was copied succesfully to "+copyFile.getName());
				result = "OK";
				
			} catch (IOException e) {
				System.out.println("Error copying "+f.getName()+" to "+copyFile.getName()+". Details: "+e.getMessage());
			}
		  }else{
			  System.out.println("Archive :"+archiveName+".sqlite3 doesn't exist");
			  			  
		  }	
		return result;		
	}
	
	public Bank findBank(String bank_id, String archive_name){
		Assert.notNull(bank_id);
		Bank result = null;
		Connection database_connection = null;
				
		try
		{
			database_connection = new OjdbcConnection().connectToSqlite(archive_name);
		}
		catch(SQLException exception)
		{
			System.out.println("Failed to connect to the database"+exception.getMessage());
			return result;
		}
		
		String query = "select * from bank where bank_id = '" + bank_id + "'";
		Statement statement = null;
		try
		{
			statement = database_connection.createStatement();
		}
		catch(SQLException exception)
		{
			System.out.println("Failed to create statement"+exception.getMessage());
			return result;
		}
		
		try
		{
			ResultSet resultSet = statement.executeQuery(query);

			if(resultSet.next())
			{
				Bank pergjigje = new Bank();
				pergjigje.setBankId(Integer.toString(resultSet.getInt("bank_id")));
				pergjigje.setDescription(resultSet.getString("description"));
				pergjigje.setDisabled(Boolean.toString(resultSet.getBoolean("disabled")));
				//pergjigje.setIdArchive(Integer.toString(resultSet.getInt("id_archive")));
				pergjigje.setName(resultSet.getString("name"));
				result = pergjigje;
			}
			else
			{
				System.out.println("No information found in database");
				return result;
			}

			statement.close();
			database_connection.close();
			System.out.println("Successfuly");
			return result;
		}
		catch(SQLException exception)
		{
			System.out.println("Failed to excecute statement"+exception.getMessage());
			return result;
		}
    }
	
	public List<Bank> findAllBanks(String archive_name) {
		//Assert.notNull(name);
		List<Bank> allBanks = new ArrayList<Bank>();
		Connection database_connection = null;
				
		try
		{
			database_connection = new OjdbcConnection().connectToSqlite(archive_name);
		}
		catch(SQLException exception)
		{
			System.out.println("Failed to connect to the database"+exception.getMessage());
			allBanks = null;
			return allBanks;
		}
		
		String query = "select * from bank";
		Statement statement = null;
		try
		{
			statement = database_connection.createStatement();
		}
		catch(SQLException exception)
		{
			System.out.println("Failed to create statement"+exception.getMessage());
			allBanks = null;
			return allBanks;
		}
		
		try
		{
			ResultSet resultSet = statement.executeQuery(query);
			boolean hasNext = false;
			while(resultSet.next())
			{
				hasNext = true;
				Bank pergjigje = new Bank();
				pergjigje.setBankId(Integer.toString(resultSet.getInt("bank_id")));
				pergjigje.setDescription(resultSet.getString("description"));
				//pergjigje.setDisabled(Boolean.toString(resultSet.getBoolean("disabled")));
				//pergjigje.setIdArchive(Integer.toString(resultSet.getInt("id_archive")));
				pergjigje.setName(resultSet.getString("name"));
				allBanks.add(pergjigje);
			}
			
			
			if(hasNext == false)
			{
				System.out.println("No information found in database");
				allBanks = null;
				return allBanks;
			}			

			statement.close();
			database_connection.close();

			return allBanks;
		}
		catch(SQLException exception)
		{
			System.out.println("Failed to excecute statement"+exception.getMessage());
			allBanks = null;
			return allBanks;
		}
		
	}
	
	public List<Bank> findAllBanksCustom(String archive_name, String queryClient) {
		//Assert.notNull(name);
		List<Bank> allBanks = new ArrayList<Bank>();
		Connection database_connection = null;
				
		try
		{
			database_connection = new OjdbcConnection().connectToSqlite(archive_name);
		}
		catch(SQLException exception)
		{
			System.out.println("Failed to connect to the database"+exception.getMessage());
			allBanks = null;
			return allBanks;
		}
		
		String query = queryClient;
		Statement statement = null;
		try
		{
			statement = database_connection.createStatement();
		}
		catch(SQLException exception)
		{
			System.out.println("Failed to create statement"+exception.getMessage());
			allBanks = null;
			return allBanks;
		}
		
		try
		{
			ResultSet resultSet = statement.executeQuery(query);
			boolean hasNext = false;
			while(resultSet.next())
			{
				hasNext = true;
				Bank pergjigje = new Bank();
				pergjigje.setBankId(Integer.toString(resultSet.getInt("bank_id")));
				pergjigje.setDescription(resultSet.getString("description"));
				//pergjigje.setDisabled(Boolean.toString(resultSet.getBoolean("disabled")));
				//pergjigje.setIdArchive(Integer.toString(resultSet.getInt("id_archive")));
				pergjigje.setName(resultSet.getString("name"));
				allBanks.add(pergjigje);
			}
			
			
			if(hasNext == false)
			{
				System.out.println("No information found in database");
				allBanks = null;
				return allBanks;
			}			

			statement.close();
			database_connection.close();

			return allBanks;
		}
		catch(SQLException exception)
		{
			System.out.println("Failed to excecute statement"+exception.getMessage());
			allBanks = null;
			return allBanks;
		}
		
	}
	
	public List<Bank> findBanksLikeName(String name,String archive_name) {
		Assert.notNull(name);
		List<Bank> allBanks = new ArrayList<Bank>();
		Connection database_connection = null;
				
		try
		{
			database_connection = new OjdbcConnection().connectToSqlite(archive_name);
		}
		catch(SQLException exception)
		{
			System.out.println("Failed to connect to the database"+exception.getMessage());
			allBanks = null;
			return allBanks;
		}
		
		String query = "select * from bank where name like '%"+name+"%'";
		Statement statement = null;
		try
		{
			statement = database_connection.createStatement();
		}
		catch(SQLException exception)
		{
			System.out.println("Failed to create statement"+exception.getMessage());
			allBanks = null;
			return allBanks;
		}
		
		try
		{
			ResultSet resultSet = statement.executeQuery(query);
			boolean hasNext = false;
			while(resultSet.next())
			{
				hasNext = true;
				Bank pergjigje = new Bank();
				pergjigje.setBankId(Integer.toString(resultSet.getInt("bank_id")));
				pergjigje.setDescription(resultSet.getString("description"));
				//pergjigje.setDisabled(Boolean.toString(resultSet.getBoolean("disabled")));
				//pergjigje.setIdArchive(Integer.toString(resultSet.getInt("id_archive")));
				pergjigje.setName(resultSet.getString("name"));
				allBanks.add(pergjigje);
			}
			
			
			if(hasNext == false)
			{
				System.out.println("No information found in database");
				allBanks = null;
				return allBanks;
			}			

			statement.close();
			database_connection.close();

			return allBanks;
		}
		catch(SQLException exception)
		{
			System.out.println("Failed to excecute statement"+exception.getMessage());
			allBanks = null;
			return allBanks;
		}
		
	}
	
	public List<Bank> findBanksByArchive(String archive_name) {
		Assert.notNull(archive_name);
		List<Bank> allBanks = new ArrayList<Bank>();
		Connection database_connection = null;
				
		try
		{
			database_connection = new OjdbcConnection().connectToSqlite(archive_name);
		}
		catch(SQLException exception)
		{
			System.out.println("Failed to connect to the database"+exception.getMessage());
			allBanks = null;
			return allBanks;
		}
		
		String query = "select * from bank where id_archive = '"+archive_name+"'";
		Statement statement = null;
		try
		{
			statement = database_connection.createStatement();
		}
		catch(SQLException exception)
		{
			System.out.println("Failed to create statement"+exception.getMessage());
			allBanks = null;
			return allBanks;
		}
		
		try
		{
			ResultSet resultSet = statement.executeQuery(query);
			boolean hasNext = false;
			while(resultSet.next())
			{
				hasNext = true;
				Bank pergjigje = new Bank();
				pergjigje.setBankId(Integer.toString(resultSet.getInt("bank_id")));
				pergjigje.setDescription(resultSet.getString("description"));
				pergjigje.setDisabled(Boolean.toString(resultSet.getBoolean("disabled")));
				pergjigje.setIdArchive(Integer.toString(resultSet.getInt("id_archive")));
				pergjigje.setName(resultSet.getString("name"));
				allBanks.add(pergjigje);
			}
			
			
			if(hasNext == false)
			{
				System.out.println("No information found in database");
				allBanks = null;
				return allBanks;
			}			

			statement.close();
			database_connection.close();

			return allBanks;
		}
		catch(SQLException exception)
		{
			System.out.println("Failed to excecute statement"+exception.getMessage());
			allBanks = null;
			return allBanks;
		}
		
	}
	
	public String addBank(Bank bank,String archive_name){
		String result = "ERROR";
		//kontrollojme vlerat
		if (bank.getName() != ""){
			
				
				Connection database_connection = null;
				
				try
				{
					database_connection = new OjdbcConnection().connectToSqlite(archive_name);
				}
				catch(SQLException exception)
				{
					System.out.println("Failed to connect to the database"+exception.getMessage());
					return result;
				}
				
				try{
					String sql = "INSERT INTO bank (name,description)  VALUES(?,?)";
					PreparedStatement pstmt = database_connection.prepareStatement(sql);
					pstmt.setString(1,bank.getName());
					pstmt.setString(2,bank.getDescription());
					//pstmt.setString(3,bank.getDisabled());
//					pstmt.setString(4,bank.getIdArchive());
					pstmt.executeUpdate();					  
				}catch(SQLException exception)
				{
					System.out.println("Failed to execute query. Exception: "+exception.getMessage());
					return result;
				}
				result = "OK";
				return result;
			
			
		}else{
			return result;
		}
		
	}
	
	public String deleteBank(String bank_id,String archive_name){
		String result = "ERROR";
		//kontrollojme vlerat
		if(findBank(bank_id,archive_name) != null){
			Connection database_connection = null;
			try
			{
				database_connection = new OjdbcConnection().connectToSqlite(archive_name);
			}
			catch(SQLException exception)
			{
				System.out.println("Failed to connect to the database"+exception.getMessage());
				return result;
			}
			try{
				String sql = "DELETE FROM bank where bank_id = ?";
				PreparedStatement pstmt = database_connection.prepareStatement(sql);						
				pstmt.setString(1,bank_id);					        
				pstmt.executeUpdate();
			}catch(SQLException exception)
			{
				System.out.println("Failed to execute query. Exception: "+exception.getMessage());
				return result;
			}
			result = "OK";
			return result;
		}else{
			//nuk gjendet archive_id ne db
			return result;
		}
		
	}
	
	public String updateBank(Bank bank,String archive_name){
		String result = "ERROR";
		//kontrollojme id
		if (findBank(bank.getBankId(),archive_name) != null){
				
					Connection database_connection = null;
					try
					{
						database_connection = new OjdbcConnection().connectToSqlite(archive_name);
					}
					catch(SQLException exception)
					{
						System.out.println("Failed to connect to the database"+exception.getMessage());
						return result;
					}
					try{
						String sql = " UPDATE bank SET name = ?, description = ?  "
								+ "WHERE bank_id = ? ";
						PreparedStatement pstmt = database_connection.prepareStatement(sql);						
						pstmt.setString(1,bank.getName());					        
						pstmt.setString(2,bank.getDescription());
						//pstmt.setString(3,bank.getDisabled());
						//pstmt.setString(4,bank.getIdArchive());
						pstmt.setString(3,bank.getBankId());
						pstmt.executeUpdate();
					}catch(SQLException exception)
					{
						System.out.println("Failed to execute query. Exception: "+exception.getMessage());
						return result;
					}
					result = "OK";
					return result;
				
		}else{
			return result;
		}
		
	}
	
	public Company findCompany(String company_id,String archive_name){
		Assert.notNull(company_id);
		Company result = null;
		Connection database_connection = null;
				
		try
		{
			database_connection = new OjdbcConnection().connectToSqlite(archive_name);
		}
		catch(SQLException exception)
		{
			System.out.println("Failed to connect to the database"+exception.getMessage());
			return result;
		}
		
		String query = "select * from company where company_id = '" + company_id + "'";
		Statement statement = null;
		try
		{
			statement = database_connection.createStatement();
		}
		catch(SQLException exception)
		{
			System.out.println("Failed to create statement"+exception.getMessage());
			return result;
		}
		
		try
		{
			ResultSet resultSet = statement.executeQuery(query);

			if(resultSet.next())
			{
				Company pergjigje = new Company();
				pergjigje.setAddress(resultSet.getString("adress"));
				pergjigje.setCity(resultSet.getString("city"));
				pergjigje.setCompanyId(Integer.toString(resultSet.getInt("company_id")));
				pergjigje.setCountry(resultSet.getString("country"));
				pergjigje.setDefaultCurrency(resultSet.getString("default_currency"));
				pergjigje.setEMail(resultSet.getString("e_mail"));
				pergjigje.setFax(resultSet.getString("fax"));
				pergjigje.setName(resultSet.getString("name"));
				pergjigje.setPhoneNumber(resultSet.getString("phone_number"));
				pergjigje.setTaxCode(resultSet.getString("tax_code"));
				pergjigje.setVatNumber(resultSet.getString("vat_number"));
				pergjigje.setWeb(resultSet.getString("web"));
				pergjigje.setZipCode(resultSet.getString("zip_code"));
				result = pergjigje;
			}
			else
			{
				System.out.println("No information found in database");
				return result;
			}

			statement.close();
			database_connection.close();
			System.out.println("Successfuly");
			return result;
		}
		catch(SQLException exception)
		{
			System.out.println("Failed to excecute statement"+exception.getMessage());
			return result;
		}
    }
	
	public List<Company> findAllCompanies(String archive_name) {
		//Assert.notNull(name);
		List<Company> allCompanies = new ArrayList<Company>();
		Connection database_connection = null;
				
		try
		{
			database_connection = new OjdbcConnection().connectToSqlite(archive_name);
		}
		catch(SQLException exception)
		{
			System.out.println("Failed to connect to the database"+exception.getMessage());
			allCompanies = null;
			return allCompanies;
		}
		
		String query = "select * from company";
		Statement statement = null;
		try
		{
			statement = database_connection.createStatement();
		}
		catch(SQLException exception)
		{
			System.out.println("Failed to create statement"+exception.getMessage());
			allCompanies = null;
			return allCompanies;
		}
		
		try
		{
			ResultSet resultSet = statement.executeQuery(query);
			boolean hasNext = false;
			while(resultSet.next())
			{
				hasNext = true;
				Company pergjigje = new Company();
				pergjigje.setAddress(resultSet.getString("adress"));
				pergjigje.setCity(resultSet.getString("city"));
				pergjigje.setCompanyId(Integer.toString(resultSet.getInt("company_id")));
				pergjigje.setCountry(resultSet.getString("country"));
				pergjigje.setDefaultCurrency(resultSet.getString("default_currency"));
				pergjigje.setEMail(resultSet.getString("e_mail"));
				pergjigje.setFax(resultSet.getString("fax"));
				pergjigje.setName(resultSet.getString("name"));
				pergjigje.setPhoneNumber(resultSet.getString("phone_number"));
				pergjigje.setTaxCode(resultSet.getString("tax_code"));
				pergjigje.setVatNumber(resultSet.getString("vat_number"));
				pergjigje.setWeb(resultSet.getString("web"));
				pergjigje.setZipCode(resultSet.getString("zip_code"));
				allCompanies.add(pergjigje);
			}
			
			
			if(hasNext == false)
			{
				System.out.println("No information found in database");
				allCompanies = null;
				return allCompanies;
			}			

			statement.close();
			database_connection.close();

			return allCompanies;
		}
		catch(SQLException exception)
		{
			System.out.println("Failed to excecute statement"+exception.getMessage());
			allCompanies = null;
			return allCompanies;
		}
		
	}
	
	public List<Company> findAllCompaniesCustom(String archive_name,String queryClient) {
		//Assert.notNull(name);
		List<Company> allCompanies = new ArrayList<Company>();
		Connection database_connection = null;
				
		try
		{
			database_connection = new OjdbcConnection().connectToSqlite(archive_name);
		}
		catch(SQLException exception)
		{
			System.out.println("Failed to connect to the database"+exception.getMessage());
			allCompanies = null;
			return allCompanies;
		}
		
		String query = queryClient;
		Statement statement = null;
		try
		{
			statement = database_connection.createStatement();
		}
		catch(SQLException exception)
		{
			System.out.println("Failed to create statement"+exception.getMessage());
			allCompanies = null;
			return allCompanies;
		}
		
		try
		{
			ResultSet resultSet = statement.executeQuery(query);
			boolean hasNext = false;
			while(resultSet.next())
			{
				hasNext = true;
				Company pergjigje = new Company();
				pergjigje.setAddress(resultSet.getString("adress"));
				pergjigje.setCity(resultSet.getString("city"));
				pergjigje.setCompanyId(Integer.toString(resultSet.getInt("company_id")));
				pergjigje.setCountry(resultSet.getString("country"));
				pergjigje.setDefaultCurrency(resultSet.getString("default_currency"));
				pergjigje.setEMail(resultSet.getString("e_mail"));
				pergjigje.setFax(resultSet.getString("fax"));
				pergjigje.setName(resultSet.getString("name"));
				pergjigje.setPhoneNumber(resultSet.getString("phone_number"));
				pergjigje.setTaxCode(resultSet.getString("tax_code"));
				pergjigje.setVatNumber(resultSet.getString("vat_number"));
				pergjigje.setWeb(resultSet.getString("web"));
				pergjigje.setZipCode(resultSet.getString("zip_code"));
				allCompanies.add(pergjigje);
			}
			
			
			if(hasNext == false)
			{
				System.out.println("No information found in database");
				allCompanies = null;
				return allCompanies;
			}			

			statement.close();
			database_connection.close();

			return allCompanies;
		}
		catch(SQLException exception)
		{
			System.out.println("Failed to excecute statement"+exception.getMessage());
			allCompanies = null;
			return allCompanies;
		}
		
	}
	
	public List<Company> findCompaniesLikeName(String name,String archive_name) {
		Assert.notNull(name);
		List<Company> allCompanies = new ArrayList<Company>();
		Connection database_connection = null;
				
		try
		{
			database_connection = new OjdbcConnection().connectToSqlite(archive_name);
		}
		catch(SQLException exception)
		{
			System.out.println("Failed to connect to the database"+exception.getMessage());
			allCompanies = null;
			return allCompanies;
		}
		
		String query = "select * from company where name like '%"+name+"%'";
		Statement statement = null;
		try
		{
			statement = database_connection.createStatement();
		}
		catch(SQLException exception)
		{
			System.out.println("Failed to create statement"+exception.getMessage());
			allCompanies = null;
			return allCompanies;
		}
		
		try
		{
			ResultSet resultSet = statement.executeQuery(query);
			boolean hasNext = false;
			while(resultSet.next())
			{
				hasNext = true;
				Company pergjigje = new Company();
				pergjigje.setAddress(resultSet.getString("adress"));
				pergjigje.setCity(resultSet.getString("city"));
				pergjigje.setCompanyId(Integer.toString(resultSet.getInt("company_id")));
				pergjigje.setCountry(resultSet.getString("country"));
				pergjigje.setDefaultCurrency(resultSet.getString("default_currency"));
				pergjigje.setEMail(resultSet.getString("e_mail"));
				pergjigje.setFax(resultSet.getString("fax"));
				pergjigje.setName(resultSet.getString("name"));
				pergjigje.setPhoneNumber(resultSet.getString("phone_number"));
				pergjigje.setTaxCode(resultSet.getString("tax_code"));
				pergjigje.setVatNumber(resultSet.getString("vat_number"));
				pergjigje.setWeb(resultSet.getString("web"));
				pergjigje.setZipCode(resultSet.getString("zip_code"));
				allCompanies.add(pergjigje);
			}
			
			
			if(hasNext == false)
			{
				System.out.println("No information found in database");
				allCompanies = null;
				return allCompanies;
			}			

			statement.close();
			database_connection.close();

			return allCompanies;
		}
		catch(SQLException exception)
		{
			System.out.println("Failed to excecute statement"+exception.getMessage());
			allCompanies = null;
			return allCompanies;
		}
		
	}

	public String addCompany(Company company,String archive_name){
		String result = "ERROR";
		//kontrollojme vlerat
		if (company.getName() != ""){							
				Connection database_connection = null;				
				try
				{
					database_connection = new OjdbcConnection().connectToSqlite(archive_name);
				}
				catch(SQLException exception)
				{
					System.out.println("Failed to connect to the database"+exception.getMessage());
					return result;
				}				
				try{
					String sql = "INSERT INTO company (name,tax_code,vat_number,adress,city,zip_code,country,phone_number,fax,e_mail,web,default_currency)  "
							+ "VALUES(?,?,?,?,?,?,?,?,?,?,?,?)";
					PreparedStatement pstmt = database_connection.prepareStatement(sql);
					pstmt.setString(1,company.getName());
					pstmt.setString(2,company.getTaxCode());
					pstmt.setString(3,company.getVatNumber());
					pstmt.setString(4,company.getAddress());
					pstmt.setString(5,company.getCity());
					pstmt.setString(6,company.getZipCode());
					pstmt.setString(7,company.getCountry());
					pstmt.setString(8,company.getPhoneNumber());
					pstmt.setString(9,company.getFax());
					pstmt.setString(10,company.getEMail());
					pstmt.setString(11,company.getWeb());
					pstmt.setString(12,company.getDefaultCurrency());
					pstmt.executeUpdate();					  
				}catch(SQLException exception)
				{
					System.out.println("Failed to execute query. Exception: "+exception.getMessage());
					return result;
				}
				result = "OK";
				return result;
		}else{
			return result;
		}		
	}
	
	public String deleteCompany(String company_id,String archive_name){
		String result = "ERROR";
		//kontrollojme vlerat
		if(findCompany(company_id,archive_name) != null){
			Connection database_connection = null;
			try
			{
				database_connection = new OjdbcConnection().connectToSqlite(archive_name);
			}
			catch(SQLException exception)
			{
				System.out.println("Failed to connect to the database"+exception.getMessage());
				return result;
			}
			try{
				String sql = "DELETE FROM company where company_id = ?";
				PreparedStatement pstmt = database_connection.prepareStatement(sql);						
				pstmt.setString(1,company_id);					        
				pstmt.executeUpdate();
			}catch(SQLException exception)
			{
				System.out.println("Failed to execute query. Exception: "+exception.getMessage());
				return result;
			}
			result = "OK";
			return result;
		}else{
			//nuk gjendet archive_id ne db
			return result;
		}
		
	}
	
	public String updateCompany(Company company,String archive_name){
		String result = "ERROR";
		//kontrollojme id
		if (findCompany(company.getCompanyId(),archive_name) != null){
					Connection database_connection = null;
					try
					{
						database_connection = new OjdbcConnection().connectToSqlite(archive_name);
					}
					catch(SQLException exception)
					{
						System.out.println("Failed to connect to the database"+exception.getMessage());
						return result;
					}
					try{
						String sql = " UPDATE company SET name = ?, tax_code = ?, vat_number = ?, adress = ?, city = ? ,"
								+ "zip_code = ? , country = ?, phone_number = ?, fax = ?, e_mail = ?, web = ?, default_currency = ? "
								+ "WHERE company_id = ? ";
						PreparedStatement pstmt = database_connection.prepareStatement(sql);						
						pstmt.setString(1,company.getName());					        
						pstmt.setString(2,company.getTaxCode());
						pstmt.setString(3,company.getVatNumber());
						pstmt.setString(4,company.getAddress());
						pstmt.setString(5,company.getCity());
						pstmt.setString(6,company.getZipCode());
						pstmt.setString(7,company.getCountry());
						pstmt.setString(8,company.getPhoneNumber());
						pstmt.setString(9,company.getFax());
						pstmt.setString(10,company.getEMail());
						pstmt.setString(11,company.getWeb());
						pstmt.setString(12,company.getDefaultCurrency());
						pstmt.setString(13,company.getCompanyId());
						pstmt.executeUpdate();
					}catch(SQLException exception)
					{
						System.out.println("Failed to execute query. Exception: "+exception.getMessage());
						return result;
					}
					result = "OK";
					return result;
		}else{
			return result;
		}
		
	}
	
	public Contact findContact(String contact_id,String archive_name){
		Assert.notNull(contact_id);
		Contact result = null;
		Connection database_connection = null;
				
		try
		{
			database_connection = new OjdbcConnection().connectToSqlite(archive_name);
		}
		catch(SQLException exception)
		{
			System.out.println("Failed to connect to the database"+exception.getMessage());
			return result;
		}
		
		String query = "select * from contact where contact_id = '" + contact_id + "'";
		Statement statement = null;
		try
		{
			statement = database_connection.createStatement();
		}
		catch(SQLException exception)
		{
			System.out.println("Failed to create statement"+exception.getMessage());
			return result;
		}
		
		try
		{
			ResultSet resultSet = statement.executeQuery(query);

			boolean hasNext = false;
			while(resultSet.next())
			{
				hasNext = true;
				Contact pergjigje = new Contact();
				pergjigje.setAddress(resultSet.getString("address"));
				pergjigje.setCity(resultSet.getString("city"));
				pergjigje.setContactId(Integer.toString(resultSet.getInt("contact_id")));
				//pergjigje.setContactType(resultSet.getString("contact_type"));
				pergjigje.setEMail1(resultSet.getString("email1"));
				pergjigje.setEMail2(resultSet.getString("email2"));
				pergjigje.setFax(resultSet.getString("fax"));
				pergjigje.setFullName(resultSet.getString("full_name"));
				pergjigje.setInstantMessage(resultSet.getString("instant_message"));
				pergjigje.setMobileNumber(resultSet.getString("mobile_number"));
				pergjigje.setNote(resultSet.getString("note"));
				pergjigje.setPhoneHome(resultSet.getString("phone_home"));
				pergjigje.setPhoneWork(resultSet.getString("phone_work"));
				pergjigje.setState(resultSet.getString("state"));
				pergjigje.setWeb(resultSet.getString("web"));
				pergjigje.setZipCode(resultSet.getString("zip_code"));
				result = pergjigje;
			}
			if(!hasNext)
			{
				System.out.println("No information found in database");
				return result;
			}

			statement.close();
			database_connection.close();
			System.out.println("Successfuly");
			return result;
		}
		catch(SQLException exception)
		{
			System.out.println("Failed to excecute statement"+exception.getMessage());
			return result;
		}
    }
	
	public List<Contact> findAllContacts(String archive_name) {
		//Assert.notNull(name);
		List<Contact> allContacts = new ArrayList<Contact>();
		Connection database_connection = null;
				
		try
		{
			database_connection = new OjdbcConnection().connectToSqlite(archive_name);
		}
		catch(SQLException exception)
		{
			System.out.println("Failed to connect to the database"+exception.getMessage());
			allContacts = null;
			return allContacts;
		}
		
		String query = "select * from contact";
		Statement statement = null;
		try
		{
			statement = database_connection.createStatement();
		}
		catch(SQLException exception)
		{
			System.out.println("Failed to create statement"+exception.getMessage());
			allContacts = null;
			return allContacts;
		}
		
		try
		{
			ResultSet resultSet = statement.executeQuery(query);
			boolean hasNext = false;
			while(resultSet.next())
			{
				hasNext = true;
				Contact pergjigje = new Contact();
				pergjigje.setAddress(resultSet.getString("address"));
				pergjigje.setCity(resultSet.getString("city"));
				pergjigje.setContactId(Integer.toString(resultSet.getInt("contact_id")));
				
				pergjigje.setEMail1(resultSet.getString("email1"));
				pergjigje.setEMail2(resultSet.getString("email2"));
				pergjigje.setFax(resultSet.getString("fax"));
				pergjigje.setFullName(resultSet.getString("full_name"));
				pergjigje.setInstantMessage(resultSet.getString("instant_message"));
				pergjigje.setMobileNumber(resultSet.getString("mobile_number"));
				pergjigje.setNote(resultSet.getString("note"));
				pergjigje.setPhoneHome(resultSet.getString("phone_home"));
				pergjigje.setPhoneWork(resultSet.getString("phone_work"));
				pergjigje.setState(resultSet.getString("state"));
				pergjigje.setWeb(resultSet.getString("web"));
				pergjigje.setZipCode(resultSet.getString("zip_code"));
				allContacts.add(pergjigje);
			}
			
			
			if(hasNext == false)
			{
				System.out.println("No information found in database");
				allContacts = null;
				return allContacts;
			}			

			statement.close();
			database_connection.close();

			return allContacts;
		}
		catch(SQLException exception)
		{
			System.out.println("Failed to excecute statement"+exception.getMessage());
			allContacts = null;
			return allContacts;
		}
		
	}
	
	public List<Contact> findAllContactsCustom(String archive_name,String queryClient) {
		//Assert.notNull(name);
		List<Contact> allContacts = new ArrayList<Contact>();
		Connection database_connection = null;
				
		try
		{
			database_connection = new OjdbcConnection().connectToSqlite(archive_name);
		}
		catch(SQLException exception)
		{
			System.out.println("Failed to connect to the database"+exception.getMessage());
			allContacts = null;
			return allContacts;
		}
		
		String query = queryClient;
		Statement statement = null;
		try
		{
			statement = database_connection.createStatement();
		}
		catch(SQLException exception)
		{
			System.out.println("Failed to create statement"+exception.getMessage());
			allContacts = null;
			return allContacts;
		}
		
		try
		{
			ResultSet resultSet = statement.executeQuery(query);
			boolean hasNext = false;
			while(resultSet.next())
			{
				hasNext = true;
				Contact pergjigje = new Contact();
				pergjigje.setAddress(resultSet.getString("address"));
				pergjigje.setCity(resultSet.getString("city"));
				pergjigje.setContactId(Integer.toString(resultSet.getInt("contact_id")));
				pergjigje.setEMail1(resultSet.getString("email1"));
				pergjigje.setEMail2(resultSet.getString("email2"));
				pergjigje.setFax(resultSet.getString("fax"));
				pergjigje.setFullName(resultSet.getString("full_name"));
				pergjigje.setInstantMessage(resultSet.getString("instant_message"));
				pergjigje.setMobileNumber(resultSet.getString("mobile_number"));
				pergjigje.setNote(resultSet.getString("note"));
				pergjigje.setPhoneHome(resultSet.getString("phone_home"));
				pergjigje.setPhoneWork(resultSet.getString("phone_work"));
				pergjigje.setState(resultSet.getString("state"));
				pergjigje.setWeb(resultSet.getString("web"));
				pergjigje.setZipCode(resultSet.getString("zip_code"));
				allContacts.add(pergjigje);
			}
			
			
			if(hasNext == false)
			{
				System.out.println("No information found in database");
				allContacts = null;
				return allContacts;
			}			

			statement.close();
			database_connection.close();

			return allContacts;
		}
		catch(SQLException exception)
		{
			System.out.println("Failed to excecute statement"+exception.getMessage());
			allContacts = null;
			return allContacts;
		}
		
	}
	
	public List<Contact> findContactsLikeName(String name,String archive_name) {
		Assert.notNull(name);
		List<Contact> all_contacts = new ArrayList<Contact>();
		Connection database_connection = null;
				
		try
		{
			database_connection = new OjdbcConnection().connectToSqlite(archive_name);
		}
		catch(SQLException exception)
		{
			System.out.println("Failed to connect to the database"+exception.getMessage());
			all_contacts = null;
			return all_contacts;
		}
		
		String query = "select * from contact where full_name like '%"+name+"%'";
		Statement statement = null;
		try
		{
			statement = database_connection.createStatement();
		}
		catch(SQLException exception)
		{
			System.out.println("Failed to create statement"+exception.getMessage());
			all_contacts = null;
			return all_contacts;
		}
		
		try
		{
			ResultSet resultSet = statement.executeQuery(query);
			boolean hasNext = false;
			while(resultSet.next())
			{
				hasNext = true;
				Contact pergjigje = new Contact();
				pergjigje.setAddress(resultSet.getString("address"));
				pergjigje.setCity(resultSet.getString("city"));
				pergjigje.setContactId(Integer.toString(resultSet.getInt("contact_id")));
				pergjigje.setEMail1(resultSet.getString("email1"));
				pergjigje.setEMail2(resultSet.getString("email2"));
				pergjigje.setFax(resultSet.getString("fax"));
				pergjigje.setFullName(resultSet.getString("full_name"));
				pergjigje.setInstantMessage(resultSet.getString("instant_message"));
				pergjigje.setMobileNumber(resultSet.getString("mobile_number"));
				pergjigje.setNote(resultSet.getString("note"));
				pergjigje.setPhoneHome(resultSet.getString("phone_home"));
				pergjigje.setPhoneWork(resultSet.getString("phone_work"));
				pergjigje.setState(resultSet.getString("state"));
				pergjigje.setWeb(resultSet.getString("web"));
				pergjigje.setZipCode(resultSet.getString("zip_code"));
				all_contacts.add(pergjigje);
			}
			
			
			if(hasNext == false)
			{
				System.out.println("No information found in database");
				all_contacts = null;
				return all_contacts;
			}			

			statement.close();
			database_connection.close();

			return all_contacts;
		}
		catch(SQLException exception)
		{
			System.out.println("Failed to excecute statement"+exception.getMessage());
			all_contacts = null;
			return all_contacts;
		}
		
	}

	public String addContact(Contact contact,String archive_name){
		String result = "ERROR";
		//kontrollojme vlerat
		if (contact.getFullName() != ""){							
				Connection database_connection = null;				
				try
				{
					database_connection = new OjdbcConnection().connectToSqlite(archive_name);
				}
				catch(SQLException exception)
				{
					System.out.println("Failed to connect to the database"+exception.getMessage());
					return result;
				}				
				try{
					String sql = "INSERT INTO contact (full_name,address,city,zip_code,state,phone_home,phone_work,mobile_number,fax,email1,email2,instant_message,web,note)  "
							+ "VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
					PreparedStatement pstmt = database_connection.prepareStatement(sql);
					pstmt.setString(1,contact.getFullName());
					pstmt.setString(2,contact.getAddress());
					pstmt.setString(3,contact.getCity());
					pstmt.setString(4,contact.getZipCode());
					pstmt.setString(5,contact.getState());
					pstmt.setString(6,contact.getPhoneHome());
					pstmt.setString(7,contact.getPhoneWork());
					pstmt.setString(8,contact.getMobileNumber());
					pstmt.setString(9,contact.getFax());
					pstmt.setString(10,contact.getEMail1());
					pstmt.setString(11,contact.getEMail2());
					pstmt.setString(12,contact.getInstantMessage());
					pstmt.setString(13,contact.getWeb());
					pstmt.setString(14,contact.getNote());
					//pstmt.setString(15,contact.getContactType());
					pstmt.executeUpdate();					  
				}catch(SQLException exception)
				{
					System.out.println("Failed to execute query. Exception: "+exception.getMessage());
					return result;
				}
				result = "OK";
				return result;
		}else{
			return result;
		}		
	}
	
	public String deleteContact(String contact_id,String archive_name){
		String result = "ERROR";
		//kontrollojme vlerat
		if(findContact(contact_id,archive_name) != null){
			Connection database_connection = null;
			try
			{
				database_connection = new OjdbcConnection().connectToSqlite(archive_name);
			}
			catch(SQLException exception)
			{
				System.out.println("Failed to connect to the database"+exception.getMessage());
				return result;
			}
			try{
				String sql = "DELETE FROM contact where contact_id = ?";
				PreparedStatement pstmt = database_connection.prepareStatement(sql);						
				pstmt.setString(1,contact_id);					        
				pstmt.executeUpdate();
			}catch(SQLException exception)
			{
				System.out.println("Failed to execute query. Exception: "+exception.getMessage());
				return result;
			}
			result = "OK";
			return result;
		}else{
			//nuk gjendet archive_id ne db
			return result;
		}
		
	}
	
	public String updateContact(Contact contact, String archive_name){
		String result = "ERROR";
		//kontrollojme id
		if (findContact(contact.getContactId(),archive_name) != null){
					Connection database_connection = null;
					try
					{
						database_connection = new OjdbcConnection().connectToSqlite(archive_name);
					}
					catch(SQLException exception)
					{
						System.out.println("Failed to connect to the database"+exception.getMessage());
						return result;
					}
					try{
						String sql = " UPDATE contact SET full_name = ?, address = ?, city = ?, zip_code = ?, state = ? ,"
								+ "phone_home = ? , phone_work = ?, mobile_number = ?, fax = ?, email1 = ?, email2 = ?,"
								+ "instant_message = ?, web = ?, note = ?"
								+ "WHERE contact_id = ? ";
						PreparedStatement pstmt = database_connection.prepareStatement(sql);						
						pstmt.setString(1,contact.getFullName());					        
						pstmt.setString(2,contact.getAddress());
						pstmt.setString(3,contact.getCity());
						pstmt.setString(4,contact.getZipCode());
						pstmt.setString(5,contact.getState());
						pstmt.setString(6,contact.getPhoneHome());
						pstmt.setString(7,contact.getPhoneWork());
						pstmt.setString(8,contact.getMobileNumber());
						pstmt.setString(9,contact.getFax());
						pstmt.setString(10,contact.getEMail1());
						pstmt.setString(11,contact.getEMail2());
						pstmt.setString(12,contact.getInstantMessage());
						pstmt.setString(13,contact.getWeb());
						pstmt.setString(14,contact.getNote());
						//pstmt.setString(15,contact.getContactType());
						pstmt.setString(15,contact.getContactId());
						pstmt.executeUpdate();
					}catch(SQLException exception)
					{
						System.out.println("Failed to execute query. Exception: "+exception.getMessage());
						return result;
					}
					result = "OK";
					return result;
		}else{
			return result;
		}
		
	}
	
	public Entry findEntry(String entry_id,String archive_name){
		Assert.notNull(entry_id);
		Entry result = null;
		Connection database_connection = null;
				
		try
		{
			database_connection = new OjdbcConnection().connectToSqlite(archive_name);
		}
		catch(SQLException exception)
		{
			System.out.println("Failed to connect to the database"+exception.getMessage());
			return result;
		}
		
		String query = "select * from entry where entry_id = '" + entry_id + "'";
		Statement statement = null;
		try
		{
			statement = database_connection.createStatement();
		}
		catch(SQLException exception)
		{
			System.out.println("Failed to create statement"+exception.getMessage());
			return result;
		}
		
		try
		{
			ResultSet resultSet = statement.executeQuery(query);

			if(resultSet.next())
			{
				Entry pergjigje = new Entry();
				pergjigje.setDescription(resultSet.getString("description"));
				pergjigje.setDocumentDate(resultSet.getString("document_date"));
				pergjigje.setDocumentNumber(resultSet.getString("document_number"));
				pergjigje.setEntryDate(resultSet.getString("entry_date"));
				pergjigje.setEntryId(Integer.toString(resultSet.getInt("entry_id")));
				pergjigje.setIdBank(Integer.toString(resultSet.getInt("id_bank")));
				pergjigje.setIdOperationType(Integer.toString(resultSet.getInt("id_operation_type")));
				pergjigje.setIncomingValueBank(Integer.toString(resultSet.getInt("incoming_value_bank")));
				pergjigje.setOutgoingValueBank(Integer.toString(resultSet.getInt("outgoing_value_bank")));
				pergjigje.setIdContact(Integer.toString(resultSet.getInt("id_contact")));
				pergjigje.setOutCash(Integer.toString(resultSet.getInt("out_cash")));
				pergjigje.setIncomingValueCash(Integer.toString(resultSet.getInt("incoming_value_cash")));
				pergjigje.setIncomingValueCash(Integer.toString(resultSet.getInt("outgoing_value_cash")));
				result = pergjigje;
			}
			else
			{
				System.out.println("No information found in database");
				return result;
			}

			statement.close();
			database_connection.close();
			System.out.println("Successfuly");
			return result;
		}
		catch(SQLException exception)
		{
			System.out.println("Failed to excecute statement"+exception.getMessage());
			return result;
		}
    }
	
	public EntryPreviousPeriod findTotalsForEntriesPreviousPeriod(String endDate,String archive_name){
		Assert.notNull(endDate);
		EntryPreviousPeriod result = null;
		Connection database_connection = null;
				
		try
		{
			database_connection = new OjdbcConnection().connectToSqlite(archive_name);
		}
		catch(SQLException exception)
		{
			System.out.println("Failed to connect to the database"+exception.getMessage());
			return result;
		}
		
		String query = "SELECT SUM(out_cash) as out_cash,  SUM((incoming_value_bank - outgoing_value_bank)) as total_bank_balance, SUM((incoming_value_cash - outgoing_value_cash)) as total_cash_balance from entry WHERE entry_date < '"+endDate+"'";
		Statement statement = null;
		try
		{
			statement = database_connection.createStatement();
		}
		catch(SQLException exception)
		{
			System.out.println("Failed to create statement"+exception.getMessage());
			return result;
		}
		
		try
		{
			ResultSet resultSet = statement.executeQuery(query);

			if(resultSet.next())
			{
				EntryPreviousPeriod pergjigje = new EntryPreviousPeriod();
				pergjigje.setTotalBalanceBank(resultSet.getString("total_bank_balance"));
				pergjigje.setTotalBalanceCash(resultSet.getString("total_cash_balance"));
				pergjigje.setTotalOutCash(resultSet.getString("out_cash"));			
				result = pergjigje;
			}
			else
			{
				System.out.println("No information found in database");
				return result;
			}

			statement.close();
			database_connection.close();
			System.out.println("Successfuly");
			return result;
		}
		catch(SQLException exception)
		{
			System.out.println("Failed to excecute statement"+exception.getMessage());
			return result;
		}
    }
	
	public List<Entry> findAllEntries(String archive_name) {
		//Assert.notNull(name);
		List<Entry> allEntries = new ArrayList<Entry>();
		Connection database_connection = null;
				
		try
		{
			database_connection = new OjdbcConnection().connectToSqlite(archive_name);
		}
		catch(SQLException exception)
		{
			System.out.println("Failed to connect to the database"+exception.getMessage());
			allEntries = null;
			return allEntries;
		}
		
		String query = "select * from entry order by entry_date asc ";
		Statement statement = null;
		try
		{
			statement = database_connection.createStatement();
		}
		catch(SQLException exception)
		{
			System.out.println("Failed to create statement"+exception.getMessage());
			allEntries = null;
			return allEntries;
		}
		
		try
		{
			ResultSet resultSet = statement.executeQuery(query);
			boolean hasNext = false;
			while(resultSet.next())
			{
				hasNext = true;
				Entry pergjigje = new Entry();
				pergjigje.setDescription(resultSet.getString("description"));
				pergjigje.setDocumentDate(resultSet.getString("document_date"));
				pergjigje.setDocumentNumber(resultSet.getString("document_number"));
				pergjigje.setEntryDate(resultSet.getString("entry_date"));
				pergjigje.setEntryId(Integer.toString(resultSet.getInt("entry_id")));
				pergjigje.setIdBank(Integer.toString(resultSet.getInt("id_bank")));
				pergjigje.setIdOperationType(Integer.toString(resultSet.getInt("id_operation_type")));
				pergjigje.setIncomingValueBank(Double.toString(resultSet.getDouble("incoming_value_bank")));
				pergjigje.setOutgoingValueBank(Double.toString(resultSet.getDouble("outgoing_value_bank")));
				pergjigje.setIdContact(Integer.toString(resultSet.getInt("id_contact")));
				pergjigje.setOutCash(Double.toString(resultSet.getDouble("out_cash")));
				pergjigje.setIncomingValueCash(Double.toString(resultSet.getDouble("incoming_value_cash")));
				pergjigje.setOutgoingValueCash(Double.toString(resultSet.getDouble("outgoing_value_cash")));
				
				allEntries.add(pergjigje);
			}
			
			
			if(hasNext == false)
			{
				System.out.println("No information found in database");
				allEntries = null;
				return allEntries;
			}			

			statement.close();
			database_connection.close();

			return allEntries;
		}
		catch(SQLException exception)
		{
			System.out.println("Failed to excecute statement"+exception.getMessage());
			allEntries = null;
			return allEntries;
		}
		
	}
	
	
	public List<EntryBalance> findAllEntriesCustom(String archive_name,String queryClient) {
		//Assert.notNull(name);
		List<EntryBalance> allEntries = new ArrayList<EntryBalance>();
		Connection database_connection = null;
				
		try
		{
			database_connection = new OjdbcConnection().connectToSqlite(archive_name);
		}
		catch(SQLException exception)
		{
			System.out.println("Failed to connect to the database"+exception.getMessage());
			allEntries = null;
			return allEntries;
		}
		
		String query = queryClient;
		Statement statement = null;
		try
		{
			statement = database_connection.createStatement();
		}
		catch(SQLException exception)
		{
			System.out.println("Failed to create statement"+exception.getMessage());
			allEntries = null;
			return allEntries;
		}
		
		try
		{
			ResultSet resultSet = statement.executeQuery(query);
			boolean hasNext = false;
			while(resultSet.next())
			{
				hasNext = true;
				EntryBalance pergjigje = new EntryBalance();
				pergjigje.setDescription(resultSet.getString("description"));
				pergjigje.setDocumentDate(resultSet.getString("document_date"));
				pergjigje.setDocumentNumber(resultSet.getString("document_number"));
				pergjigje.setEntryDate(resultSet.getString("entry_date"));
				pergjigje.setEntryId(Integer.toString(resultSet.getInt("entry_id")));
				pergjigje.setIdBank(Integer.toString(resultSet.getInt("id_bank")));
				pergjigje.setIdOperationType(Integer.toString(resultSet.getInt("id_operation_type")));
				pergjigje.setIncomingValueBank(Double.toString(resultSet.getDouble("incoming_value_bank")));
				pergjigje.setOutgoingValueBank(Double.toString(resultSet.getDouble("outgoing_value_bank")));
				pergjigje.setIdContact(Integer.toString(resultSet.getInt("id_contact")));
				pergjigje.setOutCash(Double.toString(resultSet.getDouble("out_cash")));
				pergjigje.setIncomingValueCash(Double.toString(resultSet.getDouble("incoming_value_cash")));
				pergjigje.setOutgoingValueCash(Double.toString(resultSet.getDouble("outgoing_value_cash")));
				pergjigje.setBalanceBank(Double.toString(resultSet.getDouble("balance_bank")));
				pergjigje.setBalanceCash(Double.toString(resultSet.getDouble("balance_cash")));
				allEntries.add(pergjigje);
			}
			
			
			if(hasNext == false)
			{
				System.out.println("No information found in database");
				allEntries = null;
				return allEntries;
			}			

			statement.close();
			database_connection.close();

			return allEntries;
		}
		catch(SQLException exception)
		{
			System.out.println("Failed to excecute statement"+exception.getMessage());
			allEntries = null;
			return allEntries;
		}
		
	}
	
		
	public List<Entry> findEntriesByOperationType(String id_operation_type,String archive_name) {
		Assert.notNull(id_operation_type);
		List<Entry> all_entries = new ArrayList<Entry>();
		Connection database_connection = null;
				
		try
		{
			database_connection = new OjdbcConnection().connectToSqlite(archive_name);
		}
		catch(SQLException exception)
		{
			System.out.println("Failed to connect to the database"+exception.getMessage());
			all_entries = null;
			return all_entries;
		}
		
		String query = "select * from entry where id_operation_type = '"+id_operation_type+"' order by entry_date asc";
		Statement statement = null;
		try
		{
			statement = database_connection.createStatement();
		}
		catch(SQLException exception)
		{
			System.out.println("Failed to create statement"+exception.getMessage());
			all_entries = null;
			return all_entries;
		}
		
		try
		{
			ResultSet resultSet = statement.executeQuery(query);
			boolean hasNext = false;
			while(resultSet.next())
			{
				hasNext = true;
				Entry pergjigje = new Entry();
				pergjigje.setDescription(resultSet.getString("description"));
				pergjigje.setDocumentDate(resultSet.getString("document_date"));
				pergjigje.setDocumentNumber(resultSet.getString("document_number"));
				pergjigje.setEntryDate(resultSet.getString("entry_date"));
				pergjigje.setEntryId(Integer.toString(resultSet.getInt("entry_id")));
				pergjigje.setIdBank(Integer.toString(resultSet.getInt("id_bank")));
				pergjigje.setIdOperationType(Integer.toString(resultSet.getInt("id_operation_type")));
				pergjigje.setIncomingValueBank(Double.toString(resultSet.getDouble("incoming_value_bank")));
				pergjigje.setOutgoingValueBank(Double.toString(resultSet.getDouble("outgoing_value_bank")));
				pergjigje.setIdContact(Integer.toString(resultSet.getInt("id_contact")));
				pergjigje.setOutCash(Double.toString(resultSet.getDouble("out_cash")));
				pergjigje.setIncomingValueCash(Double.toString(resultSet.getDouble("incoming_value_cash")));
				pergjigje.setOutgoingValueCash(Double.toString(resultSet.getDouble("outgoing_value_cash")));
				
				all_entries.add(pergjigje);
			}
			
			
			if(hasNext == false)
			{
				System.out.println("No information found in database");
				all_entries = null;
				return all_entries;
			}			

			statement.close();
			database_connection.close();

			return all_entries;
		}
		catch(SQLException exception)
		{
			System.out.println("Failed to excecute statement"+exception.getMessage());
			all_entries = null;
			return all_entries;
		}
		
	}
		
	public List<Entry> findEntriesByBank(String id_bank,String archive_name) {
		Assert.notNull(id_bank);
		List<Entry> all_entries = new ArrayList<Entry>();
		Connection database_connection = null;
				
		try
		{
			database_connection = new OjdbcConnection().connectToSqlite(archive_name);
		}
		catch(SQLException exception)
		{
			System.out.println("Failed to connect to the database"+exception.getMessage());
			all_entries = null;
			return all_entries;
		}
		
		String query = "select * from entry where id_bank = '"+id_bank+"' order by entry_date asc";
		Statement statement = null;
		try
		{
			statement = database_connection.createStatement();
		}
		catch(SQLException exception)
		{
			System.out.println("Failed to create statement"+exception.getMessage());
			all_entries = null;
			return all_entries;
		}
		
		try
		{
			ResultSet resultSet = statement.executeQuery(query);
			boolean hasNext = false;
			while(resultSet.next())
			{
				hasNext = true;
				Entry pergjigje = new Entry();
				pergjigje.setDescription(resultSet.getString("description"));
				pergjigje.setDocumentDate(resultSet.getString("document_date"));
				pergjigje.setDocumentNumber(resultSet.getString("document_number"));
				pergjigje.setEntryDate(resultSet.getString("entry_date"));
				pergjigje.setEntryId(Integer.toString(resultSet.getInt("entry_id")));
				pergjigje.setIdBank(Integer.toString(resultSet.getInt("id_bank")));
				pergjigje.setIdOperationType(Integer.toString(resultSet.getInt("id_operation_type")));
				pergjigje.setIncomingValueBank(Double.toString(resultSet.getDouble("incoming_value_bank")));
				pergjigje.setOutgoingValueBank(Double.toString(resultSet.getDouble("outgoing_value_bank")));
				pergjigje.setIdContact(Integer.toString(resultSet.getInt("id_contact")));
				pergjigje.setOutCash(Double.toString(resultSet.getDouble("out_cash")));
				pergjigje.setIncomingValueCash(Double.toString(resultSet.getDouble("incoming_value_cash")));
				pergjigje.setOutgoingValueCash(Double.toString(resultSet.getDouble("outgoing_value_cash")));
				all_entries.add(pergjigje);
			}
			
			
			if(hasNext == false)
			{
				System.out.println("No information found in database");
				all_entries = null;
				return all_entries;
			}			

			statement.close();
			database_connection.close();

			return all_entries;
		}
		catch(SQLException exception)
		{
			System.out.println("Failed to excecute statement"+exception.getMessage());
			all_entries = null;
			return all_entries;
		}
		
	}
	
	public List<Entry> findEntriesByDate(String date,String archive_name) {
		Assert.notNull(date);
		List<Entry> all_entries = new ArrayList<Entry>();
		Connection database_connection = null;
				
		try
		{
			database_connection = new OjdbcConnection().connectToSqlite(archive_name);
		}
		catch(SQLException exception)
		{
			System.out.println("Failed to connect to the database"+exception.getMessage());
			all_entries = null;
			return all_entries;
		}
		
		String query = "select * from entry where entry_date = '"+date+"'";
		Statement statement = null;
		try
		{
			statement = database_connection.createStatement();
		}
		catch(SQLException exception)
		{
			System.out.println("Failed to create statement"+exception.getMessage());
			all_entries = null;
			return all_entries;
		}
		
		try
		{
			ResultSet resultSet = statement.executeQuery(query);
			boolean hasNext = false;
			while(resultSet.next())
			{
				hasNext = true;
				Entry pergjigje = new Entry();
				pergjigje.setDescription(resultSet.getString("description"));
				pergjigje.setDocumentDate(resultSet.getString("document_date"));
				pergjigje.setDocumentNumber(resultSet.getString("document_number"));
				pergjigje.setEntryDate(resultSet.getString("entry_date"));
				pergjigje.setEntryId(Integer.toString(resultSet.getInt("entry_id")));
				pergjigje.setIdBank(Integer.toString(resultSet.getInt("id_bank")));
				pergjigje.setIdOperationType(Integer.toString(resultSet.getInt("id_operation_type")));
				pergjigje.setIncomingValueBank(Double.toString(resultSet.getDouble("incoming_value_bank")));
				pergjigje.setOutgoingValueBank(Double.toString(resultSet.getDouble("outgoing_value_bank")));
				pergjigje.setIdContact(Integer.toString(resultSet.getInt("id_contact")));
				pergjigje.setOutCash(Double.toString(resultSet.getDouble("out_cash")));
				pergjigje.setIncomingValueCash(Double.toString(resultSet.getDouble("incoming_value_cash")));
				pergjigje.setOutgoingValueCash(Double.toString(resultSet.getDouble("outgoing_value_cash")));
				all_entries.add(pergjigje);
			}
			
			
			if(hasNext == false)
			{
				System.out.println("No information found in database");
				all_entries = null;
				return all_entries;
			}			

			statement.close();
			database_connection.close();

			return all_entries;
		}
		catch(SQLException exception)
		{
			System.out.println("Failed to excecute statement"+exception.getMessage());
			all_entries = null;
			return all_entries;
		}
		
	}
	
	public List<Entry> findEntriesAfterDate(String date,String archive_name) {
		Assert.notNull(date);
		List<Entry> all_entries = new ArrayList<Entry>();
		Connection database_connection = null;
				
		try
		{
			database_connection = new OjdbcConnection().connectToSqlite(archive_name);
		}
		catch(SQLException exception)
		{
			System.out.println("Failed to connect to the database"+exception.getMessage());
			all_entries = null;
			return all_entries;
		}
		
		String query = "select * from entry where entry_date >= '"+date+"' order by entry_date asc";
		Statement statement = null;
		try
		{
			statement = database_connection.createStatement();
		}
		catch(SQLException exception)
		{
			System.out.println("Failed to create statement"+exception.getMessage());
			all_entries = null;
			return all_entries;
		}
		
		try
		{
			ResultSet resultSet = statement.executeQuery(query);
			boolean hasNext = false;
			while(resultSet.next())
			{
				hasNext = true;
				Entry pergjigje = new Entry();
				pergjigje.setDescription(resultSet.getString("description"));
				pergjigje.setDocumentDate(resultSet.getString("document_date"));
				pergjigje.setDocumentNumber(resultSet.getString("document_number"));
				pergjigje.setEntryDate(resultSet.getString("entry_date"));
				pergjigje.setEntryId(Integer.toString(resultSet.getInt("entry_id")));
				pergjigje.setIdBank(Integer.toString(resultSet.getInt("id_bank")));
				pergjigje.setIdOperationType(Integer.toString(resultSet.getInt("id_operation_type")));
				pergjigje.setIncomingValueBank(Double.toString(resultSet.getDouble("incoming_value_bank")));
				pergjigje.setOutgoingValueBank(Double.toString(resultSet.getDouble("outgoing_value_bank")));
				pergjigje.setIdContact(Integer.toString(resultSet.getInt("id_contact")));
				pergjigje.setOutCash(Double.toString(resultSet.getDouble("out_cash")));
				pergjigje.setIncomingValueCash(Double.toString(resultSet.getDouble("incoming_value_cash")));
				pergjigje.setOutgoingValueCash(Double.toString(resultSet.getDouble("outgoing_value_cash")));
				all_entries.add(pergjigje);
			}
			
			
			if(hasNext == false)
			{
				System.out.println("No information found in database");
				all_entries = null;
				return all_entries;
			}			

			statement.close();
			database_connection.close();

			return all_entries;
		}
		catch(SQLException exception)
		{
			System.out.println("Failed to excecute statement"+exception.getMessage());
			all_entries = null;
			return all_entries;
		}
		
	}
	
	public List<Entry> findEntriesBeforeDate(String date,String archive_name) {
		Assert.notNull(date);
		List<Entry> all_entries = new ArrayList<Entry>();
		Connection database_connection = null;
				
		try
		{
			database_connection = new OjdbcConnection().connectToSqlite(archive_name);
		}
		catch(SQLException exception)
		{
			System.out.println("Failed to connect to the database"+exception.getMessage());
			all_entries = null;
			return all_entries;
		}
		
		String query = "select * from entry where entry_date < '"+date+"' order by entry_date asc";
		Statement statement = null;
		try
		{
			statement = database_connection.createStatement();
		}
		catch(SQLException exception)
		{
			System.out.println("Failed to create statement"+exception.getMessage());
			all_entries = null;
			return all_entries;
		}
		
		try
		{
			ResultSet resultSet = statement.executeQuery(query);
			boolean hasNext = false;
			while(resultSet.next())
			{
				hasNext = true;
				Entry pergjigje = new Entry();
				pergjigje.setDescription(resultSet.getString("description"));
				pergjigje.setDocumentDate(resultSet.getString("document_date"));
				pergjigje.setDocumentNumber(resultSet.getString("document_number"));
				pergjigje.setEntryDate(resultSet.getString("entry_date"));
				pergjigje.setEntryId(Integer.toString(resultSet.getInt("entry_id")));
				pergjigje.setIdBank(Integer.toString(resultSet.getInt("id_bank")));
				pergjigje.setIdOperationType(Integer.toString(resultSet.getInt("id_operation_type")));
				pergjigje.setIncomingValueBank(Double.toString(resultSet.getDouble("incoming_value_bank")));
				pergjigje.setOutgoingValueBank(Double.toString(resultSet.getDouble("outgoing_value_bank")));
				pergjigje.setIdContact(Integer.toString(resultSet.getInt("id_contact")));
				pergjigje.setOutCash(Double.toString(resultSet.getDouble("out_cash")));
				pergjigje.setIncomingValueCash(Double.toString(resultSet.getDouble("incoming_value_cash")));
				pergjigje.setOutgoingValueCash(Double.toString(resultSet.getDouble("outgoing_value_cash")));
				all_entries.add(pergjigje);
			}
			
			
			if(hasNext == false)
			{
				System.out.println("No information found in database");
				all_entries = null;
				return all_entries;
			}			

			statement.close();
			database_connection.close();

			return all_entries;
		}
		catch(SQLException exception)
		{
			System.out.println("Failed to excecute statement"+exception.getMessage());
			all_entries = null;
			return all_entries;
		}
		
	}
	
	public List<Entry> findEntriesBetweenDates(String startDate, String endDate,String archive_name) {
		Assert.notNull(startDate);
		Assert.notNull(endDate);
		List<Entry> all_entries = new ArrayList<Entry>();
		Connection database_connection = null;
				
		try
		{
			database_connection = new OjdbcConnection().connectToSqlite(archive_name);
		}
		catch(SQLException exception)
		{
			System.out.println("Failed to connect to the database"+exception.getMessage());
			all_entries = null;
			return all_entries;
		}
		
		String query = "select * from entry where entry_date BETWEEN '"+startDate+"' AND '"+endDate+"' order by entry_date asc";
		Statement statement = null;
		try
		{
			statement = database_connection.createStatement();
		}
		catch(SQLException exception)
		{
			System.out.println("Failed to create statement"+exception.getMessage());
			all_entries = null;
			return all_entries;
		}
		
		try
		{
			ResultSet resultSet = statement.executeQuery(query);
			boolean hasNext = false;
			while(resultSet.next())
			{
				hasNext = true;
				Entry pergjigje = new Entry();
				pergjigje.setDescription(resultSet.getString("description"));
				pergjigje.setDocumentDate(resultSet.getString("document_date"));
				pergjigje.setDocumentNumber(resultSet.getString("document_number"));
				pergjigje.setEntryDate(resultSet.getString("entry_date"));
				pergjigje.setEntryId(Integer.toString(resultSet.getInt("entry_id")));
				pergjigje.setIdBank(Integer.toString(resultSet.getInt("id_bank")));
				pergjigje.setIdOperationType(Integer.toString(resultSet.getInt("id_operation_type")));
				pergjigje.setIncomingValueBank(Double.toString(resultSet.getDouble("incoming_value_bank")));
				pergjigje.setOutgoingValueBank(Double.toString(resultSet.getDouble("outgoing_value_bank")));
				pergjigje.setIdContact(Integer.toString(resultSet.getInt("id_contact")));
				pergjigje.setOutCash(Double.toString(resultSet.getDouble("out_cash")));
				pergjigje.setIncomingValueCash(Double.toString(resultSet.getDouble("incoming_value_cash")));
				pergjigje.setOutgoingValueCash(Double.toString(resultSet.getDouble("outgoing_value_cash")));
				all_entries.add(pergjigje);
			}
			
			
			if(hasNext == false)
			{
				System.out.println("No information found in database");
				all_entries = null;
				return all_entries;
			}			

			statement.close();
			database_connection.close();

			return all_entries;
		}
		catch(SQLException exception)
		{
			System.out.println("Failed to excecute statement"+exception.getMessage());
			all_entries = null;
			return all_entries;
		}
		
	}
	
	public String addEntry(Entry entry,String archive_name){
	String result = "ERROR";
	//kontrollojme vlerat
	if (entry.getEntryDate() != ""){							
			//if(findBank(entry.getIdBank(),archive_name) != null ){
				//if(findOperationType(entry.getIdOperationType(),archive_name)!= null){
					Connection database_connection = null;				
					try
					{
						database_connection = new OjdbcConnection().connectToSqlite(archive_name);
					}
					catch(SQLException exception)
					{
						System.out.println("Failed to connect to the database"+exception.getMessage());
						return result;
					}				
					try{
						String sql = "INSERT INTO entry (entry_date,document_number,document_date,id_operation_type,description,incoming_value_bank,outgoing_value_bank,id_bank,id_contact,out_cash,incoming_value_cash,outgoing_value_cash)  "
								+ "VALUES(?,?,?,?,?,?,?,?,?,?,?,?)";
						PreparedStatement pstmt = database_connection.prepareStatement(sql);
						pstmt.setString(1,entry.getEntryDate());
						pstmt.setString(2,entry.getDocumentNumber());
						pstmt.setString(3,entry.getDocumentDate());
						pstmt.setString(4,entry.getIdOperationType());
						pstmt.setString(5,entry.getDescription());
						pstmt.setString(6,entry.getIncomingValueBank());
						pstmt.setString(7,entry.getOutgoingValueBank());
						pstmt.setString(8,entry.getIdBank());
						pstmt.setString(9,entry.getIdContact());
						pstmt.setString(10, entry.getOutCash());
						pstmt.setString(11,entry.getIncomingValueCash());
						pstmt.setString(12,entry.getOutgoingValueCash());
						pstmt.executeUpdate();					  
					}catch(SQLException exception)
					{
						System.out.println("Failed to execute query. Exception: "+exception.getMessage());
						return result;
					}
					result = "OK";
					return result;
				//}else{
					//operation id not in database
				//	return result;
				//}
			//}else{
				//bank_id not in database
				//return result;
			//}
	}else{
		//entry date empty
		return result;
	}		
}

	public String deleteEntry(String entry_id,String archive_name){
		String result = "ERROR";
		//kontrollojme vlerat
		if(findEntry(entry_id,archive_name) != null){
			Connection database_connection = null;
			try
			{
				database_connection = new OjdbcConnection().connectToSqlite(archive_name);
			}
			catch(SQLException exception)
			{
				System.out.println("Failed to connect to the database"+exception.getMessage());
				return result;
			}
			try{
				String sql = "DELETE FROM entry where entry_id = ?";
				PreparedStatement pstmt = database_connection.prepareStatement(sql);						
				pstmt.setString(1,entry_id);					        
				pstmt.executeUpdate();
			}catch(SQLException exception)
			{
				System.out.println("Failed to execute query. Exception: "+exception.getMessage());
				return result;
			}
			result = "OK";
			return result;
		}else{
			//nuk gjendet archive_id ne db
			return result;
		}
		
	}
	
	public String updateEntry(Entry entry,String archive_name){
		String result = "ERROR";
		//kontrollojme id
		if (findEntry(entry.getEntryId(),archive_name) != null){
			//if(findOperationType(entry.getIdOperationType(),archive_name) != null){
			//	if(findBank(entry.getIdBank(),archive_name) != null){
					Connection database_connection = null;
					try
					{
						database_connection = new OjdbcConnection().connectToSqlite(archive_name);
					}
					catch(SQLException exception)
					{
						System.out.println("Failed to connect to the database"+exception.getMessage());
						return result;
					}
					try{
						String sql = " UPDATE entry SET entry_date = ?, document_number = ?, document_date = ?, id_operation_type = ?,"
								+ "description = ? , incoming_value_bank = ?, outgoing_value_bank = ?, id_bank = ? , id_contact = ? , out_cash = ? , incoming_value_cash = ?, outgoing_value_cash = ?"
								+ "WHERE entry_id = ? ";
						PreparedStatement pstmt = database_connection.prepareStatement(sql);						
						pstmt.setString(1,entry.getEntryDate());					        
						pstmt.setString(2,entry.getDocumentNumber());
						pstmt.setString(3,entry.getDocumentDate());
						pstmt.setString(4,entry.getIdOperationType());
						pstmt.setString(5,entry.getDescription());
						pstmt.setString(6,entry.getIncomingValueBank());
						pstmt.setString(7,entry.getOutgoingValueBank());
						pstmt.setString(8,entry.getIdBank());
						pstmt.setString(9,entry.getIdContact());
						pstmt.setString(10,entry.getOutCash());
						pstmt.setString(11,entry.getIncomingValueCash());
						pstmt.setString(12,entry.getOutgoingValueCash());
						pstmt.setString(13,entry.getEntryId());
						
						pstmt.executeUpdate();
					}catch(SQLException exception)
					{
						System.out.println("Failed to execute query. Exception: "+exception.getMessage());
						return result;
					}
					result = "OK";
					return result;
				//}else{
				//	//id bank not found
				//	return result;
				//}
			//}else{
				//id operation type not found
			//	return result;
			//}
		}else{
			return result;
		}
		
	}
	
	public OperationType findOperationType(String operation_type_id,String archive_name){
		Assert.notNull(operation_type_id);
		OperationType result = null;
		Connection database_connection = null;
				
		try
		{
			database_connection = new OjdbcConnection().connectToSqlite(archive_name);
		}
		catch(SQLException exception)
		{
			System.out.println("Failed to connect to the database"+exception.getMessage());
			return result;
		}
		
		String query = "select * from operation_type where operation_type_id = '" + operation_type_id + "'";
		Statement statement = null;
		try
		{
			statement = database_connection.createStatement();
		}
		catch(SQLException exception)
		{
			System.out.println("Failed to create statement"+exception.getMessage());
			return result;
		}
		
		try
		{
			ResultSet resultSet = statement.executeQuery(query);

			if(resultSet.next())
			{
				OperationType pergjigje = new OperationType();
				pergjigje.setName(resultSet.getString("name"));
				pergjigje.setIncoming(Boolean.toString(resultSet.getBoolean("incoming")));
				pergjigje.setOperationTypeId(Integer.toString(resultSet.getInt("operation_type_id")));
				pergjigje.setOutgoing(Boolean.toString(resultSet.getBoolean("outgoing")));
				result = pergjigje;
			}
			else
			{
				System.out.println("No information found in database");
				return result;
			}

			statement.close();
			database_connection.close();
			System.out.println("Successfuly");
			return result;
		}
		catch(SQLException exception)
		{
			System.out.println("Failed to excecute statement"+exception.getMessage());
			return result;
		}
    }
		
	public List<OperationType> findAllOperationTypes(String archive_name) {
		//Assert.notNull(name);
		List<OperationType> allOperationTypes = new ArrayList<OperationType>();
		Connection database_connection = null;
				
		try
		{
			database_connection = new OjdbcConnection().connectToSqlite(archive_name);
		}
		catch(SQLException exception)
		{
			System.out.println("Failed to connect to the database"+exception.getMessage());
			allOperationTypes = null;
			return allOperationTypes;
		}
		
		String query = "select * from operation_type ";
		Statement statement = null;
		try
		{
			statement = database_connection.createStatement();
		}
		catch(SQLException exception)
		{
			System.out.println("Failed to create statement"+exception.getMessage());
			allOperationTypes = null;
			return allOperationTypes;
		}
		
		try
		{
			ResultSet resultSet = statement.executeQuery(query);
			boolean hasNext = false;
			while(resultSet.next())
			{
				hasNext = true;
				OperationType pergjigje = new OperationType();
				pergjigje.setName(resultSet.getString("name"));
				pergjigje.setIncoming(Boolean.toString(resultSet.getBoolean("incoming")));
				pergjigje.setOperationTypeId(Integer.toString(resultSet.getInt("operation_type_id")));
				pergjigje.setOutgoing(Boolean.toString(resultSet.getBoolean("outgoing")));
				allOperationTypes.add(pergjigje);
			}
			
			
			if(hasNext == false)
			{
				System.out.println("No information found in database");
				allOperationTypes = null;
				return allOperationTypes;
			}			

			statement.close();
			database_connection.close();

			return allOperationTypes;
		}
		catch(SQLException exception)
		{
			System.out.println("Failed to excecute statement"+exception.getMessage());
			allOperationTypes = null;
			return allOperationTypes;
		}
		
	}
	
	public List<OperationType> findAllOperationTypesCustom(String archive_name,String queryClient) {
		//Assert.notNull(name);
		List<OperationType> allOperationTypes = new ArrayList<OperationType>();
		Connection database_connection = null;
				
		try
		{
			database_connection = new OjdbcConnection().connectToSqlite(archive_name);
		}
		catch(SQLException exception)
		{
			System.out.println("Failed to connect to the database"+exception.getMessage());
			allOperationTypes = null;
			return allOperationTypes;
		}
		
		String query = queryClient;
		Statement statement = null;
		try
		{
			statement = database_connection.createStatement();
		}
		catch(SQLException exception)
		{
			System.out.println("Failed to create statement"+exception.getMessage());
			allOperationTypes = null;
			return allOperationTypes;
		}
		
		try
		{
			ResultSet resultSet = statement.executeQuery(query);
			boolean hasNext = false;
			while(resultSet.next())
			{
				hasNext = true;
				OperationType pergjigje = new OperationType();
				pergjigje.setName(resultSet.getString("name"));
				pergjigje.setIncoming(Boolean.toString(resultSet.getBoolean("incoming")));
				pergjigje.setOperationTypeId(Integer.toString(resultSet.getInt("operation_type_id")));
				pergjigje.setOutgoing(Boolean.toString(resultSet.getBoolean("outgoing")));
				allOperationTypes.add(pergjigje);
			}
			
			
			if(hasNext == false)
			{
				System.out.println("No information found in database");
				allOperationTypes = null;
				return allOperationTypes;
			}			

			statement.close();
			database_connection.close();

			return allOperationTypes;
		}
		catch(SQLException exception)
		{
			System.out.println("Failed to excecute statement"+exception.getMessage());
			allOperationTypes = null;
			return allOperationTypes;
		}
		
	}
	

	public String addOperationType(OperationType operationType,String archive_name){
		String result = "ERROR";
		//kontrollojme vlerat
		if (operationType.getName() != ""){							
				Connection database_connection = null;				
				try
				{
					database_connection = new OjdbcConnection().connectToSqlite(archive_name);
				}
				catch(SQLException exception)
				{
					System.out.println("Failed to connect to the database"+exception.getMessage());
					return result;
				}				
				try
				{
					String sql = "INSERT INTO operation_type (name,incoming,outgoing)  "
							+ "VALUES(?,?,?)";
					PreparedStatement pstmt = database_connection.prepareStatement(sql);
					pstmt.setString(1,operationType.getName());
					pstmt.setString(2,operationType.getIncoming());
					pstmt.setString(3,operationType.getOutgoing());
					pstmt.executeUpdate();					  
				}catch(SQLException exception)
				{
					System.out.println("Failed to execute query. Exception: "+exception.getMessage());
					return result;
				}
				result = "OK";
				return result;
		}else{
			//entry date empty
			return result;
		}		
	}
	
	public String deleteOperationType(String operation_type_id,String archive_name){
		String result = "ERROR";
		//kontrollojme vlerat
		if(findOperationType(operation_type_id,archive_name) != null){
			Connection database_connection = null;
			try
			{
				database_connection = new OjdbcConnection().connectToSqlite(archive_name);
			}
			catch(SQLException exception)
			{
				System.out.println("Failed to connect to the database"+exception.getMessage());
				return result;
			}
			try{
				String sql = "DELETE FROM operation_type where operation_type_id = ?";
				PreparedStatement pstmt = database_connection.prepareStatement(sql);						
				pstmt.setString(1,operation_type_id);					        
				pstmt.executeUpdate();
			}catch(SQLException exception)
			{
				System.out.println("Failed to execute query. Exception: "+exception.getMessage());
				return result;
			}
			result = "OK";
			return result;
		}else{
			//nuk gjendet archive_id ne db
			return result;
		}
		
	}
	
	public String updateOperationType(OperationType operationType, String archive_name){
		String result = "ERROR";
		//kontrollojme id
		if (findOperationType(operationType.getOperationTypeId(), archive_name) != null){
					Connection database_connection = null;
					try
					{
						database_connection = new OjdbcConnection().connectToSqlite(archive_name);
					}
					catch(SQLException exception)
					{
						System.out.println("Failed to connect to the database"+exception.getMessage());
						return result;
					}
					try{
						String sql = " UPDATE operation_type SET name = ?, incoming = ?, outgoing = ? "
								+ "WHERE operation_type_id = ? ";
						PreparedStatement pstmt = database_connection.prepareStatement(sql);						
						pstmt.setString(1,operationType.getName());					        
						pstmt.setString(2,operationType.getIncoming());
						pstmt.setString(3,operationType.getOutgoing());
						pstmt.setString(4,operationType.getOperationTypeId());
						pstmt.executeUpdate();
					}catch(SQLException exception)
					{
						System.out.println("Failed to execute query. Exception: "+exception.getMessage());
						return result;
					}
					result = "OK";
					return result;
		}else{
			return result;
		}
		
	}
	
	//methods for reminder (scadenzario)
	public Reminder findReminder(String reminder_id,String archive_name){
		Assert.notNull(reminder_id);
		Reminder result = null;
		Connection database_connection = null;
				
		try
		{
			database_connection = new OjdbcConnection().connectToSqlite(archive_name);
		}
		catch(SQLException exception)
		{
			System.out.println("Failed to connect to the database"+exception.getMessage());
			return result;
		}
		
		String query = "select * from reminder where reminder_id = '" + reminder_id + "' order by date asc";
		Statement statement = null;
		try
		{
			statement = database_connection.createStatement();
		}
		catch(SQLException exception)
		{
			System.out.println("Failed to create statement"+exception.getMessage());
			return result;
		}
		
		try
		{
			ResultSet resultSet = statement.executeQuery(query);

			if(resultSet.next())
			{
				//marrim te dhenat
				Reminder pergjigje = new Reminder();
				pergjigje.setReminderId(resultSet.getString("reminder_id"));
				pergjigje.setValue(resultSet.getString("value"));
				pergjigje.setIdContact(resultSet.getString("id_contact"));
				pergjigje.setDescription(resultSet.getString("description"));
				pergjigje.setDate(resultSet.getString("date"));
				pergjigje.setCompleted(resultSet.getString("completed"));
				result = pergjigje;
			}
			else
			{
				System.out.println("No information found in database");
				return result;
			}

			statement.close();
			database_connection.close();
			System.out.println("Successfuly");
			return result;
		}
		catch(SQLException exception)
		{
			System.out.println("Failed to excecute statement"+exception.getMessage());
			return result;
		}
    }
		
	public List<Reminder> findAllReminders(String archive_name) {
		//Assert.notNull(name);
		List<Reminder> allReminders = new ArrayList<Reminder>();
		Connection database_connection = null;
				
		try
		{
			database_connection = new OjdbcConnection().connectToSqlite(archive_name);
		}
		catch(SQLException exception)
		{
			System.out.println("Failed to connect to the database"+exception.getMessage());
			allReminders = null;
			return allReminders;
		}
		
		String query = "select * from reminder order by date asc";
		Statement statement = null;
		try
		{
			statement = database_connection.createStatement();
		}
		catch(SQLException exception)
		{
			System.out.println("Failed to create statement"+exception.getMessage());
			allReminders = null;
			return allReminders;
		}
		
		try
		{
			ResultSet resultSet = statement.executeQuery(query);
			boolean hasNext = false;
			while(resultSet.next())
			{
				hasNext = true;
				Reminder pergjigje = new Reminder();
				pergjigje.setReminderId(resultSet.getString("reminder_id"));
				pergjigje.setValue(resultSet.getString("value"));
				pergjigje.setIdContact(resultSet.getString("id_contact"));
				pergjigje.setDescription(resultSet.getString("description"));
				pergjigje.setDate(resultSet.getString("date"));
				pergjigje.setCompleted(resultSet.getString("completed"));
				allReminders.add(pergjigje);
			}
			
			
			if(hasNext == false)
			{
				System.out.println("No information found in database");
				allReminders = null;
				return allReminders;
			}			

			statement.close();
			database_connection.close();

			return allReminders;
		}
		catch(SQLException exception)
		{
			System.out.println("Failed to excecute statement"+exception.getMessage());
			allReminders = null;
			return allReminders;
		}
		
	}
	
	public List<Reminder> findAllRemindersByContact(String archive_name, String id_contact) {
		//Assert.notNull(name);
		List<Reminder> allReminders = new ArrayList<Reminder>();
		Connection database_connection = null;
		try
		{
			database_connection = new OjdbcConnection().connectToSqlite(archive_name);
		}
		catch(SQLException exception)
		{
			System.out.println("Failed to connect to the database"+exception.getMessage());
			allReminders = null;
			return allReminders;
		}
		
		String query = "select * from reminder WHERE id_contact = '"+id_contact+"' order by date asc";
		Statement statement = null;
		try
		{
			statement = database_connection.createStatement();
		}
		catch(SQLException exception)
		{
			System.out.println("Failed to create statement"+exception.getMessage());
			allReminders = null;
			return allReminders;
		}
		
		try
		{
			ResultSet resultSet = statement.executeQuery(query);
			boolean hasNext = false;
			while(resultSet.next())
			{
				hasNext = true;
				Reminder pergjigje = new Reminder();
				pergjigje.setReminderId(resultSet.getString("reminder_id"));
				pergjigje.setValue(resultSet.getString("value"));
				pergjigje.setIdContact(resultSet.getString("id_contact"));
				pergjigje.setDescription(resultSet.getString("description"));
				pergjigje.setDate(resultSet.getString("date"));
				pergjigje.setCompleted(resultSet.getString("completed"));
				allReminders.add(pergjigje);
			}
			
			
			if(hasNext == false)
			{
				System.out.println("No information found in database");
				allReminders = null;
				return allReminders;
			}			

			statement.close();
			database_connection.close();

			return allReminders;
		}
		catch(SQLException exception)
		{
			System.out.println("Failed to excecute statement"+exception.getMessage());
			allReminders = null;
			return allReminders;
		}
		
	}
	
	public List<Reminder> findAllRemindersLikeDescription(String archive_name, String description) {
		//Assert.notNull(name);
		List<Reminder> allReminders = new ArrayList<Reminder>();
		Connection database_connection = null;
		try
		{
			database_connection = new OjdbcConnection().connectToSqlite(archive_name);
		}
		catch(SQLException exception)
		{
			System.out.println("Failed to connect to the database"+exception.getMessage());
			allReminders = null;
			return allReminders;
		}
		
		String query = "select * from reminder WHERE description like '%"+description+"%' order by date asc";
		Statement statement = null;
		try
		{
			statement = database_connection.createStatement();
		}
		catch(SQLException exception)
		{
			System.out.println("Failed to create statement"+exception.getMessage());
			allReminders = null;
			return allReminders;
		}
		
		try
		{
			ResultSet resultSet = statement.executeQuery(query);
			boolean hasNext = false;
			while(resultSet.next())
			{
				hasNext = true;
				Reminder pergjigje = new Reminder();
				pergjigje.setReminderId(resultSet.getString("reminder_id"));
				pergjigje.setValue(resultSet.getString("value"));
				pergjigje.setIdContact(resultSet.getString("id_contact"));
				pergjigje.setDescription(resultSet.getString("description"));
				pergjigje.setDate(resultSet.getString("date"));
				pergjigje.setCompleted(resultSet.getString("completed"));
				allReminders.add(pergjigje);
			}
			
			
			if(hasNext == false)
			{
				System.out.println("No information found in database");
				allReminders = null;
				return allReminders;
			}			

			statement.close();
			database_connection.close();

			return allReminders;
		}
		catch(SQLException exception)
		{
			System.out.println("Failed to excecute statement"+exception.getMessage());
			allReminders = null;
			return allReminders;
		}
		
	}
	
	public List<Reminder> findAllRemindersBeforeDate(String archive_name, String date) {
		//Assert.notNull(name);
		List<Reminder> allReminders = new ArrayList<Reminder>();
		Connection database_connection = null;
		try
		{
			database_connection = new OjdbcConnection().connectToSqlite(archive_name);
		}
		catch(SQLException exception)
		{
			System.out.println("Failed to connect to the database"+exception.getMessage());
			allReminders = null;
			return allReminders;
		}
		
		String query = "select * from reminder where date < '"+date+"' order by date asc";
		Statement statement = null;
		try
		{
			statement = database_connection.createStatement();
		}
		catch(SQLException exception)
		{
			System.out.println("Failed to create statement"+exception.getMessage());
			allReminders = null;
			return allReminders;
		}
		
		try
		{
			ResultSet resultSet = statement.executeQuery(query);
			boolean hasNext = false;
			while(resultSet.next())
			{
				hasNext = true;
				Reminder pergjigje = new Reminder();
				pergjigje.setReminderId(resultSet.getString("reminder_id"));
				pergjigje.setValue(resultSet.getString("value"));
				pergjigje.setIdContact(resultSet.getString("id_contact"));
				pergjigje.setDescription(resultSet.getString("description"));
				pergjigje.setDate(resultSet.getString("date"));
				pergjigje.setCompleted(resultSet.getString("completed"));
				allReminders.add(pergjigje);
			}
			
			
			if(hasNext == false)
			{
				System.out.println("No information found in database");
				allReminders = null;
				return allReminders;
			}			

			statement.close();
			database_connection.close();

			return allReminders;
		}
		catch(SQLException exception)
		{
			System.out.println("Failed to excecute statement"+exception.getMessage());
			allReminders = null;
			return allReminders;
		}
		
	}
	
	public List<Reminder> findAllRemindersAfterDate(String archive_name, String date) {
		//Assert.notNull(name);
		List<Reminder> allReminders = new ArrayList<Reminder>();
		Connection database_connection = null;
		try
		{
			database_connection = new OjdbcConnection().connectToSqlite(archive_name);
		}
		catch(SQLException exception)
		{
			System.out.println("Failed to connect to the database"+exception.getMessage());
			allReminders = null;
			return allReminders;
		}
		
		String query = "select * from reminder where date > '"+date+"' order by date asc";
		Statement statement = null;
		try
		{
			statement = database_connection.createStatement();
		}
		catch(SQLException exception)
		{
			System.out.println("Failed to create statement"+exception.getMessage());
			allReminders = null;
			return allReminders;
		}
		
		try
		{
			ResultSet resultSet = statement.executeQuery(query);
			boolean hasNext = false;
			while(resultSet.next())
			{
				hasNext = true;
				Reminder pergjigje = new Reminder();
				pergjigje.setReminderId(resultSet.getString("reminder_id"));
				pergjigje.setValue(resultSet.getString("value"));
				pergjigje.setIdContact(resultSet.getString("id_contact"));
				pergjigje.setDescription(resultSet.getString("description"));
				pergjigje.setDate(resultSet.getString("date"));
				pergjigje.setCompleted(resultSet.getString("completed"));
				allReminders.add(pergjigje);
			}
			
			
			if(hasNext == false)
			{
				System.out.println("No information found in database");
				allReminders = null;
				return allReminders;
			}			

			statement.close();
			database_connection.close();

			return allReminders;
		}
		catch(SQLException exception)
		{
			System.out.println("Failed to excecute statement"+exception.getMessage());
			allReminders = null;
			return allReminders;
		}
		
	}
	
	public List<Reminder> findAllRemindersBetweenDates(String archive_name, String startDate, String endDate) {
		//Assert.notNull(name);
		List<Reminder> allReminders = new ArrayList<Reminder>();
		Connection database_connection = null;
		try
		{
			database_connection = new OjdbcConnection().connectToSqlite(archive_name);
		}
		catch(SQLException exception)
		{
			System.out.println("Failed to connect to the database"+exception.getMessage());
			allReminders = null;
			return allReminders;
		}
		
		String query = "select * from reminder where date BETWEEN '"+startDate+"' AND '"+endDate+"' order by date asc";
		Statement statement = null;
		try
		{
			statement = database_connection.createStatement();
		}
		catch(SQLException exception)
		{
			System.out.println("Failed to create statement"+exception.getMessage());
			allReminders = null;
			return allReminders;
		}
		
		try
		{
			ResultSet resultSet = statement.executeQuery(query);
			boolean hasNext = false;
			while(resultSet.next())
			{
				hasNext = true;
				Reminder pergjigje = new Reminder();
				pergjigje.setReminderId(resultSet.getString("reminder_id"));
				pergjigje.setValue(resultSet.getString("value"));
				pergjigje.setIdContact(resultSet.getString("id_contact"));
				pergjigje.setDescription(resultSet.getString("description"));
				pergjigje.setDate(resultSet.getString("date"));
				pergjigje.setCompleted(resultSet.getString("completed"));
				allReminders.add(pergjigje);
			}
			
			
			if(hasNext == false)
			{
				System.out.println("No information found in database");
				allReminders = null;
				return allReminders;
			}			

			statement.close();
			database_connection.close();

			return allReminders;
		}
		catch(SQLException exception)
		{
			System.out.println("Failed to excecute statement"+exception.getMessage());
			allReminders = null;
			return allReminders;
		}
		
	}
	
	public List<Reminder> findAllRemindersCustom(String archive_name, String queryClient) {
		//Assert.notNull(name);
		List<Reminder> allReminders = new ArrayList<Reminder>();
		Connection database_connection = null;
		try
		{
			database_connection = new OjdbcConnection().connectToSqlite(archive_name);
		}
		catch(SQLException exception)
		{
			System.out.println("Failed to connect to the database"+exception.getMessage());
			allReminders = null;
			return allReminders;
		}
		
		String query = queryClient;
		Statement statement = null;
		try
		{
			statement = database_connection.createStatement();
		}
		catch(SQLException exception)
		{
			System.out.println("Failed to create statement"+exception.getMessage());
			allReminders = null;
			return allReminders;
		}
		
		try
		{
			ResultSet resultSet = statement.executeQuery(query);
			boolean hasNext = false;
			while(resultSet.next())
			{
				hasNext = true;
				Reminder pergjigje = new Reminder();
				pergjigje.setReminderId(resultSet.getString("reminder_id"));
				pergjigje.setValue(resultSet.getString("value"));
				pergjigje.setIdContact(resultSet.getString("id_contact"));
				pergjigje.setDescription(resultSet.getString("description"));
				pergjigje.setDate(resultSet.getString("date"));
				pergjigje.setCompleted(resultSet.getString("completed"));
				allReminders.add(pergjigje);
			}
			
			
			if(hasNext == false)
			{
				System.out.println("No information found in database");
				allReminders = null;
				return allReminders;
			}			

			statement.close();
			database_connection.close();

			return allReminders;
		}
		catch(SQLException exception)
		{
			System.out.println("Failed to excecute statement"+exception.getMessage());
			allReminders = null;
			return allReminders;
		}
		
	}
	

	public String addReminder(Reminder reminder,String archive_name){
		String result = "ERROR";
		//kontrollojme vlerat
		if (reminder.getDate() != ""){							
				Connection database_connection = null;				
				try
				{
					database_connection = new OjdbcConnection().connectToSqlite(archive_name);
				}
				catch(SQLException exception)
				{
					System.out.println("Failed to connect to the database"+exception.getMessage());
					return result;
				}				
				try
				{
					String sql = "INSERT INTO reminder (date,description,id_contact,value,completed)  "
							+ "VALUES(?,?,?,?,?)";
					PreparedStatement pstmt = database_connection.prepareStatement(sql);
					pstmt.setString(1,reminder.getDate());
					pstmt.setString(2,reminder.getDescription());
					pstmt.setString(3,reminder.getIdContact());
					pstmt.setString(4,reminder.getValue());
					pstmt.setString(5,reminder.getCompleted());
					pstmt.executeUpdate();					  
				}catch(SQLException exception)
				{
					System.out.println("Failed to execute query. Exception: "+exception.getMessage());
					return result;
				}
				result = "OK";
				return result;
		}else{
			//entry date empty
			return result;
		}		
	}
	
	public String deleteReminder(String reminder_id,String archive_name){
		String result = "ERROR";
		//kontrollojme vlerat
		if(findReminder(reminder_id,archive_name) != null){
			Connection database_connection = null;
			try
			{
				database_connection = new OjdbcConnection().connectToSqlite(archive_name);
			}
			catch(SQLException exception)
			{
				System.out.println("Failed to connect to the database"+exception.getMessage());
				return result;
			}
			try{
				String sql = "DELETE FROM reminder where reminder_id = ?";
				PreparedStatement pstmt = database_connection.prepareStatement(sql);						
				pstmt.setString(1,reminder_id);					        
				pstmt.executeUpdate();
			}catch(SQLException exception)
			{
				System.out.println("Failed to execute query. Exception: "+exception.getMessage());
				return result;
			}
			result = "OK";
			return result;
		}else{
			//nuk gjendet reminder_id ne db
			return result;
		}
		
	}
	
	public String updateReminder(Reminder reminder, String archive_name){
		String result = "ERROR";
		//kontrollojme id
		if (findReminder(reminder.getReminderId(), archive_name) != null){
					Connection database_connection = null;
					try
					{
						database_connection = new OjdbcConnection().connectToSqlite(archive_name);
					}
					catch(SQLException exception)
					{
						System.out.println("Failed to connect to the database"+exception.getMessage());
						return result;
					}
					try{
						String sql = " UPDATE reminder SET date = ?, description = ?, id_contact = ?, "
								+ " value = ?, completed = ? "
								+ "WHERE reminder_id = ? ";
						PreparedStatement pstmt = database_connection.prepareStatement(sql);						
						pstmt.setString(1,reminder.getDate());					        
						pstmt.setString(2,reminder.getDescription());
						pstmt.setString(3,reminder.getIdContact());
						pstmt.setString(4,reminder.getValue());
						pstmt.setString(5,reminder.getCompleted());
						pstmt.setString(6,reminder.getReminderId());
						pstmt.executeUpdate();
					}catch(SQLException exception)
					{
						System.out.println("Failed to execute query. Exception: "+exception.getMessage());
						return result;
					}
					result = "OK";
					return result;
		}else{
			return result;
		}
		
	}
	
	/* methods for sqlitesequence won't be used. sqliteSequence is created automatically from sqlite engine to keep track of 
	 * autoincrement fields of diffferent tables.
	public SqliteSequence findSqliteSequence(String name,String archive_name){
		Assert.notNull(name);
		SqliteSequence result = null;
		Connection database_connection = null;
				
		try
		{
			database_connection = new OjdbcConnection().connectToSqlite(archive_name);
		}
		catch(SQLException exception)
		{
			System.out.println("Failed to connect to the database"+exception.getMessage());
			return result;
		}
		
		String query = "select * from sqlite_sequence where name = '" + name + "'";
		Statement statement = null;
		try
		{
			statement = database_connection.createStatement();
		}
		catch(SQLException exception)
		{
			System.out.println("Failed to create statement"+exception.getMessage());
			return result;
		}
		
		try
		{
			ResultSet resultSet = statement.executeQuery(query);

			if(resultSet.next())
			{
				SqliteSequence pergjigje = new SqliteSequence();
				pergjigje.setName(resultSet.getString("name"));
				pergjigje.setSeq(resultSet.getString("seq"));
				result = pergjigje;
			}
			else
			{
				System.out.println("No information found in database");
				return result;
			}

			statement.close();
			database_connection.close();
			System.out.println("Successfuly");
			return result;
		}
		catch(SQLException exception)
		{
			System.out.println("Failed to excecute statement"+exception.getMessage());
			return result;
		}
    }
	
	public List<SqliteSequence> findAllSqliteSequencies(String archive_name) {
		//Assert.notNull(name);
		List<SqliteSequence> allSqliteSequencies = new ArrayList<SqliteSequence>();
		Connection database_connection = null;
				
		try
		{
			database_connection = new OjdbcConnection().connectToSqlite(archive_name);
		}
		catch(SQLException exception)
		{
			System.out.println("Failed to connect to the database"+exception.getMessage());
			allSqliteSequencies = null;
			return allSqliteSequencies;
		}
		
		String query = "select * from sqlite_sequence";
		Statement statement = null;
		try
		{
			statement = database_connection.createStatement();
		}
		catch(SQLException exception)
		{
			System.out.println("Failed to create statement"+exception.getMessage());
			allSqliteSequencies = null;
			return allSqliteSequencies;
		}
		
		try
		{
			ResultSet resultSet = statement.executeQuery(query);
			boolean hasNext = false;
			while(resultSet.next())
			{
				hasNext = true;
				SqliteSequence pergjigje = new SqliteSequence();
				pergjigje.setName(resultSet.getString("name"));
				pergjigje.setSeq(resultSet.getString("seq"));
				allSqliteSequencies.add(pergjigje);
			}
			
			
			if(hasNext == false)
			{
				System.out.println("No information found in database");
				allSqliteSequencies = null;
				return allSqliteSequencies;
			}			

			statement.close();
			database_connection.close();

			return allSqliteSequencies;
		}
		catch(SQLException exception)
		{
			System.out.println("Failed to excecute statement"+exception.getMessage());
			allSqliteSequencies = null;
			return allSqliteSequencies;
		}
		
	}
	
	public String addSqliteSequence(SqliteSequence sqliteSequence, String archive_name){
		String result = "ERROR";
		//kontrollojme vlerat
		if (sqliteSequence.getName() != ""){							
				Connection database_connection = null;				
				try
				{
					database_connection = new OjdbcConnection().connectToSqlite(archive_name);
				}
				catch(SQLException exception)
				{
					System.out.println("Failed to connect to the database"+exception.getMessage());
					return result;
				}				
				try
				{
					String sql = "INSERT INTO sqlite_sequence (name,seq)  "
							+ "VALUES(?,?)";
					PreparedStatement pstmt = database_connection.prepareStatement(sql);
					pstmt.setString(1,sqliteSequence.getName());
					pstmt.setString(2,sqliteSequence.getSeq());
					pstmt.executeUpdate();					  
				}catch(SQLException exception)
				{
					System.out.println("Failed to execute query. Exception: "+exception.getMessage());
					return result;
				}
				result = "OK";
				return result;
		}else{
			//entry date empty
			return result;
		}		
	}

	public String deleteSqliteSequence(String name, String archive_name){
		String result = "ERROR";
		//kontrollojme vlerat
		if(findSqliteSequence(name,archive_name) != null){
			Connection database_connection = null;
			try
			{
				database_connection = new OjdbcConnection().connectToSqlite(archive_name);
			}
			catch(SQLException exception)
			{
				System.out.println("Failed to connect to the database"+exception.getMessage());
				return result;
			}
			try{
				String sql = "DELETE FROM sqlite_sequence where name = ?";
				PreparedStatement pstmt = database_connection.prepareStatement(sql);						
				pstmt.setString(1,name);					        
				pstmt.executeUpdate();
			}catch(SQLException exception)
			{
				System.out.println("Failed to execute query. Exception: "+exception.getMessage());
				return result;
			}
			result = "OK";
			return result;
		}else{
			//nuk gjendet archive_id ne db
			return result;
		}
		
	}

	public String updateSqliteSequence(SqliteSequence SqliteSequence, String archive_name){
		String result = "ERROR";
		//kontrollojme id
		if (findOperationType(SqliteSequence.getName(),archive_name) != null){
					Connection database_connection = null;
					try
					{
						database_connection = new OjdbcConnection().connectToSqlite(archive_name);
					}
					catch(SQLException exception)
					{
						System.out.println("Failed to connect to the database"+exception.getMessage());
						return result;
					}
					try{
						String sql = " UPDATE sqlite_sequence SET seq = ? "
								+ "WHERE name = ? ";
						PreparedStatement pstmt = database_connection.prepareStatement(sql);						
						pstmt.setString(1,SqliteSequence.getSeq());					        
						pstmt.setString(2,SqliteSequence.getName());
						pstmt.executeUpdate();
					}catch(SQLException exception)
					{
						System.out.println("Failed to execute query. Exception: "+exception.getMessage());
						return result;
					}
					result = "OK";
					return result;
		}else{
			return result;
		}
		
	}*/
}

    


