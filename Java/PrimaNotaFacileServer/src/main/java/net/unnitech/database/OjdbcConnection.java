package net.unnitech.database;


import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.SQLException;

public class OjdbcConnection
{
	public Connection connectToSqlite(String databaseName) throws SQLException 
	{
		 try {
	            Class.forName("org.sqlite.JDBC");
	            String dbURL = "jdbc:sqlite:"+databaseName;
	            Connection conn = DriverManager.getConnection(dbURL);
	            if (conn != null) {
	                System.out.println("Connected to the database");
	                DatabaseMetaData dm = (DatabaseMetaData) conn.getMetaData();
	                System.out.println("Driver name: " + dm.getDriverName());
	                System.out.println("Driver version: " + dm.getDriverVersion());
	                System.out.println("Product name: " + dm.getDatabaseProductName());
	                System.out.println("Product version: " + dm.getDatabaseProductVersion());
	                
	            }

                return conn;
	        } catch (ClassNotFoundException ex) {
	            ex.printStackTrace();
	            return null;
	        } catch (SQLException ex) {
	            ex.printStackTrace();
	            return null;
	        }
			
	}
}
