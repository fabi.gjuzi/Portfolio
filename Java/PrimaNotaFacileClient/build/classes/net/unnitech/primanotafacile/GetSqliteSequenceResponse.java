
package net.unnitech.primanotafacile;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="sqlite_sequence" type="{http://unnitech.net/primanotafacile}sqlite_sequence"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "sqliteSequence"
})
@XmlRootElement(name = "getSqliteSequenceResponse")
public class GetSqliteSequenceResponse {

    @XmlElement(name = "sqlite_sequence", required = true)
    protected SqliteSequence sqliteSequence;

    /**
     * Gets the value of the sqliteSequence property.
     * 
     * @return
     *     possible object is
     *     {@link SqliteSequence }
     *     
     */
    public SqliteSequence getSqliteSequence() {
        return sqliteSequence;
    }

    /**
     * Sets the value of the sqliteSequence property.
     * 
     * @param value
     *     allowed object is
     *     {@link SqliteSequence }
     *     
     */
    public void setSqliteSequence(SqliteSequence value) {
        this.sqliteSequence = value;
    }

}
