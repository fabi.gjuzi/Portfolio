
package net.unnitech.primanotafacile;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the net.unnitech.primanotafacile package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: net.unnitech.primanotafacile
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link AddContactResponse }
     * 
     */
    public AddContactResponse createAddContactResponse() {
        return new AddContactResponse();
    }

    /**
     * Create an instance of {@link GetAllCompaniesCustomRequest }
     * 
     */
    public GetAllCompaniesCustomRequest createGetAllCompaniesCustomRequest() {
        return new GetAllCompaniesCustomRequest();
    }

    /**
     * Create an instance of {@link DeleteArchiveRequest }
     * 
     */
    public DeleteArchiveRequest createDeleteArchiveRequest() {
        return new DeleteArchiveRequest();
    }

    /**
     * Create an instance of {@link UpdateEntryRequest }
     * 
     */
    public UpdateEntryRequest createUpdateEntryRequest() {
        return new UpdateEntryRequest();
    }

    /**
     * Create an instance of {@link Entry }
     * 
     */
    public Entry createEntry() {
        return new Entry();
    }

    /**
     * Create an instance of {@link GetOperationTypeResponse }
     * 
     */
    public GetOperationTypeResponse createGetOperationTypeResponse() {
        return new GetOperationTypeResponse();
    }

    /**
     * Create an instance of {@link OperationType }
     * 
     */
    public OperationType createOperationType() {
        return new OperationType();
    }

    /**
     * Create an instance of {@link GetAllRemindersLikeDescriptionRequest }
     * 
     */
    public GetAllRemindersLikeDescriptionRequest createGetAllRemindersLikeDescriptionRequest() {
        return new GetAllRemindersLikeDescriptionRequest();
    }

    /**
     * Create an instance of {@link AddReminderRequest }
     * 
     */
    public AddReminderRequest createAddReminderRequest() {
        return new AddReminderRequest();
    }

    /**
     * Create an instance of {@link Reminder }
     * 
     */
    public Reminder createReminder() {
        return new Reminder();
    }

    /**
     * Create an instance of {@link GetAllRemindersBeforeDateRequest }
     * 
     */
    public GetAllRemindersBeforeDateRequest createGetAllRemindersBeforeDateRequest() {
        return new GetAllRemindersBeforeDateRequest();
    }

    /**
     * Create an instance of {@link GetAllRemindersAfterDateResponse }
     * 
     */
    public GetAllRemindersAfterDateResponse createGetAllRemindersAfterDateResponse() {
        return new GetAllRemindersAfterDateResponse();
    }

    /**
     * Create an instance of {@link GetOperationTypeRequest }
     * 
     */
    public GetOperationTypeRequest createGetOperationTypeRequest() {
        return new GetOperationTypeRequest();
    }

    /**
     * Create an instance of {@link GetAllEntriesPreviousPeriodRequest }
     * 
     */
    public GetAllEntriesPreviousPeriodRequest createGetAllEntriesPreviousPeriodRequest() {
        return new GetAllEntriesPreviousPeriodRequest();
    }

    /**
     * Create an instance of {@link AddOperationTypeRequest }
     * 
     */
    public AddOperationTypeRequest createAddOperationTypeRequest() {
        return new AddOperationTypeRequest();
    }

    /**
     * Create an instance of {@link DeleteSqliteSequenceResponse }
     * 
     */
    public DeleteSqliteSequenceResponse createDeleteSqliteSequenceResponse() {
        return new DeleteSqliteSequenceResponse();
    }

    /**
     * Create an instance of {@link GetBanksLikeNameResponse }
     * 
     */
    public GetBanksLikeNameResponse createGetBanksLikeNameResponse() {
        return new GetBanksLikeNameResponse();
    }

    /**
     * Create an instance of {@link Bank }
     * 
     */
    public Bank createBank() {
        return new Bank();
    }

    /**
     * Create an instance of {@link CopyArchiveResponse }
     * 
     */
    public CopyArchiveResponse createCopyArchiveResponse() {
        return new CopyArchiveResponse();
    }

    /**
     * Create an instance of {@link AddBankRequest }
     * 
     */
    public AddBankRequest createAddBankRequest() {
        return new AddBankRequest();
    }

    /**
     * Create an instance of {@link DeleteEntryRequest }
     * 
     */
    public DeleteEntryRequest createDeleteEntryRequest() {
        return new DeleteEntryRequest();
    }

    /**
     * Create an instance of {@link GetEntriesAfterDateRequest }
     * 
     */
    public GetEntriesAfterDateRequest createGetEntriesAfterDateRequest() {
        return new GetEntriesAfterDateRequest();
    }

    /**
     * Create an instance of {@link GetAllBankResponse }
     * 
     */
    public GetAllBankResponse createGetAllBankResponse() {
        return new GetAllBankResponse();
    }

    /**
     * Create an instance of {@link AddCompanyResponse }
     * 
     */
    public AddCompanyResponse createAddCompanyResponse() {
        return new AddCompanyResponse();
    }

    /**
     * Create an instance of {@link AddSqliteSequenceResponse }
     * 
     */
    public AddSqliteSequenceResponse createAddSqliteSequenceResponse() {
        return new AddSqliteSequenceResponse();
    }

    /**
     * Create an instance of {@link GetAllContactsRequest }
     * 
     */
    public GetAllContactsRequest createGetAllContactsRequest() {
        return new GetAllContactsRequest();
    }

    /**
     * Create an instance of {@link DeleteReminderRequest }
     * 
     */
    public DeleteReminderRequest createDeleteReminderRequest() {
        return new DeleteReminderRequest();
    }

    /**
     * Create an instance of {@link UpdateOperationTypeRequest }
     * 
     */
    public UpdateOperationTypeRequest createUpdateOperationTypeRequest() {
        return new UpdateOperationTypeRequest();
    }

    /**
     * Create an instance of {@link AddEntryRequest }
     * 
     */
    public AddEntryRequest createAddEntryRequest() {
        return new AddEntryRequest();
    }

    /**
     * Create an instance of {@link AddEntryResponse }
     * 
     */
    public AddEntryResponse createAddEntryResponse() {
        return new AddEntryResponse();
    }

    /**
     * Create an instance of {@link AddArchiveRequest }
     * 
     */
    public AddArchiveRequest createAddArchiveRequest() {
        return new AddArchiveRequest();
    }

    /**
     * Create an instance of {@link Archive }
     * 
     */
    public Archive createArchive() {
        return new Archive();
    }

    /**
     * Create an instance of {@link GetEntryRequest }
     * 
     */
    public GetEntryRequest createGetEntryRequest() {
        return new GetEntryRequest();
    }

    /**
     * Create an instance of {@link GetAllSqliteSequenciesRequest }
     * 
     */
    public GetAllSqliteSequenciesRequest createGetAllSqliteSequenciesRequest() {
        return new GetAllSqliteSequenciesRequest();
    }

    /**
     * Create an instance of {@link UpdateCompanyRequest }
     * 
     */
    public UpdateCompanyRequest createUpdateCompanyRequest() {
        return new UpdateCompanyRequest();
    }

    /**
     * Create an instance of {@link Company }
     * 
     */
    public Company createCompany() {
        return new Company();
    }

    /**
     * Create an instance of {@link GetBankRequest }
     * 
     */
    public GetBankRequest createGetBankRequest() {
        return new GetBankRequest();
    }

    /**
     * Create an instance of {@link GetAllArchiveRequest }
     * 
     */
    public GetAllArchiveRequest createGetAllArchiveRequest() {
        return new GetAllArchiveRequest();
    }

    /**
     * Create an instance of {@link GetEntriesByBankResponse }
     * 
     */
    public GetEntriesByBankResponse createGetEntriesByBankResponse() {
        return new GetEntriesByBankResponse();
    }

    /**
     * Create an instance of {@link UpdateArchiveRequest }
     * 
     */
    public UpdateArchiveRequest createUpdateArchiveRequest() {
        return new UpdateArchiveRequest();
    }

    /**
     * Create an instance of {@link GetBankResponse }
     * 
     */
    public GetBankResponse createGetBankResponse() {
        return new GetBankResponse();
    }

    /**
     * Create an instance of {@link GetAllContactsCustomRequest }
     * 
     */
    public GetAllContactsCustomRequest createGetAllContactsCustomRequest() {
        return new GetAllContactsCustomRequest();
    }

    /**
     * Create an instance of {@link GetEntriesBeforeDateResponse }
     * 
     */
    public GetEntriesBeforeDateResponse createGetEntriesBeforeDateResponse() {
        return new GetEntriesBeforeDateResponse();
    }

    /**
     * Create an instance of {@link GetCompanyRequest }
     * 
     */
    public GetCompanyRequest createGetCompanyRequest() {
        return new GetCompanyRequest();
    }

    /**
     * Create an instance of {@link UpdateContactRequest }
     * 
     */
    public UpdateContactRequest createUpdateContactRequest() {
        return new UpdateContactRequest();
    }

    /**
     * Create an instance of {@link Contact }
     * 
     */
    public Contact createContact() {
        return new Contact();
    }

    /**
     * Create an instance of {@link GetAllEntriesCustomResponse }
     * 
     */
    public GetAllEntriesCustomResponse createGetAllEntriesCustomResponse() {
        return new GetAllEntriesCustomResponse();
    }

    /**
     * Create an instance of {@link EntryBalance }
     * 
     */
    public EntryBalance createEntryBalance() {
        return new EntryBalance();
    }

    /**
     * Create an instance of {@link DeleteBankRequest }
     * 
     */
    public DeleteBankRequest createDeleteBankRequest() {
        return new DeleteBankRequest();
    }

    /**
     * Create an instance of {@link GetAllSqliteSequenciesResponse }
     * 
     */
    public GetAllSqliteSequenciesResponse createGetAllSqliteSequenciesResponse() {
        return new GetAllSqliteSequenciesResponse();
    }

    /**
     * Create an instance of {@link SqliteSequence }
     * 
     */
    public SqliteSequence createSqliteSequence() {
        return new SqliteSequence();
    }

    /**
     * Create an instance of {@link GetBanksByArchiveResponse }
     * 
     */
    public GetBanksByArchiveResponse createGetBanksByArchiveResponse() {
        return new GetBanksByArchiveResponse();
    }

    /**
     * Create an instance of {@link GetEntriesByDateResponse }
     * 
     */
    public GetEntriesByDateResponse createGetEntriesByDateResponse() {
        return new GetEntriesByDateResponse();
    }

    /**
     * Create an instance of {@link GetBanksLikeNameRequest }
     * 
     */
    public GetBanksLikeNameRequest createGetBanksLikeNameRequest() {
        return new GetBanksLikeNameRequest();
    }

    /**
     * Create an instance of {@link GetAllContactsCustomResponse }
     * 
     */
    public GetAllContactsCustomResponse createGetAllContactsCustomResponse() {
        return new GetAllContactsCustomResponse();
    }

    /**
     * Create an instance of {@link GetAllOperationTypesCustomResponse }
     * 
     */
    public GetAllOperationTypesCustomResponse createGetAllOperationTypesCustomResponse() {
        return new GetAllOperationTypesCustomResponse();
    }

    /**
     * Create an instance of {@link UpdateBankRequest }
     * 
     */
    public UpdateBankRequest createUpdateBankRequest() {
        return new UpdateBankRequest();
    }

    /**
     * Create an instance of {@link UpdateOperationTypeResponse }
     * 
     */
    public UpdateOperationTypeResponse createUpdateOperationTypeResponse() {
        return new UpdateOperationTypeResponse();
    }

    /**
     * Create an instance of {@link DeleteOperationTypeResponse }
     * 
     */
    public DeleteOperationTypeResponse createDeleteOperationTypeResponse() {
        return new DeleteOperationTypeResponse();
    }

    /**
     * Create an instance of {@link GetAllOperationTypesCustomRequest }
     * 
     */
    public GetAllOperationTypesCustomRequest createGetAllOperationTypesCustomRequest() {
        return new GetAllOperationTypesCustomRequest();
    }

    /**
     * Create an instance of {@link GetCompaniesLikeNameResponse }
     * 
     */
    public GetCompaniesLikeNameResponse createGetCompaniesLikeNameResponse() {
        return new GetCompaniesLikeNameResponse();
    }

    /**
     * Create an instance of {@link GetAllArchiveResponse }
     * 
     */
    public GetAllArchiveResponse createGetAllArchiveResponse() {
        return new GetAllArchiveResponse();
    }

    /**
     * Create an instance of {@link AddArchiveResponse }
     * 
     */
    public AddArchiveResponse createAddArchiveResponse() {
        return new AddArchiveResponse();
    }

    /**
     * Create an instance of {@link GetBanksByArchiveRequest }
     * 
     */
    public GetBanksByArchiveRequest createGetBanksByArchiveRequest() {
        return new GetBanksByArchiveRequest();
    }

    /**
     * Create an instance of {@link UpdateContactResponse }
     * 
     */
    public UpdateContactResponse createUpdateContactResponse() {
        return new UpdateContactResponse();
    }

    /**
     * Create an instance of {@link GetCompaniesLikeNameRequest }
     * 
     */
    public GetCompaniesLikeNameRequest createGetCompaniesLikeNameRequest() {
        return new GetCompaniesLikeNameRequest();
    }

    /**
     * Create an instance of {@link GetEntriesBetweenDatesRequest }
     * 
     */
    public GetEntriesBetweenDatesRequest createGetEntriesBetweenDatesRequest() {
        return new GetEntriesBetweenDatesRequest();
    }

    /**
     * Create an instance of {@link GetReminderResponse }
     * 
     */
    public GetReminderResponse createGetReminderResponse() {
        return new GetReminderResponse();
    }

    /**
     * Create an instance of {@link DeleteCompanyResponse }
     * 
     */
    public DeleteCompanyResponse createDeleteCompanyResponse() {
        return new DeleteCompanyResponse();
    }

    /**
     * Create an instance of {@link GetAllContactsResponse }
     * 
     */
    public GetAllContactsResponse createGetAllContactsResponse() {
        return new GetAllContactsResponse();
    }

    /**
     * Create an instance of {@link GetAllOperationTypesRequest }
     * 
     */
    public GetAllOperationTypesRequest createGetAllOperationTypesRequest() {
        return new GetAllOperationTypesRequest();
    }

    /**
     * Create an instance of {@link GetSqliteSequenceRequest }
     * 
     */
    public GetSqliteSequenceRequest createGetSqliteSequenceRequest() {
        return new GetSqliteSequenceRequest();
    }

    /**
     * Create an instance of {@link UpdateBankResponse }
     * 
     */
    public UpdateBankResponse createUpdateBankResponse() {
        return new UpdateBankResponse();
    }

    /**
     * Create an instance of {@link AddSqliteSequenceRequest }
     * 
     */
    public AddSqliteSequenceRequest createAddSqliteSequenceRequest() {
        return new AddSqliteSequenceRequest();
    }

    /**
     * Create an instance of {@link UpdateArchiveResponse }
     * 
     */
    public UpdateArchiveResponse createUpdateArchiveResponse() {
        return new UpdateArchiveResponse();
    }

    /**
     * Create an instance of {@link GetReminderRequest }
     * 
     */
    public GetReminderRequest createGetReminderRequest() {
        return new GetReminderRequest();
    }

    /**
     * Create an instance of {@link GetAllRemindersCustomRequest }
     * 
     */
    public GetAllRemindersCustomRequest createGetAllRemindersCustomRequest() {
        return new GetAllRemindersCustomRequest();
    }

    /**
     * Create an instance of {@link DeleteContactRequest }
     * 
     */
    public DeleteContactRequest createDeleteContactRequest() {
        return new DeleteContactRequest();
    }

    /**
     * Create an instance of {@link DeleteBankResponse }
     * 
     */
    public DeleteBankResponse createDeleteBankResponse() {
        return new DeleteBankResponse();
    }

    /**
     * Create an instance of {@link GetAllEntriesPreviousPeriodResponse }
     * 
     */
    public GetAllEntriesPreviousPeriodResponse createGetAllEntriesPreviousPeriodResponse() {
        return new GetAllEntriesPreviousPeriodResponse();
    }

    /**
     * Create an instance of {@link EntryPreviousPeriod }
     * 
     */
    public EntryPreviousPeriod createEntryPreviousPeriod() {
        return new EntryPreviousPeriod();
    }

    /**
     * Create an instance of {@link GetEntriesByBankRequest }
     * 
     */
    public GetEntriesByBankRequest createGetEntriesByBankRequest() {
        return new GetEntriesByBankRequest();
    }

    /**
     * Create an instance of {@link AddCompanyRequest }
     * 
     */
    public AddCompanyRequest createAddCompanyRequest() {
        return new AddCompanyRequest();
    }

    /**
     * Create an instance of {@link GetAllRemindersAfterDateRequest }
     * 
     */
    public GetAllRemindersAfterDateRequest createGetAllRemindersAfterDateRequest() {
        return new GetAllRemindersAfterDateRequest();
    }

    /**
     * Create an instance of {@link AddReminderResponse }
     * 
     */
    public AddReminderResponse createAddReminderResponse() {
        return new AddReminderResponse();
    }

    /**
     * Create an instance of {@link GetAllRemindersBeforeDateResponse }
     * 
     */
    public GetAllRemindersBeforeDateResponse createGetAllRemindersBeforeDateResponse() {
        return new GetAllRemindersBeforeDateResponse();
    }

    /**
     * Create an instance of {@link UpdateCompanyResponse }
     * 
     */
    public UpdateCompanyResponse createUpdateCompanyResponse() {
        return new UpdateCompanyResponse();
    }

    /**
     * Create an instance of {@link GetEntriesByOperationTypeRequest }
     * 
     */
    public GetEntriesByOperationTypeRequest createGetEntriesByOperationTypeRequest() {
        return new GetEntriesByOperationTypeRequest();
    }

    /**
     * Create an instance of {@link GetContactResponse }
     * 
     */
    public GetContactResponse createGetContactResponse() {
        return new GetContactResponse();
    }

    /**
     * Create an instance of {@link GetCompanyResponse }
     * 
     */
    public GetCompanyResponse createGetCompanyResponse() {
        return new GetCompanyResponse();
    }

    /**
     * Create an instance of {@link GetEntriesByDateRequest }
     * 
     */
    public GetEntriesByDateRequest createGetEntriesByDateRequest() {
        return new GetEntriesByDateRequest();
    }

    /**
     * Create an instance of {@link GetAllRemindersResponse }
     * 
     */
    public GetAllRemindersResponse createGetAllRemindersResponse() {
        return new GetAllRemindersResponse();
    }

    /**
     * Create an instance of {@link GetAllRemindersByContactResponse }
     * 
     */
    public GetAllRemindersByContactResponse createGetAllRemindersByContactResponse() {
        return new GetAllRemindersByContactResponse();
    }

    /**
     * Create an instance of {@link CopyArchiveRequest }
     * 
     */
    public CopyArchiveRequest createCopyArchiveRequest() {
        return new CopyArchiveRequest();
    }

    /**
     * Create an instance of {@link UpdateEntryResponse }
     * 
     */
    public UpdateEntryResponse createUpdateEntryResponse() {
        return new UpdateEntryResponse();
    }

    /**
     * Create an instance of {@link UpdateReminderResponse }
     * 
     */
    public UpdateReminderResponse createUpdateReminderResponse() {
        return new UpdateReminderResponse();
    }

    /**
     * Create an instance of {@link GetAllRemindersRequest }
     * 
     */
    public GetAllRemindersRequest createGetAllRemindersRequest() {
        return new GetAllRemindersRequest();
    }

    /**
     * Create an instance of {@link GetArchiveResponse }
     * 
     */
    public GetArchiveResponse createGetArchiveResponse() {
        return new GetArchiveResponse();
    }

    /**
     * Create an instance of {@link DeleteCompanyRequest }
     * 
     */
    public DeleteCompanyRequest createDeleteCompanyRequest() {
        return new DeleteCompanyRequest();
    }

    /**
     * Create an instance of {@link GetContactRequest }
     * 
     */
    public GetContactRequest createGetContactRequest() {
        return new GetContactRequest();
    }

    /**
     * Create an instance of {@link GetEntryResponse }
     * 
     */
    public GetEntryResponse createGetEntryResponse() {
        return new GetEntryResponse();
    }

    /**
     * Create an instance of {@link GetAllRemindersLikeDescriptionResponse }
     * 
     */
    public GetAllRemindersLikeDescriptionResponse createGetAllRemindersLikeDescriptionResponse() {
        return new GetAllRemindersLikeDescriptionResponse();
    }

    /**
     * Create an instance of {@link GetAllRemindersBetweenDatesRequest }
     * 
     */
    public GetAllRemindersBetweenDatesRequest createGetAllRemindersBetweenDatesRequest() {
        return new GetAllRemindersBetweenDatesRequest();
    }

    /**
     * Create an instance of {@link DeleteContactResponse }
     * 
     */
    public DeleteContactResponse createDeleteContactResponse() {
        return new DeleteContactResponse();
    }

    /**
     * Create an instance of {@link GetEntriesByOperationTypeResponse }
     * 
     */
    public GetEntriesByOperationTypeResponse createGetEntriesByOperationTypeResponse() {
        return new GetEntriesByOperationTypeResponse();
    }

    /**
     * Create an instance of {@link GetAllCompaniesCustomResponse }
     * 
     */
    public GetAllCompaniesCustomResponse createGetAllCompaniesCustomResponse() {
        return new GetAllCompaniesCustomResponse();
    }

    /**
     * Create an instance of {@link UpdateSqliteSequenceRequest }
     * 
     */
    public UpdateSqliteSequenceRequest createUpdateSqliteSequenceRequest() {
        return new UpdateSqliteSequenceRequest();
    }

    /**
     * Create an instance of {@link DeleteReminderResponse }
     * 
     */
    public DeleteReminderResponse createDeleteReminderResponse() {
        return new DeleteReminderResponse();
    }

    /**
     * Create an instance of {@link DeleteEntryResponse }
     * 
     */
    public DeleteEntryResponse createDeleteEntryResponse() {
        return new DeleteEntryResponse();
    }

    /**
     * Create an instance of {@link GetAllEntriesRequest }
     * 
     */
    public GetAllEntriesRequest createGetAllEntriesRequest() {
        return new GetAllEntriesRequest();
    }

    /**
     * Create an instance of {@link GetContactsLikeNameRequest }
     * 
     */
    public GetContactsLikeNameRequest createGetContactsLikeNameRequest() {
        return new GetContactsLikeNameRequest();
    }

    /**
     * Create an instance of {@link GetContactsLikeNameResponse }
     * 
     */
    public GetContactsLikeNameResponse createGetContactsLikeNameResponse() {
        return new GetContactsLikeNameResponse();
    }

    /**
     * Create an instance of {@link GetAllEntriesResponse }
     * 
     */
    public GetAllEntriesResponse createGetAllEntriesResponse() {
        return new GetAllEntriesResponse();
    }

    /**
     * Create an instance of {@link GetAllEntriesCustomRequest }
     * 
     */
    public GetAllEntriesCustomRequest createGetAllEntriesCustomRequest() {
        return new GetAllEntriesCustomRequest();
    }

    /**
     * Create an instance of {@link GetAllRemindersBetweenDatesResponse }
     * 
     */
    public GetAllRemindersBetweenDatesResponse createGetAllRemindersBetweenDatesResponse() {
        return new GetAllRemindersBetweenDatesResponse();
    }

    /**
     * Create an instance of {@link GetAllRemindersCustomResponse }
     * 
     */
    public GetAllRemindersCustomResponse createGetAllRemindersCustomResponse() {
        return new GetAllRemindersCustomResponse();
    }

    /**
     * Create an instance of {@link GetAllCompaniesResponse }
     * 
     */
    public GetAllCompaniesResponse createGetAllCompaniesResponse() {
        return new GetAllCompaniesResponse();
    }

    /**
     * Create an instance of {@link GetEntriesAfterDateResponse }
     * 
     */
    public GetEntriesAfterDateResponse createGetEntriesAfterDateResponse() {
        return new GetEntriesAfterDateResponse();
    }

    /**
     * Create an instance of {@link GetAllRemindersByContactRequest }
     * 
     */
    public GetAllRemindersByContactRequest createGetAllRemindersByContactRequest() {
        return new GetAllRemindersByContactRequest();
    }

    /**
     * Create an instance of {@link GetAllCompaniesRequest }
     * 
     */
    public GetAllCompaniesRequest createGetAllCompaniesRequest() {
        return new GetAllCompaniesRequest();
    }

    /**
     * Create an instance of {@link DeleteSqliteSequenceRequest }
     * 
     */
    public DeleteSqliteSequenceRequest createDeleteSqliteSequenceRequest() {
        return new DeleteSqliteSequenceRequest();
    }

    /**
     * Create an instance of {@link GetAllOperationTypesResponse }
     * 
     */
    public GetAllOperationTypesResponse createGetAllOperationTypesResponse() {
        return new GetAllOperationTypesResponse();
    }

    /**
     * Create an instance of {@link GetSqliteSequenceResponse }
     * 
     */
    public GetSqliteSequenceResponse createGetSqliteSequenceResponse() {
        return new GetSqliteSequenceResponse();
    }

    /**
     * Create an instance of {@link UpdateSqliteSequenceResponse }
     * 
     */
    public UpdateSqliteSequenceResponse createUpdateSqliteSequenceResponse() {
        return new UpdateSqliteSequenceResponse();
    }

    /**
     * Create an instance of {@link AddOperationTypeResponse }
     * 
     */
    public AddOperationTypeResponse createAddOperationTypeResponse() {
        return new AddOperationTypeResponse();
    }

    /**
     * Create an instance of {@link AddContactRequest }
     * 
     */
    public AddContactRequest createAddContactRequest() {
        return new AddContactRequest();
    }

    /**
     * Create an instance of {@link GetEntriesBeforeDateRequest }
     * 
     */
    public GetEntriesBeforeDateRequest createGetEntriesBeforeDateRequest() {
        return new GetEntriesBeforeDateRequest();
    }

    /**
     * Create an instance of {@link DeleteArchiveResponse }
     * 
     */
    public DeleteArchiveResponse createDeleteArchiveResponse() {
        return new DeleteArchiveResponse();
    }

    /**
     * Create an instance of {@link GetEntriesBetweenDatesResponse }
     * 
     */
    public GetEntriesBetweenDatesResponse createGetEntriesBetweenDatesResponse() {
        return new GetEntriesBetweenDatesResponse();
    }

    /**
     * Create an instance of {@link DeleteOperationTypeRequest }
     * 
     */
    public DeleteOperationTypeRequest createDeleteOperationTypeRequest() {
        return new DeleteOperationTypeRequest();
    }

    /**
     * Create an instance of {@link GetAllBankCustomResponse }
     * 
     */
    public GetAllBankCustomResponse createGetAllBankCustomResponse() {
        return new GetAllBankCustomResponse();
    }

    /**
     * Create an instance of {@link UpdateReminderRequest }
     * 
     */
    public UpdateReminderRequest createUpdateReminderRequest() {
        return new UpdateReminderRequest();
    }

    /**
     * Create an instance of {@link GetAllBankRequest }
     * 
     */
    public GetAllBankRequest createGetAllBankRequest() {
        return new GetAllBankRequest();
    }

    /**
     * Create an instance of {@link GetArchiveRequest }
     * 
     */
    public GetArchiveRequest createGetArchiveRequest() {
        return new GetArchiveRequest();
    }

    /**
     * Create an instance of {@link AddBankResponse }
     * 
     */
    public AddBankResponse createAddBankResponse() {
        return new AddBankResponse();
    }

    /**
     * Create an instance of {@link GetAllBankCustomRequest }
     * 
     */
    public GetAllBankCustomRequest createGetAllBankCustomRequest() {
        return new GetAllBankCustomRequest();
    }

}
