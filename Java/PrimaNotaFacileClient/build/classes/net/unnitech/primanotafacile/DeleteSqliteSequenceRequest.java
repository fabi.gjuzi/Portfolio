
package net.unnitech.primanotafacile;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="sqlite_sequence_id" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="archive_name" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "sqliteSequenceId",
    "archiveName"
})
@XmlRootElement(name = "deleteSqliteSequenceRequest")
public class DeleteSqliteSequenceRequest {

    @XmlElement(name = "sqlite_sequence_id", required = true)
    protected String sqliteSequenceId;
    @XmlElement(name = "archive_name", required = true)
    protected String archiveName;

    /**
     * Gets the value of the sqliteSequenceId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSqliteSequenceId() {
        return sqliteSequenceId;
    }

    /**
     * Sets the value of the sqliteSequenceId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSqliteSequenceId(String value) {
        this.sqliteSequenceId = value;
    }

    /**
     * Gets the value of the archiveName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getArchiveName() {
        return archiveName;
    }

    /**
     * Sets the value of the archiveName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setArchiveName(String value) {
        this.archiveName = value;
    }

}
