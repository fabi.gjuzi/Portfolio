package primanotafacileclient;

import com.jfoenix.controls.JFXButton;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import net.unnitech.primanotafacile.AddContactRequest;
import net.unnitech.primanotafacile.AddContactResponse;
import net.unnitech.primanotafacile.ArchivesPort;
import net.unnitech.primanotafacile.ArchivesPortService;
import net.unnitech.primanotafacile.Contact;
import net.unnitech.primanotafacile.UpdateContactRequest;
import net.unnitech.primanotafacile.UpdateContactResponse;

public class NewContactController {

    @FXML
    private AnchorPane anchorPane;

    @FXML
    private JFXButton btnSalva;

    @FXML
    private JFXButton btnAnnulla;

    @FXML
    private TextArea extratxt;

    @FXML
    private TextField nrlavorotxt;

    @FXML
    private TextField nrcasatxt;

    @FXML
    private TextField nrfaxtxt;

    @FXML
    private TextField nrcellularetxt;

    @FXML
    private TextField nometxt;

    @FXML
    private TextField indirizzotxt;

    @FXML
    private TextField cittatxt;

    @FXML
    private TextField captxt;

    @FXML
    private TextField provtxt;

    @FXML
    private TextField email1txt;

    @FXML
    private TextField email2txt;

    @FXML
    private TextField messagingtxt;

    @FXML
    private TextField webtxt;

    @FXML
    private TextField contactID;

    private Stage dialogStage;
    private Contact contact;
    private boolean okClicked = false;

    @FXML
    private void initialize() {
        anchorPane.requestFocus();
    }

    @FXML
    void handleRemoveFocus() {
        anchorPane.requestFocus();
    }

    public void setDialogStage(Stage dialogStage) {
        this.dialogStage = dialogStage;
        this.dialogStage.getScene().getStylesheets().add("assets/style/style.css");
    }

    public void setContact(Contact newContact) {
        this.contact = newContact;
        if (!"0".equals(newContact.getContactId())) {
            contactID.setText(newContact.getContactId());
            nometxt.setText(newContact.getFullName());
            indirizzotxt.setText(newContact.getAddress());
            cittatxt.setText(newContact.getCity());
            email1txt.setText(newContact.getEMail1());
            email2txt.setText(newContact.getEMail2());
            nrfaxtxt.setText(newContact.getFax());
            messagingtxt.setText(newContact.getInstantMessage());
            webtxt.setText(newContact.getWeb());
            extratxt.setText(newContact.getNote());
            nrcellularetxt.setText(newContact.getMobileNumber());
            nrcasatxt.setText(newContact.getPhoneHome());
            nrlavorotxt.setText(newContact.getPhoneWork());
            captxt.setText(newContact.getZipCode());
            provtxt.setText(newContact.getState());
        } else {
            contactID.setText(newContact.getContactId());
        }
        handleRemoveFocus();
    }

    public boolean isOkClicked() {
        return okClicked;
    }

    private final Stage getDialogStage() {
        return dialogStage;
    }

    @FXML
    private void handleOk() {
        if (isInputValid()) {
            //handle state of edit or new by checking value of  textViewScadenzarioId
            if ("0".equals(contactID.getText())) {
                //no id add new
                //we're gonna add a new reminder
                //create a new reminer class and set the textfield values
                Contact newContact = new Contact();

                newContact.setFullName(nometxt.getText());
                newContact.setAddress(indirizzotxt.getText());
                newContact.setCity(cittatxt.getText());
                newContact.setEMail1(email1txt.getText());
                newContact.setEMail2(email2txt.getText());
                newContact.setFax(nrfaxtxt.getText());
                newContact.setInstantMessage(messagingtxt.getText());
                newContact.setWeb(webtxt.getText());
                newContact.setNote(extratxt.getText());
                newContact.setMobileNumber(nrcellularetxt.getText());
                newContact.setPhoneHome(nrcasatxt.getText());
                newContact.setPhoneWork(nrlavorotxt.getText());
                newContact.setZipCode(captxt.getText());
                newContact.setState(provtxt.getText());
                newContact.setContactType("Lesh");

                ArchivesPortService archiveService = new ArchivesPortService();
                ArchivesPort archivesPort = archiveService.getArchivesPortSoap11();
                AddContactRequest addContact = new AddContactRequest();
                addContact.setContact(newContact);
                addContact.setArchiveName(PrimaNotaFacileClient.getCurrentArchive() + ".sqlite3");
                AddContactResponse addContactResponse = archivesPort.addContact(addContact);
                String pergjige = addContactResponse.getResponse();
                if ("OK".equals(pergjige)) {
                    okClicked = true;
                    dialogStage.close();

                } else {
                    Alert alert = new Alert(Alert.AlertType.ERROR);
                    alert.initOwner(dialogStage);
                    alert.setTitle("Errore!");
                    alert.setHeaderText("Errore!");
                    alert.setContentText("Errore nel tentativo di aggiungere un nuovo Contatto!\\Si prega di provare di nuovo\\n Se il problema persiste si prega di contattare l'Amministratore dell sistema");
                    alert.showAndWait();
                }
            } else {
                //textViewScadenzarioID has value , we're in edit state
                //create the object reminder and set also the reminder id in order to update
                Contact existingContact = new Contact();

                existingContact.setFullName(nometxt.getText());
                existingContact.setAddress(indirizzotxt.getText());
                existingContact.setCity(cittatxt.getText());
                existingContact.setEMail1(email1txt.getText());
                existingContact.setEMail2(email2txt.getText());
                existingContact.setFax(nrfaxtxt.getText());
                existingContact.setInstantMessage(messagingtxt.getText());
                existingContact.setWeb(webtxt.getText());
                existingContact.setNote(extratxt.getText());
                existingContact.setMobileNumber(nrcellularetxt.getText());
                existingContact.setPhoneHome(nrcasatxt.getText());
                existingContact.setPhoneWork(nrlavorotxt.getText());
                existingContact.setZipCode(captxt.getText());
                existingContact.setState(provtxt.getText());
                existingContact.setContactId(contactID.getText());
                existingContact.setContactType("Lesh");

                ArchivesPortService archiveService = new ArchivesPortService();
                ArchivesPort archivesPort = archiveService.getArchivesPortSoap11();
                UpdateContactRequest updateContactRequest = new UpdateContactRequest();
                updateContactRequest.setContact(existingContact);
                updateContactRequest.setArchiveName(PrimaNotaFacileClient.getCurrentArchive() + ".sqlite3");
                UpdateContactResponse updateContactRespone = archivesPort.updateContact(updateContactRequest);
                String pergjige = updateContactRespone.getResponse();
                if ("OK".equals(pergjige)) {
                    okClicked = true;
                    dialogStage.close();
                } else {
                    Alert alert = new Alert(Alert.AlertType.ERROR);
                    alert.initOwner(dialogStage);
                    alert.setTitle("Errore!");
                    alert.setHeaderText("Errore!");
                    alert.setContentText("Errore nel tentativo di modificare il Contatto!\\Si prega di provare di nuovo\\n Se il problema persiste si prega di contattare l'Amministratore dell sistema");
                    alert.showAndWait();
                }
            }
        }
    }

    @FXML
    private void handleCancel() {
        dialogStage.close();
    }

    private boolean isInputValid() {
        String errorMessage = "";

        if ("".equals(nometxt.getText())) {
            errorMessage = "Please set the Contact name";
        }
        if (!"".equals(email1txt.getText())) {
            if (email1txt.getText().startsWith("@")) {
                errorMessage = "Text entered for E-mail 1 field is not an e-mail";
            }
            if (!email1txt.getText().startsWith("@") && (!email1txt.getText().contains("@") || !email1txt.getText().split("@")[1].contains(".") || email1txt.getText().split("@")[1].startsWith(".")
                    || email1txt.getText().split("@")[1].indexOf(".") == email1txt.getText().split("@")[1].length() - 1)) {
                errorMessage = "Text entered for E-mail 1 field is not an e-mail";
            }
        }

        if (!"".equals(email2txt.getText())) {
            if (email2txt.getText().startsWith("@")) {
                errorMessage = "Text entered for E-mail 2 field is not an e-mail";
            }
            if (!email2txt.getText().startsWith("@") && (!email2txt.getText().contains("@") || !email2txt.getText().split("@")[1].contains(".") || email2txt.getText().split("@")[1].startsWith(".")
                    || email2txt.getText().split("@")[1].indexOf(".") == email2txt.getText().split("@")[1].length() - 1)) {
                errorMessage = "Text entered for E-mail 2 field is not an e-mail";
            }
        }

        if (!"".equals(webtxt.getText())) {
            if (webtxt.getText().indexOf(".") == webtxt.getText().length() - 1 || webtxt.getText().indexOf(".") == 0 || !webtxt.getText().contains(".")) {
                errorMessage = "Please enter an valid web adress";
            }
        }

        if (!"".equals(nrlavorotxt.getText())) {
            if (nrlavorotxt.getText().startsWith("+")) {
                if (nrlavorotxt.getText().substring(1).startsWith("+")) {
                    errorMessage = "Please enter an valid number as work number";
                } else {
                    try {
                        Integer.parseInt(nrlavorotxt.getText().substring(1));

                    } catch (NumberFormatException e) {
                        errorMessage = "Please enter an valid number as work number";
                    }
                }
            } else {
                try {
                    Integer.parseInt(nrlavorotxt.getText());

                } catch (NumberFormatException e) {
                    errorMessage = "Please enter an valid number as work number";
                }
            }
        }

        if (!"".equals(nrcasatxt.getText())) {
            if (nrcasatxt.getText().startsWith("+")) {
                if (nrcasatxt.getText().substring(1).startsWith("+")) {
                    errorMessage = "Please enter an valid number as house number";
                } else {
                    try {
                        Integer.parseInt(nrcasatxt.getText().substring(1));

                    } catch (NumberFormatException e) {
                        errorMessage = "Please enter an valid number as house number";
                    }
                }
            } else {
                try {
                    Integer.parseInt(nrcasatxt.getText());

                } catch (NumberFormatException e) {
                    errorMessage = "Please enter an valid number as house number";
                }
            }
        }

        if (!"".equals(nrcellularetxt.getText())) {
            if (nrcellularetxt.getText().startsWith("+")) {
                if (nrcellularetxt.getText().substring(1).startsWith("+")) {
                    errorMessage = "Please enter an valid number as mobile number";
                } else {
                    try {
                        Long.parseLong(nrcellularetxt.getText().substring(1));

                    } catch (NumberFormatException e) {
                        errorMessage = "Please enter an valid number as mobile number";
                    }
                }
            } else {
                try {
                    Long.parseLong(nrcellularetxt.getText());

                } catch (NumberFormatException e) {
                    errorMessage = "Please enter an valid number as mobile number";
                }
            }
        }

        if (!"".equals(nrfaxtxt.getText())) {
            if (nrfaxtxt.getText().startsWith("+")) {
                if (nrfaxtxt.getText().substring(1).startsWith("+")) {
                    errorMessage = "Please enter an valid number as fax number";
                } else {
                    try {
                        Long.parseLong(nrfaxtxt.getText().substring(1));

                    } catch (NumberFormatException e) {
                        errorMessage = "Please enter an valid number as fax number";
                    }
                }
            } else {
                try {
                    Long.parseLong(nrfaxtxt.getText());

                } catch (NumberFormatException e) {
                    errorMessage = "Please enter an valid number as fax number";
                }
            }
        }

        if (errorMessage.length() == 0) {
            return true;
        } else {
            // Show the error message.
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.initOwner(dialogStage);
            alert.setTitle("Campi non validi");
            alert.setHeaderText("Si prega di correggere i campi non validi");
            Text text = new Text(errorMessage);
            text.setWrappingWidth(400);
            alert.getDialogPane().setContent(text);
            alert.showAndWait();

            return false;
        }
    }

}
