package primanotafacileclient;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXRadioButton;
import com.jfoenix.controls.JFXTextField;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Label;
import javafx.scene.control.ToggleGroup;
import javafx.scene.layout.AnchorPane;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import net.unnitech.primanotafacile.AddOperationTypeRequest;
import net.unnitech.primanotafacile.AddOperationTypeResponse;
import net.unnitech.primanotafacile.ArchivesPort;
import net.unnitech.primanotafacile.ArchivesPortService;
import net.unnitech.primanotafacile.OperationType;
import net.unnitech.primanotafacile.UpdateOperationTypeRequest;
import net.unnitech.primanotafacile.UpdateOperationTypeResponse;

public class NewOperationType {

    @FXML
    private AnchorPane anchorPane;
    
    @FXML
    private JFXTextField nametxt;

    @FXML
    private JFXButton savebtn;

    @FXML
    private JFXButton cancelbtn;

    @FXML
    private JFXRadioButton entrateRadioButton;

    @FXML
    private JFXRadioButton usciteRadioButton;

    @FXML
    private Label id;

    private Stage dialogStage;
    private OperationType operationType;
    private boolean okClicked = false;

    @FXML
    public void initialize() {
        ToggleGroup group = new ToggleGroup();
        entrateRadioButton.setToggleGroup(group);
        usciteRadioButton.setToggleGroup(group);
        anchorPane.requestFocus();
    }
    
    @FXML
    void handleRemoveFocus() {
        anchorPane.requestFocus();
    }
    public void setDialogStage(Stage dialogStage) {
        this.dialogStage = dialogStage;
        this.dialogStage.getScene().getStylesheets().add("assets/style/style.css");
        anchorPane.requestFocus();
    }

    public void setOperationType(OperationType newOperationType) {

        this.operationType = newOperationType;
        nametxt.setText(newOperationType.getName());
        if ("true".equals(newOperationType.getIncoming())) {
            entrateRadioButton.setSelected(true);
        } else {
            entrateRadioButton.setSelected(false);
        }
        if ("true".equals(newOperationType.getOutgoing())) {
            usciteRadioButton.setSelected(true);
        } else {
            usciteRadioButton.setSelected(false);
        }
        id.setText(newOperationType.getOperationTypeId());

    }

    public boolean isOkClicked() {
        return okClicked;
    }

    private final Stage getDialogStage() {
        return dialogStage;
    }

    @FXML
    private void handleOk() {
        if (isInputValid()) {
            //handle state of edit or new by checking value of  textViewScadenzarioId
            if ("0".equals(id.getText())) {
                //no id add new
                //we're gonna add a new reminder
                //create a new reminer class and set the textfield values
                OperationType newOperationType = new OperationType();

                if (entrateRadioButton.isSelected()) {
                    newOperationType.setIncoming("1");
                } else {
                    newOperationType.setIncoming("0");
                }
                if (usciteRadioButton.isSelected()) {
                    newOperationType.setOutgoing("1");
                } else {
                    newOperationType.setOutgoing("0");
                }
                newOperationType.setName(nametxt.getText());
                newOperationType.setOperationTypeId("0");

                ArchivesPortService archiveService = new ArchivesPortService();
                ArchivesPort archivesPort = archiveService.getArchivesPortSoap11();
                AddOperationTypeRequest addOperationType = new AddOperationTypeRequest();
                addOperationType.setOperationType(newOperationType);
                addOperationType.setArchiveName(PrimaNotaFacileClient.getCurrentArchive() + ".sqlite3");
                AddOperationTypeResponse addOperationTypeResponse = archivesPort.addOperationType(addOperationType);
                String pergjige = addOperationTypeResponse.getResponse();
                if ("OK".equals(pergjige)) {
                    okClicked = true;
                    dialogStage.close();

                } else {
                    Alert alert = new Alert(Alert.AlertType.ERROR);
                    alert.initOwner(dialogStage);
                    alert.setTitle("Errore!");
                    alert.setHeaderText("Errore!");
                    alert.setContentText("Errore nel tentativo di aggiungere un nuovo Operazione!\\Si prega di provare di nuovo\\n Se il problema persiste si prega di contattare l'Amministratore dell sistema");
                    alert.showAndWait();
                }
            } else {
                //textViewScadenzarioID has value , we're in edit state
                //create the object reminder and set also the reminder id in order to update
                OperationType existingOperationType = new OperationType();

                if (entrateRadioButton.isSelected()) {
                    existingOperationType.setIncoming("1");
                } else {
                    existingOperationType.setIncoming("0");
                }
                if (usciteRadioButton.isSelected()) {
                    existingOperationType.setOutgoing("1");
                } else {
                    existingOperationType.setOutgoing("0");
                }
                existingOperationType.setName(nametxt.getText());
                existingOperationType.setOperationTypeId(id.getText());

                ArchivesPortService archiveService = new ArchivesPortService();
                ArchivesPort archivesPort = archiveService.getArchivesPortSoap11();
                UpdateOperationTypeRequest updateOperationTypeRequest = new UpdateOperationTypeRequest();
                updateOperationTypeRequest.setOperationType(existingOperationType);
                updateOperationTypeRequest.setArchiveName(PrimaNotaFacileClient.getCurrentArchive() + ".sqlite3");
                UpdateOperationTypeResponse updateOperationTypeRespone = archivesPort.updateOperationType(updateOperationTypeRequest);
                String pergjige = updateOperationTypeRespone.getResponse();
                if ("OK".equals(pergjige)) {
                    okClicked = true;
                    dialogStage.close();
                } else {
                    Alert alert = new Alert(Alert.AlertType.ERROR);
                    alert.initOwner(dialogStage);
                    alert.setTitle("Errore!");
                    alert.setHeaderText("Errore!");
                    alert.setContentText("Errore nel tentativo di modificare il Operazione!\\Si prega di provare di nuovo\\n Se il problema persiste si prega di contattare l'Amministratore dell sistema");
                    alert.showAndWait();
                }
            }
        }
    }

    @FXML
    private void handleCancel() {
        dialogStage.close();
    }

    private boolean isInputValid() {
        String errorMessage = "";

        if ("".equals(nametxt.getText())) {
            errorMessage = "Please set the Name of the Operation";
        }

        if (errorMessage.length() == 0) {
            return true;
        } else {
            // Show the error message.
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.initOwner(dialogStage);
            alert.setTitle("Campi non validi");
            alert.setHeaderText("Si prega di correggere i campi non validi");
            Text text = new Text(errorMessage);
            text.setWrappingWidth(400);
            alert.getDialogPane().setContent(text);
            alert.showAndWait();

            return false;
        }
    }
}
