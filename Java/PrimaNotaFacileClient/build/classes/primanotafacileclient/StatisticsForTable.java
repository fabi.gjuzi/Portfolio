/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package primanotafacileclient;

import javafx.beans.property.SimpleStringProperty;

/**
 *
 * @author programues
 */
public class StatisticsForTable {
    
    public SimpleStringProperty tableColumnMese = new SimpleStringProperty("");
    public SimpleStringProperty tableColumnFouriCassa = new SimpleStringProperty("");
    public SimpleStringProperty tableColumnEntrateCassa = new SimpleStringProperty("");
    public SimpleStringProperty tableColumnUsciteCassa = new SimpleStringProperty("");
    public SimpleStringProperty tableColumnSaldoCassa = new SimpleStringProperty("");
    public SimpleStringProperty tableColumnEntrateBanca = new SimpleStringProperty("");
    public SimpleStringProperty tableColumnUsciteBanca = new SimpleStringProperty("");
    public SimpleStringProperty tableColumnSaldoBanca = new SimpleStringProperty("");
    public SimpleStringProperty tableColumnSaldoTotale = new SimpleStringProperty("");

   
    
    public StatisticsForTable() {
        this("", "", "", "", "", "", "", "", "");
    }
    
    public StatisticsForTable(String tableColumnMese, String tableColumnFouriCassa, String  tableColumnEntrateCassa, String tableColumnUsciteCassa,
            String tableColumnSaldoCassa, String tableColumnEntrateBanca,String tableColumnUsciteBanca, String tableColumnSaldoBanca, String tableColumnSaldoTotale) {
        
        setTableColumnMese(tableColumnMese);
        setTableColumnFouriCassa(tableColumnFouriCassa);
        setTableColumnEntrateCassa(tableColumnEntrateCassa);
        setTableColumnUsciteCassa(tableColumnUsciteCassa);
        setTableColumnSaldoCassa(tableColumnSaldoCassa);
        setTableColumnEntrateBanca(tableColumnEntrateBanca);
        setTableColumnUsciteBanca(tableColumnUsciteBanca);
        setTableColumnSaldoBanca(tableColumnSaldoBanca);
        setTableColumnSaldoTotale(tableColumnSaldoTotale);
    }

//    public EntryForTable(String entryDate, String string, String outCash, String string0, String string1, String incomingValue, String outgoingValue, String string2, String string3) {
//        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
//    }
    
    public String getTableColumnMese() {
        return tableColumnMese.getValue();
    }
    
    public void setTableColumnMese(String tableColumnMese) {
        this.tableColumnMese.set(tableColumnMese);
    }
    
    public String getTableColumnFouriCassa() {
        return tableColumnFouriCassa.getValue();
    }
    
    public void setTableColumnFouriCassa(String tableColumnFouriCassa) {
        this.tableColumnFouriCassa.set(tableColumnFouriCassa);
    }
    
    public String getTableColumnEntrateCassa() {
        return tableColumnEntrateCassa.getValue();
    }
    
    public void setTableColumnEntrateCassa(String tableColumnEntrateCassa) {
        this.tableColumnEntrateCassa.set(tableColumnEntrateCassa);
    }
    
    public String getTablePrimaNotaCassaEntrate() {
        return tableColumnEntrateCassa.getValue();
    }
    
    public void setTableColumnUsciteCassa(String tableColumnUsciteCassa) {
        this.tableColumnUsciteCassa.set(tableColumnUsciteCassa);
    }
    
    public String getTableColumnUsciteCassa() {
        return tableColumnUsciteCassa.getValue();
    }
    
    public void setTableColumnSaldoCassa(String tableColumnSaldoCassa) {
        this.tableColumnSaldoCassa.set(tableColumnSaldoCassa);
    }
    
    public String getTableColumnSaldoCassa() {
        return tableColumnSaldoCassa.getValue();
    }
    
    public void setTableColumnEntrateBanca(String tableColumnEntrateBanca) {
        this.tableColumnEntrateBanca.set(tableColumnEntrateBanca);
    }
    
    public String getTableColumnEntrateBanca() {
        return tableColumnEntrateBanca.getValue();
    }
    
    public void setTableColumnUsciteBanca(String tableColumnUsciteBanca) {
        this.tableColumnUsciteBanca.set(tableColumnUsciteBanca);
    }
    
    public String getTableColumnUsciteBanca() {
        return tableColumnUsciteBanca.getValue();
    }
    
    public void setTableColumnSaldoBanca(String tableColumnSaldoBanca) {
        this.tableColumnSaldoBanca.set(tableColumnSaldoBanca);
    }
    
    public String getTableColumnSaldoBanca() {
        return tableColumnSaldoBanca.getValue();
    }
    
    public void setTableColumnSaldoTotale(String tableColumnSaldoTotale) {
        this.tableColumnSaldoTotale.set(tableColumnSaldoTotale);
    }
    
    public String getTableColumnSaldoTotale() {
        return tableColumnSaldoTotale.getValue();
    }
    
    
}
