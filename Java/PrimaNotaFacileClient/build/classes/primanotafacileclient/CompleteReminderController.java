/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package primanotafacileclient;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXRadioButton;
import java.net.URL;
import java.text.DateFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.ToggleGroup;
import javafx.stage.Stage;
import net.unnitech.primanotafacile.AddEntryRequest;
import net.unnitech.primanotafacile.AddEntryResponse;
import net.unnitech.primanotafacile.ArchivesPort;
import net.unnitech.primanotafacile.ArchivesPortService;
import net.unnitech.primanotafacile.Bank;
import net.unnitech.primanotafacile.DeleteReminderRequest;
import net.unnitech.primanotafacile.DeleteReminderResponse;
import net.unnitech.primanotafacile.Entry;
import net.unnitech.primanotafacile.GetAllBankRequest;
import net.unnitech.primanotafacile.GetAllBankResponse;
import net.unnitech.primanotafacile.Reminder;
import net.unnitech.primanotafacile.UpdateReminderRequest;
import net.unnitech.primanotafacile.UpdateReminderResponse;

/**
 * FXML Controller class
 *
 * @author programues
 */
public class CompleteReminderController implements Initializable {

    /**
     * Initializes the controller class.
     */
    @FXML
    private JFXRadioButton radioNessuna;

    @FXML
    private JFXRadioButton radioNuovaUsciteaCassa;

    @FXML
    private JFXRadioButton radioNuovaEntrataCassa;

    @FXML
    private JFXRadioButton radioNuovaUscitaBanca;

    @FXML
    private JFXRadioButton radioNuovaEntrataBanca;

    @FXML
    private JFXRadioButton radioNuovoFuoriCassa;

    @FXML
    private JFXButton buttonComplete;

    @FXML
    private JFXButton buttonCancel;
    
    @FXML
    private ComboBox<String> comboBoxBanca;
    
    private final LinkedHashMap<String, String> comboBankPrimaNotaHash = new LinkedHashMap<>();
    private Reminder thisReminder;
    ToggleGroup completeReminderRadios;
    private boolean okClicked = false;
    private Stage dialogStage;
    
     /**
     * Sets the stage of this dialog.
     *
     * @param dialogStage
     */
    public void setDialogStage(Stage dialogStage) {
        this.dialogStage = dialogStage;
        this.dialogStage.getScene().getStylesheets().add("assets/style/style.css");
    }

    private final Stage getDialogStage() {
        return dialogStage;
    }
    
     /**
     * Returns true if the user clicked OK, false otherwise.
     *
     * @return
     */
    public boolean isOkClicked() {
        return okClicked;
    }
    
    @FXML 
    public void setReminder(Reminder reminder){
        thisReminder = reminder;
    }
    @FXML
    public void handleComplete() throws ParseException{
        //Logic of completing reminder
        ArchivesPortService archiveService = new ArchivesPortService();
        ArchivesPort archivesPort = archiveService.getArchivesPortSoap11();
        String pergjige = "";
        RadioButton selectedRadio = (RadioButton) completeReminderRadios.getSelectedToggle();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");
        switch (selectedRadio.getText()){
            case "Nessuna":
                //just complete reminder
                thisReminder.setCompleted("1");               
                LocalDate date = LocalDate.parse(thisReminder.getDate(), formatter); 
                thisReminder.setDate(date.toString()+" 00:00:00");
                thisReminder.setValue(NumberFormat.getCurrencyInstance(Locale.ITALY).parse(thisReminder.getValue()).toString());
                UpdateReminderRequest updateRequest = new UpdateReminderRequest();
                updateRequest.setReminder(thisReminder);                
                updateRequest.setArchiveName(PrimaNotaFacileClient.getCurrentArchive() + ".sqlite3");
                UpdateReminderResponse updateResponse = archivesPort.updateReminder(updateRequest);
                
                pergjige = updateResponse.getResponse();
                if ("OK".equals(pergjige)) {
                    okClicked = true;
                    dialogStage.close();
                } else {
                    Alert alert = new Alert(Alert.AlertType.ERROR);
                    alert.initOwner(dialogStage);
                    alert.setTitle("Errore!");
                    alert.setHeaderText("Errore!");
                    alert.setContentText("Errore nel tentativo di completare scadenzario!\n Si prega di provare di nuovo\n Se il problema persiste si prega di contattare l'Amministratore dell sistema");
                    alert.showAndWait();
                }
                break;
            case "Nuovo Fuori Cassa" :
                //add value of reminder as a new entry with value fouri cassa
                //we're just updating reminder also not deleting it
                Entry newEntry = new Entry();
                newEntry.setDescription(thisReminder.getDescription());
                date = LocalDate.parse(thisReminder.getDate(), formatter); 
                 newEntry.setEntryDate(date.toString()+" 00:00:00");
                newEntry.setIdContact(thisReminder.getIdContact());
                newEntry.setIdBank("0");
                newEntry.setOutCash(NumberFormat.getCurrencyInstance(Locale.ITALY).parse(thisReminder.getValue()).toString());
                newEntry.setIdOperationType("0");
                newEntry.setIncomingValueBank("0");
                newEntry.setIncomingValueCash("0");
                newEntry.setOutgoingValueBank("0");
                newEntry.setOutgoingValueCash("0");
                AddEntryRequest newEntryRequest = new AddEntryRequest();
                newEntryRequest.setEntry(newEntry);
                newEntryRequest.setArchiveName(PrimaNotaFacileClient.getCurrentArchive() + ".sqlite3");
                AddEntryResponse newEntryResponse = archivesPort.addEntry(newEntryRequest);
                pergjige = newEntryResponse.getResponse();
                if ("OK".equals(pergjige)) {
                    //update reminder
                    pergjige = handleUpdateReminderAfterSettingAsEntry();
                     if ("OK".equals(pergjige)) {
                        okClicked = true;
                        dialogStage.close();
                    } else {
                    Alert alert = new Alert(Alert.AlertType.ERROR);
                    alert.initOwner(dialogStage);
                    alert.setTitle("Errore!");
                    alert.setHeaderText("Errore!");
                    alert.setContentText("Errore nel tentativo di completare scadenzario!\n Si prega di provare di nuovo\n Se il problema persiste si prega di contattare l'Amministratore dell sistema");
                    alert.showAndWait();
                    }
                   
                } else {
                    Alert alert = new Alert(Alert.AlertType.ERROR);
                    alert.initOwner(dialogStage);
                    alert.setTitle("Errore!");
                    alert.setHeaderText("Errore!");
                    alert.setContentText("Errore nel tentativo di completare scadenzario!\n Si prega di provare di nuovo\n Se il problema persiste si prega di contattare l'Amministratore dell sistema");
                    alert.showAndWait();
                }
                break;               
            case "Nuova Entrata Cassa":
                //add value of reminder as a new entry with value in entrate cassa
                 newEntry = new Entry();
                newEntry.setDescription(thisReminder.getDescription());               
                date = LocalDate.parse(thisReminder.getDate(), formatter); 
                 newEntry.setEntryDate(date.toString()+" 00:00:00");
                newEntry.setIdContact(thisReminder.getIdContact());
                newEntry.setIdBank("0");
                newEntry.setOutCash(NumberFormat.getCurrencyInstance(Locale.ITALY).parse(thisReminder.getValue()).toString());
                newEntry.setIdOperationType("0");
                newEntry.setIncomingValueBank("0");
                newEntry.setIncomingValueCash("0");
                newEntry.setOutgoingValueBank("0");
                newEntry.setOutgoingValueCash("0");
                newEntryRequest = new AddEntryRequest();
                newEntryRequest.setEntry(newEntry);
                newEntryRequest.setArchiveName(PrimaNotaFacileClient.getCurrentArchive() + ".sqlite3");
                newEntryResponse = archivesPort.addEntry(newEntryRequest);
                pergjige = newEntryResponse.getResponse();
                if ("OK".equals(pergjige)) {
                    //update reminder
                    pergjige = handleUpdateReminderAfterSettingAsEntry();
                     if ("OK".equals(pergjige)) {
                        okClicked = true;
                        dialogStage.close();
                    } else {
                    Alert alert = new Alert(Alert.AlertType.ERROR);
                    alert.initOwner(dialogStage);
                    alert.setTitle("Errore!");
                    alert.setHeaderText("Errore!");
                    alert.setContentText("Errore nel tentativo di completare scadenzario!\n Si prega di provare di nuovo\n Se il problema persiste si prega di contattare l'Amministratore dell sistema");
                    alert.showAndWait();
                    }
                   
                } else {
                    Alert alert = new Alert(Alert.AlertType.ERROR);
                    alert.initOwner(dialogStage);
                    alert.setTitle("Errore!");
                    alert.setHeaderText("Errore!");
                    alert.setContentText("Errore nel tentativo di completare scadenzario!\n Si prega di provare di nuovo\n Se il problema persiste si prega di contattare l'Amministratore dell sistema");
                    alert.showAndWait();
                }
                break;               
            case "Nuova Uscita Cassa" :
                //add value of reminder as a new entry with value in uscite cassa
                newEntry = new Entry();
                newEntry.setDescription(thisReminder.getDescription());
                 date = LocalDate.parse(thisReminder.getDate(), formatter); 
                 newEntry.setEntryDate(date.toString()+" 00:00:00");
                newEntry.setIdContact(thisReminder.getIdContact());
                newEntry.setIdBank("0");
                newEntry.setOutCash("0");
                newEntry.setIdOperationType("0");
                newEntry.setIncomingValueBank("0");
                newEntry.setIncomingValueCash("0");
                newEntry.setOutgoingValueBank("0");
                newEntry.setOutgoingValueCash(NumberFormat.getCurrencyInstance(Locale.ITALY).parse(thisReminder.getValue()).toString());
                newEntryRequest = new AddEntryRequest();
                newEntryRequest.setEntry(newEntry);
                newEntryRequest.setArchiveName(PrimaNotaFacileClient.getCurrentArchive() + ".sqlite3");
                newEntryResponse = archivesPort.addEntry(newEntryRequest);
                pergjige = newEntryResponse.getResponse();
                if ("OK".equals(pergjige)) {
                    //update reminder
                    pergjige = handleUpdateReminderAfterSettingAsEntry();
                     if ("OK".equals(pergjige)) {
                        okClicked = true;
                        dialogStage.close();
                    } else {
                    Alert alert = new Alert(Alert.AlertType.ERROR);
                    alert.initOwner(dialogStage);
                    alert.setTitle("Errore!");
                    alert.setHeaderText("Errore!");
                    alert.setContentText("Errore nel tentativo di completare scadenzario!\n Si prega di provare di nuovo\n Se il problema persiste si prega di contattare l'Amministratore dell sistema");
                    alert.showAndWait();
                    }
                   
                } else {
                    Alert alert = new Alert(Alert.AlertType.ERROR);
                    alert.initOwner(dialogStage);
                    alert.setTitle("Errore!");
                    alert.setHeaderText("Errore!");
                    alert.setContentText("Errore nel tentativo di completare scadenzario!\n Si prega di provare di nuovo\n Se il problema persiste si prega di contattare l'Amministratore dell sistema");
                    alert.showAndWait();
                }
                break;              
            case "Nuova Entrata Banca" :
                //add value of reminder as a new entry with value in entrate banca
                newEntry = new Entry();
                newEntry.setDescription(thisReminder.getDescription());
                 date = LocalDate.parse(thisReminder.getDate(), formatter); 
                 newEntry.setEntryDate(date.toString()+" 00:00:00");
                newEntry.setIdContact(thisReminder.getIdContact());               
                newEntry.setIdBank(comboBankPrimaNotaHash.get(comboBoxBanca.getSelectionModel().getSelectedItem()));
                newEntry.setOutCash("0");
                newEntry.setIdOperationType("0");
                newEntry.setIncomingValueBank(NumberFormat.getCurrencyInstance(Locale.ITALY).parse(thisReminder.getValue()).toString());
                newEntry.setIncomingValueCash("0");
                newEntry.setOutgoingValueBank("0");
                newEntry.setOutgoingValueCash("0");
                newEntryRequest = new AddEntryRequest();
                newEntryRequest.setEntry(newEntry);
                newEntryRequest.setArchiveName(PrimaNotaFacileClient.getCurrentArchive() + ".sqlite3");
                newEntryResponse = archivesPort.addEntry(newEntryRequest);
                pergjige = newEntryResponse.getResponse();
                if ("OK".equals(pergjige)) {
                    //update reminder
                    pergjige = handleUpdateReminderAfterSettingAsEntry();
                     if ("OK".equals(pergjige)) {
                        okClicked = true;
                        dialogStage.close();
                    } else {
                    Alert alert = new Alert(Alert.AlertType.ERROR);
                    alert.initOwner(dialogStage);
                    alert.setTitle("Errore!");
                    alert.setHeaderText("Errore!");
                    alert.setContentText("Errore nel tentativo di completare scadenzario!\n Si prega di provare di nuovo\n Se il problema persiste si prega di contattare l'Amministratore dell sistema");
                    alert.showAndWait();
                    }
                   
                } else {
                    Alert alert = new Alert(Alert.AlertType.ERROR);
                    alert.initOwner(dialogStage);
                    alert.setTitle("Errore!");
                    alert.setHeaderText("Errore!");
                    alert.setContentText("Errore nel tentativo di completare scadenzario!\n Si prega di provare di nuovo\n Se il problema persiste si prega di contattare l'Amministratore dell sistema");
                    alert.showAndWait();
                }
                break;               
            case "Nuova Uscita Banca" :
                //add value of reminder as a new entry with value in uscite banca
                newEntry = new Entry();
                newEntry.setDescription(thisReminder.getDescription());
                 date = LocalDate.parse(thisReminder.getDate(), formatter); 
                 newEntry.setEntryDate(date.toString()+" 00:00:00");
                newEntry.setIdContact(thisReminder.getIdContact());               
                newEntry.setIdBank(comboBankPrimaNotaHash.get(comboBoxBanca.getSelectionModel().getSelectedItem()));
                newEntry.setOutCash("0");
                newEntry.setIdOperationType("0");
                newEntry.setIncomingValueBank("0");
                newEntry.setIncomingValueCash("0");
                newEntry.setOutgoingValueBank(NumberFormat.getCurrencyInstance(Locale.ITALY).parse(thisReminder.getValue()).toString());
                newEntry.setOutgoingValueCash("0");
                newEntryRequest = new AddEntryRequest();
                newEntryRequest.setEntry(newEntry);
                newEntryRequest.setArchiveName(PrimaNotaFacileClient.getCurrentArchive() + ".sqlite3");
                newEntryResponse = archivesPort.addEntry(newEntryRequest);
                pergjige = newEntryResponse.getResponse();
                if ("OK".equals(pergjige)) {
                    //update reminder
                    pergjige = handleUpdateReminderAfterSettingAsEntry();
                     if ("OK".equals(pergjige)) {
                        okClicked = true;
                        dialogStage.close();
                    } else {
                    Alert alert = new Alert(Alert.AlertType.ERROR);
                    alert.initOwner(dialogStage);
                    alert.setTitle("Errore!");
                    alert.setHeaderText("Errore!");
                    alert.setContentText("Errore nel tentativo di completare scadenzario!\n Si prega di provare di nuovo\n Se il problema persiste si prega di contattare l'Amministratore dell sistema");
                    alert.showAndWait();
                    }
                   
                } else {
                    Alert alert = new Alert(Alert.AlertType.ERROR);
                    alert.initOwner(dialogStage);
                    alert.setTitle("Errore!");
                    alert.setHeaderText("Errore!");
                    alert.setContentText("Errore nel tentativo di completare scadenzario!\n Si prega di provare di nuovo\n Se il problema persiste si prega di contattare l'Amministratore dell sistema");
                    alert.showAndWait();
                }
                break;              
        }
    }
    
    @FXML
    public void handleCancel(){
        okClicked = false;
        dialogStage.close();
    } 
    
    @FXML 
    public String handleUpdateReminderAfterSettingAsEntry() throws ParseException{
         ArchivesPortService archiveService = new ArchivesPortService();
        ArchivesPort archivesPort = archiveService.getArchivesPortSoap11();
         //delete this reminder
                    thisReminder.setCompleted("1");
                    thisReminder.setDescription("(Scritto in prima nota) "+thisReminder.getDescription());
                    thisReminder.setValue(NumberFormat.getCurrencyInstance(Locale.ITALY).parse(thisReminder.getValue()).toString());
                    DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");
                    LocalDate date = LocalDate.parse(thisReminder.getDate(), formatter); 
                 thisReminder.setDate(date.toString()+" 00:00:00");
                    UpdateReminderRequest updateRequest = new UpdateReminderRequest();
                    updateRequest.setReminder(thisReminder);
                    updateRequest.setArchiveName(PrimaNotaFacileClient.getCurrentArchive() + ".sqlite3");
                    UpdateReminderResponse updateResponse = archivesPort.updateReminder(updateRequest);
                
                    return updateResponse.getResponse();
    }
   
    @FXML
    public void handleRadioButtonClicks(){
        //method to handle radio button filters
        //will modify the state of the combo box banca to enabled only if it is selected radio button solo banca, disable otherwise
        if (radioNuovaEntrataBanca.isSelected() == true || radioNuovaUscitaBanca.isSelected() == true ){
            comboBoxBanca.setDisable(false);
            comboBoxBanca.getSelectionModel().clearSelection();
            //selected first element
            comboBoxBanca.getSelectionModel().select(0);
        }else{
            comboBoxBanca.setDisable(true);
            comboBoxBanca.getSelectionModel().clearSelection();
        }
        
    }
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        
        //set values for banca combobox
         ArchivesPortService archiveService = new ArchivesPortService();
        ArchivesPort archivesPort = archiveService.getArchivesPortSoap11();
         GetAllBankRequest allBanksRequest = new GetAllBankRequest();
        allBanksRequest.setArchiveName(PrimaNotaFacileClient.getCurrentArchive() + ".sqlite3");
        GetAllBankResponse allBanksResponse = archivesPort.getAllBank(allBanksRequest);
        //si pergjigje marrim nje liste me banca
         //delete first all elements
        comboBoxBanca.getItems().remove(0, comboBoxBanca.getItems().size());
        comboBankPrimaNotaHash.clear();
        List<Bank> allBanks = allBanksResponse.getAllBankResponse();
        ObservableList<String> comboBanks = comboBoxBanca.getItems();
        for (Bank bnk : allBanks) {
            comboBanks.add(bnk.getName());
            comboBankPrimaNotaHash.put(bnk.getName(),bnk.getBankId());
        }
        //disabled by default
        comboBoxBanca.setDisable(true);
        //initialize radios
         completeReminderRadios = new ToggleGroup();
         radioNessuna.setToggleGroup(completeReminderRadios);
         radioNuovaUsciteaCassa.setToggleGroup(completeReminderRadios);
         radioNuovaEntrataCassa.setToggleGroup(completeReminderRadios);
         radioNuovaUscitaBanca.setToggleGroup(completeReminderRadios);
         radioNuovaEntrataBanca.setToggleGroup(completeReminderRadios);
         radioNuovoFuoriCassa.setToggleGroup(completeReminderRadios);
          //event handlers for radio buttons in prima nota
        radioNessuna.setOnAction((event)-> {
            handleRadioButtonClicks();
        });
        
       radioNuovaUsciteaCassa.setOnAction((event)-> {
            handleRadioButtonClicks();
        });
        
        radioNuovaEntrataCassa.setOnAction((event)-> {
            handleRadioButtonClicks();
        });
        radioNuovaUscitaBanca.setOnAction((event)-> {
            handleRadioButtonClicks();
        });
        radioNuovaEntrataBanca.setOnAction((event)-> {
            handleRadioButtonClicks();
        });
        radioNuovoFuoriCassa.setOnAction((event)-> {
            handleRadioButtonClicks();
        });
         
         //select nessuna radio
         completeReminderRadios.selectToggle(radioNessuna);
         //call method of radios for the first time
         handleRadioButtonClicks();
         
    }    
    
   
}
