/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package primanotafacileclient;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXCheckBox;
import com.jfoenix.controls.JFXDatePicker;
import com.jfoenix.controls.JFXTextArea;
import com.jfoenix.controls.JFXTextField;
import java.io.IOException;
import java.text.NumberFormat;
import java.text.ParseException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.scene.text.Text;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.event.ActionEvent;
import javafx.scene.control.Label;
import javafx.scene.layout.AnchorPane;
import javafx.util.StringConverter;
import net.unnitech.primanotafacile.*;


/**
 *
 * @author programues
 */
public class NewEntry {

    @FXML
    private AnchorPane anchorPane;

    @FXML
    private JFXDatePicker btnNewPrimaNotaData;

    @FXML
    private JFXTextArea btnNewPrimaNotaOperazione;

    @FXML
    private JFXTextField btnNewScadenzarioImporto;

    @FXML
    private JFXButton btnSave;

    @FXML
    private JFXButton btnClose;

    @FXML
    private ComboBox<String> comboBoxControparte;

    @FXML
    private JFXTextField entrateCassaNewPrimaNota;

    @FXML
    private JFXTextField usciteCassaNewPrimaNota;

    @FXML
    private JFXTextField fuoriCassaNewPrimaNota;

    @FXML
    private JFXTextField entrateBancaNewPrimaNota;

    @FXML
    private JFXTextField usciteBancaNewPrimaNota;

    @FXML
    private ComboBox<String> comboBoxBanca;
      
    @FXML
    private ComboBox<String> comboBoxCategoria;

    @FXML
    private Label textViewEntryId;

    @FXML
    private ImageView addNewBank;
    
    @FXML
    private ImageView addNewOperationType;

    private Stage dialogStage;
    private EntryForTable entryForTable;
    private Entry entry;
    private boolean okClicked = false;
    private LinkedHashMap<String, Integer> comboContact = new LinkedHashMap<>();
    private LinkedHashMap<String, String> comboBank = new LinkedHashMap<>();
    private LinkedHashMap<String, Integer> comboCategory = new LinkedHashMap<>();

    /**
     * Initializes the controller class. This method is automatically called
     * after the fxml file has been loaded.
     */
    @FXML
    private void initialize() {

        btnNewPrimaNotaData.setConverter(new StringConverter<LocalDate>() {
            String pattern = "dd-MM-yyyy";
            DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern(pattern);

            {
                btnNewPrimaNotaData.setPromptText(pattern.toLowerCase());
            }

            @Override
            public String toString(LocalDate date) {
                if (date != null) {
                    return dateFormatter.format(date);
                } else {
                    return "";
                }
            }

            @Override
            public LocalDate fromString(String string) {
                if (string != null && !string.isEmpty()) {
                    return LocalDate.parse(string, dateFormatter);
                } else {
                    return null;
                }
            }
        });

        ArchivesPortService archiveService = new ArchivesPortService();
        ArchivesPort archivesPort = archiveService.getArchivesPortSoap11();
        GetAllContactsRequest allContactsRequest = new GetAllContactsRequest();
        allContactsRequest.setArchiveName(PrimaNotaFacileClient.getCurrentArchive() + ".sqlite3");
        GetAllContactsResponse allContactsResponse = archivesPort.getAllContacts(allContactsRequest);

        //si pergjigje marrim nje liste me contacte
        //delete first all elements
        comboBoxControparte.getItems().remove(0, comboBoxControparte.getItems().size());
        comboContact.clear();
        List<Contact> allcontacs = allContactsResponse.getAllContactsResponse();
        ObservableList<String> comboContacts = comboBoxControparte.getItems();
        for (Contact contact : allcontacs) {
            comboContacts.add(contact.getFullName());
            comboContact.put(contact.getFullName(), Integer.parseInt(contact.getContactId()));
        }

        GetAllBankRequest allBanksRequest = new GetAllBankRequest();
        allBanksRequest.setArchiveName(PrimaNotaFacileClient.getCurrentArchive() + ".sqlite3");
        GetAllBankResponse allBanksResponse = archivesPort.getAllBank(allBanksRequest);

        //si pergjigje marrim nje liste me contacte
         //delete first all elements
        comboBoxBanca.getItems().remove(0, comboBoxBanca.getItems().size());
        comboBank.clear();
        List<Bank> allBanks = allBanksResponse.getAllBankResponse();
        ObservableList<String> comboBanks = comboBoxBanca.getItems();
        comboBanks.add("Nessuno");
        for (Bank bnk : allBanks) {
            comboBanks.add(bnk.getName());
            comboBank.put(bnk.getName(), bnk.getBankId());
        }
        comboBoxBanca.getSelectionModel().select("Nessuno");
        
        GetAllOperationTypesRequest allOperationTypesRequest = new GetAllOperationTypesRequest();
        allOperationTypesRequest.setArchiveName(PrimaNotaFacileClient.getCurrentArchive() + ".sqlite3");
        GetAllOperationTypesResponse allOperationTypesResponse = archivesPort.getAllOperationTypes(allOperationTypesRequest);
        
         //delete first all elements
        comboBoxCategoria.getItems().remove(0, comboBoxCategoria.getItems().size());
        comboCategory.clear();
        List<OperationType> allOperationTypes = allOperationTypesResponse.getAllOperationTypesResponse();
        ObservableList<String> comboValuesCategory = comboBoxCategoria.getItems();
        comboValuesCategory.add("Generico");
        for (OperationType ot : allOperationTypes){
            comboValuesCategory.add(ot.getName());
            comboCategory.put(ot.getName(), Integer.parseInt(ot.getOperationTypeId()));
        }
         comboBoxCategoria.getSelectionModel().select("Generico");
        anchorPane.requestFocus();
    }

    @FXML
    void handleRemoveFocusNewEntry() {
        anchorPane.requestFocus();
    }

    @FXML
    void handleComboBank() {
        /*
        if(comboBoxBanca.getSelectionModel().getSelectedItem() != "Nessuno")
        {
            entrateCassaNewPrimaNota.setDisable(true);
            usciteCassaNewPrimaNota.setDisable(true);
            entrateBancaNewPrimaNota.setDisable(false);
            usciteBancaNewPrimaNota.setDisable(false);
        }
        else
        {
            entrateCassaNewPrimaNota.setDisable(false);
            usciteCassaNewPrimaNota.setDisable(false);
            entrateBancaNewPrimaNota.setDisable(true);
            usciteBancaNewPrimaNota.setDisable(true);
        }*/
    }

    /**
     * Sets the stage of this dialog.
     *
     * @param dialogStage
     */
    public void setDialogStage(Stage dialogStage) {
        this.dialogStage = dialogStage;
        this.dialogStage.getScene().getStylesheets().add("assets/style/style.css");
    }

    private final Stage getDialogStage() {
        return dialogStage;
    }

    /**
     * Sets the Reminder to be edited in the dialog.
     *
     * @param newEntry
     * @throws java.text.ParseException
     * 
     */
    public void setEntry(Entry newEntry) throws ParseException {
        this.entry = newEntry;
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");
        //CharSequence cs = reminder.getDate();
        if (newEntry.getEntryDate() != null) {
            LocalDate date = LocalDate.parse(newEntry.getEntryDate(), formatter);

            btnNewPrimaNotaData.setValue(date);
            btnNewPrimaNotaData.setPromptText(newEntry.getEntryDate());

        } //        btnNewPrimaNotaData.setPromptText(newEntry.getEntryDate());
        else {
            //today's date
            LocalDate today = LocalDate.now();
            btnNewPrimaNotaData.setValue(today);       
            btnNewPrimaNotaData.setPromptText(today.format(formatter));
        }
        //we're gonna set every value even if it's only cash or only bank
        if(newEntry.getOutgoingValueBank()== null){
            usciteBancaNewPrimaNota.setText("0");
        }else{
             usciteBancaNewPrimaNota.setText(NumberFormat.getCurrencyInstance(Locale.ITALY).parse(newEntry.getOutgoingValueBank()).toString());
        }
       
        if(newEntry.getIncomingValueBank()== null){
            entrateBancaNewPrimaNota.setText("0");
        }else{
            entrateBancaNewPrimaNota.setText(NumberFormat.getCurrencyInstance(Locale.ITALY).parse(newEntry.getIncomingValueBank()).toString());
        }
        
        if(newEntry.getOutgoingValueCash()==null){
            usciteCassaNewPrimaNota.setText("0");
        }else{
            usciteCassaNewPrimaNota.setText(NumberFormat.getCurrencyInstance(Locale.ITALY).parse(newEntry.getOutgoingValueCash()).toString());
        }
        
        if(newEntry.getIncomingValueCash() == null){
            entrateCassaNewPrimaNota.setText("0");
        }else{
            entrateCassaNewPrimaNota.setText(NumberFormat.getCurrencyInstance(Locale.ITALY).parse(newEntry.getIncomingValueCash()).toString());
        }
        
        if(newEntry.getOutCash()== null) {
            fuoriCassaNewPrimaNota.setText("0");
        } else {
            fuoriCassaNewPrimaNota.setText(NumberFormat.getCurrencyInstance(Locale.ITALY).parse(newEntry.getOutCash()).toString());
        }
        
        btnNewPrimaNotaOperazione.setText(newEntry.getDescription());
        
        if (newEntry.getIdContact() != null && !"0".equals(newEntry.getIdContact())) {
             int n = 0;
            for (Map.Entry<String, Integer> entry : comboContact.entrySet()) {
                //since combo box select index starts from 0 we're gonna increment n after checking for specified value in hashmap
                //in the main controller we display in table the full name of the contact so we're searching in this hashmap for key
                 if (entry.getValue().equals(Integer.parseInt(newEntry.getIdContact()))) {

                     break;
                 }

                 n++;
             }
            comboBoxControparte.getSelectionModel().select(n);
        }else
        {
            comboBoxControparte.getSelectionModel().clearSelection();
        }  
        
       if (newEntry.getIdOperationType()!= null && !"0".equals(newEntry.getIdOperationType())){
            int z = 0;
        for (Map.Entry<String, Integer> entry : comboCategory.entrySet()) {
            //since combo box select index starts from 0 we're gonna increment n after checking for specified value in hashmap
            //in the main controller we display in table the full name of the contact so we're searching in this hashmap for key
            if (entry.getKey().equals(newEntry.getIdOperationType())) {

                break;
            }

            z++;
        }
        comboBoxCategoria.getSelectionModel().select(z);
       }else{
           comboBoxCategoria.getSelectionModel().select(0);
       }
        
                
        String bank = "Nessuno";
        for (Map.Entry<String, String> entry : comboBank.entrySet()) {
            //since combo box select index starts from 0 we're gonna increment n after checking for specified value in hashmap
            //in the main controller we display in table the full name of the contact so we're searching in this hashmap for key
            if (entry.getValue().equals(newEntry.getIdBank())) {
                bank = entry.getKey();
                break;
            }
        }
        comboBoxBanca.getSelectionModel().select(bank);
        textViewEntryId.setText(entry.getEntryId());

    }

    /**
     * Returns true if the user clicked OK, false otherwise.
     *
     * @return
     */
    public boolean isOkClicked() {
        return okClicked;
    }

    /**
     * Called when the user clicks ok.
     */
    @FXML
    private void handleOk() {
        if (isInputValid()) {
            //handle state of edit or new by checking value of  textViewScadenzarioId
            if ("0".equals(textViewEntryId.getText())) {
                //no id add new
                //we're gonna add a new reminder
                //create a new reminer class and set the textfield values
                Entry newEntry = new Entry();

                ArchivesPortService archiveService = new ArchivesPortService();
                ArchivesPort archivesPort = archiveService.getArchivesPortSoap11();              
                newEntry.setEntryDate(btnNewPrimaNotaData.getValue().toString() + " 00:00:00");
                newEntry.setDescription(btnNewPrimaNotaOperazione.getText());
                newEntry.setOutCash(fuoriCassaNewPrimaNota.getText());
                newEntry.setDocumentNumber("1");
                
                if(comboBoxCategoria.getSelectionModel().getSelectedItem() != null && comboBoxCategoria.getSelectionModel().getSelectedItem() != "Generico"){
                    newEntry.setIdOperationType(String.valueOf(comboCategory.get(comboBoxCategoria.getSelectionModel().getSelectedItem())));
               }else{
                    newEntry.setIdOperationType("0");
                }
                
                 if(comboBoxControparte.getSelectionModel().getSelectedItem() != null && comboBoxControparte.getSelectionModel().getSelectedItem() != "Nessuno"){
                    newEntry.setIdContact(String.valueOf(comboContact.get(comboBoxControparte.getSelectionModel().getSelectedItem())));
                }else{
                    newEntry.setIdContact("0");
                } 
               

                if (comboBoxBanca.getSelectionModel().getSelectedItem() != null && comboBoxBanca.getSelectionModel().getSelectedItem() != "Nessuno") {
                 
                    newEntry.setIdBank(comboBank.get(comboBoxBanca.getSelectionModel().getSelectedItem()));
                } else {
                   
                    newEntry.setIdBank("0");
                }
                
                   newEntry.setOutgoingValueBank(usciteBancaNewPrimaNota.getText());
                    newEntry.setIncomingValueBank(entrateBancaNewPrimaNota.getText());
                     newEntry.setOutgoingValueCash(usciteCassaNewPrimaNota.getText());
                    newEntry.setIncomingValueCash(entrateCassaNewPrimaNota.getText());
                
                AddEntryRequest addEntry = new AddEntryRequest();
                addEntry.setEntry(newEntry);
                addEntry.setArchiveName(PrimaNotaFacileClient.getCurrentArchive() + ".sqlite3");
                AddEntryResponse addEntryResponse = archivesPort.addEntry(addEntry);
                String pergjige = addEntryResponse.getResponse();
                if ("OK".equals(pergjige)) {
                    okClicked = true;
                    dialogStage.close();
                } else {
                    Alert alert = new Alert(AlertType.ERROR);
                    alert.initOwner(dialogStage);
                    alert.setTitle("Errore!");
                    alert.setHeaderText("Errore!");
                    alert.setContentText("Errore nel tentativo di aggiungere un nuovo scadenzario!\\Si prega di provare di nuovo\\n Se il problema persiste si prega di contattare l'Amministratore dell sistema");
                    alert.showAndWait();
                }
            } else {
                //textViewScadenzarioID has value , we're in edit state
                //create the object reminder and set also the reminder id in order to update
                Entry existingEntry = new Entry();
                existingEntry.setEntryId(textViewEntryId.getText());
                existingEntry.setEntryDate(btnNewPrimaNotaData.getValue().toString() + " 00:00:00");
                existingEntry.setDescription(btnNewPrimaNotaOperazione.getText());
                existingEntry.setOutCash(fuoriCassaNewPrimaNota.getText());
                existingEntry.setDocumentNumber("1");
                
                 if(comboBoxCategoria.getSelectionModel().getSelectedItem() != null && comboBoxCategoria.getSelectionModel().getSelectedItem() != "Generico"){
                    existingEntry.setIdOperationType(String.valueOf(comboCategory.get(comboBoxCategoria.getSelectionModel().getSelectedItem())));
                }else{
                    existingEntry.setIdOperationType("0");
                }
                
                 if(comboBoxControparte.getSelectionModel().getSelectedItem() != null ){
                    existingEntry.setIdContact(String.valueOf(comboContact.get(comboBoxControparte.getSelectionModel().getSelectedItem())));
                }else{
                    existingEntry.setIdContact("0");
                } 
                 
                 if(comboBoxBanca.getSelectionModel().getSelectedItem() != null && !"Nessuno".equals(comboBoxBanca.getSelectionModel().getSelectedItem())){
                    existingEntry.setIdBank(String.valueOf(comboBank.get(comboBoxBanca.getSelectionModel().getSelectedItem())));
                }else{
                    existingEntry.setIdBank("0");
                } 
                

                existingEntry.setOutgoingValueBank(usciteBancaNewPrimaNota.getText());
                existingEntry.setIncomingValueBank(entrateBancaNewPrimaNota.getText());
                existingEntry.setOutgoingValueCash(usciteCassaNewPrimaNota.getText());
                existingEntry.setIncomingValueCash(entrateCassaNewPrimaNota.getText());
               

                //set class to search from soap server
                ArchivesPortService archiveService = new ArchivesPortService();
                ArchivesPort archivesPort = archiveService.getArchivesPortSoap11();
                UpdateEntryRequest updateEntry = new UpdateEntryRequest();
                updateEntry.setEntry(existingEntry);
                updateEntry.setArchiveName(PrimaNotaFacileClient.getCurrentArchive() + ".sqlite3");
                UpdateEntryResponse updateEntryResponse = archivesPort.updateEntry(updateEntry);
                String pergjige = updateEntryResponse.getResponse();
                if ("OK".equals(pergjige)) {
                    okClicked = true;
                    dialogStage.close();
                } else {
                    Alert alert = new Alert(AlertType.ERROR);
                    alert.initOwner(dialogStage);
                    alert.setTitle("Errore!");
                    alert.setHeaderText("Errore!");
                    alert.setContentText("Errore nel tentativo di modificare il scadenzario!\\Si prega di provare di nuovo\\n Se il problema persiste si prega di contattare l'Amministratore dell sistema");
                    alert.showAndWait();
                }
            }
        }
    }

    /**
     * Called when the user clicks cancel.
     */
    @FXML
    private void handleCancel() {
        dialogStage.close();
    }

    
    @FXML
    private void handleAddNewOperationType(){
         OperationType newOperationType = new OperationType();
        //set the reminder id to 0 to know that's new reminder
        newOperationType.setOperationTypeId("0");
        boolean okClicked = showOperationTypeEditDialog(newOperationType);
        if (okClicked) {
            //if result true refresh table 
            //or ADD an info  if you want
            initialize();
        }
    }
    
    @FXML
    public boolean showOperationTypeEditDialog(OperationType operationType){
        try {

            // Load the fxml file and create a new stage for the popup dialog.
            FXMLLoader loader = new FXMLLoader(getClass().getResource("NewOperationType.fxml"));
            Parent root2 = (Parent) loader.load();

            // Create the dialog Stage.
            Stage dialogStage = new Stage();
            dialogStage.setTitle("Categoria");
            dialogStage.initModality(Modality.APPLICATION_MODAL);
            dialogStage.initOwner(addNewOperationType.getScene().getWindow());
            Scene scene = new Scene(root2);
            dialogStage.setScene(scene);

            // Set the person into the controller.
            NewOperationType controller = loader.getController();
            controller.setDialogStage(dialogStage);
            controller.setOperationType(operationType);
            // Show the dialog and wait until the user closes it
            dialogStage.showAndWait();

            return controller.isOkClicked();
        } catch (IOException e) {
            System.out.println(e.getMessage());
            return false;
        }
    }
    @FXML
    private void handleAddNewBank() {
        Bank newBank = new Bank();
        //set the reminder id to 0 to know that's new reminder
        newBank.setBankId("0");
        boolean okClicked = showBankEditDialog(newBank);
        if (okClicked) {
            //if result true refresh table 
            //or ADD an info  if you want
            initialize();
        }
    }

    @FXML
    public boolean showBankEditDialog(Bank bank) {
        try {

            // Load the fxml file and create a new stage for the popup dialog.
            FXMLLoader loader = new FXMLLoader(getClass().getResource("NewBank.fxml"));
            Parent root2 = (Parent) loader.load();

            // Create the dialog Stage.
            Stage dialogStage = new Stage();
            dialogStage.setTitle("Bank");
            dialogStage.initModality(Modality.APPLICATION_MODAL);
            dialogStage.initOwner(addNewBank.getScene().getWindow());
            Scene scene = new Scene(root2);
            dialogStage.setScene(scene);

            // Set the person into the controller.
            NewBank controller = loader.getController();
            controller.setDialogStage(dialogStage);
            controller.setBank(bank);

            // Show the dialog and wait until the user closes it
            dialogStage.showAndWait();

            return controller.isOkClicked();
        } catch (IOException e) {
            System.out.println(e.getMessage());
            return false;
        }
    }

    private boolean isInputValid() {
        String errorMessage = "";
        Double inBank=0.0;
        Double outBank=0.0;
        Double inCash=0.0;
        Double outCash=0.0;
        Double fuoriCassa=0.0;
        //TO DO:
        //User should always set a description
        //User should always choose a 

        if (btnNewPrimaNotaOperazione.getText() == null) {
            errorMessage = "Please set description";
        }

        if (btnNewPrimaNotaData.getValue() == null) {
            errorMessage = "Please set the date at the right format: dd-mm-yyyy";
        }
        
         if (comboBoxCategoria.getSelectionModel().getSelectedItem() == null) {
            errorMessage = "Si prega di selezionare una voce nell'elenco delle categorie\n";
        }
         
        //checking values of input bank and cash
        try{
           fuoriCassa =  Double.parseDouble(fuoriCassaNewPrimaNota.getText());
           
        }
        catch(NumberFormatException nr){
              errorMessage ="Si prega di inserire un valore valido per il campo fuori cassa \n";
        }
        catch(NullPointerException e){
            errorMessage ="Si prega di inserire un valore per il campo fuori cassa \n";
        }
        try{
            inBank    =   Double.parseDouble(entrateBancaNewPrimaNota.getText());
        //check if bank selected if value for bank is > 0
            if (inBank > 0.00){
                  if (comboBoxBanca.getSelectionModel().getSelectedItem() == null || "Nessuno".equals(comboBoxBanca.getSelectionModel().getSelectedItem())) {
                    errorMessage = "Si prega di selezionare una banca nell'elenco delle bance\n";
                }
                         
             } else if (inBank == 0.0){
                //check banc selection
                 if (comboBoxBanca.getSelectionModel().getSelectedItem() != null && !"Nessuno".equals(comboBoxBanca.getSelectionModel().getSelectedItem())&& outBank == 0.0) {
                    errorMessage = "Si prega di deselezionare la banca nell'elenco delle bance\n";
                }
            
            } 
             else{
               //values lower than 0. wont allow
               errorMessage = "Si prega di inserire un valore valido per il campo entrate banca\n";
               
             }
        
        }catch(NumberFormatException nr){
              errorMessage ="Si prega di inserire un valore valido per il campo entrate banca \n";
        }
        catch(NullPointerException e){
            errorMessage ="Si prega di inserire un valore per il campo entrate banca \n";
        }
          try{ 
                outBank   =   Double.parseDouble(usciteBancaNewPrimaNota.getText());
             //check if bank selected if value for bank is > 0
             if (outBank > 0.00){
                 if (comboBoxBanca.getSelectionModel().getSelectedItem() == null || "Nessuno".equals(comboBoxBanca.getSelectionModel().getSelectedItem())) {
                    errorMessage = "Si prega di selezionare una banca nell'elenco delle bance\n";
                }
         
             }   else if (outBank == 0.0){
                //check banc selection
                 if (comboBoxBanca.getSelectionModel().getSelectedItem() != null && !"Nessuno".equals(comboBoxBanca.getSelectionModel().getSelectedItem()) && inBank == 0.0) {
                    errorMessage = "Si prega di deselezionare la banca nell'elenco delle bance\n";
                }
            
            } 
             else{
               //values lower than 0. wont allow
               errorMessage = "Si prega di inserire un valore valido per il campo uscite banca\n";
               
             }
              
          }
          catch(NumberFormatException nr){
              errorMessage ="Si prega di inserire un valore valido per il campo uscite banca \n";
        }
        catch(NullPointerException e){
            errorMessage ="Si prega di inserire un valore per il campo uscite banca \n";
        }
          
         try{
                inCash =      Double.parseDouble(entrateCassaNewPrimaNota.getText());
                if (inCash < 0.0){
                 errorMessage ="Si prega di inserire un valore valido per il campo entrate cassa \n";
                }
             
         }catch(NumberFormatException nr){
              errorMessage ="Si prega di inserire un valore valido per il campo entrate cassa \n";
        }
        catch(NullPointerException e){
            errorMessage ="Si prega di inserire un valore per il campo entrate cassa \n";
        }
        try{  
            outCash =     Double.parseDouble(usciteCassaNewPrimaNota.getText());
             if (outCash < 0.0){
                 errorMessage ="Si prega di inserire un valore valido per il campo entrate cassa \n";
                }
        }catch(NumberFormatException nr){
              errorMessage ="Si prega di inserire un valore valido per il campo uscite cassa \n";
        }
        catch(NullPointerException e){
            errorMessage ="Si prega di inserire un valore per il campo uscite cassa \n";
        }
        
        //check to disallow values both for cassa and banca
        if(inBank > 0.0 || outBank > 0.0){
            //check for cash
            if(inCash > 0.0 || outCash > 0.0){
                errorMessage ="Non è possibile inserire valori per banca e cassa in un unico movimento \n";
            }
        }
        
        if(inCash > 0.0 || outCash > 0.0){
            //check for bank
            if(inBank > 0.0 || outBank > 0.0){
                errorMessage ="Non è possibile inserire valori per cassa e banca in un unico movimento \n";
            }
        }
        

        if (errorMessage.length() == 0) {
            return true;
        } else {
            // Show the error message.
            Alert alert = new Alert(AlertType.ERROR);
            alert.initOwner(dialogStage);
            alert.setTitle("Campi non validi");
            alert.setHeaderText("Si prega di correggere i campi non validi");
            Text text = new Text(errorMessage);
            text.setWrappingWidth(400);
            alert.getDialogPane().setContent(text);
            alert.showAndWait();

            return false;
        }
    }

}
