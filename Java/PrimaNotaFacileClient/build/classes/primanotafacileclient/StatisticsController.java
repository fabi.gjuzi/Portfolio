/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package primanotafacileclient;

import java.net.URL;
import java.text.NumberFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Spinner;
import javafx.scene.control.SpinnerValueFactory;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;
import javafx.stage.Stage;
import javafx.util.Callback;
import net.unnitech.primanotafacile.ArchivesPort;
import net.unnitech.primanotafacile.ArchivesPortService;
import net.unnitech.primanotafacile.EntryBalance;
import net.unnitech.primanotafacile.GetAllEntriesCustomRequest;
import net.unnitech.primanotafacile.GetAllEntriesCustomResponse;

/**
 * FXML Controller class
 *
 * @author programues
 */
public class StatisticsController implements Initializable {

     @FXML
    private Spinner<Integer> spinnerPrimaNotaAnno;

    @FXML
    private TableView<StatisticsForTable> tableViewStatistiche;

    @FXML
    private TableColumn<StatisticsForTable, String> tableColumnMese;

    

    @FXML
    private TableColumn<StatisticsForTable, String> tableColumnEntrateCassa;

    @FXML
    private TableColumn<StatisticsForTable, String> tableColumnUsciteCassa;

    @FXML
    private TableColumn<StatisticsForTable, String> tableColumnSaldoCassa;

    @FXML
    private TableColumn<StatisticsForTable, String> tableColumnEntrateBanca;

    @FXML
    private TableColumn<StatisticsForTable, String> tableColumnUsciteBanca;

    @FXML
    private TableColumn<StatisticsForTable, String> tableColumnSaldoBanca;

    @FXML
    private TableColumn<StatisticsForTable, String> tableColumnSaldoTotale;
    
    @FXML
    private TableColumn<StatisticsForTable, String> tableColumnFuoriCassa;
    
    private Stage dialogStage;
    
    public void handleStatistics(){
        
        int yearSelected = spinnerPrimaNotaAnno.getValue();
            
        
             String months[] = {"Gennaio","Febbraio","Marzo","Aprile","Maggio","Giugno","Luglio","Agosto","Settembre","Ottobre","Novembre","Dicembre"};
             ObservableList<StatisticsForTable> data = FXCollections.observableArrayList();
             LocalDate iThStartDate;
             LocalDate iThEndDate;
             ArchivesPortService archiveService = new ArchivesPortService();
            ArchivesPort archivesPort = archiveService.getArchivesPortSoap11(); 
            for (int i = 1; i<=12;i++){
                iThStartDate = LocalDate.of(yearSelected, i, 1);
                iThEndDate = LocalDate.of(yearSelected,i,iThStartDate.lengthOfMonth());
                String query = "Select *, (incoming_value_bank - outgoing_value_bank) as balance_bank, (incoming_value_cash - outgoing_value_cash) as balance_cash from entry Where 1=1";
                query+=" AND entry_date BETWEEN "+"'"+iThStartDate.toString()+" 00:00:00'"+" AND "+"'"+iThEndDate.toString()+" 00:00:00'";
                query += " order by entry_date asc;";
                System.out.println(query);   
                GetAllEntriesCustomRequest allEntriesRequest = new GetAllEntriesCustomRequest();
                allEntriesRequest.setArchiveName(PrimaNotaFacileClient.getCurrentArchive() + ".sqlite3");
                allEntriesRequest.setQueryClient(query);
                GetAllEntriesCustomResponse allEntriesResponse = archivesPort.getAllEntriesCustom(allEntriesRequest);           
                List<EntryBalance> allEntries = allEntriesResponse.getAllEntriesCustomResponse(); 
                Double TotalCashOut = 0.0;
                Double TotalIncomingValueCash = 0.0;
                Double TotalOutgoingValueCash = 0.0;
                Double TotalIncomingValueBank = 0.0;
                Double TotalOutgoingValueBank = 0.0;
                Double TotalBalanceCash = 0.0;
                Double TotalBalanceBank = 0.0;
                for(EntryBalance entry : allEntries){
                    //add records
                    TotalCashOut += Double.parseDouble(entry.getOutCash());
                    TotalIncomingValueCash += Double.parseDouble(entry.getIncomingValueCash());
                    TotalOutgoingValueCash += Double.parseDouble(entry.getOutgoingValueCash());
                    TotalIncomingValueBank += Double.parseDouble(entry.getIncomingValueBank());
                    TotalOutgoingValueBank += Double.parseDouble(entry.getOutgoingValueBank());
                    TotalBalanceCash += Double.parseDouble(entry.getBalanceCash());
                    TotalBalanceBank += Double.parseDouble(entry.getBalanceBank());                    
                }
                
               //adding values to Observable list of table
                data.add(new StatisticsForTable(months[i-1],NumberFormat.getCurrencyInstance(Locale.ITALY).format(TotalCashOut),NumberFormat.getCurrencyInstance(Locale.ITALY).format(TotalIncomingValueCash)
                ,NumberFormat.getCurrencyInstance(Locale.ITALY).format(TotalOutgoingValueCash),NumberFormat.getCurrencyInstance(Locale.ITALY).format(TotalBalanceCash)
                ,NumberFormat.getCurrencyInstance(Locale.ITALY).format(TotalIncomingValueBank),NumberFormat.getCurrencyInstance(Locale.ITALY).format(TotalOutgoingValueBank)
                ,NumberFormat.getCurrencyInstance(Locale.ITALY).format(TotalBalanceBank),NumberFormat.getCurrencyInstance(Locale.ITALY).format(TotalBalanceBank+TotalBalanceCash)));
                
                
            }

       
        
        tableViewStatistiche.setItems(data);
    
    }
    
     /**
     * Sets the stage of this dialog.
     *
     * @param dialogStage
     */
    public void setDialogStage(Stage dialogStage) {
        this.dialogStage = dialogStage;
        this.dialogStage.getScene().getStylesheets().add("assets/style/style.css");
    }

    private final Stage getDialogStage() {
        return dialogStage;
    }
    
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        
        //initialize spinner
        LocalDate today = LocalDate.now();
        SpinnerValueFactory valueFactory = new SpinnerValueFactory.IntegerSpinnerValueFactory(1900, 2100, today.getYear());
        spinnerPrimaNotaAnno.setValueFactory(valueFactory);
        
        //add event handler for spinner
        spinnerPrimaNotaAnno.setOnMouseClicked((event)-> {
            handleStatistics();
        });
        
        //set property value factory for columns. here we're doing it in code not in FXML like in MainScreenController
        tableColumnMese.setCellValueFactory(new PropertyValueFactory<>("tableColumnMese"));
        tableColumnFuoriCassa.setCellValueFactory(new PropertyValueFactory<>("tableColumnFouriCassa"));
        tableColumnEntrateCassa.setCellValueFactory(new PropertyValueFactory<>("tableColumnEntrateCassa"));
        tableColumnUsciteCassa.setCellValueFactory(new PropertyValueFactory<>("tableColumnUsciteCassa"));
        tableColumnSaldoCassa.setCellValueFactory(new PropertyValueFactory<>("tableColumnSaldoCassa"));
        tableColumnEntrateBanca.setCellValueFactory(new PropertyValueFactory<>("tableColumnEntrateBanca"));
        tableColumnUsciteBanca.setCellValueFactory(new PropertyValueFactory<>("tableColumnUsciteBanca"));
        tableColumnSaldoBanca.setCellValueFactory(new PropertyValueFactory<>("tableColumnSaldoBanca"));
        tableColumnSaldoTotale.setCellValueFactory(new PropertyValueFactory<>("tableColumnSaldoTotale"));
        
        //styling of columns
         tableColumnFuoriCassa.setCellFactory(new Callback<TableColumn<StatisticsForTable, String>, TableCell<StatisticsForTable, String>>() {
        @Override
            public TableCell<StatisticsForTable, String> call(TableColumn<StatisticsForTable, String> param) {
                TableCell<StatisticsForTable, String> cell = new TableCell<StatisticsForTable, String>() {
                 private Text text = new Text();

                {
                 text.wrappingWidthProperty().bind(tableColumnFuoriCassa.widthProperty());
                    
               
                }          
                    @Override
                    public void updateItem(final String item, boolean empty) {
                       super.updateItem(item, empty);

                        if (empty || item == null) {
                            setGraphic(null);
                        } else {
                            text.setText(item);
                            setGraphic(text);
                            text.setStyle("-fx-font-size: 8pt;");
                            text.setTextAlignment(TextAlignment.RIGHT);


                        }                    
                    }
                };
                return cell;
            }
        });
         
         tableColumnEntrateCassa.setCellFactory(new Callback<TableColumn<StatisticsForTable, String>, TableCell<StatisticsForTable, String>>() {
        @Override
            public TableCell<StatisticsForTable, String> call(TableColumn<StatisticsForTable, String> param) {
                TableCell<StatisticsForTable, String> cell = new TableCell<StatisticsForTable, String>() {
                 private Text text = new Text();

                {
                 text.wrappingWidthProperty().bind(tableColumnEntrateCassa.widthProperty());
                    
               
                }          
                    @Override
                    public void updateItem(final String item, boolean empty) {
                       super.updateItem(item, empty);

                        if (empty || item == null) {
                            setGraphic(null);
                        } else {
                            text.setText(item);
                            setGraphic(text);
                            text.setStyle("-fx-font-size: 8pt;");
                            text.setTextAlignment(TextAlignment.RIGHT);


                        }                    
                    }
                };
                return cell;
            }
        }); 
         
          tableColumnUsciteCassa.setCellFactory(new Callback<TableColumn<StatisticsForTable, String>, TableCell<StatisticsForTable, String>>() {
        @Override
            public TableCell<StatisticsForTable, String> call(TableColumn<StatisticsForTable, String> param) {
                TableCell<StatisticsForTable, String> cell = new TableCell<StatisticsForTable, String>() {
                 private Text text = new Text();

                {
                 text.wrappingWidthProperty().bind(tableColumnUsciteCassa.widthProperty());
                    
               
                }          
                    @Override
                    public void updateItem(final String item, boolean empty) {
                       super.updateItem(item, empty);

                        if (empty || item == null) {
                            setGraphic(null);
                        } else {
                            text.setText(item);
                            setGraphic(text);
                            text.setStyle("-fx-font-size: 8pt;");
                            text.setTextAlignment(TextAlignment.RIGHT);


                        }                    
                    }
                };
                return cell;
            }
        }); 
          
           tableColumnSaldoCassa.setCellFactory(new Callback<TableColumn<StatisticsForTable, String>, TableCell<StatisticsForTable, String>>() {
        @Override
            public TableCell<StatisticsForTable, String> call(TableColumn<StatisticsForTable, String> param) {
                TableCell<StatisticsForTable, String> cell = new TableCell<StatisticsForTable, String>() {
                 private Text text = new Text();

                {
                 text.wrappingWidthProperty().bind(tableColumnSaldoCassa.widthProperty());
                    
               
                }          
                    @Override
                    public void updateItem(final String item, boolean empty) {
                       super.updateItem(item, empty);

                        if (empty || item == null) {
                            setGraphic(null);
                        } else {
                            text.setText(item);
                            setGraphic(text);
                            text.setStyle("-fx-font-size: 8pt;");
                            text.setTextAlignment(TextAlignment.RIGHT);


                        }                    
                    }
                };
                return cell;
            }
        }); 
           
           tableColumnEntrateBanca.setCellFactory(new Callback<TableColumn<StatisticsForTable, String>, TableCell<StatisticsForTable, String>>() {
        @Override
            public TableCell<StatisticsForTable, String> call(TableColumn<StatisticsForTable, String> param) {
                TableCell<StatisticsForTable, String> cell = new TableCell<StatisticsForTable, String>() {
                 private Text text = new Text();

                {
                 text.wrappingWidthProperty().bind(tableColumnEntrateBanca.widthProperty());
                    
               
                }          
                    @Override
                    public void updateItem(final String item, boolean empty) {
                       super.updateItem(item, empty);

                        if (empty || item == null) {
                            setGraphic(null);
                        } else {
                            text.setText(item);
                            setGraphic(text);
                            text.setStyle("-fx-font-size: 8pt;");
                            text.setTextAlignment(TextAlignment.RIGHT);


                        }                    
                    }
                };
                return cell;
            }
        }); 
           
           
           tableColumnUsciteBanca.setCellFactory(new Callback<TableColumn<StatisticsForTable, String>, TableCell<StatisticsForTable, String>>() {
        @Override
            public TableCell<StatisticsForTable, String> call(TableColumn<StatisticsForTable, String> param) {
                TableCell<StatisticsForTable, String> cell = new TableCell<StatisticsForTable, String>() {
                 private Text text = new Text();

                {
                 text.wrappingWidthProperty().bind(tableColumnUsciteBanca.widthProperty());
                    
               
                }          
                    @Override
                    public void updateItem(final String item, boolean empty) {
                       super.updateItem(item, empty);

                        if (empty || item == null) {
                            setGraphic(null);
                        } else {
                            text.setText(item);
                            setGraphic(text);
                            text.setStyle("-fx-font-size: 8pt;");
                            text.setTextAlignment(TextAlignment.RIGHT);


                        }                    
                    }
                };
                return cell;
            }
        }); 
           
           
           tableColumnSaldoBanca.setCellFactory(new Callback<TableColumn<StatisticsForTable, String>, TableCell<StatisticsForTable, String>>() {
        @Override
            public TableCell<StatisticsForTable, String> call(TableColumn<StatisticsForTable, String> param) {
                TableCell<StatisticsForTable, String> cell = new TableCell<StatisticsForTable, String>() {
                 private Text text = new Text();

                {
                 text.wrappingWidthProperty().bind(tableColumnSaldoBanca.widthProperty());
                    
               
                }          
                    @Override
                    public void updateItem(final String item, boolean empty) {
                       super.updateItem(item, empty);

                        if (empty || item == null) {
                            setGraphic(null);
                        } else {
                            text.setText(item);
                            setGraphic(text);
                            text.setStyle("-fx-font-size: 8pt;");
                            text.setTextAlignment(TextAlignment.RIGHT);


                        }                    
                    }
                };
                return cell;
            }
        }); 
           
           tableColumnSaldoTotale.setCellFactory(new Callback<TableColumn<StatisticsForTable, String>, TableCell<StatisticsForTable, String>>() {
        @Override
            public TableCell<StatisticsForTable, String> call(TableColumn<StatisticsForTable, String> param) {
                TableCell<StatisticsForTable, String> cell = new TableCell<StatisticsForTable, String>() {
                 private Text text = new Text();

                {
                 text.wrappingWidthProperty().bind(tableColumnSaldoTotale.widthProperty());
                    
               
                }          
                    @Override
                    public void updateItem(final String item, boolean empty) {
                       super.updateItem(item, empty);

                        if (empty || item == null) {
                            setGraphic(null);
                        } else {
                            text.setText(item);
                            setGraphic(text);
                            text.setStyle("-fx-font-size: 8pt;");
                            text.setTextAlignment(TextAlignment.RIGHT);


                        }                    
                    }
                };
                return cell;
            }
        }); 
        
        //call function handling statisctics
        handleStatistics();
        
    }    
    
}
