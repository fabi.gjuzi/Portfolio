package net.unnitech.primanotafacile;

import javafx.beans.property.SimpleStringProperty;


public final class ContactForTable 
{
    
    public SimpleStringProperty contactId = new SimpleStringProperty("");
    public SimpleStringProperty fullName = new SimpleStringProperty("");
    public SimpleStringProperty address = new SimpleStringProperty("");
    public SimpleStringProperty city = new SimpleStringProperty("");
    public SimpleStringProperty zipCode = new SimpleStringProperty("");
    public SimpleStringProperty state = new SimpleStringProperty("");
    public SimpleStringProperty phoneHome = new SimpleStringProperty("");
    public SimpleStringProperty phoneWork = new SimpleStringProperty("");
    public SimpleStringProperty mobileNumber = new SimpleStringProperty("");
    public SimpleStringProperty fax = new SimpleStringProperty("");
    public SimpleStringProperty eMail1 = new SimpleStringProperty("");
    public SimpleStringProperty eMail2 = new SimpleStringProperty("");
    public SimpleStringProperty instantMessage = new SimpleStringProperty("");
    public SimpleStringProperty web = new SimpleStringProperty("");
    public SimpleStringProperty note = new SimpleStringProperty("");
    public SimpleStringProperty contactType = new SimpleStringProperty("");
    
    public ContactForTable()
    {
        this("","","","","","","","","","","","","","","","");
    }
    
    public ContactForTable(String contactId, String fullName, String address, String city, String zipCode, String state, String phoneHome,
                            String phoneWork, String mobileNumber, String fax, String email1, String email2, String instantMessage, String web,
                            String note, String contactType)
    {
        setContactId(contactId);
        setFullName(fullName);
        setAddress(address);
        setCity(city);
        setZipCode(zipCode);
        setState(state);
        setPhoneHome(phoneHome);
        setPhoneWork(phoneWork);
        setMobileNumber(mobileNumber);
        setFax(fax);
        seteMail1(email1);
        seteMail2(email2);
        setInstantMessage(instantMessage);
        setWeb(web);
        setNote(note);
        setContactType(contactType);
    }

    public String getContactId() 
    {
        return contactId.getValue();
    }

    public void setContactId(String contactId) 
    {
        this.contactId.set(contactId);
    }

    public String getFullName() 
    {
        return fullName.getValue();
    }

    public void setFullName(String fullName) 
    {
        this.fullName.set(fullName);
    }

    public String getAddress() 
    {
        return address.getValue();
    }

    public void setAddress(String address) 
    {
        this.address.set(address);
    }

    public String getCity() {
        return city.getValue();
    }

    public void setCity(String city) 
    {
        this.city.set(city);
    }

    public String getZipCode() 
    {
        return zipCode.getValue();
    }

    public void setZipCode(String zipCode) 
    {
        this.zipCode.set(zipCode);
    }

    public String getState() 
    {
        return state.getValue();
    }

    public void setState(String state) 
    {
        this.state.set(state);
    }

    public String getPhoneHome() 
    {
        return phoneHome.getValue();
    }

    public void setPhoneHome(String phoneHome) 
    {
        this.phoneHome.set(phoneHome);
    }

    public String getPhoneWork() {
        return phoneWork.getValue();
    }

    public void setPhoneWork(String phoneWork) 
    {
        this.phoneWork.set(phoneWork);
    }

    public String getMobileNumber()
    {
        return mobileNumber.getValue();
    }

    public void setMobileNumber(String mobileNumber) 
    {
        this.mobileNumber.set(mobileNumber);
    }

    public String getFax() 
    {
        return fax.getValue();
    }

    public void setFax(String fax) 
    {
        this.fax.set(fax);
    }

    public String geteMail1()
    {
        return eMail1.getValue();
    }

    public void seteMail1(String eMail1) 
    {
        this.eMail1.set(eMail1);
    }

    public String geteMail2() {
        return eMail2.getValue();
    }

    public void seteMail2(String eMail2) 
    {
        this.eMail2.set(eMail2);
    }

    public String getInstantMessage() 
    {
        return instantMessage.getValue();
    }

    public void setInstantMessage(String instantMessage)
    {
        this.instantMessage.set(instantMessage);
    }

    public String getWeb() 
    {
        return web.getValue();
    }

    public void setWeb(String web) 
    {
        this.web.set(web);
    }

    public String getNote()
    {
        return note.getValue();
    }

    public void setNote(String note) 
    {
        this.note.set(note);
    }

    public String getContactType() 
    {
        return contactType.getValue();
    }

    public void setContactType(String contactType) 
    {
        this.contactType.set(contactType);
    }
    
}
