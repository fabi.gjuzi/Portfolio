
package net.unnitech.primanotafacile;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="oldArchive" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="newArchive" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "oldArchive",
    "newArchive"
})
@XmlRootElement(name = "updateArchiveRequest")
public class UpdateArchiveRequest {

    @XmlElement(required = true)
    protected String oldArchive;
    @XmlElement(required = true)
    protected String newArchive;

    /**
     * Gets the value of the oldArchive property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOldArchive() {
        return oldArchive;
    }

    /**
     * Sets the value of the oldArchive property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOldArchive(String value) {
        this.oldArchive = value;
    }

    /**
     * Gets the value of the newArchive property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNewArchive() {
        return newArchive;
    }

    /**
     * Sets the value of the newArchive property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNewArchive(String value) {
        this.newArchive = value;
    }

}
