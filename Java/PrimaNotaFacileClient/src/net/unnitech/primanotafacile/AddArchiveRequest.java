
package net.unnitech.primanotafacile;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="archive" type="{http://unnitech.net/primanotafacile}archive"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "archive"
})
@XmlRootElement(name = "addArchiveRequest")
public class AddArchiveRequest {

    @XmlElement(required = true)
    protected Archive archive;

    /**
     * Gets the value of the archive property.
     * 
     * @return
     *     possible object is
     *     {@link Archive }
     *     
     */
    public Archive getArchive() {
        return archive;
    }

    /**
     * Sets the value of the archive property.
     * 
     * @param value
     *     allowed object is
     *     {@link Archive }
     *     
     */
    public void setArchive(Archive value) {
        this.archive = value;
    }

}
