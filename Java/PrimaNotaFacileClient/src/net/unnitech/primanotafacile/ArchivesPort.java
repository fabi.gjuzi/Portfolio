
package net.unnitech.primanotafacile;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.xml.bind.annotation.XmlSeeAlso;


/**
 * This class was generated by the JAX-WS RI.
 * JAX-WS RI 2.2.9-b130926.1035
 * Generated source version: 2.2
 * 
 */
@WebService(name = "ArchivesPort", targetNamespace = "http://unnitech.net/primanotafacile")
@SOAPBinding(parameterStyle = SOAPBinding.ParameterStyle.BARE)
@XmlSeeAlso({
    ObjectFactory.class
})
public interface ArchivesPort {


    /**
     * 
     * @param deleteContactRequest
     * @return
     *     returns net.unnitech.primanotafacile.DeleteContactResponse
     */
    @WebMethod
    @WebResult(name = "deleteContactResponse", targetNamespace = "http://unnitech.net/primanotafacile", partName = "deleteContactResponse")
    public DeleteContactResponse deleteContact(
        @WebParam(name = "deleteContactRequest", targetNamespace = "http://unnitech.net/primanotafacile", partName = "deleteContactRequest")
        DeleteContactRequest deleteContactRequest);

    /**
     * 
     * @param getAllRemindersCustomRequest
     * @return
     *     returns net.unnitech.primanotafacile.GetAllRemindersCustomResponse
     */
    @WebMethod
    @WebResult(name = "getAllRemindersCustomResponse", targetNamespace = "http://unnitech.net/primanotafacile", partName = "getAllRemindersCustomResponse")
    public GetAllRemindersCustomResponse getAllRemindersCustom(
        @WebParam(name = "getAllRemindersCustomRequest", targetNamespace = "http://unnitech.net/primanotafacile", partName = "getAllRemindersCustomRequest")
        GetAllRemindersCustomRequest getAllRemindersCustomRequest);

    /**
     * 
     * @param getReminderRequest
     * @return
     *     returns net.unnitech.primanotafacile.GetReminderResponse
     */
    @WebMethod
    @WebResult(name = "getReminderResponse", targetNamespace = "http://unnitech.net/primanotafacile", partName = "getReminderResponse")
    public GetReminderResponse getReminder(
        @WebParam(name = "getReminderRequest", targetNamespace = "http://unnitech.net/primanotafacile", partName = "getReminderRequest")
        GetReminderRequest getReminderRequest);

    /**
     * 
     * @param updateArchiveRequest
     * @return
     *     returns net.unnitech.primanotafacile.UpdateArchiveResponse
     */
    @WebMethod
    @WebResult(name = "updateArchiveResponse", targetNamespace = "http://unnitech.net/primanotafacile", partName = "updateArchiveResponse")
    public UpdateArchiveResponse updateArchive(
        @WebParam(name = "updateArchiveRequest", targetNamespace = "http://unnitech.net/primanotafacile", partName = "updateArchiveRequest")
        UpdateArchiveRequest updateArchiveRequest);

    /**
     * 
     * @param updateCompanyRequest
     * @return
     *     returns net.unnitech.primanotafacile.UpdateCompanyResponse
     */
    @WebMethod
    @WebResult(name = "updateCompanyResponse", targetNamespace = "http://unnitech.net/primanotafacile", partName = "updateCompanyResponse")
    public UpdateCompanyResponse updateCompany(
        @WebParam(name = "updateCompanyRequest", targetNamespace = "http://unnitech.net/primanotafacile", partName = "updateCompanyRequest")
        UpdateCompanyRequest updateCompanyRequest);

    /**
     * 
     * @param getEntriesByOperationTypeRequest
     * @return
     *     returns net.unnitech.primanotafacile.GetEntriesByOperationTypeResponse
     */
    @WebMethod
    @WebResult(name = "getEntriesByOperationTypeResponse", targetNamespace = "http://unnitech.net/primanotafacile", partName = "getEntriesByOperationTypeResponse")
    public GetEntriesByOperationTypeResponse getEntriesByOperationType(
        @WebParam(name = "getEntriesByOperationTypeRequest", targetNamespace = "http://unnitech.net/primanotafacile", partName = "getEntriesByOperationTypeRequest")
        GetEntriesByOperationTypeRequest getEntriesByOperationTypeRequest);

    /**
     * 
     * @param getContactRequest
     * @return
     *     returns net.unnitech.primanotafacile.GetContactResponse
     */
    @WebMethod
    @WebResult(name = "getContactResponse", targetNamespace = "http://unnitech.net/primanotafacile", partName = "getContactResponse")
    public GetContactResponse getContact(
        @WebParam(name = "getContactRequest", targetNamespace = "http://unnitech.net/primanotafacile", partName = "getContactRequest")
        GetContactRequest getContactRequest);

    /**
     * 
     * @param addCompanyRequest
     * @return
     *     returns net.unnitech.primanotafacile.AddCompanyResponse
     */
    @WebMethod
    @WebResult(name = "addCompanyResponse", targetNamespace = "http://unnitech.net/primanotafacile", partName = "addCompanyResponse")
    public AddCompanyResponse addCompany(
        @WebParam(name = "addCompanyRequest", targetNamespace = "http://unnitech.net/primanotafacile", partName = "addCompanyRequest")
        AddCompanyRequest addCompanyRequest);

    /**
     * 
     * @param getAllRemindersAfterDateRequest
     * @return
     *     returns net.unnitech.primanotafacile.GetAllRemindersAfterDateResponse
     */
    @WebMethod
    @WebResult(name = "getAllRemindersAfterDateResponse", targetNamespace = "http://unnitech.net/primanotafacile", partName = "getAllRemindersAfterDateResponse")
    public GetAllRemindersAfterDateResponse getAllRemindersAfterDate(
        @WebParam(name = "getAllRemindersAfterDateRequest", targetNamespace = "http://unnitech.net/primanotafacile", partName = "getAllRemindersAfterDateRequest")
        GetAllRemindersAfterDateRequest getAllRemindersAfterDateRequest);

    /**
     * 
     * @param addReminderRequest
     * @return
     *     returns net.unnitech.primanotafacile.AddReminderResponse
     */
    @WebMethod
    @WebResult(name = "addReminderResponse", targetNamespace = "http://unnitech.net/primanotafacile", partName = "addReminderResponse")
    public AddReminderResponse addReminder(
        @WebParam(name = "addReminderRequest", targetNamespace = "http://unnitech.net/primanotafacile", partName = "addReminderRequest")
        AddReminderRequest addReminderRequest);

    /**
     * 
     * @param getAllRemindersBeforeDateRequest
     * @return
     *     returns net.unnitech.primanotafacile.GetAllRemindersBeforeDateResponse
     */
    @WebMethod
    @WebResult(name = "getAllRemindersBeforeDateResponse", targetNamespace = "http://unnitech.net/primanotafacile", partName = "getAllRemindersBeforeDateResponse")
    public GetAllRemindersBeforeDateResponse getAllRemindersBeforeDate(
        @WebParam(name = "getAllRemindersBeforeDateRequest", targetNamespace = "http://unnitech.net/primanotafacile", partName = "getAllRemindersBeforeDateRequest")
        GetAllRemindersBeforeDateRequest getAllRemindersBeforeDateRequest);

    /**
     * 
     * @param getEntriesByBankRequest
     * @return
     *     returns net.unnitech.primanotafacile.GetEntriesByBankResponse
     */
    @WebMethod
    @WebResult(name = "getEntriesByBankResponse", targetNamespace = "http://unnitech.net/primanotafacile", partName = "getEntriesByBankResponse")
    public GetEntriesByBankResponse getEntriesByBank(
        @WebParam(name = "getEntriesByBankRequest", targetNamespace = "http://unnitech.net/primanotafacile", partName = "getEntriesByBankRequest")
        GetEntriesByBankRequest getEntriesByBankRequest);

    /**
     * 
     * @param deleteBankRequest
     * @return
     *     returns net.unnitech.primanotafacile.DeleteBankResponse
     */
    @WebMethod
    @WebResult(name = "deleteBankResponse", targetNamespace = "http://unnitech.net/primanotafacile", partName = "deleteBankResponse")
    public DeleteBankResponse deleteBank(
        @WebParam(name = "deleteBankRequest", targetNamespace = "http://unnitech.net/primanotafacile", partName = "deleteBankRequest")
        DeleteBankRequest deleteBankRequest);

    /**
     * 
     * @param getAllEntriesPreviousPeriodRequest
     * @return
     *     returns net.unnitech.primanotafacile.GetAllEntriesPreviousPeriodResponse
     */
    @WebMethod
    @WebResult(name = "getAllEntriesPreviousPeriodResponse", targetNamespace = "http://unnitech.net/primanotafacile", partName = "getAllEntriesPreviousPeriodResponse")
    public GetAllEntriesPreviousPeriodResponse getAllEntriesPreviousPeriod(
        @WebParam(name = "getAllEntriesPreviousPeriodRequest", targetNamespace = "http://unnitech.net/primanotafacile", partName = "getAllEntriesPreviousPeriodRequest")
        GetAllEntriesPreviousPeriodRequest getAllEntriesPreviousPeriodRequest);

    /**
     * 
     * @param getAllRemindersRequest
     * @return
     *     returns net.unnitech.primanotafacile.GetAllRemindersResponse
     */
    @WebMethod
    @WebResult(name = "getAllRemindersResponse", targetNamespace = "http://unnitech.net/primanotafacile", partName = "getAllRemindersResponse")
    public GetAllRemindersResponse getAllReminders(
        @WebParam(name = "getAllRemindersRequest", targetNamespace = "http://unnitech.net/primanotafacile", partName = "getAllRemindersRequest")
        GetAllRemindersRequest getAllRemindersRequest);

    /**
     * 
     * @param updateEntryRequest
     * @return
     *     returns net.unnitech.primanotafacile.UpdateEntryResponse
     */
    @WebMethod
    @WebResult(name = "updateEntryResponse", targetNamespace = "http://unnitech.net/primanotafacile", partName = "updateEntryResponse")
    public UpdateEntryResponse updateEntry(
        @WebParam(name = "updateEntryRequest", targetNamespace = "http://unnitech.net/primanotafacile", partName = "updateEntryRequest")
        UpdateEntryRequest updateEntryRequest);

    /**
     * 
     * @param updateReminderRequest
     * @return
     *     returns net.unnitech.primanotafacile.UpdateReminderResponse
     */
    @WebMethod
    @WebResult(name = "updateReminderResponse", targetNamespace = "http://unnitech.net/primanotafacile", partName = "updateReminderResponse")
    public UpdateReminderResponse updateReminder(
        @WebParam(name = "updateReminderRequest", targetNamespace = "http://unnitech.net/primanotafacile", partName = "updateReminderRequest")
        UpdateReminderRequest updateReminderRequest);

    /**
     * 
     * @param getAllRemindersByContactRequest
     * @return
     *     returns net.unnitech.primanotafacile.GetAllRemindersByContactResponse
     */
    @WebMethod
    @WebResult(name = "getAllRemindersByContactResponse", targetNamespace = "http://unnitech.net/primanotafacile", partName = "getAllRemindersByContactResponse")
    public GetAllRemindersByContactResponse getAllRemindersByContact(
        @WebParam(name = "getAllRemindersByContactRequest", targetNamespace = "http://unnitech.net/primanotafacile", partName = "getAllRemindersByContactRequest")
        GetAllRemindersByContactRequest getAllRemindersByContactRequest);

    /**
     * 
     * @param copyArchiveRequest
     * @return
     *     returns net.unnitech.primanotafacile.CopyArchiveResponse
     */
    @WebMethod
    @WebResult(name = "copyArchiveResponse", targetNamespace = "http://unnitech.net/primanotafacile", partName = "copyArchiveResponse")
    public CopyArchiveResponse copyArchive(
        @WebParam(name = "copyArchiveRequest", targetNamespace = "http://unnitech.net/primanotafacile", partName = "copyArchiveRequest")
        CopyArchiveRequest copyArchiveRequest);

    /**
     * 
     * @param getCompanyRequest
     * @return
     *     returns net.unnitech.primanotafacile.GetCompanyResponse
     */
    @WebMethod
    @WebResult(name = "getCompanyResponse", targetNamespace = "http://unnitech.net/primanotafacile", partName = "getCompanyResponse")
    public GetCompanyResponse getCompany(
        @WebParam(name = "getCompanyRequest", targetNamespace = "http://unnitech.net/primanotafacile", partName = "getCompanyRequest")
        GetCompanyRequest getCompanyRequest);

    /**
     * 
     * @param getEntriesByDateRequest
     * @return
     *     returns net.unnitech.primanotafacile.GetEntriesByDateResponse
     */
    @WebMethod
    @WebResult(name = "getEntriesByDateResponse", targetNamespace = "http://unnitech.net/primanotafacile", partName = "getEntriesByDateResponse")
    public GetEntriesByDateResponse getEntriesByDate(
        @WebParam(name = "getEntriesByDateRequest", targetNamespace = "http://unnitech.net/primanotafacile", partName = "getEntriesByDateRequest")
        GetEntriesByDateRequest getEntriesByDateRequest);

    /**
     * 
     * @param getEntryRequest
     * @return
     *     returns net.unnitech.primanotafacile.GetEntryResponse
     */
    @WebMethod
    @WebResult(name = "getEntryResponse", targetNamespace = "http://unnitech.net/primanotafacile", partName = "getEntryResponse")
    public GetEntryResponse getEntry(
        @WebParam(name = "getEntryRequest", targetNamespace = "http://unnitech.net/primanotafacile", partName = "getEntryRequest")
        GetEntryRequest getEntryRequest);

    /**
     * 
     * @param getArchiveRequest
     * @return
     *     returns net.unnitech.primanotafacile.GetArchiveResponse
     */
    @WebMethod
    @WebResult(name = "getArchiveResponse", targetNamespace = "http://unnitech.net/primanotafacile", partName = "getArchiveResponse")
    public GetArchiveResponse getArchive(
        @WebParam(name = "getArchiveRequest", targetNamespace = "http://unnitech.net/primanotafacile", partName = "getArchiveRequest")
        GetArchiveRequest getArchiveRequest);

    /**
     * 
     * @param deleteCompanyRequest
     * @return
     *     returns net.unnitech.primanotafacile.DeleteCompanyResponse
     */
    @WebMethod
    @WebResult(name = "deleteCompanyResponse", targetNamespace = "http://unnitech.net/primanotafacile", partName = "deleteCompanyResponse")
    public DeleteCompanyResponse deleteCompany(
        @WebParam(name = "deleteCompanyRequest", targetNamespace = "http://unnitech.net/primanotafacile", partName = "deleteCompanyRequest")
        DeleteCompanyRequest deleteCompanyRequest);

    /**
     * 
     * @param deleteEntryRequest
     * @return
     *     returns net.unnitech.primanotafacile.DeleteEntryResponse
     */
    @WebMethod
    @WebResult(name = "deleteEntryResponse", targetNamespace = "http://unnitech.net/primanotafacile", partName = "deleteEntryResponse")
    public DeleteEntryResponse deleteEntry(
        @WebParam(name = "deleteEntryRequest", targetNamespace = "http://unnitech.net/primanotafacile", partName = "deleteEntryRequest")
        DeleteEntryRequest deleteEntryRequest);

    /**
     * 
     * @param getAllEntriesRequest
     * @return
     *     returns net.unnitech.primanotafacile.GetAllEntriesResponse
     */
    @WebMethod
    @WebResult(name = "getAllEntriesResponse", targetNamespace = "http://unnitech.net/primanotafacile", partName = "getAllEntriesResponse")
    public GetAllEntriesResponse getAllEntries(
        @WebParam(name = "getAllEntriesRequest", targetNamespace = "http://unnitech.net/primanotafacile", partName = "getAllEntriesRequest")
        GetAllEntriesRequest getAllEntriesRequest);

    /**
     * 
     * @param getContactsLikeNameRequest
     * @return
     *     returns net.unnitech.primanotafacile.GetContactsLikeNameResponse
     */
    @WebMethod
    @WebResult(name = "getContactsLikeNameResponse", targetNamespace = "http://unnitech.net/primanotafacile", partName = "getContactsLikeNameResponse")
    public GetContactsLikeNameResponse getContactsLikeName(
        @WebParam(name = "getContactsLikeNameRequest", targetNamespace = "http://unnitech.net/primanotafacile", partName = "getContactsLikeNameRequest")
        GetContactsLikeNameRequest getContactsLikeNameRequest);

    /**
     * 
     * @param getAllCompaniesCustomRequest
     * @return
     *     returns net.unnitech.primanotafacile.GetAllCompaniesCustomResponse
     */
    @WebMethod
    @WebResult(name = "getAllCompaniesCustomResponse", targetNamespace = "http://unnitech.net/primanotafacile", partName = "getAllCompaniesCustomResponse")
    public GetAllCompaniesCustomResponse getAllCompaniesCustom(
        @WebParam(name = "getAllCompaniesCustomRequest", targetNamespace = "http://unnitech.net/primanotafacile", partName = "getAllCompaniesCustomRequest")
        GetAllCompaniesCustomRequest getAllCompaniesCustomRequest);

    /**
     * 
     * @param updateSqliteSequenceRequest
     * @return
     *     returns net.unnitech.primanotafacile.UpdateSqliteSequenceResponse
     */
    @WebMethod
    @WebResult(name = "updateSqliteSequenceResponse", targetNamespace = "http://unnitech.net/primanotafacile", partName = "updateSqliteSequenceResponse")
    public UpdateSqliteSequenceResponse updateSqliteSequence(
        @WebParam(name = "updateSqliteSequenceRequest", targetNamespace = "http://unnitech.net/primanotafacile", partName = "updateSqliteSequenceRequest")
        UpdateSqliteSequenceRequest updateSqliteSequenceRequest);

    /**
     * 
     * @param deleteReminderRequest
     * @return
     *     returns net.unnitech.primanotafacile.DeleteReminderResponse
     */
    @WebMethod
    @WebResult(name = "deleteReminderResponse", targetNamespace = "http://unnitech.net/primanotafacile", partName = "deleteReminderResponse")
    public DeleteReminderResponse deleteReminder(
        @WebParam(name = "deleteReminderRequest", targetNamespace = "http://unnitech.net/primanotafacile", partName = "deleteReminderRequest")
        DeleteReminderRequest deleteReminderRequest);

    /**
     * 
     * @param getAllRemindersLikeDescriptionRequest
     * @return
     *     returns net.unnitech.primanotafacile.GetAllRemindersLikeDescriptionResponse
     */
    @WebMethod
    @WebResult(name = "getAllRemindersLikeDescriptionResponse", targetNamespace = "http://unnitech.net/primanotafacile", partName = "getAllRemindersLikeDescriptionResponse")
    public GetAllRemindersLikeDescriptionResponse getAllRemindersLikeDescription(
        @WebParam(name = "getAllRemindersLikeDescriptionRequest", targetNamespace = "http://unnitech.net/primanotafacile", partName = "getAllRemindersLikeDescriptionRequest")
        GetAllRemindersLikeDescriptionRequest getAllRemindersLikeDescriptionRequest);

    /**
     * 
     * @param getAllRemindersBetweenDatesRequest
     * @return
     *     returns net.unnitech.primanotafacile.GetAllRemindersBetweenDatesResponse
     */
    @WebMethod
    @WebResult(name = "getAllRemindersBetweenDatesResponse", targetNamespace = "http://unnitech.net/primanotafacile", partName = "getAllRemindersBetweenDatesResponse")
    public GetAllRemindersBetweenDatesResponse getAllRemindersBetweenDates(
        @WebParam(name = "getAllRemindersBetweenDatesRequest", targetNamespace = "http://unnitech.net/primanotafacile", partName = "getAllRemindersBetweenDatesRequest")
        GetAllRemindersBetweenDatesRequest getAllRemindersBetweenDatesRequest);

    /**
     * 
     * @param getAllOperationTypesRequest
     * @return
     *     returns net.unnitech.primanotafacile.GetAllOperationTypesResponse
     */
    @WebMethod
    @WebResult(name = "getAllOperationTypesResponse", targetNamespace = "http://unnitech.net/primanotafacile", partName = "getAllOperationTypesResponse")
    public GetAllOperationTypesResponse getAllOperationTypes(
        @WebParam(name = "getAllOperationTypesRequest", targetNamespace = "http://unnitech.net/primanotafacile", partName = "getAllOperationTypesRequest")
        GetAllOperationTypesRequest getAllOperationTypesRequest);

    /**
     * 
     * @param deleteSqliteSequenceRequest
     * @return
     *     returns net.unnitech.primanotafacile.DeleteSqliteSequenceResponse
     */
    @WebMethod
    @WebResult(name = "deleteSqliteSequenceResponse", targetNamespace = "http://unnitech.net/primanotafacile", partName = "deleteSqliteSequenceResponse")
    public DeleteSqliteSequenceResponse deleteSqliteSequence(
        @WebParam(name = "deleteSqliteSequenceRequest", targetNamespace = "http://unnitech.net/primanotafacile", partName = "deleteSqliteSequenceRequest")
        DeleteSqliteSequenceRequest deleteSqliteSequenceRequest);

    /**
     * 
     * @param getAllCompaniesRequest
     * @return
     *     returns net.unnitech.primanotafacile.GetAllCompaniesResponse
     */
    @WebMethod
    @WebResult(name = "getAllCompaniesResponse", targetNamespace = "http://unnitech.net/primanotafacile", partName = "getAllCompaniesResponse")
    public GetAllCompaniesResponse getAllCompanies(
        @WebParam(name = "getAllCompaniesRequest", targetNamespace = "http://unnitech.net/primanotafacile", partName = "getAllCompaniesRequest")
        GetAllCompaniesRequest getAllCompaniesRequest);

    /**
     * 
     * @param getEntriesAfterDateRequest
     * @return
     *     returns net.unnitech.primanotafacile.GetEntriesAfterDateResponse
     */
    @WebMethod
    @WebResult(name = "getEntriesAfterDateResponse", targetNamespace = "http://unnitech.net/primanotafacile", partName = "getEntriesAfterDateResponse")
    public GetEntriesAfterDateResponse getEntriesAfterDate(
        @WebParam(name = "getEntriesAfterDateRequest", targetNamespace = "http://unnitech.net/primanotafacile", partName = "getEntriesAfterDateRequest")
        GetEntriesAfterDateRequest getEntriesAfterDateRequest);

    /**
     * 
     * @param getAllEntriesCustomRequest
     * @return
     *     returns net.unnitech.primanotafacile.GetAllEntriesCustomResponse
     */
    @WebMethod
    @WebResult(name = "getAllEntriesCustomResponse", targetNamespace = "http://unnitech.net/primanotafacile", partName = "getAllEntriesCustomResponse")
    public GetAllEntriesCustomResponse getAllEntriesCustom(
        @WebParam(name = "getAllEntriesCustomRequest", targetNamespace = "http://unnitech.net/primanotafacile", partName = "getAllEntriesCustomRequest")
        GetAllEntriesCustomRequest getAllEntriesCustomRequest);

    /**
     * 
     * @param deleteArchiveRequest
     * @return
     *     returns net.unnitech.primanotafacile.DeleteArchiveResponse
     */
    @WebMethod
    @WebResult(name = "deleteArchiveResponse", targetNamespace = "http://unnitech.net/primanotafacile", partName = "deleteArchiveResponse")
    public DeleteArchiveResponse deleteArchive(
        @WebParam(name = "deleteArchiveRequest", targetNamespace = "http://unnitech.net/primanotafacile", partName = "deleteArchiveRequest")
        DeleteArchiveRequest deleteArchiveRequest);

    /**
     * 
     * @param addContactRequest
     * @return
     *     returns net.unnitech.primanotafacile.AddContactResponse
     */
    @WebMethod
    @WebResult(name = "addContactResponse", targetNamespace = "http://unnitech.net/primanotafacile", partName = "addContactResponse")
    public AddContactResponse addContact(
        @WebParam(name = "addContactRequest", targetNamespace = "http://unnitech.net/primanotafacile", partName = "addContactRequest")
        AddContactRequest addContactRequest);

    /**
     * 
     * @param getEntriesBeforeDateRequest
     * @return
     *     returns net.unnitech.primanotafacile.GetEntriesBeforeDateResponse
     */
    @WebMethod
    @WebResult(name = "getEntriesBeforeDateResponse", targetNamespace = "http://unnitech.net/primanotafacile", partName = "getEntriesBeforeDateResponse")
    public GetEntriesBeforeDateResponse getEntriesBeforeDate(
        @WebParam(name = "getEntriesBeforeDateRequest", targetNamespace = "http://unnitech.net/primanotafacile", partName = "getEntriesBeforeDateRequest")
        GetEntriesBeforeDateRequest getEntriesBeforeDateRequest);

    /**
     * 
     * @param addOperationTypeRequest
     * @return
     *     returns net.unnitech.primanotafacile.AddOperationTypeResponse
     */
    @WebMethod
    @WebResult(name = "addOperationTypeResponse", targetNamespace = "http://unnitech.net/primanotafacile", partName = "addOperationTypeResponse")
    public AddOperationTypeResponse addOperationType(
        @WebParam(name = "addOperationTypeRequest", targetNamespace = "http://unnitech.net/primanotafacile", partName = "addOperationTypeRequest")
        AddOperationTypeRequest addOperationTypeRequest);

    /**
     * 
     * @param getSqliteSequenceRequest
     * @return
     *     returns net.unnitech.primanotafacile.GetSqliteSequenceResponse
     */
    @WebMethod
    @WebResult(name = "getSqliteSequenceResponse", targetNamespace = "http://unnitech.net/primanotafacile", partName = "getSqliteSequenceResponse")
    public GetSqliteSequenceResponse getSqliteSequence(
        @WebParam(name = "getSqliteSequenceRequest", targetNamespace = "http://unnitech.net/primanotafacile", partName = "getSqliteSequenceRequest")
        GetSqliteSequenceRequest getSqliteSequenceRequest);

    /**
     * 
     * @param addBankRequest
     * @return
     *     returns net.unnitech.primanotafacile.AddBankResponse
     */
    @WebMethod
    @WebResult(name = "addBankResponse", targetNamespace = "http://unnitech.net/primanotafacile", partName = "addBankResponse")
    public AddBankResponse addBank(
        @WebParam(name = "addBankRequest", targetNamespace = "http://unnitech.net/primanotafacile", partName = "addBankRequest")
        AddBankRequest addBankRequest);

    /**
     * 
     * @param getAllBankCustomRequest
     * @return
     *     returns net.unnitech.primanotafacile.GetAllBankCustomResponse
     */
    @WebMethod
    @WebResult(name = "getAllBankCustomResponse", targetNamespace = "http://unnitech.net/primanotafacile", partName = "getAllBankCustomResponse")
    public GetAllBankCustomResponse getAllBankCustom(
        @WebParam(name = "getAllBankCustomRequest", targetNamespace = "http://unnitech.net/primanotafacile", partName = "getAllBankCustomRequest")
        GetAllBankCustomRequest getAllBankCustomRequest);

    /**
     * 
     * @param getAllBankRequest
     * @return
     *     returns net.unnitech.primanotafacile.GetAllBankResponse
     */
    @WebMethod
    @WebResult(name = "getAllBankResponse", targetNamespace = "http://unnitech.net/primanotafacile", partName = "getAllBankResponse")
    public GetAllBankResponse getAllBank(
        @WebParam(name = "getAllBankRequest", targetNamespace = "http://unnitech.net/primanotafacile", partName = "getAllBankRequest")
        GetAllBankRequest getAllBankRequest);

    /**
     * 
     * @param getEntriesBetweenDatesRequest
     * @return
     *     returns net.unnitech.primanotafacile.GetEntriesBetweenDatesResponse
     */
    @WebMethod
    @WebResult(name = "getEntriesBetweenDatesResponse", targetNamespace = "http://unnitech.net/primanotafacile", partName = "getEntriesBetweenDatesResponse")
    public GetEntriesBetweenDatesResponse getEntriesBetweenDates(
        @WebParam(name = "getEntriesBetweenDatesRequest", targetNamespace = "http://unnitech.net/primanotafacile", partName = "getEntriesBetweenDatesRequest")
        GetEntriesBetweenDatesRequest getEntriesBetweenDatesRequest);

    /**
     * 
     * @param deleteOperationTypeRequest
     * @return
     *     returns net.unnitech.primanotafacile.DeleteOperationTypeResponse
     */
    @WebMethod
    @WebResult(name = "deleteOperationTypeResponse", targetNamespace = "http://unnitech.net/primanotafacile", partName = "deleteOperationTypeResponse")
    public DeleteOperationTypeResponse deleteOperationType(
        @WebParam(name = "deleteOperationTypeRequest", targetNamespace = "http://unnitech.net/primanotafacile", partName = "deleteOperationTypeRequest")
        DeleteOperationTypeRequest deleteOperationTypeRequest);

    /**
     * 
     * @param getBanksLikeNameRequest
     * @return
     *     returns net.unnitech.primanotafacile.GetBanksLikeNameResponse
     */
    @WebMethod
    @WebResult(name = "getBanksLikeNameResponse", targetNamespace = "http://unnitech.net/primanotafacile", partName = "getBanksLikeNameResponse")
    public GetBanksLikeNameResponse getBanksLikeName(
        @WebParam(name = "getBanksLikeNameRequest", targetNamespace = "http://unnitech.net/primanotafacile", partName = "getBanksLikeNameRequest")
        GetBanksLikeNameRequest getBanksLikeNameRequest);

    /**
     * 
     * @param getOperationTypeRequest
     * @return
     *     returns net.unnitech.primanotafacile.GetOperationTypeResponse
     */
    @WebMethod
    @WebResult(name = "getOperationTypeResponse", targetNamespace = "http://unnitech.net/primanotafacile", partName = "getOperationTypeResponse")
    public GetOperationTypeResponse getOperationType(
        @WebParam(name = "getOperationTypeRequest", targetNamespace = "http://unnitech.net/primanotafacile", partName = "getOperationTypeRequest")
        GetOperationTypeRequest getOperationTypeRequest);

    /**
     * 
     * @param updateOperationTypeRequest
     * @return
     *     returns net.unnitech.primanotafacile.UpdateOperationTypeResponse
     */
    @WebMethod
    @WebResult(name = "updateOperationTypeResponse", targetNamespace = "http://unnitech.net/primanotafacile", partName = "updateOperationTypeResponse")
    public UpdateOperationTypeResponse updateOperationType(
        @WebParam(name = "updateOperationTypeRequest", targetNamespace = "http://unnitech.net/primanotafacile", partName = "updateOperationTypeRequest")
        UpdateOperationTypeRequest updateOperationTypeRequest);

    /**
     * 
     * @param addEntryRequest
     * @return
     *     returns net.unnitech.primanotafacile.AddEntryResponse
     */
    @WebMethod
    @WebResult(name = "addEntryResponse", targetNamespace = "http://unnitech.net/primanotafacile", partName = "addEntryResponse")
    public AddEntryResponse addEntry(
        @WebParam(name = "addEntryRequest", targetNamespace = "http://unnitech.net/primanotafacile", partName = "addEntryRequest")
        AddEntryRequest addEntryRequest);

    /**
     * 
     * @param addSqliteSequenceRequest
     * @return
     *     returns net.unnitech.primanotafacile.AddSqliteSequenceResponse
     */
    @WebMethod
    @WebResult(name = "addSqliteSequenceResponse", targetNamespace = "http://unnitech.net/primanotafacile", partName = "addSqliteSequenceResponse")
    public AddSqliteSequenceResponse addSqliteSequence(
        @WebParam(name = "addSqliteSequenceRequest", targetNamespace = "http://unnitech.net/primanotafacile", partName = "addSqliteSequenceRequest")
        AddSqliteSequenceRequest addSqliteSequenceRequest);

    /**
     * 
     * @param getAllContactsRequest
     * @return
     *     returns net.unnitech.primanotafacile.GetAllContactsResponse
     */
    @WebMethod
    @WebResult(name = "getAllContactsResponse", targetNamespace = "http://unnitech.net/primanotafacile", partName = "getAllContactsResponse")
    public GetAllContactsResponse getAllContacts(
        @WebParam(name = "getAllContactsRequest", targetNamespace = "http://unnitech.net/primanotafacile", partName = "getAllContactsRequest")
        GetAllContactsRequest getAllContactsRequest);

    /**
     * 
     * @param getBankRequest
     * @return
     *     returns net.unnitech.primanotafacile.GetBankResponse
     */
    @WebMethod
    @WebResult(name = "getBankResponse", targetNamespace = "http://unnitech.net/primanotafacile", partName = "getBankResponse")
    public GetBankResponse getBank(
        @WebParam(name = "getBankRequest", targetNamespace = "http://unnitech.net/primanotafacile", partName = "getBankRequest")
        GetBankRequest getBankRequest);

    /**
     * 
     * @param addArchiveRequest
     * @return
     *     returns net.unnitech.primanotafacile.AddArchiveResponse
     */
    @WebMethod
    @WebResult(name = "addArchiveResponse", targetNamespace = "http://unnitech.net/primanotafacile", partName = "addArchiveResponse")
    public AddArchiveResponse addArchive(
        @WebParam(name = "addArchiveRequest", targetNamespace = "http://unnitech.net/primanotafacile", partName = "addArchiveRequest")
        AddArchiveRequest addArchiveRequest);

    /**
     * 
     * @param getAllSqliteSequenciesRequest
     * @return
     *     returns net.unnitech.primanotafacile.GetAllSqliteSequenciesResponse
     */
    @WebMethod
    @WebResult(name = "getAllSqliteSequenciesResponse", targetNamespace = "http://unnitech.net/primanotafacile", partName = "getAllSqliteSequenciesResponse")
    public GetAllSqliteSequenciesResponse getAllSqliteSequencies(
        @WebParam(name = "getAllSqliteSequenciesRequest", targetNamespace = "http://unnitech.net/primanotafacile", partName = "getAllSqliteSequenciesRequest")
        GetAllSqliteSequenciesRequest getAllSqliteSequenciesRequest);

    /**
     * 
     * @param getBanksByArchiveRequest
     * @return
     *     returns net.unnitech.primanotafacile.GetBanksByArchiveResponse
     */
    @WebMethod
    @WebResult(name = "getBanksByArchiveResponse", targetNamespace = "http://unnitech.net/primanotafacile", partName = "getBanksByArchiveResponse")
    public GetBanksByArchiveResponse getBanksByArchive(
        @WebParam(name = "getBanksByArchiveRequest", targetNamespace = "http://unnitech.net/primanotafacile", partName = "getBanksByArchiveRequest")
        GetBanksByArchiveRequest getBanksByArchiveRequest);

    /**
     * 
     * @param updateContactRequest
     * @return
     *     returns net.unnitech.primanotafacile.UpdateContactResponse
     */
    @WebMethod
    @WebResult(name = "updateContactResponse", targetNamespace = "http://unnitech.net/primanotafacile", partName = "updateContactResponse")
    public UpdateContactResponse updateContact(
        @WebParam(name = "updateContactRequest", targetNamespace = "http://unnitech.net/primanotafacile", partName = "updateContactRequest")
        UpdateContactRequest updateContactRequest);

    /**
     * 
     * @param getAllContactsCustomRequest
     * @return
     *     returns net.unnitech.primanotafacile.GetAllContactsCustomResponse
     */
    @WebMethod
    @WebResult(name = "getAllContactsCustomResponse", targetNamespace = "http://unnitech.net/primanotafacile", partName = "getAllContactsCustomResponse")
    public GetAllContactsCustomResponse getAllContactsCustom(
        @WebParam(name = "getAllContactsCustomRequest", targetNamespace = "http://unnitech.net/primanotafacile", partName = "getAllContactsCustomRequest")
        GetAllContactsCustomRequest getAllContactsCustomRequest);

    /**
     * 
     * @param getAllArchiveRequest
     * @return
     *     returns net.unnitech.primanotafacile.GetAllArchiveResponse
     */
    @WebMethod
    @WebResult(name = "getAllArchiveResponse", targetNamespace = "http://unnitech.net/primanotafacile", partName = "getAllArchiveResponse")
    public GetAllArchiveResponse getAllArchive(
        @WebParam(name = "getAllArchiveRequest", targetNamespace = "http://unnitech.net/primanotafacile", partName = "getAllArchiveRequest")
        GetAllArchiveRequest getAllArchiveRequest);

    /**
     * 
     * @param updateBankRequest
     * @return
     *     returns net.unnitech.primanotafacile.UpdateBankResponse
     */
    @WebMethod
    @WebResult(name = "updateBankResponse", targetNamespace = "http://unnitech.net/primanotafacile", partName = "updateBankResponse")
    public UpdateBankResponse updateBank(
        @WebParam(name = "updateBankRequest", targetNamespace = "http://unnitech.net/primanotafacile", partName = "updateBankRequest")
        UpdateBankRequest updateBankRequest);

    /**
     * 
     * @param getAllOperationTypesCustomRequest
     * @return
     *     returns net.unnitech.primanotafacile.GetAllOperationTypesCustomResponse
     */
    @WebMethod
    @WebResult(name = "getAllOperationTypesCustomResponse", targetNamespace = "http://unnitech.net/primanotafacile", partName = "getAllOperationTypesCustomResponse")
    public GetAllOperationTypesCustomResponse getAllOperationTypesCustom(
        @WebParam(name = "getAllOperationTypesCustomRequest", targetNamespace = "http://unnitech.net/primanotafacile", partName = "getAllOperationTypesCustomRequest")
        GetAllOperationTypesCustomRequest getAllOperationTypesCustomRequest);

    /**
     * 
     * @param getCompaniesLikeNameRequest
     * @return
     *     returns net.unnitech.primanotafacile.GetCompaniesLikeNameResponse
     */
    @WebMethod
    @WebResult(name = "getCompaniesLikeNameResponse", targetNamespace = "http://unnitech.net/primanotafacile", partName = "getCompaniesLikeNameResponse")
    public GetCompaniesLikeNameResponse getCompaniesLikeName(
        @WebParam(name = "getCompaniesLikeNameRequest", targetNamespace = "http://unnitech.net/primanotafacile", partName = "getCompaniesLikeNameRequest")
        GetCompaniesLikeNameRequest getCompaniesLikeNameRequest);

}
