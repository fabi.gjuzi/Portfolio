
package net.unnitech.primanotafacile;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for operation_type complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="operation_type">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="name" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="incoming" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="outgoing" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="operation_type_id" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "operation_type", propOrder = {
    "name",
    "incoming",
    "outgoing",
    "operationTypeId"
})
public class OperationType {

    @XmlElement(required = true)
    protected String name;
    @XmlElement(required = true)
    protected String incoming;
    @XmlElement(required = true)
    protected String outgoing;
    @XmlElement(name = "operation_type_id", required = true)
    protected String operationTypeId;

    /**
     * Gets the value of the name property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the value of the name property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setName(String value) {
        this.name = value;
    }

    /**
     * Gets the value of the incoming property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIncoming() {
        return incoming;
    }

    /**
     * Sets the value of the incoming property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIncoming(String value) {
        this.incoming = value;
    }

    /**
     * Gets the value of the outgoing property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOutgoing() {
        return outgoing;
    }

    /**
     * Sets the value of the outgoing property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOutgoing(String value) {
        this.outgoing = value;
    }

    /**
     * Gets the value of the operationTypeId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOperationTypeId() {
        return operationTypeId;
    }

    /**
     * Sets the value of the operationTypeId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOperationTypeId(String value) {
        this.operationTypeId = value;
    }

}
