
package net.unnitech.primanotafacile;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for entryPreviousPeriod complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="entryPreviousPeriod">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="totalOutCash" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="totalBalanceCash" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="totalBalanceBank" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "entryPreviousPeriod", propOrder = {
    "totalOutCash",
    "totalBalanceCash",
    "totalBalanceBank"
})
public class EntryPreviousPeriod {

    @XmlElement(required = true)
    protected String totalOutCash;
    @XmlElement(required = true)
    protected String totalBalanceCash;
    @XmlElement(required = true)
    protected String totalBalanceBank;

    /**
     * Gets the value of the totalOutCash property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTotalOutCash() {
        return totalOutCash;
    }

    /**
     * Sets the value of the totalOutCash property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTotalOutCash(String value) {
        this.totalOutCash = value;
    }

    /**
     * Gets the value of the totalBalanceCash property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTotalBalanceCash() {
        return totalBalanceCash;
    }

    /**
     * Sets the value of the totalBalanceCash property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTotalBalanceCash(String value) {
        this.totalBalanceCash = value;
    }

    /**
     * Gets the value of the totalBalanceBank property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTotalBalanceBank() {
        return totalBalanceBank;
    }

    /**
     * Sets the value of the totalBalanceBank property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTotalBalanceBank(String value) {
        this.totalBalanceBank = value;
    }

}
