package net.unnitech.primanotafacile;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


public final class BankForTable 
{

    private final SimpleStringProperty bankId = new SimpleStringProperty("");
    private final SimpleStringProperty name = new SimpleStringProperty("");
    private final SimpleStringProperty description = new SimpleStringProperty("");
    private final SimpleStringProperty disabled = new SimpleStringProperty("");
    private final SimpleStringProperty idArchive = new SimpleStringProperty("");
    
    public BankForTable() 
    {
     this("","","","");
    }
    
    public BankForTable( String bankId, String name, String description, String disabled) 
    {
    setBankId(bankId);
    setName(name);
    setDescription(description);
    setDisabled(disabled);
//    setIdArchive(idArchive);
    }
    
    public String getBankId() 
    {
        return bankId.getValue();
    }

    public void setBankId(String bankId) 
    {
        this.bankId.set(bankId);
    }

    public String getName() 
    {
        return name.getValue();
    }

    public void setName(String name) 
    {
        this.name.set(name);
    }

    public String getDescription() 
    {
        return description.getValue();
    }

    public void setDescription(String description) 
    {
        this.description.set(description);
    }

    public String getDisabled() 
    {
        return disabled.getValue();
    }

    public void setDisabled(String disabled) 
    {
        this.disabled.set(disabled);
    }

    public String getIdArchive()
    {
        return idArchive.getValue();
    }

    public void setIdArchive(String idArchive)
    {
        this.idArchive.set(idArchive);
    }
}