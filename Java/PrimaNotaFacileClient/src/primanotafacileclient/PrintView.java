package primanotafacileclient;

import java.io.IOException;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.print.PageLayout;
import javafx.print.PageOrientation;
import javafx.print.Paper;
import javafx.print.Printer;
import javafx.print.PrinterAttributes;
import javafx.print.PrinterJob;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.transform.Scale;
import javafx.stage.Stage;

public class PrintView 
{
    @FXML
    private AnchorPane root;

    @FXML
    private Label contentNamelbl;

     @FXML
    private Label filterlbl;

    @FXML
    private Label periodlbl;

    @FXML
    private AnchorPane anchorPaneTableView;
        
    @FXML
    public void setPrintView(TableView tableScadenzario, String mainLabel, String periodFilter, String controparteFilter ) throws IOException, InstantiationException, IllegalAccessException
    {
//        TableView tableView1 = new TableView<>();
//        
//        tableScadenzario.setEditable(true);
        //System.out.println(tableView1.getColumns().get(0));
        
        //tableView = tableView1;
        AnchorPane root = FXMLLoader.load(getClass().getResource("PrintView.fxml"));

       
        tableScadenzario.setEditable(true);
        tableScadenzario.columnResizePolicyProperty().setValue(TableView.CONSTRAINED_RESIZE_POLICY);
        TableColumn desc = (TableColumn)tableScadenzario.getColumns().get(0);
        desc.setMaxWidth(Integer.MAX_VALUE);
        Scene scene = new Scene(root);
        AnchorPane tb = (AnchorPane) scene.lookup("#anchorPaneTableView");
        tb.getChildren().add(tableScadenzario);
        tb.getChildren().get(0).prefWidth(1004);
        tb.getChildren().get(0).prefHeight(558);
        AnchorPane.setLeftAnchor(tb.getChildren().get(0), 10.0);
        AnchorPane.setRightAnchor(tb.getChildren().get(0), 10.0);
        AnchorPane.setBottomAnchor(tb.getChildren().get(0), 10.0);
        AnchorPane.setTopAnchor(tb.getChildren().get(0), 10.0);
        Label lbl = (Label) scene.lookup("#contentNamelbl");
        lbl.setText(mainLabel);
        lbl = (Label) scene.lookup("#periodlbl");
        lbl.setText(periodFilter);
        lbl = (Label) scene.lookup("#filterlbl");
        lbl.setText(controparteFilter);
        
        Stage stage = new Stage();
        stage.setResizable(true);
        stage.setScene(scene);
        stage.show();
        
        Printer printer = Printer.getDefaultPrinter();
        PageLayout pageLayout = printer.createPageLayout(Paper.A4, PageOrientation.LANDSCAPE, Printer.MarginType.HARDWARE_MINIMUM);
        PrinterJob job = PrinterJob.createPrinterJob();
    
        double scaleX = pageLayout.getPrintableWidth() / root.getBoundsInParent().getWidth();

        double scaleY  = pageLayout.getPrintableHeight() / root.getBoundsInParent().getHeight();
        System.out.println("Printable Width: "+ pageLayout.getPrintableWidth()+" Printable Height: "+pageLayout.getPrintableHeight());
         System.out.println("Margins : "+ pageLayout.getTopMargin()+" "+pageLayout.getRightMargin()+" "+pageLayout.getBottomMargin()+" "+pageLayout.getLeftMargin());
         System.out.println("Bounds in parent Width : "+ root.getBoundsInParent().getWidth()+"Bound in parent Height: "+root.getBoundsInParent().getHeight());
         System.out.println("calculated scaleX: "+scaleX+" calculated scaleY: "+scaleY);
        Scale scale = new Scale(scaleX, scaleY);
        root.getTransforms().add(scale);

        if (job != null && job.showPrintDialog(root.getScene().getWindow())) {
          System.out.println("Printable Width: "+ pageLayout.getPrintableWidth()+" Printable Height: "+pageLayout.getPrintableHeight());
         System.out.println("Margins : "+ pageLayout.getTopMargin()+" "+pageLayout.getRightMargin()+" "+pageLayout.getBottomMargin()+" "+pageLayout.getLeftMargin());
            boolean success = job.printPage(pageLayout, root);
          if (success) {
            job.endJob();

          }
        }
        root.getTransforms().remove(scale);
       
    }
    
    

}