package primanotafacileclient;

import net.unnitech.primanotafacile.Reminder;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXCheckBox;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXDatePicker;
import com.jfoenix.controls.JFXDrawer;
import com.jfoenix.controls.JFXHamburger;
import com.jfoenix.controls.JFXListView;
import com.jfoenix.controls.JFXTextArea;
import com.jfoenix.controls.JFXTextField;
import com.jfoenix.transitions.hamburger.HamburgerBackArrowBasicTransition;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.text.NumberFormat;
import java.text.ParseException;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.beans.Observable;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.event.EventType;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Pos;
import javafx.print.*;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Control;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.Spinner;
import javafx.scene.control.SpinnerValueFactory;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableRow;
import javafx.scene.control.TableView;
import javafx.scene.control.ToggleGroup;
import javafx.scene.control.cell.CheckBoxTableCell;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;
import javafx.scene.transform.Scale;
import javafx.stage.FileChooser;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.Window;
import javafx.util.Callback;
import javafx.util.StringConverter;
import javax.mail.MessagingException;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.xml.JRXmlLoader;
import net.sf.jasperreports.swing.JRViewer;
import net.sf.jasperreports.view.JasperViewer;
import net.unnitech.primanotafacile.*;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

/**
 * FXML Controller class
 *
 * @author unnitech
 */
public class MainScreenController implements Initializable {
    
    @FXML
    private AnchorPane anchorPane;
    
    @FXML
    private JFXHamburger hamburgerMenu;
    
    @FXML
    private JFXDrawer drwMenu;
    
    @FXML
    private VBox vBoxMenu;
    
    @FXML
    private JFXButton btnScadenzario;
    
    @FXML
    private JFXButton btnPrimaNota;
    
    @FXML
    private JFXButton btnRubrica;
    
    @FXML
    private JFXButton btnImpostazioni;
    
    @FXML
    private JFXButton btnInfo;
    
    @FXML
    private TabPane tabPaneMain;
    
    @FXML
    private Tab tabScandezario;
    
    @FXML
    private AnchorPane anchorPaneScadenzario;
    
    @FXML
    private JFXButton btnScadenzarioNew;
    
    @FXML
    private JFXButton btnScadenzarioModify;
    
    @FXML
    private JFXButton btnScadenzarioDelete;
    
    @FXML
    private JFXButton btnScadenzarioPrint;
    
    @FXML
    private JFXTextField txtScadenzarioDescrizione;
    
    @FXML
    private JFXComboBox<String> comboScadenzarioControparte;
    
    @FXML
    private JFXDatePicker datePickerScadenzarioFrom;
    
    @FXML
    private JFXDatePicker datePickerScadenzarioTo;
    
     @FXML
    private JFXCheckBox checkBoxScadenzarioNascondiCompletati;
    
    @FXML
    public TableView<ReminderForTable> tableScadenzario;
    
    @FXML
    public TableColumn<ReminderForTable, String> tableScadenzarioData;
    
    @FXML
    public TableColumn<ReminderForTable, String> tableScadenzarioDescrizione;
    
    @FXML
    public TableColumn<ReminderForTable, String> tableScadenzarioControparte;
    
    @FXML
    public TableColumn<ReminderForTable, String> tableScadenzarioImporto;
    
    @FXML
    public TableColumn<ReminderForTable, Boolean> tableScadenzarioCompletato;
    
    @FXML
    public TableColumn<ReminderForTable, String> tableScadenzarioId;
    
    @FXML
    private Tab tabPrimaNota;
    
    @FXML
    private AnchorPane anchorPanePrimaNota;
    
    @FXML
    private JFXButton buttonPrimaNotaNuovo;
    
    @FXML
    private JFXButton buttonPrimaNotaModifica;
    
    @FXML
    private JFXButton buttonPrimaNotaElimina;
    
    @FXML
    private TableView<EntryForTable> tablePrimaNota;
    
    @FXML
    private TableColumn<EntryForTable, String> tablePrimaNotaData;
    
    @FXML
    private TableColumn<EntryForTable, String> tablePrimaNotaOperazione;
    
    @FXML
    private TableColumn<EntryForTable, String> tablePrimaNotaFuoriCassa;
    
    @FXML
    private TableColumn<EntryForTable, String> tablePrimaNotaCassa;
    
    @FXML
    private TableColumn<EntryForTable, String> tablePrimaNotaCassaEntrate;
    
    @FXML
    private TableColumn<EntryForTable, String> tablePrimaNotaCassaUscite;
    
    @FXML
    private TableColumn<EntryForTable, String> tablePrimaNotaBanca;
    
    @FXML
    private TableColumn<EntryForTable, String> tablePrimaNotaBancaEntrate;
    
    @FXML
    private TableColumn<EntryForTable, String> tablePrimaNotaBancaUscite;
    
    @FXML
    private TableColumn<EntryForTable, String> tablePrimaNotaSaldi;
    
    @FXML
    private TableColumn<EntryForTable, String> tablePrimaNotaSaldiCassa;
    
    @FXML
    private TableColumn<EntryForTable, String> tablePrimaNotaSaldiBanca;
    
    @FXML
    private TableColumn<EntryForTable, String> tablePrimaNotaBankId;
    
    @FXML
    private TableColumn<EntryForTable, String> tablePrimaNotaContactId;

    @FXML
    private TableColumn<EntryForTable, String> tablePrimaNotaCategoryId;
    
    @FXML
    private JFXComboBox<String> comboPrimaNotaBanca;
    
    @FXML
    private RadioButton radioPrimaNotaFilterTutto;
    
    @FXML
    private RadioButton radioPrimaNotaFilterSoloBanca;
    
    @FXML
    private RadioButton radioPrimaNotaFilterSoloCassa;
       
    @FXML
    private JFXTextField txtPrimaNotaDescrizione;
    
    @FXML
    private JFXComboBox<String> comboPrimaNotaControparte;
    
    @FXML
    private JFXComboBox<String> comboPrimaNotaCategoria;
    
    @FXML
    private JFXDatePicker datePickerPrimaNotaFrom;
    
    @FXML
    private JFXDatePicker datePickerPrimaNotaTo;
    
    @FXML
    private Spinner<String> spinnerPrimaNotaMese;
    
    @FXML
    private Spinner<Integer> spinnerPrimaNotaAnno;
    
    @FXML
    private Label tabPrimaNotaBottomLabelData;
     
    @FXML
    private JFXButton buttonPrimaNotaExportXls;
    
    @FXML
    private JFXButton buttonPrimaNotaStampa;
    
    @FXML
    
    private JFXButton buttonPrimaNotaRiepilogo;

    @FXML
    private JFXButton buttonPrimaNotaSendByEmail;

    @FXML
    private Label tabPrimaNotaBottomLabelOperazione;

    @FXML
    private Label tabPrimaNotaBottomLabelFuoriCassa;

    @FXML
    private Label tabPrimaNotaBottomLabelCassaEmtrate;

    @FXML
    private Label tabPrimaNotaBottomLabelCassaUscite;

    @FXML
    private Label tabPrimaNotaBottomLabelBancaEntrate;

    @FXML
    private Label tabPrimaNotaBottomLabelBancaUscite;

    @FXML
    private Label tabPrimaNotaBottomLabelSaldiCassa;

    @FXML
    private Label tabPrimaNotaBottomLabelSaldiBanca;

    
    @FXML
    private Label labelPrimaNotaSaldoTotale;
    
    @FXML
    private Label labelPrimaNotaSaldoTotaleValue;
    
    @FXML
    private JFXButton buttonPrimaNotaSearch;
    
    @FXML
    private Tab tabRubrica;
    
    @FXML
    private AnchorPane anchorPaneRubrica;
    
    @FXML
    private JFXListView<String> listViewRubrica;
    
    @FXML
    private Label labelRubricaCategory;
    
    @FXML
    private JFXComboBox<?> comoBoxRubricaCategory;
    
    @FXML
    private JFXTextField txtRubricaDenominazioneFilter;
    
    @FXML
    private VBox vBoxRubricaFields;
    
    @FXML
    private Label labelRubricaDatiAnagrafici;
    
    @FXML
    private JFXTextField txtRubricaFormDenominazione;
    
    @FXML
    private JFXTextField txtRubricaFormIndirizzo;
    
    @FXML
    private JFXTextField txtRubricaFormCitta;
    
    @FXML
    private JFXTextField txtRubricaFormProvinca;
    
    @FXML
    private JFXTextField txtRubricaFormCap;
    
    @FXML
    private Label labelRubricaDatiAnagrafici1;
    
    @FXML
    private JFXTextField txtRubricaFormTelLavoro;
    
    @FXML
    private JFXTextField txtRubricaFormTelCasa;
    
    @FXML
    private JFXTextField txtRubricaFormFax;
    
    @FXML
    private JFXTextField txtRubricaFormCellullare;
    
    @FXML
    private Label labelRubricaDatiAnagrafici11;
    
    @FXML
    private JFXTextField txtRubricaFormInternetEmail1;
    
    @FXML
    private JFXTextField txtRubricaFormInternetEmail2;
    
    @FXML
    private JFXTextField txtRubricaFormInternetIM;
    
    @FXML
    private JFXTextField txtRubricaFormInternetWeb;
    
    @FXML
    private Label labelRubricaDatiAnagrafici12;
    
    @FXML
    private JFXTextArea txtRubricaFormExtra;
    
    @FXML
    private Tab tabImpostazioni;
    
    @FXML
    private AnchorPane anchorPaneImpostazioni;
    
    @FXML
    private TabPane tabPaneImpostazioni;
    
    @FXML
    private Tab tabBancaImpostazioni;
    
    @FXML
    private JFXButton btnIpostazioniBancaNew;
    
    @FXML
    private JFXButton btnIpostazioniBancaModify;
    
    @FXML
    private JFXButton btnIpostazioniBancaDelete;
    
    @FXML
    private JFXListView<String> listViewImpostazioniBanca;
    
    @FXML
    private Tab tabCategorieImpostazioni;
    
    @FXML
    private JFXButton btnIpostazioniCategorieNew;
    
    @FXML
    private JFXButton btnIpostazioniCategorieModify;
    
    @FXML
    private JFXButton btnIpostazioniCategorieDelete;
    
    @FXML
    private JFXListView<RadioButton> listViewCategorieImpostazioni;
    
    @FXML
    private Tab tabOperazioniImpostazioni;
    
    @FXML
    private JFXButton btnIpostazioniOperazioniNew;
    
    @FXML
    private JFXButton btnIpostazioniOperazioniModify;
    
    @FXML
    private JFXButton btnIpostazioniOperazioniDelete;
    
    @FXML
    private JFXListView<String> listViewImpostazioniOperazioni;
    
    @FXML
    private Tab tabAziendaImpostazioni;
    
    @FXML
    private JFXButton btnIpostazioniAziendaNew;
    
    @FXML
    private JFXButton btnIpostazioniAziendaModify;
    
    @FXML
    private JFXButton btnIpostazioniAziendaDelete;
    
    @FXML
    private Button btnNuovoRubrica;
    
    @FXML
    private Button btnModificaRubrica;
    
    @FXML
    private Button btnStampaRubrica;
    
    @FXML
    private Button btnEliminaRubrica;
    
    @FXML
    private JFXListView<String> listViewImpostazioniAzienda;
    
    @FXML
    private Tab tabInfo;
    
    @FXML
    private AnchorPane anchorPaneInfo;
    
    @FXML
    private ImageView imageViewAds;
    
    private final LinkedHashMap<Integer, String> comboContact = new LinkedHashMap<>();
    private final LinkedHashMap<Integer, String> comboCategoryHash = new LinkedHashMap<>();
    private final LinkedHashMap<Integer, String> comboBankPrimaNotaHash = new LinkedHashMap<>();
    private final HashMap<String, String> banks = new HashMap<>();
    private final HashMap<String, String> contacts = new HashMap<>();
    private final HashMap<String, OperationType> operationTypes = new HashMap<>();
    private ReminderForTable selectedReminder;
    private EntryForTable selectedEntry;
    private String selectedBank;
    private String selectedContact;
    private String selectedOperationType;
    private List<Contact> allContacts;
    ToggleGroup primaNotaRadioFilterGroup;
//    private NewReminder newReminder = new NewReminder();

    @FXML 
    void handleImagaView(){
            //method will search for Url of image and will be called in initialization of MainScreen controller
         //URL must be stored in a configAd.json file
          File f = new File("configAd.json");
         String imageSource = "";
             if (f.exists() && !f.isDirectory()) {
                  JSONParser parser = new JSONParser();
            try {
                JSONObject urlImage = (JSONObject) parser.parse(new FileReader("configAd.json"));
                imageSource = (String) urlImage.get("adUrl");
                 
            } catch (FileNotFoundException e) {
                Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, e);
            } catch (IOException | org.json.simple.parser.ParseException e) {
                Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, e);
            }
            
            }
           
         
        imageViewAds.setImage(new Image(imageSource));
        imageViewAds.fitWidthProperty().bind(anchorPane.widthProperty().add(-150.0));
       //imageViewAds.setFitHeight(42);
      // imageViewAds.setFitWidth(tabPaneMain.getWidth());
       imageViewAds.setPreserveRatio(false);
                
                //ImageViewBuilder.create().image(new Image(imageSource)).build();
            
    }
    
    @FXML
    void handleImpostazioni(MouseEvent event) {
        btnScadenzario.getStyleClass().set(0, "menu-item");
        btnPrimaNota.getStyleClass().set(0, "menu-item");
        btnInfo.getStyleClass().set(0, "menu-item");
        btnImpostazioni.getStyleClass().set(0, "selected-menu-item");
        btnRubrica.getStyleClass().set(0, "menu-item");
        tabPaneMain.getSelectionModel().select(tabImpostazioni);
        handleImpostazioniInit();
        //anchorPane.setBackground(new Background(new BackgroundFill(Paint.valueOf("#673AB7"), CornerRadii.EMPTY, Insets.EMPTY)));
    }
    
    @FXML
    void handleImpostazioniInit() {
        ArchivesPortService archiveService = new ArchivesPortService();
        ArchivesPort archivesPort = archiveService.getArchivesPortSoap11();
        GetAllBankRequest allBankRequest = new GetAllBankRequest();
        
        allBankRequest.setArchiveName(PrimaNotaFacileClient.getCurrentArchive() + ".sqlite3");
        GetAllBankResponse allBankResponse = archivesPort.getAllBank(allBankRequest);
        List<Bank> allBanks = allBankResponse.getAllBankResponse();
        
        List<String> data = listViewImpostazioniBanca.getItems();
        data.removeAll(listViewImpostazioniBanca.getItems());
        for (Bank bnk : allBanks) {
            if ("".equals(bnk.getDescription())) {
                data.add(bnk.getName());
            } else {
                data.add(bnk.getName() + " - " + bnk.getDescription());
            }
            
            banks.put(bnk.getName(), bnk.getBankId());
        }
    }
    
    @FXML
    void handleInfo(MouseEvent event) {
        
        btnScadenzario.getStyleClass().set(0, "menu-item");
        btnPrimaNota.getStyleClass().set(0, "menu-item");
        btnInfo.getStyleClass().set(0, "selected-menu-item");
        btnImpostazioni.getStyleClass().set(0, "menu-item");
        btnRubrica.getStyleClass().set(0, "menu-item");
        tabPaneMain.getSelectionModel().select(tabInfo);
        //anchorPane.setBackground(new Background(new BackgroundFill(Paint.valueOf("#03A9F4"), CornerRadii.EMPTY, Insets.EMPTY)));
    }
    
    @FXML
    void handlePrimaNota(MouseEvent event) throws ParseException {
        
        btnScadenzario.getStyleClass().set(0, "menu-item");
        btnPrimaNota.getStyleClass().set(0, "selected-menu-item");
        btnInfo.getStyleClass().set(0, "menu-item");
        btnImpostazioni.getStyleClass().set(0, "menu-item");
        btnRubrica.getStyleClass().set(0, "menu-item");
        tabPaneMain.getSelectionModel().select(tabPrimaNota);
        //anchorPane.setBackground(new Background(new BackgroundFill(Paint.valueOf("#43A047"), CornerRadii.EMPTY, Insets.EMPTY)));
        //in first click datefields will display first day and last day of month
        LocalDate today = LocalDate.now();
        LocalDate start = today.withDayOfMonth(1);
        LocalDate end = today.withDayOfMonth(today.lengthOfMonth());
        String Dita;
        String Muaji;
        String Viti;
        String DataPrompt;
        datePickerPrimaNotaFrom.setValue(start);
        Dita = String.valueOf(start.getDayOfMonth());
        Muaji = String.valueOf(start.getMonthValue());
        Viti = String.valueOf(start.getYear());
        DataPrompt = Dita + "-" + Muaji + "-" + Viti;
        datePickerPrimaNotaFrom.setPromptText(DataPrompt);
        
        datePickerPrimaNotaTo.setValue(end);
        Dita = String.valueOf(end.getDayOfMonth());
        Muaji = String.valueOf(end.getMonthValue());
        Viti = String.valueOf(end.getYear());
        DataPrompt = Dita + "-" + Muaji + "-" + Viti;
        datePickerPrimaNotaTo.setPromptText(DataPrompt);
        //set values for spinner
        SpinnerValueFactory valueFactory = new SpinnerValueFactory.IntegerSpinnerValueFactory(1900, 2100, start.getYear());
        spinnerPrimaNotaAnno.setValueFactory(valueFactory);
        ObservableList<String> months = FXCollections.observableArrayList(
               "GENNAIO", "FEBBRAIO", "MARZO", "APRILE", 
               "MAGGIO", "GIUNGO", "LUGLIO", "AGOSTO", 
               "SETTEMBRE", "OTTOBRE", "NOVEMBRE", "DICEMBRE");
       
        SpinnerValueFactory meseValueFactory = new SpinnerValueFactory.ListSpinnerValueFactory<String>(months);
        // Default value
        String monthString = null;
        switch (start.getMonthValue()) {
            case 1:  monthString = "GENNAIO";
                     break;
            case 2:  monthString = "FEBBRAIO";
                     break;
            case 3:  monthString = "MARZO";
                     break;
            case 4:  monthString = "APRILE";
                     break;
            case 5:  monthString = "MAGGIO";
                     break;
            case 6:  monthString = "GIUNGO";
                     break;
            case 7:  monthString = "LUGLIO";
                     break;
            case 8:  monthString = "AGOSTO";
                     break;
            case 9:  monthString = "SETTEMBRE";
                     break;
            case 10: monthString = "OTTOBRE";
                     break;
            case 11: monthString = "NOVEMBRE";
                     break;
            case 12: monthString = "DICEMBRE";
                     break;
            
        }
        //default value
       meseValueFactory.setValue(monthString);
 
       spinnerPrimaNotaMese.setValueFactory(meseValueFactory);
        
       ArchivesPortService archiveService = new ArchivesPortService();
        ArchivesPort archivesPort = archiveService.getArchivesPortSoap11();
       
       //gettin all banks and putting them in combo prima nota and hashmap
        GetAllBankRequest allBankRequest = new GetAllBankRequest();
        allBankRequest.setArchiveName(PrimaNotaFacileClient.getCurrentArchive() + ".sqlite3");
        GetAllBankResponse allBankResponse = archivesPort.getAllBank(allBankRequest);
        List<Bank> allBanks = allBankResponse.getAllBankResponse();
        //refreshing combo and hashmap
        comboPrimaNotaBanca.getItems().remove(0, comboPrimaNotaBanca.getItems().size());
        comboBankPrimaNotaHash.clear();
        ObservableList<String> comboBanks = comboPrimaNotaBanca.getItems();
        comboBanks.add("Tutte");
        for(Bank aBank : allBanks){
            comboBanks.add(aBank.getName());
            comboBankPrimaNotaHash.put(Integer.parseInt(aBank.getBankId()),aBank.getName());
        } 
       
        //gettin all operation types and putting them to combo and hashmap
        GetAllOperationTypesRequest allOperationTypesRequest = new GetAllOperationTypesRequest();
        allOperationTypesRequest.setArchiveName(PrimaNotaFacileClient.getCurrentArchive() + ".sqlite3");
        GetAllOperationTypesResponse allOperationTypesResponse = archivesPort.getAllOperationTypes(allOperationTypesRequest);
        List<OperationType> allOperationTypes = allOperationTypesResponse.getAllOperationTypesResponse();
        //refreshing combo and hash map
        comboPrimaNotaCategoria.getItems().remove(0, comboPrimaNotaCategoria.getItems().size());
        comboCategoryHash.clear();
        ObservableList<String> comboCategories = comboPrimaNotaCategoria.getItems();
        comboCategories.add("Generico");
        for (OperationType op : allOperationTypes){
            comboCategories.add(op.getName());
            comboCategoryHash.put(Integer.parseInt(op.getOperationTypeId()),op.getName());
        }
        
        GetAllContactsRequest allContactsRequest = new GetAllContactsRequest();
        allContactsRequest.setArchiveName(PrimaNotaFacileClient.getCurrentArchive() + ".sqlite3");
        GetAllContactsResponse allContactsResponse = archivesPort.getAllContacts(allContactsRequest);

        //si pergjigje marrim nje liste me contact
        List<Contact> allcontacs = allContactsResponse.getAllContactsResponse();
        //refresh combo and hash
        comboPrimaNotaControparte.getItems().remove(0, comboPrimaNotaControparte.getItems().size());
        comboContact.clear();
        ObservableList<String> comboMainContacts = comboPrimaNotaControparte.getItems();
        //in order to have in filter a value for all contacts we're gonna add a new element in combo
        comboMainContacts.add("Tutti");
        for (Contact contact : allcontacs) {
            comboMainContacts.add(contact.getFullName());
            comboContact.put(Integer.parseInt(contact.getContactId()), contact.getFullName());
        }
        
        //make comboPrimaNota Disabled by default.
        //select by default radioButton Tutti
        primaNotaRadioFilterGroup = new ToggleGroup();
        radioPrimaNotaFilterSoloBanca.setToggleGroup(primaNotaRadioFilterGroup);
        radioPrimaNotaFilterSoloCassa.setToggleGroup(primaNotaRadioFilterGroup);
        radioPrimaNotaFilterTutto.setToggleGroup(primaNotaRadioFilterGroup);
        
        radioPrimaNotaFilterTutto.setSelected(true);
        handlePrimaNotaRadioFilters();
        
        
        
        handleVisualizzaDettagli();
        
         
              
        
    }
    
    @FXML
    void handleRubrica(MouseEvent event) {
        btnScadenzario.getStyleClass().set(0, "menu-item");
        btnPrimaNota.getStyleClass().set(0, "menu-item");
        btnInfo.getStyleClass().set(0, "menu-item");
        btnImpostazioni.getStyleClass().set(0, "menu-item");
        btnRubrica.getStyleClass().set(0, "selected-menu-item");
        //btnRubrica.setStyle("-fx-background-color: #42A5F5");

        tabPaneMain.getSelectionModel().select(tabRubrica);
        //anchorPane.setBackground(new Background(new BackgroundFill(Paint.valueOf("#0288D1"), CornerRadii.EMPTY, Insets.EMPTY)));
        handleRubricaTabInit();
        
        txtRubricaFormDenominazione.setText(listViewRubrica.getSelectionModel().getSelectedItem());
        
        txtRubricaFormDenominazione.setEditable(false);
        txtRubricaFormIndirizzo.setEditable(false);
        txtRubricaFormCitta.setEditable(false);
        txtRubricaFormProvinca.setEditable(false);
        txtRubricaFormCap.setEditable(false);
        txtRubricaFormTelLavoro.setEditable(false);
        txtRubricaFormTelCasa.setEditable(false);
        txtRubricaFormCellullare.setEditable(false);
        txtRubricaFormExtra.setEditable(false);
        txtRubricaFormInternetEmail1.setEditable(false);
        txtRubricaFormInternetEmail2.setEditable(false);
        txtRubricaFormInternetIM.setEditable(false);
        txtRubricaFormInternetWeb.setEditable(false);
        txtRubricaFormFax.setEditable(false);
    }
    
    @FXML
    void handleScandezario(MouseEvent event) {
        //test changin background image
        //when clicking this button, every other button will be transparent backgrounded. this button will be selected and the
        //first tab of the pane will be selected.
        //same aplies for other buttons

        btnScadenzario.getStyleClass().set(0, "selected-menu-item");
        btnPrimaNota.getStyleClass().set(0, "menu-item");
        btnInfo.getStyleClass().set(0, "menu-item");
        btnImpostazioni.getStyleClass().set(0, "menu-item");
        btnRubrica.getStyleClass().set(0, "menu-item");
        
        tabPaneMain.getSelectionModel().select(tabScandezario);
        
        ArchivesPortService archiveService = new ArchivesPortService();
        ArchivesPort archivesPort = archiveService.getArchivesPortSoap11();
        GetAllContactsRequest allContactsRequest = new GetAllContactsRequest();
        allContactsRequest.setArchiveName(PrimaNotaFacileClient.getCurrentArchive() + ".sqlite3");
        GetAllContactsResponse allContactsResponse = archivesPort.getAllContacts(allContactsRequest);

        //si pergjigje marrim nje liste me contact
        List<Contact> allcontacs = allContactsResponse.getAllContactsResponse();
        //refresh combo and hash
        comboScadenzarioControparte.getItems().remove(0, comboScadenzarioControparte.getItems().size());
        comboContact.clear();
        ObservableList<String> comboMainContacts = comboScadenzarioControparte.getItems();
        //in order to have in filter a value for all contacts we're gonna add a new element in combo
        comboMainContacts.add("Tutti");
        for (Contact contact : allcontacs) {
            comboMainContacts.add(contact.getFullName());
            comboContact.put(Integer.parseInt(contact.getContactId()), contact.getFullName());
        }

        //anchorPane.setBackground(new Background(new BackgroundFill(Paint.valueOf("#F4511E"), CornerRadii.EMPTY, Insets.EMPTY)));
        handleScadenzarioEventsForFilters();
    }
    
    @FXML
    void handleRemoveFocusFromOtherElementsRubrica() {
        txtRubricaDenominazioneFilter.clear();
        listViewRubrica.getSelectionModel().clearSelection();
        anchorPaneRubrica.requestFocus();
        txtRubricaFormDenominazione.clear();
        txtRubricaFormIndirizzo.clear();
        txtRubricaFormCitta.clear();
        txtRubricaFormProvinca.clear();
        txtRubricaFormCap.clear();
        txtRubricaFormTelLavoro.clear();
        txtRubricaFormTelCasa.clear();
        txtRubricaFormCellullare.clear();
        txtRubricaFormExtra.clear();
        txtRubricaFormInternetEmail1.clear();
        txtRubricaFormInternetEmail2.clear();
        txtRubricaFormInternetIM.clear();
        txtRubricaFormInternetWeb.clear();
        txtRubricaFormFax.clear();
    }
    
    @FXML
    void handleRubricaListViewClick() {
        anchorPaneRubrica.requestFocus();
        txtRubricaDenominazioneFilter.clear();
        listViewRubrica.getSelectionModel().clearSelection();
        listViewRubrica.requestFocus();
        txtRubricaFormDenominazione.clear();
        txtRubricaFormIndirizzo.clear();
        txtRubricaFormCitta.clear();
        txtRubricaFormProvinca.clear();
        txtRubricaFormCap.clear();
        txtRubricaFormTelLavoro.clear();
        txtRubricaFormTelCasa.clear();
        txtRubricaFormCellullare.clear();
        txtRubricaFormExtra.clear();
        txtRubricaFormInternetEmail1.clear();
        txtRubricaFormInternetEmail2.clear();
        txtRubricaFormInternetIM.clear();
        txtRubricaFormInternetWeb.clear();
        txtRubricaFormFax.clear();
        
    }
    
    @FXML
    void handleRubricaTabInit() {
        txtRubricaDenominazioneFilter.clear();
        listViewRubrica.getSelectionModel().clearSelection();
        
        txtRubricaFormDenominazione.clear();
        txtRubricaFormIndirizzo.clear();
        txtRubricaFormCitta.clear();
        txtRubricaFormProvinca.clear();
        txtRubricaFormCap.clear();
        txtRubricaFormTelLavoro.clear();
        txtRubricaFormTelCasa.clear();
        txtRubricaFormCellullare.clear();
        txtRubricaFormExtra.clear();
        txtRubricaFormInternetEmail1.clear();
        txtRubricaFormInternetEmail2.clear();
        txtRubricaFormInternetIM.clear();
        txtRubricaFormInternetWeb.clear();
        txtRubricaFormFax.clear();
        
        ArchivesPortService archiveService = new ArchivesPortService();
        ArchivesPort archivesPort = archiveService.getArchivesPortSoap11();
        GetAllContactsRequest allContactRequest = new GetAllContactsRequest();
        
        allContactRequest.setArchiveName(PrimaNotaFacileClient.getCurrentArchive() + ".sqlite3");
        GetAllContactsResponse allContactResponse = archivesPort.getAllContacts(allContactRequest);
        allContacts = allContactResponse.getAllContactsResponse();
        
        List<String> data = listViewRubrica.getItems();
        data.removeAll(listViewRubrica.getItems());
        for (Contact cnt : allContacts) {
            
            data.add(cnt.getFullName());
            
            contacts.put(cnt.getFullName(), cnt.getContactId());
        }
//        listViewRubrica.getSelectionModel().selectAll();
        listViewRubrica.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> ov, String old_val, String new_val) {
                if ("".equals(listViewRubrica.getSelectionModel().getSelectedItem())) {
                } else {
                    
                    for (Contact contact : allContacts) {
                        if (contact.getFullName().equals(listViewRubrica.getSelectionModel().getSelectedItem())) {
                            txtRubricaFormDenominazione.setText(listViewRubrica.getSelectionModel().getSelectedItem());
                            txtRubricaFormIndirizzo.setText(contact.getAddress());
                            txtRubricaFormCitta.setText(contact.getCity());
                            txtRubricaFormProvinca.setText(contact.getState());
                            txtRubricaFormCap.setText(contact.getZipCode());
                            txtRubricaFormTelLavoro.setText(contact.getPhoneWork());
                            txtRubricaFormTelCasa.setText(contact.getPhoneHome());
                            txtRubricaFormCellullare.setText(contact.getMobileNumber());
                            txtRubricaFormExtra.setText(contact.getNote());
                            txtRubricaFormInternetEmail1.setText(contact.getEMail1());
                            txtRubricaFormInternetEmail2.setText(contact.getEMail2());
                            txtRubricaFormInternetIM.setText(contact.getInstantMessage());
                            txtRubricaFormInternetWeb.setText(contact.getWeb());
                            txtRubricaFormFax.setText(contact.getFax());
                        }
                    }
                }
            }
        });
    }
    
    @FXML
    void handleNuovoRubrica() {
        Contact newContact = new Contact();
        //set the reminder id to 0 to know that's new reminder
        newContact.setContactId("0");
        
        boolean okClicked = showContactEditDialog(newContact);
        if (okClicked) {
            //if result true refresh table 
            //or ADD an info  if you want
        }
        handleRubricaTabInit();
    }
    
    @FXML
    void handleModificaRubrica() {
        selectedContact = listViewRubrica.getSelectionModel().getSelectedItem();
        if (selectedContact == null) {
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("Warning");
            alert.setHeaderText("Warning");
            Text text = new Text("Si prega di selezionare un Contatto da modificare ");
            text.setWrappingWidth(400);
            alert.getDialogPane().setContent(text);
            Optional<ButtonType> result = alert.showAndWait();
            if (result.get() == ButtonType.OK) {
                alert.close();
            }
        } else {
            
            Contact existingContact = new Contact();
            existingContact.setFullName(selectedContact);
            
            ArchivesPortService archiveService = new ArchivesPortService();
            ArchivesPort archivesPort = archiveService.getArchivesPortSoap11();
            GetContactRequest contactRequest = new GetContactRequest();
            
            contactRequest.setArchiveName(PrimaNotaFacileClient.getCurrentArchive() + ".sqlite3");
            contactRequest.setContactId(contacts.get(selectedContact));
            GetContactResponse contactResponse = archivesPort.getContact(contactRequest);
            Contact contactById = contactResponse.getContact();
            existingContact.setContactId(contactById.getContactId());
            existingContact.setAddress(contactById.getAddress());
            existingContact.setCity(contactById.getCity());
            existingContact.setEMail1(contactById.getEMail1());
            existingContact.setEMail2(contactById.getEMail2());
            existingContact.setFax(contactById.getFax());
            existingContact.setInstantMessage(contactById.getInstantMessage());
            existingContact.setWeb(contactById.getWeb());
            existingContact.setNote(contactById.getNote());
            existingContact.setMobileNumber(contactById.getMobileNumber());
            existingContact.setPhoneHome(contactById.getPhoneHome());
            existingContact.setPhoneWork(contactById.getPhoneWork());
            existingContact.setZipCode(contactById.getZipCode());
            existingContact.setState(contactById.getState());
            
            boolean okClicked = showContactEditDialog(existingContact);
            if (okClicked) {
                //if result true refresh table 
                //or ADD an info  if you want
            }
            //if ok is false

            //anyway refresh table scadenzario
            handleRubricaTabInit();
        }
    }
    
    @FXML
    void filterRubricaByName() {
        ArchivesPortService archiveService = new ArchivesPortService();
        ArchivesPort archivesPort = archiveService.getArchivesPortSoap11();
        
        GetContactsLikeNameRequest getContactByNameRequest = new GetContactsLikeNameRequest();
        getContactByNameRequest.setArchiveName(PrimaNotaFacileClient.getCurrentArchive() + ".sqlite3");
        getContactByNameRequest.setFullName(txtRubricaDenominazioneFilter.getText());
        GetContactsLikeNameResponse getContactByNameResponse = archivesPort.getContactsLikeName(getContactByNameRequest);
        List<Contact> allContacts = getContactByNameResponse.getAllContactsLikeNameResponse();
        
        List<String> data = listViewRubrica.getItems();
        data.removeAll(listViewRubrica.getItems());
        for (Contact cnt : allContacts) {
            
            data.add(cnt.getFullName());
            contacts.put(cnt.getFullName(), cnt.getContactId());
        }
    }
    
    @FXML
    void handleEliminaRubrica() {
        Stage dialogStage;
        selectedContact = listViewRubrica.getSelectionModel().getSelectedItem();
        if (selectedContact == null) {
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("Warning");
            alert.setHeaderText("Warning");
            Text text = new Text("Si prega di selezionare un Contatto da eliminare ");
            text.setWrappingWidth(400);
            alert.getDialogPane().setContent(text);
            //alert.setContentText("Si prega di selezionare un scadenzario da modificare ");
            Optional<ButtonType> result = alert.showAndWait();
            if (result.get() == ButtonType.OK) {
                alert.close();
            }
        } else {
//            PrimaNotaFacileClient.getInstance().setCurrentEntry(selectedEntry.getTablePrimaNotaId());
//            System.out.println(PrimaNotaFacileClient.getInstance().getCurrentEntry());

            Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
            alert.setTitle("Conferma azione");
            alert.setHeaderText("Conferma azione");
            alert.setContentText("Sei sicuro di voler eliminare Contatto ?");
            Optional<ButtonType> result = alert.showAndWait();
            if (result.get() == ButtonType.OK) {
                ArchivesPortService archiveService = new ArchivesPortService();
                ArchivesPort archivesPort = archiveService.getArchivesPortSoap11();
                DeleteContactRequest deleteContact = new DeleteContactRequest();
                deleteContact.setContactId(contacts.get(selectedContact));
                deleteContact.setArchiveName(PrimaNotaFacileClient.getCurrentArchive() + ".sqlite3");
                DeleteContactResponse deleteContactResponse = archivesPort.deleteContact(deleteContact);
                String response = deleteContactResponse.getResponse();
                if ("OK".equals(response)) {
                    listViewRubrica.setEditable(true);
                    listViewRubrica.getItems().remove(selectedContact);
                    System.out.println("Reminder deleted!");
                } else {
                    System.out.println("Error delete Bank!");
                }
            }
        }
    }
    
    @FXML
    void handleStampaRubrica() {
        
    }
    
    @FXML
    public boolean showContactEditDialog(Contact Contact) {
        try {

            // Load the fxml file and create a new stage for the popup dialog.
            FXMLLoader loader = new FXMLLoader(getClass().getResource("NewContact.fxml"));
            Parent root2 = (Parent) loader.load();

            // Create the dialog Stage.
            Stage dialogStage = new Stage();
            dialogStage.setTitle("Contact");
            dialogStage.initModality(Modality.APPLICATION_MODAL);
            dialogStage.initOwner(btnNuovoRubrica.getScene().getWindow());
            Scene scene = new Scene(root2);
            dialogStage.setScene(scene);

            // Set the person into the controller.
            NewContactController controller = loader.getController();
            controller.setDialogStage(dialogStage);
            controller.setContact(Contact);

            // Show the dialog and wait until the user closes it
            dialogStage.showAndWait();
            
            return controller.isOkClicked();
        } catch (IOException e) {
            System.out.println(e.getMessage());
            return false;
        }
    }
    
    @FXML
    void handleImpostazioniOperazioniInit() {
        if (tabOperazioniImpostazioni.isSelected()) {
            ArchivesPortService archiveService = new ArchivesPortService();
            ArchivesPort archivesPort = archiveService.getArchivesPortSoap11();
            GetAllOperationTypesRequest allOperationTypesRequest = new GetAllOperationTypesRequest();
            allOperationTypesRequest.setArchiveName(PrimaNotaFacileClient.getInstance().getCurrentArchive() + ".sqlite3");
            GetAllOperationTypesResponse allOperationTypesResponse = archivesPort.getAllOperationTypes(allOperationTypesRequest);
            List<OperationType> allOperationTypes = allOperationTypesResponse.getAllOperationTypesResponse();
            
            List<String> data = listViewImpostazioniOperazioni.getItems();
            data.removeAll(listViewImpostazioniOperazioni.getItems());
            for (OperationType operationType : allOperationTypes) {
                
                data.add(operationType.getName());
                operationTypes.put(operationType.getName(), operationType);
            }
        }
    }
    
    @FXML
    void handleNewEmail() {
        try {

            //FXMLLoader fxmlLoaderselectedEntry.getTablePrimaNotaId() = new FXMLLoader(getClass().getResource("Demo.fxml"));
            //   Parent root1 = (Parent) fxmlLoader.load();
            //  Stage stage = new Stage();
            //  stage.initModality(Modality.APPLICATION_MODAL);
            //      stage.setTitle("ABC");
            //    stage.setScene(new Scene(root1));  
            //   stage.show();
            // Load the fxml file and create a new stage for the popup dialog.
            FXMLLoader loader = new FXMLLoader(getClass().getResource("NewEmail.fxml"));
            Parent root2 = (Parent) loader.load();

            // Create the dialog Stage.
            Stage dialogStage = new Stage();
            dialogStage.setTitle("Email");
            dialogStage.initModality(Modality.APPLICATION_MODAL);
            dialogStage.initOwner(buttonPrimaNotaNuovo.getScene().getWindow());
            Scene scene = new Scene(root2);
            dialogStage.setScene(scene);

            // Set the person into the controller.
            // Show the dialog and wait until the user closes it
            dialogStage.showAndWait();
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }
    
    private static MainScreenController instance;
    
    public MainScreenController() {
        instance = this;
    }
    
    public static MainScreenController getInstance() {
        return instance;
    }
    
    @FXML
    void addNewEmailToListView(String email) {
//        listViewImpostazioniCategorie.setEditable(true);
        RadioButton emailRadioButton = new RadioButton();
        emailRadioButton.setText(email);
        List<RadioButton> data = listViewCategorieImpostazioni.getItems();
        data.add(emailRadioButton);
    }
    
    @FXML
    void handleNewOperationType() {
        
        OperationType newOperationType = new OperationType();
        newOperationType.setOperationTypeId("0");
        boolean okClicked = showOperationTypeEditDialog(newOperationType);
        if (okClicked) {
        }
        
        handleImpostazioniOperazioniInit();
    }
    
    @FXML
    void handleModifyOperation() {
        selectedOperationType = listViewImpostazioniOperazioni.getSelectionModel().getSelectedItem();
        if (selectedOperationType == null) {
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("Warning");
            alert.setHeaderText("Warning");
            Text text = new Text("Si prega di selezionare un scadenzario da modificare ");
            text.setWrappingWidth(400);
            alert.getDialogPane().setContent(text);
            Optional<ButtonType> result = alert.showAndWait();
            if (result.get() == ButtonType.OK) {
                alert.close();
            }
        } else {
            //PrimaNotaFacileClient.getInstance().setCurrentEntry(selectedEntry.getTablePrimaNotaId());

            OperationType newOperationType = new OperationType();
            newOperationType.setOperationTypeId(operationTypes.get(selectedOperationType).getOperationTypeId());
            newOperationType.setName(selectedOperationType.toString());
            newOperationType.setIncoming(operationTypes.get(selectedOperationType).getIncoming());
            newOperationType.setOutgoing(operationTypes.get(selectedOperationType).getOutgoing());
            boolean okClicked = showOperationTypeEditDialog(newOperationType);
            if (okClicked) {
                //if result true refresh table 
                //or ADD an info  if you want
            }
            //if ok is false

            //anyway refresh table scadenzario
            handleImpostazioniOperazioniInit();
        }
    }
    
    @FXML
    public boolean showOperationTypeEditDialog(OperationType operationType) {
        try {

            //FXMLLoader fxmlLoaderselectedEntry.getTablePrimaNotaId() = new FXMLLoader(getClass().getResource("Demo.fxml"));
            //   Parent root1 = (Parent) fxmlLoader.load();
            //  Stage stage = new Stage();
            //  stage.initModality(Modality.APPLICATION_MODAL);
            //      stage.setTitle("ABC");
            //    stage.setScene(new Scene(root1));  
            //   stage.show();
            // Load the fxml file and create a new stage for the popup dialog.
            FXMLLoader loader = new FXMLLoader(getClass().getResource("NewOperationType.fxml"));
            Parent root2 = (Parent) loader.load();

            // Create the dialog Stage.
            Stage dialogStage = new Stage();
            dialogStage.setTitle("PrimaNota");
            dialogStage.initModality(Modality.APPLICATION_MODAL);
            dialogStage.initOwner(buttonPrimaNotaNuovo.getScene().getWindow());
            Scene scene = new Scene(root2);
            dialogStage.setScene(scene);

            // Set the person into the controller.
            NewOperationType controller = loader.getController();
            controller.setDialogStage(dialogStage);
            controller.setOperationType(operationType);

            // Show the dialog and wait until the user closes it
            dialogStage.showAndWait();
            
            return controller.isOkClicked();
        } catch (IOException e) {
            System.out.println(e.getMessage());
            return false;
        }
    }
    
    @FXML
    void handleOperationTypeElimina() {
        Stage dialogStage;
        selectedOperationType = listViewImpostazioniOperazioni.getSelectionModel().getSelectedItem();
        if (selectedOperationType == null) {
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("Warning");
            alert.setHeaderText("Warning");
            Text text = new Text("Si prega di selezionare un Operazione da eliminare ");
            text.setWrappingWidth(400);
            alert.getDialogPane().setContent(text);
            //alert.setContentText("Si prega di selezionare un scadenzario da modificare ");
            Optional<ButtonType> result = alert.showAndWait();
            if (result.get() == ButtonType.OK) {
                alert.close();
            }
        } else {
//            PrimaNotaFacileClient.getInstance().setCurrentEntry(selectedEntry.getTablePrimaNotaId());
//            System.out.println(PrimaNotaFacileClient.getInstance().getCurrentEntry());

            Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
            alert.setTitle("Conferma azione");
            alert.setHeaderText("Conferma azione");
            alert.setContentText("Sei sicuro di voler eliminare il Operazione ?");
            Optional<ButtonType> result = alert.showAndWait();
            if (result.get() == ButtonType.OK) {
                ArchivesPortService archiveService = new ArchivesPortService();
                ArchivesPort archivesPort = archiveService.getArchivesPortSoap11();
                DeleteOperationTypeRequest deleteOperationTypeRequest = new DeleteOperationTypeRequest();
                deleteOperationTypeRequest.setOperationTypeId(operationTypes.get(selectedOperationType).getOperationTypeId());
                deleteOperationTypeRequest.setArchiveName(PrimaNotaFacileClient.getCurrentArchive() + ".sqlite3");
                DeleteOperationTypeResponse deleteContactResponse = archivesPort.deleteOperationType(deleteOperationTypeRequest);
                String response = deleteContactResponse.getResponse();
                if ("OK".equals(response)) {
                    listViewImpostazioniOperazioni.setEditable(true);
                    listViewImpostazioniOperazioni.getItems().remove(selectedOperationType);
                    System.out.println("Reminder deleted!");
                    //initialize interface
                    handleImpostazioniOperazioniInit();
                } else {
                    System.out.println("Error delete Bank!");
                }
            }
        }
    }
    
    @FXML
    void handleScadenzarioTabInit() {
        //method will be called during window initialization and on function handleScadenzario
        //this method will call WSDL method getAllReminders
        //Since JavaFX needs a seperate data model for tables with string properties, we're gonna create a separate class for each class we get from WS
        //For Scadenzario (Reminder) we created a class with string properties ReminderForTable and we're gonna use that to display data in table
        //The table view and table columns are linked in FXML with the new Class ReminderForTable

        //Get Reminders from WS with Reminder Class      
        //set class to search from soap server
        ArchivesPortService archiveService = new ArchivesPortService();
        ArchivesPort archivesPort = archiveService.getArchivesPortSoap11();
        GetAllRemindersRequest allRemindersRequest = new GetAllRemindersRequest();
        //set the current archive to the search query
        allRemindersRequest.setArchiveName(PrimaNotaFacileClient.getCurrentArchive() + ".sqlite3");
        GetAllRemindersResponse allRemindersResponse = archivesPort.getAllReminders(allRemindersRequest);
        List<Reminder> allReminders = allRemindersResponse.getAllRemindersResponse();
        //we'll use a boolean variable to hold value of completed column
        boolean completed = false;
       tableScadenzario.getItems().clear();
        //Create observable list with class for table. add elements.
        ObservableList<ReminderForTable> data = FXCollections.observableArrayList();//= tableScadenzario.getItems();

        String Date;
        String viti;
        String muaji;
        String dita;
        for (Reminder rem : allReminders) {
            //we're gonna search in the hashmap to find the appropriate string value
            if (rem.getCompleted().equals("1")) {
                //reminder completed
                Date = rem.getDate();
                viti = Date.substring(0, 4);
                muaji = Date.substring(5, 7);
                dita = Date.substring(8, 10);
                data.add(new ReminderForTable(dita + "-" + muaji + "-" + viti, rem.getDescription(), comboContact.get(Integer.parseInt(rem.getIdContact())), NumberFormat.getCurrencyInstance(Locale.ITALY).format(Double.parseDouble(rem.getValue())), true, rem.getReminderId()));
                
            } else {
                //reminder not completed
                Date = rem.getDate();
                viti = Date.substring(0, 4);
                muaji = Date.substring(5, 7);
                dita = Date.substring(8, 10);
                data.add(new ReminderForTable(dita + "-" + muaji + "-" + viti, rem.getDescription(), comboContact.get(Integer.parseInt(rem.getIdContact())), NumberFormat.getCurrencyInstance(Locale.ITALY).format(Double.parseDouble(rem.getValue())), false, rem.getReminderId()));
            }
           //Double lesh =  NumberFormat.getCurrencyInstance(Locale.ITALIAN).format(Double.parseDouble(rem.getValue()));
        }
        
        tableScadenzario.setItems(data);  
        
        tableScadenzarioImporto.setCellFactory(new Callback<TableColumn<ReminderForTable, String>, TableCell<ReminderForTable, String>>() {
        @Override
            public TableCell<ReminderForTable, String> call(TableColumn<ReminderForTable, String> param) {
                TableCell<ReminderForTable, String> cell = new TableCell<ReminderForTable, String>() {
                         private Text text = new Text();

                {
                 text.wrappingWidthProperty().bind(tableScadenzarioImporto.widthProperty());
               
                } 
                    @Override
                    public void updateItem(final String item, boolean empty) {
                        super.updateItem(item, empty);

                        if (empty || item == null) {
                            setGraphic(null);
                        } else {
                            text.setText(item);
                            setGraphic(text);
                            text.setTextAlignment(TextAlignment.RIGHT);
                            Double value = 0.0;
                             if(!"".equals(item)){
                                try {
                                value = NumberFormat.getCurrencyInstance(Locale.ITALY).parse(item).doubleValue();
                            } catch (ParseException ex) {
                                Logger.getLogger(MainScreenController.class.getName()).log(Level.SEVERE, null, ex);
                            }
                            }
                            if(value!=0.0){                                                         
                                 //text.setStyle("-fx-font-size: 8pt;");
                                 text.setFill(Color.valueOf("#EF5350"));
                            }else{
                                //text.setStyle("-fx-font-size: 8pt;");
                            }

                        }       
                    }
                };
                return cell;
            }
        });
        
//               tableScadenzarioCompletato.setCellFactory(CheckBoxTableCell.forTableColumn(new Callback<Integer, ObservableValue<Boolean>>() {
//
//    @Override
//    public ObservableValue<Boolean> call(Integer param) {
//       //System.out.println("Scadenzario "+data.get(param).getTableScadenzarioDescrizione()+" changed value to " +data.get(param).getTableScadenzarioCompletato());
//        
//        return tableScadenzario.getItems().get(param).tableScadenzarioCompletatoProperty();
//    }
//}));
               
               tableScadenzarioCompletato.setCellFactory(new Callback<TableColumn<ReminderForTable, Boolean>, TableCell<ReminderForTable, Boolean>>() {
                    @Override
                    public TableCell<ReminderForTable, Boolean> call(TableColumn<ReminderForTable, Boolean> p) {
                        final CheckBoxTableCell<ReminderForTable, Boolean> ctCell = new CheckBoxTableCell<>();
                        ctCell.setSelectedStateCallback(new Callback<Integer, ObservableValue<Boolean>>() {
                            @Override
                            public ObservableValue<Boolean> call(Integer index) {
                                return tableScadenzario.getItems().get(index).tableScadenzarioCompletatoProperty();
                            }
                        });
                       
                        return ctCell;
                    }
                });
          int indeksiElementit = 0;
          
          for (ReminderForTable row : tableScadenzario.getItems()){
              final int indeksiElementitFinal = indeksiElementit; 
              tableScadenzario.getItems().get(indeksiElementit).tableScadenzarioCompletatoProperty().addListener(new ChangeListener<Boolean>() {
                            @Override
                            public void changed(ObservableValue<? extends Boolean> obs, Boolean wasSelected, Boolean isSelected) {
                                
                                try {
                                    handleScadenzarioCompleta(indeksiElementitFinal, wasSelected, isSelected);
                                } catch (ParseException ex) {
                                    Logger.getLogger(MainScreenController.class.getName()).log(Level.SEVERE, null, ex);
                                }
                                System.out.println("ObservableVlalu :  "+obs+" boolean wasSelected: "+wasSelected+" boolean isSelected: "+isSelected);
                            }
                        });     
          
              indeksiElementit++;
          }     
         
               
             
    }
    
    @FXML
    void handleScadenzarioFilterComboControparte(String controparte) {

        //Get Reminders from WS with Reminder Class      
        //set class to search from soap server
        ArchivesPortService archiveService = new ArchivesPortService();
        ArchivesPort archivesPort = archiveService.getArchivesPortSoap11();
        GetAllRemindersByContactRequest allRemindersByContactRequest = new GetAllRemindersByContactRequest();
        //set the current archive to the search query
        allRemindersByContactRequest.setArchiveName(PrimaNotaFacileClient.getCurrentArchive() + ".sqlite3");
        allRemindersByContactRequest.setIdContact(controparte);
        GetAllRemindersByContactResponse allRemindersByContactResponse = archivesPort.getAllRemindersByContact(allRemindersByContactRequest);
        List<Reminder> allReminders = allRemindersByContactResponse.getAllRemindersByContactResponse();
        //we'll use a boolean variable to hold value of completed column
        boolean completed = false;
        //Create observable list with class for table. add elements.
        ObservableList<ReminderForTable> data = tableScadenzario.getItems();
        data.remove(0, data.size());
        String Date;
        String viti;
        String muaji;
        String dita;
        for (Reminder rem : allReminders) {
            //we're gonna search in the hashmap to find the appropriate string value
            if (rem.getCompleted().equals("1")) {
                //reminder completed
                Date = rem.getDate();
                viti = Date.substring(0, 4);
                muaji = Date.substring(5, 7);
                dita = Date.substring(8, 10);
                data.add(new ReminderForTable(dita + "-" + muaji + "-" + viti, rem.getDescription(), comboContact.get(Integer.parseInt(rem.getIdContact())), NumberFormat.getCurrencyInstance(Locale.ITALY).format(rem.getValue()), true, rem.getReminderId()));
                
            } else {
                //reminder not completed
                Date = rem.getDate();
                viti = Date.substring(0, 4);
                muaji = Date.substring(5, 7);
                dita = Date.substring(8, 10);
                data.add(new ReminderForTable(dita + "-" + muaji + "-" + viti, rem.getDescription(), comboContact.get(Integer.parseInt(rem.getIdContact())), NumberFormat.getCurrencyInstance(Locale.ITALY).format(rem.getValue()), false, rem.getReminderId()));
            }
        }
        tableScadenzarioImporto.setCellFactory(new Callback<TableColumn<ReminderForTable, String>, TableCell<ReminderForTable, String>>() {
        @Override
            public TableCell<ReminderForTable, String> call(TableColumn<ReminderForTable, String> param) {
                TableCell<ReminderForTable, String> cell = new TableCell<ReminderForTable, String>() {
                         private Text text = new Text();

                {
                 text.wrappingWidthProperty().bind(tableScadenzarioImporto.widthProperty());
               
                } 
                    @Override
                    public void updateItem(final String item, boolean empty) {
                        super.updateItem(item, empty);

                        if (empty || item == null) {
                            setGraphic(null);
                        } else {
                            text.setText(item);
                            setGraphic(text);
                            text.setTextAlignment(TextAlignment.RIGHT);
                            Double value = 0.0;
                             if(!"".equals(item)){
                                try {
                                value = NumberFormat.getCurrencyInstance(Locale.ITALY).parse(item).doubleValue();
                            } catch (ParseException ex) {
                                Logger.getLogger(MainScreenController.class.getName()).log(Level.SEVERE, null, ex);
                            }
                            }
                            if(value!=0.0){                                                         
                                // text.setStyle("-fx-font-size: 8pt;");
                                 text.setFill(Color.valueOf("#EF5350"));
                            }else{
                              //  text.setStyle("-fx-font-size: 8pt;");
                            }

                        }       
                    }
                };
                return cell;
            }
        });
    }

    /**
     * This method will be called on every event handler of filter items for
     * scadenzario the method will make the logic and call the method wich
     * communicates with the webservice
     */
    @FXML
    void handleScadenzarioEventsForFilters() {
        //get id from the value of the selected combo
        int contactId = 0;
        for (Map.Entry<Integer, String> entry : comboContact.entrySet()) {
            //since combo box select index starts from 0 we're gonna increment n after checking for specified value in hashmap
            //in the main controller we display in table the full name of the contact so we're searching in this hashmap for key
            if (entry.getValue().equals(comboScadenzarioControparte.getSelectionModel().getSelectedItem())) {
                contactId = entry.getKey();
                break;
            }
        }
        String contactID;
        if (contactId == 0) {
            contactID = "Tutti";
        } else {
            contactID = String.valueOf(contactId);
        }

        //get text from descrizione
        String description = txtScadenzarioDescrizione.getText();

        //get text from date
        String fromDate;
        if (datePickerScadenzarioFrom.getValue() != null) {
//            int ditet = (datePickerScadenzarioFrom.getValue().getDayOfMonth());
//            int length = (int) (Math.log10(ditet) + 1);
//            int muaji = (datePickerScadenzarioFrom.getValue().getMonthValue());
//            int lengthMonth = (int) (Math.log10(muaji) + 1);
//            String ditaNeDate = Integer.toString(datePickerScadenzarioFrom.getValue().getDayOfMonth());
//            String muajiNeDate = Integer.toString(datePickerScadenzarioFrom.getValue().getMonthValue());
//            if (length == 1) {
//                ditaNeDate = "0" + datePickerScadenzarioFrom.getValue().getDayOfMonth();
//            }
//            if (lengthMonth == 1) {
//                muajiNeDate = "0" + datePickerScadenzarioFrom.getValue().getMonthValue();
//            }

            fromDate = datePickerScadenzarioFrom.getValue().toString() + " 00:00:00";
        } else {
            fromDate = "";
        }

        //get text to date
        String toDate;
        if (datePickerScadenzarioTo.getValue() != null) {
            /*int ditet2 = (datePickerScadenzarioTo.getValue().getDayOfMonth());
            int length2 = (int) (Math.log10(ditet2) + 1);
            int muaji2 = (datePickerScadenzarioTo.getValue().getMonthValue());
            int lengthMonth2 = (int) (Math.log10(muaji2) + 1);
            String ditaNeDate2 = Integer.toString(datePickerScadenzarioTo.getValue().getDayOfMonth());
            String muajiNeDate2 = Integer.toString(datePickerScadenzarioTo.getValue().getMonthValue());
            if (length2 == 1) {
                ditaNeDate2 = "0" + datePickerScadenzarioTo.getValue().getDayOfMonth();
            }
            if (lengthMonth2 == 1) {
                muajiNeDate2 = "0" + datePickerScadenzarioTo.getValue().getMonthValue();
            }
             */
            toDate = datePickerScadenzarioTo.getValue().toString() + " 00:00:00";
        } else {
            toDate = "";
        }
        
        //get value of checkbox
        Boolean hideCompleted =  checkBoxScadenzarioNascondiCompletati.isSelected();
        

        //call method which talks to webservice
        handleScadenzarioFilterCustom(description, contactID, fromDate, toDate,hideCompleted);
        
    }
    
    void handleScadenzarioFilterCustom(String descrizione, String controparte, String fromDate, String toDate, Boolean hideCompleted) {

        //Get Reminders from WS with Reminder Class      
        //set class to search from soap server
        ArchivesPortService archiveService = new ArchivesPortService();
        ArchivesPort archivesPort = archiveService.getArchivesPortSoap11();
        GetAllRemindersCustomRequest allRemindersCustomRequest = new GetAllRemindersCustomRequest();
        //set the current archive to the search query
        allRemindersCustomRequest.setArchiveName(PrimaNotaFacileClient.getCurrentArchive() + ".sqlite3");
        
        String query = "SELECT * FROM reminder WHERE 1=1 ";
        if (!"".equals(descrizione)) {
            query += " AND description like '%" + descrizione + "%' ";
            if (!"Tutti".equals(controparte)) {
                query += " AND id_contact = '" + controparte + "' ";
                //vazhdojme kontrollin e te tjerave
                //kontrollojme per start date
                if (!"".equals(fromDate)) {
                    //ka shenuar from date
                    query += " AND date >= '" + fromDate + "' ";
                    //kontrollojme per end date
                    if (!"".equals(toDate)) {
                        //eshte shenuar end date
                        query += " AND date <= '" + toDate + "' ";
                    } else {
                        //nuk eshte shenuar end date
                        //query += " ;";
                    }
                } else {
                    //from date eshte bosh nuk e marrim parasysh 
                    //do kontrollojme per end date
                    if (!"".equals(toDate)) {
                        //eshte shenuar end date
                        query += " AND date <= '" + toDate + "' ";
                    } else {
                        //nuk eshte shenuar end date
                        //query += " ;";
                    }
                }
            } else {
                //eshte zgjedh tutti nuk do merret parasysh filtrimi i combo contact
                //kontrollojme per start date
                if (!"".equals(fromDate)) {
                    //ka shenuar from date
                    query += " AND date >= '" + fromDate + "' ";
                    //kontrollojme per end date
                    if (!"".equals(toDate)) {
                        //eshte shenuar end date
                        query += " AND date <= '" + toDate + "' ";
                    } else {
                        //nuk eshte shenuar end date
                        //query += " ;";
                    }
                } else {
                    //from date eshte bosh nuk e marrim parasysh 
                    //do kontrollojme per end date
                    if (!"".equals(toDate)) {
                        //eshte shenuar end date
                        query += " AND date <= '" + toDate + "' ";
                    } else {
                        //nuk eshte shenuar end date
                        //query += " ;";
                    }
                }
            }
        } else {
            //descrizione bosh vazhdo me kontrollin e te tjerave
            if (!"Tutti".equals(controparte)) {
                query += " AND id_contact = '" + controparte + "' ";
                //vazhdojme kontrollin e te tjerave
                //kontrollojme per start date
                if (!"".equals(fromDate)) {
                    //ka shenuar from date
                    query += " AND date >= '" + fromDate + "' ";
                    //kontrollojme per end date
                    if (!"".equals(toDate)) {
                        //eshte shenuar end date
                        query += " AND date <= '" + toDate + "' ";
                    } else {
                        //nuk eshte shenuar end date
                       // query += " ;";
                    }
                } else {
                    //from date eshte bosh nuk e marrim parasysh 
                    //do kontrollojme per end date
                    if (!"".equals(toDate)) {
                        //eshte shenuar end date
                        query += " AND date <= '" + toDate + "' ";
                    } else {
                        //nuk eshte shenuar end date
                       // query += " ;";
                    }
                }
            } else {
                //eshte zgjedh tutti nuk do merret parasysh filtrimi i combo contact
                //kontrollojme per start date
                if (!"".equals(fromDate)) {
                    //ka shenuar from date
                    query += " AND date >= '" + fromDate + "' ";
                    //kontrollojme per end date
                    if (!"".equals(toDate)) {
                        //eshte shenuar end date
                        query += " AND date <= '" + toDate + "'";
                    } else {
                        //nuk eshte shenuar end date
                       // query += " ;";
                    }
                } else {
                    //from date eshte bosh nuk e marrim parasysh 
                    //do kontrollojme per end date
                    if (!"".equals(toDate)) {
                        //eshte shenuar end date
                        query += " WHERE date <= '" + toDate + "' ";
                    } else {
                        //nuk eshte shenuar end date
                       // query += " ;";
                    }
                }
            }
        }
        
        //filter for completed
        if (hideCompleted == true){
            //user wants to hide completed so will show only those not completed
             query += " AND completed = 0 ";
        }else{
            //users wants all of them 
            //do nothing
        }
       
        //order by date asc
        query += " ORDER BY date asc;";
        
        System.out.println(query);
        allRemindersCustomRequest.setQuery(query);
        GetAllRemindersCustomResponse allRemindersCustomResponse = archivesPort.getAllRemindersCustom(allRemindersCustomRequest);
        List<Reminder> allReminders = allRemindersCustomResponse.getAllRemindersCustomResponse();
        //we'll use a boolean variable to hold value of completed column
        boolean completed = false;
         tableScadenzario.getItems().clear();
        //Create observable list with class for table. add elements.
        ObservableList<ReminderForTable> data = FXCollections.observableArrayList();//tableScadenzario.getItems();
        //data.remove(0, data.size());
        String Date;
        String viti;
        String muaji;
        String dita;
        for (Reminder rem : allReminders) {
            //we're gonna search in the hashmap to find the appropriate string value
            if (rem.getCompleted().equals("1")) {
                //reminder completed
                Date = rem.getDate();
                viti = Date.substring(0, 4);
                muaji = Date.substring(5, 7);
                dita = Date.substring(8, 10);
                data.add(new ReminderForTable(dita + "-" + muaji + "-" + viti, rem.getDescription(), comboContact.get(Integer.parseInt(rem.getIdContact())), NumberFormat.getCurrencyInstance(Locale.ITALY).format(Double.parseDouble(rem.getValue())), true, rem.getReminderId()));
                
            } else {
                //reminder not completed
                Date = rem.getDate();
                viti = Date.substring(0, 4);
                muaji = Date.substring(5, 7);
                dita = Date.substring(8, 10);
                data.add(new ReminderForTable(dita + "-" + muaji + "-" + viti, rem.getDescription(), comboContact.get(Integer.parseInt(rem.getIdContact())), NumberFormat.getCurrencyInstance(Locale.ITALY).format(Double.parseDouble(rem.getValue())), false, rem.getReminderId()));
            }
        }
        
        tableScadenzario.setItems(data);  
        
        tableScadenzarioImporto.setCellFactory(new Callback<TableColumn<ReminderForTable, String>, TableCell<ReminderForTable, String>>() {
        @Override
            public TableCell<ReminderForTable, String> call(TableColumn<ReminderForTable, String> param) {
                TableCell<ReminderForTable, String> cell = new TableCell<ReminderForTable, String>() {
                         private Text text = new Text();

                {
                 text.wrappingWidthProperty().bind(tableScadenzarioImporto.widthProperty());
               
                } 
                    @Override
                    public void updateItem(final String item, boolean empty) {
                        super.updateItem(item, empty);

                        if (empty || item == null) {
                            setGraphic(null);
                        } else {
                            text.setText(item);
                            setGraphic(text);
                            text.setTextAlignment(TextAlignment.RIGHT);
                            Double value = 0.0;
                             if(!"".equals(item)){
                                try {
                                value = NumberFormat.getCurrencyInstance(Locale.ITALY).parse(item).doubleValue();
                            } catch (ParseException ex) {
                                Logger.getLogger(MainScreenController.class.getName()).log(Level.SEVERE, null, ex);
                            }
                            }
                            if(value!=0.0){                                                         
                                // text.setStyle("-fx-font-size: 8pt;");
                                 text.setFill(Color.valueOf("#EF5350"));
                            }else{
                               // text.setStyle("-fx-font-size: 8pt;");
                            }

                        }       
                    }
                };
                return cell;
            }
        });
        
        tableScadenzarioCompletato.setCellFactory(new Callback<TableColumn<ReminderForTable, Boolean>, TableCell<ReminderForTable, Boolean>>() {
                    @Override
                    public TableCell<ReminderForTable, Boolean> call(TableColumn<ReminderForTable, Boolean> p) {
                        final CheckBoxTableCell<ReminderForTable, Boolean> ctCell = new CheckBoxTableCell<>();
                        ctCell.setSelectedStateCallback(new Callback<Integer, ObservableValue<Boolean>>() {
                            @Override
                            public ObservableValue<Boolean> call(Integer index) {
                                return tableScadenzario.getItems().get(index).tableScadenzarioCompletatoProperty();
                            }
                        });
                       
                        return ctCell;
                    }
                });
          int indeksiElementit = 0;
          
          for (ReminderForTable row : tableScadenzario.getItems()){
              final int indeksiElementitFinal = indeksiElementit; 
              tableScadenzario.getItems().get(indeksiElementit).tableScadenzarioCompletatoProperty().addListener(new ChangeListener<Boolean>() {
                            @Override
                            public void changed(ObservableValue<? extends Boolean> obs, Boolean wasSelected, Boolean isSelected) {
                                
                                try {
                                    handleScadenzarioCompleta(indeksiElementitFinal, wasSelected, isSelected);
                                } catch (ParseException ex) {
                                    Logger.getLogger(MainScreenController.class.getName()).log(Level.SEVERE, null, ex);
                                }
                                System.out.println("ObservableVlalu :  "+obs+" boolean wasSelected: "+wasSelected+" boolean isSelected: "+isSelected);
                            }
                        });     
          
              indeksiElementit++;
          }     
         
    }
    
    
    @FXML
    void handlePrimaNotaExportXls(){
        
        
        FileChooser fileChooser = new FileChooser();
  
        //Set extension filter
        FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter("Excel files (*.xlsx)", "*.xlsx");
        fileChooser.getExtensionFilters().add(extFilter);
              
        //Show save file dialog
        File file = fileChooser.showSaveDialog(anchorPane.getScene().getWindow());//giving main window
              
        if(file != null){
                  //only if the file is not null we'll start building excel file
                  
                    //create new workbook
                    XSSFWorkbook workBook = new XSSFWorkbook();
                    //create new sheet
                    XSSFSheet sheet = workBook.createSheet("Prima Nota");
                    //create row header
                    XSSFRow header = sheet.createRow(0);
                    header.createCell(0).setCellValue("Data");
                    header.createCell(1).setCellValue("Operazione");
                    header.createCell(2).setCellValue("Fuori Cassa");;


                    header.createCell(3).setCellValue("Cassa");
                    sheet.addMergedRegion(new CellRangeAddress(0,0,3,4));
                   // header.createCell(4).setCellValue("Cassa ");
                    header.createCell(5).setCellValue("Banca ");
                    sheet.addMergedRegion(new CellRangeAddress(0,0,5,6));
                   // header.createCell(6).setCellValue("Banca ");
                    header.createCell(7).setCellValue("Saldi ");
                    sheet.addMergedRegion(new CellRangeAddress(0,0,7,8));
                   // header.createCell(8).setCellValue("Saldi ");
                    header = sheet.createRow(1);
                    header.createCell(0).setCellValue("");
                      sheet.addMergedRegion(new CellRangeAddress(0,1,0,0)); //merge data rows
                      sheet.addMergedRegion(new CellRangeAddress(0,1,1,1)); //merge operation rows

                    //header.createCell(1).setCellValue("Operazione");
                    //header.createCell(2).setCellValue("Fuori Cassa");
                    header.createCell(3).setCellValue("Entrate");
                    header.createCell(4).setCellValue("Uscite ");
                    header.createCell(5).setCellValue("Entrate ");
                    header.createCell(6).setCellValue("Uscite ");
                    header.createCell(7).setCellValue("Cassa ");
                    header.createCell(8).setCellValue("Banca ");

                         //getting all records from table.
                    XSSFRow row;
                    for(int i=0;i<tablePrimaNota.getItems().size();i++){
                        row= sheet.createRow(i+2);//first 2 rows header          

                         row.createCell(0).setCellValue((String) tablePrimaNota.getColumns().get(0).getCellData(i));
                         row.createCell(1).setCellValue((String) tablePrimaNota.getColumns().get(1).getCellData(i));
                         row.createCell(2).setCellValue((String) tablePrimaNota.getColumns().get(2).getCellData(i));
                         row.createCell(3).setCellValue((String) tablePrimaNota.getColumns().get(3).getColumns().get(0).getCellData(i));
                         row.createCell(4).setCellValue((String) tablePrimaNota.getColumns().get(3).getColumns().get(1).getCellData(i));
                         row.createCell(5).setCellValue((String) tablePrimaNota.getColumns().get(4).getColumns().get(0).getCellData(i));
                         row.createCell(6).setCellValue((String) tablePrimaNota.getColumns().get(4).getColumns().get(1).getCellData(i));
                         row.createCell(7).setCellValue((String) tablePrimaNota.getColumns().get(5).getColumns().get(0).getCellData(i));
                         row.createCell(8).setCellValue((String) tablePrimaNota.getColumns().get(5).getColumns().get(1).getCellData(i));


                    }
                    int lastIndex = sheet.getLastRowNum();
                    lastIndex +=  2;
                    XSSFRow totals = sheet.createRow(lastIndex);

                    totals.createCell(1).setCellValue(tabPrimaNotaBottomLabelOperazione.getText());
                    totals.createCell(2).setCellValue(tabPrimaNotaBottomLabelFuoriCassa.getText());
                    totals.createCell(3).setCellValue(tabPrimaNotaBottomLabelCassaEmtrate.getText());
                    totals.createCell(4).setCellValue(tabPrimaNotaBottomLabelCassaUscite.getText());
                    totals.createCell(5).setCellValue(tabPrimaNotaBottomLabelBancaEntrate.getText());
                    totals.createCell(6).setCellValue(tabPrimaNotaBottomLabelBancaUscite.getText());
                    totals.createCell(7).setCellValue(tabPrimaNotaBottomLabelSaldiCassa.getText());
                    totals.createCell(8).setCellValue(tabPrimaNotaBottomLabelSaldiBanca.getText());
                    lastIndex++;
                    totals = sheet.createRow(lastIndex);
                    totals.createCell(7).setCellValue(labelPrimaNotaSaldoTotale.getText());
                    totals.createCell(8).setCellValue(labelPrimaNotaSaldoTotaleValue.getText());

                    int noOfColumns = sheet.getRow(0).getLastCellNum();
                    for(int z = 0; z < noOfColumns; z++){
                     sheet.autoSizeColumn(z);


                    }
                    //merge fuori cassa column after resizing
                    sheet.addMergedRegion(new CellRangeAddress(0,1,2,2)); //merge fuori cassa rows
                    FileOutputStream fileOut;

                    try {
                        fileOut = new FileOutputStream(file);
                        workBook.write(fileOut);
                        fileOut.close();
                    } catch (FileNotFoundException  ex) {
                        Logger.getLogger(MainScreenController.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    catch (IOException  e) {
                        Logger.getLogger(MainScreenController.class.getName()).log(Level.SEVERE, null, e);
                    }
        
              }
        
   
        
    }
    
     @FXML
    void handlePrimaNotaInviaConsulente(){
        //method will handle exporting prima nota in excel and sending by email through email setted by user in dialog 
        
        try {

            FXMLLoader loader = new FXMLLoader(getClass().getResource("NewEmail.fxml"));
            Parent root2 = (Parent) loader.load();

            // Create the dialog Stage.
            Stage dialogStage = new Stage();
            dialogStage.setTitle("PrimaNota");
            dialogStage.initModality(Modality.APPLICATION_MODAL);
            dialogStage.initOwner(buttonPrimaNotaSendByEmail.getScene().getWindow());
            Scene scene = new Scene(root2);
            dialogStage.setScene(scene);
            
            NewEmail controller = loader.getController();
            controller.setDialogStage(dialogStage);
                     

            // Show the dialog and wait until the user closes it
            dialogStage.showAndWait();
            
            //get email from dialog if it's set from user
            EmailConsulente email = controller.returnEmailConsulente();
            
            if (email.getRecipientEmail()!= null ){
                //export xls and send by email
                //create new workbook
                    XSSFWorkbook workBook = new XSSFWorkbook();
                    //create new sheet
                    XSSFSheet sheet = workBook.createSheet("Prima Nota");
                    //create row header
                    XSSFRow header = sheet.createRow(0);
                    header.createCell(0).setCellValue("Data");
                    header.createCell(1).setCellValue("Operazione");
                    header.createCell(2).setCellValue("Fuori Cassa");;


                    header.createCell(3).setCellValue("Cassa");
                    sheet.addMergedRegion(new CellRangeAddress(0,0,3,4));
                   // header.createCell(4).setCellValue("Cassa ");
                    header.createCell(5).setCellValue("Banca ");
                    sheet.addMergedRegion(new CellRangeAddress(0,0,5,6));
                   // header.createCell(6).setCellValue("Banca ");
                    header.createCell(7).setCellValue("Saldi ");
                    sheet.addMergedRegion(new CellRangeAddress(0,0,7,8));
                   // header.createCell(8).setCellValue("Saldi ");
                    header = sheet.createRow(1);
                    header.createCell(0).setCellValue("");
                      sheet.addMergedRegion(new CellRangeAddress(0,1,0,0)); //merge data rows
                      sheet.addMergedRegion(new CellRangeAddress(0,1,1,1)); //merge operation rows

                    //header.createCell(1).setCellValue("Operazione");
                    //header.createCell(2).setCellValue("Fuori Cassa");
                    header.createCell(3).setCellValue("Entrate");
                    header.createCell(4).setCellValue("Uscite ");
                    header.createCell(5).setCellValue("Entrate ");
                    header.createCell(6).setCellValue("Uscite ");
                    header.createCell(7).setCellValue("Cassa ");
                    header.createCell(8).setCellValue("Banca ");

                         //getting all records from table.
                    XSSFRow row;
                    for(int i=0;i<tablePrimaNota.getItems().size();i++){
                        row= sheet.createRow(i+2);//first 2 rows header          

                         row.createCell(0).setCellValue((String) tablePrimaNota.getColumns().get(0).getCellData(i));
                         row.createCell(1).setCellValue((String) tablePrimaNota.getColumns().get(1).getCellData(i));
                         row.createCell(2).setCellValue((String) tablePrimaNota.getColumns().get(2).getCellData(i));
                         row.createCell(3).setCellValue((String) tablePrimaNota.getColumns().get(3).getColumns().get(0).getCellData(i));
                         row.createCell(4).setCellValue((String) tablePrimaNota.getColumns().get(3).getColumns().get(1).getCellData(i));
                         row.createCell(5).setCellValue((String) tablePrimaNota.getColumns().get(4).getColumns().get(0).getCellData(i));
                         row.createCell(6).setCellValue((String) tablePrimaNota.getColumns().get(4).getColumns().get(1).getCellData(i));
                         row.createCell(7).setCellValue((String) tablePrimaNota.getColumns().get(5).getColumns().get(0).getCellData(i));
                         row.createCell(8).setCellValue((String) tablePrimaNota.getColumns().get(5).getColumns().get(1).getCellData(i));


                    }
                    int lastIndex = sheet.getLastRowNum();
                    lastIndex +=  2;
                    XSSFRow totals = sheet.createRow(lastIndex);

                    totals.createCell(1).setCellValue(tabPrimaNotaBottomLabelOperazione.getText());
                    totals.createCell(2).setCellValue(tabPrimaNotaBottomLabelFuoriCassa.getText());
                    totals.createCell(3).setCellValue(tabPrimaNotaBottomLabelCassaEmtrate.getText());
                    totals.createCell(4).setCellValue(tabPrimaNotaBottomLabelCassaUscite.getText());
                    totals.createCell(5).setCellValue(tabPrimaNotaBottomLabelBancaEntrate.getText());
                    totals.createCell(6).setCellValue(tabPrimaNotaBottomLabelBancaUscite.getText());
                    totals.createCell(7).setCellValue(tabPrimaNotaBottomLabelSaldiCassa.getText());
                    totals.createCell(8).setCellValue(tabPrimaNotaBottomLabelSaldiBanca.getText());
                    lastIndex++;
                    totals = sheet.createRow(lastIndex);
                    totals.createCell(7).setCellValue(labelPrimaNotaSaldoTotale.getText());
                    totals.createCell(8).setCellValue(labelPrimaNotaSaldoTotaleValue.getText());

                    int noOfColumns = sheet.getRow(0).getLastCellNum();
                    for(int z = 0; z < noOfColumns; z++){
                     sheet.autoSizeColumn(z);


                    }
                    //merge fuori cassa column after resizing
                    sheet.addMergedRegion(new CellRangeAddress(0,1,2,2)); //merge fuori cassa rows


                    FileOutputStream fileOut;

                    try {
                        fileOut = new FileOutputStream("PrimaNotaExport.xlsx");
                        workBook.write(fileOut);
                        fileOut.close();
                        //try sending by email
                        try {
                               
                                String[] attachFiles = new String[1];
                                attachFiles[0] = "PrimaNotaExport.xlsx";
                                email.setAttachedFiles(attachFiles);
                                email.setEnableTls("false");
                                email.setRequireAuthentication("true");
                                email.setMailSubject("Prima Nota Esporto");
                                email.setMailBody("Il file allegato e stato esportato in Excel da Prima Nota");
                                email.setSmtpPort("25");
                                NewEmail newEmail = new NewEmail();
                                newEmail.sendEmailWithAttachments(email.getSmtpServer(), email.getSmtpPort(), email.getSenderEmail(), email.getSenderPassword(), email.getRecipientEmail(),
                                        email.getMailSubject(),email.getMailBody(), email.getAttachedFiles(),email.getRequireAuthentication(),email.getEnableTls());
                                Alert alert = new Alert(Alert.AlertType.INFORMATION);
                                    alert.initOwner(dialogStage);
                                    alert.setTitle("Conferma");
                                    alert.setHeaderText("Successo!");
                                    Text text = new Text("Email inviata correttamente");
                                    text.setWrappingWidth(400);
                                    alert.getDialogPane().setContent(text);
                                    alert.showAndWait();
                                //after sending delete file
                                File f = new File("PrimaNotaExport.xlsx");
                                f.delete();
                            } catch (MessagingException ex) {
                                    Alert alert = new Alert(Alert.AlertType.ERROR);
                                    alert.initOwner(dialogStage);
                                    alert.setTitle("Errore!");
                                    alert.setHeaderText("Errore!");
                                    Text text = new Text("Errore nel tentativo di inviare mail! \n Si prega di provare di nuovo \n Se il problema persiste si prega di contattare l'Amministratore dell sistema \n"+ex.getMessage());
                                    text.setWrappingWidth(400);
                                    alert.getDialogPane().setContent(text);
                                    alert.showAndWait();
                               
                                System.out.println("Su dergua");
                                System.out.println(ex.getMessage());
                            }
                        
                    } catch (FileNotFoundException  ex) {
                        Logger.getLogger(MainScreenController.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    catch (IOException  e) {
                        Logger.getLogger(MainScreenController.class.getName()).log(Level.SEVERE, null, e);
                    }
            }
            
        } catch (IOException e) {
            System.out.println(e.getMessage());
            
        }
   
    }
    
    @FXML
    void handlePrimaNotaStampa() throws FileNotFoundException, ParseException {
     
        try {
                InputStream inputStream = new FileInputStream("PrimaNotaReport.jrxml");
                JasperDesign jasperDesign = JRXmlLoader.load(inputStream);
                JasperReport jasperReport = JasperCompileManager.compileReport(jasperDesign);

                HashMap parameters = new HashMap();
                String period ="";
                if (datePickerPrimaNotaFrom.getValue() != null){
                    period = datePickerPrimaNotaFrom.getValue().toString();
                }
                if (datePickerPrimaNotaTo.getValue()!=null){
                    period += " - "+ datePickerPrimaNotaTo.getValue().toString();
                }
                if (!"".equals(period)){
                    parameters.put("periodFilter",period );
                }
                
                String allFilters ="";
                RadioButton selected = (RadioButton) primaNotaRadioFilterGroup.getSelectedToggle();
                allFilters += selected.getText();
                if(comboPrimaNotaBanca.getSelectionModel().getSelectedItem()!= null){
                    allFilters += ": "+comboPrimaNotaBanca.getSelectionModel().getSelectedItem();
                }
                
                if(comboPrimaNotaControparte.getSelectionModel().getSelectedItem() != null){
                    allFilters += " - Controparte: "+comboPrimaNotaControparte.getSelectionModel().getSelectedItem();
                }
                if(comboPrimaNotaCategoria.getSelectionModel().getSelectedItem() != null){
                    allFilters += " - Categoria: "+comboPrimaNotaCategoria.getSelectionModel().getSelectedItem();
                }
                
                 if( !txtPrimaNotaDescrizione.getText().isEmpty()){
                     allFilters +=" - Descrizione: "+txtPrimaNotaDescrizione.getText();
                 }
                 
                  parameters.put("allFilters",allFilters );
                
                 String numeroMovimenti =  tabPrimaNotaBottomLabelOperazione.getText().split(":")[1].replace(" ", "");
                 String fuoriCassaTotal = tabPrimaNotaBottomLabelFuoriCassa.getText();
                 String cassaEntrateTotal = tabPrimaNotaBottomLabelCassaEmtrate.getText();
                 String cassaUsciteTotal = tabPrimaNotaBottomLabelCassaUscite.getText();
                 String bancaEntrateTotal = tabPrimaNotaBottomLabelBancaEntrate.getText();
                 String bancaUsciteTotal = tabPrimaNotaBottomLabelBancaUscite.getText();
                 String saldiCassaTotal = tabPrimaNotaBottomLabelSaldiCassa.getText();
                 String saldiBancaTotal = tabPrimaNotaBottomLabelSaldiBanca.getText();
                 String saldoTotale = labelPrimaNotaSaldoTotaleValue.getText();
                 
                 parameters.put("numeroMovimenti",numeroMovimenti);
                 parameters.put("fuoriCassaTotal",fuoriCassaTotal);
                 parameters.put("entrateCassaTotal",cassaEntrateTotal);
                 parameters.put("usciteCassaTotal",cassaUsciteTotal);
                 parameters.put("entrateBancaTotal",bancaEntrateTotal);
                 parameters.put("usciteBancaTotal",bancaUsciteTotal);
                 parameters.put("saldoCassaTotal",saldiCassaTotal);
                 parameters.put("saldoBancaTotal",saldiBancaTotal);
                 parameters.put("saldoTotale",saldoTotale);
            
                //prepare data
                ObservableList<EntryForTable> tableData = tablePrimaNota.getItems();
                ObservableList<EntryForReport> data = FXCollections.observableArrayList();
                String fuoriCassaPrevious = "";
                String saldiCassaPrevious = "";
                String saldiBancaPrevious = "";
                for(EntryForTable oneRow : tableData ){
                    if (oneRow.getTablePrimaNotaData().equals("")){
                      //first row of previous records
                      fuoriCassaPrevious    =  oneRow.getTablePrimaNotaFuoriCassa();
                      saldiCassaPrevious =   oneRow.getTablePrimaNotaSaldiCassa();
                      saldiBancaPrevious =   oneRow.getTablePrimaNotaSaldiBanca();
                    }else{
                       //rreshtat e tjere
                       //shtojme te dhenat te lista me objekte EnryForReport
                       data.add(new EntryForReport(oneRow.getTablePrimaNotaData(),oneRow.getTablePrimaNotaOperazione(),comboBankPrimaNotaHash.get(Integer.parseInt(oneRow.getTablePrimaNotaBankId())),
                       oneRow.getTablePrimaNotaFuoriCassa(),oneRow.getTablePrimaNotaCassaEntrate(),oneRow.getTablePrimaNotaCassaUscite(),oneRow.getTablePrimaNotaBancaEntrate(),
                       oneRow.getTablePrimaNotaBancaUscite(),oneRow.getTablePrimaNotaSaldiCassa(),oneRow.getTablePrimaNotaSaldiBanca()));
                    }
                }
                
                 parameters.put("fuoriCassaPrevious",fuoriCassaPrevious);
                 parameters.put("saldoCassaPrevious",saldiCassaPrevious);
                 parameters.put("saldoBancaPrevious",saldiBancaPrevious);
                 
                 //hardcoded company details
                 parameters.put("companyName","TEST COMPANY");
                 parameters.put("codiceFiscale","1234567891012345");
                 parameters.put("partitaIva","12345678901");
                 parameters.put("companyAddress","Street Name, City Name (Italy) - Box Number Ex. 0000");
                
                
                JRBeanCollectionDataSource beanColDataSource = new JRBeanCollectionDataSource(data);
                JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, beanColDataSource);
                
                
                JasperViewer.viewReport(jasperPrint,false);
     
               
            } catch (JRException ex) {
                Logger.getLogger(PrimaNotaFacileClient.class.getName()).log(Level.SEVERE, null, ex);
            }
        
        
    
    }
    
    
    
    void handlePrimaNotaFilterCustom(String contact, String description, String fromDate, String endDate, String TuttiRadioButton, String SoloCasaRadioButton, String SoloBancaRadioButton, String category, String selectedBank) throws ParseException {
        String query = "Select *, (incoming_value_bank - outgoing_value_bank) as balance_bank, (incoming_value_cash - outgoing_value_cash) as balance_cash from entry Where 1=1";
        ArchivesPortService archiveService = new ArchivesPortService();
        ArchivesPort archivesPort = archiveService.getArchivesPortSoap11();
//        if (!"".equals(contact)) {
//            query += " and id_contact = '" + contact + "'";
//        }
        if ("Tutti".equals(contact) || "".equals(contact)) {
            if (!"".equals(description)) {
                query += " and description like '%" + description + "%'";
            }
            
            if (!"".equals(fromDate) && !"".equals(endDate)) {
                query += " and entry_date BETWEEN '" + fromDate + "' and '" + endDate + "'";
            }
            if ("".equals(fromDate) || "".equals(endDate)) {
                if (!"".equals(fromDate)) {
                    query += " and entry_date >= '" + fromDate + "'";
                }
                if (!"".equals(endDate)) {
                    query += " and entry_date <= '" + endDate + "'";
                }
            }
        } else {
            if (!"".equals(contact)) {
                query += " and id_contact = '" + contact + "'";
            }
            if (!"".equals(description)) {
                query += " and description like '%" + description + "%'";
            }
            
            if (!"".equals(fromDate) && !"".equals(endDate)) {
                query += " and entry_date BETWEEN '" + fromDate + "' and '" + endDate + "'";
            }
            if ("".equals(fromDate) || "".equals(endDate)) {
                if (!"".equals(fromDate)) {
                    query += " and entry_date >= '" + fromDate + "'";
                }
                if (!"".equals(endDate)) {
                    query += " and entry_date <= '" + endDate + "'";
                }
            }
        }
        
        if (!"true".equals(TuttiRadioButton)) {
            if ("true".equals(SoloBancaRadioButton)) {
                //check which bank
                if("Tutte".equals(selectedBank)){
                    query += " and id_bank != '0'";
                }else{
                    //specific bank
                    query += " and id_bank = '"+selectedBank+"' ";
                }
                
            }
            if ("true".equals(SoloCasaRadioButton)) {
                query += " and id_bank = '0'";
            }
        }
        
        if(!"".equals(category) && !"Generico".equals(category)){
            query += " and id_operation_type = '" + category + "'";
        } 
        
        query += " order by entry_date asc;";
        System.out.println(query);
        GetAllEntriesCustomRequest allEntriesRequest = new GetAllEntriesCustomRequest();
        allEntriesRequest.setArchiveName(PrimaNotaFacileClient.getCurrentArchive() + ".sqlite3");
        allEntriesRequest.setQueryClient(query);
        GetAllEntriesCustomResponse allEntriesResponse = archivesPort.getAllEntriesCustom(allEntriesRequest);
        
        List<EntryBalance> allEntries = allEntriesResponse.getAllEntriesCustomResponse();    
                   
        //Create observable list with class for table. add elements.
        ObservableList<EntryForTable> data = tablePrimaNota.getItems();
        tablePrimaNota.getItems().removeAll(data);
        data.remove(0, data.size());
        
        //first row of table we're gonna add the totals for the previous period
        //method will call webservice method for previous period totals
        ArchivesPortService archiveService2 = new ArchivesPortService();
        ArchivesPort archivesPort2 = archiveService2.getArchivesPortSoap11();
        GetAllEntriesPreviousPeriodRequest allEntriesPreviousPeriodRequest = new GetAllEntriesPreviousPeriodRequest();
        //set the current archive to the search query
        allEntriesPreviousPeriodRequest.setArchiveName(PrimaNotaFacileClient.getCurrentArchive() + ".sqlite3");
        allEntriesPreviousPeriodRequest.setQueryClient(fromDate);//we're setting the from date as end date for calculating previous period totals
        //even though the method is named setQueryClient we put the date because we didn't change archives.xsd to save time generating all methods with array lists in server side :)
        
        GetAllEntriesPreviousPeriodResponse allEntriesPreviousPeriodResponse = archivesPort2.getAllEntriesPreviousPeriod(allEntriesPreviousPeriodRequest);
        EntryPreviousPeriod entryPreviousPeriod = allEntriesPreviousPeriodResponse.getEntryPreviousPeriod();

        //adding firs row with totals from previous period
        String TotalOutCash, TotalBalanceCash, TotalBalanceBank;
        if (entryPreviousPeriod.getTotalOutCash() == null) {
            TotalOutCash = "0";
        } else {
            TotalOutCash = entryPreviousPeriod.getTotalOutCash();
        }
        
        if (entryPreviousPeriod.getTotalBalanceCash() == null) {
            TotalBalanceCash = "0";
        } else {
            TotalBalanceCash = entryPreviousPeriod.getTotalBalanceCash();
        }
        
        if (entryPreviousPeriod.getTotalBalanceBank() == null) {
            TotalBalanceBank = "0";
        } else {
            TotalBalanceBank = entryPreviousPeriod.getTotalBalanceBank();
        }
        
        data.add(new EntryForTable("", "0", "", "Riporti del periodo precedente: ", NumberFormat.getCurrencyInstance(Locale.ITALY).format(Double.parseDouble(TotalOutCash)), "", "", "", "", NumberFormat.getCurrencyInstance(Locale.ITALY).format(Double.parseDouble(TotalBalanceCash)), NumberFormat.getCurrencyInstance(Locale.ITALY).format(Double.parseDouble(TotalBalanceBank)), "","",""));
        
        String Date;
        String viti;
        String muaji;
        String dita;
        String categoryString = null;
        for (EntryBalance ent : allEntries) {
            //check category
            if ("0".equals(ent.getIdOperationType()) ){
              //categoria generico
                categoryString = "Generico";
            }
            else
            {
             //different id for category   
            GetOperationTypeRequest operationTypeRequest = new GetOperationTypeRequest();
            operationTypeRequest.setOperationTypeId(ent.getIdOperationType());
            operationTypeRequest.setArchiveName(PrimaNotaFacileClient.getCurrentArchive() + ".sqlite3");
            GetOperationTypeResponse operationTypeResponse = archivesPort.getOperationType(operationTypeRequest);
            OperationType existentOperationType = operationTypeResponse.getOperationType();
                categoryString = existentOperationType.getName();
            }
            //adding values to table
            if (categoryString != null) {
                Date = ent.getEntryDate();
                viti = Date.substring(0, 4);
                muaji = Date.substring(5, 7);
                dita = Date.substring(8, 10);
                if (data.size() < 2) {
                    //rreshti i pare me vlera
                    data.add(new EntryForTable(ent.getEntryId(), comboContact.get(Integer.parseInt(ent.getIdContact())), 
                            dita + "-" + muaji + "-" + viti, ent.getDescription() + " - " + categoryString, 
                            NumberFormat.getCurrencyInstance(Locale.ITALY).format(Double.parseDouble(ent.getOutCash())), 
                            NumberFormat.getCurrencyInstance(Locale.ITALY).format(Double.parseDouble(ent.getIncomingValueCash())),
                            NumberFormat.getCurrencyInstance(Locale.ITALY).format(Double.parseDouble(ent.getOutgoingValueCash())),
                            NumberFormat.getCurrencyInstance(Locale.ITALY).format(Double.parseDouble(ent.getIncomingValueBank())),
                            NumberFormat.getCurrencyInstance(Locale.ITALY).format(Double.parseDouble(ent.getOutgoingValueBank())), 
                            NumberFormat.getCurrencyInstance(Locale.ITALY).format(Double.parseDouble(ent.getBalanceCash())), 
                            NumberFormat.getCurrencyInstance(Locale.ITALY).format(Double.parseDouble(ent.getBalanceBank())), 
                            ent.getIdBank(),ent.getIdContact(),ent.getIdOperationType()));
                } else {
                    //rreshtat e tjere
                    //bilanci bank dhe cash do te mblidhet me bilanci bank dhe cash te nje rreshti me siper
                    data.add(new EntryForTable(ent.getEntryId(), comboContact.get(Integer.parseInt(ent.getIdContact())), dita + "-" + muaji + "-" + viti,
                            ent.getDescription() + " - " + categoryString, NumberFormat.getCurrencyInstance(Locale.ITALY).format(Double.parseDouble(ent.getOutCash())),
                           NumberFormat.getCurrencyInstance(Locale.ITALY).format(Double.parseDouble(ent.getIncomingValueCash())), NumberFormat.getCurrencyInstance(Locale.ITALY).format(Double.parseDouble(ent.getOutgoingValueCash())),
                           NumberFormat.getCurrencyInstance(Locale.ITALY).format(Double.parseDouble(ent.getIncomingValueBank())), NumberFormat.getCurrencyInstance(Locale.ITALY).format(Double.parseDouble(ent.getOutgoingValueBank())), 
                       
                           NumberFormat.getCurrencyInstance(Locale.ITALY).format(Double.parseDouble(ent.getBalanceCash()) + NumberFormat.getCurrencyInstance(Locale.ITALY).parse(data.get(data.size() - 1).getTablePrimaNotaSaldiCassa()).doubleValue()),
                           NumberFormat.getCurrencyInstance(Locale.ITALY).format(Double.parseDouble(ent.getBalanceBank()) + NumberFormat.getCurrencyInstance(Locale.ITALY).parse(data.get(data.size() - 1).getTablePrimaNotaSaldiBanca()).doubleValue()), ent.getIdBank(), ent.getIdContact(),ent.getIdOperationType()));
                    
                }
            }
            
            
            
        }
        //set the value to the bottom row of labels
        Double TotalCashOut = 0.0;
        Double TotalIncomingValueCash = 0.0;
        Double TotalOutgoingValueCash = 0.0;
        Double TotalIncomingValueBank = 0.0;
        Double TotalOutgoingValueBank = 0.0;
        Double TotalBalanceCashInt = 0.0;
        Double TotalBalanceBankInt = 0.0;
        Double index = 0.0;
        for (EntryForTable onerow : data) {
            index++;
            if (index > 1) {
                TotalCashOut += NumberFormat.getCurrencyInstance(Locale.ITALY).parse(onerow.getTablePrimaNotaFuoriCassa()).doubleValue();
                TotalIncomingValueCash += NumberFormat.getCurrencyInstance(Locale.ITALY).parse(onerow.getTablePrimaNotaCassaEntrate()).doubleValue();   
                TotalOutgoingValueCash +=  NumberFormat.getCurrencyInstance(Locale.ITALY).parse(onerow.getTablePrimaNotaCassaUscite()).doubleValue();
                TotalIncomingValueBank +=  NumberFormat.getCurrencyInstance(Locale.ITALY).parse(onerow.getTablePrimaNotaBancaEntrate()).doubleValue();
                TotalOutgoingValueBank +=  NumberFormat.getCurrencyInstance(Locale.ITALY).parse(onerow.getTablePrimaNotaBancaUscite()).doubleValue();
                TotalBalanceCashInt =  NumberFormat.getCurrencyInstance(Locale.ITALY).parse(onerow.getTablePrimaNotaSaldiCassa()).doubleValue();
                TotalBalanceBankInt =  NumberFormat.getCurrencyInstance(Locale.ITALY).parse(onerow.getTablePrimaNotaSaldiBanca()).doubleValue();
                
                
            }
        }
        
       // data.add(new EntryForTable("", "", "", "Numero movimenti: " + String.valueOf(data.size()), String.valueOf(TotalCashOut), String.valueOf(TotalIncomingValueCash), String.valueOf(TotalOutgoingValueCash), String.valueOf(TotalIncomingValueBank), String.valueOf(TotalOutgoingValueBank), String.valueOf(TotalBalanceCashInt), String.valueOf(TotalBalanceBankInt), ""));
        tabPrimaNotaBottomLabelData.setText("");//empty for label date just added there for positioning purposes
        tabPrimaNotaBottomLabelOperazione.setText("Numero movimenti: " + String.valueOf(data.size()-1));//first row previous period doesn't count
        tabPrimaNotaBottomLabelFuoriCassa.setText(NumberFormat.getCurrencyInstance(Locale.ITALY).format(TotalCashOut));
        tabPrimaNotaBottomLabelCassaEmtrate.setText(NumberFormat.getCurrencyInstance(Locale.ITALY).format(TotalIncomingValueCash));
        tabPrimaNotaBottomLabelCassaUscite.setText(NumberFormat.getCurrencyInstance(Locale.ITALY).format(TotalOutgoingValueCash));
        tabPrimaNotaBottomLabelBancaEntrate.setText(NumberFormat.getCurrencyInstance(Locale.ITALY).format(TotalIncomingValueBank));
        tabPrimaNotaBottomLabelBancaUscite.setText(NumberFormat.getCurrencyInstance(Locale.ITALY).format(TotalOutgoingValueBank));
        tabPrimaNotaBottomLabelSaldiBanca.setText(NumberFormat.getCurrencyInstance(Locale.ITALY).format(TotalBalanceBankInt));
        tabPrimaNotaBottomLabelSaldiCassa.setText(NumberFormat.getCurrencyInstance(Locale.ITALY).format(TotalBalanceCashInt));
        
        
        
        labelPrimaNotaSaldoTotaleValue.setText(NumberFormat.getCurrencyInstance(Locale.ITALY).format(TotalBalanceBankInt + TotalBalanceCashInt));
        
         //trying word wrap in tableprima nota operazione and all others           
        tablePrimaNotaOperazione.setCellFactory(new Callback<TableColumn<EntryForTable, String>, TableCell<EntryForTable, String>>() {
    @Override
    public TableCell<EntryForTable, String> call(TableColumn<EntryForTable, String> operazioneTableColumn) {
        return new TableCell<EntryForTable, String>() {
            private Text text = new Text();

            {
               text.wrappingWidthProperty().bind(tablePrimaNotaOperazione.widthProperty());
               
            }

            @Override
            public void updateItem(String item, boolean empty) {
                super.updateItem(item, empty);

                if (empty || item == null) {
                    setGraphic(null);
                } else {
                    text.setText(item);
                    setGraphic(text);
                    TableRow<EntryForTable> currentRow = getTableRow();
                            if("Riporti del periodo precedente: ".equals(item)){
                                 currentRow.setStyle("-fx-background-color:#EEEEEE;");
                                text.setFill(Color.RED);
                 
                                }
                }
            }
            
        };
    }
});
            
        //formating text style of Prima nota table columns with currency values
        tablePrimaNotaFuoriCassa.setCellFactory(new Callback<TableColumn<EntryForTable, String>, TableCell<EntryForTable, String>>() {
        @Override
            public TableCell<EntryForTable, String> call(TableColumn<EntryForTable, String> param) {
                TableCell<EntryForTable, String> cell = new TableCell<EntryForTable, String>() {
                 private Text text = new Text();

                {
                 text.wrappingWidthProperty().bind(tablePrimaNotaFuoriCassa.widthProperty());
                    
               
                }          
                    @Override
                    public void updateItem(final String item, boolean empty) {
                       super.updateItem(item, empty);

                        if (empty || item == null) {
                            setGraphic(null);
                        } else {
                            text.setText(item);
                            setGraphic(text);
                            text.setStyle("-fx-font-size: 8pt;");
                            text.setTextAlignment(TextAlignment.RIGHT);


                        }                    
                    }
                };
                return cell;
            }
        });
         tablePrimaNotaCassaEntrate.setCellFactory(new Callback<TableColumn<EntryForTable, String>, TableCell<EntryForTable, String>>() {
        @Override
            public TableCell<EntryForTable, String> call(TableColumn<EntryForTable, String> param) {
                TableCell<EntryForTable, String> cell = new TableCell<EntryForTable, String>() {
                     private Text text = new Text();

                {
                 text.wrappingWidthProperty().bind(tablePrimaNotaCassaEntrate.widthProperty());
               
                } 
                    @Override
                    public void updateItem(final String item, boolean empty) {
                        super.updateItem(item, empty);

                        if (empty || item == null) {
                            setGraphic(null);
                        } else {
                            text.setText(item);
                            setGraphic(text);
                            text.setTextAlignment(TextAlignment.RIGHT);
                            Double value = 0.0;
                            if(!"".equals(item)){
                                try {
                                value = NumberFormat.getCurrencyInstance(Locale.ITALY).parse(item).doubleValue();
                            } catch (ParseException ex) {
                                Logger.getLogger(MainScreenController.class.getName()).log(Level.SEVERE, null, ex);
                            }
                            }
                            
                            if(value!=0.0){                                                         
                                 text.setStyle("-fx-font-size: 8pt;");
                                 text.setFill(Color.valueOf("#66BB6A"));
                            }else{
                                text.setStyle("-fx-font-size: 8pt;");
                            }

                        }       

                    }
                };
                return cell;
            }
        });
        tablePrimaNotaCassaUscite.setCellFactory(new Callback<TableColumn<EntryForTable, String>, TableCell<EntryForTable, String>>() {
        @Override
            public TableCell<EntryForTable, String> call(TableColumn<EntryForTable, String> param) {
                TableCell<EntryForTable, String> cell = new TableCell<EntryForTable, String>() {
                         private Text text = new Text();

                {
                 text.wrappingWidthProperty().bind(tablePrimaNotaCassaUscite.widthProperty());
               
                } 
                    @Override
                    public void updateItem(final String item, boolean empty) {
                        super.updateItem(item, empty);

                        if (empty || item == null) {
                            setGraphic(null);
                        } else {
                            text.setText(item);
                            setGraphic(text);
                            text.setTextAlignment(TextAlignment.RIGHT);
                            Double value = 0.0;
                             if(!"".equals(item)){
                                try {
                                value = NumberFormat.getCurrencyInstance(Locale.ITALY).parse(item).doubleValue();
                            } catch (ParseException ex) {
                                Logger.getLogger(MainScreenController.class.getName()).log(Level.SEVERE, null, ex);
                            }
                            }
                            if(value!=0.0){                                                         
                                 text.setStyle("-fx-font-size: 8pt;");
                                 text.setFill(Color.valueOf("#EF5350"));
                            }else{
                                text.setStyle("-fx-font-size: 8pt;");
                            }

                        }       
                    }
                };
                return cell;
            }
        });
        tablePrimaNotaBancaEntrate.setCellFactory(new Callback<TableColumn<EntryForTable, String>, TableCell<EntryForTable, String>>() {
        @Override
            public TableCell<EntryForTable, String> call(TableColumn<EntryForTable, String> param) {
                TableCell<EntryForTable, String> cell = new TableCell<EntryForTable, String>() {
                          private Text text = new Text();

                {
                 text.wrappingWidthProperty().bind(tablePrimaNotaBancaEntrate.widthProperty());
               
                } 
                    @Override
                    public void updateItem(final String item, boolean empty) {
                        super.updateItem(item, empty);

                        if (empty || item == null) {
                            setGraphic(null);
                        } else {
                            text.setText(item);
                            setGraphic(text);
                            text.setTextAlignment(TextAlignment.RIGHT);
                            Double value = 0.0;
                             if(!"".equals(item)){
                                try {
                                value = NumberFormat.getCurrencyInstance(Locale.ITALY).parse(item).doubleValue();
                            } catch (ParseException ex) {
                                Logger.getLogger(MainScreenController.class.getName()).log(Level.SEVERE, null, ex);
                            }
                            }
                            if(value!=0.0){                                                         
                                 text.setStyle("-fx-font-size: 8pt;");
                                 text.setFill(Color.valueOf("#66BB6A"));
                            }else{
                                text.setStyle("-fx-font-size: 8pt;");
                            }

                        } 
                    }
                };
                return cell;
            }
        });
        tablePrimaNotaBancaUscite.setCellFactory(new Callback<TableColumn<EntryForTable, String>, TableCell<EntryForTable, String>>() {
        @Override
            public TableCell<EntryForTable, String> call(TableColumn<EntryForTable, String> param) {
                TableCell<EntryForTable, String> cell = new TableCell<EntryForTable, String>() {
                             private Text text = new Text();

                {
                 text.wrappingWidthProperty().bind(tablePrimaNotaBancaUscite.widthProperty());
               
                } 
                    @Override
                    public void updateItem(final String item, boolean empty) {
                         super.updateItem(item, empty);

                        if (empty || item == null) {
                            setGraphic(null);
                        } else {
                            text.setText(item);
                            setGraphic(text);
                            text.setTextAlignment(TextAlignment.RIGHT);
                            Double value = 0.0;
                            if(!"".equals(item)){
                                try {
                                value = NumberFormat.getCurrencyInstance(Locale.ITALY).parse(item).doubleValue();
                            } catch (ParseException ex) {
                                Logger.getLogger(MainScreenController.class.getName()).log(Level.SEVERE, null, ex);
                            }
                            }
                            if(value!=0.0){                                                         
                                 text.setStyle("-fx-font-size: 8pt;");
                                 text.setFill(Color.valueOf("#EF5350"));
                            }else{
                                text.setStyle("-fx-font-size: 8pt;");
                            }

                        }
                    }
                };
                return cell;
            }
        });
        tablePrimaNotaSaldiCassa.setCellFactory(new Callback<TableColumn<EntryForTable, String>, TableCell<EntryForTable, String>>() {
        @Override
            public TableCell<EntryForTable, String> call(TableColumn<EntryForTable, String> param) {
                TableCell<EntryForTable, String> cell = new TableCell<EntryForTable, String>() {
                                 private Text text = new Text();

                {
                 text.wrappingWidthProperty().bind(tablePrimaNotaSaldiCassa.widthProperty());
               
                } 
                    @Override
                    public void updateItem(final String item, boolean empty) {
                         super.updateItem(item, empty);

                        if (empty || item == null) {
                            setGraphic(null);
                        } else {
                            text.setText(item);
                            setGraphic(text);
                            text.setTextAlignment(TextAlignment.RIGHT);
                            Double value = 0.0;
                             if(!"".equals(item)){
                                try {
                                value = NumberFormat.getCurrencyInstance(Locale.ITALY).parse(item).doubleValue();
                            } catch (ParseException ex) {
                                Logger.getLogger(MainScreenController.class.getName()).log(Level.SEVERE, null, ex);
                            }
                            }
                            if(value >0.0){
                                setStyle("-fx-font-size: 8pt;");
                                 text.setFill(Color.valueOf("#66BB6A"));
                            }else if(value <0.0)
                            {
                             text.setStyle("-fx-font-size: 8pt;");
                                 text.setFill(Color.valueOf("#EF5350"));
                            } 
                            else{
                            setStyle("-fx-font-size: 8pt;");
                            }
                           
                        }

                    }
                };
                return cell;
            }
        });
          tablePrimaNotaSaldiBanca.setCellFactory(new Callback<TableColumn<EntryForTable, String>, TableCell<EntryForTable, String>>() {
        @Override
            public TableCell<EntryForTable, String> call(TableColumn<EntryForTable, String> param) {
                TableCell<EntryForTable, String> cell = new TableCell<EntryForTable, String>() {
                private Text text = new Text();

                {
                 text.wrappingWidthProperty().bind(tablePrimaNotaSaldiBanca.widthProperty());
               
                } 
                    @Override
                    public void updateItem(final String item, boolean empty) {
                       super.updateItem(item, empty);

                        if (empty || item == null) {
                            setGraphic(null);
                        } else {
                            text.setText(item);
                            setGraphic(text);
                            text.setTextAlignment(TextAlignment.RIGHT);
                            Double value = 0.0;
                             if(!"".equals(item)){
                                try {
                                value = NumberFormat.getCurrencyInstance(Locale.ITALY).parse(item).doubleValue();
                            } catch (ParseException ex) {
                                Logger.getLogger(MainScreenController.class.getName()).log(Level.SEVERE, null, ex);
                            }
                            }
                            if(value >0.0){
                                setStyle("-fx-font-size: 8pt;");
                                 text.setFill(Color.valueOf("#66BB6A"));
                            }else if(value <0.0)
                            {
                             text.setStyle("-fx-font-size: 8pt;");
                                 text.setFill(Color.valueOf("#EF5350"));
                            } 
                            else{
                            setStyle("-fx-font-size: 8pt;");
                            }
                           
                        }
                    }
                };
                return cell;
            }
        });
              
        
        
    }
    
    @FXML
    public void getQueryStrings() throws ParseException {
        String contact = "";
        String description = "";
        String fromDate = "";
        String endDate = "";
        String soloCasaRadioButton = "";
        String tuttiRadioButton = "";
        String soloBancaRadioButton = "";
        String selectedBank = "";
        String category = "";
        
        if (!comboPrimaNotaControparte.getSelectionModel().isEmpty()) {
            if (comboPrimaNotaControparte.getSelectionModel().getSelectedItem().equals("Tutti")) {
                //call the default method
                contact = "Tutti";
            } else {
                //call the method with filtering
                int contactId = 0;
                for (Map.Entry<Integer, String> entry : comboContact.entrySet()) {
                    //since combo box select index starts from 0 we're gonna increment n after checking for specified value in hashmap
                    //in the main controller we display in table the full name of the contact so we're searching in this hashmap for key
                    if (entry.getValue().equals(comboPrimaNotaControparte.getSelectionModel().getSelectedItem())) {
                        contactId = entry.getKey();
                        contact = String.valueOf(contactId);
                        break;
                    }
                }
            }
            
        } else {
            contact = "";
        }
        
        
         if (!comboPrimaNotaCategoria.getSelectionModel().isEmpty()) {
            if (comboPrimaNotaCategoria.getSelectionModel().getSelectedItem().equals("Tutti")) {
                //call the default method
                category = "Tutti";
            } else {
                //call the method with filtering
                int categoryId = 0;
                for (Map.Entry<Integer, String> entry : comboCategoryHash.entrySet()) {
                    //since combo box select index starts from 0 we're gonna increment n after checking for specified value in hashmap
                    //in the main controller we display in table the full name of the contact so we're searching in this hashmap for key
                    if (entry.getValue().equals(comboPrimaNotaCategoria.getSelectionModel().getSelectedItem())) {
                        categoryId = entry.getKey();
                        category = String.valueOf(categoryId);
                        break;
                    }
                }
            }
            
        } else {
            category = "";
        }
        
        if (txtPrimaNotaDescrizione.getText() != null) {
            description = txtPrimaNotaDescrizione.getText();
        } else {
            description = "";
        }
        if (datePickerPrimaNotaFrom.getValue() != null) {
            fromDate = datePickerPrimaNotaFrom.getValue().toString() + " 00:00:00";
        } else {
            fromDate = "";
        }
        if (datePickerPrimaNotaTo.getValue() != null) {
            endDate = datePickerPrimaNotaTo.getValue().toString() + " 00:00:00";
        } else {
            endDate = "";
        }
        
        if (radioPrimaNotaFilterSoloBanca.isSelected()) {
            soloBancaRadioButton = "true";
            //check which bank is selected
            if(comboPrimaNotaBanca.getSelectionModel().getSelectedItem().equals("Tutte")){
                //any bank
                selectedBank = "Tutte";
            }else{
                 int bankId = 0;
                 for (Map.Entry<Integer, String> entry : comboBankPrimaNotaHash.entrySet()) {
                     //since combo box select index starts from 0 we're gonna increment n after checking for specified value in hashmap
                     //in the main controller we display in table the full name of the contact so we're searching in this hashmap for key
                     if (entry.getValue().equals(comboPrimaNotaBanca.getSelectionModel().getSelectedItem())) {
                         bankId = entry.getKey();
                         selectedBank = String.valueOf(bankId);
                         break;
                     }
                 }
            }
            
        } else {
            soloBancaRadioButton = "";
        }
        if (radioPrimaNotaFilterSoloCassa.isSelected()) {
            soloCasaRadioButton = "true";
        } else {
            soloCasaRadioButton = "";
        }
        if (radioPrimaNotaFilterTutto.isSelected()) {
            tuttiRadioButton = "true";
            soloCasaRadioButton = "";
            soloBancaRadioButton = "";
        } else {
            tuttiRadioButton = "";
        }
        
        handlePrimaNotaFilterCustom(contact, description, fromDate, endDate, tuttiRadioButton, soloCasaRadioButton, soloBancaRadioButton, category, selectedBank);
    }
    
    @FXML
    void handleVisualizzaDettagli() throws ParseException {
        getQueryStrings();
    }
    
    @FXML
    void handleSpinnerMese(){
        //on changing months it will be changed also the start date and end date
        LocalDate date = LocalDate.now() ;
        int year = spinnerPrimaNotaAnno.getValue();
        
        switch (spinnerPrimaNotaMese.getValue()) {
            case "GENNAIO":  date = LocalDate.of(year, 1,1 );
            
                     break;
            case "FEBBRAIO":  date = LocalDate.of(year, 2, 1);
                     break;
            case "MARZO": date = LocalDate.of(year, 3, 1);
                     break;
            case "APRILE":  date = LocalDate.of(year, 4, 1);
                     break;
            case "MAGGIO":  date = LocalDate.of(year, 5, 1);
                     break;
            case "GIUNGO":  date = LocalDate.of(year, 6, 1);
                     break;
            case "LUGLIO":  date = LocalDate.of(year, 7, 1);
                     break;
            case "AGOSTO":  date = LocalDate.of(year, 8, 1);
                     break;
            case "SETTEMBRE":  date = LocalDate.of(year, 9, 1);
                     break;
            case "OTTOBRE": date = LocalDate.of(year, 10, 1);
                     break;
            case "NOVEMBRE": date = LocalDate.of(year, 11, 1);
                     break;
            case "DICEMBRE": date = LocalDate.of(year, 12, 1);
                     break;
            
        }
        
       
        LocalDate end = date.withDayOfMonth(date.lengthOfMonth());
        String Dita;
        String Muaji;
        String Viti;
        String DataPrompt;
        datePickerPrimaNotaFrom.setValue(date);
        Dita = String.valueOf(date.getDayOfMonth());
        Muaji = String.valueOf(date.getMonthValue());
        Viti = String.valueOf(date.getYear());
        DataPrompt = Dita + "-" + Muaji + "-" + Viti;
        datePickerPrimaNotaFrom.setPromptText(DataPrompt);
        
        datePickerPrimaNotaTo.setValue(end);
        Dita = String.valueOf(end.getDayOfMonth());
        Muaji = String.valueOf(end.getMonthValue());
        Viti = String.valueOf(end.getYear());
        DataPrompt = Dita + "-" + Muaji + "-" + Viti;
        datePickerPrimaNotaTo.setPromptText(DataPrompt);
        //set values for spinner
       
    }
    
    @FXML
    void handleSpinnerAnno(){
        LocalDate start = datePickerPrimaNotaFrom.getValue();
        LocalDate end = datePickerPrimaNotaTo.getValue();
        String Dita;
        String Muaji;
        String Viti;
        String DataPrompt;
        start =start.plusYears((spinnerPrimaNotaAnno.getValue()-start.getYear()));//change year. add difference
        datePickerPrimaNotaFrom.setValue(start);
        Dita = String.valueOf(start.getDayOfMonth());
        Muaji = String.valueOf(start.getMonthValue());
        Viti = String.valueOf(start.getYear());
        DataPrompt = Dita + "-" + Muaji + "-" + Viti;
        datePickerPrimaNotaFrom.setPromptText(DataPrompt);
        end = end.plusYears((spinnerPrimaNotaAnno.getValue()-end.getYear()));
        datePickerPrimaNotaTo.setValue(end);
        Dita = String.valueOf(end.getDayOfMonth());
        Muaji = String.valueOf(end.getMonthValue());
        Viti = String.valueOf(end.getYear());
        DataPrompt = Dita + "-" + Muaji + "-" + Viti;
        datePickerPrimaNotaTo.setPromptText(DataPrompt);
            
    }
    
    @FXML
    void handleFromDateEnter() {
//        getQueryStrings();
    }
    
    @FXML
    void handleToDateEnter() {
//        getQueryStrings();
    }
    
    @FXML
    void handleDescriptionEnter() {
//        getQueryStrings();
    }
    
    @FXML
    void hanldeComboPrimaNota() {
//        getQueryStrings();
    }
    
    @FXML
    void handlePrimaNotaTabInit() {
        //method will be called during window initialization and on function handleScadenzario
        //this method will call WSDL method getAllReminders
        //Since JavaFX needs a seperate data model for tables with string properties, we're gonna create a separate class for each class we get from WS
        //For Scadenzario (Reminder) we created a class with string properties ReminderForTable and we're gonna use that to display data in table
        //The table view and table columns are linked in FXML with the new Class ReminderForTable

        //Get Reminders from WS with Reminder Class      
        //set class to search from soap server
        comboPrimaNotaControparte.getSelectionModel().clearSelection();
        datePickerPrimaNotaFrom.getEditor().clear();
        datePickerPrimaNotaFrom.setValue(null);
        datePickerPrimaNotaTo.getEditor().clear();
        datePickerPrimaNotaTo.setValue(null);
        txtPrimaNotaDescrizione.clear();

        //we're changing logic of prima nota initialization
        //we're gonna call a custom entries from table
        ArchivesPortService archiveService = new ArchivesPortService();
        ArchivesPort archivesPort = archiveService.getArchivesPortSoap11();
        GetAllEntriesCustomRequest allEntriesCustomRequest = new GetAllEntriesCustomRequest();
        //set the current archive to the search query
        allEntriesCustomRequest.setArchiveName(PrimaNotaFacileClient.getCurrentArchive() + ".sqlite3");
        String customQuery = "SELECT *, (incoming_value_bank - outgoing_value_bank) as balance_bank, (incoming_value_cash - outgoing_value_cash) as balance_cash FROM entry order by entry_date asc ";
        allEntriesCustomRequest.setQueryClient(customQuery);
        GetAllEntriesCustomResponse allEntriesCustomResponse = archivesPort.getAllEntriesCustom(allEntriesCustomRequest);
        List<EntryBalance> allEntries = allEntriesCustomResponse.getAllEntriesCustomResponse();

        //Create observable list with class for table. add elements.
        ObservableList<EntryForTable> data = tablePrimaNota.getItems();
        data.remove(0, data.size());
        String Date;
        String viti;
        String muaji;
        String dita;
        String categoryString = null;
        for (EntryBalance ent : allEntries) {
             //check category
            if ("0".equals(ent.getIdOperationType()) ){
              //categoria generico
                categoryString = "Generico";
            }
            else
            {
             //different id for category   
            GetOperationTypeRequest operationTypeRequest = new GetOperationTypeRequest();
            operationTypeRequest.setOperationTypeId(ent.getIdOperationType());
            operationTypeRequest.setArchiveName(PrimaNotaFacileClient.getCurrentArchive() + ".sqlite3");
            GetOperationTypeResponse operationTypeResponse = archivesPort.getOperationType(operationTypeRequest);
            OperationType existentOperationType = operationTypeResponse.getOperationType();
                categoryString = existentOperationType.getName();
            }
            
            //adding values to table
            if (categoryString != null) {
                Date = ent.getEntryDate();
                viti = Date.substring(0, 4);
                muaji = Date.substring(5, 7);
                dita = Date.substring(8, 10);
                if (data.size() < 1) {
                    //rreshti i pare
                    data.add(new EntryForTable(ent.getEntryId(), comboContact.get(Integer.parseInt(ent.getIdContact())), dita + "-" + muaji + "-" + viti, ent.getDescription() + " - " + categoryString, ent.getOutCash(), ent.getIncomingValueCash(), ent.getOutgoingValueCash(), ent.getIncomingValueBank(), ent.getOutgoingValueBank(), ent.getBalanceCash(), ent.getBalanceBank(), ent.getIdBank(),ent.getIdContact(),ent.getIdOperationType()));
                } else {
                    //rreshtat e tjere
                    //bilanci bank dhe cash do te mblidhet me bilanci bank dhe cash te nje rreshti me siper
                    data.add(new EntryForTable(ent.getEntryId(), comboContact.get(Integer.parseInt(ent.getIdContact())), dita + "-" + muaji + "-" + viti,
                            ent.getDescription() + " - " + categoryString, ent.getOutCash(), ent.getIncomingValueCash(), ent.getOutgoingValueCash(),
                            ent.getIncomingValueBank(), ent.getOutgoingValueBank(), String.valueOf(Long.parseLong(ent.getBalanceCash()) + Long.parseLong(data.get(data.size() - 1).getTablePrimaNotaSaldiCassa())), String.valueOf(Long.parseLong(ent.getBalanceBank()) + Long.parseLong(data.get(data.size() - 1).getTablePrimaNotaSaldiBanca())), ent.getIdBank(),ent.getIdContact(), ent.getIdOperationType()));
                    
                }
            }
            
        }
        
    }
    
    @FXML
    public boolean showReminderEditDialog(Reminder reminder) throws ParseException {
        try {

            //FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("Demo.fxml"));
            //   Parent root1 = (Parent) fxmlLoader.load();
            //  Stage stage = new Stage();
            //  stage.initModality(Modality.APPLICATION_MODAL);
            //      stage.setTitle("ABC");
            //    stage.setScene(new Scene(root1));  
            //   stage.show();
            // Load the fxml file and create a new stage for the popup dialog.
            FXMLLoader loader = new FXMLLoader(getClass().getResource("NewReminder.fxml"));
            Parent root1 = (Parent) loader.load();
            // Create the dialog Stage.
            Stage dialogStage = new Stage();
            dialogStage.setTitle("Scadenzario");
            dialogStage.initModality(Modality.APPLICATION_MODAL);
            dialogStage.initOwner(btnScadenzarioNew.getScene().getWindow());
            Scene scene = new Scene(root1);
            dialogStage.setScene(scene);

            // Set the person into the controller.
            NewReminder controller = loader.getController();
            controller.setDialogStage(dialogStage);
            controller.setReminder(reminder);

            // Show the dialog and wait until the user closes it
            dialogStage.showAndWait();
            
            return controller.isOkClicked();
        } catch (IOException e) {
            System.out.println(e.getMessage());
            return false;
        }
    }
    
    @FXML
    public void handlePrimaNotaRiepilogo() throws IOException{
         FXMLLoader loader = new FXMLLoader(getClass().getResource("Statistics.fxml"));
            Parent root2 = (Parent) loader.load();

            // Create the dialog Stage.
            Stage dialogStage = new Stage();
            dialogStage.setTitle("Statistics");
            dialogStage.initModality(Modality.APPLICATION_MODAL);
            dialogStage.initOwner(buttonPrimaNotaNuovo.getScene().getWindow());
            Scene scene = new Scene(root2);
            dialogStage.setScene(scene);

            // Set the person into the controller.
            StatisticsController controller = loader.getController();
            controller.setDialogStage(dialogStage);
            

            // Show the dialog and wait until the user closes it
            dialogStage.showAndWait();

    }
    
    @FXML
    public boolean showEntryEditDialog(Entry entry) throws ParseException {
        try {

            //FXMLLoader fxmlLoaderselectedEntry.getTablePrimaNotaId() = new FXMLLoader(getClass().getResource("Demo.fxml"));
            //   Parent root1 = (Parent) fxmlLoader.load();
            //  Stage stage = new Stage();
            //  stage.initModality(Modality.APPLICATION_MODAL);
            //      stage.setTitle("ABC");
            //    stage.setScene(new Scene(root1));  
            //   stage.show();
            // Load the fxml file and create a new stage for the popup dialog.
            FXMLLoader loader = new FXMLLoader(getClass().getResource("NewEntry.fxml"));
            Parent root2 = (Parent) loader.load();

            // Create the dialog Stage.
            Stage dialogStage = new Stage();
            dialogStage.setTitle("PrimaNota");
            dialogStage.initModality(Modality.APPLICATION_MODAL);
            dialogStage.initOwner(buttonPrimaNotaNuovo.getScene().getWindow());
            Scene scene = new Scene(root2);
            dialogStage.setScene(scene);

            // Set the person into the controller.
            NewEntry controller = loader.getController();
            controller.setDialogStage(dialogStage);
            controller.setEntry(entry);

            // Show the dialog and wait until the user closes it
            dialogStage.showAndWait();
            
            return controller.isOkClicked();
        } catch (IOException e) {
            System.out.println(e.getMessage());
            return false;
        }
    }
    
    @FXML
    public boolean showScadenzarioCompletato(Reminder reminder)  {
        try {

            FXMLLoader loader = new FXMLLoader(getClass().getResource("CompleteReminder.fxml"));
            Parent root2 = (Parent) loader.load();

            // Create the dialog Stage.
            Stage dialogStage = new Stage();
            dialogStage.setTitle("Scadenza");
            dialogStage.initModality(Modality.APPLICATION_MODAL);
            dialogStage.initOwner(tableScadenzario.getScene().getWindow());
            Scene scene = new Scene(root2);
            dialogStage.setScene(scene);

            // Set the person into the controller.
            CompleteReminderController controller = loader.getController();
            controller.setDialogStage(dialogStage);
            controller.setReminder(reminder);
            // Show the dialog and wait until the user closes it
            dialogStage.showAndWait();
            
            return controller.isOkClicked();
        } catch (IOException e) {
            System.out.println(e.getMessage());
            return false;
        }
    }
    
    @FXML
    void handleScadenzarioNew() throws ParseException {

        //method will handle the creation of a new reminder
        Reminder newReminder = new Reminder();
        //set the reminder id to 0 to know that's new reminder
        newReminder.setReminderId("0");
        boolean okClicked = PrimaNotaFacileClient.getInstance().showReminderEditDialog(newReminder);
        if (okClicked) {
            //if result true refresh table 
            //or ADD an info  if you want
        }
        //if ok is false

        //anyway refresh table scadenzario
        handleScadenzarioTabInit();
        
    }
    
    @FXML
    void handlePrimaNotaNew() throws ParseException {
        //method will handle the creation of a new reminder
        Entry newEntry = new Entry();
        //set the reminder id to 0 to know that's new reminder
        newEntry.setEntryId("0");
        boolean okClicked = showEntryEditDialog(newEntry);
        if (okClicked) {
            //if result true refresh table 
            //or ADD an info  if you want
        }
        //if ok is false

        handleVisualizzaDettagli();
    }
    
    @FXML
    void handleNewBankImpostazione() {
        Bank newBank = new Bank();
        //set the reminder id to 0 to know that's new reminder
        newBank.setBankId("0");
        
        boolean okClicked = showBankEditDialog(newBank);
        if (okClicked) {
            //if result true refresh table 
            //or ADD an info  if you want
        }
        handleImpostazioniInit();
    }
    
    @FXML
    void handleBankModifyImpostazioni() {
        selectedBank = listViewImpostazioniBanca.getSelectionModel().getSelectedItem();
        if (selectedBank == null) {
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("Warning");
            alert.setHeaderText("Warning");
            Text text = new Text("Si prega di selezionare un Banca da modificare ");
            text.setWrappingWidth(400);
            alert.getDialogPane().setContent(text);
            Optional<ButtonType> result = alert.showAndWait();
            if (result.get() == ButtonType.OK) {
                alert.close();
            }
        } else {
            //method will handle the creation of a new reminder
            Bank existingBank = new Bank();
            if (selectedBank.contains("-")) {
                //ka desc
                existingBank.setName(selectedBank.split(" - ")[0]);
                existingBank.setDescription(selectedBank.split(" - ")[1]);
                existingBank.setBankId(banks.get(selectedBank.split(" - ")[0]));
            } else {
                //ska
                existingBank.setName(selectedBank);
                existingBank.setDescription("");
                existingBank.setBankId(banks.get(selectedBank));
            }
            
            boolean okClicked = showBankEditDialog(existingBank);
            if (okClicked) {
                //if result true refresh table 
                //or ADD an info  if you want
            }
            //if ok is false

            //anyway refresh table scadenzario
            handleImpostazioniInit();
        }
    }
    
    @FXML
    void handleBankDeleteImpostazioni() {
        Stage dialogStage;
        selectedBank = listViewImpostazioniBanca.getSelectionModel().getSelectedItem();
        if (selectedBank == null) {
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("Warning");
            alert.setHeaderText("Warning");
            Text text = new Text("Si prega di selezionare una Banca da eliminare ");
            text.setWrappingWidth(400);
            alert.getDialogPane().setContent(text);
            //alert.setContentText("Si prega di selezionare un scadenzario da modificare ");
            Optional<ButtonType> result = alert.showAndWait();
            if (result.get() == ButtonType.OK) {
                alert.close();
            }
        } else {
//            PrimaNotaFacileClient.getInstance().setCurrentEntry(selectedEntry.getTablePrimaNotaId());
//            System.out.println(PrimaNotaFacileClient.getInstance().getCurrentEntry());

            Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
            alert.setTitle("Conferma azione");
            alert.setHeaderText("Conferma azione");
            alert.setContentText("Sei sicuro di voler eliminare Banca ?");
            Optional<ButtonType> result = alert.showAndWait();
            if (result.get() == ButtonType.OK) {
                ArchivesPortService archiveService = new ArchivesPortService();
                ArchivesPort archivesPort = archiveService.getArchivesPortSoap11();
                DeleteBankRequest deleteBank = new DeleteBankRequest();
                deleteBank.setBankId(banks.get(selectedBank.split(" - ")[0]));
                deleteBank.setArchiveName(PrimaNotaFacileClient.getCurrentArchive() + ".sqlite3");
                DeleteBankResponse deleteBankResponse = archivesPort.deleteBank(deleteBank);
                String response = deleteBankResponse.getResponse();
                if ("OK".equals(response)) {
                    listViewImpostazioniBanca.setEditable(true);
                    listViewImpostazioniBanca.getItems().remove(selectedBank);
                    System.out.println("Reminder deleted!");
                } else {
                    System.out.println("Error delete Bank!");
                }
            }
        }
        
    }
    
    @FXML
    public boolean showBankEditDialog(Bank bank) {
        try {

            // Load the fxml file and create a new stage for the popup dialog.
            FXMLLoader loader = new FXMLLoader(getClass().getResource("NewBank.fxml"));
            Parent root2 = (Parent) loader.load();

            // Create the dialog Stage.
            Stage dialogStage = new Stage();
            dialogStage.setTitle("Bank");
            dialogStage.initModality(Modality.APPLICATION_MODAL);
            dialogStage.initOwner(btnIpostazioniBancaNew.getScene().getWindow());
            Scene scene = new Scene(root2);
            dialogStage.setScene(scene);

            // Set the person into the controller.
            NewBank controller = loader.getController();
            controller.setDialogStage(dialogStage);
            controller.setBank(bank);

            // Show the dialog and wait until the user closes it
            dialogStage.showAndWait();
            
            return controller.isOkClicked();
        } catch (IOException e) {
            System.out.println(e.getMessage());
            return false;
        }
    }
    
    @FXML
    void handleScadenzarioModifica() throws ParseException {
        
        selectedReminder = tableScadenzario.getSelectionModel().getSelectedItem();
        if (selectedReminder == null) {
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("Warning");
            alert.setHeaderText("Warning");
            Text text = new Text("Si prega di selezionare un scadenzario da modificare ");
            text.setWrappingWidth(400);
            alert.getDialogPane().setContent(text);
            Optional<ButtonType> result = alert.showAndWait();
            if (result.get() == ButtonType.OK) {
                alert.close();
            }
        } else {
            PrimaNotaFacileClient.getInstance().setCurrentReminder(selectedReminder.getTableScadenzarioId());
            System.out.println(PrimaNotaFacileClient.getInstance().getCurrentReminder());

            //method will handle the creation of a new reminder
            Reminder newReminder = new Reminder();
            //set the reminder id to 0 to know that's new reminder
            newReminder.setReminderId(PrimaNotaFacileClient.getInstance().getCurrentReminder());
            newReminder.setDate(selectedReminder.getTableScadenzarioData());
            newReminder.setDescription(selectedReminder.getTableScadenzarioDescrizione());
            //Since we changed the Data Model for the table scadenzario ReminderForTable to have tableCompleted as booelan
            //we're changing the way we store the value of the selcted row from the table
            if (selectedReminder.getTableScadenzarioCompletato() == true) {
                newReminder.setCompleted("1");
            } else {
                newReminder.setCompleted("0");
            }
            newReminder.setIdContact(selectedReminder.getTableScadenzarioControparte());
            newReminder.setValue(selectedReminder.getTableScadenzarioImporto());
            boolean okClicked = showReminderEditDialog(newReminder);
            if (okClicked) {
                //if result true refresh table 
                //or ADD an info  if you want
            }
            //if ok is false

            //anyway refresh table scadenzario
            handleScadenzarioEventsForFilters();
        }
    }
    
    @FXML
    void handleScadenzarioCompleta(Integer index, Boolean wasSelected, Boolean isSelected) throws ParseException {
        tableScadenzario.getSelectionModel().select(index);
        selectedReminder = tableScadenzario.getSelectionModel().getSelectedItem();
                
        if (selectedReminder == null) {
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("Warning");
            alert.setHeaderText("Warning");
            Text text = new Text("Si prega di selezionare un scadenzario da completare ");
            text.setWrappingWidth(400);
            alert.getDialogPane().setContent(text);
            Optional<ButtonType> result = alert.showAndWait();
            if (result.get() == ButtonType.OK) {
                alert.close();
            }
        } else {
             
        if (wasSelected == true && isSelected == false){
            //nuk mund te uncomplete
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("Warning");
            alert.setHeaderText("Warning");
            Text text = new Text("Non si puo completare un scadenzario gia completato!");
            text.setWrappingWidth(400);
            alert.getDialogPane().setContent(text);
            Optional<ButtonType> result = alert.showAndWait();
            if (result.get() == ButtonType.OK) {
                alert.close();
                //inicializo scadenzario tab init meqe po u ndryshua do na futet prap ne cikel nga eventi on change
               handleScadenzarioEventsForFilters();
            }
        }else if (wasSelected == false && isSelected == true){
          //do ta kompletoje
          //check value of column
            //Since we changed the Data Model for the table scadenzario ReminderForTable to have tableCompleted as booelan
            //we're changing the way we store the value of the selcted row from the table
            //method will handle the creation of a new reminder
            Reminder thisReminder = new Reminder();
            PrimaNotaFacileClient.getInstance().setCurrentReminder(selectedReminder.getTableScadenzarioId());
            System.out.println(PrimaNotaFacileClient.getInstance().getCurrentReminder());
                thisReminder.setCompleted("1");           
            //set the reminder id to 0 to know that's new reminder
            thisReminder.setReminderId(PrimaNotaFacileClient.getInstance().getCurrentReminder());
            thisReminder.setDate(selectedReminder.getTableScadenzarioData());
            thisReminder.setDescription(selectedReminder.getTableScadenzarioDescrizione());
          
             int contactId = 0;
            for (Map.Entry<Integer, String> entry : comboContact.entrySet()) {
            //since combo box select index starts from 0 we're gonna increment n after checking for specified value in hashmap
            //in the main controller we display in table the full name of the contact so we're searching in this hashmap for key
            if (entry.getValue().equals(selectedReminder.getTableScadenzarioControparte())) {
                contactId = entry.getKey();
                break;
            }
             }
       
            thisReminder.setIdContact(String.valueOf(contactId));
            thisReminder.setValue(selectedReminder.getTableScadenzarioImporto());
            boolean okClicked = showScadenzarioCompletato(thisReminder);
            if (okClicked) {
                //if result true refresh table 
                //or ADD an info  if you want
            }else{
              //return copletato to 0//practically init table scadenzario
              handleScadenzarioTabInit();
            }


            //anyway refresh table scadenzario
            handleScadenzarioEventsForFilters();
        }
            
        }
       
       
    }
    
    @FXML
    void handlePrimaNotaTableClick() {
        removeFocusFromTableCells();
    }
    
    @FXML
    void removeFocusFromTableCells() {
        anchorPanePrimaNota.requestFocus();
    }
    
    @FXML
    void handlePrimaNotaModifica() throws ParseException {
        
        selectedEntry = tablePrimaNota.getSelectionModel().getSelectedItem();
        if (selectedEntry == null) {
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("Warning");
            alert.setHeaderText("Warning");
            Text text = new Text("Si prega di selezionare un scadenzario da modificare ");
            text.setWrappingWidth(400);
            alert.getDialogPane().setContent(text);
            Optional<ButtonType> result = alert.showAndWait();
            if (result.get() == ButtonType.OK) {
                alert.close();
            }
        } else {
            PrimaNotaFacileClient.getInstance().setCurrentEntry(selectedEntry.getTablePrimaNotaId());
            
            Entry newEntry = new Entry();
            newEntry.setEntryId(PrimaNotaFacileClient.getInstance().getCurrentEntry());
            newEntry.setEntryDate(selectedEntry.getTablePrimaNotaData());
            newEntry.setDescription(selectedEntry.getTablePrimaNotaOperazione().split("-")[0]);
            newEntry.setOutCash(selectedEntry.getTablePrimaNotaFuoriCassa());
            newEntry.setIncomingValueBank(selectedEntry.getTablePrimaNotaBancaEntrate());
            newEntry.setOutgoingValueBank(selectedEntry.getTablePrimaNotaBancaUscite());
            newEntry.setIncomingValueCash(selectedEntry.getTablePrimaNotaCassaEntrate());
            newEntry.setOutgoingValueCash(selectedEntry.getTablePrimaNotaCassaUscite());
            newEntry.setIdOperationType(selectedEntry.getTablePrimaNotaCategoryId());
            newEntry.setIdContact(selectedEntry.getTablePrimaNotaContactId());
           
            
            newEntry.setIdBank(selectedEntry.getTablePrimaNotaBankId());
            boolean okClicked = showEntryEditDialog(newEntry);
            if (okClicked) {
                //if result true refresh table 
                //or ADD an info  if you want
            }
            //if ok is false

            //anyway refresh table scadenzario
            handleVisualizzaDettagli();
        }
    }
       
    @FXML
    void handleScadenzarioStampa() throws FileNotFoundException, ParseException {
     
        try {
                InputStream inputStream = new FileInputStream("ScadenzarioReport.jrxml");
                JasperDesign jasperDesign = JRXmlLoader.load(inputStream);
                JasperReport jasperReport = JasperCompileManager.compileReport(jasperDesign);

                HashMap parameters = new HashMap();
                String period ="";
                if (datePickerScadenzarioFrom.getValue() != null){
                    period = datePickerScadenzarioFrom.getValue().toString();
                }
                if (datePickerScadenzarioTo.getValue()!=null){
                    period += " - "+ datePickerScadenzarioTo.getValue().toString();
                }
                if (!"".equals(period)){
                    parameters.put("parameterPeriod",period );
                }
                
                String TotaleScadenze = null;
                 
                parameters.put("parameterFilter", comboScadenzarioControparte.getSelectionModel().getSelectedItem());
                
                ObservableList<ReminderForTable> data = tableScadenzario.getItems();
                Double totaleScadenze = 0.0;
                for (ReminderForTable rem : data){
                   totaleScadenze += NumberFormat.getCurrencyInstance(Locale.ITALY).parse(rem.getTableScadenzarioImporto()).doubleValue();                 
                }
                TotaleScadenze = NumberFormat.getCurrencyInstance(Locale.ITALY).format(totaleScadenze);
                parameters.put("totaleScadenze",TotaleScadenze );
                
                JRBeanCollectionDataSource beanColDataSource = new JRBeanCollectionDataSource(data);
                JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, beanColDataSource);
                
                
                JasperViewer.viewReport(jasperPrint,false);
     
               
            } catch (JRException ex) {
                Logger.getLogger(PrimaNotaFacileClient.class.getName()).log(Level.SEVERE, null, ex);
            }
        
        
    
    }
    
    @FXML
    void handleScadenzarioElimina() {
        Stage dialogStage;
        selectedReminder = tableScadenzario.getSelectionModel().getSelectedItem();
        if (selectedReminder == null) {
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("Warning");
            alert.setHeaderText("Warning");
            Text text = new Text("Si prega di selezionare un scadenzario da eliminare ");
            text.setWrappingWidth(400);
            alert.getDialogPane().setContent(text);
            //alert.setContentText("Si prega di selezionare un scadenzario da modificare ");
            Optional<ButtonType> result = alert.showAndWait();
            if (result.get() == ButtonType.OK) {
                alert.close();
            }
        } else {
            PrimaNotaFacileClient.getInstance().setCurrentReminder(selectedReminder.getTableScadenzarioId());
            System.out.println(PrimaNotaFacileClient.getInstance().getCurrentReminder());
            
            Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
            alert.setTitle("Conferma azione");
            alert.setHeaderText("Conferma azione");
            alert.setContentText("Sei sicuro di voler eliminare PrimaNota ?");
            Optional<ButtonType> result = alert.showAndWait();
            if (result.get() == ButtonType.OK) {
                ArchivesPortService archiveService = new ArchivesPortService();
                ArchivesPort archivesPort = archiveService.getArchivesPortSoap11();
                DeleteReminderRequest deleteReminder = new DeleteReminderRequest();
                deleteReminder.setReminderId(selectedReminder.getTableScadenzarioId());
                deleteReminder.setArchiveName(PrimaNotaFacileClient.getCurrentArchive() + ".sqlite3");
                DeleteReminderResponse deleteReminderResponse = archivesPort.deleteReminder(deleteReminder);
                String response = deleteReminderResponse.getResponse();
                if ("OK".equals(response)) {
                    tableScadenzario.setEditable(true);
                    tableScadenzario.getItems().remove(selectedReminder);
                    //anyway refresh table scadenzario
                    handleScadenzarioTabInit();
                    System.out.println("Reminder deleted!");
                } else {
                    System.out.println("Error delete reminder!");
                }
            }
        }
    }
    
    @FXML
    void handlePrimaNotaElimina() throws ParseException {
        Stage dialogStage;
        selectedEntry = tablePrimaNota.getSelectionModel().getSelectedItem();
        if (selectedEntry == null) {
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("Warning");
            alert.setHeaderText("Warning");
            Text text = new Text("Si prega di selezionare una PrimaNota da eliminare ");
            text.setWrappingWidth(400);
            alert.getDialogPane().setContent(text);
            //alert.setContentText("Si prega di selezionare un scadenzario da modificare ");
            Optional<ButtonType> result = alert.showAndWait();
            if (result.get() == ButtonType.OK) {
                alert.close();
            }
        } else {
            PrimaNotaFacileClient.getInstance().setCurrentEntry(selectedEntry.getTablePrimaNotaId());
            System.out.println(PrimaNotaFacileClient.getInstance().getCurrentEntry());
            
            Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
            alert.setTitle("Conferma azione");
            alert.setHeaderText("Conferma azione");
            alert.setContentText("Sei sicuro di voler eliminare PrimaNota ?");
            Optional<ButtonType> result = alert.showAndWait();
            if (result.get() == ButtonType.OK) {
                ArchivesPortService archiveService = new ArchivesPortService();
                ArchivesPort archivesPort = archiveService.getArchivesPortSoap11();
                DeleteEntryRequest deleteEntry = new DeleteEntryRequest();
                deleteEntry.setEntryId(PrimaNotaFacileClient.getInstance().getCurrentEntry());
                deleteEntry.setArchiveName(PrimaNotaFacileClient.getCurrentArchive() + ".sqlite3");
                DeleteEntryResponse deleteEntryResponse = archivesPort.deleteEntry(deleteEntry);
                String response = deleteEntryResponse.getResponse();
                if ("OK".equals(response)) {
                    tablePrimaNota.setEditable(true);
                   tablePrimaNota.getItems().remove(selectedEntry);
                    handleVisualizzaDettagli();
                    System.out.println("Reminder deleted!");
                } else {
                    System.out.println("Error delete reminder!");
                }
            }
        }
    }
    
    @FXML
    public void handlePrimaNotaRadioFilters(){
        //method to handle radio button filters
        //will modify the state of the combo box banca to enabled only if it is selected radio button solo banca, disable otherwise
        if (radioPrimaNotaFilterSoloBanca.isSelected() == true){
            comboPrimaNotaBanca.setDisable(false);
            comboPrimaNotaBanca.getSelectionModel().clearSelection();
            //selected first element
            comboPrimaNotaBanca.getSelectionModel().select("Tutte");
        }else{
           comboPrimaNotaBanca.setDisable(true);
           comboPrimaNotaBanca.getSelectionModel().clearSelection();
        }
        
    }
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        
        //image ad
        handleImagaView();
        
        HamburgerBackArrowBasicTransition hamburgerTransition = new HamburgerBackArrowBasicTransition(hamburgerMenu);
        tabPaneMain.getStyleClass().add("floating");
        tabPaneImpostazioni.getStyleClass().add("floating");
        //event handler per mouse click ne hamburger menu
        hamburgerMenu.addEventHandler(MouseEvent.MOUSE_CLICKED, e -> {
            hamburgerTransition.setRate(hamburgerTransition.getRate() * - 1);
            hamburgerTransition.play();
            if (drwMenu.isShown()) {
                tabPaneMain.setLayoutX(1);
                drwMenu.close();
                //AnchorPane.setLeftAnchor(tabPaneMain, 0.0);
            } else {
                drwMenu.open();
                tabPaneMain.setLayoutX(151);
                AnchorPane.setLeftAnchor(tabPaneMain, 150.0);
            }
            
        });
        hamburgerTransition.setOnFinished(new EventHandler<ActionEvent>() {
            
            @Override
            public void handle(ActionEvent event) {
                System.out.println(drwMenu.isShowing());
                if (!drwMenu.isShowing()) {
                    
                    System.out.println("Finishing the animation");
                    AnchorPane.setLeftAnchor(tabPaneMain, 0.0);
                    
                }
                
            }
        });

        //ne inicializim menuja duhet te jete opened.
        //hamburgeriduhet te kete shenjen e shigjetes
        // tabPaneMain.setLayoutX(1);   
        tabPaneMain.setLayoutX(1);
        AnchorPane.setLeftAnchor(tabPaneMain, 0.0);
        hamburgerTransition.setRate(-1);
        hamburgerTransition.play();
        drwMenu.setSidePane(vBoxMenu);
        drwMenu.close();
        btnScadenzario.getStyleClass().set(0, "selected-menu-item");
        btnPrimaNota.getStyleClass().set(0, "menu-item");
        btnInfo.getStyleClass().set(0, "menu-item");
        btnImpostazioni.getStyleClass().set(0, "menu-item");
        btnRubrica.getStyleClass().set(0, "menu-item");
        tabPaneMain.getSelectionModel().select(tabScandezario);

        //change format of date pickers for scadenzario tab
        datePickerScadenzarioFrom.setConverter(new StringConverter<LocalDate>() {
            String pattern = "dd-MM-yyyy";
            DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern(pattern);
            
            {
                datePickerScadenzarioFrom.setPromptText(pattern.toLowerCase());
            }
            
            @Override
            public String toString(LocalDate date) {
                if (date != null) {
                    return dateFormatter.format(date);
                } else {
                    return "";
                }
            }
            
            @Override
            public LocalDate fromString(String string) {
                if (string != null && !string.isEmpty()) {
                    return LocalDate.parse(string, dateFormatter);
                } else {
                    return null;
                }
            }
        });
        
        datePickerScadenzarioTo.setConverter(new StringConverter<LocalDate>() {
            String pattern = "dd-MM-yyyy";
            DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern(pattern);
            
            {
                datePickerScadenzarioTo.setPromptText(pattern.toLowerCase());
            }
            
            @Override
            public String toString(LocalDate date) {
                if (date != null) {
                    return dateFormatter.format(date);
                } else {
                    return "";
                }
            }
            
            @Override
            public LocalDate fromString(String string) {
                if (string != null && !string.isEmpty()) {
                    return LocalDate.parse(string, dateFormatter);
                } else {
                    return null;
                }
            }
        });

        //set format for datepickers of prima nota tab
        datePickerPrimaNotaFrom.setConverter(new StringConverter<LocalDate>() {
            String pattern = "dd-MM-yyyy";
            DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern(pattern);
            
            {
                datePickerPrimaNotaFrom.setPromptText(pattern.toLowerCase());
            }
            
            @Override
            public String toString(LocalDate date) {
                if (date != null) {
                    return dateFormatter.format(date);
                } else {
                    return "";
                }
            }
            
            @Override
            public LocalDate fromString(String string) {
                if (string != null && !string.isEmpty()) {
                    return LocalDate.parse(string, dateFormatter);
                } else {
                    return null;
                }
            }
        });
        
        datePickerPrimaNotaTo.setConverter(new StringConverter<LocalDate>() {
            String pattern = "dd-MM-yyyy";
            DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern(pattern);
            
            {
                datePickerPrimaNotaTo.setPromptText(pattern.toLowerCase());
            }
            
            @Override
            public String toString(LocalDate date) {
                if (date != null) {
                    return dateFormatter.format(date);
                } else {
                    return "";
                }
            }
            
            @Override
            public LocalDate fromString(String string) {
                if (string != null && !string.isEmpty()) {
                    return LocalDate.parse(string, dateFormatter);
                } else {
                    return null;
                }
            }
        });
  
        //during initalization we're gonna save all contacts ids and full names in a hashmap in order to use them for scadenzario and prima nota table
        //and also to initialise comboScadenzarioControparte 
        //this to minimise calls to web service
        //set class to search from soap server
        ArchivesPortService archiveService = new ArchivesPortService();
        ArchivesPort archivesPort = archiveService.getArchivesPortSoap11();
        GetAllContactsRequest allContactsRequest = new GetAllContactsRequest();
        allContactsRequest.setArchiveName(PrimaNotaFacileClient.getCurrentArchive() + ".sqlite3");
        GetAllContactsResponse allContactsResponse = archivesPort.getAllContacts(allContactsRequest);

        //si pergjigje marrim nje liste me contact
        List<Contact> allcontacs = allContactsResponse.getAllContactsResponse();
        //refresh combo and hash
        comboScadenzarioControparte.getItems().remove(0, comboScadenzarioControparte.getItems().size());
        comboContact.clear();
        ObservableList<String> comboMainContacts = comboScadenzarioControparte.getItems();
        //in order to have in filter a value for all contacts we're gonna add a new element in combo
        comboMainContacts.add("Tutti");
        for (Contact contact : allcontacs) {
            comboMainContacts.add(contact.getFullName());
            comboContact.put(Integer.parseInt(contact.getContactId()), contact.getFullName());
        }
    
        //we're gonna create an event handler for combobox selection in Scadenzario Tab 
        // Handle ComboBox event.
        comboScadenzarioControparte.setOnAction((event) -> {
            //on selection call the handle method 
            handleScadenzarioEventsForFilters();
        });

        //event handler for descrizione textfield in scadenzario tab
        txtScadenzarioDescrizione.setOnAction((event) -> {
            handleScadenzarioEventsForFilters();
        });

        //event handler for date pickers
        datePickerScadenzarioTo.setOnAction((event) -> {
            handleScadenzarioEventsForFilters();
        });
        
        datePickerScadenzarioFrom.setOnAction((event) -> {
            handleScadenzarioEventsForFilters();
        });
        
        checkBoxScadenzarioNascondiCompletati.setOnMouseClicked((event) -> {
            handleScadenzarioEventsForFilters();
        });
        

        spinnerPrimaNotaMese.setOnMouseClicked((event)-> {
            handleSpinnerMese();
        });
        
        spinnerPrimaNotaAnno.setOnMouseClicked((event)-> {
            handleSpinnerAnno();
        });
//        tabOperazioniImpostazioni.getTabPane().setOnMouseClicked((event) -> {
//            handleImpostazioniOperazioniInit();
//        });
        
        //event handlers for radio buttons in prima nota
        radioPrimaNotaFilterTutto.setOnAction((event)-> {
            handlePrimaNotaRadioFilters();
        });
        
        radioPrimaNotaFilterSoloBanca.setOnAction((event)-> {
            handlePrimaNotaRadioFilters();
        });
        
        radioPrimaNotaFilterSoloCassa.setOnAction((event)-> {
            handlePrimaNotaRadioFilters();
        });
        
        buttonPrimaNotaRiepilogo.setOnAction((event)-> {
            try {
                handlePrimaNotaRiepilogo();
            } catch (IOException ex) {
                Logger.getLogger(MainScreenController.class.getName()).log(Level.SEVERE, null, ex);
            }
        });
    
        
         handleScadenzarioEventsForFilters();
    }
    
}
