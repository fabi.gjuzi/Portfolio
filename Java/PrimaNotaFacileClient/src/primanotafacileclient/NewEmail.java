package primanotafacileclient;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXPasswordField;
import com.jfoenix.controls.JFXTextField;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.text.ParseException;
import java.util.Date;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.mail.Authenticator;
import javax.mail.PasswordAuthentication;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

public class NewEmail {

    @FXML
    private AnchorPane anchorPane;

    @FXML
    private JFXTextField senderEmail;

    @FXML
    private JFXPasswordField senderEmailPassword;

    @FXML
    private JFXButton btnSave;

    @FXML
    private JFXButton btnCancel;
    
    @FXML
    private JFXTextField recipientEmail;

     private Stage dialogStage;
     
     private final EmailConsulente newEmailConsulente = new EmailConsulente(); 
     
     /**
     * Sets the stage of this dialog.
     *
     * @param dialogStage
     */
    public void setDialogStage(Stage dialogStage) {
        this.dialogStage = dialogStage;
        this.dialogStage.getScene().getStylesheets().add("assets/style/style.css");
    }

    private final Stage getDialogStage() {
        return dialogStage;
    }
    
    /**
     * Returns EmailConsulente 
     *
     * @return
     */
    public EmailConsulente returnEmailConsulente() {
        return newEmailConsulente;
    }
    
    @FXML
    void handleOk(MouseEvent event) {
           
        if(isInputValid()){
            //smtp will be rad from a config file
             File f = new File("mailConfig.json");
             if (f.exists() && !f.isDirectory()) {
                  JSONParser parser = new JSONParser();
            try {
                JSONObject mailConfig = (JSONObject) parser.parse(new FileReader("mailConfig.json"));
                String SmtpServer = (String) mailConfig.get("smtpServer");
                 newEmailConsulente.setSmtpServer(SmtpServer);


            } catch (FileNotFoundException e) {
                Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, e);
            } catch (IOException | org.json.simple.parser.ParseException e) {
                Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, e);
            }
            
            }else{
                //create
                JSONObject jsonMailConfig = new JSONObject();
                jsonMailConfig.put("smtpServer", "smtp.ipage.com"); //by default we're adding this one
                newEmailConsulente.setSmtpServer("smtp.ipage.com");
                try (FileWriter file2 = new FileWriter("mailConfig.json")) {
                file2.write(jsonMailConfig.toJSONString());
                file2.close();
                System.out.println("Successfully Copied JSON Object to File...");
                System.out.println("\nJSON Object: " + jsonMailConfig);
                 } catch (IOException ex) {
                    Logger.getLogger(NewEmail.class.getName()).log(Level.SEVERE, null, ex);
                 }
                
            }
            
              newEmailConsulente.setSenderEmail(senderEmail.getText());
              newEmailConsulente.setSenderPassword(senderEmailPassword.getText());
              newEmailConsulente.setRecipientEmail(recipientEmail.getText());
              dialogStage.close(); 
        }
           
        
    }
    
      @FXML
    void handleCancel(MouseEvent event) {
         
         dialogStage.close();   
        
    }
    
    private boolean isInputValid() {
        String errorMessage = "";
       
        if ("".equals(senderEmail.getText())) {
            errorMessage = "Please set the sender's email address";
        }
         if ("".equals(senderEmailPassword.getText())) {
            errorMessage = "Please set the sender's email address";
        }
          if ("".equals(recipientEmail.getText())) {
            errorMessage = "Please set the recipient's email address";
        }
         
           
        if (!"".equals(senderEmail.getText())) {
            if (senderEmail.getText().startsWith("@")) {
                errorMessage = "Text entered for sender's email address is not a valid e-mail";
            }
            if (!senderEmail.getText().startsWith("@") && (!senderEmail.getText().contains("@") || !senderEmail.getText().split("@")[1].contains(".") || senderEmail.getText().split("@")[1].startsWith(".")
                    || senderEmail.getText().split("@")[1].indexOf(".") == senderEmail.getText().split("@")[1].length() - 1)) {
                errorMessage = "Text entered for sender's email address is not a valid e-mail";
            }
        }
        
         if (!"".equals(recipientEmail.getText())) {
            if (recipientEmail.getText().startsWith("@")) {
                errorMessage = "Text entered for recipient's email address is not a valid e-mail";
            }
            if (!recipientEmail.getText().startsWith("@") && (!recipientEmail.getText().contains("@") || !recipientEmail.getText().split("@")[1].contains(".") || recipientEmail.getText().split("@")[1].startsWith(".")
                    || recipientEmail.getText().split("@")[1].indexOf(".") == recipientEmail.getText().split("@")[1].length() - 1)) {
                errorMessage = "Text entered for recipient's email address is not a valid e-mail";
            }
        }
        
        if (errorMessage.length() == 0) {
            return true;
        } else {
            // Show the error message.
            Alert alert = new Alert(AlertType.ERROR);
            alert.initOwner(dialogStage);
            alert.setTitle("Campi non validi");
            alert.setHeaderText("Si prega di correggere i campi non validi");
            Text text = new Text(errorMessage);
            text.setWrappingWidth(400);
            alert.getDialogPane().setContent(text);
            alert.showAndWait();

            return false;
        }
    }
    

    
    @FXML
    private void initialize() {
        
    }
    /*
    @FXML
    void handleOk() {
        MainScreenController.getInstance().addNewEmailToListView(emailtxt.getText());
    }
    */
   
    public static void sendEmailWithAttachments(String host, String port,
            final String userName, final String password, String toAddress,
            String subject, String message, String[] attachFiles, String requireAuth, String enableTls)
            throws AddressException, MessagingException, IOException {
        // sets SMTP server properties
        Properties properties = System.getProperties();
        properties.put("mail.smtp.host", host);
        properties.put("mail.smtp.port", port);
        properties.put("mail.smtp.auth", requireAuth);
        properties.put("mail.smtp.starttls.enable", enableTls);
        properties.put("mail.smtp.user", userName);
        properties.put("mail.smtp.password", password);
 
//         creates a new session with an authenticator
        Authenticator auth = new Authenticator() {
            
            public PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(userName, password);
            }
        };
        Session session = Session.getInstance(properties, auth);
 
        // creates a new e-mail message
        MimeMessage msg = new MimeMessage(session);
 
        msg.setFrom(new InternetAddress(userName));
        InternetAddress[] toAddresses = { new InternetAddress(toAddress) };
        msg.setRecipients(Message.RecipientType.TO, toAddresses);
        msg.setSubject(subject);
        msg.setSentDate(new Date());
 
        // creates message part
        MimeBodyPart messageBodyPart = new MimeBodyPart();
        messageBodyPart.setContent(message, "text/html");
 
        // creates multi-part
        Multipart multipart = new MimeMultipart();
        multipart.addBodyPart(messageBodyPart);
 
        // adds attachments
        if (attachFiles != null && attachFiles.length > 0) {
            for (String filePath : attachFiles) {
                MimeBodyPart attachPart = new MimeBodyPart();
 
                try {
                    attachPart.attachFile(filePath);
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
 
                multipart.addBodyPart(attachPart);
            }
        }
 
        // sets the multi-part as e-mail's content
        msg.setContent(multipart);
 
        // sends the e-mail
        Transport.send(msg);
 
    }
}
