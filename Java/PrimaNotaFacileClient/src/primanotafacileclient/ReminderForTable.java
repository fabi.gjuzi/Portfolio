
package primanotafacileclient;

import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleStringProperty;



public final class ReminderForTable {

private final SimpleStringProperty tableScadenzarioData = new SimpleStringProperty("");
private final SimpleStringProperty tableScadenzarioDescrizione = new SimpleStringProperty("");
private final SimpleStringProperty tableScadenzarioControparte = new SimpleStringProperty("");
private final SimpleStringProperty tableScadenzarioImporto = new SimpleStringProperty();
private final SimpleBooleanProperty tableScadenzarioCompletato = new SimpleBooleanProperty();
private final SimpleStringProperty tableScadenzarioId = new SimpleStringProperty("");

public ReminderForTable() {
     this("","","","",false,"");
}

public ReminderForTable( String tableScadenzarioData, String tableScadenzarioDescrizione, String tableScadenzarioControparte, String tableScadenzarioImporto, Boolean tableScadenzarioCompletato, String tableScadenzarioId) {
    /*
    this.tableScadenzarioData = new SimpleStringProperty(tableScadenzarioData);
    this.tableScadenzarioDescrizione = new SimpleStringProperty(tableScadenzarioDescrizione);
    this.tableScadenzarioControparte = new SimpleStringProperty(tableScadenzarioControparte);
    this.tableScadenzarioImporto = new SimpleStringProperty(tableScadenzarioImporto);
    this.tableScadenzarioCompletato = new SimpleStringProperty(tableScadenzarioCompletato);    
     */
    setTableScadenzarioData(tableScadenzarioData);
    setTableScadenzarioDescrizione(tableScadenzarioDescrizione);
    setTableScadenzarioControparte(tableScadenzarioControparte);
    setTableScadenzarioImporto(tableScadenzarioImporto);
    setTableScadenzarioCompletato(tableScadenzarioCompletato);
    setTableScadenzarioId(tableScadenzarioId);
}
public SimpleBooleanProperty tableScadenzarioCompletatoProperty() {
        return this.tableScadenzarioCompletato;
    }

    public java.lang.Boolean getTableScadenzarioCompletato() {
        return this.tableScadenzarioCompletatoProperty().get();
    }

    public void setTableScadenzarioCompletato(final java.lang.Boolean tableScadenzarioCompletato) {
        this.tableScadenzarioCompletatoProperty().set(tableScadenzarioCompletato);
    }

public String getTableScadenzarioData() {
    return tableScadenzarioData.getValue();
}
public void setTableScadenzarioData(String tableScadenzarioData) {
    this.tableScadenzarioData.set(tableScadenzarioData);
}

public String getTableScadenzarioDescrizione() {
    return tableScadenzarioDescrizione.get();
}
public void setTableScadenzarioDescrizione(String tableScadenzarioDescrizione) {
    this.tableScadenzarioDescrizione.set(tableScadenzarioDescrizione);
}

public String getTableScadenzarioControparte() {
    return tableScadenzarioControparte.getValue();
}
public void setTableScadenzarioControparte(String tableScadenzarioControparte) {
    this.tableScadenzarioControparte.set(tableScadenzarioControparte);
}
public String getTableScadenzarioImporto() {
    return tableScadenzarioImporto.getValue();
}
public void setTableScadenzarioImporto(String tableScadenzarioImporto) {
    this.tableScadenzarioImporto.set(tableScadenzarioImporto);
}
//public Boolean getTableScadenzarioCompletato() {
//    return tableScadenzarioCompletato.getValue();
//}
//public void setTableScadenzarioCompletato(Boolean tableScadenzarioCompletato) {
//    this.tableScadenzarioCompletato.set(tableScadenzarioCompletato);
//}
public String getTableScadenzarioId() {
    return tableScadenzarioId.getValue();
}
public void setTableScadenzarioId(String tableScadenzarioId) {
    this.tableScadenzarioId.set(tableScadenzarioId);
}
}