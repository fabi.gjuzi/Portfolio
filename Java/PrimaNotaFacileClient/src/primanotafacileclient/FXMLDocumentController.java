package primanotafacileclient;


import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URL;
import java.util.List;

import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ContentDisplay;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleButton;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.TextAlignment;
import javafx.stage.Modality;
import javafx.stage.Stage;
import org.apache.commons.io.FilenameUtils;

import org.json.simple.*;


import net.unnitech.primanotafacile.*;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

/**
 *
 * @author unnitech
 */
public class FXMLDocumentController implements Initializable {
           
    @FXML
    private BorderPane borderPane;

    @FXML
    private VBox vBoxLeft;

    @FXML
    private VBox vBoxCrudFunctions;

    @FXML
    private Button btnOpen;

    @FXML
    private Button btnNew;

    @FXML
    private Button btnCopy;

    @FXML
    private Button btnDelete;

    @FXML
    private Button btnRename;

    @FXML
    private ToggleButton toggleButton1;

    @FXML
    private VBox vBoxBackupFunctions;

    @FXML
    private Button btnExport;

    @FXML
    private Button btnImport;

    @FXML
    private HBox hBoxBottom;

    @FXML
    private AnchorPane anchorPaneCenter;

    @FXML
    private GridPane gridPaneArchives;

    @FXML
    private VBox vBoxTop;

    @FXML
    private Label labelChooseLocation;

    @FXML
    private HBox hBoxRadioButtons;

    @FXML
    private RadioButton radioLocal;

    @FXML
    private RadioButton radioRemote;
    
    //input box FXML reference
    @FXML
    private AnchorPane anchorPaneInputBox;

    
    @FXML
    void handleActionCopy(ActionEvent event) {

    }

    @FXML
    void handleActionDelete(ActionEvent event) {

    }

    @FXML
    void handleActionExport(ActionEvent event) {

    }

    @FXML
    void handleActionImport(ActionEvent event) {

    }
    
    @FXML
    void handleActionNew(ActionEvent event) {        
        //Shtimi i nje arkive te re do te konsistoje ne shtimin e emrit te file JSON si dhe thirrja me sukses e metodes SOAP server
        String archiveName = "";
        try {
              archiveName = InputConfirmBox.display("Si prega di scrivere un nome per il nuovo archivio");
             if(archiveName != null){
                 //nese eshte vendosur nje emer archive
                 Archive archive = new Archive();
                 archive.setArchiveName(archiveName+".sqlite3");
                 //set class to search from soap server
                ArchivesPortService archiveService = new ArchivesPortService();
                ArchivesPort archivesPort = archiveService.getArchivesPortSoap11();           
                AddArchiveRequest addArchive = new AddArchiveRequest();
                addArchive.setArchive(archive);
                AddArchiveResponse addArchiveResponse = archivesPort.addArchive(addArchive);
                String pergjige = addArchiveResponse.getResponse();
                if ("OK".equals(pergjige)){
                    //arkiva u shtua
                    //shtojme elementin ne JSON
                    File f = new File("config.json");
                    if(f.exists() && !f.isDirectory()) { 
                      JSONParser parser = new JSONParser();
                      try{
                          FileReader fileReader = new FileReader("config.json");
                          JSONArray localArchives = (JSONArray) parser.parse(fileReader);
                          fileReader.close();
                          //shtojme elementin e ri
                          JSONObject newArchive = new JSONObject();
                        newArchive.put("archiveName", FilenameUtils.removeExtension(archive.getArchiveName()));
                        newArchive.put("password", "");  
                        //e sthojme ne array
                        localArchives.add(newArchive);  
                        //shkruajme file-in
                        try (FileWriter file = new FileWriter("config.json") ) {
                        file.write(localArchives.toJSONString());
                        System.out.println("Successfully Copied JSON Object to File...");
                        System.out.println("\nJSON Object: " + localArchives);
                         file.close();
                         //shtojme butonin ne grid
                            int i = localArchives.size();                
                            final ToggleButton temp = new ToggleButton(archiveName);
                            final ImageView toggleImage = new ImageView();
                            final Image image = new Image("/assets/change.png"); 
                            toggleImage.setImage(image);
                            toggleImage.setFitHeight(40);
                            toggleImage.setFitWidth(40);
                            temp.setGraphic(toggleImage);
                            temp.setId("ToggleButton"+i);
                            temp.setContentDisplay(ContentDisplay.TOP);
                            temp.setPrefSize(135, 80);
                            temp.setWrapText(true);
                            temp.setAlignment(Pos.CENTER);
                            temp.setTextAlignment(TextAlignment.CENTER);
                            temp.setMaxHeight(Double.MAX_VALUE);
                            temp.setMaxWidth(Double.MAX_VALUE);
                            gridPaneArchives.setHgap(5); //horizontal gap in pixels => that's what you are asking for
                            gridPaneArchives.setVgap(5); //vertical gap in pixels
                            gridPaneArchives.setPadding(new Insets(5, 5, 5, 5));
                            anchorPaneCenter.setRightAnchor(gridPaneArchives, .0);
                            anchorPaneCenter.setTopAnchor(gridPaneArchives, .0);
                            anchorPaneCenter.setLeftAnchor(gridPaneArchives, .0);
                            AnchorPane.setBottomAnchor(gridPaneArchives, .0);
                            temp.setStyle("-fx-background-color: transparent");
                            temp.setOnAction((ActionEvent e) -> {
                         
                            if (temp.isSelected() == true) {
                                temp.setStyle("-fx-background-color: #3026d9");
                                System.out.println("I'm button with id: "+temp.getId()+"\n You selected me!");
                               
                                
                            }      
                            else{
                                temp.setStyle("-fx-background-color: transparent");
                                System.out.println("I'm button with id: "+temp.getId()+"\n You unselected me!");
                            }
                        
                            });
                            //ne kete rast do te nise numerimi nga 1 sepse i merr vlere nga size i array
                            if(i>=1 && i <=3){
                                gridPaneArchives.add(temp, i-1, 0);
                            }else if(i > 3 && i<=6){
                                gridPaneArchives.add(temp, (i-4), 1);
                            }else if(i >6 && i<=9){
                                gridPaneArchives.add(temp, (i-7), 2);
                            }else if(i >9 && i <= 12){
                                gridPaneArchives.add(temp, (i-10), 3);
                            }else{
                                //i > 11 outside of grid error
                                System.out.println("To many archives, archives outside of grid");
                            }
      
               
                        } catch (IOException ex) {
                        Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, ex);
                        }
                          
                        
                      }catch (FileNotFoundException e) {
                        Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, e);
                      } catch (IOException e) {
                        Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, e);
                      } catch (ParseException e) {
                        Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, e);
                      }
                      
                    }
                }else{
                    System.out.println("Gabim ne shtimin e arkives!");
                }
             }
         } catch (Exception ex) {
             Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, ex);
         }
        
        //
        
    }
    
      
    @FXML
    void handleActionOpen(ActionEvent event) {

    }

    @FXML
    void handleActionRename(ActionEvent event) {

    }
    
     @FXML
    void handleOnFunction(ActionEvent event) {
        
        if (toggleButton1.isSelected() == true) {
            toggleButton1.setStyle("-fx-background-color: #3026d9");

        }      
        else{
            toggleButton1.setStyle("-fx-background-color: transparent");
        }
            
            
                
    }
    
    
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        
        toggleButton1.setStyle("-fx-background-color: transparent");
        
           /*
            Olsi code to initialize Grid pane to show archives from JSON config file or Create JSON config file from soap server
            The Grid is 3X4 
            */
           //During initialisation we'll search for config.txt JSON file with all archives listed.
        File f = new File("config.json");
        if(f.exists() && !f.isDirectory()) { 
            JSONParser parser = new JSONParser();
        try {                       
                JSONArray localArchives = (JSONArray) parser.parse(new FileReader("config.json"));
                int i = 0;
                for (Object objArchive : localArchives){   
                    JSONObject archive = (JSONObject) objArchive;
                    String archiveName = (String) archive.get("archiveName");
                    System.out.println(archiveName);  
                    
                    final ToggleButton temp = new ToggleButton(archiveName);
                    final ImageView toggleImage = new ImageView();
                    final Image image = new Image("/assets/change.png"); 
                    toggleImage.setImage(image);
                    toggleImage.setFitHeight(40);
                    toggleImage.setFitWidth(40);
                    temp.setGraphic(toggleImage);
                    temp.setId("ToggleButton"+i);
                    temp.setContentDisplay(ContentDisplay.TOP);
                    temp.setPrefSize(135, 80);
                    temp.setWrapText(true);
                    temp.setAlignment(Pos.CENTER);
                    temp.setTextAlignment(TextAlignment.CENTER);
                    temp.setMaxHeight(Double.MAX_VALUE);
                    temp.setMaxWidth(Double.MAX_VALUE);
                    gridPaneArchives.setHgap(5);  //horizontal gap in pixels => that's what you are asking for
                    gridPaneArchives.setVgap(5); //vertical gap in pixels
                    gridPaneArchives.setPadding(new Insets(5, 5, 5, 5));
                    anchorPaneCenter.setRightAnchor(gridPaneArchives, .0);
                    anchorPaneCenter.setTopAnchor(gridPaneArchives, .0);
                    anchorPaneCenter.setLeftAnchor(gridPaneArchives, .0);
                    AnchorPane.setBottomAnchor(gridPaneArchives, .0);
                    temp.setStyle("-fx-background-color: transparent");
                    temp.setOnAction((ActionEvent e) -> {
                         
                        if (temp.isSelected() == true) {
                                temp.setStyle("-fx-background-color: #3026d9");
                                System.out.println("I'm button with id: "+temp.getId()+"\n You selected me!");
                                //simulojme unselectimin e butonave te tjere kur eshte selektuar nje but
                                 int nrButonave = localArchives.size();
                                 System.out.println("Nr i butonave: "+nrButonave);
                                for (int z = 0;z<nrButonave;z++){
                                    if (!("ToggleButton"+z).equals(temp.getId())){
                                        System.out.println("The other button of for loop: "+"ToggleButton"+z);
                                        Scene scene = gridPaneArchives.getScene();
                                        String btnId = "ToggleButton"+z;
                                        ToggleButton tB = (ToggleButton) scene.lookup("#"+btnId);
                                        if (tB.isSelected() == true){
                                            tB.setSelected(false);
                                            tB.setStyle("-fx-background-color: transparent");
                                        }
                                        
                                        System.out.println("Po erdhi ktu duhet te beje unselect butonat e tjere");
                                    }
                                }
                        }      
                        else{
                                temp.setStyle("-fx-background-color: transparent");
                                System.out.println("I'm button with id: "+temp.getId()+"\n You unselected me!");
                        }
                        
                     });
                    
                    if(i>=0 && i <=2){
                        gridPaneArchives.add(temp, i, 0);
                    }else if(i > 2 && i<=5){
                         gridPaneArchives.add(temp, (i-3), 1);
                    }else if(i >5 && i<=8){
                         gridPaneArchives.add(temp, (i-6), 2);
                    }else if(i >8 && i < 12){
                         gridPaneArchives.add(temp, (i-9), 3);
                    }else{
                       //i > 11 outside of grid error
                       System.out.println("To many archives, archives outside of grid");
                    }
      
                    i++;
                }  
            } catch (FileNotFoundException e) {
                    Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, e);
            } catch (IOException e) {
                    Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, e);
            } catch (ParseException e) {
                    Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, e);
            }
        }
        else {
            //nuk gjendet file. Kerko nga SOAP Server dhe krijo file config.txt
             //set class to search from soap server
            ArchivesPortService archiveService = new ArchivesPortService();
            ArchivesPort archivesPort = archiveService.getArchivesPortSoap11();
  
            GetAllArchiveRequest allArchivesRequest = new GetAllArchiveRequest();
            GetAllArchiveResponse allArchivesResponse = archivesPort.getAllArchive(allArchivesRequest);
                    
             List<Archive> allArchives = allArchivesResponse.getAllArchiveResponse();
             JSONArray jsonArchives = new JSONArray();
             for (Archive archive : allArchives) {
                 //per gjithe arkivat qe gjejme do te krijojme file-in JSON 
                  JSONObject jsonArchive = new JSONObject();
                  jsonArchive.put("archiveName",archive.getArchiveName());
                  jsonArchive.put("password", archive.getPassword());  
                  
                  jsonArchives.add(jsonArchive);  
             }
              
            try (FileWriter file2 = new FileWriter("config.json") ) {
            file2.write(jsonArchives.toJSONString());
            file2.close();
            System.out.println("Successfully Copied JSON Object to File...");
            System.out.println("\nJSON Object: " + jsonArchives);
            //pasi krijohet file JSON, vazhdojme me shtimin e elementeve ne Grid. 
            //Duke qene se nuk funksiononte rithirrja e funksionit initialize do te shtojme kodin nga fillimi
                     JSONParser parser = new JSONParser();
                try {                       
                        JSONArray localArchives = (JSONArray) parser.parse(new FileReader("config.json"));
                        int i = 0;
                        for (Object objArchive : localArchives){   
                             JSONObject archive = (JSONObject) objArchive;
                             String archiveName = (String) archive.get("archiveName");
                             System.out.println(archiveName);  
                    
                             final ToggleButton temp = new ToggleButton(archiveName);
                             final ImageView toggleImage = new ImageView();
                             final Image image = new Image("/assets/change.png"); 
                             toggleImage.setImage(image);
                             toggleImage.setFitHeight(40);
                             toggleImage.setFitWidth(40);
                             temp.setGraphic(toggleImage);
                             temp.setId("ToggleButton"+i);
                             temp.setContentDisplay(ContentDisplay.TOP);
                             temp.setPrefSize(135, 80);
                             temp.setWrapText(true);
                             temp.setAlignment(Pos.CENTER);
                             temp.setTextAlignment(TextAlignment.CENTER);
                             temp.setMaxHeight(Double.MAX_VALUE);
                             temp.setMaxWidth(Double.MAX_VALUE);
                             gridPaneArchives.setHgap(5); //horizontal gap in pixels => that's what you are asking for
                             gridPaneArchives.setVgap(5); //vertical gap in pixels
                             gridPaneArchives.setPadding(new Insets(5, 5, 5, 5));
                             anchorPaneCenter.setRightAnchor(gridPaneArchives, .0);
                             anchorPaneCenter.setTopAnchor(gridPaneArchives, .0);
                             anchorPaneCenter.setLeftAnchor(gridPaneArchives, .0);
                             AnchorPane.setBottomAnchor(gridPaneArchives, .0);
                             temp.setStyle("-fx-background-color: transparent");
                             temp.setOnAction((ActionEvent e) -> {
                         
                             if (temp.isSelected() == true) {
                                temp.setStyle("-fx-background-color: #3026d9");
                                System.out.println("I'm button with id: "+temp.getId()+"\n You selected me!");
                                
                             }      
                             else{
                                temp.setStyle("-fx-background-color: transparent");
                                System.out.println("I'm button with id: "+temp.getId()+"\n You unselected me!");
                             }
                        
                            });
                    
                            if(i>=0 && i <=2){
                                gridPaneArchives.add(temp, i, 0);
                            }else if(i > 2 && i<=5){
                                gridPaneArchives.add(temp, (i-3), 1);
                            }else if(i >5 && i<=8){
                                gridPaneArchives.add(temp, (i-6), 2);
                            }else if(i >8 && i < 12){
                                gridPaneArchives.add(temp, (i-9), 3);
                            }else{
                                //i > 11 outside of grid error
                                System.out.println("To many archives, archives outside of grid");
                            }
      
                            i++;
                        }  
            } catch (FileNotFoundException e) {
                    Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, e);
            } catch (IOException e) {
                    Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, e);
            } catch (ParseException e) {
                    Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, e);
            }
            
            
              } catch (IOException ex) {
                   Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, ex);
              }    
            
        }
         
    }    
 
    
   
}