package primanotafacileclient;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXTextArea;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import net.unnitech.primanotafacile.AddBankRequest;
import net.unnitech.primanotafacile.AddBankResponse;
import net.unnitech.primanotafacile.ArchivesPort;
import net.unnitech.primanotafacile.ArchivesPortService;
import net.unnitech.primanotafacile.Bank;
import net.unnitech.primanotafacile.UpdateBankRequest;
import net.unnitech.primanotafacile.UpdateBankResponse;

public class NewBank {
    
    @FXML
    private AnchorPane anchorPane;
    
    @FXML
    private JFXTextArea descrizione;
    
    @FXML
    private JFXButton btnSave;
    
    @FXML
    private JFXButton btnClose;
    
    @FXML
    private TextField bankName;
    
     @FXML
    private Label bankID;
    
    private Stage dialogStage;
    private Bank bank;
    private boolean okClicked = false;
    
    @FXML
    private void initialize() {
        anchorPane.requestFocus();
    }
    
    @FXML
    void handleRemoveFocus() {
        anchorPane.requestFocus();
    }
    public void setDialogStage(Stage dialogStage) {
        this.dialogStage = dialogStage;
        this.dialogStage.getScene().getStylesheets().add("assets/style/style.css");
    }
    
    public void setBank(Bank newBank) {
        
       
        this.bank = newBank;
        descrizione.setText(newBank.getDescription());
        bankName.setText(newBank.getName());
        bankID.setText(newBank.getBankId());
        anchorPane.requestFocus();
    }
    
    public boolean isOkClicked() {
        return okClicked;
    }
    
    private final Stage getDialogStage() {
        return dialogStage;
    }
    
    @FXML
    private void handleOk() {
        if (isInputValid()) {
            //handle state of edit or new by checking value of  textViewScadenzarioId
            if ("0".equals(bankID.getText())) {
                //no id add new
                //we're gonna add a new reminder
                //create a new reminer class and set the textfield values
                Bank newBank = new Bank();
                
                newBank.setDescription(descrizione.getText());
                newBank.setName(bankName.getText());
                 
                ArchivesPortService archiveService = new ArchivesPortService();
                ArchivesPort archivesPort = archiveService.getArchivesPortSoap11();
                AddBankRequest addBank = new AddBankRequest();
                addBank.setBank(newBank);
                addBank.setArchiveName(PrimaNotaFacileClient.getCurrentArchive() + ".sqlite3");
                AddBankResponse addBankResponse = archivesPort.addBank(addBank);
                String pergjige = addBankResponse.getResponse();
                if ("OK".equals(pergjige)) {
                    okClicked = true;
                    dialogStage.close();
                    
                } else {
                    Alert alert = new Alert(Alert.AlertType.ERROR);
                    alert.initOwner(dialogStage);
                    alert.setTitle("Errore!");
                    alert.setHeaderText("Errore!");
                    alert.setContentText("Errore nel tentativo di aggiungere una nuova banca!\\Si prega di provare di nuovo\\n Se il problema persiste si prega di contattare l'Amministratore dell sistema");
                    alert.showAndWait();
                }
            } else {
                //textViewScadenzarioID has value , we're in edit state
                //create the object reminder and set also the reminder id in order to update
                Bank existingBank = new Bank();
                existingBank.setBankId(bankID.getText());
                existingBank.setName(bankName.getText());
                existingBank.setDescription(descrizione.getText());
                existingBank.setDisabled("0");
                existingBank.setIdArchive("1");
                ArchivesPortService archiveService = new ArchivesPortService();
                ArchivesPort archivesPort = archiveService.getArchivesPortSoap11();
                UpdateBankRequest updateBankRequest = new UpdateBankRequest();
                updateBankRequest.setBank(existingBank);
                updateBankRequest.setArchiveName(PrimaNotaFacileClient.getCurrentArchive() + ".sqlite3");
                UpdateBankResponse updateBankRespone = archivesPort.updateBank(updateBankRequest);
                String pergjige = updateBankRespone.getResponse();
                if ("OK".equals(pergjige)) {
                    okClicked = true;
                    dialogStage.close();
                } else {
                    Alert alert = new Alert(Alert.AlertType.ERROR);
                    alert.initOwner(dialogStage);
                    alert.setTitle("Errore!");
                    alert.setHeaderText("Errore!");
                    alert.setContentText("Errore nel tentativo di modificare la banca!\\Si prega di provare di nuovo\\n Se il problema persiste si prega di contattare l'Amministratore dell sistema");
                    alert.showAndWait();
                }
            }
        }
    }
    
    @FXML
    private void handleCancel() {
        dialogStage.close();
    }
    
    private boolean isInputValid() {
        String errorMessage = "";
        
        if("".equals(bankName.getText()))
        {
            errorMessage += "Please set the bank name";
        }
        
        if(bankName.getText() == null)
        {
            errorMessage += "Please set the bank name";
        }
        
        if (errorMessage.length() == 0) {
            return true;
        } else {
            // Show the error message.
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.initOwner(dialogStage);
            alert.setTitle("Campi non validi");
            alert.setHeaderText("Si prega di correggere i campi non validi");
            Text text = new Text(errorMessage);
            text.setWrappingWidth(400);
            alert.getDialogPane().setContent(text);
            alert.showAndWait();
            
            return false;
        }
    }
    
}
