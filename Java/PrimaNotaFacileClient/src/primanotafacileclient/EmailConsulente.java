/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package primanotafacileclient;

/**
 *
 * @author programues
 * Class will be used for sending email to consultant. Email attachments will be XLSX files exported from table entries
 */
public class EmailConsulente {
    private String smtpServer;
    private String smtpPort;
    private String requireAuthentication;  //"true" or "false"
    private String enableTls; //"true" or "false"
    private String senderEmail;
    private String senderPassword;
    private String mailSubject;
    private String mailBody;
    private String[] attachedFiles;
    private String recipientEmail;

    /**
     * @return the smtpServer
     */
    public String getSmtpServer() {
        return smtpServer;
    }

    /**
     * @param smtpServer the smtpServer to set
     */
    public void setSmtpServer(String smtpServer) {
        this.smtpServer = smtpServer;
    }

    /**
     * @return the smtpPort
     */
    public String getSmtpPort() {
        return smtpPort;
    }

    /**
     * @param smtpPort the smtpPort to set
     */
    public void setSmtpPort(String smtpPort) {
        this.smtpPort = smtpPort;
    }

    /**
     * @return the requireAuthentication
     */
    public String getRequireAuthentication() {
        return requireAuthentication;
    }

    /**
     * @param requireAuthentication the requireAuthentication to set
     */
    public void setRequireAuthentication(String requireAuthentication) {
        this.requireAuthentication = requireAuthentication;
    }

    /**
     * @return the enableTls
     */
    public String getEnableTls() {
        return enableTls;
    }

    /**
     * @param enableTls the enableTls to set
     */
    public void setEnableTls(String enableTls) {
        this.enableTls = enableTls;
    }

    /**
     * @return the senderEmail
     */
    public String getSenderEmail() {
        return senderEmail;
    }

    /**
     * @param senderEmail the senderEmail to set
     */
    public void setSenderEmail(String senderEmail) {
        this.senderEmail = senderEmail;
    }

    /**
     * @return the senderPassword
     */
    public String getSenderPassword() {
        return senderPassword;
    }

    /**
     * @param senderPassword the senderPassword to set
     */
    public void setSenderPassword(String senderPassword) {
        this.senderPassword = senderPassword;
    }

    /**
     * @return the mailSubject
     */
    public String getMailSubject() {
        return mailSubject;
    }

    /**
     * @param mailSubject the mailSubject to set
     */
    public void setMailSubject(String mailSubject) {
        this.mailSubject = mailSubject;
    }

    /**
     * @return the mailBody
     */
    public String getMailBody() {
        return mailBody;
    }

    /**
     * @param mailBody the mailBody to set
     */
    public void setMailBody(String mailBody) {
        this.mailBody = mailBody;
    }

    /**
     * @return the attachedFiles
     */
    public String[] getAttachedFiles() {
        return attachedFiles;
    }

    /**
     * @param attachedFiles the attachedFiles to set
     */
    public void setAttachedFiles(String[] attachedFiles) {
        this.attachedFiles = attachedFiles;
    }

    /**
     * @return the recipientEmail
     */
    public String getRecipientEmail() {
        return recipientEmail;
    }

    /**
     * @param recipientEmail the recipientEmail to set
     */
    public void setRecipientEmail(String recipientEmail) {
        this.recipientEmail = recipientEmail;
    }

}
