
package primanotafacileclient;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


public final class PrimaNotaForTable {

    private final SimpleStringProperty tablePrimaNotaData = new SimpleStringProperty("");
    private final SimpleStringProperty tablePrimaNotaOperazione = new SimpleStringProperty("");
    private final SimpleStringProperty tablePrimaNotaFuoriCassa = new SimpleStringProperty("");
    private final SimpleStringProperty tablePrimaNotaCassaEntrate = new SimpleStringProperty("");
    private final SimpleStringProperty tablePrimaNotaCassaUscite = new SimpleStringProperty("");
    private final SimpleStringProperty tablePrimaNotaBancaEntrate = new SimpleStringProperty("");
    private final SimpleStringProperty tablePrimaNotaBancaUscite = new SimpleStringProperty("");
    private final SimpleStringProperty tablePrimaNotaSaldiEntrate = new SimpleStringProperty("");
    private final SimpleStringProperty tablePrimaNotaSaldiUscite = new SimpleStringProperty("");

    public PrimaNotaForTable() 
    {
         this("","","","","","","","","");
    }

    public PrimaNotaForTable( String tablePrimaNotaData, String tablePrimaNotaOperazione, String tablePrimaNotaFuoriCassa, String tablePrimaNotaCassaEntrate, String tablePrimaNotaCassaUscite, String tablePrimaNotaBancaEntrate, String tablePrimaNotaBancaUscite, String tablePrimaNotaSaldiEntrate, String tablePrimaNotaSaldiUscite) 
    {

        setTablePrimaNotaData(tablePrimaNotaData);
        setTablePrimaNotaOperazione(tablePrimaNotaOperazione);
        setTablePrimaNotaFuoriCassa(tablePrimaNotaFuoriCassa);
        setTablePrimaNotaCassaEntrate(tablePrimaNotaCassaEntrate);
        setTablePrimaNotaCassaUscite(tablePrimaNotaCassaUscite);
        setTablePrimaNotaBancaEntrate(tablePrimaNotaBancaEntrate);
        setTablePrimaNotaBancaUscite(tablePrimaNotaBancaUscite);
        setTablePrimaNotaSaldiEntrate(tablePrimaNotaSaldiEntrate);
        setTablePrimaNotaSaldiUscite(tablePrimaNotaSaldiUscite);
    }

    public String getTablePrimaNotaData() 
    {
        return tablePrimaNotaData.getValue();
    }

    public void setTablePrimaNotaData(String tablePrimaNotaData) 
    {
        this.tablePrimaNotaData.set(tablePrimaNotaData);
    }

    public String getTablePrimaNotaOperazione() 
    {
        return tablePrimaNotaOperazione.getValue();
    }

    public void setTablePrimaNotaOperazione(String tablePrimaNotaOperazione) 
    {
        this.tablePrimaNotaOperazione.set(tablePrimaNotaOperazione);
    }

    public String getTablePrimaNotaFuoriCassa()
    {
        return tablePrimaNotaFuoriCassa.getValue();
    }

    public void setTablePrimaNotaFuoriCassa(String tablePrimaNotaFuoriCassa)
    {
        this.tablePrimaNotaFuoriCassa.set(tablePrimaNotaFuoriCassa);
    }

    public String getTablePrimaNotaCassaEntrate()
    {
        return tablePrimaNotaCassaEntrate.getValue();
    }

    public void setTablePrimaNotaCassaEntrate(String tablePrimaNotaCassaEntrate)
    {
        this.tablePrimaNotaCassaEntrate.set(tablePrimaNotaCassaEntrate);
    }

    public String getTablePrimaNotaCassaUscite() 
    {
        return tablePrimaNotaCassaUscite.getValue();
    }

    public void setTablePrimaNotaCassaUscite(String tablePrimaNotaCassaUscite)
    {
        this.tablePrimaNotaCassaUscite.set(tablePrimaNotaCassaUscite);
    }

    public String getTablePrimaNotaBancaEntrate()
    {
        return tablePrimaNotaBancaEntrate.getValue();
    }

    public void setTablePrimaNotaBancaEntrate(String tablePrimaNotaBancaEntrate)
    {
        this.tablePrimaNotaBancaEntrate.set(tablePrimaNotaBancaEntrate);
    }

    public String getTablePrimaNotaBancaUscite()
    {
        return tablePrimaNotaBancaUscite.getValue();
    }

    public void setTablePrimaNotaBancaUscite(String tablePrimaNotaBancaUscite)
    {
        this.tablePrimaNotaBancaUscite.set(tablePrimaNotaBancaUscite);
    }

    public String getTablePrimaNotaSaldiEntrate()
    {
        return tablePrimaNotaSaldiEntrate.getValue();
    }

    public void setTablePrimaNotaSaldiEntrate(String tablePrimaNotaSaldiEntrate) 
    {
        this.tablePrimaNotaSaldiEntrate.set(tablePrimaNotaSaldiEntrate);
    }

    public String getTablePrimaNotaSaldiUscite()
    {
        return tablePrimaNotaSaldiUscite.getValue();
    }

    public void setTablePrimaNotaSaldiUscite(String tablePrimaNotaSaldiUscite) 
    {
        this.tablePrimaNotaSaldiUscite.set(tablePrimaNotaSaldiUscite);
    }
}