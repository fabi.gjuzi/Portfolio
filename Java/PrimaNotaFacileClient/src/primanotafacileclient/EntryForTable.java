/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package primanotafacileclient;

import javafx.beans.property.SimpleStringProperty;

/**
 *
 * @author programues
 */
public final class EntryForTable {
    
    public SimpleStringProperty tablePrimaNotaId = new SimpleStringProperty("");
    public SimpleStringProperty tablePrimaNotaControparte = new SimpleStringProperty("");
    public SimpleStringProperty tablePrimaNotaData = new SimpleStringProperty("");
    public SimpleStringProperty tablePrimaNotaOperazione = new SimpleStringProperty("");
    public SimpleStringProperty tablePrimaNotaFuoriCassa = new SimpleStringProperty("");
    public SimpleStringProperty tablePrimaNotaCassaEntrate = new SimpleStringProperty("");
    public SimpleStringProperty tablePrimaNotaCassaUscite = new SimpleStringProperty("");
    public SimpleStringProperty tablePrimaNotaBancaEntrate = new SimpleStringProperty("");
    public SimpleStringProperty tablePrimaNotaBancaUscite = new SimpleStringProperty("");
    public SimpleStringProperty tablePrimaNotaSaldiCassa = new SimpleStringProperty("");
    public SimpleStringProperty tablePrimaNotaSaldiBanca = new SimpleStringProperty("");
    public SimpleStringProperty tablePrimaNotaBankId = new SimpleStringProperty("");
    public SimpleStringProperty tablePrimaNotaContactId = new SimpleStringProperty("");
    public SimpleStringProperty tablePrimaNotaCategoryId = new SimpleStringProperty("");
   
    
    public EntryForTable() {
        this("", "", "", "", "", "", "", "", "", "","","","","");
    }
    
    public EntryForTable(String tablePrimaNotaId, String tablePrimaNotaControparte, String  tablePrimaNotaData, String tablePrimaNotaOperazione, String tablePrimaNotaFuoriCassa, String tablePrimaNotaCassaEntrate,
            String tablePrimaNotaCassaUscite, String tablePrimaNotaBancaEntrate, String tablePrimaNotaBancaUscite, String tablePrimaNotaSaldiCassa,
            String tablePrimaNotaSaldiBanca, String tablePrimaNotaBankId,String tablePrimaNotaContactId, String tablePrimaNotaCategoryId) {
        
        setTablePrimaNotaId(tablePrimaNotaId);
        setTablePrimaNotaControparte(tablePrimaNotaControparte);
        setTablePrimaNotaData(tablePrimaNotaData);
        setTablePrimaNotaOperazione(tablePrimaNotaOperazione);
        setTablePrimaNotaFuoriCassa(tablePrimaNotaFuoriCassa);
        setTablePrimaNotaCassaEntrate(tablePrimaNotaCassaEntrate);
        setTablePrimaNotaCassaUscite(tablePrimaNotaCassaUscite);
        setTablePrimaNotaBancaEntrate(tablePrimaNotaBancaEntrate);
        setTablePrimaNotaBancaUscite(tablePrimaNotaBancaUscite);
        setTablePrimaNotaSaldiCassa(tablePrimaNotaSaldiCassa);
        setTablePrimaNotaSaldiBanca(tablePrimaNotaSaldiBanca);
        setTablePrimaNotaBankId(tablePrimaNotaBankId);
        setTablePrimaNotaContactId(tablePrimaNotaContactId);
        setTablePrimaNotaCategoryId(tablePrimaNotaCategoryId);
    }

//    public EntryForTable(String entryDate, String string, String outCash, String string0, String string1, String incomingValue, String outgoingValue, String string2, String string3) {
//        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
//    }
    
    public String getTablePrimaNotaData() {
        return tablePrimaNotaData.getValue();
    }
    
    public void setTablePrimaNotaData(String tablePrimaNotaData) {
        this.tablePrimaNotaData.set(tablePrimaNotaData);
    }
    
    public String getTablePrimaNotaOperazione() {
        return tablePrimaNotaOperazione.getValue();
    }
    
    public void setTablePrimaNotaOperazione(String tablePrimaNotaOperazione) {
        this.tablePrimaNotaOperazione.set(tablePrimaNotaOperazione);
    }
    
    public String getTablePrimaNotaFuoriCassa() {
        return tablePrimaNotaFuoriCassa.getValue();
    }
    
    public void setTablePrimaNotaFuoriCassa(String tablePrimaNotaFuoriCassa) {
        this.tablePrimaNotaFuoriCassa.set(tablePrimaNotaFuoriCassa);
    }
    
    public String getTablePrimaNotaCassaEntrate() {
        return tablePrimaNotaCassaEntrate.getValue();
    }
    
    public void setTablePrimaNotaCassaEntrate(String tablePrimaNotaCassaEntrate) {
        this.tablePrimaNotaCassaEntrate.set(tablePrimaNotaCassaEntrate);
    }
    
    public String getTablePrimaNotaCassaUscite() {
        return tablePrimaNotaCassaUscite.getValue();
    }
    
    public void setTablePrimaNotaCassaUscite(String tablePrimaNotaCassaUscite) {
        this.tablePrimaNotaCassaUscite.set(tablePrimaNotaCassaUscite);
    }
    
    public String getTablePrimaNotaBancaEntrate() {
        return tablePrimaNotaBancaEntrate.getValue();
    }
    
    public void setTablePrimaNotaBancaEntrate(String tablePrimaNotaBancaEntrate) {
        this.tablePrimaNotaBancaEntrate.set(tablePrimaNotaBancaEntrate);
    }
    
    public String getTablePrimaNotaBancaUscite() {
        return tablePrimaNotaBancaUscite.getValue();
    }
    
    public void setTablePrimaNotaBancaUscite(String tablePrimaNotaBancaUscite) {
        this.tablePrimaNotaBancaUscite.set(tablePrimaNotaBancaUscite);
    }
    
    public String getTablePrimaNotaSaldiCassa() {
        return tablePrimaNotaSaldiCassa.getValue();
    }
    
    public void setTablePrimaNotaSaldiCassa(String tablePrimaNotaSaldiCassa) {
        this.tablePrimaNotaSaldiCassa.set(tablePrimaNotaSaldiCassa);
    }
    
    public String getTablePrimaNotaSaldiBanca() {
        return tablePrimaNotaSaldiBanca.getValue();
    }
    
    public void setTablePrimaNotaSaldiBanca(String tablePrimaNotaSaldiBanca) {
        this.tablePrimaNotaSaldiBanca.set(tablePrimaNotaSaldiBanca);
    }
    
    public String getTablePrimaNotaId() {
        return tablePrimaNotaId.getValue();
    }
    
    public void setTablePrimaNotaId(String tablePrimaNotaId) {
        this.tablePrimaNotaId.set(tablePrimaNotaId);
    }
    public String getTablePrimaNotaControparte() {
        return tablePrimaNotaControparte.getValue();
    }
    
    public void setTablePrimaNotaControparte(String tablePrimaNotaControparte) {
        this.tablePrimaNotaControparte.set(tablePrimaNotaControparte);
    }
    
     public String getTablePrimaNotaBankId() {
        return tablePrimaNotaBankId.getValue();
    }
    
    public void setTablePrimaNotaBankId(String tablePrimaNotaBankId) {
        this.tablePrimaNotaBankId.set(tablePrimaNotaBankId);
    }
    
     public String getTablePrimaNotaContactId() {
        return tablePrimaNotaContactId.getValue();
    }
    
    public void setTablePrimaNotaContactId(String tablePrimaNotaContactId) {
        this.tablePrimaNotaContactId.set(tablePrimaNotaContactId);
    }
    
     public String getTablePrimaNotaCategoryId() {
        return tablePrimaNotaCategoryId.getValue();
    }
    
    public void setTablePrimaNotaCategoryId(String tablePrimaNotaCategoryId) {
        this.tablePrimaNotaCategoryId.set(tablePrimaNotaCategoryId);
    }
}
