
package primanotafacileclient;

/**
 *
 * @author unnitech
 */

import javafx.stage.*;
import javafx.scene.*;
import javafx.scene.layout.*;
import javafx.scene.control.*;
import javafx.geometry.*;
import javafx.scene.effect.DropShadow;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;

public class InputConfirmBox {
    //Create variable
    static String archiveName;

    public static String display(String message) {
        Stage window = new Stage();
        window.initModality(Modality.APPLICATION_MODAL);
        window.setTitle("Aggiungi nuovo archivio");
       // window.setMinHeight(150);
        //window.setMinWidth(200);
        window.setResizable(false);

        Label label = new Label();
        label.setText(message);
        
        //create text field
        TextField archiveNameTextField = new TextField();
       
        //Create two buttons
        Button okButton = new Button("Ok");
        Button cancelButton = new Button("Cancel");
        
        //okButton.(cancelButton.getMinWidth());
        
        DropShadow shadow = new DropShadow();
        //Adding the shadow when the mouse cursor is on
        okButton.addEventHandler(MouseEvent.MOUSE_ENTERED, (MouseEvent e) -> {
        okButton.setEffect(shadow);
        });
 
        //Removing the shadow when the mouse cursor is off
        okButton.addEventHandler(MouseEvent.MOUSE_EXITED, (MouseEvent e) -> {
        okButton.setEffect(null);
        });
        //Adding the shadow when the mouse cursor is on
        cancelButton.addEventHandler(MouseEvent.MOUSE_ENTERED, (MouseEvent e) -> {
        cancelButton.setEffect(shadow);
        });
 
        //Removing the shadow when the mouse cursor is off
        cancelButton.addEventHandler(MouseEvent.MOUSE_EXITED, (MouseEvent e) -> {
        cancelButton.setEffect(null);
        });
        
        //warning label
        Label warningLabel = new Label();

        //Clicking will set answer and close window
        okButton.setOnAction(e -> {
           if (archiveNameTextField.getText() == null || archiveNameTextField.getText().trim().isEmpty()) {
                warningLabel.setText("(*) Nome dell'archivio vuoto!");
                warningLabel.setTextFill(Color.RED);
            }else{
               archiveName = archiveNameTextField.getText();
               window.close();
           }  
        });
        cancelButton.setOnAction(e -> {
            archiveName = null;
            window.close();
        });

       
        VBox layout = new VBox(10);
        layout.setPadding(new Insets(10,10,10,10));
        
        HBox buttons = new HBox(10);
        
        buttons.setAlignment(Pos.CENTER);
        buttons.setHgrow(okButton, Priority.ALWAYS);
        buttons.setHgrow(cancelButton, Priority.ALWAYS);
        okButton.setMaxWidth(Double.MAX_VALUE);
        cancelButton.setMaxWidth(Double.MAX_VALUE);
        buttons.getChildren().addAll(okButton, cancelButton);
       
        //Add buttons
        layout.getChildren().addAll(label, archiveNameTextField,buttons,warningLabel);
        layout.setAlignment(Pos.CENTER);
        Scene scene = new Scene(layout);
        window.setScene(scene);
        window.showAndWait();

        //Make sure to return answer
        return archiveName;
    }

}
