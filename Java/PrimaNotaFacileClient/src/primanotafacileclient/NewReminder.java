/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package primanotafacileclient;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXCheckBox;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXDatePicker;
import com.jfoenix.controls.JFXTextArea;
import com.jfoenix.controls.JFXTextField;
import java.io.IOException;
import java.text.NumberFormat;
import java.text.ParseException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.image.ImageView;
import javafx.scene.text.Text;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.util.StringConverter;
import net.unnitech.primanotafacile.AddReminderRequest;
import net.unnitech.primanotafacile.AddReminderResponse;
import net.unnitech.primanotafacile.ArchivesPort;
import net.unnitech.primanotafacile.ArchivesPortService;
import net.unnitech.primanotafacile.Contact;
import net.unnitech.primanotafacile.GetAllContactsRequest;
import net.unnitech.primanotafacile.GetAllContactsResponse;
import net.unnitech.primanotafacile.Reminder;
import net.unnitech.primanotafacile.UpdateReminderRequest;
import net.unnitech.primanotafacile.UpdateReminderResponse;

/**
 *
 * @author programues
 */
public class NewReminder {

    @FXML
    private JFXDatePicker btnNewScadenzarioData;

    @FXML
    private JFXTextArea btnNewScadenzarioDescrizione;

    @FXML
    private JFXTextField btnNewScadenzarioImporto;

    @FXML
    private JFXCheckBox chBoxNewScadenzarioCompletato;

    @FXML
    private JFXButton btnSave;

    @FXML
    private JFXButton btnClose;

    @FXML
    private JFXTextField textViewScadenzarioId;

    @FXML
    private JFXComboBox<String> comboNewScadenzarioControparte;

    @FXML
    private ImageView addNewContact;

    private Stage dialogStage;
    private ReminderForTable reminderForTable;
    private Reminder reminder;
    private boolean okClicked = false;

    private LinkedHashMap<String, Integer> comboContact = new LinkedHashMap<>();

    /**
     * Initializes the controller class. This method is automatically called
     * after the fxml file has been loaded.
     */
    @FXML
    private void initialize() {

        btnNewScadenzarioData.setConverter(new StringConverter<LocalDate>() {
            String pattern = "dd-MM-yyyy";
            DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern(pattern);

            {
                btnNewScadenzarioData.setPromptText(pattern.toLowerCase());
            }

            @Override
            public String toString(LocalDate date) {
                if (date != null) {
                    return dateFormatter.format(date);
                } else {
                    return "";
                }
            }

            @Override
            public LocalDate fromString(String string) {
                if (string != null && !string.isEmpty()) {
                    return LocalDate.parse(string, dateFormatter);
                } else {
                    return null;
                }
            }
        });

        //on initialize populate combobox with ids from database
        //set class to search from soap server
        ArchivesPortService archiveService = new ArchivesPortService();
        ArchivesPort archivesPort = archiveService.getArchivesPortSoap11();
        GetAllContactsRequest allContactsRequest = new GetAllContactsRequest();
        allContactsRequest.setArchiveName(PrimaNotaFacileClient.getCurrentArchive() + ".sqlite3");
        GetAllContactsResponse allContactsResponse = archivesPort.getAllContacts(allContactsRequest);

        //si pergjigje marrim nje liste me contacte
        List<Contact> allcontacs = allContactsResponse.getAllContactsResponse();
        ObservableList<String> comboContacts = comboNewScadenzarioControparte.getItems();
        comboContacts.remove(0, comboContacts.size());
        for (Contact contact : allcontacs) {
            comboContacts.add(contact.getFullName());
            comboContact.put(contact.getFullName(), Integer.parseInt(contact.getContactId()));
        }

    }

    /**
     * Sets the stage of this dialog.
     *
     * @param dialogStage
     */
    public void setDialogStage(Stage dialogStage) {
        this.dialogStage = dialogStage;
        this.dialogStage.getScene().getStylesheets().add("assets/style/style.css");
    }

    private final Stage getDialogStage() {
        return dialogStage;
    }

    @FXML
    public boolean showContactEditDialog(Contact contact) {
        try {

            // Load the fxml file and create a new stage for the popup dialog.
            FXMLLoader loader = new FXMLLoader(getClass().getResource("NewContact.fxml"));
            Parent root2 = (Parent) loader.load();

            // Create the dialog Stage.
            Stage dialogStage = new Stage();
            dialogStage.setTitle("Contact");
            dialogStage.initModality(Modality.APPLICATION_MODAL);
            dialogStage.initOwner(addNewContact.getScene().getWindow());
            Scene scene = new Scene(root2);
            dialogStage.setScene(scene);

            // Set the person into the controller.
            NewContactController controller = loader.getController();
            controller.setDialogStage(dialogStage);
            controller.setContact(contact);

            // Show the dialog and wait until the user closes it
            dialogStage.showAndWait();
            initialize();
            return controller.isOkClicked();
        } catch (IOException e) {
            System.out.println(e.getMessage());
            return false;
        }
    }

    /**
     * Sets the Reminder to be edited in the dialog.
     *
     * @param reminder
     */
    public void setReminder(Reminder reminder) throws ParseException {
        this.reminder = reminder;
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");
        //logic to check if we're in new or edit mode
        if ("0".equals(reminder.getReminderId())) {
            //new mode
            //we're gonna set just the hidden field reminder id to 0
            textViewScadenzarioId.setText(reminder.getReminderId());
            //also setting importo to 0.00
            btnNewScadenzarioImporto.setText("0.00");
            //also setting date to today
            LocalDate today = LocalDate.now();
            btnNewScadenzarioData.setValue(today);
            
            btnNewScadenzarioData.setPromptText(today.format(formatter));
            
        } else {
            //edit mode
            
            //CharSequence cs = reminder.getDate();
            if (reminder.getDate() != null) {
                LocalDate date = LocalDate.parse(reminder.getDate(), formatter);

                btnNewScadenzarioData.setValue(date);
            }
            btnNewScadenzarioData.setPromptText(reminder.getDate());
            //terator iterator = comboContact.entrySet().iterator();
            int n = 0;
            for (Map.Entry<String, Integer> entry : comboContact.entrySet()) {
                //since combo box select index starts from 0 we're gonna increment n after checking for specified value in hashmap
                //in the main controller we display in table the full name of the contact so we're searching in this hashmap for key
                if (entry.getKey().equals(reminder.getIdContact())) {

                    break;
                }

                n++;
            }

            //combo starts index from 0, hence the first element has index 0
            comboNewScadenzarioControparte.getSelectionModel().select(n);
            btnNewScadenzarioDescrizione.setText(reminder.getDescription());
            btnNewScadenzarioImporto.setText(NumberFormat.getCurrencyInstance(Locale.ITALY).parse(reminder.getValue()).toString());
            if ("1".equals(reminder.getCompleted())) {
                chBoxNewScadenzarioCompletato.setSelected(true);
            } else {
                chBoxNewScadenzarioCompletato.setSelected(false);
            }
            //set id if in edit state
            textViewScadenzarioId.setText(reminder.getReminderId());
        }
    }

    /**
     * Returns true if the user clicked OK, false otherwise.
     *
     * @return
     */
    public boolean isOkClicked() {
        return okClicked;
    }

    /**
     * Called when the user clicks newContact image
     */
    @FXML
    private void handleNewContact() {
        Contact newContact = new Contact();
        //set the reminder id to 0 to know that's new reminder
        newContact.setContactId("0");
        boolean okClicked = showContactEditDialog(newContact);
        if (okClicked) {
            //if result true refresh table 
            //or ADD an info  if you want
        }
    }

    /**
     * Called when the user clicks ok.
     */
    @FXML
    private void handleOk() {
        if (isInputValid()) {
            //handle state of edit or new by checking value of  textViewScadenzarioId
            if ("0".equals(textViewScadenzarioId.getText())) {
                //no id add new
                //we're gonna add a new reminder
                //create a new reminer class and set the textfield values
                Reminder newReminder = new Reminder();
                if (chBoxNewScadenzarioCompletato.isSelected()) {
                    newReminder.setCompleted("1");
                } else {
                    newReminder.setCompleted("0");
                }

                System.out.println((btnNewScadenzarioData.getValue().getDayOfMonth()) + "-" + (btnNewScadenzarioData.getValue().getMonthValue())
                        + "-" + (btnNewScadenzarioData.getValue().getYear()));
                int ditet = (btnNewScadenzarioData.getValue().getDayOfMonth());
                int length = (int) (Math.log10(ditet) + 1);
                int muaji = (btnNewScadenzarioData.getValue().getMonthValue());
                int lengthMonth = (int) (Math.log10(muaji) + 1);
                String ditaNeDate = Integer.toString(btnNewScadenzarioData.getValue().getDayOfMonth());
                String muajiNeDate = Integer.toString(btnNewScadenzarioData.getValue().getMonthValue());
                if (length == 1) {
                    ditaNeDate = "0" + btnNewScadenzarioData.getValue().getDayOfMonth();
                }
                if (lengthMonth == 1) {
                    muajiNeDate = "0" + btnNewScadenzarioData.getValue().getMonthValue();
                }

                newReminder.setDate((btnNewScadenzarioData.getValue().getYear()) + "-" + (muajiNeDate) + "-" + (ditaNeDate) + " 00:00:00");
                newReminder.setDescription(btnNewScadenzarioDescrizione.getText());
                //System.out.println("IDja "+idContact);
                newReminder.setIdContact(String.valueOf(comboContact.get(comboNewScadenzarioControparte.getSelectionModel().getSelectedItem())));
                newReminder.setValue(btnNewScadenzarioImporto.getText());

                //set class to search from soap server
                ArchivesPortService archiveService = new ArchivesPortService();
                ArchivesPort archivesPort = archiveService.getArchivesPortSoap11();
                AddReminderRequest addReminder = new AddReminderRequest();
                addReminder.setReminder(newReminder);
                addReminder.setArchiveName(PrimaNotaFacileClient.getCurrentArchive() + ".sqlite3");
                AddReminderResponse addReminderResponse = archivesPort.addReminder(addReminder);
                String pergjige = addReminderResponse.getResponse();
                if ("OK".equals(pergjige)) {
                    okClicked = true;
                    dialogStage.close();
                } else {
                    Alert alert = new Alert(AlertType.ERROR);
                    alert.initOwner(dialogStage);
                    alert.setTitle("Errore!");
                    alert.setHeaderText("Errore!");
                    alert.setContentText("Errore nel tentativo di aggiungere un nuovo scadenzario!\\Si prega di provare di nuovo\\n Se il problema persiste si prega di contattare l'Amministratore dell sistema");
                    alert.showAndWait();
                }
            } else {
                //textViewScadenzarioID has value , we're in edit state
                //create the object reminder and set also the reminder id in order to update
                Reminder existingReminder = new Reminder();
                if (chBoxNewScadenzarioCompletato.isSelected()) {
                    existingReminder.setCompleted("1");
                } else {
                    existingReminder.setCompleted("0");
                }
                System.out.println(btnNewScadenzarioData.getValue());
                System.out.println(btnNewScadenzarioData.getValue().toString());
                int ditet = (btnNewScadenzarioData.getValue().getDayOfMonth());
                int length = (int) (Math.log10(ditet) + 1);
                int muaji = (btnNewScadenzarioData.getValue().getMonthValue());
                int lengthMonth = (int) (Math.log10(muaji) + 1);
                String ditaNeDate = Integer.toString(btnNewScadenzarioData.getValue().getDayOfMonth());
                String muajiNeDate = Integer.toString(btnNewScadenzarioData.getValue().getMonthValue());
                if (length == 1) {
                    ditaNeDate = "0" + btnNewScadenzarioData.getValue().getDayOfMonth();
                }
                if (lengthMonth == 1) {
                    muajiNeDate = "0" + btnNewScadenzarioData.getValue().getMonthValue();
                }

                existingReminder.setDate((btnNewScadenzarioData.getValue().getYear()) + "-" + (muajiNeDate) + "-" + (ditaNeDate) + " 00:00:00");
                existingReminder.setDescription(btnNewScadenzarioDescrizione.getText());
                existingReminder.setIdContact(String.valueOf(comboContact.get(comboNewScadenzarioControparte.getSelectionModel().getSelectedItem())));
                existingReminder.setValue(btnNewScadenzarioImporto.getText());
                existingReminder.setReminderId(textViewScadenzarioId.getText());

                //set class to search from soap server
                ArchivesPortService archiveService = new ArchivesPortService();
                ArchivesPort archivesPort = archiveService.getArchivesPortSoap11();
                UpdateReminderRequest updateReminder = new UpdateReminderRequest();
                updateReminder.setReminder(existingReminder);
                updateReminder.setArchiveName(PrimaNotaFacileClient.getCurrentArchive() + ".sqlite3");
                UpdateReminderResponse updateReminderResponse = archivesPort.updateReminder(updateReminder);
                String pergjige = updateReminderResponse.getResponse();
                if ("OK".equals(pergjige)) {
                    okClicked = true;
                    dialogStage.close();
                } else {
                    Alert alert = new Alert(AlertType.ERROR);
                    alert.initOwner(dialogStage);
                    alert.setTitle("Errore!");
                    alert.setHeaderText("Errore!");
                    alert.setContentText("Errore nel tentativo di modificare il scadenzario!\\Si prega di provare di nuovo\\n Se il problema persiste si prega di contattare l'Amministratore dell sistema");
                    alert.showAndWait();
                }
            }
        }
    }

    /**
     * Called when the user clicks cancel.
     */
    @FXML
    private void handleCancel() {
        dialogStage.close();
    }

    /**
     * Validates the user input in the text fields.
     *
     * @return true if the input is valid
     */
    private boolean isInputValid() {
        String errorMessage = "";
        //TO DO:
        // Validate input fields needed
        //
        if (btnNewScadenzarioData.getValue() == null) {
            errorMessage += "Please set the date at the right format: dd-mm-yyyy";
        }

        if (comboNewScadenzarioControparte.getSelectionModel().getSelectedItem() == null) {
            errorMessage += "Si prega di selezionare una voce nell'elenco dei contatti\n";
        }
        //check if importo is a double value
        try{
           Double importValue =  Double.parseDouble(btnNewScadenzarioImporto.getText());
        }
        catch(NumberFormatException nr){
              errorMessage +="Si prega di inserire un valore valido per il campo di importo \n";
        }
        catch(NullPointerException e){
            errorMessage +="Si prega di inserire un valore per il campo di importo \n";
        }
        if(btnNewScadenzarioImporto.getText().contains(",")   ){
        
        }
        if (errorMessage.length() == 0) {
            return true;
        } else {
            // Show the error message.
            Alert alert = new Alert(AlertType.ERROR);
            alert.initOwner(dialogStage);
            alert.setTitle("Campi non validi");
            alert.setHeaderText("Si prega di correggere i campi non validi");
            Text text = new Text(errorMessage);
            text.setWrappingWidth(400);
            alert.getDialogPane().setContent(text);
            alert.showAndWait();

            return false;
        }
    }

}
