/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package primanotafacileclient;

import java.io.IOException;
import java.text.ParseException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Modality;

import javafx.stage.Stage;
import net.unnitech.primanotafacile.Reminder;

/**
 *
 * @author unnitech
 */
public class PrimaNotaFacileClient extends Application {

    /*
      Percaktojme nje variabel globale qe mban URL e webservice
     */
    private static String customWsUrl; //"http://localhost:8080/ws/archives.wsdl";
    
    public static String getCustomWsUrl(){
        return customWsUrl;
    }
    
    public static void setCustomWsUrl(String url){
        customWsUrl = url;
    }
    
    //variabel per gjendjen remote or local
    private static boolean isRemote;
    
    public static boolean getIsRemote(){
        return isRemote;
    }
    
    public static void setIsRemote(Boolean remote){
        isRemote = remote;
    }
    /*
      Percaktojme nje variabel globale qe mban emrin e arkives ne perdorim
     */
    private static String currentArchive;
    private static String currentReminder;
    private static String currentEntry;

    public static String getCurrentArchive() {
        return currentArchive;
    }

    public static void setCurrentArchive(String archieve) {
        currentArchive = archieve;
    }

    public static String getCurrentReminder() {
        return currentReminder;
    }

    public static void setCurrentReminder(String reminder) {
        currentReminder = reminder;
    }
    
    public static String getCurrentEntry() {
        return currentEntry;
    }

    public static void setCurrentEntry(String entry) {
        currentEntry = entry;
    }
    /*
    @Override
    public void start(Stage stage) throws Exception {
      
        Parent root = FXMLLoader.load(getClass().getResource("InitialScreen.fxml"));
        Scene scene = new Scene(root,1024,768);
        stage.setTitle("PrimaNotaFacile");
        stage.setScene(scene);
        stage.show();
       
      
        
        
    }
     */

    //per te menaxhuar kalimin nga nje dritare ne tjetren do perdorim kodin e gjetur ne stackoverflow
    private Stage stage;

    private static PrimaNotaFacileClient instance;

    public PrimaNotaFacileClient() {
        instance = this;
    }

    public static PrimaNotaFacileClient getInstance() {
        return instance;
    }

    @Override
    public void start(Stage primaryStage) {
        try {
            stage = primaryStage;
            goToArchiveSelector();
            primaryStage.show();
        } catch (Exception ex) {
            Logger.getLogger(PrimaNotaFacileClient.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public void goToMainScreen() {

        try {
            replaceSceneContent("MainScreen.fxml");
        } catch (Exception ex) {
            Logger.getLogger(PrimaNotaFacileClient.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public void goToArchiveSelector() {
        try {
            replaceSceneContent("InitialScreen.fxml");
        } catch (Exception ex) {
            Logger.getLogger(PrimaNotaFacileClient.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private Parent replaceSceneContent(String fxml) throws Exception {
        Parent page = FXMLLoader.load(PrimaNotaFacileClient.class.getResource(fxml));//, null, new JavaFXBuilderFactory());

        Scene scene = stage.getScene();
        
        if (scene == null) {
            //first time opening
            scene = new Scene(page, 500, 500);
            scene.getStylesheets().add("assets/style/style.css");
            stage.setScene(scene);
        } else {
            //ekranet e tjera
            stage.setWidth(1024);
            stage.setHeight(768);
            stage.getScene().setRoot(page);
            stage.getScene().getStylesheets().add("assets/style/style.css");
        }
        // stage.sizeToScene();
        return page;
    }

    public boolean showReminderEditDialog(Reminder reminder) throws ParseException {
        try {
            // Load the fxml file and create a new stage for the popup dialog.
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(PrimaNotaFacileClient.class.getResource("NewReminder.fxml"));

            AnchorPane page = (AnchorPane) loader.load();

            // Create the dialog Stage.
            Stage dialogStage = new Stage();
            dialogStage.setTitle("Scadenzario");
            dialogStage.initModality(Modality.WINDOW_MODAL);
            dialogStage.initOwner(stage);
            Scene scene = new Scene(page);
            dialogStage.setScene(scene);

            // Set the person into the controller.
            NewReminder controller = loader.getController();
            controller.setDialogStage(dialogStage);
            controller.setReminder(reminder);

            // Show the dialog and wait until the user closes it
            dialogStage.showAndWait();

            return controller.isOkClicked();
        } catch (IOException e) {
            return false;
        }
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }

}
