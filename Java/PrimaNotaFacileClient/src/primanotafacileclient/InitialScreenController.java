/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package primanotafacileclient;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TextInputDialog;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import net.unnitech.primanotafacile.*;
import org.apache.commons.io.FilenameUtils;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

/**
 * FXML Controller class
 *
 * @author unnitech
 */
public class InitialScreenController implements Initializable {

    @FXML
    private Pane mainPane;

    @FXML
    private TabPane tabPane;

    @FXML
    private Tab tabLocalArchives;

    @FXML
    private Pane paneLocalArchives;

    @FXML
    private ListView<String> listViewLocalArchives;

    @FXML
    private Tab tabRemoteArchives;

    @FXML
    private Pane paneRemoteArchives;

    @FXML
    private ListView<String> listViewRemoteArchives;

    @FXML
    private VBox vBoxButtonsRemoteArchives;

    @FXML
    private VBox vBoxCrudButtonsRemoteArchives;

    @FXML
    private VBox vBoxBackupButtonsRemoteArchives;

    @FXML
    private VBox vBoxButtons;

    @FXML
    private VBox vBoxCrudButtons;

    @FXML
    private Button btnOpenArchives;

    @FXML
    private Button btnNewArchives;

    @FXML
    private Button btnCopyArchives;

    @FXML
    private Button btnDeleteArchives;

    @FXML
    private Button btnRenameArchives;

    @FXML
    private VBox vBoxBackupButtons;

    @FXML
    private Button btnExportArchives;

    @FXML
    private Button btnImportArchives;

    @FXML
    void handleCopyAction(ActionEvent event) {
        //this method will create a new archive with _copy at the end
        //check if remote or local
        if(PrimaNotaFacileClient.getIsRemote()==true){
            //remote
            //check if we have selected an item from the listview
        if (PrimaNotaFacileClient.getCurrentArchive() != null) {
            //dialog with the user
            Alert alert = new Alert(AlertType.CONFIRMATION);
            alert.setTitle("Conferma azione");
            alert.setHeaderText("Conferma azione");
            alert.setContentText("Sei sicuro di voler copiare archivio " + PrimaNotaFacileClient.getCurrentArchive() + " ?");
            Optional<ButtonType> result = alert.showAndWait();
            if (result.get() == ButtonType.OK) {
                //set class to search from soap server
                ArchivesPortService archiveService = new ArchivesPortService();
                ArchivesPort archivesPort = archiveService.getArchivesPortSoap11();
                CopyArchiveRequest copyArchiveRequest = new CopyArchiveRequest();
                copyArchiveRequest.setArchiveName(PrimaNotaFacileClient.getCurrentArchive());//server method ads the extension _copy
                CopyArchiveResponse copyArchiveResponse = archivesPort.copyArchive(copyArchiveRequest);
                String pergjige = copyArchiveResponse.getResponse();
                if ("OK".equals(pergjige)) {
                    //add an element to JSON file with the name of the archive and "_copy" at the end
                    File f = new File("configRemote.json");
                    if (f.exists() && !f.isDirectory()) {
                        JSONParser parser = new JSONParser();
                        try {
                            JSONArray localArchives = (JSONArray) parser.parse(new FileReader(f));
                            JSONObject newJsonArchive = new JSONObject();
                            newJsonArchive.put("archiveName", PrimaNotaFacileClient.getCurrentArchive() + "_copy");
                            newJsonArchive.put("password", "");
                            localArchives.add(newJsonArchive);
                            try (FileWriter file2 = new FileWriter("configRemote.json")) {
                                file2.write(localArchives.toJSONString());
                                file2.close();
                                System.out.println("Successfully Copied JSON Object to File...");
                                System.out.println("\nJSON Object: " + localArchives);
                            } catch (IOException ex) {
                                Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, ex);
                            }

                        } catch (FileNotFoundException e) {
                            Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, e);
                        } catch (IOException e) {
                            Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, e);
                        } catch (ParseException e) {
                            Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, e);
                        }
                    }

                    //shtojme arkiven ne listview
                    remoteArchiveNames.add(PrimaNotaFacileClient.getCurrentArchive() + "_copy");
                    listViewRemoteArchives.setItems(remoteArchiveNames);

                    listViewRemoteArchives.setCellFactory(param -> new ListCell<String>() {
                        private ImageView imageViewArkiva = new ImageView();

                        @Override
                        public void updateItem(String name, boolean empty) {
                            super.updateItem(name, empty);
                            if (empty) {
                                setText(null);
                                setGraphic(null);
                            } else {
                                imageViewArkiva.setImage(archiveImage);
                                imageViewArkiva.setFitHeight(40);
                                imageViewArkiva.setFitWidth(40);
                                setText(name);
                                setGraphic(imageViewArkiva);
                            }
                        }
                    });
                } else {
                    //failed to delete archive
                    System.out.println("Error copying archive");
                }
            } else {
                //do nothing user canceled dialog
            }
        } else {
            Alert alert = new Alert(AlertType.WARNING);
            alert.setTitle("Attenzione");
            alert.setHeaderText("Nessun archivio selezionato!");
            alert.setContentText("Si prega di selezionare un archivio da eliminare");
            alert.showAndWait();
        }

        //clear selection 
        listViewRemoteArchives.getSelectionModel().clearSelection();
        
        }else{
            //local
            //check if we have selected an item from the listview
        if (PrimaNotaFacileClient.getCurrentArchive() != null) {
            //dialog with the user
            Alert alert = new Alert(AlertType.CONFIRMATION);
            alert.setTitle("Conferma azione");
            alert.setHeaderText("Conferma azione");
            alert.setContentText("Sei sicuro di voler copiare archivio " + PrimaNotaFacileClient.getCurrentArchive() + " ?");
            Optional<ButtonType> result = alert.showAndWait();
            if (result.get() == ButtonType.OK) {
                //set class to search from soap server
                ArchivesPortService archiveService = new ArchivesPortService();
                ArchivesPort archivesPort = archiveService.getArchivesPortSoap11();
                CopyArchiveRequest copyArchiveRequest = new CopyArchiveRequest();
                copyArchiveRequest.setArchiveName(PrimaNotaFacileClient.getCurrentArchive());//server method ads the extension _copy
                CopyArchiveResponse copyArchiveResponse = archivesPort.copyArchive(copyArchiveRequest);
                String pergjige = copyArchiveResponse.getResponse();
                if ("OK".equals(pergjige)) {
                    //add an element to JSON file with the name of the archive and "_copy" at the end
                    File f = new File("configLocal.json");
                    if (f.exists() && !f.isDirectory()) {
                        JSONParser parser = new JSONParser();
                        try {
                            JSONArray localArchives = (JSONArray) parser.parse(new FileReader(f));
                            JSONObject newJsonArchive = new JSONObject();
                            newJsonArchive.put("archiveName", PrimaNotaFacileClient.getCurrentArchive() + "_copy");
                            newJsonArchive.put("password", "");
                            localArchives.add(newJsonArchive);
                            try (FileWriter file2 = new FileWriter("configLocal.json")) {
                                file2.write(localArchives.toJSONString());
                                file2.close();
                                System.out.println("Successfully Copied JSON Object to File...");
                                System.out.println("\nJSON Object: " + localArchives);
                            } catch (IOException ex) {
                                Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, ex);
                            }

                        } catch (FileNotFoundException e) {
                            Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, e);
                        } catch (IOException e) {
                            Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, e);
                        } catch (ParseException e) {
                            Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, e);
                        }
                    }

                    //shtojme arkiven ne listview
                    localArchiveNames.add(PrimaNotaFacileClient.getCurrentArchive() + "_copy");
                    listViewLocalArchives.setItems(localArchiveNames);

                    listViewLocalArchives.setCellFactory(param -> new ListCell<String>() {
                        private ImageView imageViewArkiva = new ImageView();

                        @Override
                        public void updateItem(String name, boolean empty) {
                            super.updateItem(name, empty);
                            if (empty) {
                                setText(null);
                                setGraphic(null);
                            } else {
                                imageViewArkiva.setImage(archiveImage);
                                imageViewArkiva.setFitHeight(40);
                                imageViewArkiva.setFitWidth(40);
                                setText(name);
                                setGraphic(imageViewArkiva);
                            }
                        }
                    });
                } else {
                    //failed to delete archive
                    System.out.println("Error copying archive");
                }
            } else {
                //do nothing user canceled dialog
            }
        } else {
            Alert alert = new Alert(AlertType.WARNING);
            alert.setTitle("Attenzione");
            alert.setHeaderText("Nessun archivio selezionato!");
            alert.setContentText("Si prega di selezionare un archivio da eliminare");
            alert.showAndWait();
        }

        //clear selection 
        listViewLocalArchives.getSelectionModel().clearSelection();
        }
    }

    @FXML
    void handleDeleteAction(ActionEvent event) {
        //check if remote or local
        if(PrimaNotaFacileClient.getIsRemote()==true){
            //remote
            //check if we have selected an item from listview
        if (PrimaNotaFacileClient.getCurrentArchive() != null) {
            //dialog with the user
            Alert alert = new Alert(AlertType.CONFIRMATION);
            alert.setTitle("Conferma azione");
            alert.setHeaderText("Conferma azione");
            alert.setContentText("Sei sicuro di voler eliminare archivio " + PrimaNotaFacileClient.getCurrentArchive() + " ?");
            Optional<ButtonType> result = alert.showAndWait();
            if (result.get() == ButtonType.OK) {
                //set class to search from soap server
                ArchivesPortService archiveService = new ArchivesPortService();
                ArchivesPort archivesPort = archiveService.getArchivesPortSoap11();
                DeleteArchiveRequest deleteArchive = new DeleteArchiveRequest();
                deleteArchive.setArchiveName(PrimaNotaFacileClient.getCurrentArchive() + ".sqlite3");
                DeleteArchiveResponse deleteArchiveResponse = archivesPort.deleteArchive(deleteArchive);
                String pergjige = deleteArchiveResponse.getResponse();
                if ("OK".equals(pergjige)) {
                    //delete selected index from listview
                    remoteArchiveNames.remove(listViewRemoteArchives.getSelectionModel().getSelectedIndex());
                    listViewRemoteArchives.setItems(remoteArchiveNames);

                    //delete object from JSON config file
                    File f = new File("configRemote.json");
                    if (f.exists() && !f.isDirectory()) {
                        JSONParser parser = new JSONParser();
                        try {
                            JSONArray localArchives = (JSONArray) parser.parse(new FileReader(f));
                            int indexToRemove = 0;
                            for (int i = 0; i < localArchives.size(); i++) {
                                JSONObject jsonArchive = (JSONObject) localArchives.get(i);
                                String archiveName = (String) jsonArchive.get("archiveName");
                                if (archiveName.equals(PrimaNotaFacileClient.getCurrentArchive())) {
                                    indexToRemove = i;
                                }
                            }
                            localArchives.remove(indexToRemove);
                            try (FileWriter file2 = new FileWriter("configRemote.json")) {
                                file2.write(localArchives.toJSONString());
                                file2.close();
                                System.out.println("Successfully Copied JSON Object to File...");
                                System.out.println("\nJSON Object: " + localArchives);
                            } catch (IOException ex) {
                                Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, ex);
                            }

                        } catch (FileNotFoundException e) {
                            Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, e);
                        } catch (IOException e) {
                            Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, e);
                        } catch (ParseException e) {
                            Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, e);
                        }
                    }

                } else {
                    //failed to delete archive
                    System.out.println("Error delete archive");
                }
            } else {
                //do nothing
            }
        } else {
            Alert alert = new Alert(AlertType.WARNING);
            alert.setTitle("Attenzione");
            alert.setHeaderText("Nessun archivio selezionato!");
            alert.setContentText("Si prega di selezionare un archivio da eliminare");
            alert.showAndWait();
        }
        //clear selection 
        listViewRemoteArchives.getSelectionModel().clearSelection();
        PrimaNotaFacileClient.setCurrentArchive(null);
        }else{
            //local
            //check if we have selected an item from listview
        if (PrimaNotaFacileClient.getCurrentArchive() != null) {
            //dialog with the user
            Alert alert = new Alert(AlertType.CONFIRMATION);
            alert.setTitle("Conferma azione");
            alert.setHeaderText("Conferma azione");
            alert.setContentText("Sei sicuro di voler eliminare archivio " + PrimaNotaFacileClient.getCurrentArchive() + " ?");
            Optional<ButtonType> result = alert.showAndWait();
            if (result.get() == ButtonType.OK) {
                //set class to search from soap server
                ArchivesPortService archiveService = new ArchivesPortService();
                ArchivesPort archivesPort = archiveService.getArchivesPortSoap11();
                DeleteArchiveRequest deleteArchive = new DeleteArchiveRequest();
                deleteArchive.setArchiveName(PrimaNotaFacileClient.getCurrentArchive() + ".sqlite3");
                DeleteArchiveResponse deleteArchiveResponse = archivesPort.deleteArchive(deleteArchive);
                String pergjige = deleteArchiveResponse.getResponse();
                if ("OK".equals(pergjige)) {
                    //delete selected index from listview
                    localArchiveNames.remove(listViewLocalArchives.getSelectionModel().getSelectedIndex());
                    listViewLocalArchives.setItems(localArchiveNames);

                    //delete object from JSON config file
                    File f = new File("configLocal.json");
                    if (f.exists() && !f.isDirectory()) {
                        JSONParser parser = new JSONParser();
                        try {
                            JSONArray localArchives = (JSONArray) parser.parse(new FileReader(f));
                            int indexToRemove = 0;
                            for (int i = 0; i < localArchives.size(); i++) {
                                JSONObject jsonArchive = (JSONObject) localArchives.get(i);
                                String archiveName = (String) jsonArchive.get("archiveName");
                                if (archiveName.equals(PrimaNotaFacileClient.getCurrentArchive())) {
                                    indexToRemove = i;
                                }
                            }
                            localArchives.remove(indexToRemove);
                            try (FileWriter file2 = new FileWriter("configLocal.json")) {
                                file2.write(localArchives.toJSONString());
                                file2.close();
                                System.out.println("Successfully Copied JSON Object to File...");
                                System.out.println("\nJSON Object: " + localArchives);
                            } catch (IOException ex) {
                                Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, ex);
                            }

                        } catch (FileNotFoundException e) {
                            Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, e);
                        } catch (IOException e) {
                            Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, e);
                        } catch (ParseException e) {
                            Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, e);
                        }
                    }

                } else {
                    //failed to delete archive
                    System.out.println("Error delete archive");
                }
            } else {
                //do nothing
            }
        } else {
            Alert alert = new Alert(AlertType.WARNING);
            alert.setTitle("Attenzione");
            alert.setHeaderText("Nessun archivio selezionato!");
            alert.setContentText("Si prega di selezionare un archivio da eliminare");
            alert.showAndWait();
        }
        //clear selection 
        listViewLocalArchives.getSelectionModel().clearSelection();
        PrimaNotaFacileClient.setCurrentArchive(null);
        }

    }

    @FXML
    void handleMouseClicked(MouseEvent event) {
        if(PrimaNotaFacileClient.getIsRemote()==true){
           //remote
           PrimaNotaFacileClient.setCurrentArchive(listViewRemoteArchives.getSelectionModel().getSelectedItem());
           System.out.println("Vlera e selektuar e listview: " + PrimaNotaFacileClient.getCurrentArchive());
        }else{
           //local
            PrimaNotaFacileClient.setCurrentArchive(listViewLocalArchives.getSelectionModel().getSelectedItem());
            System.out.println("Vlera e selektuar e listview: " + PrimaNotaFacileClient.getCurrentArchive());
        }
       
    }

    @FXML
    void handleNewAction(ActionEvent event) {
        //check if remote or local
        if(PrimaNotaFacileClient.getIsRemote() == true){
           //remote
                //Shtimi i nje arkive te re do te konsistoje ne shtimin e emrit te file JSON si dhe thirrja me sukses e metodes SOAP server
                String archiveName = "";
                try {
                    //we're gonna use the official way for custom input box as described in: http://code.makery.ch/blog/javafx-dialogs-official/  
                    TextInputDialog dialog = new TextInputDialog("Nuovo Archivio");
                    dialog.setTitle("Nuovo Archivio");
                    dialog.setHeaderText("Nuovo Archivio");
                    dialog.setContentText("Scrivete il nome dell'archivio:");
                    Optional<String> result = dialog.showAndWait();
                    if (result.isPresent()) {
                        archiveName = result.get();
                        if ("".equals(archiveName)) {
                            //nuk eshte vendsour emer
                        } else {
                            //nese eshte vendosur nje emer archive
                            Archive archive = new Archive();
                            archive.setArchiveName(archiveName + ".sqlite3");
                            //set class to search from soap server
                            ArchivesPortService archiveService = new ArchivesPortService();
                            ArchivesPort archivesPort = archiveService.getArchivesPortSoap11();
                            AddArchiveRequest addArchive = new AddArchiveRequest();
                            addArchive.setArchive(archive);
                            AddArchiveResponse addArchiveResponse = archivesPort.addArchive(addArchive);
                            String pergjige = addArchiveResponse.getResponse();
                            if ("OK".equals(pergjige)) {
                                //arkiva u shtua
                                //shtojme elementin ne JSON
                                File f = new File("configRemote.json");
                                if (f.exists() && !f.isDirectory()) {
                                    JSONParser parser = new JSONParser();
                                    try {
                                        FileReader fileReader = new FileReader("configRemote.json");
                                        JSONArray localArchives = (JSONArray) parser.parse(fileReader);
                                        fileReader.close();
                                        //shtojme elementin e ri
                                        JSONObject newArchive = new JSONObject();
                                        newArchive.put("archiveName", FilenameUtils.removeExtension(archive.getArchiveName()));
                                        newArchive.put("password", "");
                                        //e sthojme ne array
                                        localArchives.add(newArchive);
                                        //shkruajme file-in
                                        try (FileWriter file = new FileWriter("configRemote.json")) {
                                            file.write(localArchives.toJSONString());
                                            System.out.println("Successfully Copied JSON Object to File...");
                                            System.out.println("\nJSON Object: " + localArchives);
                                            file.close();
                                            //shtojme arkiven ne listview
                                            remoteArchiveNames.add(FilenameUtils.removeExtension(archive.getArchiveName()));
                                            listViewRemoteArchives.setItems(remoteArchiveNames);

                                            listViewRemoteArchives.setCellFactory(param -> new ListCell<String>() {
                                                private ImageView imageViewArkiva = new ImageView();

                                                @Override
                                                public void updateItem(String name, boolean empty) {
                                                    super.updateItem(name, empty);
                                                    if (empty) {
                                                        setText(null);
                                                        setGraphic(null);
                                                    } else {
                                                        imageViewArkiva.setImage(archiveImage);
                                                        imageViewArkiva.setFitHeight(40);
                                                        imageViewArkiva.setFitWidth(40);
                                                        setText(name);
                                                        setGraphic(imageViewArkiva);
                                                    }
                                                }
                                            });

                                        } catch (IOException ex) {
                                            Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, ex);
                                        }

                                    } catch (FileNotFoundException e) {
                                        Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, e);
                                    } catch (IOException e) {
                                        Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, e);
                                    } catch (ParseException e) {
                                        Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, e);
                                    }

                                }
                            } else {
                                System.out.println("Gabim ne shtimin e arkives!");
                            }
                        }

                    }
                    //archiveName = InputConfirmBox.display("Si prega di scrivere un nome per il nuovo archivio");

                } catch (Exception ex) {
                    Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, ex);
                }
                //clear selection 
                listViewRemoteArchives.getSelectionModel().clearSelection();
                PrimaNotaFacileClient.setCurrentArchive(null);
        }else{
            //local
            //Shtimi i nje arkive te re do te konsistoje ne shtimin e emrit te file JSON si dhe thirrja me sukses e metodes SOAP server
        String archiveName = "";
        try {
            //we're gonna use the official way for custom input box as described in: http://code.makery.ch/blog/javafx-dialogs-official/  
            TextInputDialog dialog = new TextInputDialog("Nuovo Archivio");
            dialog.setTitle("Nuovo Archivio");
            dialog.setHeaderText("Nuovo Archivio");
            dialog.setContentText("Scrivete il nome dell'archivio:");
            Optional<String> result = dialog.showAndWait();
            if (result.isPresent()) {
                archiveName = result.get();
                if ("".equals(archiveName)) {
                    //nuk eshte vendsour emer
                } else {
                    //nese eshte vendosur nje emer archive
                    Archive archive = new Archive();
                    archive.setArchiveName(archiveName + ".sqlite3");
                    //set class to search from soap server
                    ArchivesPortService archiveService = new ArchivesPortService();
                    ArchivesPort archivesPort = archiveService.getArchivesPortSoap11();
                    AddArchiveRequest addArchive = new AddArchiveRequest();
                    addArchive.setArchive(archive);
                    AddArchiveResponse addArchiveResponse = archivesPort.addArchive(addArchive);
                    String pergjige = addArchiveResponse.getResponse();
                    if ("OK".equals(pergjige)) {
                        //arkiva u shtua
                        //shtojme elementin ne JSON
                        File f = new File("configLocal.json");
                        if (f.exists() && !f.isDirectory()) {
                            JSONParser parser = new JSONParser();
                            try {
                                FileReader fileReader = new FileReader("configLocal.json");
                                JSONArray localArchives = (JSONArray) parser.parse(fileReader);
                                fileReader.close();
                                //shtojme elementin e ri
                                JSONObject newArchive = new JSONObject();
                                newArchive.put("archiveName", FilenameUtils.removeExtension(archive.getArchiveName()));
                                newArchive.put("password", "");
                                //e sthojme ne array
                                localArchives.add(newArchive);
                                //shkruajme file-in
                                try (FileWriter file = new FileWriter("configLocal.json")) {
                                    file.write(localArchives.toJSONString());
                                    System.out.println("Successfully Copied JSON Object to File...");
                                    System.out.println("\nJSON Object: " + localArchives);
                                    file.close();
                                    //shtojme arkiven ne listview
                                    localArchiveNames.add(FilenameUtils.removeExtension(archive.getArchiveName()));
                                    listViewLocalArchives.setItems(localArchiveNames);

                                    listViewLocalArchives.setCellFactory(param -> new ListCell<String>() {
                                        private ImageView imageViewArkiva = new ImageView();

                                        @Override
                                        public void updateItem(String name, boolean empty) {
                                            super.updateItem(name, empty);
                                            if (empty) {
                                                setText(null);
                                                setGraphic(null);
                                            } else {
                                                imageViewArkiva.setImage(archiveImage);
                                                imageViewArkiva.setFitHeight(40);
                                                imageViewArkiva.setFitWidth(40);
                                                setText(name);
                                                setGraphic(imageViewArkiva);
                                            }
                                        }
                                    });

                                } catch (IOException ex) {
                                    Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, ex);
                                }

                            } catch (FileNotFoundException e) {
                                Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, e);
                            } catch (IOException e) {
                                Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, e);
                            } catch (ParseException e) {
                                Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, e);
                            }

                        }
                    } else {
                        System.out.println("Gabim ne shtimin e arkives!");
                    }
                }

            }
            //archiveName = InputConfirmBox.display("Si prega di scrivere un nome per il nuovo archivio");

        } catch (Exception ex) {
            Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, ex);
        }
        //clear selection 
        listViewLocalArchives.getSelectionModel().clearSelection();
        PrimaNotaFacileClient.setCurrentArchive(null);
        }

    }

    @FXML
    void handleOpenAction(ActionEvent event) throws IOException {
        //kontrollojme if ka selektuar nje arkive dhe shkojme ne Main Screen
        if (PrimaNotaFacileClient.getCurrentArchive() != null) {
            PrimaNotaFacileClient.getInstance().goToMainScreen();
        } else {
            Alert alert = new Alert(AlertType.WARNING);
            alert.setTitle("Attenzione");
            alert.setHeaderText("Nessun archivio selezionato!");
            alert.setContentText("Si prega di selezionare un archivio da aprire");
            alert.showAndWait();
        }

    }
    
    @FXML
    void handleRenameAction(ActionEvent event) {
        //check if we're remote or local
        
        if(PrimaNotaFacileClient.getIsRemote() == true){
            //remote
            //check if we've selected any archive
        if (PrimaNotaFacileClient.getCurrentArchive() != null) {
            //rename will take the old name, ask user for input of new name and call the rename method from soap server
            String newArchiveName = "";
            try {
                //we're gonna use the official way for custom input box as described in: http://code.makery.ch/blog/javafx-dialogs-official/  
                TextInputDialog dialog = new TextInputDialog(PrimaNotaFacileClient.getCurrentArchive());
                dialog.setTitle("Rinomina Archivio");
                dialog.setHeaderText("Rinomina Archivio");
                dialog.setContentText("Scrivete il nouvo nome dell'archivio:");
                Optional<String> result = dialog.showAndWait();
                if (result.isPresent()) {
                    newArchiveName = result.get();
                    if ("".equals(newArchiveName)) {
                        //nuk eshte vendsour emer
                        //do nothing
                    } else {
                        //nese eshte vendosur nje emer archive
                        //set class to search from soap server
                        ArchivesPortService archiveService = new ArchivesPortService();
                        ArchivesPort archivesPort = archiveService.getArchivesPortSoap11();
                        UpdateArchiveRequest updateArchiveRequest = new UpdateArchiveRequest();
                        updateArchiveRequest.setNewArchive(newArchiveName);
                        updateArchiveRequest.setOldArchive(PrimaNotaFacileClient.getCurrentArchive());
                        UpdateArchiveResponse updateArchiveResponse = archivesPort.updateArchive(updateArchiveRequest);
                        String pergjige = updateArchiveResponse.getResponse();
                        if ("OK".equals(pergjige)) {
                            //arkiva u rename
                            //duhet te update emrin ne file-in JSON
                            File f = new File("configRemote.json");
                            if (f.exists() && !f.isDirectory()) {
                                JSONParser parser = new JSONParser();
                                try {
                                    FileReader fileReader = new FileReader(f);
                                    JSONArray localArchives = (JSONArray) parser.parse(fileReader);
                                    fileReader.close();
                                    //gjejme indeksin ne array te objektit te vjeter
                                    int indexToUpdate = 0;
                                    for (int i = 0; i < localArchives.size(); i++) {
                                        JSONObject existentArchive = (JSONObject) localArchives.get(i);
                                        String archiveName = (String) existentArchive.get("archiveName");
                                        if (updateArchiveRequest.getOldArchive().equals(archiveName)) {
                                            indexToUpdate = i;
                                        }
                                    }
                                    JSONObject updatedArchive = new JSONObject();
                                    updatedArchive.put("archiveName", newArchiveName);
                                    updatedArchive.put("password", "");
                                    //modifikojme objektin ne indeksin e gjetur
                                    localArchives.set(indexToUpdate, updatedArchive);
                                    //shkruajme file-in
                                    try (FileWriter file = new FileWriter("configRemote.json")) {
                                        file.write(localArchives.toJSONString());
                                        System.out.println("Successfully Copied JSON Object to File...");
                                        System.out.println("\nJSON Object: " + localArchives);
                                        file.close();
                                        //duhet te perditesojme emrin ne listview
                                        //po rishkruajme gjithe listview qe te mos rrime per te kerkuar elementin ne listview te modifikuar
                                        int i = 0;
                                        for (Object objArchive : localArchives) {
                                            JSONObject archive = (JSONObject) objArchive;
                                            String archiveName = (String) archive.get("archiveName");
                                            System.out.println(archiveName);
                                            remoteArchiveNames.set(i, archiveName);
                                            i++;
                                        }

                                        listViewRemoteArchives.setItems(remoteArchiveNames);
                                        listViewRemoteArchives.setCellFactory(param -> new ListCell<String>() {
                                            private ImageView imageViewArkiva = new ImageView();

                                            @Override
                                            public void updateItem(String name, boolean empty) {
                                                super.updateItem(name, empty);
                                                if (empty) {
                                                    setText(null);
                                                    setGraphic(null);
                                                } else {
                                                    imageViewArkiva.setImage(archiveImage);
                                                    imageViewArkiva.setFitHeight(40);
                                                    imageViewArkiva.setFitWidth(40);
                                                    setText(name);
                                                    setGraphic(imageViewArkiva);
                                                }
                                            }
                                        });
                                    } catch (IOException ex) {
                                        Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, ex);
                                    }

                                } catch (FileNotFoundException e) {
                                    Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, e);
                                } catch (IOException e) {
                                    Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, e);
                                } catch (ParseException e) {
                                    Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, e);
                                }

                            }
                        } else {
                            System.out.println("Gabim ne updatetimin e arkives!");
                        }
                    }

                }

            } catch (Exception ex) {
                Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else {
            Alert alert = new Alert(AlertType.WARNING);
            alert.setTitle("Attenzione");
            alert.setHeaderText("Nessun archivio selezionato!");
            alert.setContentText("Si prega di selezionare un archivio da eliminare");
            alert.showAndWait();
        }
        //clear selection 
        listViewRemoteArchives.getSelectionModel().clearSelection();
        PrimaNotaFacileClient.setCurrentArchive(null);
        }else{
          //local
          //check if we've selected any archive
        if (PrimaNotaFacileClient.getCurrentArchive() != null) {
            //rename will take the old name, ask user for input of new name and call the rename method from soap server
            String newArchiveName = "";
            try {
                //we're gonna use the official way for custom input box as described in: http://code.makery.ch/blog/javafx-dialogs-official/  
                TextInputDialog dialog = new TextInputDialog(PrimaNotaFacileClient.getCurrentArchive());
                dialog.setTitle("Rinomina Archivio");
                dialog.setHeaderText("Rinomina Archivio");
                dialog.setContentText("Scrivete il nouvo nome dell'archivio:");
                Optional<String> result = dialog.showAndWait();
                if (result.isPresent()) {
                    newArchiveName = result.get();
                    if ("".equals(newArchiveName)) {
                        //nuk eshte vendsour emer
                        //do nothing
                    } else {
                        //nese eshte vendosur nje emer archive
                        //set class to search from soap server
                        ArchivesPortService archiveService = new ArchivesPortService();
                        ArchivesPort archivesPort = archiveService.getArchivesPortSoap11();
                        UpdateArchiveRequest updateArchiveRequest = new UpdateArchiveRequest();
                        updateArchiveRequest.setNewArchive(newArchiveName);
                        updateArchiveRequest.setOldArchive(PrimaNotaFacileClient.getCurrentArchive());
                        UpdateArchiveResponse updateArchiveResponse = archivesPort.updateArchive(updateArchiveRequest);
                        String pergjige = updateArchiveResponse.getResponse();
                        if ("OK".equals(pergjige)) {
                            //arkiva u rename
                            //duhet te update emrin ne file-in JSON
                            File f = new File("configLocal.json");
                            if (f.exists() && !f.isDirectory()) {
                                JSONParser parser = new JSONParser();
                                try {
                                    FileReader fileReader = new FileReader(f);
                                    JSONArray localArchives = (JSONArray) parser.parse(fileReader);
                                    fileReader.close();
                                    //gjejme indeksin ne array te objektit te vjeter
                                    int indexToUpdate = 0;
                                    for (int i = 0; i < localArchives.size(); i++) {
                                        JSONObject existentArchive = (JSONObject) localArchives.get(i);
                                        String archiveName = (String) existentArchive.get("archiveName");
                                        if (updateArchiveRequest.getOldArchive().equals(archiveName)) {
                                            indexToUpdate = i;
                                        }
                                    }
                                    JSONObject updatedArchive = new JSONObject();
                                    updatedArchive.put("archiveName", newArchiveName);
                                    updatedArchive.put("password", "");
                                    //modifikojme objektin ne indeksin e gjetur
                                    localArchives.set(indexToUpdate, updatedArchive);
                                    //shkruajme file-in
                                    try (FileWriter file = new FileWriter("configLocal.json")) {
                                        file.write(localArchives.toJSONString());
                                        System.out.println("Successfully Copied JSON Object to File...");
                                        System.out.println("\nJSON Object: " + localArchives);
                                        file.close();
                                        //duhet te perditesojme emrin ne listview
                                        //po rishkruajme gjithe listview qe te mos rrime per te kerkuar elementin ne listview te modifikuar
                                        int i = 0;
                                        for (Object objArchive : localArchives) {
                                            JSONObject archive = (JSONObject) objArchive;
                                            String archiveName = (String) archive.get("archiveName");
                                            System.out.println(archiveName);
                                            localArchiveNames.set(i, archiveName);
                                            i++;
                                        }

                                        listViewLocalArchives.setItems(localArchiveNames);
                                        listViewLocalArchives.setCellFactory(param -> new ListCell<String>() {
                                            private ImageView imageViewArkiva = new ImageView();

                                            @Override
                                            public void updateItem(String name, boolean empty) {
                                                super.updateItem(name, empty);
                                                if (empty) {
                                                    setText(null);
                                                    setGraphic(null);
                                                } else {
                                                    imageViewArkiva.setImage(archiveImage);
                                                    imageViewArkiva.setFitHeight(40);
                                                    imageViewArkiva.setFitWidth(40);
                                                    setText(name);
                                                    setGraphic(imageViewArkiva);
                                                }
                                            }
                                        });
                                    } catch (IOException ex) {
                                        Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, ex);
                                    }

                                } catch (FileNotFoundException e) {
                                    Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, e);
                                } catch (IOException e) {
                                    Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, e);
                                } catch (ParseException e) {
                                    Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, e);
                                }

                            }
                        } else {
                            System.out.println("Gabim ne updatetimin e arkives!");
                        }
                    }

                }

            } catch (Exception ex) {
                Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else {
            Alert alert = new Alert(AlertType.WARNING);
            alert.setTitle("Attenzione");
            alert.setHeaderText("Nessun archivio selezionato!");
            alert.setContentText("Si prega di selezionare un archivio da eliminare");
            alert.showAndWait();
        }
        //clear selection 
        listViewLocalArchives.getSelectionModel().clearSelection();
        PrimaNotaFacileClient.setCurrentArchive(null);
        }
    }

    private final Image archiveImage = new Image("/assets/change.png");
    final ObservableList<String> localArchiveNames = FXCollections.observableArrayList();
    final ObservableList<String> remoteArchiveNames = FXCollections.observableArrayList();

    @FXML
    public void handleLocalArchives(){
        File f = new File("configLocal.json");
        localArchiveNames.clear();
                if (f.exists() && !f.isDirectory()) {
                    JSONParser parser = new JSONParser();
                    try {
                        JSONArray localArchives = (JSONArray) parser.parse(new FileReader("configLocal.json"));
                        int i = 0;
                        for (Object objArchive : localArchives) {
                            JSONObject archive = (JSONObject) objArchive;
                            String archiveName = (String) archive.get("archiveName");
                            System.out.println(archiveName);
                            //localArchiveNames.set(i, archiveName);
                            localArchiveNames.add(archiveName);

                            i++;
                        }

                        listViewLocalArchives.setItems(localArchiveNames);

                        listViewLocalArchives.setCellFactory(param -> new ListCell<String>() {
                            private ImageView imageViewArkiva = new ImageView();

                            @Override
                            public void updateItem(String name, boolean empty) {
                                super.updateItem(name, empty);
                                if (empty) {
                                    setText(null);
                                    setGraphic(null);
                                } else {
                                    imageViewArkiva.setImage(archiveImage);
                                    imageViewArkiva.setFitHeight(40);
                                    imageViewArkiva.setFitWidth(40);
                                    setText(name);
                                    setGraphic(imageViewArkiva);
                                }
                            }
                        });

                    } catch (FileNotFoundException e) {
                        Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, e);
                    } catch (IOException e) {
                        Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, e);
                    } catch (ParseException e) {
                        Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, e);
                    }
                } else {
                    //nuk gjendet file. Kerko nga SOAP Server dhe krijo file config.txt
                    //set class to search from soap server
                    ArchivesPortService archiveService = new ArchivesPortService();
                    ArchivesPort archivesPort = archiveService.getArchivesPortSoap11();

                    GetAllArchiveRequest allArchivesRequest = new GetAllArchiveRequest();
                    GetAllArchiveResponse allArchivesResponse = archivesPort.getAllArchive(allArchivesRequest);

                    List<Archive> allArchives = allArchivesResponse.getAllArchiveResponse();
                    JSONArray jsonArchives = new JSONArray();
                    for (Archive archive : allArchives) {
                        //per gjithe arkivat qe gjejme do te krijojme file-in JSON 
                        JSONObject jsonArchive = new JSONObject();
                        jsonArchive.put("archiveName", archive.getArchiveName());
                        jsonArchive.put("password", archive.getPassword());
                        jsonArchives.add(jsonArchive);
                    }

                    try (FileWriter file2 = new FileWriter("configLocal.json")) {
                        file2.write(jsonArchives.toJSONString());
                        file2.close();
                        System.out.println("Successfully Copied JSON Object to File...");
                        System.out.println("\nJSON Object: " + jsonArchives);
                        //pasi krijohet file JSON, vazhdojme me shtimin e elementeve ne Grid. 
                        //get archives from JSon array created earlier
                            int i = 0;
                            for (Object objArchive : jsonArchives) {
                                JSONObject archive = (JSONObject) objArchive;
                                String archiveName = (String) archive.get("archiveName");
                                System.out.println(archiveName);
                                localArchiveNames.add(archiveName);
                                i++;
                            }

                            listViewLocalArchives.setItems(localArchiveNames);
                            listViewLocalArchives.setCellFactory(param -> new ListCell<String>() {
                                private ImageView imageViewArkiva = new ImageView();

                                @Override
                                public void updateItem(String name, boolean empty) {
                                    super.updateItem(name, empty);
                                    if (empty) {
                                        setText(null);
                                        setGraphic(null);
                                    } else {
                                        imageViewArkiva.setImage(archiveImage);
                                        imageViewArkiva.setFitHeight(40);
                                        imageViewArkiva.setFitWidth(40);
                                        setText(name);
                                        setGraphic(imageViewArkiva);
                                    }
                                }
                            });

                    } catch (IOException ex) {
                        Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
    }
    
    @FXML
    public void handleSettingWebServiceUrl(){
        //we're getting urls from a config file called configUrl.json
        File f = new File("configUrl.json");
        String localUrl = ""; String remoteUrl = "";
             if (f.exists() && !f.isDirectory()) {
                  JSONParser parser = new JSONParser();
            try {
                JSONObject urlConfig = (JSONObject) parser.parse(new FileReader("configUrl.json"));
                localUrl = (String) urlConfig.get("local");
                remoteUrl = (String) urlConfig.get("remote");
                 
            } catch (FileNotFoundException e) {
                Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, e);
            } catch (IOException | org.json.simple.parser.ParseException e) {
                Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, e);
            }
            
            }else{
                //get default url for localhost
                localUrl = "http://localhost:8080/ws/archives.wsdl";
                remoteUrl = "";
                //alert user
                // Show the error message.
                Alert alert = new Alert(AlertType.ERROR);
                alert.setTitle("Errore!");
                alert.setHeaderText("Errore di configurazione!");
                alert.setContentText("File di configurazione non è stato trovato. Si prega di contattare l'amministratore del sistema");
                alert.showAndWait();
            }
             
            //set specific url
            if(PrimaNotaFacileClient.getIsRemote()==true){
                //remote
                PrimaNotaFacileClient.setCustomWsUrl(remoteUrl);
            }else{
              //local
              PrimaNotaFacileClient.setCustomWsUrl(localUrl);
            }
             
    }
    
    @FXML 
    public void handleRemoteArchives(){
        File f = new File("configRemote.json");
        remoteArchiveNames.clear();
                if (f.exists() && !f.isDirectory()) {
                    JSONParser parser = new JSONParser();
                    try {
                        JSONArray localArchives = (JSONArray) parser.parse(new FileReader("configRemote.json"));
                        int i = 0;
                        for (Object objArchive : localArchives) {
                            JSONObject archive = (JSONObject) objArchive;
                            String archiveName = (String) archive.get("archiveName");
                            System.out.println(archiveName);
                            //localArchiveNames.set(i, archiveName);
                            remoteArchiveNames.add(archiveName);

                            i++;
                        }

                        listViewRemoteArchives.setItems(remoteArchiveNames);

                        listViewRemoteArchives.setCellFactory(param -> new ListCell<String>() {
                            private ImageView imageViewArkiva = new ImageView();

                            @Override
                            public void updateItem(String name, boolean empty) {
                                super.updateItem(name, empty);
                                if (empty) {
                                    setText(null);
                                    setGraphic(null);
                                } else {
                                    imageViewArkiva.setImage(archiveImage);
                                    imageViewArkiva.setFitHeight(40);
                                    imageViewArkiva.setFitWidth(40);
                                    setText(name);
                                    setGraphic(imageViewArkiva);
                                }
                            }
                        });

                    } catch (FileNotFoundException e) {
                        Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, e);
                    } catch (IOException e) {
                        Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, e);
                    } catch (ParseException e) {
                        Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, e);
                    }
                } else {
                    //nuk gjendet file. Kerko nga SOAP Server dhe krijo file config.txt
                    //set class to search from soap server
                    ArchivesPortService archiveService = new ArchivesPortService();
                    ArchivesPort archivesPort = archiveService.getArchivesPortSoap11();

                    GetAllArchiveRequest allArchivesRequest = new GetAllArchiveRequest();
                    GetAllArchiveResponse allArchivesResponse = archivesPort.getAllArchive(allArchivesRequest);

                    List<Archive> allArchives = allArchivesResponse.getAllArchiveResponse();
                    JSONArray jsonArchives = new JSONArray();
                    for (Archive archive : allArchives) {
                        //per gjithe arkivat qe gjejme do te krijojme file-in JSON 
                        JSONObject jsonArchive = new JSONObject();
                        jsonArchive.put("archiveName", archive.getArchiveName());
                        jsonArchive.put("password", archive.getPassword());
                        jsonArchives.add(jsonArchive);
                    }

                    try (FileWriter file2 = new FileWriter("configRemote.json")) {
                        file2.write(jsonArchives.toJSONString());
                        file2.close();
                        System.out.println("Successfully Copied JSON Object to File...");
                        System.out.println("\nJSON Object: " + jsonArchives);
                        //pasi krijohet file JSON, vazhdojme me shtimin e elementeve ne Grid. 
                        //get archives from JSon array created earlier
                            int i = 0;
                            for (Object objArchive : jsonArchives) {
                                JSONObject archive = (JSONObject) objArchive;
                                String archiveName = (String) archive.get("archiveName");
                                System.out.println(archiveName);
                                remoteArchiveNames.add(archiveName);
                                i++;
                            }

                            listViewRemoteArchives.setItems(remoteArchiveNames);
                            listViewRemoteArchives.setCellFactory(param -> new ListCell<String>() {
                                private ImageView imageViewArkiva = new ImageView();

                                @Override
                                public void updateItem(String name, boolean empty) {
                                    super.updateItem(name, empty);
                                    if (empty) {
                                        setText(null);
                                        setGraphic(null);
                                    } else {
                                        imageViewArkiva.setImage(archiveImage);
                                        imageViewArkiva.setFitHeight(40);
                                        imageViewArkiva.setFitWidth(40);
                                        setText(name);
                                        setGraphic(imageViewArkiva);
                                    }
                                }
                            });

                    } catch (IOException ex) {
                        Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
    }
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        
        //check for active tab in tab pane
        Tab activeTab = tabPane.getSelectionModel().getSelectedItem();
          tabPane.getStyleClass().add("floating");
        //During initialisation we'll search for config.json JSON file with all archives listed.
        if(activeTab == tabLocalArchives){
            //local           
            PrimaNotaFacileClient.setIsRemote(Boolean.FALSE);
            handleSettingWebServiceUrl();
            handleLocalArchives();
        }else{
            PrimaNotaFacileClient.setIsRemote(Boolean.TRUE);
            handleSettingWebServiceUrl();
            handleRemoteArchives();
        }

        //add event handler for tab selection
        tabPane.getSelectionModel().selectedItemProperty().addListener((ov, oldTab, newTab) -> {
            if (newTab == tabLocalArchives){
                 //local archives
                PrimaNotaFacileClient.setIsRemote(Boolean.FALSE);
                handleSettingWebServiceUrl();
                handleLocalArchives();
            
             }else{
                //remote archives
               PrimaNotaFacileClient.setIsRemote(Boolean.TRUE);
               handleSettingWebServiceUrl();
               handleRemoteArchives();
            }          
        });
        
       

    }
}
