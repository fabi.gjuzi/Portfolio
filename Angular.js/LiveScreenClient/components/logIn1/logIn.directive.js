(function() {

    'use strict';

    angular.module('app.login')
        .directive('tmplLogin', directiveFunction)
        .controller('LogInController', ['$scope', '$http', '$location', '$window', '$rootScope', function($scope, $http, $location, $window, $rootScope) {
            $rootScope.currentView = "login";
            $scope.submitLogIn = function() {
                $http({
                    url: "http://192.168.2.221/livescreen_webservice/users/logIn",
                    method: 'POST',
                    headers: { 'Auth-Key': 'engjelltest', 'Content-Type': 'application/json' },
                    data: { 'username': this.username, 'password': this.password }
                }).then(function successCallback(response) {
                    if (parseInt(JSON.stringify(response['data']['response']['code'])) == 0) {
                        $rootScope.currentView = 'spot';
                    } else {
                        alert('LogIn i pa suksesshem');
                    }
                }, function errorCallback(response) {
                    alert("Cannot connect to server");
                });
            }
        }]);
    // ----- directiveFunction -----
    directiveFunction.$inject = [];

    /* @ngInject */
    function directiveFunction() {

        var directive = {
            restrict: 'E',
            templateUrl: 'components/logIn/logIn.html',
            scope: {},
            controller: 'LogInController',
            controllerAs: 'vm'
        };

        return directive;
    }
})();