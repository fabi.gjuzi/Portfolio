(function() {

    angular.module('newSpotWizard')
        .directive('tmplNewspotwizard', directiveFunction)
        .directive('ngFiles', ['$parse', function($parse) {

            function fn_link(scope, element, attrs) {

                var onChange = $parse(attrs.ngFiles);
                element.on('change', function(event) {

                    onChange(scope, { $files: event.target.files });
                });
            };

            return {

                link: fn_link
            }
        }])
        .directive('numberOnlyInput', function() {

            return {

                restrict: 'EA',
                template: '<input name="{{inputName}}" ng-model="inputValue" id="durationValue"/>',
                scope: {
                    inputValue: '=',
                    inputName: '='
                },
                link: function(scope) {

                    scope.$watch('inputValue', function(newValue, oldValue) {

                        var arr = String(newValue).split("");
                        if (arr.length === 0) {

                            return;
                        }
                        if (arr.length === 1 && (arr[0] == '-' || arr[0] === '.')) {

                            return;
                        }
                        if (arr.length === 2 && newValue === '-.') {

                            return;
                        }
                        if (isNaN(newValue)) {

                            scope.inputValue = oldValue;
                        }
                    });
                }
            };
        })
        .controller('NewSpotWizardController', ['$scope', '$http', '$rootScope', '$base64', 'FileSaver', '$q', function($scope, $http, $rootScope, $base64, FileSaver, $q) {

            var entry_animation;
            var exit_animation;
            $scope.filename;
            $scope.step = 1;
            $scope.isUploaded = false;
            $scope.wks = { number: 1, name: 'duration' };
            var lastAddedMediaFileID;

            $scope.imageBorders = [
                { id: '1', active: false, disabled: false, src: "../../assets/images/top_left" },
                { id: '2', active: false, disabled: false, src: "../../assets/images/top_right" },
                { id: '3', active: false, disabled: false, src: "../../assets/images/center" },
                { id: '4', active: false, disabled: false, src: "../../assets/images/fullscreen" },
                { id: '5', active: false, disabled: false, src: "../../assets/images/bottom_left" },
                { id: '6', active: false, disabled: false, src: "../../assets/images/bottom_right" }
            ];

            $scope.datas = { "id_content_type": 1, "id_animation_entry": null, "id_animation_exit": null, "duration": null, "id_layout": null }

            $http({

                url: "http://192.168.2.221/livescreen_webservice/animations/getAll",
                method: 'GET',
                headers: { 'Auth-Key': 'engjelltest', 'Content-Type': 'application/json' },
                data: {}
            }).then(function successCallback(response) {

                $scope.showUpAnimation = [response['data']['response']['body'][0]['name'], response['data']['response']['body'][1]['name']];
                // alert(JSON.stringify(response['data']['response']['body'][0]['name']));
                $scope.closeAnimation = $scope.showUpAnimation;
            }, function errorCallback(response) {

                alert("Cannot connect to server");
            });

            $scope.setEntryAnimation = function() {

                entry_animation = document.getElementById('showUpAnimationValue').value;
                if (entry_animation.includes('In')) {
                    $scope.datas['id_animation_entry'] = 1;
                } else {
                    $scope.datas['id_animation_entry'] = 2;
                }
            }

            $scope.setExitAnimation = function() {

                exit_animation = document.getElementById('closeAnimationValue').value;
                if (exit_animation.includes('In')) {
                    $scope.datas['id_animation_exit'] = 1;
                } else {
                    $scope.datas['id_animation_exit'] = 2;
                }
            }

            $scope.enableImageBorder = function(index) {

                $scope.imageBorders.forEach(function(b) {
                    b.active = false;
                });
                $scope.imageBorders[index].active = true;
                $scope.datas['id_layout'] = index + 1;
            }

            $scope.finishClicked = function() {

                if ($scope.datas['id_layout'] != null) {

                    $scope.datas['duration'] = document.getElementById('durationValue').value;
                    $http({

                        url: "http://192.168.2.221/livescreen_webservice/blocks/add",
                        method: 'POST',
                        headers: { 'Auth-Key': 'engjelltest', 'Content-Type': 'application/json' },
                        data: $scope.datas
                    }).then(function successCallback(response) {

                        console.log(JSON.stringify(response['data']['response']['body']['block_id']));
                        console.log(lastAddedMediaFileID);
                        $http({

                            url: "http://192.168.2.221/livescreen_webservice/media_file_contents/update",
                            method: 'PUT',
                            headers: { 'Auth-Key': 'engjelltest', 'Content-Type': 'application/json' },
                            data: { "media_file_content_id": lastAddedMediaFileID, "block_id": response['data']['response']['body']['block_id'] }
                        }).then(function successCallback(response) {

                            alert(JSON.stringify(response['data']));
                        }, function errorCallback(response) {

                            alert("Cannot connect to server");
                        });
                    }, function errorCallback(response) {

                        alert("Cannot connect to server");
                    });
                } else {

                    alert("Please choose a Layout!");
                }
            }

            $scope.nextClicked = function() {

                if ($scope.step == 1) {

                    if (document.getElementById('showUpAnimationValue').value == "?") {

                        alert("Set Show-Up Animation value!")
                    } else if (document.getElementById('closeAnimationValue').value == "?") {

                        alert("Set Close Animation value!");
                    } else {

                        $scope.step++;
                        jQuery('#wizard').smartWizard('goToStep', 2);
                    }
                } else if ($scope.step == 2) {

                    if (document.getElementById('upload').value == "" || !$scope.isUploaded) {

                        alert("Please upload a file!");
                    } else {

                        $scope.step++;
                        jQuery('#wizard').smartWizard('goToStep', 3);
                    }
                } else if ($scope.step == 3) {

                    if (!(document.getElementById('durationValue').value == "")) {

                        $scope.step++;
                        jQuery('#wizard').smartWizard('goToStep', 4);
                    } else {

                        alert("Please set Duration value");
                    }
                }
            }

            $scope.changeStep = function(step) {

                $scope.step = step;
            }

            var formdata = new FormData();
            $scope.getTheFiles = function($files) {

                angular.forEach($files, function(value, key) {

                    formdata.append(key, value);
                    $scope.filename = $files[0].name;
                });
            };

            // NOW UPLOAD THE FILES.
            $scope.uploadFiles = function() {

                var data1;
                $scope.isUploaded = true;

                var f = document.getElementById('upload').files[0],
                    r = new FileReader();
                r.onloadend = function(e) {
                    data1 = e.target.result;

                    var request = {

                        method: 'POST',
                        url: 'http://192.168.2.221/livescreen_webservice/media_file_contents/add',
                        data: {
                            "content": e.target.result,
                            "name": f['name'],
                            "type": f['type'].split('/')[1]
                        },
                        headers: { 'Auth-Key': 'engjelltest', 'Content-Type': 'application/json' }
                    };

                    // SEND THE FILES.
                    $http(request)
                        .success(function(d) {

                            lastAddedMediaFileID = d['response']['body']['media_file_content_id'];
                            var blob = dataURItoBlob(d['response']['body']['content']);
                            var file = new File([blob], d['response']['body']['name'], { type: "'image/" + d['response']['body']['type'] });
                            // FileSaver.saveAs(file, d['response']['body']['name']);
                        })
                        .error(function() {

                        });
                }
                r.readAsDataURL(f);
            }

            function dataURItoBlob(dataURI) {

                // convert base64/URLEncoded data component to raw binary data held in a string
                var byteString;
                if (dataURI.split(',')[0].indexOf('base64') >= 0) {

                    byteString = atob(dataURI.split(',')[1]);
                } else {

                    byteString = unescape(dataURI.split(',')[1]);
                }
                // separate out the mime component
                var mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0];

                // write the bytes of the string to a typed array
                var ia = new Uint8Array(byteString.length);
                for (var i = 0; i < byteString.length; i++) {

                    ia[i] = byteString.charCodeAt(i);
                }

                return new Blob([ia], { type: mimeString });
            }

        }]);

    // ----- directiveFunction -----
    directiveFunction.$inject = [];

    /* @ngInject */
    function directiveFunction() {

        var directive = {

            restrict: 'E',
            templateUrl: 'components/newSpotWizard/newSpotWizard.html',
            scope: {},
            controller: 'NewSpotWizardController',
            controllerAs: 'vm'
        };

        return directive;
    }
})()