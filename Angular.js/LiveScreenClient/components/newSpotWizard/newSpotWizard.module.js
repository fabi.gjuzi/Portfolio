(function() {
    'use strict';

    angular.module('newSpotWizard', ['base64', 'ngFileSaver']);
})();