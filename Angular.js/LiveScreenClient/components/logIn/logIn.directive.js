(function() {

    'use strict';

    angular.module('index')
        .directive('tmplIndex', directiveFunction)
        .controller('IndexController', ['$scope', '$http', '$location', '$window', '$rootScope', function($scope, $http, $location, $window, $rootScope) {
            $rootScope.username;
            $scope.submitLogIn = function() {
                localStorage.setItem('username', document.getElementById("usernameValue").value);
                $window.open('../spot/spot', '_self');
                // $http({

                //     url: "http://192.168.2.221/livescreen_webservice/users/logIn",
                //     method: 'POST',
                //     headers: { 'Auth-Key': 'engjelltest', 'Content-Type': 'application/json' },
                //     data: { 'username': this.username, 'password': this.password }
                // }).then(function successCallback(response) {

                //     if (parseInt(JSON.stringify(response['data']['response']['code'])) == 0) {

                //         localStorage.setItem('username', document.getElementById("usernameValue").value);
                //         $window.open('../spot/spot', '_self');
                //     } else {

                //         alert('LogIn i pa suksesshem');
                //     }
                // }, function errorCallback(response) {

                //     alert("Cannot connect to server");
                // });
            }
        }]);
    // ----- directiveFunction -----
    directiveFunction.$inject = [];

    /* @ngInject */
    function directiveFunction() {

        var directive = {

            restrict: 'E',
            templateUrl: 'index.html',
            scope: {},
            controller: 'IndexController',
            controllerAs: 'vm'
        };

        return directive;
    }
})();