(function() {
    'use strict';

    angular.module('gridstack-angular', []);

    var app = angular.module('gridstack-angular');

    app.controller('GridstackController', ['$scope', '$log', function($scope, $log) {

        var gridstack = null;

        this.init = function(element, options) {
            gridstack = element.gridstack(options).data('gridstack');
            return gridstack;
        };

        this.removeItem = function(element) {
            if (gridstack) {
                return gridstack.removeWidget(element, false);
            }
            return null;
        };

        this.addItem = function(element) {
            if (gridstack) {
                gridstack.makeWidget(element);
                return element;
            }
            return null;
        };

        $scope.widgets = [{
            x: 0,
            y: 0,
            width: 3,
            height: 1
        }, {
            x: 0,
            y: 0,
            width: 1,
            height: 1
        }];

        $scope.options = {
            cellHeight: 200,
            verticalMargin: 10
        };

        $scope.addWidget = function() {
            var newWidget = {
                x: 0,
                y: 0,
                width: 1,
                height: 1
            };
            $scope.widgets.push(newWidget);
        };

        $scope.removeWidget = function(w) {
            var index = $scope.widgets.indexOf(w);
            $scope.widgets.splice(index, 1);
        };

        $scope.onChange = function(event, items) {
            // $log.log("onChange event: " + event + " items:" + items);
        };

        $scope.onDragStart = function(event, ui) {
            $log.log(event);
            $log.log(ui);
            // $log.log("onDragStart event: " + event + " ui:" + ui);
        };

        $scope.onDragStop = function(event, ui) {
            // $log.log("onDragStop event: " + event + " ui:" + ui);
        };

        $scope.onResizeStart = function(event, ui) {
            // $log.log("onResizeStart event: " + event + " ui:" + ui);
        };

        $scope.onResizeStop = function(event, ui) {
            // $log.log("onResizeStop event: " + event + " ui:" + ui);
        };

        $scope.onItemAdded = function(item) {
            // $log.log("onItemAdded item: " + item);
        };

        $scope.onItemRemoved = function(item) {
            // $log.log("onItemRemoved item: " + item);
        };


    }]);
})();