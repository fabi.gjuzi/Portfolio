(function() {

    'use strict';

    angular.module('spot')
        .directive('tmplSpot', directiveFunction)
        .controller('SpotController', ['$scope', '$http', '$rootScope', '$window', function($scope, $http, $rootScope, $window) {
            $scope.id;
            $scope.index;
            $http({
                url: "http://192.168.2.221/livescreen_webservice/spots/getAllSpotsByUser",
                method: 'POST',
                headers: { 'Auth-Key': 'engjelltest', 'Content-Type': 'application/json' },
                data: { "username": localStorage.getItem('username') }
            }).then(function successCallback(response) {
                $scope.spots = response['data']['response']['body'];
            }, function errorCallback(response) {
                alert("Could not connect to server");
            });
            $scope.deleteSpot = function() {
                $http({
                    url: "http://192.168.2.221/livescreen_webservice/spots/delete/" + $scope.id,
                    method: 'DELETE',
                    headers: { 'Auth-Key': 'engjelltest', 'Content-Type': 'application/json' },
                    data: ''
                }).then(function successCallback(response) {
                    $scope.spots.splice($scope.index, 1);
                }, function errorCallback(response) {
                    // called asynchronously if an error occurs
                    // or server returns response with an error status.
                });
            }
            $scope.openModal = function(id, index) {
                $scope.id = id;
                $scope.index = index;
            }

            $scope.insertNewSpot = function() {
                $window.open('../../components/newSpot/newSpot', '_self');
            }

            $scope.modifySpot = function(spot_id) {

                localStorage.setItem('spot_id_to_modify', spot_id);
                $window.open('../../components/newSpot/newSpot', '_self');
            }
        }]);


    // ----- directiveFunction -----
    directiveFunction.$inject = [];

    /* @ngInject */
    function directiveFunction() {

        var directive = {
            restrict: 'E',
            templateUrl: 'components/spot/spot.html',
            scope: {},
            controller: 'SpotController',
            controllerAs: 'vm'
        };

        return directive;
    }
})();