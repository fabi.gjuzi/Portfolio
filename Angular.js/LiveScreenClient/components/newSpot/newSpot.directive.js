(function() {

    'use strict';

    angular.module('newSpot')
        .directive('tmplNewspot', directiveFunction)
        .directive('gridstack', ['$timeout', function($timeout) {

            return {
                restrict: 'A',
                controller: 'NewSpotController',
                scope: {
                    onChange: '&',
                    onDragStart: '&',
                    onDragStop: '&',
                    onResizeStart: '&',
                    onResizeStop: '&',
                    gridstackHandler: '=?',
                    options: '='
                },
                link: function(scope, element, attrs, controller, ngModel) {

                    var gridstack = controller.init(element, scope.options);
                    scope.gridstackHandler = gridstack;

                    element.on('change', function(e, items) {
                        $timeout(function() {
                            scope.$apply();
                            scope.onChange({ event: e, items: items });
                        });
                    });

                    element.on('dragstart', function(e, ui) {
                        scope.onDragStart({ event: e, ui: ui });
                    });

                    element.on('dragstop', function(e, ui) {
                        $timeout(function() {
                            scope.$apply();
                            scope.onDragStop({ event: e, ui: ui });
                        });
                    });

                    element.on('resizestart', function(e, ui) {
                        scope.onResizeStart({ event: e, ui: ui });
                    });

                    element.on('resizestop', function(e, ui) {
                        $timeout(function() {
                            scope.$apply();
                            scope.onResizeStop({ event: e, ui: ui });
                        });
                    });

                }
            }
        }])
        .controller('NewSpotController', ['$scope', '$http', '$log', '$rootScope', function($scope, $http, $log, $rootScope) {
            $scope.range = function(n) {

                return new Array(n);
            };

            var gridstack = null;

            this.init = function(element, options) {

                gridstack = element.gridstack(options).data('gridstack');
                return gridstack;
            };

            this.removeItem = function(element) {

                if (gridstack) {
                    return gridstack.removeWidget(element, false);
                }
                return null;
            };

            this.addItem = function(element) {

                if (gridstack) {
                    gridstack.makeWidget(element);
                    return element;
                }
                return null;
            };



            $scope.onChange = function(event, items) {
                // $log.log("onChange event: " + event + " items:" + items);
            };

            $scope.onDragStart = function(event, ui) {
                // $log.log(event);
                // $log.log(ui);
                // $log.log("onDragStart event: " + event + " ui:" + ui);
            };

            $scope.onDragStop = function(event, ui) {
                // $log.log("onDragStop event: " + event + " ui:" + ui);
            };

            $scope.onResizeStart = function(event, ui) {
                // $log.log("onResizeStart event: " + event + " ui:" + ui);
            };

            $scope.onResizeStop = function(event, ui) {
                // $log.log("onResizeStop event: " + event + " ui:" + ui);
            };

            $scope.onItemAdded = function(item) {
                // $log.log("onItemAdded item: " + item);
            };

            $scope.onItemRemoved = function(item) {
                // $log.log("onItemRemoved item: " + item);
            };


            if (localStorage.getItem('spot_id_to_modify') != null) {

                $http({

                    url: 'http://192.168.2.221/livescreen_webservice/users/getAllBlocksByUser',
                    method: 'POST',
                    headers: { 'Auth-Key': 'engjelltest', 'Content-Type': 'application/json' },
                    data: { 'username': localStorage.getItem('username'), 'spot_id': localStorage.getItem('spot_id_to_modify') }
                }).then(function successCallback(response) {
                        // localStorage.setItem('blocks', JSON.stringify(response['data']['response']['body']));

                        $scope.widgets = [];
                        var image_source;

                        function round(value, step) {
                            step || (step = 1.0);
                            var inv = 1.0 / step;
                            return Math.round(value * inv) / inv;
                        }



                        // console.log($('#container')[0].scrollWidth);
                        // $('#test').attr('style', 'width: ' + $('#container').prop('scrollWidth') + ' !important');
                        // console.log($('.test')[0].width)
                        response['data']['response']['body'].forEach(function(element) {

                            if (element['file_name'].includes('mp4')) {

                                image_source = '../../assets/images/video_type.png';
                            } else if (element['file_name'].includes('jpg') || element['file_name'].includes('jpeg') || element['file_name'].includes('png') || element['file_name'].includes('bmp')) {

                                image_source = '../../assets/images/image_type.png';
                            } else if (element['file_name'].includes('mp3') || element['file_name'].includes('wav') || element['file_name'].includes('ogg') || element['file_name'].includes('wma')) {

                                image_source = '../../assets/images/audio_type.png';
                            }

                            $scope.widgets.push({
                                x: ((round(element['start_time'], 0.5)) / 0.5),
                                y: (parseInt(element['z_index'])),
                                width: ((round(element['duration'], 0.5)) / 0.5),
                                height: 1,
                                auto_position: 0,
                                index: 1,
                                content_name: element['file_name'],
                                image_src: image_source
                            })
                        });

                        $scope.options = {
                            cellHeight: 200,
                            verticalMargin: 10
                        };

                        $scope.addWidget = function() {
                            var newWidget = {
                                x: 1,
                                y: 1,
                                width: 1,
                                height: 1,
                                auto_position: 1,
                                index: 1,
                                content_name: null,
                                image_src: null
                            };
                            $scope.widgets.push(newWidget);
                        };

                        $scope.removeWidget = function(w) {
                            var index = $scope.widgets.indexOf(w);
                            $scope.widgets.splice(index, 1);
                        };
                    },
                    function errorCallback(response) {

                        alert("Cannot connect to server");
                    });
            }


        }]);


    // ----- directiveFunction -----
    directiveFunction.$inject = [];

    /* @ngInject */
    function directiveFunction() {

        var directive = {
            restrict: 'E',
            templateUrl: 'components/newSpot/newSpot.html',
            scope: {},
            controller: 'NewSpotController',
            controllerAs: 'vm'
        };

        return directive;
    }
})();