interact('.draggable')
    // .draggable({
    //     snap: {
    //         targets: [
    //             interact.createSnapGrid({ x: 144.517, y: 100 })
    //         ],
    //         range: Infinity,
    //     },
    //     // inertia: true,
    //     restrict: {
    //         restriction: element.parentNode,
    //         elementRect: { top: 0, left: 0, bottom: 1, right: 1 },
    //         endOnly: true
    //     }
    // })
    .draggable({
        snap: {
            targets: [
                interact.createSnapGrid({ x: 144.517, y: 100 })
            ],
            range: Infinity,
        },
        // enable inertial throwing
        // inertia: true,
        // keep the element within the area of it's parent
        restrict: {
            restriction: "parent",
            endOnly: true,
            elementRect: { top: 1, left: 1, bottom: 0, right: 0 }
        },
        // enable autoScroll
        autoScroll: true,

        // call this function on every dragmove event
        onmove: dragMoveListener,
        // call this function on every dragend event
        onend: function(event) {
            var textEl = event.target.querySelector('p');

            textEl && (textEl.textContent =
                'moved a distance of ' +
                (Math.sqrt(event.dx * event.dx +
                    event.dy * event.dy) | 0) + 'px');
        }
    })
    .resizable({
        preserveAspectRatio: false,
        edges: { left: true, right: true, bottom: false, top: false },
        snap: {
            targets: [
                interact.createSnapGrid({ x: 144.517, y: 50 })
            ]
        },
    })
    .on('resizemove', function(event) {
        var target = event.target,
            x = (parseFloat(target.getAttribute('data-x')) || 0),
            y = (parseFloat(target.getAttribute('data-y')) || 0);

        // update the element's style
        target.style.width = event.rect.width + 'px';
        target.style.height = event.rect.height + 'px';

        // translate when resizing from top or left edges
        x += event.deltaRect.left;
        y += event.deltaRect.top;

        target.style.webkitTransform = target.style.transform =
            'translate(' + x + 'px,' + y + 'px)';

        target.setAttribute('data-x', x);
        target.setAttribute('data-y', y);
        // target.textContent = Math.round(event.rect.width) + '×' + Math.round(event.rect.height);
    });

function dragMoveListener(event) {
    var target = event.target,
        // keep the dragged position in the data-x/data-y attributes
        x = (parseFloat(target.getAttribute('data-x')) || 0) + event.dx,
        y = (parseFloat(target.getAttribute('data-y')) || 0) + event.dy;

    // translate the element
    target.style.webkitTransform =
        target.style.transform =
        'translate(' + x + 'px, ' + y + 'px)';

    // update the posiion attributes
    target.setAttribute('data-x', x);
    target.setAttribute('data-y', y);
}

// interact('.dropzone').dropzone({
//     // only accept elements matching this CSS selector
//     accept: '.draggable',
//     // Require a 75% element overlap for a drop to be possible
//     overlap: 0.75,

//     // listen for drop related events:

//     ondropactivate: function(event) {
//         // add active dropzone feedback
//         event.target.classList.add('drop-active');
//     },
//     ondragenter: function(event) {
//         var draggableElement = event.relatedTarget,
//             dropzoneElement = event.target;

//         // feedback the possibility of a drop
//         dropzoneElement.classList.add('drop-target');
//         draggableElement.classList.add('can-drop');
//         draggableElement.textContent = 'Dragged in';
//     },
//     ondragleave: function(event) {
//         // remove the drop feedback style
//         event.target.classList.remove('drop-target');
//         event.relatedTarget.classList.remove('can-drop');
//         event.relatedTarget.textContent = 'Dragged out';
//     },
//     ondrop: function(event) {
//         event.relatedTarget.textContent = 'Dropped';
//     },
//     ondropdeactivate: function(event) {
//         // remove active dropzone feedback
//         event.target.classList.remove('drop-active');
//         event.target.classList.remove('drop-target');
//     }
// });

// this is used later in the resizing and gesture demos
window.dragMoveListener = dragMoveListener;